<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-booklet-duplex" xml:lang="lv">

  <info>
    <link type="guide" xref="printing-booklet"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="candidate"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Drukājiet salocītas brošūras (kā grāmatu vai bukletu) no PDF, izmantojot parasto A4/vēstuļu izmēra papīru.</desc>
  </info>

  <title>Izdrukāt bukletu ar divpusējo printeri</title>

  <p>Jūs varat izveidot salocītu bukletu (kā nelielu grāmatu), drukājot dokumenta lappuses īpašā secībā un mainot pāris drukāšanas opcijas.</p>

  <p>Šīs instrukcijas ir par to, kā veidot brošūras no PDF dokumentiem.</p>

  <p>Ja vēlaties veidot brošūras no <app>LibreOffice</app> dokumenta, vispirms eksportējiet to uz PDF, izvēloties <guiseq><gui>Datne</gui><gui>Eksportēt kā PDF…</gui></guiseq>. Jūsu dokumentā lapu skaitam ir jādalās ar 4 (4, 8, 12, 16,…). Iespējams, ka jums būs jāpievieno līdz 3 tukšas lapas.</p>

  <p>Lai izdrukātu bukletu:</p>

  <steps>
    <item>
      <p>Atvērt drukāšanas dialoglodziņu. To parasti var izdarīt izvēlnē <gui style="menuitem">Drukāt</gui>, vai izmantojot <keyseq><key>Ctrl</key><key>P</key></keyseq> taustiņu kombināciju.</p>
    </item>
    <item>
      <p>Spiediet <gui>Īpašības…</gui> pogu</p>
      <p>Pārliecinieties, ka izkrītošajā sarakstā <gui>Orientācija</gui> ir izvēlēts<gui>Ainava</gui>.</p>
      <p>Izkrītošajā sarakstā<gui>Duplekss</gui> izvēlieties <gui>Īsā mala</gui>.</p>
      <p>Spiediet <gui>Labi</gui>, lai ietu atpakaļ uz drukāšanas dialoglodziņu.</p>
    </item>
    <item>
      <p>Zem <em>Diapazons un kopijas</em>, izvēlies <gui>Lapas</gui>.</p>
    </item>
    <item>
      <p>Ievadiet lappušu numurus šādā secībā (n ir kopējais lappušu skaits, un tas dalās ar 4):</p>
      <p>n, 1, 2, n-1, n-2, 3, 4, n-3, n-4, 5, 6, n-5, n-6, 7, 8, n-7, n-8, 9, 10, n-9, n-10, 11, 12, n-11…</p>
      <p>Piemēri:</p>
      <list>
        <item><p>4 lappušu bukleti: ievadiet <input>4,1,2,3</input></p></item>
        <item><p>8 lappušu bukleti: ievadiet <input>8,1,2,7,6,3,4,5</input></p></item>
        <item><p>20 lappušu bukleti: ievadiet <input>20,1,2,19,18,3,4,17,16,5,6,15,14,7,8,13,12,9,10,11</input></p></item>
      </list>
    </item>
    <item>
      <p>Izvēlieties <gui>Lappuses izkārtojums</gui> cilni.</p>
      <p>Sadaļā <gui>Izkārtojums</gui> izvēlieties <gui>Brošūra</gui>.</p>
      <p>Sadaļā <em>Lappušu puses</em> izkrītošajā sarakstā <gui>Iekļaut</gui> izvēlieties <gui>Visas lappuses</gui>.</p>
    </item>
    <item>
      <p>Spied <gui>Drukāt</gui>.</p>
    </item>
  </steps>

</page>
