<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-capacity" xml:lang="nl">
  <info>
    <link type="guide" xref="disk"/>

    <credit type="author">
      <name>Gnome-documentatieproject</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <revision pkgversion="3.4.3" date="2012-06-15" status="review"/>
    <revision pkgversion="3.13.91" date="2014-09-05" status="review"/>

    <desc>Gebruik <gui>Schijfgebruik</gui> of <gui>Systeemmonitor</gui> om ruimte en capaciteit te controleren.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

<title>Controleer hoeveel schijfruimte er nog besschikbaar is</title>

  <p>U kunt kijken hoeveel schijfruimte u over heeft via <app>Schijfgebruik</app> en <app>Systeemmonitor</app>.</p>

<section id="disk-usage-analyzer">
<title>Controleren met Schijfgebruik</title>

  <p>Om de vrije ruimte van de schijf en de schijfcapaciteit te controleren via <app>Schijfgebruik</app>:</p>

  <list>
    <item>
      <p>Open <app>Schijfgebruik</app> vanuit het <gui>Activiteiten</gui>-overzicht. Het venster geeft een lijst met bestandslocaties weer tezamen met het gebruik en de capaciteit van elk.</p>
    </item>
    <item>
      <p>Klik op één van de items in de lijst om een gedetailleerde samenvatting van het schijfgebruik te tonen. Klik op de menuknop, en dan op <gui>Map scannen…</gui> of <gui>Map op afstand scannen…</gui> om een andere locatie te scannen.</p>
    </item>
  </list>
  <p>De informatie wordt weergegeven naar <gui>Map</gui>, <gui>Grootte</gui>, <gui>Inhoud</gui> en wanneer de gegevens voor het laatst zijn <gui>Gewijzigd</gui>.  Ga voor meer informatie naar <link href="help:baobab"><app>Schijfgebruik</app></link>.</p>

</section>

<section id="system-monitor">

<title>Met Systeemmonitor controleren</title>

  <p>Vrije schijfruimte en schijfcapaciteit controleren met <app>Systeemmonitor</app>:</p>

<steps>
 <item>
  <p>Open vanuit het <gui>Activiteiten</gui>overzicht de <app>Systeemmonitor</app>-toepassing.</p>
 </item>
 <item>
  <p>Selecteer het tabblad <gui>Bestandssystemen</gui> om de partities van het systeem en het schijfgebruik te bekijken. De informatie wordt weergegeven in de kolommen <gui>Totaal</gui>, <gui>Vrij</gui>, <gui>Beschikbaar</gui> en <gui>Gebruikt</gui>.</p>
 </item>
</steps>
</section>

<section id="disk-full">

<title>Wat als de schijf te vol is?</title>

  <p>Als de schijf te vol is moet u:</p>

 <list>
  <item>
   <p>Bestanden die niet belangrijk zijn of bestanden die u niet meer gebruikt verwijderen.</p>
  </item>
  <item>
   <p>Maak <link xref="backup-why">reservekopieën</link> van de belangrijke bestanden die u voorlopig niet nodig heeft en wis ze van de harde schijf.</p>
  </item>
 </list>
</section>

</page>
