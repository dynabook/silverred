<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-mobile" xml:lang="lv">

  <info>
    <link type="guide" xref="hardware-phone#setup"/>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.14" date="2014-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.24" date="2017-03-26" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>GNOME dokumentācijas projekts</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <desc>Izmantojiet savu tālruni vai interneta modemu puļķi, lai savienotos ar mobilo platjoslas tīklu.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Savienoties ar mobilo platjoslu</title>

  <p>Varat izveidot savienojumu ar mobilo (3G) tīklu ar datorā iebūvētu 3G modemu, savu mobilo tālruni vai interneta puļķi.</p>

  <note style="tip">
  <p>Most phones have a setting called <link xref="net-tethering">USB
  tethering</link> that requires no setup on the computer, and is generally the
  better method to connect to the cellular network.</p>
  </note>

  <steps>
    <item><p>Ja jums nav iebūvēts 3G modems, pievienojiet savu tālruni vai interneta puļķi USB portā savā datorā.</p>
    </item>
    <item>
    <p>Open the <gui xref="shell-introduction#systemmenu">system menu</gui> from the right
    side of the top bar.</p>
  </item>
  <item>
    <p>Izvēlieties <gui><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-signal-excellent-symbolic.svg" width="16" height="16"/> Mobilā platjosla izslēgta</gui>. Tiks izvēsta sadaļa<gui>Mobilā platjosla</gui>.</p>
      <note>
        <p>If <gui>Mobile Broadband</gui> does not appear in the system menu,
        ensure that your device is not set to connect as Mass Storage.</p>
      </note>
    </item>
    <item><p>Izvēlieties <gui>Savienot</gui>. Ja savienojaties pirmo reizi, tiks palaists vednis <gui>Iestatīt mobilās platjoslas savienojumu</gui>. Atvērtais ekrāns parādīs sarakstu ar nepieciešamo informāciju. Spiediet <gui style="button">Nākamais</gui>.</p></item>
    <item><p>Sarakstā izvēlieties sava pakalpojuma sniedzēja valsti vai reģionu. Spiediet <gui style="button">Nākamais</gui>.</p></item>
    <item><p>Sarakstā izvēlieties pakalpojuma sniedzēju. Spiediet <gui style="button">Nākamais</gui>.</p></item>
    <item><p>Izvēlieties plānu atbilstoši savienotās ierīces veidam. Tas noteiks piekļuves punkta nosaukumu. Spiediet <gui style="button">Nākamais</gui>.</p></item>
    <item><p>Apstipriniet izvēlētos iestatījumus, spiežot <gui style="button">Pielietot</gui>. Vednis aizvērsies un <gui>Tīkla</gui> rūts parādīs savienojuma īpašības.</p></item>
  </steps>

</page>
