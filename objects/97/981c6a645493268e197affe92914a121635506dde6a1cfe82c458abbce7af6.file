<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-display" xml:lang="de">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-display"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Symbolbeschriftungen in der Dateiverwaltung festlegen.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2019.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2019.</mal:years>
    </mal:credit>
  </info>

<title>Einstellungen für die Anzeige der Dateiverwaltung</title>

<p>Sie können die Anzeige von Beschriftungen unter Symbolen in der Dateiverwaltung in vielerlei Hinsicht festlegen. Klicken Sie dazu auf den Menüknopf oben rechts im Fenster und wählen Sie <gui>Einstellungen</gui> und anschließend den Reiter <gui>Ansichten</gui>.</p>

<section id="icon-captions">
  <title>Symbolbeschriftungen</title>
  <!-- TODO: update screenshot for 3.18 and above. -->
  <media type="image" src="figures/nautilus-icons.png" width="250" height="110" style="floatend floatright">
    <p>Einstellungen für die Spalten in der Dateiverwaltungssymbole mit Beschriftungen</p>
  </media>
  <p>Wenn Sie die Symbolansicht verwenden, können Sie zusätzliche Informationen über Dateien und Ordner in einer Beschriftung unter jedem einzelnen Symbol anzeigen lassen. Dies ist zum Beispiel nützlich, wenn Sie oft überprüfen müssen, wer der Besitzer einer Datei ist oder wann sie das letzte Mal geändert wurde.</p>
  <p>Sie können einen Ordner vergrößern, indem Sie auf den Ansichtsoptionen-Knopf in der Werkzeugleiste klicken und die Vergrößerungsstufe mit dem Schieberegler wählen. Je weiter Sie die Ansicht vergrößern, desto mehr Informationen werden in den Symbolbeschriftungen angezeigt. Sie können bis zu drei Informationen pro Beschriftungen anzeigen lassen. Die erste wird in den meisten Ansichtsgrößen angezeigt, die letzte nur bei sehr starker Vergrößerung.</p>
  <p>Die Informationen, die Sie in den Symbolbeschriftungen anzeigen lassen können, sind dieselben, die in der Listenansicht zur Verfügung stehen. Siehe <link xref="nautilus-list"/> für weitere Informationen.</p>
</section>

<section id="list-view">

  <title>Listenansicht</title>

  <p>When viewing files as a list, you can <gui>Allow folders to be
  expanded</gui>. This shows expanders on each directory in the file list, so
  that the contents of several folders can be shown at once. This is useful if
  the folder structure is relevant, such as if your music files are organized
  with a folder per artist, and a subfolder per album.</p>

</section>

</page>
