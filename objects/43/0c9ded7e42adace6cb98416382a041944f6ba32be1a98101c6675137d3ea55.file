<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-sort" xml:lang="el">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-25" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Τακτοποιήστε τα αρχεία κατά όνομα, μέγεθος, τύπο, ή όταν έχουν αλλάξει.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

<title>Ταξινόμηση αρχείων και φακέλων</title>

<p>Μπορείτε να ταξινομήσετε τα αρχεία με διαφορετικούς τρόπους σε έναν φάκελο, για παράδειγμα ταξινομώντας τα με σειρά ημερομηνίας ή μεγέθους αρχείου. Δείτε <link xref="#ways"/> παρακάτω για μια λίστα συνηθισμένων τρόπων ταξινόμησης αρχείων. Δείτε <link xref="nautilus-views"/> για πληροφορίες πώς να αλλάξετε την προεπιλεγμένη σειρά ταξινόμησης.</p>

<p>Ο τρόπος που μπορείτε να ταξινομήσετε τα αρχεία εξαρτάται από την <em>προβολή φακέλου</em> που χρησιμοποιείτε. Μπορείτε να αλλάξετε την τρέχουσα προβολή χρησιμοποιώντας τη λίστα ή τα κουμπιά εικονιδίων στην εργαλειοθήκη.</p>

<section id="icon-view">
  <title>Προβολή εικονιδίων</title>

  <p>Για να ταξινομήσετε τα αρχεία με διαφορετική σειρά, κάντε κλικ στο κουμπί προβολής επιλογών στην εργαλειοθήκη και επιλέξτε <gui>Κατά όνομα</gui>, <gui>Κατά μέγεθος</gui>, <gui>Κατά τύπο</gui>, <gui>Κατά ημερομηνία τροποποίησης</gui>, ή <gui>Κατά ημερομηνία πρόσβασης</gui>.</p>

  <p>Για παράδειγμα, εάν επιλέξετε <gui>Κατά όνομα</gui>, τα αρχεία θα ταξινομηθούν με τα ονόματά τους, σε αλφαβητική σειρά. Για άλλες επιλογές δείτε <link xref="#ways"/>.</p>

  <p>Μπορείτε να ταξινομήσετε με την αντίστροφη σειρά επιλέγοντας <gui>Αντίστροφη σειρά</gui> από το μενού.</p>

</section>

<section id="list-view">
  <title>Προβολή λίστας</title>

  <p>Για να ταξινομήσετε αρχεία με διαφορετική σειρά, πατήστε μια από τις κεφαλίδες της στήλης στον διαχειριστή αρχείων. Για παράδειγμα, κάντε κλικ στο <gui>Τύπος</gui> για ταξινόμηση κατά τύπο αρχείου. Κάντε κλικ στην κεφαλίδα της στήλης ξανά για ταξινόμηση με αντίστροφη σειρά.</p>
  <p>Στη προβολή λίστας, μπορείτε να εμφανίσετε τις στήλες με τα περισσότερα γνωρίσματα και να ταξινομήσετε αυτές τις στήλες. Κάντε κλικ στο κουμπί επιλογών στην εργαλειοθήκη, επιλέξτε <gui>Ορατές στήλες…</gui> και επιλέξτε τις στήλες που θέλετε να είναι ορατές. Θα μπορείτε τότε να ταξινομήσετε αυτές τις στήλες. Δείτε <link xref="nautilus-list"/> για περιγραφές των διαθέσιμων στηλών.</p>

</section>

<section id="ways">
  <title>Τρόποι ταξινόμησης αρχείων</title>

  <terms>
    <item>
      <title>Όνομα</title>
      <p>Ταξινομεί αλφαβητικά κατά το όνομα του αρχείου.</p>
    </item>
    <item>
      <title>Μέγεθος</title>
      <p>Ταξινομεί κατά το μέγεθος του αρχείου (πόσο χώρο δίσκου καταλαμβάνει). Ταξινομεί από το πιο μικρό προς το πιο μεγάλο από προεπιλογή.</p>
    </item>
    <item>
      <title>Τύπος</title>
      <p>Ταξινομεί αλφαβητικά κατά το είδος του αρχείου. Αρχεία του ίδιου είδους ομαδοποιούνται μαζί, έπειτα ταξινομούνται κατά όνομα.</p>
    </item>
    <item>
      <title>Τελευταία τροποποίηση</title>
      <p>Ταξινομεί κατά ημερομηνία και ώρα που ένα αρχείο άλλαξε τελευταία. Ταξινομεί από το πιο παλιό προς το πιο νέο από προεπιλογή.</p>
    </item>
  </terms>

</section>

</page>
