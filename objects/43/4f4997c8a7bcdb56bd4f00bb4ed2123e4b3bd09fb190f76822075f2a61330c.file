<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-brightness" xml:lang="id">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="guide" xref="hardware-problems-graphics"/>
    <link type="seealso" xref="power-whydim"/>
    <link type="seealso" xref="a11y-contrast"/>

    <revision pkgversion="3.8" version="0.4" date="2013-03-28" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.28" date="2018-07-19" status="review"/>
    <revision pkgversion="3.33" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Proyek Dokumentasi GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ubah kecerahan layar agar layar lebih mudah dibaca saat berada di cahaya yang terang.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

  <title>Atur kecerahan layar</title>

  <p>Tergantung dari perangkat keras Anda, Anda dapat mengubah kecerahan layar Anda untuk menghemat daya atau membuat layar lebih mudah dibaca saat berada di cahaya yang terang.</p>

  <p>To change the brightness of your screen, click the
  <gui xref="shell-introduction#systemmenu">system menu</gui> on the right side
  of the top bar and adjust the screen brightness slider to the value you want
  to use. The change should take effect immediately.</p>

  <note style="tip">
    <p>Banyak papan tik laptop memiliki tombol khusus untuk mengatur kecerahan layar. Umumnya memiliki gambar yang terlihat seperti matahari. Tekan dan tahan tombol <key>Fn</key> untuk menggunakan tombol tersebut.</p>
  </note>

  <p>Anda juga dapat mengatur kecerahan layar dengan menggunakan panel <gui>Daya</gui>.</p>

  <steps>
    <title>Untuk mengatur kecerahan layar menggunakan panel Daya:</title>
    <item>
      <p>Buka ringkasan <gui xref="shell-introduction#activities">Aktivitas</gui> dan mulai mengetik <gui>Daya</gui>.</p>
    </item>
    <item>
      <p>Klik pada <gui>Daya</gui> untuk membuka panel.</p>
    </item>
    <item>
      <p>Geser <gui>Kecerahan layar</gui> ke nilai yang ingin Anda gunakan. Efek perubahannya harusnya langsung terjadi.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Bila komputer Anda memiliki integrasi fitur sensor cahaya, kecerahan layar akan otomatis disesuaikan untuk Anda. Anda dapat menonaktifkan kecerahan layar otomatis pada panel <gui>Daya</gui>.</p>
  </note>

  <p>Bila memungkinkan untuk mengatur kecerahan layar, Anda juga dapat meredupkan layar secara otomatis untuk menghemat Daya. Untuk informasi lebih lanjut, lihat <link xref="power-whydim"/>.</p>

</page>
