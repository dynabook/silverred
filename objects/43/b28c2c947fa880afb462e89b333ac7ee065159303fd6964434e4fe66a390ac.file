<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-where" xml:lang="hu">

  <info>
    <link type="guide" xref="backup-why"/>
    <title type="sort">c</title>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>GNOME dokumentációs projekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Hol tárolja mentéseit, és milyen típusú tárolóeszközt használjon.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

<title>Hol tárolja a mentést</title>

  <p>A biztonsági mentéseket a számítógépétől külön helyen tárolja (például külső merevlemezen). Így ha a számítógép megsérül, a mentés érintetlen marad. A legnagyobb biztonság érdekében ne tárolja a biztonsági mentést a számítógéppel azonos épületben. Ha tűz üt ki, vagy lopás történik, akkor az adatok mindkét másolata elveszhet, ha együtt tárolja azokat.</p>

  <p>Fontos a megfelelő <em>mentési adathordozó</em> kiválasztása is. Olyan eszközt válasszon, amely elegendő kapacitással rendelkezik az összes menteni kívánt fájlhoz.</p>

   <list style="compact">
    <title>Helyi és távoli tárolási lehetőségek</title>
    <item>
      <p>USB memóriakulcs (alacsony kapacitás)</p>
    </item>
    <item>
      <p>Belső lemezmeghajtó (nagy kapacitás)</p>
    </item>
    <item>
      <p>Külső lemezmeghajtó (általában nagy kapacitás)</p>
    </item>
    <item>
      <p>Hálózati meghajtó (nagy kapacitás)</p>
    </item>
    <item>
      <p>Fájl- vagy mentési kiszolgáló (nagy kapacitás)</p>
    </item>
    <item>
     <p>Írható CD vagy DVD (alacsony/közepes kapacitás)</p>
    </item>
    <item>
     <p>Online mentési szolgáltatás (például <link href="http://aws.amazon.com/s3/">Amazon S3</link>; a kapacitás díjfüggő)</p>
    </item>
   </list>

  <p>Ezek némelyike elegendő kapacitással rendelkezik a rendszer minden fájljának mentéséhez, amit <em>teljes rendszermentésnek</em> is neveznek.</p>
</page>
