<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-capacity" xml:lang="de">
  <info>
    <link type="guide" xref="disk"/>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <revision pkgversion="3.4.3" date="2012-06-15" status="review"/>
    <revision pkgversion="3.13.91" date="2014-09-05" status="review"/>

    <desc>Verwenden Sie die <gui>Festplattenbelegungsanalyse</gui> oder die <gui>Systemüberwachung</gui> zum Überprüfen des vorhandenen Plattenplatzes und der Kapazität.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2019.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2019.</mal:years>
    </mal:credit>
  </info>

<title>Überprüfen, wieviel Festplattenspeicher verfügbar ist</title>

  <p>Mit den Anwendungen <app>Festplattenbelegungsanalyse</app> und <app>Systemüberwachung</app> können Sie überprüfen, wie viel Festplattenplatz noch verfügbar ist.</p>

<section id="disk-usage-analyzer">
<title>Überprüfen mit der Festplattenbelegungsanalyse</title>

  <p>So überprüfen Sie den freien Plattenplatz und die Kapazität von Datenträgern mit der <app>Festplattenbelegungsanalyse</app>:</p>

  <list>
    <item>
      <p>Öffnen Sie die Anwendung <app>Festplattenbelegungsanalyse</app> in der <gui>Aktivitäten</gui>-Übersicht. Das Fenster zeigt Ihnen eine Liste mit Laufwerken und Einhängeorten, zusammen mit der jeweiligen Belegung und Speicherkapazität.</p>
    </item>
    <item>
      <p>Klicken Sie auf einen der Einträge in der Liste, um eine detaillierte Übersicht der Belegung dafür anzuzeigen. Klicken Sie auf den Menüknopf und dann auf <gui>Ordner analysieren …</gui> oder <gui>Entfernten Ordner analysieren …</gui>, um einen anderen Ordner zu analysieren.</p>
    </item>
  </list>
  <p>Die Informationen werden nach <gui>Ordner</gui>, <gui>Größe</gui> und <gui>Inhalt</gui> aufgeschlüsselt, sowie nach dem letzten <gui>Änderungsdatum</gui> der Daten. Weitere Details hierzu finden Sie im Handbuch der <link href="help:baobab"><app>Festplattenbelegungsanalyse</app></link>.</p>

</section>

<section id="system-monitor">

<title>Überprüfen mit der Systemüberwachung</title>

  <p>So überprüfen Sie den freien Festplattenplatz und die Festplattenkapazität mit der <app>Systemüberwachung</app>:</p>

<steps>
 <item>
  <p>Öffnen Sie in der <gui>Aktivitäten</gui>-Übersicht die Anwendung <app>Systemüberwachung</app>.</p>
 </item>
 <item>
  <p>Wählen Sie den Reiter <gui>Dateisysteme</gui>, um die Partitionen des Systems und ihre Festplattenbelegung zu betrachten, aufgeschlüsselt in <gui>Gesamt</gui>, <gui>Frei</gui>, <gui>Verfügbar</gui> und <gui>Belegt</gui>.</p>
 </item>
</steps>
</section>

<section id="disk-full">

<title>Warum ist die Festplatte zu voll?</title>

  <p>Falls die Festplatte zu voll ist, sollten Sie:</p>

 <list>
  <item>
   <p>Dateien löschen, die unbedeutend sind oder nicht mehr benötigt werden.</p>
  </item>
  <item>
   <p><link xref="backup-why">Sicherungen</link> von wichtigen Dateien erstellen, die Sie vorübergehend nicht benötigen und diese von der Festplatte entfernen.</p>
  </item>
 </list>
</section>

</page>
