<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="desktop-lockscreen" xml:lang="cs">

  <info>
    <link type="guide" xref="appearance"/>
    <link type="seealso" xref="dconf-profiles"/>
    <link type="seealso" xref="dconf-lockdown"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

    <desc>Jak zařídit, aby se obrazovka po nějaké době nečinnosti automaticky zamkla a uživatel tak musel zadat heslo.</desc>
  </info>

  <title>Zamknutí obrazovky při nečinnosti uživatele</title>

  <p>Můžete obrazovku přimět, aby se automaticky uzamkla, kdykoliv nebude uživatel vyvíjet žádnou činnost po nějakou dobu. To se hodí, když uživatel nechává svůj počítač bez dozoru na veřejném nebo nezabezpečeném místě.</p>

  <steps>
    <title>Povolení automatického zamykání obrazovky</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
    <item>
      <p>Vytvořte soubor s klíči <file>/etc/dconf/db/local.d/00-screensaver</file>, který bude poskytovat informace pro databázi <sys>local</sys>.</p>
      <listing>
      <title><file>/etc/dconf/db/local.d/00-screensaver</file></title>
<code>
# Specify the dconf path
[org/gnome/desktop/session]

# Number of seconds of inactivity before the screen goes blank
# Když chcete šetřič obrazovky úplně vypnout, nastavte na 0 sekund
idle-delay=uint32 180

# Specify the dconf path
[org/gnome/desktop/screensaver]

# Number of seconds after the screen is blank before locking the screen
lock-delay=uint32 0
</code>
      </listing>
      <p>Musíte vložit <code>uint32</code> spolu s celočíselnou hodnotou klíče, jak bylo ukázáno.</p>
    </item>
    <item>
      <p>Aby uživatel nemohl tato nastavení přepsat, vytvořte soubor <file>/etc/dconf/db/local.d/locks/screensaver</file> s následujícím obsahem:</p>
      <listing>
      <title><file>/etc/dconf/db/local.db/locks/screensaver</file></title>
<code>
# Lock desktop screensaver settings
/org/gnome/desktop/session/idle-delay
/org/gnome/desktop/screensaver/lock-delay
</code>
      </listing>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
  </steps>

</page>
