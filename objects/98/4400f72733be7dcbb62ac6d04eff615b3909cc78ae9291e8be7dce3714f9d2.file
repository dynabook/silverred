<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-use-windows-workspaces" xml:lang="he">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>
    <link type="guide" xref="getting-started" group="tasks"/>
    <title role="trail" type="link">שימוש בחלונות ובשולחנות עבודה</title>
    <link type="seealso" xref="shell-windows-switching"/>
    <title role="seealso" type="link">הדרכה עבור שימוש בחלונות ושולחנות עבודה</title>
    <link type="next" xref="gs-use-system-search"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Niv Baehr</mal:name>
      <mal:email>bloop93@gmail.com</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  </info>

  <title>שימוש בחלונות ובשולחנות עבודה</title>

  <ui:overlay width="812" height="452">
  <media type="video" its:translate="no" src="figures/gnome-windows-and-workspaces.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-windows-and-workspaces.svg"/>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>חלונות ושולחנות עבודה</tt:p>
         </tt:div>
         <tt:div begin="6s" end="10s">
           <tt:p>To maximize a window, grab the window’s titlebar and drag it to
            the top of the screen.</tt:p>
           </tt:div>
         <tt:div begin="10s" end="13s">
           <tt:p>כאשר המסך יודגש, שחררו את החלון.</tt:p>
         </tt:div>
         <tt:div begin="14s" end="20s">
           <tt:p>To unmaximize a window, grab the window’s titlebar and drag it
            away from the edges of the screen.</tt:p>
         </tt:div>
         <tt:div begin="25s" end="29s">
           <tt:p>ניתן לגרור את החלון ולשחזר את גודלו גם על ידי לחיצה על הלוח העליון.</tt:p>
         </tt:div>
         <tt:div begin="34s" end="38s">
           <tt:p>To maximize a window along the left side of the screen, grab
            the window’s titlebar and drag it to the left.</tt:p>
         </tt:div>
         <tt:div begin="38s" end="40s">
           <tt:p>כאשר מחצית המסך תודגש, שחררו את החלון.</tt:p>
         </tt:div>
         <tt:div begin="41s" end="44s">
           <tt:p>To maximize a window along the right side of the screen, grab
            the window’s titlebar and drag it to the right.</tt:p>
         </tt:div>
         <tt:div begin="44s" end="48s">
           <tt:p>כאשר מחצית המסך תודגש, שחררו את החלון.</tt:p>
         </tt:div>
         <tt:div begin="54s" end="60s">
           <tt:p>להגדלת חלון באמצעות המקלדת, החזיקו את מקש ה־<key href="help:gnome-help/keyboard-key-super">Super</key> והקישו <key>↑</key>.</tt:p>
         </tt:div>
         <tt:div begin="61s" end="66s">
           <tt:p>לשחזור גודל החלון, החזיקו את מקש ה־<key href="help:gnome-help/keyboard-key-super">Super</key> והקישו <key>↓</key>.</tt:p>
         </tt:div>
         <tt:div begin="66s" end="73s">
           <tt:p>להגדלת חלון לאורך צדו הימני של המסך, החזיקו את מקש ה־<key href="help:gnome-help/keyboard-key-super">Super</key> והקישו <key>→</key>.</tt:p>
         </tt:div>
         <tt:div begin="76s" end="82s">
           <tt:p>להגדלת חלון לאורך צדו השמאלי של המסך, החזיקו את מקש ה־<key href="help:gnome-help/keyboard-key-super">Super</key> והקישו <key>←</key>.</tt:p>
         </tt:div>
         <tt:div begin="83s" end="89s">
           <tt:p>למעבר לשולחן עבודה הנמצא מתחת לשולחן העבודה הנוכחי, לחצו <keyseq><key href="help:gnome-help/keyboard-key-super">Super </key><key>Page Down</key></keyseq>.</tt:p>
         </tt:div>
         <tt:div begin="90s" end="97s">
           <tt:p>למעבר לשולחן עבודה הנמצא מעל לשולחן העבודה הנוכחי, לחצו <keyseq><key href="help:gnome-help/keyboard-key-super">Super </key><key>Page Up</key></keyseq>.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>
  
  <section id="use-workspaces-and-windows-maximize">
    <title>הגדלת חלונות ושחזורם</title>
    <p/>
    
    <steps>
      <item><p>To maximize a window so that it takes up all of the space on
       your desktop, grab the window’s titlebar and drag it to the top of the
       screen.</p></item>
      <item><p>כאשר המסך יודגש, שחררו את החלון להגדלתו.</p></item>
      <item><p>To restore a window to its unmaximized size, grab the window’s
       titlebar and drag it away from the edges of the screen.</p></item>
    </steps>
    
  </section>

  <section id="use-workspaces-and-windows-tile">
    <title>ריצוף חלונות</title>
    <p/>
    
    <steps>
      <item><p>To maximize a window along a side of the screen, grab the window’s
       titlebar and drag it to the left or right side of the screen.</p></item>
      <item><p>כאשר מחצית המסך תודגש, שחררו את החלון להגדלתו לאורך הצד שנבחר.</p></item>
      <item><p>להגדלת שני חלונות זה לצד זה, החזיקו בשורת הכותרת של החלון השני וגררו אותה לצדו הנגדי של המסך.</p></item>
       <item><p>כאשר מחצית המסך תודגש, שחררו את החלון להגדלתו לאורך הצד הנגדי.</p></item>
    </steps>
    
  </section>
  
  <section id="use-workspaces-and-windows-maximize-keyboard">
    <title>הגדלת חלונות ושחזורם באמצעות המקלדת</title>
    
    <steps>
      <item><p>להגדלת חלון באמצעות המקלדת, החזיקו את מקש ה־<key href="help:gnome-help/keyboard-key-super">Super</key> והקישו <key>↑</key>.</p></item>
      <item><p>לשחזור גודל חלון באמצעות המקלדת, החזיקו את מקש ה־<key href="help:gnome-help/keyboard-key-super">Super</key> והקישו <key>↓</key>.</p></item>
    </steps>
    
  </section>
  
  <section id="use-workspaces-and-windows-tile-keyboard">
    <title>ריצוף חלונות באמצעות המקלדת</title>
    
    <steps>
      <item><p>להגדלת חלון לאורך צדו הימני של המסך, החזיקו את מקש ה־<key href="help:gnome-help/keyboard-key-super">Super</key> והקישו <key>→</key>.</p></item>
      <item><p>להגדלת חלון לאורך צדו השמאלי של המסך, החזיקו את מקש ה־<key href="help:gnome-help/keyboard-key-super">Super</key> והקישו <key>←</key>.</p></item>
    </steps>
    
  </section>
  
  <section id="use-workspaces-and-windows-workspaces-keyboard">
    <title>מעבר בין שולחנות עבודה באמצעות המקלדת</title>
    
    <steps>
    
    <item><p>למעבר לשולחן עבודה הנמצא מתחת לשולחן העבודה הנוכחי, לחצו <keyseq><key href="help:gnome-help/keyboard-key-super">Super</key><key>Page Down</key></keyseq>.</p></item>
    <item><p>למעבר לשולחן עבודה הנמצא מעל לשולחן העבודה הנוכחי, לחצו <keyseq><key href="help:gnome-help/keyboard-key-super">Super</key><key>Page Up</key></keyseq>.</p></item>

    </steps>
    
  </section>

</page>
