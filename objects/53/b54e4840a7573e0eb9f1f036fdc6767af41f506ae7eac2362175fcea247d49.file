<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-autorun" xml:lang="ru">

  <info>
    <link type="guide" xref="media#photos"/>
    <link type="guide" xref="media#videos"/>
    <link type="guide" xref="media#music"/>
    <link type="guide" xref="files#removable"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-10-04" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Шобха Тьяги (Shobha Tyagi)</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Дэвид Кинг (David King)</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Автоматический запуск приложений для CD и DVD, камер, аудиопроигрывателей и других устройств и носителей информации.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <!-- TODO: fix bad UI strings, then update help -->
  <title>Открытие приложений для устройств и дисков</title>

  <p>Вы можете настроить автоматический запуск приложения при подключении устройства, загрузки диска или медиа-носителя. Например, можно сделать так, чтобы при подключении цифровой камеры автоматически запускалось приложение для управления фотоснимками. Можно также отключить автоматическое действие.</p>

  <p>Чтобы решить, какое приложение должно запускаться при подключении того или иного устройства:</p>

<steps>
  <item>
    <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
    start typing <gui>Details</gui>.</p>
  </item>
  <item>
    <p>Нажмите <gui>Подробности</gui>, чтобы открыть этот раздел настроек.</p>
  </item>
  <item>
    <p>Нажмите <gui>Съёмный носитель</gui>.</p>
  </item>
  <item>
    <p>Найдите нужное устройство или тип носителя информации, затем выберите приложение или действие для этого типа носителя. Смотрите ниже описание различных типов устройств и носителей.</p>
    <p>Instead of starting an application, you can also set it so that the
    device will be shown in the file manager, with the <gui>Open folder</gui>
    option. When that happens, you will be asked what to do, or nothing will
    happen automatically.</p>
  </item>
  <item>
    <p>If you do not see the device or media type that you want to change in
    the list (such as Blu-ray discs or E-book readers), click <gui>Other
    Media…</gui> to see a more detailed list of devices. Select the type of
    device or media from the <gui>Type</gui> drop-down and the application or
    action from the <gui>Action</gui> drop-down.</p>
  </item>
</steps>

  <note style="tip">
    <p>If you do not want any applications to be opened automatically, whatever
    you plug in, select <gui>Never prompt or start programs on media
    insertion</gui> at the bottom of the <gui>Details</gui> window.</p>
  </note>

<section id="files-types-of-devices">
  <title>Типы устройств и носителей</title>
<terms>
  <item>
    <title>Звуковые диски</title>
    <p>Choose your favorite music application or CD audio extractor to handle
    audio CDs. If you use audio DVDs (DVD-A), select how to open them under
    <gui>Other Media…</gui>. If you open an audio disc with the file manager,
    the tracks will appear as WAV files that you can play in any audio player
    application.</p>
  </item>
  <item>
    <title>Видеодиски</title>
    <p>Choose your favorite video application to handle video DVDs. Use the
    <gui>Other Media…</gui> button to set an application for Blu-ray, HD DVD,
    video CD (VCD), and super video CD (SVCD). If DVDs or other video discs
    do not work correctly when you insert them, see <link xref="video-dvd"/>.
    </p>
  </item>
  <item>
    <title>Чистые диски</title>
    <p>Use the <gui>Other Media…</gui> button to select a disc-writing
    application for blank CDs, blank DVDs, blank Blu-ray discs, and blank HD
    DVDs.</p>
  </item>
  <item>
    <title>Камеры и фотографии</title>
    <p>Чтобы установить приложение для автоматического запуска при подключении цифровой, используйте выпадающий список <gui>Фотоснимки</gui>. Это действие также распространяется на подключение различных медиа-носителей (карты формата CF, SD, MMC или MS). Вы также можете просто просмотреть фотоснимки с помощью менеджера файлов.</p>
    <p>Under <gui>Other Media…</gui>, you can select an application to open
    Kodak picture CDs, such as those you might have made in a store. These are
    regular data CDs with JPEG images in a folder called
    <file>Pictures</file>.</p>
  </item>
  <item>
    <title>Музыкальные проигрыватели</title>
    <p>Выберите приложение для управления музыкальной коллекцией на вашем портативном музыкальном плеере или управляйте файлами с помощью менеджера файлов.</p>
    </item>
    <item>
      <title>Устройства для чтения электронных книг</title>
      <p>Use the <gui>Other Media…</gui> button to choose an application to
      manage the books on your e-book reader, or manage the files yourself
      using the file manager.</p>
    </item>
    <item>
      <title>Программы</title>
      <p>На некоторых дисках и съёмных устройствах находится программное обеспечение, которое должно автоматически запускаться при подключении устройства. Используйте параметр <gui>Приложение</gui> для управления обработкой такого программного обеспечения. Перед запуском программного обеспечения всегда будет запрашиваться подтверждение.</p>
      <note style="warning">
        <p>Never run software from media you don’t trust.</p>
      </note>
   </item>
</terms>

</section>

</page>
