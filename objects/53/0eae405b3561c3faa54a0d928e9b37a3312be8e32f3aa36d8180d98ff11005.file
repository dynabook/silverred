<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-purge" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.10" date="2013-09-29" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shaun McCance</name>
      <email>mdhillca@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Defina o quão frequente seus arquivos da lixeira e temporários serão excluídos de seu computador.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

  <title>Excluindo permanentemente arquivos da lixeira e temporários</title>

  <p>Esvaziar sua lixeira e arquivos temporários remove arquivos indesejados e desnecessários de seu computador, e também libera mais espaço em seu disco rígido. Você pode manualmente esvaziar sua lixeira e excluir seus arquivos temporários, mas você também pode definir seu computador para fazer isso automaticamente para você.</p>

  <p>Arquivos temporários são arquivos criados automaticamente por aplicativos em plano de fundo. Eles podem melhorar o desempenho fornecendo uma cópia dos dados que foram baixados ou computados.</p>

  <steps>
    <title>Automaticamente esvaziar sua impressora e excluir os arquivos temporários</title>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Privacidade</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Privacidade</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Selecione <gui>Excluir permanentemente arquivos da lixeira e temporários</gui>.</p>
    </item>
    <item>
      <p>Defina um ou ambos alternadores <gui>Esvaziar a lixeira automaticamente</gui> ou <gui>Excluir permanentemente arquivos temporários de forma automática</gui> para ligado.</p>
    </item>
    <item>
      <p>Defina o quão frequente você gostaria que sua <em>Lixeira</em> e <em>Arquivos temporários</em> sejam excluídos permanentemente alterando o valor de <gui>Excluir permanentemente após</gui>.</p>
    </item>
    <item>
      <p>Use os botões <gui>Esvaziar lixeira</gui> ou <gui>Excluir permanentemente arquivos temporários</gui> para realizar estas ações imediatamente.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Você pode excluir arquivos imediatamente e permanentemente usando a lixeira. Veja <link xref="files-delete#permanent"/> para informações.</p>
  </note>

</page>
