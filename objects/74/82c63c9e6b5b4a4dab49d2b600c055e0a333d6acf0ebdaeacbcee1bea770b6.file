<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-setup-default-printer" xml:lang="gl">

  <info>
    <link type="guide" xref="printing#setup"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.7.1" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Seleccionar a impresora que usa máis frecuentemente.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2020</mal:years>
    </mal:credit>
  </info>

  <title>Estabelecer a impresora predeterminada</title>

  <p>Se ten máis dunha impresora dispoñíbel, pode seleccionar a que será a súa impresora predeterminada. É posíbel que queira elixir a impresora que usa con máis frecuencia.</p>

  <note>
    <p>Necesita <link xref="user-admin-explain">privilexios de administrador</link> do sistema para estabelecer a impresora predeterminada.</p>
  </note>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Printers</gui>.</p>
    </item>
    <item>
      <p>Prema <gui>Impresoras</gui>.</p>
    </item>
    <item>
      <p>Seleccione a impresora predeterminada da lista de impresoras dispoñíbeis.</p>
    </item>
    <item>
      <p>Prema <gui style="button">Desbloquear</gui> na esquina superior dereita e escriba o seu contrasinal cando se lle pregunte.</p>
    </item>
    <item>
      <p>Seleccione a caixa de verificación <gui>Impresora por omisión</gui>.</p>
    </item>
  </steps>

  <p>When you print in an application, the default printer is automatically
  used, unless you choose a different printer.</p>

</page>
