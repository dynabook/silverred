<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-calibrate-printer" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-calibrate-scanner"/>
    <link type="seealso" xref="color-calibrate-screen"/>
    <link type="seealso" xref="color-calibrate-camera"/>
    <desc>校准打印机对打印颜色的准确性非常重要。</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>如何校准我的打印机？</title>

  <p>获取打印设备配置文件的方法有两种：</p>

  <list>
    <item><p>使用光谱仪设备，如 Pantone ColorMunki</p></item>
    <item><p>从颜料厂商下载打印参考文件</p></item>
  </list>

  <p>如果您只有一或两个不同的纸张类型，让颜料厂商生成打印机配置文件一般是最便宜的选择。通过从公司网站下载参考图，您之后可以用内衬信封将打印件发送回公司，公司将扫描这页纸，生成配置文件并通过电子邮件给您发送准确的 ICC 配置文件。</p>
  <p>只有在您要获取大量油墨集或纸张类型的配置文件时，使用昂贵设备(如 ColorMunki)才比较划算。</p>

  <note style="tip">
    <p>如果您改变了您的油墨供应商，请务必重新校准打印机！</p>
  </note>

</page>
