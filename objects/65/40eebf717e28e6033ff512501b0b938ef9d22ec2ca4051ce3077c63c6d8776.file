<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-templates" xml:lang="fi">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Anita Reitere</name>
      <email>nitalynx@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Luo nopeasti uusia tiedostoja omavalintaisista asiakirjamalleista.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

  <title>Asiakirjamalleja yleisille tiedostotyypeille</title>

  <p>Jos luot usein samaan sisältöön perustuvia asiakirjoja, voit hyötyä asiakirjamallien käytöstä. Asiakirjamalli voidaan luoda kaikenlaisista asiakirjoista käyttäen asettelua ja sisältöä, jota haluat käyttää uudelleen. Esimerkiksi voit luoda asiakirjamallin, josta löytyy usein käyttämäsi ylätunniste.</p>

  <steps>
    <title>Luo uusi asiakirjamalli</title>
    <item>
      <p>Luo asiakirja, jota tulet käyttämään asiakirjamallina. Voit esimerkiksi luoda asiakirjapohjan tekstinkäsittelyohjelmaan.</p>
    </item>
    <item>
      <p>Tallenna asiakirjamalli kotikansion alikansioon <file>Mallit</file>. Jos <file>Mallit</file>-kansiota ei ole olemassa, luo se.</p>
    </item>
  </steps>

  <steps>
    <title>Käytä asiakirjamallia asiakirjan luontiin</title>
    <item>
      <p>Avaa kansio, jonne haluat tallentaa uuden asiakirjan.</p>
    </item>
    <item>
      <p>Right-click anywhere in the empty space in the folder, then choose
      <gui style="menuitem">New Document</gui>. The names of available
      templates will be listed in the submenu.</p>
    </item>
    <item>
      <p>Valitse haluamasi malli luettelosta.</p>
    </item>
    <item>
      <p>Double-click the file to open it and start editing. You may wish to
      <link xref="files-rename">rename the file</link> when you are
      finished.</p>
    </item>
  </steps>

</page>
