<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-font-size" xml:lang="ro">

  <info>
    <link type="guide" xref="a11y#vision" group="lowvision"/>

    <revision pkgversion="3.7.1" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.26" date="2017-08-04" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Utilizează fonturi mai mari pentru a face textul mai lizibil.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Șerbănescu</mal:name>
      <mal:email>daniel [at] serbanescu [dot] dk</mal:email>
      <mal:years>2016, 2019</mal:years>
    </mal:credit>
  </info>

  <title>Schimbarea dimensiunii textului de pe ecran</title>

  <p>Dacă aveți dificultăți în a citi textul de pe ecran, puteți schimba dimensiunea fontului.</p>

  <steps>
    <item>
      <p>Deschideți vederea de ansamblu <gui xref="shell-introduction#activities">Activități</gui> și tastați <gui>Acces universal</gui>.</p>
    </item>
    <item>
      <p>Apăsați pe <gui>Acces universal</gui> pentru a deschide panoul.</p>
    </item>
    <item>
      <p>In the <gui>Seeing</gui> section, switch the <gui>Large Text</gui>
      switch to on.</p>
    </item>
  </steps>

  <p>În mod alternativ, puteți schimba rapid dimensiunea textului apăsând pe <link xref="a11y-icon">iconița de accesibilitate</link> de pe bara de sus și selectați <gui>Text mărit</gui>.</p>

  <note style="tip">
    <p>În multe aplicații, puteți mări dimensiunea textului oricând apăsând <keyseq><key>Ctrl</key><key>+</key></keyseq>. Pentru a reduce dimensiunea textului, apăsați <keyseq><key>Ctrl</key><key>-</key></keyseq>.</p>
  </note>

  <p><gui>Text mărit</gui> va scala textul cu 1,2 din dimensiunea inițială. Puteți folosi <app>Intrument de ajustare</app> pentru a face textul mai mare sau mai mic.</p>

</page>
