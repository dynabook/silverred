<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-display" xml:lang="sr">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-display"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Шон Мек Кенс</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Мајкл Хил</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Дејвид Кинг</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Управљајте натписима иконица коришћеним у управнику датотека.</desc>

  </info>

<title>Поставке приказа управника датотека</title>

<p>You can control how the file manager displays captions under icons. Click
the menu button in the top-right corner of the window and select
<gui>Preferences</gui>, then select the <gui>Views</gui> tab.</p>

<section id="icon-captions">
  <title>Натписи иконице</title>
  <!-- TODO: update screenshot for 3.18 and above. -->
  <media type="image" src="figures/nautilus-icons.png" width="250" height="110" style="floatend floatright">
    <p>Иконице управника датотека са натписима</p>
  </media>
  <p>Када користите преглед иконицама, можете да изаберете приказ додатних података о датотекама и фасциклама у натпису испод сваке иконице. Ово је корисно, на пример, ако често имате потребу да видите ко је власник датотеке или када је последњи пут измењена.</p>
  <p>Можете да увећате приказ фасцикле тако што ћете кликнути на дугме опција прегледа на траци алата и клизачем изабрати ниво увеличања. Како будете увећавали, управник датотека ће приказивати више и више података у натпису. Можете да изаберете до три ствари за приказивање у натпису. Прва ће бити приказана на већини нивоа увећавања. Последња ће бити приказана при врло великим величинама.</p>
  <p>Подаци које можете да прикажете у натпису иконице су исти као и колоне које можете да користите у прегледу списком. Погледајте <link xref="nautilus-list"/> за више података.</p>
</section>

<section id="list-view">

  <title>Преглед списком</title>

  <p>When viewing files as a list, you can <gui>Allow folders to be
  expanded</gui>. This shows expanders on each directory in the file list, so
  that the contents of several folders can be shown at once. This is useful if
  the folder structure is relevant, such as if your music files are organized
  with a folder per artist, and a subfolder per album.</p>

</section>

</page>
