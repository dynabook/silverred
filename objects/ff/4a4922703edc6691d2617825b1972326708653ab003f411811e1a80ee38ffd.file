<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-wireless-disconnecting" xml:lang="id">

  <info>
    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-problem"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <desc>You might have low signal, or the network might not be letting you
    connect properly.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

<title>Mengapa jaringan nirkabel saya terputus terus?</title>

<p>You may find that you have been disconnected from a wireless network even
 though you wanted to stay connected. Your computer will normally try to
 reconnect to the network as soon as this happens (the network icon on the top
 bar will display three dots if it is trying to reconnect), but it can be
 annoying, especially if you were using the internet at the time.</p>

<section id="signal">
 <title>Sinyal nirkabel lemah</title>

 <p>Alasan umum untuk terputus dari jaringan nirkabel adalah bahwa Anda memiliki sinyal lemah. Jaringan nirkabel memiliki jangkauan yang terbatas, jadi jika Anda terlalu jauh dari base station nirkabel Anda mungkin tidak bisa mendapatkan sinyal yang cukup kuat untuk mempertahankan koneksi. Dinding dan benda-benda lain antara Anda dan base station juga dapat melemahkan sinyal.</p>

 <p>The network icon on the top bar displays how strong your wireless signal is.
 If the signal looks low, try moving closer to the wireless base station.</p>

</section>

<section id="network">
 <title>Network connection not being established properly</title>

 <p>Sometimes, when you connect to a wireless network, it may appear that you
 have successfully connected at first, but then you will be disconnected soon
 after. This normally happens because your computer was only partially
 successful in connecting to the network — it managed to establish a connection,
 but was unable to finalize the connection for some reason and so was
 disconnected.</p>

 <p>Alasan yang mungkin adalah Anda memasukkan frasa sandi nirkabel yang salah, atau bahwa komputer Anda tak diijinkan pada jaringan (karena jaringan memerlukan suatu nama pengguna untuk log masuk, misalnya).</p>

</section>

<section id="hardware">
 <title>Perangkat keras/penggerak nirkabel yang tak reliabel</title>

 <p>Some wireless network hardware can be a little unreliable. Wireless
 networks are complicated, so wireless cards and base stations occasionally run
 into minor problems and may drop connections. This is annoying, but it happens
 quite regularly with many devices. If you are disconnected from wireless
 connections from time to time, this may be the only reason. If it happens very
 regularly, you may want to consider getting some different hardware.</p>

</section>

<section id="busy">
 <title>Jaringan nirkabel sibuk</title>

 <p>Wireless networks in busy places (in universities and coffee shops, for
 example) often have many computers trying to connect to them at once. Sometimes
 these networks get too busy and may not be able to handle all of the computers
 that are trying to connect, so some of them get disconnected.</p>

</section>

</page>
