<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="video-dvd" xml:lang="nl">

  <info>
    <link type="guide" xref="media#videos"/>
    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.12.1" date="2014-03-30" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>

    <credit type="author">
      <name>Gnome-documentatieproject</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Het kan zijn dat u niet de juiste codecs geïnstalleerd heeft, of de dvd kan de verkeerde regiocode hebben.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Waarom kunnen dvd's niet worden afgespeeld?</title>

  <p>Wanneer een dvd niet kan worden afgespeeld nadat u deze in uw computer heeft gestopt, dan heeft u mogelijk niet de juiste dvd-<em>codecs</em> geïnstalleerd, of de dvd kan uit een andere <em>regio</em> komen.</p>

<section id="codecs">
  <title>De juiste codecs voor het afspelen van een dvd installeren</title>

  <p>Om dvd's af te spelen dient u de juiste <em>codecs</em> geïnstalleerd te hebben. Een codec is software waarmee toepassingen een bepaald video- of audioformaat kunnen lezen. Als uw mediaspeler niet de juiste codecs vindt, zal deze u mogelijk aanbieden de codecs voor u te installeren. Zo niet, dan zult u de codecs handmatig moeten installeren — vraag hulp over hoe u dit doet, bijvoorbeeld bij een ondersteunend forum van uw Linux-distributie.</p>

  <p>DVDs are also <em>copy-protected</em> using a system called CSS. This
  prevents you from copying DVDs, but it also prevents you from playing them
  unless you have extra software to handle the copy protection. This software
  is available from a number of Linux distributions, but cannot be legally used
  in all countries. You can buy a commercial DVD decoder that can handle copy
  protection from
  <link href="https://fluendo.com/en/products/multimedia/oneplay-dvd-player/">Fluendo</link>.
  It works with Linux and should be legal to use in all countries.</p>

  </section>

<section id="region">
  <title>Het dvd-gebied controleren</title>

  <p>Dvd's hebben een <em>regiocode</em> die aangeeft in welk deel van de wereld u de dvd mag afspelen. Dvd-spelers kunnen alleen dvd's afspelen uit dezelfde regio als de dvd-speler. Als u bijvoorbeeld een dvd-speler voor regio 1 heeft, dan kunt u alleen dvd's uit Noord Amerika hierop afspelen.</p>

  <p>Het is vaak mogelijk de regio die door uw dvd-speler gebruikt wordt te veranderen, maar u kunt dit slechts een aantal malen doen voordat de speler permanent vastgezet wordt op één bepaalde regio. Gebruik <link href="http://linvdr.org/projects/regionset/">regionset</link> om de dvd-regio van uw dvd-speler te wijzigen.</p>

  <p>U kunt <link href="https://en.wikipedia.org/wiki/DVD_region_code">meer informatie over dvd-regiocodes op Wikipedia</link> vinden.</p>

</section>

</page>
