<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mouse-middleclick" xml:lang="cs">

  <info>
    <link type="guide" xref="tips"/>
    <link type="guide" xref="mouse#tips"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.4" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak používat prostřední tlačítko k otevření aplikací, otevření karet a dalším věcem.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

<title>Kliknutí prostředním tlačítkem</title>

<p>Řada myší a touchpadů má prostřední tlačítko. Na myši s kolečkem můžete obvykle zmáčknout přímo rolovací kolečko jako prostřední tlačítko. V případě, že prostřední není přítomno, můžete zmáčknout naráz levé a pravé tlačítko, a bude to považováno za prostřední tlačítko.</p>

<p>Na vícedotykových touchpadech můžete dosáhnout kliknutí prostředním tlačítkem pomocí klepnutí třemi prsty naráz. Aby to fungovalo, musíte mít v nastaveních touchpadu <link xref="mouse-touchpad-click">povolené kliknutí klepnutím</link>.</p>

<p>Řada aplikací používá kliknutí prostředním tlačítkem pro pokročilé klikací zkratky.</p>

<list>
  <item><p>V aplikacích s posuvníky kliknutí levým tlačítkem na volné místo na liště posuvníku provede posun přímo na dané místo. Kliknutí prostředním tlačítkem provede posun o jednu stránku v daném směru.</p></item>

  <item><p>V přehledu <gui>Činností</gui> můžete rychle otevřít nové okno pro aplikaci pomocí kliknutí prostředním tlačítkem na ikonu aplikaci buď nalevo v oblíbených nebo v přehledu aplikací. Přehled aplikací se zobrazí pomocí tlačítky s mřížkou, které najdete dole v oblíbených.</p></item>

  <item><p>Většina webových prohlížečů umožňuje rychle otevřít odkazy v kartách pomocí prostředního tlačítka myši. Stačí na libovolný odkaz kliknout prostředním tlačítkem a ten se otevře v nové kartě.</p></item>

  <item><p>Ve správci souborů hraje prostřední kliknutí dvě role. Když kliknete prostředním tlačítkem na složku, otevře se v nové kartě. Tím se napodobuje chování oblíbených webových prohlížečů. Když kliknete prostředním tlačítkem na soubor, otevře se soubor stejným způsobem, jako byste na něj klikli dvojitě.</p></item>
</list>

<p>Některé specializované aplikace umí využít prostřední tlačítko myši pro další funkce. Hledejte v nápovědě k takovéto aplikaci výraz <em>prostřední kliknutí</em> nebo <em>kliknutí prostředním tlačítkem</em>.</p>

</page>
