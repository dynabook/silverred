<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-launch-applications" xml:lang="zh-TW">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>

    <link type="guide" xref="getting-started" group="videos"/>
    <title role="trail" type="link">啟動應用程式</title>
    <link type="seealso" xref="shell-apps-open"/>
    <title role="seealso" type="link">啟動應用程式的教學</title>
    <link type="next" xref="gs-switch-tasks"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Cheng-Chia Tseng</mal:name>
      <mal:email>pswo10680@gmail.com</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  </info>

  <title>啟動應用程式</title>

  <ui:overlay width="812" height="452">
   <media type="video" its:translate="no" src="figures/gnome-launching-applications.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-launching-apps.svg"/>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>啟動應用程式</tt:p>
         </tt:div>
         <tt:div begin="5s" end="7.5s">
           <tt:p>將您的滑鼠指標移向螢幕左上角的 <gui>概覽</gui>。</tt:p>
           </tt:div>
         <tt:div begin="7.5s" end="9.5s">
           <tt:p>點按 <gui>顯示應用程式</gui> 圖示。</tt:p>
         </tt:div>
         <tt:div begin="9.5s" end="11s">
           <tt:p>點按您想要執行的應用程式，舉例來說：「求助」。</tt:p>
         </tt:div>
         <tt:div begin="12s" end="21s">
           <tt:p>您也可以改用鍵盤開啟 <gui>活動概覽</gui>，按下 <key href="help:gnome-help/keyboard-key-super">Super</key> 鍵即可。</tt:p>
         </tt:div>
         <tt:div begin="22s" end="29s">
           <tt:p>開始輸入您想要啟動的應用程式名稱。</tt:p>
         </tt:div>
         <tt:div begin="30s" end="33s">
           <tt:p>按下 <key>Enter</key> 鍵啟動應用程式。</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
   </media>
  </ui:overlay>

  <section id="launch-apps-mouse">
    <title>使用滑鼠啟動應用程式</title>
 
  <steps>
    <item><p>將您的滑鼠指標移到螢幕左上角的 <gui>概覽</gui> 角落，如此可顯示 <gui>活動概覽</gui> 畫面。</p></item>
    <item><p>點按螢幕左手邊長欄底部的 <gui>顯示應用程式</gui> 圖示。</p></item>
    <item><p>應用程式會以列表顯示。點按您想要執行的程式，舉個例：求助。</p></item>
  </steps>

  </section>

  <section id="launch-app-keyboard">
    <title>使用鍵盤啟動應用程式</title>

  <steps>
    <item><p>按下 <key href="help:gnome-help/keyboard-key-super">Super</key> 鍵開啟 <gui>活動概覽</gui>。</p></item>
    <item><p>開始輸入您想要啟動的程式之名稱。應用程式的搜尋結果會即時反應。</p></item>
    <item><p>一旦出現該應用程式的圖示且選取後，按下 <key>Enter</key> 鍵啟動應用程式。</p></item>
  </steps>

  </section>

</page>
