;; ml-inscript2.mim -- Malayalam input method for enhanced inscript layout
;; Copyright (c) 2011-2016 Red Hat, Inc. All Rights Reserved.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation; either version 2.1 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public
;; License along with the m17n library; if not, write to the Free
;; Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.
;;
;; Author: Parag Nemade <pnemade@redhat.com>

(input-method ml inscript2)

(description "Malayalam input method for enhanced inscript layout.

Reference URL - http://pune.cdac.in/html/gist/down/inscript_d.asp

Use AltGr (Alt_R key) to type the following characters:

Character    Key
----------------------------
ZWJ            AltGr + 1
ZWNJ         AltGr + 2
₹              AltGr + 4

Following are the conjuncts formed in our language, shown along with the keys to reproduce them:

Case 1:
CHILLU aksharam:
(i) ന്‍   =   ന + ് + zero width joiner(zwj)
ie,    key 'V' + key 'd' + key <AltGr+1>    

(ii) ല്‍   =   ല + ് + zero width joiner(zwj)
ie,    key 'N' + key 'd' + key <AltGr+1>    

(iii) ര്‍   =   ര + ് + zero width joiner(zwj)
ie,    key 'J' + key 'd' + key <AltGr+1>   

(iv) ണ്‍   =   ണ + ് + zero width joiner(zwj)
ie,    shift key 'C' + key 'd' + key <AltGr+1> 

(v) ള്‍   =   ള + ് + zero width joiner(zwj)
ie,    shift key 'N' + key 'd' + key <AltGr+1>
         
Case 2:
(i) ങ്ങ   =   ങ +  ്  +  ക
ie, shift key 'U' + key 'd' + key 'K'               

(ii) ന്ത   =   ന + ് + ത
ie, key 'V'  + key 'd' + key 'L'

(iii) ഞ്ച   =    ഞ + ് + ച
ie, shift key '}' + key 'd' + key ';'

(iv) ണ്ട   =   ണ + ് + ട
ie, shift key 'C' + key 'd' + key '\"'

(v) മ്പ   =   മ + ് + പ
ie, key 'V' + key 'd' + key 'H'

(vi) ക്ഷ   =    ക + ് + ഷ
ie, key 'C' + key 'd' + shift key '<'

Case 3:
Koottaksharangal:

(i) ക്ക   =   ക + ് + ക
ie, key 'K' + key 'd' + key 'K'

(ii) ങ്ങ   =   ങ + ് + ങ
ie, shift key 'U' + key 'd' + shift key 'U'

(iii) ച്ച   =   ച + ് + ച
ie, key ';' + key 'd' + key ';'

(iv) ഞ്ഞ   =   ഞ + ് + ഞ
ie, shift key '}' + key 'd' + shift key '}'

(v) ട്ട   =   ട + ് + ട
ie, key '\"' + key 'd' + key '\"' 

(vi) ണ്ണ   =   ണ + ് + ണ 
ie, shift key 'C' + key 'd' + shift key 'C' 

(vii) ത്ത   =   ത + ് + ത
ie, key 'L' + key 'd' +  key 'L' 

(viii) ന്ന   =   ന + ് + ന
ie, key 'V' + key 'd' +  key 'V' 

(ix) മ്മ   =   മ + ് + മ
ie, key 'C' + key 'd' + key 'C'

(x) ല്ല   =   ല + ് + ല
ie, key 'N' + key 'd' + key 'N' 

(xi) വ്വ   =   വ + ് + വ
ie, key 'B' + key 'd' + key 'B'

(xii) യ്യ   =   യ + ് + യ
ie, key '?' + key 'd' + key '?'

(xiii) ശ്ശ   =   ശ + ് + ശ 
ie, shift key 'M' + key 'd' + shift key 'M' 

(xiv) സ്സ   =   സ  + ് + സ
ie, key 'M' + key 'd' + key 'M' 

(xv) ള്ള   =   ള + ് + ള
ie, shift key 'N' + key 'd' + shift key 'N'

(xvi) റ്റ   =   റ + ് + റ
ie, shift key 'J' + key 'd' + shift key 'J'

Case 4:
(Following conjuncts are explained with the help of consonant 'ക')

(1) Conjuncts formed with ര (ra):
 ക്ര   =   ക + ് + ര
ie, key 'K' + key 'd' + key 'J'

(2) Conjuncts formed with യ (ya):
 ക്യ   =   ക + ് + യ
ie, key 'K' + key 'd' + key '?'

(3) Conjuncts formed with വ (va):
 ക്വ   =   ക + ് + വ
ie, key 'K' + key 'd' + key 'B'

Special case:

ന്റ   =   ന + ് + റ
ie, key 'V' + key 'd' + shift key 'J'

Author: Ani Peter <apeter@redhat.com>
")

(title "ക")

(map
 (trans
  ((KP_1) "൧")
  ((KP_2) "൨")
  ((KP_3) "൩")
  ((KP_4) "൪")
  ((KP_5) "൫")
  ((KP_6) "൬")
  ((KP_7) "൭")
  ((KP_8) "൮")
  ((KP_9) "൯")
  ((KP_0) "൦")
  ((KP_Decimal) ".")
  ((KP_Divide) "/")
  ((KP_Multiply) "*")
  ((KP_Add) "+")
  ((KP_Subtract) "-") 
 
  ("1" "൧")
  ((G-1) "‍")
  ((G-!) "൰") 
  ("2" "൨")
  ((G-2) "‌")
  ((G-@) "൱")
  ("3" "൩")
  ("#" "്ര")
  ((G-#) "൲")
  ("4" "൪")
  ((G-$) "൳")
  ((G-4) "₹")
  ("5" "൫")
  ((G-%) "൴")
  ("6" "൬")
  ((G-^) "൵")
  ("7" "൭")
  ("&" "ക്ഷ")
  ("8" "൮")
  ("*" "ൾ")
  ("(" "(")
  ("9" "൯")
  (")" ")")
  ("0" "൦")
  ("_" "ഃ")
  ("-" "-")
  ("+" "ഋ")
  ((G-+) "ൠ")
  ("=" "ൃ")
  ((G-=) "ൄ")
  ("Q" "ഔ")
  ("q" "ൗ")
  ((G-q) "ൌ")
  ("W" "ഐ")
  ("w" "ൈ")
  ("E" "ആ")
  ("e" "ാ")
  ("R" "ഈ")
  ((G-R) "ൡ")
  ("r" "ീ")
  ((G-r) "ൣ")
  ("T" "ഊ")
  ("t" "ൂ")
  ("Y" "ഭ")
  ("y" "ബ")
  ("U" "ങ")
  ("u" "ഹ")
  ("I" "ഘ")
  ("i" "ഗ")
  ("O" "ധ")
  ("o" "ദ")
  ("P" "ഝ")
  ("p" "ജ")
  ("{" "ഢ")
  ("[" "ഡ")
  ("}" "ഞ")
  ("\\" "ർ")
  ("A" "ഓ")
  ("a" "ോ")
  ("S" "ഏ")
  ("s" "േ")
  ("D" "അ")
  ("d" "്")
  ("F" "ഇ")
  ((G-F) "ഌ")
  ("f" "ി")
  ((G-f) "ൢ")
  ("G" "ഉ")
  ("g" "ു")
  ("H" "ഫ")
  ("h" "പ")
  ("J" "റ")
  ("j" "ര")
  ((G-j) "ർ")
  ("K" "ഖ")
  ("k" "ക")
  ((G-k) "ൿ")
  ("L" "ഥ")
  ("l" "ത")
  (":" "ഛ")
  (";" "ച")
  ("\"" "ഠ")
  ("'" "ട")
  ("~" "ഒ")
  ("`" "ൊ")
  ("Z" "എ")
  ("z" "െ")
  ("X" "ൺ")
  ("x" "ം")
  ("C" "ണ")
  ("c" "മ")
  ((G-c) "ൺ")
  ("V" "ൻ")
  ("v" "ന")
  ((G-v) "൹") 
  ("B" "ഴ")
  ("b" "വ")
  ("N" "ള")
  ((G-N) "ൾ")
  ("n" "ല")
  ((G-n) "ൽ")
  ("M" "ശ")
  ("m" "സ")
  ("<" "ഷ")
  ("," ",")
  (">" "ൽ")
  ((G->) "ഽ")
  ("." ".")
  ((G-.) "॥")
  ("/" "യ")
  ))

(state
 (init
  (trans)))


