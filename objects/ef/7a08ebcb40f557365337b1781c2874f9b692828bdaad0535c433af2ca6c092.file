<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="login-logo" xml:lang="sv">

  <info>
    <link type="guide" xref="login#appearance"/>
    <link type="seealso" xref="login-banner"/>
    <!--<link type="seealso" xref="gdm-restart"/>-->
    <revision pkgversion="3.11" date="2014-01-29" status="draft"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="author copyright editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2013</years>
    </credit>

    <desc>Redigera en <sys its:translate="no">GDM</sys>-profil i <sys its:translate="no">dconf</sys> för att visa en bild på inloggningsskärmen.</desc>
   
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Lägg till en välkomstlogo till inloggningsskärmen</title>

  <p>Välkomstlogon på inloggningsskärmen styrs av GSettings-nyckeln <sys>org.gnome.login-screen.logo</sys>. Eftersom <sys>GDM</sys> använder sin egen <sys>dconf</sys>-profil kan du lägga till en välkomstlogo genom att ändra inställningarna i den profilen.</p>
  
  <p>Då du väljer en lämplig bild för logon till din inloggningsskärm kan du betänka följande krav som finns på bilden:</p>
  
  <list>
  <item><p>Alla stora format stöds: ANI, BPM, GIF, ICNS, ICO, JPEG, JPEG 2000, PCX, PNM, PBM, PGM, PPM, GTIFF, RAS, TGA, TIFF, XBM, WBMP, XPM och SVG.</p></item>
  <item><p>Storleken på bilden skalas proportionellt till höjden på 48 bildpunkter. Så om du ställer in logon till exempelvis 1920x1080 så ändras den till en miniatyrbild av ursprungsbilden med dimensionerna 85x48.</p></item>
  </list>
  
  <steps>
  <title>Ställ in nyckeln org.gnome.login-screen.logo</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-gdm'])"/>
  <item><p>Skapa en <sys>gdm</sys>-databas för maskinomfattande inställningar i <file>/etc/dconf/db/gdm.d/<var>01-logo</var></file>:</p>
  <code>[org/gnome/login-screen]
  logo='<var>/usr/share/pixmaps/logo/greeter-logo.png</var>'
  </code>
  <p>Ersätt <var>/usr/share/pixmaps/logo/greeter-logo.png</var> med sökvägen till bildfilen som du vill använda som välkomstlogo.</p></item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  </steps>

  <section id="login-logo-not-update">
  <title>Vad är orsaken om logotypen inte uppdateras?</title>

  <p>Säkerställ att du har kört kommandot <cmd>dconf update</cmd> för att uppdatera systemdatabaserna.</p>

  <p>Om logotypen inte uppdateras, försök att starta om <sys>GDM</sys>.</p>
  </section>

</page>
