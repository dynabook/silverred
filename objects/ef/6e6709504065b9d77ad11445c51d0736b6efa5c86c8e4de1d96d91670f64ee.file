<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="look-display-fuzzy" xml:lang="de">

  <info>
    <link type="guide" xref="hardware-problems-graphics"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-10-11" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.28" date="2018-07-28" status="review"/>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Die Bildschirmauflösung könnte falsch eingestellt sein.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2019.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2019.</mal:years>
    </mal:credit>
  </info>

  <title>Warum ist das Bild auf meinem Bildschirm unscharf/verpixelt?</title>

  <p>Die eingestellte Bildschirmauflösung könnte für Ihren Bildschirm ungeeignet sein. So lösen Sie das Problem:</p>

  <steps>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Einstellungen</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Einstellungen</gui>.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Geräte</gui> in der Seitenleiste.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Bildschirme</gui> in der Seitenleiste, um das Panel zu öffnen.</p>
    </item>
    <item>
      <p>Probieren Sie einige der <gui>Auflösung</gui>-Optionen und wählen Sie diejenige aus, mit welcher der Bildschirm besser aussieht.</p>
    </item>
  </steps>

<section id="multihead">
  <title>Bei mehreren angeschlossenen Bildschirmen</title>

  <p>Wenn Sie zwei Bildschirme am Rechner angeschlossen haben (zum Beispiel einen normalen Monitor und einen Projektor), können die Bildschirme unterschiedliche optimale (<link xref="look-resolution#native">native</link>) Auflösungen haben.</p>

  <p>Mit dem Modus <link xref="display-dual-monitors#modes">Bildschirm spiegeln</link> wird der gleiche Inhalt auf beiden Bildschirmen dargestellt. Beide verwenden dabei die gleiche Auflösung, die gegebenenfalls nicht der nativen Auflösung eines von beiden Bildschirmen entspricht. Dies kann sich negativ auf die Bildschärfe auswirken.</p>

  <p>Im Modus <link xref="display-dual-monitors#modes">Bildschirme verketten</link> kann die Auflösung jedes Bildschirms individuell eingestellt werden, so dass beide auf Ihre native Auflösung eingestellt werden können.</p>

</section>

</page>
