<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-workspaces-switch" xml:lang="fi">

  <info>
    <link type="guide" xref="shell-windows#working-with-workspaces"/>
    <link type="seealso" xref="shell-workspaces"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.35.91" date="2020-02-27" status="candidate"/>

    <credit type="author">
      <name>Gnomen dokumentointiprojekti</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Käytä työtilan valitsinta.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

 <title>Vaihtele työtilojen välillä</title>

 <steps>  
 <title>Hiirtä käyttäen:</title>
 <item>
  <p if:test="!platform:gnome-classic">Avaa <gui xref="shell-introduction#activities">Toiminnot</gui>-yleisnäkymä.</p>
  <p if:test="platform:gnome-classic">At the bottom right of the screen, click
  on one of the four workspaces to activate the workspace.</p>
 </item>
 <item if:test="!platform:gnome-classic">
  <p>Napsauta työtilaa näytön oikeassa laidassa olevasta <link xref="shell-workspaces">työtilanvaihtimesta</link> nähdäksesi kyseisessä työtilassa avoinna olevat ikkunat.</p>
 </item>
 <item if:test="!platform:gnome-classic">
  <p>Napsauta mitä tahansa ikkunan pienoiskuvaa aktivoidaksesi työtilan.</p>
 </item>
 </steps>
 
 <list>
 <title>Näppäimistöltä:</title>  
  <item>
    <p if:test="!platform:gnome-classic">Paina <keyseq><key xref="keyboard-key-super">Super</key><key>Page Up</key></keyseq> tai <keyseq><key>Ctrl</key><key>Alt</key><key>nuoli ylös</key></keyseq> siirtyäksesi työtilanvaihtimessa näkyvissä olevan nykyisen työtilan yllä olevaan työtilaan.</p>
    <p if:test="platform:gnome-classic">Press <keyseq><key>Ctrl</key>
    <key>Alt</key><key>←</key></keyseq> to move to the workspace shown left
    of the current workspace on the <em>workspace selector</em>.</p>
  </item>
  <item>
    <p if:test="!platform:gnome-classic">Paina <keyseq><key>Super</key><key>Page Down</key></keyseq> tai <keyseq><key>Ctrl</key><key>Alt</key><key>nuoli alas</key></keyseq> siirtyäksesi työtilanvaihtimessa näkyvissä olevan nykyisen työtilan alla olevaan työtilaan.</p>
    <p if:test="platform:gnome-classic">Press <keyseq><key>Ctrl</key>
    <key>Alt</key><key>→</key></keyseq> to move to the workspace shown right of
    the current workspace on the <em>workspace selector</em>.</p>
  </item>
 </list>

</page>
