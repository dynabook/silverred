<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-disable-service" xml:lang="fi">

  <info>
    <link type="guide" xref="accounts"/>

    <revision pkgversion="3.5.5" date="2012-08-14" status="review"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Joillakin verkkotileillä voi olla käyttöoikeus moniin palveluihin, kuten kalenteriin ja sähköpostiin. Voit hallita, mitä palveluja sovellukset voivat käyttää.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

  <title>Hallitse mihin verkkopalveluihin tilillä on käyttöoikeus</title>

  <p>Some types of online account providers allow you to access several services
  with the same user account. For example, Google accounts provide access to
  calendar, email, contacts and chat. You may want to use your account for some
  services, but not others. For example, you may want to use your Google account
  for email but not chat if you have a different online account that you use
  for chat.</p>

  <p>You can disable some of the services that are provided by each online
  account:</p>

  <steps>
    <item>
      <p>Avaa <gui xref="shell-introduction#activities">Toiminnot</gui>-yleisnäkymä ja kirjoita <gui>Verkkotilit</gui>.</p>
    </item>
    <item>
      <p>Napsauta <gui>Verkkotilit</gui>.</p>
    </item>
    <item>
      <p>Select the account which you want to change from the list on the
      right.</p>
    </item>
    <item>
      <p>A list of services that are available with this account will be
      shown under <gui>Use for</gui>. See <link xref="accounts-which-application"/>
      to see which applications access which services.</p>
    </item>
    <item>
      <p>Switch off any of the services that you do not want to use.</p>
    </item>
  </steps>

  <p>Once a service has been disabled for an account, applications on your
  computer will not be able to use the account to connect to that service any
  more.</p>

  <p>To turn on a service that you disabled, just go back to the <gui>Online
  Accounts</gui> panel and switch it on.</p>

</page>
