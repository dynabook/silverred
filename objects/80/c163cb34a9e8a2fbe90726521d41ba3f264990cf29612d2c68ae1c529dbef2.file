<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="bluetooth-send-file" xml:lang="sr">

  <info>
    <link type="guide" xref="bluetooth"/>
    <link type="guide" xref="sharing"/>
    <link type="seealso" xref="files-share"/>

    <revision pkgversion="3.8" date="2013-05-16" status="review"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Џим Кембел</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Пол В. Фрилдс</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Мајкл Хил</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Дејвид Кинг</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Делите датотеке Блутут уређајима као што је ваш телефон.</desc>
  </info>

  <title>Пошаљите датотеке Блутут уређају</title>

  <p>Можете да шаљете датотеке на повезане блутут уређаје, као што су неки мобилни телефони или други рачунари. Неке врсте уређаја не дозвољавају пренос датотека, или нарочитих врста датотека. Можете да пошаљете датотеке користећи прозор подешавања блутута.</p>

  <note style="important">
    <p><gui>Send Files</gui> does not work on unsupported devices such as iPhones.</p>
  </note>

  <steps>
    <item>
      <p>Отворите преглед <gui xref="shell-introduction#activities">Активности</gui> и почните да куцате <gui>Блутут</gui>.</p>
    </item>
    <item>
      <p>Кликните на <gui>Блутут</gui> да отворите панел.</p>
    </item>
    <item>
      <p>Make sure Bluetooth is enabled: the switch in the titlebar should be
      set to on.</p>
    </item>
    <item>
      <p>На списку <gui>уређаја</gui>, изаберите уређај коме ће се слати датотеке. Ако жељени уређај није приказан као <gui>Повезан</gui> на списку, треба да се <link xref="bluetooth-connect-device">повежете</link> с њим.</p>
      <p>Појавиће се панел намењен спољном уређају.</p>
    </item>
    <item>
      <p>Кликните <gui>Пошаљи датотеке…</gui> и појавиће се бирач датотека.</p>
    </item>
    <item>
      <p>Изаберите датотеку коју желите да пошаљете и кликните <gui>Изабери</gui>.</p>
      <p>Да пошаљете више од једне датотеке у фасцикли, држите притиснутим <key>Ктрл</key> и бирајте датотеку једну по једну.</p>
    </item>
    <item>
      <p>Власник пријемног уређаја би требао да притисне дугме да би прихватио датотеку. Прозорче <gui>преноса датотека блутутом</gui> ће приказати траку напредовања. Кликните <gui>Затвори</gui> када се пренос заврши.</p>
    </item>
  </steps>

</page>
