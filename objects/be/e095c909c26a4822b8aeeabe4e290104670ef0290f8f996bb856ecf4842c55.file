<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-setup" xml:lang="pt">

  <info>
    <link type="guide" xref="printing#setup" group="#first"/>
    <link type="seealso" xref="printing-setup-default-printer"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="final"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Phil Bulh</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbelh</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hilh</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Set up a printer that is connected to your computer, or your local
    network.</desc>
  </info>

  <title>Configurar uma impressora local</title>

  <p>Your system can recognize many types of printers automatically once they
  are connected. Most printers are connected with a USB cable that attaches to
  your computer, but some printers connect to your wired or wireless
  network.</p>

  <note style="tip">
    <p>If your printer is connected to the network, it will not be set up
    automatically – you should add it from the <gui>Printers</gui> panel in
    <gui>Settings</gui>.</p>
  </note>

  <steps>
    <item>
      <p>Assegure-se que a impressora está acendida.</p>
    </item>
    <item>
      <p>Ligue a impressora a seu sistema mediante o cabo apropriado. Pode ver a atividade no ecrã, quando o sistema procura os controladores, e pode que se lhe peça que se autentique para os instalar.</p>
    </item>
    <item>
      <p>Aparecerá uma mensagem quando o sistema tenha terminado de instalar a impressora. Selecione <gui>Imprimir uma página de prova</gui> para imprimir uma página de prova, ou <gui>Opções</gui> para fazer mudanças adicionais na configuração da impressora.</p>
    </item>
  </steps>

  <p>Se a sua impressora não se configurou de maneira automática, pode acrescentar na configuração de impressora.</p>

  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Atividades</gui> e comece a escrever <gui>Impressoras</gui>.</p>
    </item>
    <item>
      <p>Carregue <gui>Impressoras</gui>.</p>
    </item>
    <item>
      <p>Carregue <gui style="button">Desbloquear</gui> no canto superior direita e introduza a sua palavra-passe quando se lhe peça.</p>
    </item>
    <item>
      <p>Press the <gui style="button">Add…</gui> button.</p>
    </item>
    <item>
      <p>In the pop-up window, select your new printer and press
      <gui style="button">Add</gui>.</p>
      <note style="tip">
        <p>If your printer is not discovered automatically, but you know its
        network address, enter it into the text field at the bottom of the
        dialog and then press <gui style="button">Add</gui></p>
      </note>
    </item>
  </steps>

  <p>If your printer does not appear in the <gui>Add Printer</gui> window, you
  may need to install print drivers.</p>

  <p>Apos instalar a impressora pode querer <link xref="printing-setup-default-printer">mudar a sua impressora predeterminada</link>.</p>

</page>
