<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="sharing-desktop" xml:lang="as">

  <info>
    <link type="guide" xref="sharing"/>
    <link type="guide" xref="prefs-sharing"/>

    <revision pkgversion="3.14" date="2015-01-25" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.29" date="2018-08-28" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <credit type="author">
      <name>একাটেৰিনা গেৰাচিমভা</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>মাইকেল হিল</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>জিম কেম্পবেল</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>অন্য মানুহক VNC ব্যৱহাৰ কৰি আপোনাৰ ডেস্কটপক দৰ্শন আৰু ভাৱবিনিময় কৰাৰ অনুমতি দিয়ক।</desc>
  </info>

  <title>আপোনাৰ ডেস্কটপ অংশীদাৰী কৰক</title>

  <p>আপুনি অন্য মানুহক এটা ডেস্কটপ দৰ্শন কৰা এপ্লিকেচনৰ সৈতে অন্য কমপিউটাৰৰ পৰা আপোনাৰ ডেস্কটপ দৰ্শন আৰু নিয়ন্ত্ৰণ কৰাব পাৰে। অন্য আপোনাৰ ডেস্কটপ অভিগম কৰাৰ অনুমতি আৰু সুৰক্ষা পছন্দসমূহ সংহতি কৰিবলৈ <gui>পৰ্দা অংশীদাৰী</gui> সংৰূপণ কৰক।</p>

  <note style="info package">
    <p><gui>পৰ্দা অংশীদাৰী</gui> দৃশ্যমান হবলৈ আপোনাৰ <app>Vino</app> পেকেইজ ইনস্টল থাকিব লাগিব।</p>

    <if:choose xmlns:if="http://projectmallard.org/if/1.0/">
      <if:when test="action:install">
        <p><link action="install:vino" style="button">Vino ইনস্টল কৰক</link></p>
      </if:when>
    </if:choose>
  </note>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Sharing</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>If the <gui>Sharing</gui> switch at the top-right of the window is set
      to off, switch it to on.</p>

      <note style="info"><p>If the text below <gui>Computer Name</gui> allows
      you to edit it, you can <link xref="sharing-displayname">change</link>
      the name your computer displays on the network.</p></note>
    </item>
    <item>
      <p><gui>পৰ্দাৰ অংশীদাৰী</gui>।</p>
    </item>
    <item>
      <p>To let others view your desktop, switch the <gui>Screen Sharing</gui>
      switch to on. This means that other people will be able to attempt to
      connect to your computer and view what’s on your screen.</p>
    </item>
    <item>
      <p>To let others interact with your desktop, ensure that <gui>Allow
      connections to control the screen</gui> is checked. This may allow the
      other person to move your mouse, run applications, and browse files on
      your computer, depending on the security settings which you are currently
      using.</p>
    </item>
  </steps>

  <section id="security">
  <title>সুৰক্ষা</title>

  <p>আপুনি কোনো সুৰক্ষা বিকল্প পৰিবৰ্তন কৰাৰ আগত তাৰ সম্পূৰ্ণ অৰ্থ জনাটো গুৰুত্বপূৰ্ণ।</p>

  <terms>
    <item>
      <title>New connections must ask for access</title>
      <p>If you want to be able to choose whether to allow someone to access
      your desktop, enable <gui>New connections must ask for access</gui>.  If
      you disable this option, you will not be asked whether you want to allow
      someone to connect to your computer.</p>
      <note style="tip">
        <p>এই বিকল্প অবিকল্পিতভাৱে সামৰ্থবান।</p>
      </note>
    </item>
    <item>
      <title>Require a Password</title>
      <p>To require other people to use a password when connecting to your
      desktop, enable <gui>Require a Password</gui>. If you do not use this
      option, anyone can attempt to view your desktop.</p>
      <note style="tip">
        <p>এই বিকল্প অবিকল্পিতভাৱে অসামৰ্থবান, কিন্তু আপুনি ইয়াক সামৰ্থবান কৰি এটা সুৰক্ষিত পাছৱাৰ্ড সংহতি কৰিব লাগিব।</p>
      </note>
    </item>
<!-- TODO: check whether this option exists.
    <item>
      <title>Allow access to your desktop over the Internet</title>
      <p>If your router supports UPnP Internet Gateway Device Protocol and it
      is enabled, you can allow other people who are not on your local network
      to view your desktop. To allow this, select <gui>Automatically configure
      UPnP router to open and forward ports</gui>. Alternatively, you can
      configure your router manually.</p>
      <note style="tip">
        <p>This option is disabled by default.</p>
      </note>
    </item>
-->
  </terms>
  </section>

  <section id="networks">
  <title>Networks</title>

  <p>The <gui>Networks</gui> section lists the networks to which you are
  currently connected. Use the switch next to each to choose where your
  desktop can be shared.</p>
  </section>

  <section id="disconnect">
  <title>Stop sharing your desktop</title>

  <p>To disconnect someone who is viewing your desktop:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Sharing</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p><gui>Screen Sharing</gui> will show as <gui>Active</gui>. Click on
      it.</p>
    </item>
    <item>
      <p>Toggle the switch at the top to off.</p>
    </item>
  </steps>

  </section>


</page>
