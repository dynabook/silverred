<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-font-size" xml:lang="fi">

  <info>
    <link type="guide" xref="a11y#vision" group="lowvision"/>

    <revision pkgversion="3.7.1" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.26" date="2017-08-04" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Käytä isompaa fonttia helppolukuisuuden lisäämiseksi.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

  <title>Muuta tekstin kokoa näytöllä</title>

  <p>Jos lukeminen on vaikeaa tekstin pienen koon vuoksi, voit vaihtaa kirjaimen kokoa.</p>

  <steps>
    <item>
      <p>Avaa <gui xref="shell-introduction#activities">Toiminnot</gui>-yleisnäkymä ja aloita kirjoittamaan <gui>Esteettömyys</gui>.</p>
    </item>
    <item>
      <p>Napsauta <gui>Esteettömyys</gui> avataksesi paneelin.</p>
    </item>
    <item>
      <p>In the <gui>Seeing</gui> section, switch the <gui>Large Text</gui>
      switch to on.</p>
    </item>
  </steps>

  <p>Alternatively, you can quickly change the text size by clicking the
  <link xref="a11y-icon">accessibility icon</link> on the top bar and selecting
  <gui>Large Text</gui>.</p>

  <note style="tip">
    <p>In many applications, you can increase the text size at any time by
    pressing <keyseq><key>Ctrl</key><key>+</key></keyseq>. To reduce the text
    size, press <keyseq><key>Ctrl</key><key>-</key></keyseq>.</p>
  </note>

  <p><gui>Large Text</gui> will scale the text by 1.2 times. You can use
  <app>Tweaks</app> to make text size bigger or smaller.</p>

</page>
