<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="getting-started" version="1.0 if/1.0" xml:lang="gu">

<info>
    <link type="guide" xref="index" group="gs"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <desc>GNOME માટે નવાં છો? શીખો કેવી એની આસપાસ શું છે તે જાણો.</desc>
    <title type="link">Getting started with GNOME</title>
    <title type="text">શરૂ થઇ રહ્યુ છે</title>
</info>

<title>શરૂ થઇ રહ્યુ છે</title>

<if:choose>
<if:when test="!platform:gnome-classic">

  <ui:overlay width="235" height="145">
  <media type="video" its:translate="no" src="figures/gnome-launching-applications.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-launching-apps.svg">
      <ui:caption its:translate="yes"><desc style="center">કાર્યક્રમોને શરૂ કરો</desc></ui:caption>
    </ui:thumb>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>કાર્યક્રમોને શરૂ કરી રહ્યા છે</tt:p>
         </tt:div>
         <tt:div begin="5s" end="7.5s">
           <tt:p>સ્ક્રીનની ટોચે ડાબે ખૂણે <gui>પ્રવૃત્તિ</gui> તરફ તમારા માઉસ પોઇંટરને ખસાડો.</tt:p>
           </tt:div>
         <tt:div begin="7.5s" end="9.5s">
           <tt:p><gui>કાર્યક્રમો બતાવો</gui> ચિહ્ન પર ક્લિક કરો.</tt:p>
         </tt:div>
         <tt:div begin="9.5s" end="11s">
           <tt:p>કાર્યક્રમ પર ક્લિક કરો જે તમે ચલાવવા માંગો છો, ઉદાહરણ તરીકે, મદદ.</tt:p>
         </tt:div>
         <tt:div begin="12s" end="21s">
           <tt:p>વૈકલ્પિક રીતે, <key href="help:gnome-help/keyboard-key-super">Super</key> કી દબાવીને <gui>પ્રવૃત્તિ ઝાંખી</gui> ને ખોલવા માટે કિબોર્ડને વાપરો.</tt:p>
         </tt:div>
         <tt:div begin="22s" end="29s">
           <tt:p>કાર્યક્રમનાં નામને લખવાનું શરૂ કરો જે તમે શરૂ કરવા માંગો છો.</tt:p>
         </tt:div>
         <tt:div begin="30s" end="33s">
           <tt:p>કાર્યક્રમને શરૂ કરવા માટે <key>Enter</key> દબાવો.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

</if:when>
</if:choose>

  <ui:overlay width="235" height="145">
  <media type="video" its:translate="no" src="figures/gnome-task-switching.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-task-switching.svg">
        <ui:caption its:translate="yes"><desc style="center">કાર્યોને બદલો</desc></ui:caption>
    </ui:thumb>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>કાર્યોને બદલી રહ્યા છે</tt:p>
         </tt:div>
         <tt:div begin="5s" end="8s">
           <tt:p if:test="!platform:gnome-classic">સ્ક્રીનની ટોચે ડાબે ખૂણે <gui>પ્રવૃત્તિ</gui> તરફ તમારા માઉસ પોઇંટરને ખસાડો.</tt:p>
         </tt:div>
         <tt:div begin="9s" end="12s">
           <tt:p>કાર્યને બદલવા માટે વિન્ડો પર ક્લિક કરો.</tt:p>
         </tt:div>
         <tt:div begin="12s" end="14s">
           <tt:p>To maximize a window along the left side of the screen, grab
            the window’s titlebar and drag it to the left.</tt:p>
         </tt:div>
         <tt:div begin="14s" end="16s">
           <tt:p>જ્યારે અડધી સ્ક્રીન પ્રકાશિત થયેલ હોય, વિન્ડોને પ્રકાશિત કરો.</tt:p>
         </tt:div>
         <tt:div begin="16s" end="18">
           <tt:p>To maximize a window along the right side, grab the window’s
            titlebar and drag it to the right.</tt:p>
         </tt:div>
         <tt:div begin="18s" end="20s">
           <tt:p>જ્યારે અડધી સ્ક્રીન પ્રકાશિત થયેલ હોય, વિન્ડોને પ્રકાશિત કરો.</tt:p>
         </tt:div>
         <tt:div begin="23s" end="27s">
           <tt:p><gui>વિન્ડો સ્વીચર</gui> ને બતાવવા માટે <keyseq> <key href="help:gnome-help/keyboard-key-super">Super</key><key> Tab</key></keyseq> દબાવો.</tt:p>
         </tt:div>
         <tt:div begin="27s" end="29s">
           <tt:p>આગળની પ્રકાશિત થયેલ વિન્ડોને પસંદ કરવા માટે <key href="help:gnome-help/keyboard-key-super">Super </key> ને પ્રકાશિત કરો.</tt:p>
         </tt:div>
         <tt:div begin="29s" end="32s">
           <tt:p>વિન્ડોને ખોલવાની યાદી મારફતે, <key href="help:gnome-help/keyboard-key-super">Super</key> ને પ્રકાશિત કરો નહિં પરંતુ તેને પકડી રાખો, અને <key>Tab</key> ને દબાવો.</tt:p>
         </tt:div>
         <tt:div begin="35s" end="37s">
           <tt:p><gui>પ્રવૃત્તિ ઝાંખી</gui> ને બતાવવા માટે <key href="help:gnome-help/keyboard-key-super">Super </key> કીને દબાવો.</tt:p>
         </tt:div>
         <tt:div begin="37s" end="40s">
           <tt:p>કાર્યક્રમનાં નામને લખવાનું શરૂ કરો જે તમે બદલવા માંગો છો.</tt:p>
         </tt:div>
         <tt:div begin="40s" end="43s">
           <tt:p>પહેલાં પરિણામ પ્રમાણે જ્યારે કાર્યક્રમ દેખાય, તેને બદલવા માટે <key> Enter</key> દબાવો.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

  <ui:overlay width="235" height="145">
  <media type="video" its:translate="no" src="figures/gnome-windows-and-workspaces.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-windows-and-workspaces.svg">
          <ui:caption its:translate="yes"><desc style="center">વિન્ડો અને કામ કરવાની જગ્યાને વાપરો</desc></ui:caption>
    </ui:thumb>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>વિન્ડો અને કામ કરવાની જગ્યા</tt:p>
         </tt:div>
         <tt:div begin="6s" end="10s">
           <tt:p>To maximize a window, grab the window’s titlebar and drag it to
            the top of the screen.</tt:p>
           </tt:div>
         <tt:div begin="10s" end="13s">
           <tt:p>જ્યારે સ્ક્રીન પ્રકાશિત થયેલ હોય, વિન્ડોને પ્રકાશિત કરો.</tt:p>
         </tt:div>
         <tt:div begin="14s" end="20s">
           <tt:p>To unmaximize a window, grab the window’s titlebar and drag it
            away from the edges of the screen.</tt:p>
         </tt:div>
         <tt:div begin="25s" end="29s">
           <tt:p>વિન્ડોને દૂર ખેંચવા માટે તમે પણ ટોચની પટ્ટી પર ક્લિક કરી શકો છો અને તેને મહત્તમ કરો નહિં.</tt:p>
         </tt:div>
         <tt:div begin="34s" end="38s">
           <tt:p>To maximize a window along the left side of the screen, grab
            the window’s titlebar and drag it to the left.</tt:p>
         </tt:div>
         <tt:div begin="38s" end="40s">
           <tt:p>જ્યારે અડધી સ્ક્રીન પ્રકાશિત થયેલ હોય, વિન્ડોને પ્રકાશિત કરો.</tt:p>
         </tt:div>
         <tt:div begin="41s" end="44s">
           <tt:p>To maximize a window along the right side of the screen, grab
            the window’s titlebar and drag it to the right.</tt:p>
         </tt:div>
         <tt:div begin="44s" end="48s">
           <tt:p>જ્યારે અડધી સ્ક્રીન પ્રકાશિત થયેલ હોય, વિન્ડોને પ્રકાશિત કરો.</tt:p>
         </tt:div>
         <tt:div begin="54s" end="60s">
           <tt:p>કિબોર્ડની મદદથી વિન્ડોને મહત્તમ કરવા માટે, <key href="help:gnome-help/keyboard-key-super">Super</key> કીને પકડો અને <key>↑</key> ને દબાવો.</tt:p>
         </tt:div>
         <tt:div begin="61s" end="66s">
           <tt:p>તેનાં મહત્તમ ન હોય તેવા માપ સાથે વિન્ડોને પુન:સંગ્રહવા માટે, <key href="help:gnome-help/keyboard-key-super">Super</key> કીને પકડો અને <key>↓</key> દબાવો.</tt:p>
         </tt:div>
         <tt:div begin="66s" end="73s">
           <tt:p>સ્ક્રીનની જમણી બાજુ સાથે વિન્ડોને મહત્તમ કરવા માટે, <key href="help:gnome-help/keyboard-key-super">Super</key> કીને પકડી રાખો અને <key>←</key> દબાવો.</tt:p>
         </tt:div>
         <tt:div begin="76s" end="82s">
           <tt:p>સ્ક્રીનની ડાબી બાજુ સાથે વિન્ડોને મહત્તમ કરવા માટે, <key href="help:gnome-help/keyboard-key-super">Super</key> કીને પકડી રાખો અને <key>←</key> દબાવો.</tt:p>
         </tt:div>
         <tt:div begin="83s" end="89s">
           <tt:p>કામ કરવાની જગ્યાને ખસેડવા કે જે હાલની કામ કરવાની જગ્યાની નીચે છે, <keyseq><key href="help:gnome-help/keyboard-key-super">Super</key><key>Page Down</key></keyseq> ને દબાવો.</tt:p>
         </tt:div>
         <tt:div begin="90s" end="97s">
           <tt:p>કામ કરવાની જગ્યાને ખસેડવા કે જે હાલની કામ કરવાની જગ્યાની ઉપર છે, <keyseq><key href="help:gnome-help/keyboard-key-super">Super</key><key>Page Up</key></keyseq> ને દબાવો.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

<links type="topic" style="grid" groups="tasks">
<title style="heading">Common tasks</title>
</links>

<links type="guide" style="heading nodesc">
<title/>
</links>

</page>
