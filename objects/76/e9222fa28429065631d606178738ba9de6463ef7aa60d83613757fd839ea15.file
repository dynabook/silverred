<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="bluetooth-turn-on-off" xml:lang="lv">

  <info>
    <link type="guide" xref="bluetooth" group="#first"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-04" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-21" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Aktivēt vai deaktivēt Bluetooth ierīci jūsu datorā.</desc>
  </info>

<title>Ieslēgt vai izslēgt Bluetooth</title>

  <p>Jūs varat ieslēgt Bluetooth, lai savienotos ar citām Bluetooth ierīcēm, vai to izslēgt, lai taupītu enerģiju Lai ieslēgtu Bluetooth:</p>

  <steps>
    <item>
      <p>Atveriet <gui xref="shell-introduction#activities">Aktivitāšu</gui> pārskatu un sāciet rakstīt <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Spiediet <gui>Bluetooth</gui>, lai atvērtu paneli.</p>
    </item>
    <item>
      <p>Set the switch at the top to on.</p>
    </item>
  </steps>

  <p>Daudziem klēpjdatoriem ir aparatūras slēdzis vai taustiņu kombinācija, lai ieslēgtu un izslēgtu Bluetooth. Meklējiet slēdzi uz sava datora vai taustiņa savā tastatūrā. Tastatūras taustiņš parasti tiek izmantots kombinācijā ar <key>Fn</key> taustiņu.</p>

  <p>Lai izslēgtu Bluetooth:</p>
  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#systemmenu">system menu</gui> from the
      right side of the top bar.</p>
    </item>
    <item>
      <p>Izvēlieties <gui><media its:translate="no" type="image" mime="image/svg" src="figures/bluetooth-active-symbolic.svg"/> Netiek izmantots</gui>. Bluetooth izvēlnes sadaļa izvērsīsies.</p>
    </item>
    <item>
      <p>Izvēlieties <gui>Izslēgt</gui>.</p>
    </item>
  </steps>

  <note><p>Jūsu dators ir <link xref="bluetooth-visibility">redzams</link> kamēr ir atvērts <gui>Bluetooth</gui> panelis.</p></note>

</page>
