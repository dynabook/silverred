<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-slow" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="net-problem"/>

    <revision pkgversion="3.4.0" date="2012-02-21" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>可能有其他下载的任务，您只有一个很慢的连接，或者现在是网络繁忙时段。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>网速感觉很慢</title>

  <p>有很多原因可能导致网络连接的速度变慢。</p>

  <p>尝试关闭 Web 浏览器再重新打开，从Internet 断开再重新连接。(这样做可以重置很多可能导致 Internet 变慢的因素。)</p>

  <list>
    <item>
      <p><em style="strong">网络繁忙</em></p>
      <p>Internet service providers commonly setup internet connections so that
      they are shared between several households. Even though you connect
      separately, through your own phone line or cable connection, the
      connection to the rest of the internet at the telephone exchange might
      actually be shared. If this is the case and lots of your neighbors are
      using the internet at the same time as you, you might notice a slow-down.
      You’re most likely to experience this at times when your neighbors are
      probably on the internet (in the evenings, for example).</p>
    </item>
    <item>
      <p><em style="strong">一次下载很多内容</em></p>
      <p>如果您或别的什么人，使用网络同时下载很多文件，或者在看视频，网速可能会力不从心达不到要求，在这种情况下，就会感觉网速慢。</p>
    </item>
    <item>
      <p><em style="strong">不可靠的连接</em></p>
      <p>一些地方的网络连接确实不稳定，尤其是临时场所或连接要求多的地方。如果您在一个繁忙的咖啡店或者会议中心，网络连接可能会非常忙碌或者变得不稳定。</p>
    </item>
    <item>
      <p><em style="strong">弱无线连接信号</em></p>
      <p>If you are connected to the internet by wireless (Wi-Fi), check the
      network icon on the top bar to see if you have good wireless signal. If
      not, the internet may be slow because you don’t have a very strong
      signal.</p>
    </item>
    <item>
      <p><em style="strong">使用的移动网络连接速度较慢</em></p>
      <p>If you have a mobile internet connection and notice that it is slow,
      you may have moved into an area where signal reception is poor. When this
      happens, the internet connection will automatically switch from a fast
      “mobile broadband” connection like 3G to a more reliable, but slower,
      connection like GPRS.</p>
    </item>
    <item>
      <p><em style="strong">Web 浏览器有问题</em></p>
      <p>Sometimes web browsers encounter a problem that makes them run slow.
      This could be for any number of reasons — you could have visited a
      website that the browser struggled to load, or you might have had the
      browser open for a long time, for example. Try closing all of the
      browser’s windows and then opening the browser again to see if this makes
      a difference.</p>
    </item>
  </list>

</page>
