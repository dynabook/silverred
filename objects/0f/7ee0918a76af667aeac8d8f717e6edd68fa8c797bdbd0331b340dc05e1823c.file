<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="disk-benchmark" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="disk"/>

    <revision pkgversion="3.6.2" version="0.2" date="2012-11-16" status="review"/>
    <revision pkgversion="3.10" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
   <credit type="editor">
     <name>Michael Hill</name>
     <email>mdhillca@gmail.com</email>
   </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>您可以在您的硬盘上运行性能测试程序，检查其速度。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>测试硬盘性能</title>

  <p>测试您的硬盘的速度</p>

  <steps>
    <item>
      <p>Open <app>Disks</app> from the
      <gui xref="shell-introduction#activities">Activities</gui> overview.</p>
    </item>
    <item>
      <p>Choose the disk from the list in the left pane.</p>
    </item>
    <item>
      <p>Click the menu button and select <gui>Benchmark disk…</gui> from the
      menu.</p>
    </item>
    <item>
      <p>Click <gui>Start Benchmark…</gui> and adjust the <gui>Transfer
      Rate</gui> and <gui>Access Time</gui> parameters as desired.</p>
    </item>
    <item>
      <p>Click <gui>Start Benchmarking</gui> to test how fast data can be read
      from the disk. <link xref="user-admin-explain">Administrative
      privileges</link> may be required. Enter your password, or the password
      for the requested administrator account.</p>
      <note>
        <p>If <gui>Perform write-benchmark</gui> is checked, the benchmark
        will test how fast data can be read from and written to the disk. This
        will take longer to complete.</p>
      </note>
    </item>
  </steps>

  <p>测试完成后，将会显示测试结果图表，绿色的点和线段代表样例，它们与右边的纵轴线相对应，显示访问时间，与底部横座标轴线组成小方格，对应花费的时间百分比。蓝线代表读取速率，有红线代表写入速率，在左侧的纵轴上显示了访问速率，与底部横座标轴线组成小方格，从外到内，对应磁盘访问的百分比。</p>

  <p>图像下方将显示最大、最小和平均读写率等多个指标的数值。</p>

</page>
