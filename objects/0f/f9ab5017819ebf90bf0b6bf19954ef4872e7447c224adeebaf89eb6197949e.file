<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-changepassword" xml:lang="id">

  <info>
    <link type="guide" xref="user-accounts#passwords"/>
    <link type="seealso" xref="user-goodpassword"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Proyek Dokumentasi GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Keep your account secure by changing your password often in your
    account settings.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

  <title>Ubah kata sandi Anda</title>

  <p>It is a good idea to change your password from time to time, especially if
  you think someone else knows your password.</p>

  <p>You need <link xref="user-admin-explain">administrator privileges</link>
  to edit user accounts other than your own.</p>

  <steps>
    <item>
      <p>Buka ringkasan <gui xref="shell-introduction#activities">Aktivitas</gui> dan mulai mengetik <gui>Pengguna</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Users</gui> to open the panel.</p>
    </item>
    <item>
      <p>Click the label <gui>·····</gui> next to <gui>Password</gui>. If you
      are changing the password for a different user, you will first need to
      <gui>Unlock</gui> the panel.</p>
    </item>
    <item>
      <p>Enter your current password, then a new password. Enter your new
      password again in the <gui>Verify New Password</gui> field.</p>
      <p>You can press the <gui style="button"><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16">
      <span its:translate="yes">generate password</span></media></gui> icon to
      automatically generate a random password.</p>
    </item>
    <item>
      <p>Klik <gui>Ubah</gui>.</p>
    </item>
  </steps>

  <p>Pastikan bahwa Anda <link xref="user-goodpassword">memilih suatu sandi yang baik</link>. Ini akan membantu menjaga akun pengguna Anda aman.</p>

  <note>
    <p>Ketika Anda memutakhirkan sandi log masuk ANda, sandi ring kunci log masuk Anda akan secara otomatis dimutakhirkan agar sama dengan sandi log masuk baru Anda.</p>
  </note>

  <p>If you forget your password, any user with administrator privileges can
  change it for you.</p>

</page>
