<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-sensitivity" xml:lang="pl">

  <info>
    <link type="guide" xref="mouse" group="#first"/>
    <link type="guide" xref="a11y#mobility" group="pointing"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.10" date="2013-10-29" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-05" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit>
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit>
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Zmiana prędkości poruszania się kursora podczas używania myszy lub panelu dotykowego.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  </info>

  <title>Dostosowanie prędkości myszy i panelu dotykowego</title>

  <p>Jeśli kursor porusza się za szybko lub za wolno podczas używania myszy lub panelu dotykowego, to można dostosować jego prędkość.</p>

  <steps>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Mysz i panel dotykowy</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Mysz i panel dotykowy</gui>, aby otworzyć panel.</p>
    </item>
    <item>
      <p>Ustaw suwak <gui>Prędkość myszy</gui> lub <gui>Prędkość panelu dotykowego</gui> na wygodną wartość. Czasami najwygodniejsze ustawienia dla jednego rodzaju urządzeń nie są odpowiednie dla drugiego.</p>
    </item>
  </steps>

  <note>
    <p>Sekcja <gui>Panel dotykowy</gui> jest wyświetlana tylko, jeśli komputer ma panel dotykowy, a sekcja <gui>Mysz</gui> jest wyświetlana tylko, kiedy mysz jest podłączona.</p>
  </note>

</page>
