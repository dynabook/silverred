<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-maximize" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>
    <link type="seealso" xref="shell-windows-tiled"/>

    <revision pkgversion="3.4.0" date="2012-03-14" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Šon Mek Kens</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Dva puta kliknite ili prevucite traku naslova da uvećate ili povratite prozor.</desc>
  </info>

  <title>Povećajte i poništite povećanje prozora</title>

  <p>Možete da povećate prozor da zauzme čitav prostor na radnoj površi i da poništite povećanje prozora da biste ga vratili na njegovu uobičajenu veličinu. Možete takođe da povećate prozore uspravno duž leve ili desne strane ekrana, tako da možete s lakoćom da posmatrate dva prozora odjednom. Pogledajte <link xref="shell-windows-tiled"/> za pojedinosti.</p>

  <p>Da povećate prozor, uhvatite traku naslova i prevucite je do vrha ekrana, ili samo dva puta kliknite na istu. Da povećate prozor koristeći tastaturu, držite pritisnutim taster <key xref="keyboard-key-super">Super</key> i pritisnite <key>↑</key>, ili pritisnite <keyseq><key>Alt</key><key>F10</key></keyseq>.</p>

  <p if:test="platform:gnome-classic">Prozor možete takođe da uvećate pritiskom na dugme za uvećavanje na naslovnoj traci.</p>

  <p>Da povratite prozor na njegovu nepovećanu veličinu, odlepite ga od ivice ekrana. Ako je prozor u potpunosti povećan, možete dva puta da kliknete na traku naslova da biste ga povratili. Možete takođe da koristite iste prečice tastature koje ste koristili za povećanje prozora.</p>

  <note style="tip">
    <p>Držite pritisnutim taster <key>Super</key> i zgrabite bilo gde unutar prozora da ga pomerite.</p>
  </note>

</page>
