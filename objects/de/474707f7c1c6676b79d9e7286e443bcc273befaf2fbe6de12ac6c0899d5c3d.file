<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-language" xml:lang="ca">

  <info>
    <link type="guide" xref="prefs-language"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Canvieu a un idioma diferent per a la interfície d'usuari i el text d'ajuda.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

  <title>Canviar l'idioma que utilitzeu</title>

  <p>Podeu fer servir l'escriptori i les aplicacions en dotzenes d'idiomes, sempre que tingueu instal·lats els paquets d'idioma adequats a l'ordinador.</p>

  <steps>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a teclejar <gui>Regió i idioma</gui>.</p>
    </item>
    <item>
      <p>Feu clic a <gui>Regió i idioma</gui> per obrir el quadre.</p>
    </item>
    <item>
      <p>Feu clic a <gui>Idioma</gui>.</p>
    </item>
    <item>
      <p>Seleccioneu la regió i l'idioma desitjats. Si la vostra regió i el vostre idioma no apareixen a la llista, feu clic a <gui><media its:translate="no" type="image" mime="image/svg" src="figures/view-more-symbolic.svg"><span its:translate="yes">…</span></media></gui> a la part inferior de la llista per seleccionar entre totes les regions els idiomes disponibles.</p>
    </item>
    <item>
      <p>Feu clic a <gui style="button">Fet</gui> per desar.</p>
    </item>
    <item>
      <p>Responeu a la sol·licitud, <gui> 	S'ha de tornar a iniciar la sessió perquè els canvis tinguin efecte</gui> fent clic a <gui style="button">Reinicia ara</gui>, o fent clic a <gui style="button">×</gui> per reiniciar més endavant.</p>
    </item>
  </steps>

  <p>Algunes traduccions poden ser incompletes, i és possible que algunes aplicacions no admetin la vostra llengua. Qualsevol text no traduït apareixerà en l'idioma en què s'hagi desenvolupat originalment el programari, en general anglès americà.</p>

  <p>Hi ha algunes carpetes especials a la vostra carpeta d'inici on les aplicacions poden emmagatzemar elements com música, imatges i documents. Aquestes carpetes usen noms estàndard segons el vostre idioma. Quan torneu a iniciar la sessió, se us demanarà si voleu canviar el nom d'aquestes carpetes als noms estàndard per a l'idioma seleccionat. Si teniu previst utilitzar sempre el nou idioma, heu d'actualitzar els noms de les carpetes.</p>

  <note style="tip">
    <p>Si hi ha diversos comptes d'usuari al vostre sistema, hi ha una instància separada del quadre <gui>Regió i idioma</gui> per a la pantalla d'inici de sessió. Feu clic al botó <gui>Inicia sessió</gui> a la part superior dreta per alternar entre les dues instàncies.</p>
  </note>

</page>
