<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-calibrate-scanner" xml:lang="lv">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-calibrationtargets"/>
    <link type="seealso" xref="color-calibrate-printer"/>
    <link type="seealso" xref="color-calibrate-screen"/>
    <link type="seealso" xref="color-calibrate-camera"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-05" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jūsu skenera kalibrēšana ir svarīga, lai iegūtu precīzas krāsas.</desc>
  </info>

  <title>Kā lai es kalibrēju savu skeneri?</title>

  <p>Ja vēlaties, lai skeneris pareizi attēlotu ieskanētā krāsas, jums to vajadzētu kalibrēt.</p>

  <steps>
    <item>
      <p>Pārliecinieties, ka skeneris ir savienots ar jūsu datoru ar vadu vai caur tīklu.</p>
    </item>
    <item>
      <p>Ieskenējiet savu kalibrēšanas mērķi un saglabājiet to kā nesaspiestu TIFF datni.</p>
    </item>
    <item>
      <p>Atveriet <gui xref="shell-introduction#activities">aktivitāšu</gui> pārskatu un sāciet rakstīt <gui>Iestatījumi</gui>.</p>
    </item>
    <item>
      <p>Spiediet <gui>Iestatījumi</gui>.</p>
    </item>
    <item>
      <p>Sānu joslā nospiediet <gui>Ierīces</gui>.</p>
    </item>
    <item>
      <p>Sānu joslā spiediet <gui>Krāsa</gui>, lai atvērtu paneli.</p>
    </item>
    <item>
      <p>Izvēlieties savu skeneri.</p>
    </item>
    <item>
      <p>Spiediet <gui style="button">Kalibrēt…</gui>, lai sāktu kalibrēšanu.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Skenera ierīces ir ļoti stabilas gan no vecuma, gan temperatūras viedokļa, tāpēc tās parasti nav nepieciešams pārkalibrēt.</p>
  </note>

</page>
