<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-willnotturnon" xml:lang="gu">

  <info>
    <link type="guide" xref="power#problems"/>
    <link type="guide" xref="hardware-problems-graphics" group="#last"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>કેબલ ઢીલા હોવા અને હાર્ડવેર સમસ્યાઓ એ શક્ય કારણો છે.</desc>
    <credit type="author">
      <name>GNOME દસ્તાવેજીકરણ પ્રોજેક્ટ</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>મારું કમ્પ્યૂટર ચાલુ થશે નહિં</title>

<p>ત્યાં ઘણા કારણો છે શા માટે તમારાં કમ્પ્યૂટરને ચાલુ કરશે નહિં. આ વિષય એ અમુક શક્ય કારણોની ઝાંખી આપે છે.</p>
	
<section id="nopower">
  <title>કમ્પ્યૂટર પ્લગ થયેલ નથી, ખાલી બેટરી છે, અથવા કેબલ ઢીલો છે</title>
  <p>ખાતરી કરો ક કમ્પ્યૂટરનું પાવર કેબલ એ પ્લગ થયેલ છે અને પાવર આઉટલેટ ચાલુ થયેલ છે. ખાતરી કરો કે મોનિટર તેમાં પ્લગ થયેલ છે અને ચાલુ પણ છે. જો તમારી પાસે લેપટોપ હોય તો, ચાર્જિંગ કેબલ સાથે જોડાવો (આ સ્થિતિમાં બેટરી ઊતરી જાય છે). તમ પણ ત બેટરી ચકાસવાની ઇચ્છા રાખી શકો છો ક બેટરી એ યોગ્ય જગ્યામાં બંધબેસેલ છે (લેપટોપની નીચેની બાજુએ ચકાસો) જો તે દૂર કરી શકાય તેવી છે.</p>
</section>

<section id="hardwareproblem">
  <title>કમ્પ્યૂટર હાર્ડવેર સાથે સમસ્યા</title>
  <p>A component of your computer may be broken or malfunctioning. If this is
  the case, you will need to get your computer repaired. Common faults include
  a broken power supply unit, incorrectly-fitted components (such as the
  memory or RAM) and a faulty motherboard.</p>
</section>

<section id="beeps">
  <title>કમ્પ્યૂટરમાંથી અવાજ આવે છે અને પછી બંધ થઇ જાય છે</title>
  <p>If the computer beeps several times when you turn it on and then turns off
  (or fails to start), it may be indicating that it has detected a problem.
  These beeps are sometimes referred to as <em>beep codes</em>, and the pattern
  of beeps is intended to tell you what the problem with the computer is.
  Different manufacturers use different beep codes, so you will have to consult
  the manual for your computer’s motherboard, or take your computer in for
  repairs.</p>
</section>

<section id="fans">
  <title>કમ્પ્યૂટરનો પંખો ફરી રહ્યો છે પરંતુ સ્ક્રીન પર કંઇ થતુ નથી</title>
  <p>પહેલી વસ્તુ એ ચકાસે કે તમારું પ્રિન્ટર એ પ્લગ થયેલ અને ચાલુ થયેલ છે.</p>
  <p>આ સમસ્યા હાર્ડવેર સમસ્યાને કારણે થઇ શકે છે. પંખો ચાલુ થઇ શકે છે જ્યારે તમે પાવર બટનને દબાવો, પરંતુ કમ્પ્યૂટરનાં બીજા જરૂરી ભાગો ચાલુ થવામાં નિષ્ફળ થઇ શકે છે. આ સ્થિતિમાં, તમારાં કમ્પ્યૂટરને સુધારવા લઇ જાવો.</p>
</section>

</page>
