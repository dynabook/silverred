<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting" xml:lang="cs">

  <info>
    <link type="guide" xref="net-wireless" group="first"/>
    <link type="guide" xref="hardware#problems" group="first"/>
    <link type="next" xref="net-wireless-troubleshooting-initial-check"/>

    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Přispěvatelé do wiki s dokumentací k Ubuntu</name>
    </credit>
    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Určení a oprava problémů s bezdrátovými připojeními.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Řešení problémů s bezdrátovými sítěmi</title>

  <p>Zde je průvodce, která vám krok za krokem pomůže nalézt a opravit problémy s bezdrátovým připojením. Když se vám z nějakého důvodu nedaří připojit k bezdrátové síti, zkuste následující instrukce uvedené zde.</p>

  <p>Postupně vás provedeme těmito kroky, abyste dosáhli připojení svého počítače k Internetu:</p>

  <list style="numbered compact">
    <item>
      <p>Provedení počáteční kontroly</p>
    </item>
    <item>
      <p>Získání informací o vašem hardwaru</p>
    </item>
    <item>
      <p>Kontrola vašeho hardwaru</p>
    </item>
    <item>
      <p>Pokus o vytvoření připojení k vašemu bezdrátovému směrovači</p>
    </item>
    <item>
      <p>Provedení kontroly vašeho modemu a směrovače</p>
    </item>
  </list>

  <p>Vše začněte kliknutím na odkaz <em>Následující</em> v pravo nahoře nebo dole na této stránce. Tyto odkazy a podobné na dalších stránkách vás provedou krok za krokem.</p>

  <note>
    <title>Používání příkazového řádku</title>
    <p>Některé instrukce v tomto průvodci vás budou vyzývat k napsání příkazu do <em>příkazové řádky</em> (Terminálu). Aplikaci <app>Terminál</app> najdete v přehledu <gui>Činnosti</gui>.</p>
    <p>I když nemáte zkušenosti s používáním příkazové řádky, nemusíte se bát – tento průvodce vás v každém kroku nasměruje. Vše, co si potřebujete pamatovat, je, že v příkazech se rozlišuje velikost písmen (takže musí být napsány <em>naprosto přesně</em>, jako zde) a po zadání každého příkazu musíte zmáčknout <key>Enter</key>.</p>
  </note>

</page>
