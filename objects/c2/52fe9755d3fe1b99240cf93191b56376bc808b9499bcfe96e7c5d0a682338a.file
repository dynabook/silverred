<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-hotcomputer" xml:lang="sv">

  <info>
    <link type="guide" xref="power#problems"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Datorer blir vanligtvis varma, men om de blir allt för varma kan de överhettas vilket kan skada datorn.</desc>

    <credit type="author">
      <name>Dokumentationsprojekt för GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

<title>Min dator blir väldigt varm</title>

<p>De flesta datorer blir varma efter ett tag och visa kan blir riktigt varma. Detta är normalt: det är helt enkelt en del av sättet på vilket datorn kyler sig själv. Om datorn blir allt för varm kan det vara ett tecken på att den överhettas, vilket potentiellt kan orsaka skador.</p>

<p>De flesta bärbara datorer blir ganska varma när du använt dem ett tag. Det är vanligtvis inget att oroa sig för — datorer producerar mycket värme och bärbara datorer är väldigt kompakta så de måste avge sin värme snabbt och deras ytterhölje värms upp som ett resultat. Vissa bärbara datorer blir dock för varma och kan bli obehagliga att använda. Detta är normalt ett resultat av dåligt designade kylsystem. Du kan ibland köpa extra kylningstillbehör som täcker botten på den bärbara datorn och ger en mer effektiv kylning.</p>

<p>Om du har en stationär dator som känns varm vid beröring kan den ha otillräcklig kylning. Om detta bekymrar dig kan du köpa extra kylningsfläktar eller kontrollera att kylningsfläktarna och ventilationen inte är blockerade av damm eller något annat. Det kan också vara bra att överväga att placera datorn på en bättre ventilerad plats — om den placeras i trånga utrymmen (till exempel i ett skåp) kan det vara så att kylsystemet i datorn inte kan avge värme och cirkulera kall luft snabbt nog.</p>

<p>Vissa personer är bekymrade om hälsoeffekterna av att använda bärbara datorer. Det finns indikationer på att förlängd användning av en varm, bärbar dator i ditt knä möjligt skulle kunna reducera (manlig) fruktbarhet och det finns rapporter om mindre brännmärken (i extrema fall). Om du är bekymrad om dessa potentiella problem kan det vara bra att ta kontakt med en praktiserande läkare för råd. Naturligtvis kan du helt enkelt välja att inte ha den bärbara datorn i ditt knä.</p>

<p>De flesta moderna datorer kommer att stänga ner sig själva om de blir allt för varma, för att förhindra att de själva blir skadad. Om din dator stänger ner sig ofta kan detta vara orsaken. Om din dator överhettas kommer du förmodligen att behöva få den reparerad.</p>

</page>
