<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-suspendfail" xml:lang="ca">

  <info>
    <link type="guide" xref="power#problems"/>
    <link type="guide" xref="hardware-problems-graphics"/>
    <link type="seealso" xref="hardware-driver"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Alguns maquinari de l'ordinador provoquen problemes amb la suspensió.</desc>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

<title>Per què el meu equip no es torna a activar després d'haver-lo suspès?</title>

<p>Si es <link xref="power-suspend">suspèn</link> l'ordinador, a continuació, intenteu reprendre'l, us podeu trobar amb què no funciona tal com esperàveu. Això podria ser degut al fet que el vostre maquinari no suporta correctament la suspensió.</p>

<section id="resume">
  <title>El meu ordinador ha entrat en suspensió i no es reprèn</title>
  <p>Si suspeneu l'ordinador i, a continuació, premeu una tecla o feu clic al ratolí, hauria de despertar-se i mostrar una pantalla demanant-vos la contrasenya. Si això no passa, proveu de prémer el botó d'encesa (no el mantingueu premut, simplement premeu-lo una vegada).</p>
  <p>Si això encara no us ajuda, assegureu-vos que el monitor de l'ordinador estigui activat i proveu de tornar a prémer una tecla del teclat.</p>
  <p>Com a últim recurs, apagueu l'ordinador mantenint al botó d'engegada premut durant 5-10 segons, encara que fent-ho perdreu qualsevol treball sense desar. A continuació, heu de tornar a activar l'ordinador.</p>
  <p>Si això passa cada vegada que suspeneu l'equip, és possible que la funció de suspensió no funcioni amb el vostre maquinari.</p>
  <note style="warning">
    <p>Si l'ordinador perd energia i no té una font d'alimentació alternativa (com una bateria de treball), es desactivarà.</p>
  </note>
</section>

<section id="hardware">
  <title>La meva connexió de xarxa sense fil (o un altre maquinari) no funciona quan es desperta l'ordinador</title>
  <p>Si suspeneu l'ordinador i el torneu a reprendre, podeu trobar-vos amb què la vostra connexió a Internet, el ratolí o algun altre dispositiu no funcioni correctament. Això podria ser perquè el controlador del dispositiu no admet correctament la suspensió. Es tracta d'un <link xref="hardware-driver">problema amb el controlador</link> i no del dispositiu per ell mateix.</p>
  <p>Si el dispositiu té un interruptor d'energia, proveu d'apagar-lo i tornar-lo a encendre. En la majoria dels casos, el dispositiu tornarà a funcionar de nou. Si es connecta mitjançant un cable USB o similar, desconnecteu el dispositiu i torneu-lo a connectar i mireu si funciona.</p>
  <p>Si no podeu apagar o desconnectar el dispositiu, o si això no funciona, és possible que hàgiu de reiniciar l'equip perquè el dispositiu torni a funcionar.</p>
</section>

</page>
