<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-blank" xml:lang="fi">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="guide" xref="hardware-problems-graphics"/>
    <link type="guide" xref="power#saving"/>
    <link type="seealso" xref="power-whydim"/>
    <link type="seealso" xref="session-screenlocks"/>

    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>
    <revision pkgversion="3.28" date="2018-07-22" status="review"/>
    <revision pkgversion="3.34" date="2019-10-28" status="review"/>

    <credit type="author editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>


    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Muuta näytön pimenemisviivettä säästääksesi virtaa.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

  <title>Aseta näytön pimenemisaika</title>

  <p>To save power, you can adjust the time before the screen blanks when left
  idle. You can also disable the blanking completely.</p>

  <steps>
    <title>Aseta näytön pimenemisaika seuraavasti:</title>
    <item>
      <p>Avaa <gui xref="shell-introduction#activities">Toiminnot</gui>-yleisnäkymä ja ala kirjoittamaan <gui>Virransäästö</gui>.</p>
    </item>
    <item>
      <p>Napsauta <gui>Virranhallinta</gui> avataksesi paneelin.</p>
    </item>
    <item>
      <p>Use the <gui>Blank screen</gui> drop-down list under <gui>Power
      Saving</gui> to set the time until the screen blanks, or disable the blanking
      completely.</p>
    </item>
  </steps>
  
  <note style="tip">
    <p>Kun tietokone jätetään jouten, näyttö lukkiutuu automaattisesti tietoturvasyistä. Tätä toimintaa voi vaihtaa, lue lisää kohdasta <link xref="session-screenlocks"/>.</p>
  </note>

</page>
