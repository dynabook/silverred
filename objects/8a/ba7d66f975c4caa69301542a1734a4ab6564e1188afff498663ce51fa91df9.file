<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="net-macaddress" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>O identificador único atribuído a uma hardware de rede.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

  <title>O que é um endereço MAC?</title>

  <p>Um <em>endereço MAC</em> é o identificador único que é atribuído pelo fabricante a uma peça do hardware de rede (como uma placa de rede sem fio ou uma placa ethernet). MAC significa <em>Media Access Control</em>, ou controle de acesso ao meio, e cada identificador tem a intenção de ser único para um dispositivo em particular.</p>

  <p>Um endereço MAC consiste em seis conjuntos de dois caracteres, cada um separado por uma vírgula. <code>00:1B:44:11:3A:B7</code> é um exemplo de endereço MAC.</p>

  <p>Para identificar o endereço MAC de seu próprio hardware de rede:</p>

  <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Rede</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Rede</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Escolha o dispositivo, <gui>Wi-Fi</gui> ou <gui>Com fio</gui>, no painel à esquerda.</p>
      <p>O endereço MAC do dispositivo com fio será exibido como o <gui>Endereço físico</gui> à direita.</p>
      
      <p>Clique no botão <media its:translate="no" type="image" src="figures/emblem-system.png"><span its:translate="yes">configurações</span></media> para ver o endereço MAC do dispositivo sem fio exibido como o <gui>Endereço físico</gui> no painel <gui>Detalhes</gui>.</p>
    </item>
  </steps>

  <p>Na prática, você pode precisar modificar ou “burlar” um endereço MAC. Por exemplo, alguns provedores de serviço de Internet podem exigir que um endereço MAC específico seja usado para acessar seus serviços. Se a placa de rede parar de funcionar e você precisar trocar por uma nova placa, o serviço não vai mais funcionar. Em tais casos, você precisaria burlar o endereço MAC.</p>

</page>
