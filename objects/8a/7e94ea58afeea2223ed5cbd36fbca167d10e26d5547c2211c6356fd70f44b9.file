<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="keyboard-osk" xml:lang="ca">

  <info>
    <link type="guide" xref="keyboard" group="a11y"/>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>

    <credit type="author">
      <name>Jeremy Bicha</name>
      <email>jbicha@ubuntu.com</email>
    </credit>
    <credit type="author">
      <name>Julita Inca</name>
      <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Utilitzeu un teclat en pantalla per introduir text fent clic als botons amb el ratolí o amb una pantalla tàctil.</desc>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

  <title>Utilitzar un teclat en pantalla</title>

  <p>Si no té un teclat connectat a l'ordinador o prefereix no utilitzar-lo, pot activar el <em>teclat en pantalla</em> per introduir text.</p>

  <note>
    <p>El teclat en pantalla s'activa automàticament si feu servir una pantalla tàctil</p>
  </note>

  <steps>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a teclejar <gui>Paràmetres</gui>.</p>
    </item>
    <item>
      <p>Féu clic a <gui>Paràmetres</gui>.</p>
    </item>
    <item>
      <p>Feu clic a <gui>Accés universal</gui> en la barra lateral per obrir el quadre.</p>
    </item>
    <item>
      <p>Canvieu a <gui>Teclat en pantalla</gui> a la secció <gui>Escriptura</gui>.</p>
    </item>
  </steps>

  <p>Quan després tingueu l'oportunitat d'escriure, el teclat en pantalla s'obrirà a la part inferior de la pantalla.</p>

  <p>Premeu els botons <gui style="button">123</gui> per introduir números i símbols. Hi ha més símbols disponibles si premeu el botó <gui style="button">/&lt;</gui>. Per a tornar al teclat alfabètic, premeu el botó <gui style="button">ABC</gui>.</p>

  <p>Podeu prémer el botó <gui style="button"><media its:translate="no" type="image" src="figures/go-down-symbolic.svg" width="16" height="16"><span its:translate="yes">avall</span></media></gui> per amagar el teclat temporalment. El teclat tornarà a mostrar-se automàticament quan feu clic a sobre un lloc on pugueu utilitzar-lo.</p>
  <p>Premeu el botó <gui style="button"><media its:translate="no" type="image" src="figures/emoji-flags-symbolic.svg" width="16" height="16"><span its:translate="yes">bandera</span></media></gui> per canviar la configuració d'<link xref="session-language">Idioma</link> o <link xref="-layouts">Font d'entrada</link>.</p>
  <!-- obsolete <p>To make the keyboard show again, open the
  <link xref="shell-notifications">messagetray</link> (by moving your mouse to
  the bottom of the screen), and press the keyboard icon.</p> -->

</page>
