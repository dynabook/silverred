<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-purge" xml:lang="sv">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.10" date="2013-09-29" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shaun McCance</name>
      <email>mdhillca@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ställ in hur ofta din papperskorg och tillfälliga filer kommer att rensas bort från din dator.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Töm papperskorgen &amp; ta bort tillfälliga filer</title>

  <p>Att rensa ut din papperskorg och tillfälliga filer, ta bort oönskade och onödiga filer från din dator och frigör också mer utrymme på din hårddisk. Du kan tömma din papperskorg och rensa ut tillfälliga filer manuellt, men du kan också ställa in din dator att automatisk göra detta åt dig.</p>

  <p>Temporära filer är filer som automatiskt skapas i bakgrunden av program. De kan öka prestanda genom att tillhandahålla en kopia av data som hämtats ner eller beräknats.</p>

  <steps>
    <title>Töm automatiskt din papperskorg och rensa ut tillfälliga filer</title>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Sekretessinställningar</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Sekretessinställningar</gui> för att öppna panelen.</p>
    </item>
    <item>
      <p>Välj <gui>Rensa papperskorgen &amp; tillfälliga filer</gui>.</p>
    </item>
    <item>
      <p>Slå på den ena eller båda av <gui>Töm automatiskt papperskorgen</gui> och <gui>Rensa automatiskt tillfälliga filer</gui>.</p>
    </item>
    <item>
      <p>Ställ in hur ofta du vill att din <em>Papperskorg</em> och <em>Tillfälliga filer</em> ska rensas ut genom att ändra värdet <gui>Rensa efter</gui>.</p>
    </item>
    <item>
      <p>Använd knapparna <gui>Töm papperskorgen</gui> eller <gui>Ta bort tillfälliga filer</gui> för att göra dessa åtgärder omedelbart.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Du kan ta bort filer omedelbart och permanent utan att använda Papperskorgen. Se <link xref="files-delete#permanent"/> för vidare information.</p>
  </note>

</page>
