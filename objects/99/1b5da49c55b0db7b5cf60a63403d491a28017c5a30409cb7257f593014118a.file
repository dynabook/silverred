<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-touchpad-click" xml:lang="fr">

  <info>
    <link type="guide" xref="mouse"/>

    <revision pkgversion="3.7" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-29" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013, 2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Cliquer, glisser et faire défiler en utilisant des tapotements et des mouvements sur le pavé tactile.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Clic, glissement et défilement avec le pavé tactile</title>

  <p>Vous pouvez cliquer, double-cliquer, glisser et faire défiler en utilisant uniquement le pavé tactile, sans l'aide de boutons accessoires.</p>

  <note>
    <p><link xref="touchscreen-gestures">Touchscreen gestures</link> are
    covered separately.</p>
  </note>

<section id="tap">
  <title>Tapotement pour le clic</title>

  <p>Vous pouvez tapoter le pavé tactile pour cliquer au lieu d'utiliser un bouton.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Mouse &amp; Touchpad</gui>.</p>
    </item>
    <item>
      <p>Cliquez sur <gui>Souris &amp; pavé tactile</gui>.</p>
    </item>
    <item>
      <p>In the <gui>Touchpad</gui> section, make sure the <gui>Touchpad</gui>
      switch is set to on.</p>
      <note>
        <p>La rubrique <gui>Pavé tactile</gui> n'est présente que si le système dispose d'un pavé tactile.</p>
      </note>
    </item>
   <item>
      <p>Switch the <gui>Tap to click</gui> switch to on.</p>
    </item>
  </steps>

  <list>
    <item>
      <p>Pour cliquer, tapotez sur le pavé tactile.</p>
    </item>
    <item>
      <p>Pour double-cliquer, tapotez deux fois.</p>
    </item>
    <item>
      <p>To drag an item, double-tap but don’t lift your finger after the
      second tap. Drag the item where you want it, then lift your finger to
      drop.</p>
    </item>
    <item>
      <p>Si le pavé tactile prend en charge les tapotements à plusieurs doigts, vous pouvez faire un clic droit en tapant simultanément à deux doigts. Sinon, il est quand même nécessaire de faire appel à un bouton matériel pour le clic droit. Consultez <link xref="a11y-right-click"/> pour une méthode de clic droit sans deuxième bouton de souris.</p>
    </item>
    <item>
      <p>Si le pavé tactile prend en charge les tapotements à plusieurs doigts, on peut faire un <link xref="mouse-middleclick">clic milieu</link> en tapant simultanément avec trois doigts.</p>
    </item>
  </list>

  <note>
    <p>When tapping or dragging with multiple fingers, make sure your fingers
    are spread far enough apart. If your fingers are too close, your computer
    may think they’re a single finger.</p>
  </note>

</section>

<section id="twofingerscroll">
  <title>Défilement à deux doigts</title>

  <p>Il est possible de faire défiler avec le pavé tactile en utilisant deux doigts.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Mouse &amp; Touchpad</gui>.</p>
    </item>
    <item>
      <p>Cliquez sur <gui>Souris &amp; pavé tactile</gui>.</p>
    </item>
    <item>
      <p>In the <gui>Touchpad</gui> section, make sure the <gui>Touchpad</gui>
      switch is set to on.</p>
    </item>
    <item>
      <p>Switch the <gui>Two-finger Scrolling</gui> switch to on.</p>
    </item>
  </steps>

  <p>Avec cette option, le tapotement et le glissement à un doigt fonctionne comme normalement, mais si vous faites glisser deux doigts à n'importe quel endroit du pavé tactile, l'ordinateur effectue un défilement. Si l'option <gui>Activer le défilement horizontal</gui> est aussi activée, vous pouvez bougez vos doigts vers la gauche ou la droite pour défiler horizontalement. Prenez soin d'espacer suffisamment vos doigts. S'ils sont trop proches, le pavé tactile considère qu'il ne s'agit que d'un gros doigt.</p>

  <note>
    <p>Certains pavés tactiles ne prennent pas en charge le défilement à deux doigts.</p>
  </note>

</section>

<section id="contentsticks">
  <title>Défilement naturel</title>

  <p>Vous pouvez glisser un contenu, en utilisant le pavé tactile, comme si vous faisiez glisser un morceau de papier.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Mouse &amp; Touchpad</gui>.</p>
    </item>
    <item>
      <p>Cliquez sur <gui>Souris &amp; pavé tactile</gui>.</p>
    </item>
    <item>
      <p>In the <gui>Touchpad</gui> section, make sure that the
     <gui>Touchpad</gui> switch is set to on.</p>
    </item>
    <item>
      <p>Switch the <gui>Natural Scrolling</gui> switch to on.</p>
    </item>
  </steps>

  <note>
    <p>Cette fonction est aussi appelée <em>Défilement inverse</em>.</p>
  </note>

</section>

</page>
