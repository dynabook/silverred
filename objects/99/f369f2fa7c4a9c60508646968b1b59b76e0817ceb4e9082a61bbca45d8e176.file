<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="power-dim-screen" xml:lang="de">

  <info>
    <link type="guide" xref="user-settings"/>
    <link type="seealso" xref="dconf-profiles"/>
    <link type="seealso" xref="dconf-lockdown"/>
    <revision pkgversion="3.12" date="2014-06-20" status="candidate"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>

    <desc>Den Bildschirm nach einer bestimmten Zeitspanne abdunkeln, wenn der Benutzer untätig ist.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2017, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

  <title>Den Bildschirm abdunkeln, wenn der Benutzer untätig ist</title>

  <p>Sie können den Bildschirm des Rechners abdunkeln, nachdem er für eine bestimmte Zeitspanne untätig war.</p>

  <steps>
    <title>Bildschirm eines untätigen Rechners abdunkeln</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
    <item>
      <p>Erstellen Sie die Schlüsseldatei <file>/etc/dconf/db/local.d/00-power</file>, um Informationen für die <sys>local</sys>-Datenbank bereitzustellen.</p>
      <listing>
        <title><file>/etc/dconf/db/local.d/00-power</file></title>
<code>
# Den dconf-Pfad festlegen
[org/gnome/settings-daemon/plugins/power]

# Bildschirmabdunklung aktivieren
idle-dim=true

# Helligkeit im abgedunkelten Zustand festlegen
idle-brightness=30
</code>
      </listing>
    </item>
    <item>
      <p>Erstellen Sie die Schlüsseldatei <file>/etc/dconf/db/local.d/00-session</file>, um Informationen für die <sys>local</sys>-Datenbank bereitzustellen.</p>
      <listing>
        <title><file>/etc/dconf/db/local.d/00-session</file></title>
<code>
# Den dconf-Pfad festlegen
[org/gnome/desktop/session]

# Sekunden der Inaktivität, bevor die Sitzung als untätig betrachtet wird
idle-delay=uint32 300
</code>
      </listing>
      <p>Sie müssen <code>uint32</code> zusammen mit dem ganzzahligen Schlüssel angeben, wie angezeigt.</p>
    </item>
    <item>
      <p>Um den Benutzer daran zu hindern, diese Einstellungen zu überschreiben, erstellen Sie die Datei <file>/etc/dconf/db/local.d/locks/power-saving</file> mit dem folgenden Inhalt:</p>
      <listing>
        <title><file>/etc/dconf/db/local.db/locks/power-saving</file></title>
<code>
# Bildschirmabdunklung Untätigkeits-Zeitüberschreitung sperren
/org/gnome/settings-daemon/plugins/power/idle-dim
/org/gnome/settings-daemon/plugins/power/idle-brightness
/org/gnome/desktop/session/idle-delay
</code>
      </listing>
      <p>Wenn Sie den Benutzer diese Einstellungen ändern lassen wollen, überspringen Sie diesen Schritt.</p>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
  </steps>

</page>
