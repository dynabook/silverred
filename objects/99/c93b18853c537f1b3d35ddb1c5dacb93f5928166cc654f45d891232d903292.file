<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="a11y task" id="a11y-screen-reader" xml:lang="sv">

  <info>
    <link type="guide" xref="a11y#vision" group="blind"/>

    <revision pkgversion="3.13.92" date="2014-09-20" status="incomplete"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Jana Heves</name>
      <email>jsvarova@gnome.org</email>
    </credit>

    <desc>Använd skärmläsaren <app>Orca</app> för att läsa upp användargränssnittet.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Högläsning av skärmen</title>

  <p>GNOME tillhandahåller skärmläsaren <app>Orca</app> för att läsa upp användargränssnittet. Beroende på hur du har installerat GNOME så kan det vara så att du inte har Orca installerat. Om inte, installera Orca först.</p>

  <p if:test="action:install"><link style="button" action="install:orca">Installera Orca</link></p>
  
  <p>För att starta <app>Orca</app> med tangentbordet:</p>
  
  <steps>
    <item>
    <p>Tryck <key>Super</key>+<key>Alt</key>+<key>S</key>.</p>
    </item>
  </steps>
  
  <p>Eller för att starta <app>Orca</app> med en mus och tangentbordet:</p>

  <steps>
    <item>
      <p>Öppna <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Hjälpmedel</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Hjälpmedel</gui> för att öppna panelen.</p>
    </item>
    <item>
      <p>Klicka på <gui>Skärmläsare</gui> i avsnittet <gui>Se</gui>, slå sedan på <gui>Skärmläsare</gui> i dialogen.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Aktivera och inaktivera skärmläsare snabbt</title>
    <p>Du kan aktivera eller inaktivera skärmläsare genom att klicka på <link xref="a11y-icon">hjälpmedelsikonen</link> i systemraden och välja <gui>Skärmläsare</gui>.</p>
  </note>

  <p>Se vidare i <link href="help:orca">Hjälp för Orca</link> för ytterligare detaljer.</p>
</page>
