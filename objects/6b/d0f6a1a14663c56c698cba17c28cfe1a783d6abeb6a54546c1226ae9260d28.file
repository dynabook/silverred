<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-calibrationdevices" xml:lang="el">

  <info>

    <link type="guide" xref="color#calibration"/>

    <desc>Υποστηρίζονται μεγάλος αριθμός συσκευών βαθμονόμησης.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Ποια όργανα μέτρησης χρώματος υποστηρίζονται;</title>

  <p>Το GNOME βασίζεται στο σύστημα διαχείρισης χρώματος Argyll για υποστήριξη οργάνων χρώματος. Έτσι υποστηρίζονται τα ακόλουθα όργανα μέτρησης εμφάνισης:</p>

  <list>
    <item><p>Gretag-Macbeth i1 Pro (φασματόμετρο)</p></item>
    <item><p>Gretag-Macbeth i1 Monitor (φασματόμετρο)</p></item>
    <item><p>Gretag-Macbeth i1 Display 1, 2 ή LT (χρωματόμετρο)</p></item>
    <item><p>X-Rite i1 Display Pro (χρωματόμετρο)</p></item>
    <item><p>X-Rite ColorMunki σχεδίαση ή φωτογραφία (φασματόμετρο)</p></item>
    <item><p>X-Rite ColorMunki Create (χρωματόμετρο)</p></item>
    <item><p>X-Rite ColorMunki Display (χρωματόμετρο)</p></item>
    <item><p>Pantone Huey (χρωματόμετρο)</p></item>
    <item><p>MonacoOPTIX (χρωματόμετρο)</p></item>
    <item><p>ColorVision Spyder 2 και 3 (χρωματόμετρο)</p></item>
    <item><p>Colorimètre HCFR (χρωματόμετρο)</p></item>
  </list>

  <note style="tip">
   <p>Το Pantone Huey είναι τώρα το πιο φτηνό και άριστα υποστηριζόμενο υλικό στο Linux.</p>
  </note>

  <p>
    Thanks to Argyll there’s also a number of spot and strip reading
    reflective spectrometers supported to help you calibrating and
    characterizing your printers:
  </p>

  <list>
    <item><p>X-Rite DTP20 “Pulse” (“swipe” type reflective spectrometer)</p></item>
    <item><p>X-Rite DTP22 Digital Swatchbook (σημείο τύπου ανακλαστικού φασματομέτρου)</p></item>
    <item><p>X-Rite DTP41 (ανάγνωσης σημείου και λουρίδας ανακλαστικού φασματομέτρου)</p></item>
    <item><p>X-Rite DTP41T (ανάγνωσης σημείου και λουρίδας ανακλαστικού φασματομέτρου)</p></item>
    <item><p>X-Rite DTP51 (ανάγνωσης σημείου ανακλαστικού φασματομέτρου)</p></item>
  </list>

</page>
