<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="shell-exit" xml:lang="hr">

  <info>
    <link type="guide" xref="shell-overview"/>
    <link type="guide" xref="power"/>
    <link type="guide" xref="index" group="#first"/>

    <revision pkgversion="3.6.0" date="2012-09-15" status="review"/>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.24.2" date="2017-06-11" status="candidate"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="author">
      <name>Alexandre Franke</name>
      <email>afranke@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David Faour</name>
      <email>dfaour.gnome@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Naučite kako napustiti svoj korisnički račun, odjavom, zamjenom korisnika, itd.</desc>
    <!-- Should this be a guide which links to other topics? -->
  </info>

  <title>Odjava, isključivanje i zamjena korisnika</title>

  <p>Kada završite s korištenjem svojeg računala možete ga isključiti, suspendirati (kako bi uštedjeli energiju) ili ga ostaviti uključenim i odjaviti se.</p>

<section id="logout">
  <title>Odjava ili zamjena korisnika</title>

  <p>Kako bi dopustili drugim korisnicima korištenje računala, možete se ili odjaviti ili ostati prijavljeni i jednostavno zamijeniti korisnika. Ako zamijenite korisnika, sve vaše aplikacije će nastaviti s radom i sve će biti u istome stanju kada se prijavite ponovno.</p>

  <p>Kako bi se <gui>Odjavili</gui> ili <gui>Zamijenili korisnika</gui>, kliknite na <link xref="shell-introduction#systemmenu">izbornik sustava</link> na desnoj strani gornje trake, kliknite na svoje ime i zatim odaberite prikladnu mogućnost.</p>

  <note if:test="!platform:gnome-classic">
    <p>Stavke <gui>Odjava</gui> i <gui>Zamjena korisnika</gui> pojavljuju se u izborniku samo ako imate više od jednog korisničkog računa na sustavu.</p>
  </note>

  <note if:test="platform:gnome-classic">
    <p>Stavka <gui>Zamijeni korisnika</gui> pojavljuju se u izborniku samo ako imate više od jednog korisničkog računa na sustavu.</p>
  </note>

</section>

<section id="lock-screen">
  <info>
    <link type="seealso" xref="session-screenlocks"/>
  </info>

  <title>Zaključavanje zaslona</title>

  <p>Ako napuštate svoje računalo samo nakratko, trebali biste zaključati svoj zaslon kako bi spriječili druge osobe u pristupu vašim datotekama i pokrenutim aplikacijama. Kada se vratite, podignite zastor <link xref="shell-lockscreen">zaslona zaključavanja</link> i upišite svoju lozinku za ponovnu prijavu. Ako niste zaključali svoj zaslon, zaključati će se nakon određenog vremena.</p>

  <p>Kako bi zaključali svoj zaslon, kliknite na izbornik sustava desno u gornjoj traci i pritisnite tipku zaključavanja zaslona na dnu izbornika.</p>

  <p>Kada je vaš zaslon zaključan, drugi korisnici se mogu prijaviti u svoje račune klikom na <gui>Prijavi se kao drugi korisnik</gui> na zaslonu lozinke. Možete se vratiti natrag u svoju radnu površinu kada drugi korisnik prestane s radom.</p>

</section>

<section id="suspend">
  <info>
    <link type="seealso" xref="power-suspend"/>
  </info>

  <title>Suspenzija</title>

  <p>Kako bi štedjeli energiju, suspendirajte svoje računalo kada ga ne koristite. Ako koristite prijenosno računalo, GNOME po zadanome, automatski suspendira vaše računalo kada spustite poklopac zaslona. To sprema stanje vašeg računala u memoriju i isključuje većinu funkcija računala. Vrlo mala količina energije se koristi tijekom suspenzije.</p>

  <p>Kako bi suspendirali svoje računalo ručno, kliknite na izbornik sustava desno u gornjoj traci. Iz izbornika možete ili držati pritisnutu <key>Alt</key> tipku i kliknuti na tipku isključivanja, ili jednostavno dulje kliknuti na tipku isključivanja.</p>

</section>

<section id="shutdown">
<!--<info>
  <link type="seealso" xref="power-off"/>
</info>-->

  <title>Isključivanje ili ponovno pokretanje</title>

  <p>Ako želite u potpunosti isključiti svoje računalo ili ga ponovno pokrenuti, kliknite na izbornik sustava desno u gornjoj traci i pritisnite tipku isključivanja na dnu izbornika. Otvoriti će se dijalog koji vam nudi mogućnosti <gui>Ponovnog pokretanja</gui> ili <gui>Isključivanja</gui>.</p>

  <p>Ako postoje drugi prijavljeni korisnici, možda vam neće biti dopušteno isključivanje ili ponovno pokretanje računala zato jer bi se tada završile njihove prijave. Ako ste administrativni korisnik, možda ćete biti upitani za upis lozinke kako bi isključili računalo.</p>

  <note style="tip">
    <p>Možda želite isključiti svoje računalo ako se želite premjestiti a nemate dovoljno energije u bateriji, ako vaša baterija nema dovoljno energije i ne puni se ispravno. Isključeno računalo koristi <link xref="power-batterylife">manje energije</link> nego kada je suspendirano.</p>
  </note>

</section>

</page>
