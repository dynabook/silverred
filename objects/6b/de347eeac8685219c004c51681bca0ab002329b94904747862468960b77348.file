<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="dconf-lockdown" xml:lang="de">

  <info>
    <link type="guide" xref="user-settings#lockdown"/>
    <link type="guide" xref="setup"/>
    <link type="seealso" xref="dconf"/>
    <link type="seealso" xref="dconf-profiles"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Ryan Lortie</name>
      <email>desrt@desrt.ca</email>
      <years>2012</years>
    </credit>
    <credit type="copyright editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013, 2015</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Verwenden Sie den <em>Sperrmodus</em> in <sys its:translate="no">dconf</sys>, um Benutzer an der Änderung spezifischer Einstellungen zu hindern.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2017, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

  <title>Spezifische Einstellungen sperren</title>

  <p>Mit Hilfe des Sperrmodus in Dconf können Sie Benutzer daran hindern, ihre spezifischen Einstellungen zu ändern. Wenn die Systemeinstellungen nicht gesperrt sind, werden Benutzereinstellungen gegenüber den Systemeinstellungen bevorzugt.</p>

  <p>Um einen <sys its:translate="no">dconf</sys>-Schlüssel oder Unterpfad zu <em>sperren</em>, müssen Sie einen <file its:translate="no">locks</file>-Unterordner im Schlüsseldatei-Ordner anlegen. Die Dateien in diesem Ordner enthalten eine Liste der zu sperrenden Schlüssel oder Unterpfade. Wie bei <link xref="dconf-keyfiles">Schlüsseldateien</link> auch können Sie eine beliebige Anzahl Dateien in diesem Ordner ablegen.</p>

  <steps>
    <title>Eine Einstellung sperren</title>
    <item>
      <p>Bevor Sie einen Schlüssel oder Unterpfad sperren können, müssen Sie ihn erst setzen. Dieses Beispiel zeigt, wie Sie eine <link xref="desktop-background">Hintergrundeinstellung</link> sperren können, sobald sie gesetzt ist.</p>

      <p>Bevor Sie einen Schlüssel sperren können, müssen Sie ihn erst setzen. Dieses Beispiel zeigt, wie Sie eine <link xref="desktop-background">Hintergrundeinstellung</link> sperren können, sobald sie gesetzt ist. Dazu sollten Sie ein <link xref="dconf-profiles"> <sys>user</sys>-Profil</link> und eine <link xref="dconf-keyfiles">Schlüsseldatei</link> mit den Einstellungen haben, die Sie sperren wollen.</p>
    </item>
    <item>
      <p>Legen Sie einen Ordner namens <file its:translate="no">/etc/dconf/db/local.d/locks</file> an.</p>
    </item>
    <item>
      <p>Erstellen Sie eine Datei im Ordner <file its:translate="no">/etc/dconf/db/local.d/locks/</file> und schreiben Sie jeden Schlüssel oder Unterpfad in eine eigene Zeile. Zum Beispiel erstellen Sie <file its:translate="no">/etc/dconf/db/local.d/locks/00_default-wallpaper</file> wie folgt:</p>
<code its:translate="no">
# <span its:translate="yes">Änderungen des Hintergrundbildes unterbinden</span>
/org/gnome/desktop/background/picture-uri
/org/gnome/desktop/background/picture-options
/org/gnome/desktop/background/primary-color
/org/gnome/desktop/background/secondary-color
</code>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  </steps>

</page>
