<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="sound-volume" xml:lang="es">

  <info>
    <link type="guide" xref="media#sound"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ajustar el volumen del sonido para el equipo y controlar el volumen de cada aplicación.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>Cambiar el volumen del sonido</title>

  <p>Para cambiar el volumen del sonido, abra el <gui xref="shell-introduction#systemmenu">menú del sistema</gui> en el lado derecho de la barra superior y mueva el control deslizante de volumen a la derecha o a la izquierda. Puede desactivar completamente el sonido llevando el control al extremo izquierdo.</p>

  <p>Algunos teclados tienen teclas que le permiten controlar el volumen. Normalmente representan altavoces estilizados emitiendo «ondas» y frecuentemente están cerca de las teclas «F» en la parte superior. En los portátiles, normalmente están en las teclas «F». Para usarles mantenga pulsada la tecla <key>Fn</key> en su teclado.</p>

  <p>Si tiene altavoces externos, también puede cambiar el volumen con el control de volumen en los propios altavoces. Algunos auriculares tienen también un control de volumen.</p>

<section id="apps">
 <title>Cambiar el volumen del sonido para aplicaciones individuales</title>

  <p>Puede cambiar el volumen de una aplicación, pero dejar el volumen de las demás sin cambios. Esto es útil si está escuchando música y navegando por la web, por ejemplo; es posible que quiera desactivar el volumen en el navegador web para que los sonidos de las páginas web no interrumpan la música.</p>

  <p>Algunas aplicaciones tienen controles de volumen en su ventana principal. Si su aplicación lo tiene, úselo para cambiar el volumen. Si no lo tiene:</p>

    <steps>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Sonido</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Sonido</gui> para abrir el panel.</p>
    </item>
    <item>
      <p>En <gui>Niveles de volumen</gui> cambie el volumen de la aplicación mostrada aquí.</p>

  <note style="tip">
    <p>Solo se listan las aplicaciones que están reproduciendo sonidos. Si una aplicación está reproduciendo sonidos, pero no aparece, es posible que no admita la característica que le permite controlar el volumen de esta manera. En ese caso, no puede cambiar su volumen.</p>
  </note>
    </item>

  </steps>

</section>

</page>
