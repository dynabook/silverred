<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="backup-how" xml:lang="sl">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit>
      <name>Dokumentacijski projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Uporaba Déjà Dup (ali katerega drugega programa za izdelavo varnostnih kopij) za izdelavo kopij vrednih datotek in storitev za zaščito pred izgubo podatkov.</desc>
  </info>

<title>Kako ustvariti varnostno kopijo</title>

  <p>Najlažji način ustvarjanja varnostne kopije datotek in nastavitev je, da programu varnostnih kopij pustite upravljanje opravil varnostnih kopij. Na voljo so številni programi varnostnih kopij, na primer <app>Déjà Dup</app>.</p>

  <p>Pomoč za vaš izbrani program vas bo vodila skozi nastavljanje možnosti varnostne kopije in tudi skozi obnovitev podatkov, če pride do težav.</p>

  <p>Druga možnost je enostavno <link xref="files-copy">kopiranje vaših datotek</link> na varno mesto kot je zunanji trdi disk, drug računalnik na omrežju ali pogon USB. Vaše <link xref="backup-thinkabout">osebne datoteke</link> in nastavitve so ponavadi v vaši domači mapi, zato jih lahko kopirate od tam.</p>

  <p>Količina podatkov katerih varnostno kopijo lahko ustvarite je omejena z velikostjo naprave shrambe. V primeru da imate na svoji napravi varnostne kopije prostor, je najboljše narediti varnostno kopijo cele domače mape z naslednjimi izjemami:</p>

<list>
 <item><p>Datoteke, ki že imajo varnostne kopije drugje, kot na primer na CD-ju, DVD-ju ali drugih odstranljivih medijih.</p></item>
 <item><p>Files that you can recreate easily. For example, if you are a
 programmer, you do not have to back up the files that get produced when you
 compile your programs. Instead, just make sure that you back up the original
 source files.</p></item>
 <item><p>Vaše datoteke smeti, ki jih lahko najdete v <file>~/.local/share/Trash</file> (mapa smeti).</p></item>
</list>

</page>
