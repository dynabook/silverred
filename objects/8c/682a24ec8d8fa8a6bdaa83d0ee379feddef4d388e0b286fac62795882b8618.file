<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="color-whyimportant" xml:lang="el">

  <info>
    <link type="guide" xref="color"/>
    <desc>Η διαχείριση χρώματος είναι σημαντική για σχεδιαστές, φωτογράφους και καλλιτέχνες.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Γιατί είναι σημαντική η διαχείριση του χρώματος;</title>
  <p>Η διαχείριση χρώματος είναι η διαδικασία λήψης ενός χρώματος χρησιμοποιώντας μια συσκευή εισόδου, η εμφάνιση της σε μια οθόνη και η εκτύπωσή της ενώ διαχειρίζεστε τα ακριβή χρώματα και την περιοχή των χρωμάτων σε κάθε μέσο.</p>

  <p>Η ανάγκη για χρωματική διαχείριση εξηγείται προφανώς άριστα με μια φωτογραφία ενός πουλιού σε πια παγωμένη ημέρα του χειμώνα.</p>

  <figure>
    <desc>Ένα πουλί σε έναν παγωμένο τοίχο όπως φαίνεται στην κάμερα εύρεσης προβολής</desc>
    <media its:translate="no" type="image" mime="image/png" src="figures/color-camera.png"/>
  </figure>

  <p>Εμφανίζει τυπικά υπερκορεσμένο το γαλάζιο κανάλι, κάνοντας τις εικόνες να δείχνουν κρύες.</p>

  <figure>
    <desc>Αυτό βλέπει ο χρήστης σε μια τυπική οθόνη επαγγελματικού φορητού υπολογιστή</desc>
    <media its:translate="no" type="image" mime="image/png" src="figures/color-display.png"/>
  </figure>

  <p>
    Notice how the white is not “paper white” and the black of the eye
    is now a muddy brown.
  </p>

  <figure>
    <desc>Αυτό βλέπει ο χρήστης όταν τυπώσει σε έναν τυπικό εκτυπωτή ψεκασμού</desc>
    <media its:translate="no" type="image" mime="image/png" src="figures/color-printer.png"/>
  </figure>

  <p>Το βασικό πρόβλημα που έχουμε εδώ είναι ότι κάθε συσκευή μπορεί να χειριστεί μια διαφορετική περιοχή χρωμάτων. Έτσι ενώ μπορείτε να πάρετε μια φωτογραφία ηλεκτρικού γαλάζιου, οι περισσότεροι εκτυπωτές δεν πρόκειται να μπορέσουν να το αναπαράγουν.</p>
  <p>
    Most image devices capture in RGB (Red, Green, Blue) and have
    to convert to CMYK (Cyan, Magenta, Yellow, and Black) to print.
    Another problem is that you can’t have <em>white</em> ink, and so
    the whiteness can only be as good as the paper color.
  </p>

  <p>
    Another problem is units. Without specifying the scale on which a
    color is measured, we don’t know if 100% red is near infrared or
    just the deepest red ink in the printer. What is 50% red on one
    display is probably something like 62% on another display.
    It’s like telling a person that you’ve just driven 7 units of
    distance, without the unit you don’t know if that’s 7 kilometers or
    7 meters.
  </p>

  <p>
    In color, we refer to the units as gamut. Gamut is essentially the
    range of colors that can be reproduced.
    A device like a DSLR camera might have a very large gamut, being able
    to capture all the colors in a sunset, but a projector has a very
    small gamut and all the colors are going to look “washed out”.
  </p>

  <p>
    In some cases we can <em>correct</em> the device output by altering
    the data we send to it, but in other cases where that’s not
    possible (you can’t print electric blue) we need to show the user
    what the result is going to look like.
  </p>

  <p>
    For photographs it makes sense to use the full tonal range of a color
    device, to be able to make smooth changes in color.
    For other graphics, you might want to match the color exactly, which
    is important if you’re trying to print a custom mug with the Red Hat
    logo that <em>has</em> to be the exact Red Hat Red.
  </p>

</page>
