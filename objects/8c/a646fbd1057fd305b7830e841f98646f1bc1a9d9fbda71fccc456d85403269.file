<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-suspend" xml:lang="hu">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspendfail"/>

    <desc>A felfüggesztés alvó állapotba helyezi számítógépét, így az kevesebb energiát használ.</desc>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>GNOME dokumentációs projekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

<title>Mi történik a számítógép felfüggesztésekor?</title>

<p>A számítógép <em>felfüggesztésekor</em> alvó állapotba helyezi a számítógépet. Minden alkalmazása és dokumentuma nyitva marad, de a képernyő és a számítógép más alkatrészei az energiatakarékosság érdekében kikapcsolnak. A számítógép azonban bekapcsolva marad, és egy kevés áramot fogyaszt, és egy billentyű lenyomásával vagy egy egérkattintással felébresztheti. Ha ez nem történik meg, próbálja megnyomni a bekapcsológombot.</p>

<p>Egyes számítógépek hardvere nem megfelelően támogatott, ami azt jelenti, hogy ezeken <link xref="power-suspendfail">a felfüggesztés nem működik</link>. Javasoljuk, hogy tesztelje le a számítógép felfüggesztését, mielőtt ráhagyatkozik.</p>

<note style="important">
  <title>Mindig mentse munkáját felfüggesztés előtt</title>
  <p>A számítógép felfüggesztése előtt mindig mentse dokumentumait, hogy akkor se veszítse el az aktuális változtatásokat, ha egy hiba miatt az alkalmazások és dokumentumok nem lesznek helyreállíthatók a számítógép használatának folytatásakor.</p>
</note>

</page>
