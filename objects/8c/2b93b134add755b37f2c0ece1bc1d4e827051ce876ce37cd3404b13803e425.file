<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-multi-monitor" xml:lang="pt">

  <info>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.28" date="2018-07-22" status="review"/>
    <revision pkgversion="3.33" date="2019-07-21" status="candidate"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Michael Hilh</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Mapear a tableta Wacom a um ecrã específico.</desc>
  </info>

  <title>Eleger um ecrã</title>

<steps>
  <item>
    <p>Abra a vista de <gui xref="shell-introduction#activities">Atividades</gui> e comece a escrever <gui>Tablet Wacom</gui>.</p>
  </item>
  <item>
    <p>Carregue em <gui>Tablet Wacom</gui> para abrir o painel.</p>
  </item>
  <item>
    <p>Click the <gui>Tablet</gui> button in the header bar.</p>
    <note style="tip"><p>If no tablet is detected, you’ll be asked to
    <gui>Please plug in or turn on your Wacom tablet</gui>. Click the
    <gui>Bluetooth Settings</gui> link to connect a wireless tablet.</p></note>
  </item>
  <item><p>Carregue <gui>Mapa que monitorar…</gui>.</p></item>
  <item><p>Marque <gui>Mapear um único ecrã</gui>.</p></item>
  <item><p>Junto a <gui>Saída</gui>, selecione o ecrã da que quer receber a entrada de a sua tableta gráfica.</p>
     <note style="tip"><p>So se podem selecionar os ecrãs que estão configuradas.</p></note>
  </item>
  <item>
    <p>Switch the <gui>Keep aspect ratio (letterbox)</gui> switch to on to
    match the drawing area of the tablet to the proportions of the monitor.
      This setting, also called <em>force proportions</em>,
      “letterboxes” the drawing area on a tablet to correspond more
      directly to a display. For example, a 4∶3 tablet would be mapped so that
      the drawing area would correspond to a widescreen display.</p>
  </item>
  <item><p>Carregue <gui>Fechar</gui>.</p></item>
</steps>

</page>
