<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-suspend" xml:lang="ru">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspendfail"/>

    <desc>В ждущем режиме компьютер «засыпает» и потребляет меньшую мощность.</desc>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Что происходит при переходе компьютера в ждущий режим?</title>

<p>Когда компьютер переходит в <em>ждущий режим</em>, он «засыпает». Все открытые приложения и документы остаются открытыми, но экран и другие части компьютера отключаются, для уменьшения энергопотребления. Тем не менее, компьютер всё равно остаётся включенным и потребляет небольшое количество электроэнергии. Его можно вернуть в рабочий режим нажатием клавиши или щелчком мыши. Если это не действует, попробуйте нажать кнопку питания.</p>

<p>Some computers have problems with hardware support which mean that they
<link xref="power-suspendfail">may not be able to suspend properly</link>. It is
a good idea to test suspend on your computer to see if it does work before
relying on it.</p>

<note style="important">
  <title>Всегда сохраняйте свою работу перед переводом компьютера в ждущий режим</title>
  <p>Настоятельно рекомендуется сохранять всю выполненную работу перед переводом компьютера в ждущий режим, на случай, если что-то пойдёт не так, и открытые приложения и документы не будут восстановлены при возвращении компьютера в рабочий режим.</p>
</note>

</page>
