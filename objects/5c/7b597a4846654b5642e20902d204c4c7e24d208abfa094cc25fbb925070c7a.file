<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-name-location" xml:lang="as">

  <info>
    <link type="guide" xref="printing#setup"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author copyright">
      <name>জানা চ্ভাৰ'ভা</name>
      <email>jana.svarova@gmail.com</email>
      <years>২০১৩</years>
    </credit>
    <credit type="editor">
      <name>জিম কেম্পবেল</name>
      <email>jcampbell@gnome.org</email>
      <years>২০১৩</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>প্ৰিন্টাৰ সংহতিসমূহত এটা প্ৰিন্টাৰ নাম অথবা অৱস্থান পৰিবৰ্তন কৰক।</desc>
  </info>
  
  <title>এটা প্ৰিন্টাৰৰ নাম অথবা অৱস্থান পৰিবৰ্তন কৰক</title>

  <p>আপুনি প্ৰিন্টাৰ সংহতিসমূহত এটা প্ৰিন্টাৰৰ নাম অথবা অৱস্থান পৰিবৰ্তন কৰিব পাৰিব।</p>

  <note>
    <p>You need <link xref="user-admin-explain">administrative privileges</link>
    on the system to change the name or location of a printer.</p>
  </note>

  <section id="printer-name-change">
    <title>প্ৰিন্টাৰৰ নাম পৰিবৰ্তন কৰক</title>

  <p>যদি আপুনি এটা প্ৰিন্টাৰৰ নাম পৰিবৰ্তন কৰিব বিচাৰে, নিম্নলিখিত স্তৰ অনুকৰণ কৰক:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Printers</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Printers</gui> to open the panel.</p>
    </item>
    <item>
      <p>Press <gui style="button">Unlock</gui> in the top right corner and
      type in your password when prompted.</p>
    </item>
    <item>
      <p>Click the name of your printer, and start typing a new name for
      the printer.</p>
    </item>
    <item>
      <p>Press <key>Enter</key> to save your changes.</p>
    </item>
  </steps>

  </section>

  <section id="printer-location-change">
    <title>প্ৰিন্টাৰৰ অৱস্থান পৰিবৰ্তন কৰক</title>

  <p>আপোনাৰ প্ৰিন্টাৰৰ অৱস্থান পৰিবৰ্তন কৰিবলৈ:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Printers</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Printers</gui> to open the panel.</p>
    </item>
    <item>
      <p>Press <gui style="button">Unlock</gui> in the top right corner and
      type in your password when prompted.</p>
    </item>
    <item>
      <p>Click the location, and start editing the location.</p>
    </item>
    <item>
      <p>পৰিবৰ্তনসমূহ সংৰক্ষণ কৰিবলৈ <key>Enter</key> টিপক।</p>
    </item>
  </steps>

  </section>

</page>
