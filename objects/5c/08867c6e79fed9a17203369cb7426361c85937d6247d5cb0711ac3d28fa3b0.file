<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="units" xml:lang="de">
  <info>
    <revision version="0.2" pkgversion="3.11" date="2014-01-26" status="review"/>
    <link type="guide" xref="index#other" group="other"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author copyright">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
      <years>2011</years>
    </credit>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2011, 2014</years>
    </credit>

    <desc>Maßeinheiten für Arbeitsspeicher und Festplattenspeicher</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Ist GiB das gleiche wie GB (Gigabyte)?</title>

    <p>Die Statistiken für Arbeitsspeicher und Festplattenspeicher werden mit <em>IEC Binärvorsätzen</em>: KiB, MiB, GiB, TiB für (kibi, mebi, gibi und tebi) angegeben. Damit ist beabsichtigt zwischen den in der Systemüberwachung verwendeten binären Größenangaben (Vielfache von 1024) und den bei Festplatten gewöhnlich genutzten dezimalen Größenangaben (Vielfache von 1000) zu unterscheiden.</p>

    <p>Typische binäre Einheiten:</p>
  <list>
    <item><p>1 KiB = 1024 Byte</p></item>
    <item><p>1 MiB = 1048576 Byte</p></item>
    <item><p>1 GiB = 1073741842 Byte</p></item>
  </list>

    <p>Eine externe Festplatte, die mit 1.0 TB (Terabyte) beworben wird, würde mit 0.909 TiB (Tebibyte) angezeigt werden.</p>

</page>
