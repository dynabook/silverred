<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="printing-streaks" xml:lang="de">

  <info>
    <link type="guide" xref="printing#problems"/>
    <link type="seealso" xref="printing-inklevel"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Falls Ihre Ausdrucke Streifen aufweisen, Farben verblassen bzw. ganz fehlen, überprüfen Sie die Tintenfüllstände oder reinigen Sie den Druckkopf.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2019.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2019.</mal:years>
    </mal:credit>
  </info>

  <title>Warum sind auf meinen Ausdrucken Streifen, Linien oder falsche Farben zu sehen?</title>

  <p>Falls Ihre Ausdrucke Streifen aufweisen, dort Linien sind, wo keine sein sollten, oder die Qualität anderweitig gemindert wird, liegt entweder ein Problem mit dem Drucker vor oder die Tinten-/Tonerfüllstände sind niedrig.</p>

  <terms>
     <item>
       <title>Verblasster Text oder verblasste Bilder</title>
       <p>Ihr Tinten-/Tonervorrat könnte zur Neige gehen. Überprüfen Sie die Füllstände und kaufen Sie eine neue Kartusche, falls nötig.</p>
     </item>
     <item>
       <title>Streifen und Linien</title>
       <p>Wenn Sie einen Tintendrucker besitzen, kann der Druckkopf verschmutzt oder teilweise blockiert sein. Versuchen Sie den Druckkopf zu reinigen (Anweisungen zur Reinigung finden Sie im Handbuch des Druckers).</p>
     </item>
     <item>
       <title>Falsche Farben</title>
       <p>Der Tinten-/Tonervorrat einer Farbe könnte zur Neige gehen. Überprüfen Sie die Füllstände und kaufen Sie eine neue Kartusche, falls nötig.</p>
     </item>
     <item>
       <title>Gezackte oder ungerade Linien</title>
       <p>Wenn Linien auf Ihrem Ausdruck gezackt sind, obwohl sie gerade sein sollten, müssen Sie eventuell den Druckkopf neu ausrichten. Werfen Sie einen Blick in das Handbuch Ihres Druckers, um herauszufinden, wie Sie dazu vorgehen müssen.</p>
     </item>
  </terms>

</page>
