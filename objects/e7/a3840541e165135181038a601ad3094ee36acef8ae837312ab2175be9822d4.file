<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="problem" id="bluetooth-problem-connecting" xml:lang="fi">

  <info>
    <link type="guide" xref="bluetooth#problems"/>
    <link type="seealso" xref="hardware-driver"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Sovitin voi olla pois käytöstä, sillä ei ole ajureita tai Bluetooth voi olla poistettu käytöstä tai estetty.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

  <title>Bluetooth-laitteen yhdistäminen ei onnistu</title>

  <p>Bluetooth-laitteeseen (kuten puhelimeen tai kuulokemikrofoniin) yhdistämisen epäonnistuminen voi johtua useista eri syistä.</p>

  <terms>
    <item>
      <title>Yhteys on estetty tai yhteys ei ole luotettu</title>
      <p>Tietyt Bluetooth-laitteet estävät oletuksena yhteydet tai vaativat asetusten muuttamista yhteyden luomiseksi. Varmista, että laite on asetettu hyväksymään yhteydet.</p>
    </item>
    <item>
      <title>Bluetooth-sovitinta ei tunnistettu</title>
      <p>Your Bluetooth adapter or dongle may not have been recognized by the
      computer. This could be because
      <link xref="hardware-driver">drivers</link> for the adapter are not
      installed. Some Bluetooth adapters are not supported on Linux, so you may
      not be able to get the right drivers for them. In this case, you will
      probably have to get a different Bluetooth adapter.</p>
    </item>
    <item>
      <title>Sovitinta ei ole kytketty päälle</title>
        <p>Make sure that your Bluetooth adapter is switched on. Open the
        Bluetooth panel and check that it is not
        <link xref="bluetooth-turn-on-off">disabled</link>.</p>
    </item>
    <item>
      <title>Laitteen Bluetooth-yhteys ei ole käytössä</title>
      <p>Check that Bluetooth is turned on on the device you are trying to
      connect to, and that it is <link xref="bluetooth-visibility">discoverable
      or visible</link>. For example, if you are trying to connect to a phone,
      make sure that it is not in airplane mode.</p>
    </item>
    <item>
      <title>Tietokoneessasi ei ole Bluetooth-sovitinta</title>
      <p>Useissa tietokoneissa ei ole Bluetooth-sovitinta. Voit ostaa sovittimen esimerkiksi USB-väylään, jos haluat käyttää Bluetoothia.</p>
    </item>
  </terms>

</page>
