<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-open" xml:lang="cs">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-30" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak otevřít soubory pomocí aplikace, která není výchozí pro daný typ souboru. Případně, jak ji nastavit jako výchozí.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

<title>Otevření souborů pomocí jiných aplikací</title>

  <p>Když ve správci souborů kliknete dvojitě (nebo prostředním tlačítkem) na soubor, ten se otevře ve výchozí aplikaci pro daný typ souboru. Můžete jej také otevřít v jiné aplikaci, vyhledat pro něj aplikaci on-line nebo nastavit výchozí aplikaci pro všechny soubory stejného typu.</p>

  <p>Jestli chcete otevřít soubor jinou aplikací, než je výchozí, klikněte na soubor pravým tlačítkem a vyberte v horní části nabídky aplikaci, kterou chcete. Pokud ji tam nevidíte, vyberte <gui>Otevřít jinou aplikací</gui>. Ve výchozím stavu zobrazuje správce souborů jen aplikace, o kterých ví, že umí se souborem zacházet. Abyste viděli všechny aplikace v počítači, klikněte na <gui>Zobrazit všechny aplikace</gui>.</p>

<p>Pokud stále nemůžete najít aplikaci, kterou chcete, můžete vyhledat další aplikace kliknutím na <gui>Vyhledat nové aplikace</gui>. Správce souborů vyhledá on-line balíčky obsahující aplikace, o kterých je známo, že umí pracovat se souborem příslušného typu.</p>

<section id="default">
  <title>Změna výchozí aplikace</title>
  <p>Můžete změnit výchozí aplikaci, která se používá k otevření souborů daného typu. Umožní vám to otevřít vaši upřednostňovanou aplikaci, když otevřete soubor dvojitým kliknutím. Například můžete chtít, aby se otevřel váš oblíbený hudební přehrávač, když dvojitě kliknete na soubor MP3.</p>

  <steps>
    <item><p>Vyberte soubor typu, pro který chcete výchozí aplikaci změnit. Například, když chcete změnit, která aplikace bude otevírat soubory MP3, vyberte soubor <file>.mp3</file>.</p></item>
    <item><p>Klikněte na soubor pravým tlačítkem a vyberte <gui>Vlastnosti</gui>.</p></item>
    <item><p>Vyberte kartu <gui>Otevřít pomocí</gui>.</p></item>
    <item><p>Vyberte aplikaci, kterou chcete a klikněte na <gui>Nastavit jako výchozí</gui>.</p>
    <p>Pokud <gui>Další aplikace</gui> obsahují aplikaci, kterou občas chcete použít k otevření, ale nechcete ji nastavit jako výchozí, vyberte ji a klikněte na <gui>Přidat</gui>. Bude přidána do <gui>Doporučených aplikací</gui>. Po té ji budete mít možnost použít vybráním ze seznamu po kliknutí pravým tlačítkem na soubor.</p></item>
  </steps>

  <p>Tyto změny výchozí aplikaci neplatí jen pro vybraný soubor, ale obecně pro všechny soubory stejného typu.</p>

<!-- TODO: mention resetting the open with list with the "Reset" button -->

</section>

</page>
