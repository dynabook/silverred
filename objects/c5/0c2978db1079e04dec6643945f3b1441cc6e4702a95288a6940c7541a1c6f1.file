<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-use-windows-workspaces" xml:lang="da">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>
    <link type="guide" xref="getting-started" group="tasks"/>
    <title role="trail" type="link">Brug vinduer og arbejdsområder</title>
    <link type="seealso" xref="shell-windows-switching"/>
    <title role="seealso" type="link">En vejledning til at bruge vinduer og arbejdsområder</title>
    <link type="next" xref="gs-use-system-search"/>
  </info>

  <title>Brug vinduer og arbejdsområder</title>

  <ui:overlay width="812" height="452">
  <media type="video" its:translate="no" src="figures/gnome-windows-and-workspaces.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-windows-and-workspaces.svg"/>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>Vinduer og arbejdsområder</tt:p>
         </tt:div>
         <tt:div begin="6s" end="10s">
           <tt:p>Tag fat i vinduets titellinje og træk den til toppen af skærmen for at maksimere vinduet.</tt:p>
           </tt:div>
         <tt:div begin="10s" end="13s">
           <tt:p>Slip vinduet, når skærmen er fremhævet.</tt:p>
         </tt:div>
         <tt:div begin="14s" end="20s">
           <tt:p>Tag fat i vinduets titellinje og træk den væk fra skærmens kanter for at gendanne vinduet fra maksimeret tilstand.</tt:p>
         </tt:div>
         <tt:div begin="25s" end="29s">
           <tt:p>Du kan også klikke på toplinjen og trække vinduet væk for at gendanne det.</tt:p>
         </tt:div>
         <tt:div begin="34s" end="38s">
           <tt:p>Tag fat i vinduets titellinje og træk den til venstre for at maksimere vinduet langs den venstre side af skærmen.</tt:p>
         </tt:div>
         <tt:div begin="38s" end="40s">
           <tt:p>Slip vinduet, når halvdelen af skærmen er fremhævet.</tt:p>
         </tt:div>
         <tt:div begin="41s" end="44s">
           <tt:p>Tag fat i vinduets titellinje og træk den til højre for at maksimere vinduet langs den højre side af skærmen.</tt:p>
         </tt:div>
         <tt:div begin="44s" end="48s">
           <tt:p>Slip vinduet, når halvdelen af skærmen er fremhævet.</tt:p>
         </tt:div>
         <tt:div begin="54s" end="60s">
           <tt:p>Hold <key href="help:gnome-help/keyboard-key-super">Super</key>-tasten nede og tryk på <key>↑</key> for at maksimere et vindue med tastaturet.</tt:p>
         </tt:div>
         <tt:div begin="61s" end="66s">
           <tt:p>Hold <key href="help:gnome-help/keyboard-key-super">Super</key>-tasten nede og tryk på <key>↓</key> for at gendanne vinduet til sin umaksimerede størrelse.</tt:p>
         </tt:div>
         <tt:div begin="66s" end="73s">
           <tt:p>Hold <key href="help:gnome-help/keyboard-key-super">Super</key>-tasten nede og tryk på <key>→</key> for at maksimere et vindue langs højre side af skærmen.</tt:p>
         </tt:div>
         <tt:div begin="76s" end="82s">
           <tt:p>Hold <key href="help:gnome-help/keyboard-key-super">Super</key>-tasten nede og tryk på <key>←</key> for at maksimere et vindue langs venstre side af skærmen.</tt:p>
         </tt:div>
         <tt:div begin="83s" end="89s">
           <tt:p>Tryk på <keyseq><key href="help:gnome-help/keyboard-key-super">Super </key><key>Page Down</key></keyseq> for at flytte til et arbejdsområde som er under det nuværende arbejdsområde.</tt:p>
         </tt:div>
         <tt:div begin="90s" end="97s">
           <tt:p>Tryk på <keyseq><key href="help:gnome-help/keyboard-key-super">Super </key><key>Page Up</key></keyseq> for at flytte til et arbejdsområde som er over det nuværende arbejdsområde.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>
  
  <section id="use-workspaces-and-windows-maximize">
    <title>Maksimér og gendan vinduer</title>
    <p/>
    
    <steps>
      <item><p>Tag fat i vinduets titellinje og træk den til toppen af skærmen for at maksimere et vindue, så det fylder hele skrivebordets område.</p></item>
      <item><p>Slip vinduet for at maksimere det, når skærmen er fremhævet.</p></item>
      <item><p>Tag fat i vinduets titellinje og træk det væk fra skærmens kanter for at gendanne et vindue til dets umaksimerede størrelse.</p></item>
    </steps>
    
  </section>

  <section id="use-workspaces-and-windows-tile">
    <title>Fliselæg vinduer</title>
    <p/>
    
    <steps>
      <item><p>Tag fat i vinduets titellinje og træk det til venstre eller højre side af skærmen for at maksimere et vindue langs en side af skærmen.</p></item>
      <item><p>Slip vinduet for at maksimere det langs den valgte side af skærmen, når halvdelen af skærmen er fremhævet.</p></item>
      <item><p>Tag fat i titellinjen på det andet vindue, og træk det til den modsatte side af skærmen for at maksimere to vinduer side om side.</p></item>
       <item><p>Slip vinduet for at maksimere det langs den modsatte side af skærmen, når halvdelen af skærmen er fremhævet.</p></item>
    </steps>
    
  </section>
  
  <section id="use-workspaces-and-windows-maximize-keyboard">
    <title>Maksimér og gendan vinduer ved brug af tastaturet</title>
    
    <steps>
      <item><p>Hold <key href="help:gnome-help/keyboard-key-super">Super</key>-tasten nede og tryk på <key>↑</key> for at maksimere et vindue med tastaturet.</p></item>
      <item><p>Hold <key href="help:gnome-help/keyboard-key-super">Super</key>-tasten nede og tryk på <key>↓</key> for at maksimere et vindue ved brug af tastaturet.</p></item>
    </steps>
    
  </section>
  
  <section id="use-workspaces-and-windows-tile-keyboard">
    <title>Fliselæg vinduer ved brug af tastaturet</title>
    
    <steps>
      <item><p>Hold <key href="help:gnome-help/keyboard-key-super">Super</key>-tasten nede og tryk på <key>→</key> for at maksimere et vindue langs højre side af skærmen.</p></item>
      <item><p>Hold <key href="help:gnome-help/keyboard-key-super">Super</key>-tasten nede og tryk på <key>←</key> for at maksimere et vindue langs venstre side af skærmen.</p></item>
    </steps>
    
  </section>
  
  <section id="use-workspaces-and-windows-workspaces-keyboard">
    <title>Skift arbejdsområder ved brug af tastaturet</title>
    
    <steps>
    
    <item><p>Tryk på <keyseq><key href="help:gnome-help/keyboard-key-super">Super</key><key>Page Down</key></keyseq> for at flytte til et arbejdsområde, der er neden under det nuværende arbejdsområde.</p></item>
    <item><p>Tryk på <keyseq><key href="help:gnome-help/keyboard-key-super">Super</key><key>Page Up</key></keyseq> for at flytte til et arbejdsområde, der er oven over det nuværende arbejdsområde.</p></item>

    </steps>
    
  </section>

</page>
