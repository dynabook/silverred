<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="shell-lockscreen" xml:lang="id">

  <info>
    <link type="guide" xref="shell-overview#apps"/>
    <link type="guide" xref="shell-notifications#lock-screen-notifications"/>

    <revision pkgversion="3.6.1" date="2012-11-11" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>The decorative and functional lock screen conveys useful
    information.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

  <title>The lock screen</title>

  <p>The lock screen means that you can see what is happening while your
  computer is locked, and it allows you to get a summary of what has been
  happening while you have been away. The lock screen curtain shows an
  attractive image on the screen while your computer is locked, and provides
  useful information:</p>

  <list>
    <item><p>the name of the logged-in user</p></item>
    <item><p>tanggal dan waktu, serta notifikasi tertentu</p></item>
    <item><p>status baterai dan jaringan</p></item>
<!-- No media control anymore on lock screen, see BZ #747787: 
    <item><p>the ability to control media playback — change the volume, skip a
    track or pause your music without having to enter a password</p></item> -->
  </list>

  <p>To unlock your computer, raise the lock screen curtain by dragging it
  upward with the cursor, or by pressing <key>Esc</key> or <key>Enter</key>.
  This will reveal the login screen, where you can enter your password to
  unlock. Alternatively, just start typing your password and the curtain will
  be automatically raised as you type. You can also switch users if your
  computer is configured for more than one.</p>

  <p>To hide notifications from the lock screen, see
  <link xref="shell-notifications#lock-screen-notifications"/>.</p>

</page>
