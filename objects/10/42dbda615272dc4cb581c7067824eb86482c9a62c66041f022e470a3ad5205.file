<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="a11y task" id="a11y-screen-reader" xml:lang="ca">

  <info>
    <link type="guide" xref="a11y#vision" group="blind"/>

    <revision pkgversion="3.13.92" date="2014-09-20" status="incomplete"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Jana Heves</name>
      <email>jsvarova@gnome.org</email>
    </credit>

    <desc>Com utilitzar el lector de pantalla <app>Orca</app> perquè pronunciï la interfície d'usuari en veu alta.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

  <title>Llegir la pantalla en veu alta</title>

  <p>GNOME proveeix del lector de pantalla <app>Orca</app> per llegir la interfície d'usuari. Depenent de com hàgiu instal·lat GNOME, podeu no tenir-lo instal·lat. Si no ho està, instal·leu Orca primer.</p>

  <p if:test="action:install"><link style="button" action="install:orca">Instal·la Orca</link></p>
  
  <p>Per a iniciar <app>Orca</app> utilitzant el teclat:</p>
  
  <steps>
    <item>
    <p>Premeu <key>Súper</key>+<key>Alt</key>+<key>S</key>.</p>
    </item>
  </steps>
  
  <p>O per a iniciar <app>Orca</app> utilitzant el ratolí i el teclat:</p>

  <steps>
    <item>
      <p>Obriu les <gui xref="shell-introduction#activities">Activitats</gui> i comenceu a escriure <gui>Accés universal</gui>.</p>
    </item>
    <item>
      <p>Feu clic a <gui>Accés universal</gui> per obrir el quadre.</p>
    </item>
    <item>
      <p>Cliqueu a <gui>Lector de pantalla</gui> a la secció <gui>Visió</gui>, aleshores activeu <gui>Lector de pantalla</gui> al diàleg.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Permuta ràpidament el Lector de Pantalla entre actiu i inactiu</title>
    <p>Podeu activar i desactivar el Lector de pantalla fent clic a la icona <link xref="a11y-icon">accessibilitat</link> a la barra superior i seleccionant <gui>Lector de pantalla</gui>.</p>
  </note>

  <p>Consulteu l'<link href="help:orca">ajuda d'Orca</link> per a més informació.</p>
</page>
