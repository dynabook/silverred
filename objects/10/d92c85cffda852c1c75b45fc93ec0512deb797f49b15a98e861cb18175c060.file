<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-maximize" xml:lang="lv">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>
    <link type="seealso" xref="shell-windows-tiled"/>

    <revision pkgversion="3.4.0" date="2012-03-14" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Veiciet dubultklikšķi uz loga virsraksta joslas, lai maksimizētu vai atjaunotu logu.</desc>
  </info>

  <title>Maksimizēt logu un atjaunot tā izmēru</title>

  <p>Jūs varat maksimizēt logu, lai tas aizņem visu laukumu uz darbvirsmas un atmaksimizēt logu, lai to atgrieztu parastajā izmērā. Jūs varat arī maksimizēt logu vertikāli gar kreiso un labo ekrāna malu, lai varētu viegli redzēt abus logus vienlaicīgi. Skatiet <link xref="shell-windows-tiled"/>, lai uzzinātu vairāk.</p>

  <p>Lai maksimizētu logu, satveriet virsraksta joslu un velciet to uz ekrāna augšpusi, vai vienkārši veiciet dubultklikšķi uz virsraksta joslas. Lai maksimizētu logu ar tastatūru, turiet piespiestu <key xref="keyboard-key-super">Super</key> taustiņu un spiediet <key>↑</key> vai spiediet <keyseq><key>Alt</key><key>F10</key></keyseq>.</p>

  <p if:test="platform:gnome-classic">Jūs varat arī maksimizēt logu, spiežot maksimizēšanas pogu virsraksta joslā.</p>

  <p>Lai atjaunotu logu uz tā nemaksimizēto izmēru, velciet to prom no ekrāna malām. Ja logs ir pilnībā maksimizēts, jūs varat veikt dubultklikšķi un virsraksta joslas, lai to atjaunotu. Jūs varat arī izmantot tās pašas tastatūras saīsnes, ko izmantojāt logu maksimizēšanai.</p>

  <note style="tip">
    <p>Turiet piespiestu <key>Super</key> taustiņu un velciet jebkur logā, lai to pārvietotu.</p>
  </note>

</page>
