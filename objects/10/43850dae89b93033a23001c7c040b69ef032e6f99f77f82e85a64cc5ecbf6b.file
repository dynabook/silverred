<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-bouncekeys" xml:lang="el">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Παραβλέψτε τα γρήγορα επαναλαμβανόμενων πατήματα πλήκτρου για το ίδιο πλήκτρο.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Ενεργοποίηση πλήκτρων αναπήδησης</title>

  <p>Ενεργοποιείστε τα <em>πλήκτρα αναπήδησης</em> για να αγνοήσετε τα πατήματα των πλήκτρων που επαναλαμβάνονται γρήγορα. Για παράδειγμα, εάν έχετε τρέμουλο στο χέρι που προκαλεί το πάτημα ενός πλήκτρου πολλές φορές ενώ θέλετε να το πατήσετε μόνο μια φορά, θα πρέπει να ενεργοποιήσετε τα πλήκτρα αναπήδησης.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Universal Access</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Πατήστε <gui>Βοήθεια πληκτρολόγησης (AccessX)</gui> στην ενότητα <gui>Πληκτρολόγηση</gui>.</p>
    </item>
    <item>
      <p>Switch the <gui>Bounce Keys</gui> switch to on.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Ενεργοποιήστε και απενεργοποιήστε γρήγορα τα πλήκτρα αναπήδησης</title>
    <p>Μπορείτε να ενεργοποιήσετε ή όχι τα πλήκτρα αναπήδησης κάνοντας κλικ στο <link xref="a11y-icon">εικονίδιο προσιτότητας</link> στην πάνω γραμμή και επιλέγοντας <gui>πλήκτρα αναπήδησης</gui>. Το εικονίδιο προσιτότητας είναι ορατό όταν μία ή περισσότερες ρυθμίσεις έχουν ενεργοποιηθεί από τον πίνακα <gui>Γενική πρόσβαση</gui>.</p>
  </note>

  <p>Χρησιμοποιήστε τον ολισθητή <gui>καθυστέρηση αποδοχής</gui> για να αλλάξετε τον χρόνο αναμονής των πλήκτρων αναπήδησης πριν καταχωρίσει ένα άλλο πάτημα πλήκτρου αφού πατήσατε το πλήκτρο για πρώτη φορά. Επιλέξτε <gui>Ήχος κατά την απόρριψη πλήκτρου</gui>, εάν θέλετε ο υπολογιστής σας να ηχεί κάθε φορά που αγνοεί ένα πάτημα πλήκτρου επειδή συνέβη υπερβολικά νωρίς μετά το προηγούμενο πάτημα πλήκτρου.</p>

</page>
