<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-launch-applications" xml:lang="pt-BR">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>

    <link type="guide" xref="getting-started" group="videos"/>
    <title role="trail" type="link">Executando aplicativos</title>
    <link type="seealso" xref="shell-apps-open"/>
    <title role="seealso" type="link">Um tutorial sobre a execução de aplicativos</title>
    <link type="next" xref="gs-switch-tasks"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Felipe Braga</mal:name>
      <mal:email>fbobraga@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2013-2019</mal:years>
    </mal:credit>
  </info>

  <title>Executando aplicativos</title>

  <ui:overlay width="812" height="452">
   <media type="video" its:translate="no" src="figures/gnome-launching-applications.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-launching-apps.svg"/>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>Executando aplicativos</tt:p>
         </tt:div>
         <tt:div begin="5s" end="7.5s">
           <tt:p>Mova o ponteiro do seu mouse para o canto de <gui>Atividades</gui> ao topo à esquerda da tela.</tt:p>
           </tt:div>
         <tt:div begin="7.5s" end="9.5s">
           <tt:p>Clique no ícone <gui>Mostrar aplicativos</gui>.</tt:p>
         </tt:div>
         <tt:div begin="9.5s" end="11s">
           <tt:p>Clique no aplicativo que você deseja executar, por exemplo, Ajuda.</tt:p>
         </tt:div>
         <tt:div begin="12s" end="21s">
           <tt:p>De forma alternativa, use o teclado para abrir o <gui>Panorama de atividades</gui> ao pressionar a tecla <key href="help:gnome-help/keyboard-key-super">Super</key>.</tt:p>
         </tt:div>
         <tt:div begin="22s" end="29s">
           <tt:p>Comece digitando o nome do aplicativo que você deseja executar.</tt:p>
         </tt:div>
         <tt:div begin="30s" end="33s">
           <tt:p>Pressione <key>Enter</key> para executar o aplicativo.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
   </media>
  </ui:overlay>

  <section id="launch-apps-mouse">
    <title>Executando aplicativos com o mouse</title>
 
  <steps>
    <item><p>Mova o ponteiro do seu mouse para o canto de <gui>Atividades</gui> no topo à esquerda da tela para mostrar o <gui>Panorama de atividades</gui>.</p></item>
    <item><p>Clique no ícone <gui>Mostrar aplicativos</gui> que é mostrado na parte inferior da barra no lado esquerdo da tela.</p></item>
    <item><p>Uma lista de aplicativos é mostrada. Clique no aplicativo que você deseja executar, por exemplo, Ajuda.</p></item>
  </steps>

  </section>

  <section id="launch-app-keyboard">
    <title>Executando aplicativos com o teclado</title>

  <steps>
    <item><p>Abra o <gui>Panorama de atividades</gui> ao pressionar a tecla <key href="help:gnome-help/keyboard-key-super">Super</key>.</p></item>
    <item><p>Comece digitando o nome do aplicativo que você deseja executar. A pesquisa pelo aplicativo começa instantaneamente.</p></item>
    <item><p>Assim que o ícone do aplicativo for mostrado e selecionado, pressione <key>Enter</key> para executar o aplicativo.</p></item>
  </steps>

  </section>

</page>
