<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="a11y task" id="a11y" xml:lang="fi">

  <info>
    <link type="guide" xref="index" group="a11y"/>

    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc><link xref="a11y#vision">Näkeminen</link>, <link xref="a11y#sound">kuuleminen</link>, <link xref="a11y#mobility">liikkuvuus</link>, <link xref="a11y-braille">sokeainkirjoitus</link>, <link xref="a11y-mag">näytön suurennos</link>…</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

  <title>Esteettömyys</title>

  <p>Gnome-työpöytä sisältää esteettömyystoimintoja, jotka tukevat käyttäjiä, joilla on erikoistarpeita tai yleisiä esteettömyyteen liittyviä apulaitteita. Monet esteettömyystoiminnoista ovat saatavilla yläpalkin esteettömyysvalikon kautta.</p>

  <section id="vision">
    <title>Näkörajoitteisuudet</title>

    <links type="topic" groups="blind" style="linklist">
      <title>Sokeus</title>
    </links>
    <links type="topic" groups="lowvision" style="linklist">
      <title>Heikko näkökyky</title>
    </links>
    <links type="topic" groups="colorblind" style="linklist">
      <title>Värisokeus</title>
    </links>
    <links type="topic" style="linklist">
      <title>Muut aiheet</title>
    </links>
  </section>

  <section id="sound">
    <title>Kuuloon liittyvät rajoitteisuudet</title>
    <links type="topic" style="linklist"/>
  </section>

  <section id="mobility">
    <title>Liikuntarajoitteisuudet</title>

    <links type="topic" groups="pointing" style="linklist">
      <title>Hiiren siirtäminen</title>
    </links>
    <links type="topic" groups="clicking" style="linklist">
      <title>Napsauta-ja-raahaa</title>
    </links>
    <links type="topic" groups="keyboard" style="linklist">
      <title>Näppäimistön käyttö</title>
    </links>
    <links type="topic" style="linklist">
      <title>Muut aiheet</title>
    </links>
  </section>
</page>
