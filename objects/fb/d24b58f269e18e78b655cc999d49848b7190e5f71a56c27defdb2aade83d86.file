<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="task" version="1.0 ui/1.0" id="files-copy" xml:lang="de">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-15" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Objekte in einen neuen Ordner kopieren oder verschieben.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2019.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2019.</mal:years>
    </mal:credit>
  </info>

<title>Dateien und Ordner kopieren oder verschieben</title>

 <p>Dateien oder Ordner können kopiert oder an einen neuen Ort verschoben werden, indem Sie sie mit der Maus ziehen und ablegen, die Kopieren- und Einfügen-Befehle verwenden oder Tastenkombinationen benutzen.</p>

 <p>Sie könnten zum Beispiel eine Präsentation auf einen USB-Stick kopieren wollen, damit Sie sie zur Arbeit mitnehmen können. Oder Sie könnten von einem Dokument eine Sicherungskopie anlegen, bevor Sie es ändern (und dann auf die alte Kopie zurückgreifen, falls Sie mit Ihren Änderungen nicht zufrieden sind).</p>

 <p>Diese Anweisungen werden sowohl auf Dateien als auch auf Ordner angewendet. Sie können Dateien und Ordner exakt auf die gleiche Weise kopieren.</p>

<steps ui:expanded="false">
<title>Kopieren und Einfügen von Dateien</title>
<item><p>Wählen Sie die zu kopierende Datei aus, indem Sie einmalig darauf klicken.</p></item>
<item><p>Klicken Sie mit rechts und wählen Sie <gui>Kopieren</gui> oder drücken Sie <keyseq><key>Strg</key><key>C</key></keyseq>.</p></item>
<item><p>Gehen Sie zu einem anderen Ordner, wo Sie die Kopie dieser Datei ablegen wollen.</p></item>
<item><p>Klicken Sie auf den Menüknopf und wählen Sie <gui>Einfügen</gui>, um den Kopiervorgang abzuschließen, oder drücken Sie <keyseq><key>Strg</key><key>V</key></keyseq>. Nun befindet sich eine Kopie der Datei im Originalordner und eine in dem anderen Ordner.</p></item>
</steps>

<steps ui:expanded="false">
<title>Ausschneiden und Einfügen von Dateien (um sie zu verschieben)</title>
<item><p>Wählen Sie die zu verschiebende Datei aus, indem Sie einmal darauf klicken.</p></item>
<item><p>Klicken Sie mit der rechten Maustaste und wählen Sie <gui>Ausschneiden</gui>, oder drücken Sie <keyseq><key>Strg</key><key>X</key></keyseq>.</p></item>
<item><p>Gehen Sie zu einem anderen Ordner, wohin Sie die Datei verschieben wollen.</p></item>
<item><p>Klicken Sie auf den Menüknopf in der Werkzeugleiste und wählen Sie <gui>Einfügen</gui>, um den Verschiebevorgang abzuschließen, oder drücken Sie <keyseq><key>Strg</key><key>V</key></keyseq>. Die Datei wird aus dem Originalordner entnommen und in den anderen Ordner verschoben.</p></item>
</steps>

<steps ui:expanded="false">
<title>Ziehen von Dateien zum Kopieren oder Verschieben</title>
<item><p>Öffnen Sie die Dateiverwaltung und gehen Sie zu dem Ordner, der die Datei enthält, die Sie kopieren wollen.</p></item>
<item><p>Klicken Sie auf <gui>Datei</gui> in der obersten Leiste und wählen Sie <gui>Neues Fenster</gui> (oder drücken Sie <keyseq><key>Strg</key><key>N</key></keyseq>), um ein zweites Fenster zu öffnen. Navigieren Sie im neuen Fenster zu dem Ordner, in den Sie die Datei verschieben oder kopieren wollen.</p></item>
<item>
 <p>Klicken und ziehen Sie die Datei von einem Fenster ins andere. Dadurch wird die Datei <em>verschoben</em>, wenn das Ziel auf <em>demselben</em> Gerät ist, oder <em>kopiert</em>, wenn sich das Ziel auf einem <em>anderen</em> Gerät befindet.</p>
 <p>Wenn Sie zum Beispiel eine Datei von einem USB-Stick in Ihren persönlichen Ordner ziehen, wird sie kopiert, weil Sie sie von einem Gerät zu einem anderen ziehen.</p>
 <p>Sie können erzwingen, dass die Datei kopiert wird, indem Sie die <key>Strg</key>-Taste während des Ziehens gedrückt halten, oder Sie können erzwingen, dass sie verschoben wird, indem Sie die <key>Umschalttaste</key> während des Ziehens gedrückt halten.</p>
 </item>
</steps>

<note>
  <p>Sie können eine Datei nicht in einen Ordner kopieren oder verschieben, der <em>nur lesbar</em> ist. Einige Ordner sind nur lesbar, um ein Manipulieren der Inhalte zu verhindern. Sie können ändern, dass dieser Ordner nur lesbar ist, indem Sie die <link xref="nautilus-file-properties-permissions">Zugriffsrechte ändern</link>.</p>
</note>

</page>
