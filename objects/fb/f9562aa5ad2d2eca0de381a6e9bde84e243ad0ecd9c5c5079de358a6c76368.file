<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mouse-middleclick" xml:lang="ta">

  <info>
    <link type="guide" xref="tips"/>
    <link type="guide" xref="mouse#tips"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.4" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>டிஃபானி அன்ட்டொபோல்ஸ்கி</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>ஷான் மெக்கேன்ஸ்</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>மைக்கேல் ஹில்</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>எக்காட்டெரினா ஜெராசிமோவா</name>
      <email>kittykat3756@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Use the middle mouse button to open applications, open tabs and
    more.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

<title>நடு சொடுக்கம்</title>

<p>Many mice and some touchpads have a middle mouse button. On a mouse
with a scroll wheel, you can usually press directly down on the scroll
wheel to middle-click. If you don’t have a middle mouse button, you
can press the left and right mouse buttons at the same time to
middle-click.</p>

<p>பல விரல் தட்டல்களை ஆதரிக்கும் தொடுதிட்டுகளில், நீங்கள் நடு சொடுக்கம் செய்ய ஒரே நேரத்தில் மூன்று விரல்களால் தட்டலாம். இது வேலை செய்ய நீங்கள் தொடுதிட்டு அமைவுகளில் <link xref="mouse-touchpad-click">தட்டு சொடுக்கத்தை செயல்படுத்த</link> வேண்டும்.</p>

<p>பல பயன்பாடுகள் மேம்பட்ட சொடுக்க குறுக்குவழிகளுக்கு நடு சொடுக்கத்தைப் பயன்படுத்துகின்றன.</p>

<list>
  <item><p>In applications with scrollbars, left-clicking in the empty space of
  the bar moves the scroll position directly to that place. Middle-clicking
  moves up to a single page towards that location.</p></item>

  <item><p>In the <gui>Activities</gui> overview, you can quickly open a new
  window for an application with middle-click. Simply middle-click on the
  application’s icon, either in the dash on the left, or in the applications
  overview. The applications overview is displayed using the grid button in the
  dash.</p></item>

  <item><p>Most web browsers allow you to open links in tabs quickly with the
  middle mouse button. Just click any link with your middle mouse button, and
  it will open in a new tab.</p></item>

  <item><p>கோப்பு மேலாளரில், நடு சொடுக்கம் இரு செயல்களைச் செய்கிறது. நீங்கள் ஒரு கோப்புறையை நடு சொடுக்கம் செய்தால் அது ஒரு புதிய தாவலில் திறக்கும். இது பிரபல வலை உலாவிகளின் நடத்தையைப் போன்றதே. ஒரு கோப்பை நடு சொடுக்கம் செய்தால், அதை நீங்கள் இரு சொடுக்கம் செய்ததைப் போல அது திறக்கும்.</p></item>
</list>

<p>Some specialized applications allow you to use the middle mouse
button for other functions. Search your application’s help for
<em>middle-click</em> or <em>middle mouse button</em>.</p>

</page>
