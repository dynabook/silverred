<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-select" xml:lang="da">

  <info>
    <link type="guide" xref="files#faq"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Press <keyseq><key>Ctrl</key><key>S</key></keyseq> to select multiple
    files which have similar names.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years/>
    </mal:credit>
  </info>

  <title>Vælg filer efter mønster</title>

  <p>You can select files in a folder using a pattern on the file name.
  Press <keyseq><key>Ctrl</key><key>S</key></keyseq> to bring up the
  <gui>Select Items Matching</gui> window. Type in a pattern using
  common parts of the file names plus wild card characters. There are
  two wild card characters available:</p>

  <list style="compact">
    <item><p><file>*</file> matches any number of any characters, even
    no characters at all.</p></item>
    <item><p><file>?</file> matches exactly one of any character.</p></item>
  </list>

  <p>F.eks.:</p>

  <list>
    <item><p>Hvis du har en OpenDocument-tekstfil, en PDF-fil, og et billede som alle har det samme grundnavn <file>Faktura</file>, vælges alle tre med mønsteret</p>
    <example><p><file>Faktura.*</file></p></example></item>

    <item><p>Hvis du har nogle billeder, der er navngivet som <file>Ferie-001.jpg</file>, <file>Ferie-002.jpg</file>, <file>Ferie-003.jpg</file>; så vælges de alle med mønsteret</p>
    <example><p><file>Ferie-???.jpg</file></p></example></item>

    <item><p>If you have photos as before, but you have edited some of them and
    added <file>-edited</file> to the end of the file name of the photos you
    have edited, select the edited photos with</p>
    <example><p><file>Ferie-???-redigeret.jpg</file></p></example></item>
  </list>

</page>
