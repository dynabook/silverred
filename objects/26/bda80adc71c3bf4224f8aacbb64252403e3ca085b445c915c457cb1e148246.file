<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="lockdown-online-accounts" xml:lang="es">

  <info>
    <link type="guide" xref="user-settings#lockdown"/>
    <link type="seealso" xref="dconf-lockdown"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2015</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

   <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Activar o desactivar algunas o todas las cuentas en línea.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Oliver Gutiérrez</mal:name>
      <mal:email>ogutsua@gmail.com</mal:email>
      <mal:years>2018 - 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2017 - 2019</mal:years>
    </mal:credit>
  </info>
  <title>Permitir o denegar el uso de cuentas en línea</title>

  <p>Las aplicación de <app>Cuentas en línea de GNOME</app> (GOA) se usa para integrar cuentas de red personales con el escritorio GNOME y las aplicaciones. El usuario puede añadir sus propias cuentas en linea, como Google, Facebook, Flickr, ownCloud, y otras usando la aplicación <app>Cuentas en línea</app>.</p>

  <p>Como administrador del sistema, puede:</p>
  <list>
    <item><p>activar todas las cuentas en línea;</p></item>
    <item><p>activar sólo ciertas cuentas en línea;</p></item>
    <item><p>desactivar todas las cuentas en línea;</p></item>
  </list>

<steps>
  <title>Configurar las cuentas en línea</title>
  <item><p>Make sure that you have the <sys>gnome-online-accounts</sys> package
  installed on your system.</p>
  </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
  <item>
  <p>Cree el archivo <file>/etc/dconf/db/local.d/00-goa</file> para proporcionar información a la base de datos <sys>local</sys> con la siguiente configuración.</p>
   <list>
   <item><p>Para activar proveedores específicos:</p>
<code>
[org/gnome/online-accounts]
whitelisted-providers= ['google', 'facebook']
</code>
  </item>
   <item><p>Para desactivar todos los proveedores:</p>
<code>
[org/gnome/online-accounts]
whitelisted-providers= ['']
</code>
  </item>
  <item><p>Para permitir todos los proveedores disponibles:</p>
<code>
[org/gnome/online-accounts]
whitelisted-providers= ['all']
</code>
  <p>Este es el valor predeterminado.</p></item>
   </list>
  </item>
  <item>
    <p>Para evitar que el usuario ignore estos ajustes, cree el archivo <file>/etc/dconf/db/local.d/locks/goa</file> con el siguiente contenido:</p>
    <listing>
    <title><file>/etc/dconf/db/local.db/locks/goa</file></title>
<code>
# Lock the list of providers that are allowed to be loaded
/org/gnome/online-accounts/whitelisted-providers
</code>
    </listing>
  </item>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
</steps>

</page>
