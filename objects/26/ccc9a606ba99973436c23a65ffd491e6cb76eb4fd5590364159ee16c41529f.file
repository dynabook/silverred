<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="nautilus-views" xml:lang="de">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-views"/>

    <revision pkgversion="3.8" version="0.2" date="2013-04-03" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <its:rules xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <desc>Die vorgegebene Sortierreihenfolge und Gruppierung der Dateiverwaltung festlegen.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2019.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2019.</mal:years>
    </mal:credit>
  </info>

<title>Die Einstellungen von <app>Dateien</app> zeigen</title>

<p>Sie können anpassen, wie Dateien und Ordner standardmäßig gruppiert und sortiert werden. Klicken Sie auf den Menüknopf rechts oben im Fenster, wählen Sie <gui style="menuitem">Einstellungen</gui> und anschließend den Reiter <gui style="tab">Ansichten</gui>.</p>

<section id="default-view">
<title>Vorgabeansicht</title>
<terms>
  <item>
    <title><gui>Objekte anordnen</gui></title>
    <p its:locNote="TRANSLATORS: use the translation of the tooltip for the     view selector button that opens the view popover in the main window for     'View options'">Legen Sie für Dateien die <link xref="files-sort">Sortierreihenfolge</link> individuell für einen Ordner fest, in dem Sie auf den Menüknopf der Optionen klicken und die gewünschte Einstellung unter <gui>Sortieren</gui> wählen. Alternativ klicken Sie auf die Kopfzeilen der Listenspalten in der Listenansicht.</p>
  </item>
  <item>
    <title><gui>Ordner vor Dateien anzeigen</gui></title>
    <p>Per Voreinstellung zeigt die Dateiverwaltung nicht mehr alle Ordner vor den Dateien. Aktivieren Sie diese Option, um alle Ordner vor Dateien auflisten zu lassen.</p>
  </item>
</terms>
</section>

</page>
