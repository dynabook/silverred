<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sound-usemic" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="media#sound"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>使用模拟信号或 USB 麦克风，选择默认的输入设备。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>使用其他的麦克风</title>

  <p>您可以使用外部麦克风进行音频会议，制作音频文件，或者使用其他多媒体应用程序。即使您的计算机有内置麦克风或带有麦克风的网络摄像头，但是单独的麦克风可提供更出色的音质。</p>

  <p>If your microphone has a circular plug, just plug it into the appropriate
  audio socket on your computer. Most computers have two sockets: one for
  microphones and one for speakers. This socket is usually light red in color
  or is accompanied by a picture of a microphone. Microphones plugged
  into the appropriate socket are usually used by default. If not, see the
  instructions below for selecting a default input device.</p>

  <p>如果您使用 USB 麦克风，把它插入任何一个 USB 接口。USB音箱将作为单独的音频设备，您可能要指定默认使用哪一个麦克风。</p>

  <steps>
    <title>选择默认音频输入设备</title>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Sound</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Sound</gui> to open the panel.</p>
    </item>
    <item>
      <p>In the <gui>Input</gui> tab, select the device that you want to use.
      The input level indicator should respond when you speak.</p>
    </item>
  </steps>

  <p>You can adjust the volume and switch the microphone off from this
  panel.</p>

</page>
