<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-icon" xml:lang="sv">

  <info>
    <link type="guide" xref="a11y"/>

    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <desc>Hjälpmedelsmenyn är ikonen i systemraden som ser ut som en person.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Hitta hjälpmedelsmenyn</title>

  <p><em>Hjälpmedelsmenyn</em> är menyn där du kan aktivera en eller flera av hjälpmedelsinställningarna. Du hittade denna meny genom att klicka på ikonen som ser ut som en person som omges av en cirkel i systemraden.</p>

  <figure>
    <desc>Hjälpmedelsmenyn hittas i systemraden.</desc>
    <media its:translate="no" type="image" mime="image/png" src="figures/universal-access-menu.png"/>
  </figure>

  <p>Om du inte ser någon hjälpmedelsmeny så kan du aktivera den från inställningspanelen <gui>Hjälpmedel</gui>:</p>

  <steps>
    <item>
      <p>Öppna <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Hjälpmedel</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Hjälpmedel</gui> för att öppna panelen.</p>
    </item>
    <item>
      <p>Slå på <gui>Visa alltid meny för hjälpmedel</gui>.</p>
    </item>
  </steps>

  <p>För att nå denna meny via tangentbordet snarare än med musen, tryck <keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq> för att flytta tangentbordsfokus till systemraden. En vit linje kommer att visas under <gui>Aktiviteter</gui>-knappen — detta informerar dig om vilket objekt i systemraden som är markerat. Använd piltangenterna på tangentbordet för att flytta den vita linjen till hjälpmedelsmenyn och tryck sedan på <key>Retur</key> för att öppna den. Du kan använda piltangenterna upp och ner för att välja objekt i menyn. Tryck på <key>Retur</key> för att växla tillstånd för det markerade objektet.</p>

</page>
