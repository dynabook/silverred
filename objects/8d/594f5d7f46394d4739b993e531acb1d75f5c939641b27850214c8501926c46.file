<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-changepicture" xml:lang="ta">

  <info>
    <link type="guide" xref="user-accounts#manage"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.14.0" date="2014-10-08" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.32.0" date="2019-03-16" status="final"/>

    <credit type="author">
      <name>GNOME ஆவணமாக்கத் திட்டப்பணி</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>ஷான் மெக்கேன்ஸ்</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>எக்காட்டெரினா ஜெராசிமோவா</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>உங்கள் புகுபதிவு மற்றும் பயனர் திரைகளில் உங்கள் புகைப்படத்தைச் சேர்த்தல்.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>உங்கள் புகுபதிவு திரை புகைப்படத்தை மாற்றுதல்</title>

  <p>நீங்கள் புகுபதிவு செய்யும் போது அல்லது பயனர்களை மாற்றும் போது பயனர்களின் புகைப்படங்களுடன் கூடிய பட்டியலைக் காண்பீர்கள். கணினியில் உள்ள ஸ்டாக் படங்கள் அல்லது நீங்கள் தேர்ந்தெடுக்கும் ஒரு படத்தை இதற்கு அமைக்க முடியும். உங்கள் வெப்கேமைப் பயன்படுத்தி புதிய புகுபதிவு புகைப்படத்தை எடுத்தும் பயன்படுத்தலாம்.</p>

  <p>You need <link xref="user-admin-explain">administrator privileges</link>
  to edit user accounts other than your own.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Users</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Users</gui> to open the panel.</p>
    </item>
    <item>
      <p>If you want to edit a user other than yourself, press
      <gui style="button">Unlock</gui> in the top right corner and type in your
      password when prompted.</p>
    </item>
    <item>
      <p>உங்கள் பெயருக்கு அடுத்துள்ள படத்தை சொடுக்கவும். ஸ்டாக் புகுபதிவு புகைப்படங்களைக் கொண்ட கீழ் தோன்றும் கேலரி ஒன்று காட்டப்படும். அவற்றில் ஒன்றை நீங்கள் விரும்பினால் அதை பயன்படுத்தலாம்.</p>
      <list>
        <item>
          <p>If you would rather use a picture you already have on your
          computer, click <gui>Select a file…</gui>.</p>
        </item>
        <item>
          <p>If you have a webcam, you can take a new login photo right now by
          clicking <gui>Take a picture…</gui>. Take your
          picture, then move and resize the square outline to crop out the
          parts you do not want. If you do not like the picture you took, click
          <gui style="button">Take Another Picture</gui> to try again, or
          <gui>Cancel</gui> to give up.</p>
        </item>
      </list>
    </item>
  </steps>

</page>
