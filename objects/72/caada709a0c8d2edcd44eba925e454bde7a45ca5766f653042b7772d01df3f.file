<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="process-identify-file" xml:lang="de">
  <info>
    <revision version="0.2" pkgversion="3.11" date="2014-01-26" status="review"/>
    <link type="guide" xref="index#processes-tasks" group="processes-tasks"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author copyright">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
      <years>2011</years>
    </credit>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2011, 2014</years>
    </credit>

    <desc>So suchen Sie nach offenen Dateien um zu sehen, welcher Prozesse sie verwendet.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Herausfinden, welches Programm eine spezifische Datei verwendet</title>

  <p>Gelegentlich wird eine Fehlermeldung mitteilen, dass ein Gerät (z.B. die Sound-Karte oder das DVD-ROM) belegt ist, oder dass die Datei, welche Sie bearbeiten wollen, geöffnet ist. So finden Sie den verantwortlichen Prozess bzw. die verantwortlichen Prozesse:</p>

    <steps>
      <item><p>Klicken Sie auf <guiseq><gui>Systemüberwachung</gui><gui>Offene Dateien suchen</gui></guiseq>.</p>
      </item>
      <item><p>Geben Sie einen Dateinamen oder einen Teil davon ein. Dies ist z.B. <file>/dev/snd</file> für das Audio-Gerät oder <file>/media/cdrom</file> für ein DVD-ROM.</p>
      </item>
      <item><p>Klicken Sie auf <gui>Suchen</gui>.</p>
      </item>
    </steps>

  <p>Daraufhin wird eine Liste der laufenden Prozesse angezeigt, die derzeit auf die Datei(en) zugreifen, die der Suche entsprechen. Durch schließen des Programms sollten Sie Zugriff auf das Gerät bekommen oder die Datei konfliktfrei bearbeiten können.</p>

</page>
