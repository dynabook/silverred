<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-changepassword" xml:lang="el">

  <info>
    <link type="guide" xref="user-accounts#passwords"/>
    <link type="seealso" xref="user-goodpassword"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Έργο Τεκμηρίωσης GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Κρατήστε τον λογαριασμό σας ασφαλή αλλάζοντας τον κωδικό πρόσβασής σας συχνά στις ρυθμίσεις λογαριασμών.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Αλλάξτε τον κωδικό πρόσβασής σας</title>

  <p>Είναι καλή ιδέα να αλλάζετε συχνά τον κωδικό πρόσβαση σας, ειδικά εάν νομίζετε ότι κάποιος άλλος τον γνωρίζει.</p>

  <p>Χρειάζεστε <link xref="user-admin-explain">δικαιώματα διαχειριστή</link> για να επεξεργαστείτε λογαριασμούς χρήστη άλλους εκτός από τον δικό σας.</p>

  <steps>
    <item>
      <p>Ανοίξτε την επισκόπηση <gui xref="shell-introduction#activities">Δραστηριότητες</gui> και αρχίστε να πληκτρολογείτε <gui>Χρήστες</gui>.</p>
    </item>
    <item>
      <p>Κάντε κλικ στο <gui>Χρήστες</gui> για να ανοίξετε τον πίνακα.</p>
    </item>
    <item>
      <p>Κάντε κλικ στην ετικέτα <gui>·····</gui> δίπλα στον <gui>Κωδικό πρόσβασης</gui>. Αν αλλάζετε τον κωδικό πρόσβασης για έναν διαφορετικό χρήστη, θα χρειαστείτε πρώτα να <gui>Ξεκλειδώσετε</gui> τον πίνακα.</p>
    </item>
    <item>
      <p>Πληκτρολογήστε τον τρέχοντα κωδικό πρόσβασής σας, και έπειτα εισάγετε έναν νέο κωδικό πρόσβασης. Εισάγετε τον νέο σας κωδικό πρόσβασης πάλι στο πεδίο <gui>Επαλήθευση νέου κωδικού πρόσβασης</gui>.</p>
      <p>Μπορείτε να πατήσετε στο εικονίδιο <gui style="button"><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16">
      <span its:translate="yes">δημιουργία κωδικού πρόσβασης</span></media></gui> για να δημιουργήσετε αυτόματα έναν τυχαίο κωδικό πρόσβασης.</p>
    </item>
    <item>
      <p>Κάντε κλικ στο <gui>Αλλαγή</gui>.</p>
    </item>
  </steps>

  <p>Βεβαιωθείτε ότι <link xref="user-goodpassword">διαλέξατε έναν καλό κωδικό πρόσβασης</link>. Αυτό θα βοηθήσει να κρατήσετε τον λογαριασμό χρήστη ασφαλή.</p>

  <note>
    <p>Όταν ενημερώνετε τον κωδικό πρόσβασης σύνδεσης, ο κωδικός πρόσβασης σύνδεσης της κλειδοθήκης θα ενημερωθεί αυτόματα για να είναι ο ίδιος με τον κωδικό πρόσβασης της νέας σύνδεσης.</p>
  </note>

  <p>Αν ξεχνάτε τον κωδικό πρόσβασής σας, οποιοσδήποτε χρήστης με δικαιώματα διαχειριστή μπορεί να τον αλλάξει για σας.</p>

</page>
