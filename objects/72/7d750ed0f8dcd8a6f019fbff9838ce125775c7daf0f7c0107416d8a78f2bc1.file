<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-which-application" xml:lang="lv">

  <info>
    <link type="guide" xref="accounts"/>
    <link type="seealso" xref="accounts-disable-service"/>

    <revision pkgversion="3.8.2" date="2013-05-22" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="incomplete"/>

    <credit type="author copyright">
      <name>Baptiste Mille-Mathias</name>
      <email>baptistem@gnome.org</email>
      <years>2012, 2013</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Lietotnes, kas var izmantot sadaļā <app>Tiešsaistes konti</app> iestatītos kontus un servisus, kuriem tās var piekļūt.</desc>

  </info>

  <title>Tiešsaistes servisi un lietotnes</title>

  <p>Pēc tiešsaistes konta pievienošanas, jebkura lietotne var izmantot to kontu jebkuram pieejamam pakalpojumam, ko neesat <link xref="accounts-disable-service">izslēdzis</link>. Dažādi pakalpojumu sniedzēji nodrošina dažādus pakalpojumus. Šajā lapā ir uzskaitīti dažādi pakalpojumi un dažas lietotnes, kuras šos pakalpojumus izmanto.</p>

  <terms>
    <item>
      <title>Kalendārs</title>
      <p>Kalendāra pakalpojums jums ļauj skatīt, pievienot un rediģēt notikumus tiešsaistes kalendārā. To izmanto <app>Kalendārs</app>, <app>Evolution</app> un <app>California</app> lietotnes.</p>
    </item>

    <item>
      <title>Tērzēšana</title>
      <p>Tērzēšanas pakalpojums jums ļauj tērzēt ar saviem kontaktiem populārās tūlītējās tērzēšanas platformās. To izmanto <app>Empathy</app> lietotne.</p>
    </item>

    <item>
      <title>Kontakti</title>
      <p>Kontaktu pakalpojums jums ļauj redzēt publicēto informāciju par jūsu kontaktiem dažādos pakalpojumos. To izmanto <app>Kontaktu</app> un <app>Evolution</app> lietotnes.</p>
    </item>

    <item>
      <title>Dokumenti</title>
      <p>Dokumentu pakalpojums jums ļauj skatīt tiešsaistes dokumentus, piemēram Google docs. Jūs varat skatīt savus dokumentus, izmantojot <app>Dokumentu</app> lietotni.</p>
    </item>

    <item>
      <title>Datnes</title>
      <p>Datņu pakalpojums pievieno attālināto datņu atrašanās vietas, it kā tās būtu pievienotas ar <link xref="nautilus-connect">Savienoties ar serveri</link> funkcionalitāti datņu pārvaldniekā. Jūs varat piekļūt attālinātām datnēm, izmantojot datņu pārvaldnieku, kā arī caur datņu atvēršanas un saglabāšanas dialoglodziņiem jebkurā lietotnē.</p>
    </item>

    <item>
      <title>E-pasts</title>
      <p>Pasta pakalpojums jums ļauj sūtīt un saņemt e-pastu, izmantojot pasta pakalpojuma sniedzēju, piemēram, Google. To izmanto <app>Evolution</app>.</p>
    </item>

<!-- TODO: Not sure what this does. Doesn't seem to do anything in Maps app.
    <item>
      <title>Maps</title>
    </item>
-->

    <item>
      <title>Fotogrāfijas</title>
      <p>Fotogrāfiju pakalpojums jums ļauj skatīt tiešsaistes fotogrāfijas, piemēram Facebook. Jūs varat skatīt savas fotogrāfijas, izmantojot <app>Fotogrāfiju</app> lietotni.</p>
    </item>

    <item>
      <title>Printeri</title>
      <p>Printeru pakalpojums jums ļauj sūtīt PDF kopiju pakalpojuma sniedzējam, izmantojot drukāšanas dialoglodziņu jebkurā lietotnē. Pakalpojuma sniedzējs var sniegt drukāšanas pakalpojumu, vai arī tikai kalpot kā PDF dokumentu krātuve, kuru varat lejupielādēt un izdrukāt vēlāk.</p>
    </item>

    <item>
      <title>Lasīt vēlāk</title>
      <p>Vēlākas lasīšanas pakalpojums ļauj saglabāt tīmekļa lapu ārējā pakalpojumā, lai to varētu vēlāk izlasīt uz citas ierīces. Pašlaik neviena lietotne neizmanto šo pakalpojumu.</p>
    </item>

  </terms>

</page>
