<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="video-sending" xml:lang="it">

  <info>
    <link type="guide" xref="media#videos"/>
    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.12.1" date="2014-03-30" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.17.90" date="2015-08-30" status="final"/>

    <credit type="author">
      <name>Progetto documentazione di GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Verificare che abbiano installato i corretti codec video.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Other people can’t play the videos I made</title>

  <p>Se è stato creato un video col proprio computer Linux ed è stato inviato ad altre persone che utilizzano Windows o Mac OS, queste potrebbero riscontrare dei problemi nel riprodurlo correttamente.</p>

  <p>Per poter riprodurre il video è necessario che le persone a cui è stato inviato abbiano installato i <em>codec</em> corretti. Un codec è un componente software in grado di gestire il video e visualizzarlo correttamente sullo schermo. Esistono vari formati video e ognuno di essi richiede un codec differente per poter essere visualizzato. Per controllare il formato del proprio video:</p>

  <list>
    <item>
      <p>Open <app>Files</app> from the
      <gui xref="shell-introduction#activities">Activities</gui> overview.</p>
    </item>
    <item>
      <p>Right-click on the video file and select <gui>Properties</gui>.</p>
    </item>
    <item>
      <p>Go to the <gui>Audio/Video</gui> or <gui>Video</gui> tab and look at
      which <gui>Codec</gui> are listed under <gui>Video</gui> and
      <gui>Audio</gui> (if the video also has audio).</p>
    </item>
  </list>

  <p>Ask the person having problems with playback if they have the right codec
  installed. They may find it helpful to search the web for the name of the
  codec plus the name of their video playback application. For example, if your
  video uses the <em>Theora</em> format and you have a friend using Windows
  Media Player to try and watch it, search for “theora windows media player”.
  You will often be able to download the right codec for free if it’s not
  installed.</p>

  <p>If you can’t find the right codec, try the
  <link href="http://www.videolan.org/vlc/">VLC media player</link>. It works
  on Windows and Mac OS as well as Linux, and supports a lot of different video
  formats. Failing that, try converting your video into a different format.
  Most video editors are able to do this, and specific video converter
  applications are available. Check the software installer application to see
  what’s available.</p>

  <note>
    <p>There are a few other problems which might prevent someone from playing
    your video. The video could have been damaged when you sent it to them
    (sometimes big files aren’t copied across perfectly), they could have
    problems with their video playback application, or the video may not have
    been created properly (there could have been some errors when you saved the
    video).</p>
  </note>

</page>
