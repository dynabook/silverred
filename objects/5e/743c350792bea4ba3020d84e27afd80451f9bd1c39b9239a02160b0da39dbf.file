<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="sharing-media" xml:lang="es">

  <info>
    <link type="guide" xref="sharing"/>
    <link type="guide" xref="prefs-sharing"/>

    <revision pkgversion="3.10" version="0.2" date="2013-11-02" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-13" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Compartir medios en su red local usando UPnP.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Compartir su música, fotos y vídeos</title>

  <p>Puede explorar, buscar y reproducir los medios en su equipo usando un dispositivo que soporte <sys>UPnP</sys> o <sys>DLNA</sys> como puede ser un teléfono, una televisión o una videoconsola. Configure la <gui>Compartición de medios</gui> para permitir a estos dispositivos acceder a las carpetas que contienen su música, fotos y vídeos.</p>

  <note style="info package">
    <p>Debe tener instalado el paquete <app>Rygel</app> para que la <gui>Compartición de medios</gui> esté visible.</p>

    <if:choose xmlns:if="http://projectmallard.org/if/1.0/">
      <if:when test="action:install">
        <p><link action="install:rygel" style="button">Instalar Rygel</link></p>
      </if:when>
    </if:choose>
  </note>

  <steps>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Compartición</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Compartición</gui> para abrir el panel.</p>
    </item>
    <item>
      <p>Si la opción <gui>Compartir</gui> en la parte superior derecha de la ventana está apagada, actívela.</p>

      <note style="info"><p>Si puede editar el texto de debajo de <gui>Nombre del equipo</gui> puede <link xref="sharing-displayname">cambiar</link> el nombre de su equipo en la red.</p></note>
    </item>
    <item>
      <p>Seleccione <gui>Compartición de medios</gui>.</p>
    </item>
    <item>
      <p>Active la opción <gui>Compartición multimedia</gui>.</p>
    </item>
    <item>
      <p>Las carpetas, <file>Música</file>, <file>Imágenes</file> y <file>Vídeos</file> se comparten de manera predeterminada. Para eliminar una de ellas, pulse la <gui>×</gui> junto al nombre de la carpeta.</p>
    </item>
    <item>
      <p>Para añadir otra carpeta, pulse <gui style="button">+</gui> para abrir la ventana <gui>Elegir una carpeta</gui>. Navegue <em>dentro</em> de la carpeta que quiera y pulse <gui style="button">Abrir</gui>.</p>
    </item>
    <item>
      <p>Pulse <gui style="button">×</gui>. Ahora podrá explorar o reproducir los archivos de las carpetas que haya seleccionado usando el dispositivo externo.</p>
    </item>
  </steps>

  <section id="networks">
  <title>Redes</title>

  <p>La sección <gui>Redes</gui> lista las redes a las que está actualmente conectado. Use el interruptor junto a cada una para elegir qué medios se pueden compartir.</p>

  </section>

</page>
