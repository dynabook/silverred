<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-workspaces-movewindow" xml:lang="da">

  <info>
    <link type="guide" xref="shell-windows#working-with-workspaces"/>
    <link type="seealso" xref="shell-workspaces"/>

    <revision pkgversion="3.8" version="0.3" date="2013-05-10" status="review"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.35.91" date="2020-02-27" status="candidate"/>

    <credit type="author">
      <name>GNOMEs dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Gå til <gui>Aktivitetsoversigten</gui> og træk vinduet til et andet arbejdsområde.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years/>
    </mal:credit>
  </info>

  <title>Flyt et vindue til et andet arbejdsområde</title>

 <if:choose>
 <if:when test="platform:gnome-classic">
  <steps>
    <title>Med musen:</title>
    <item>
      <p>Press the button at the bottom left of the screen in the window list.</p>
    </item>
    <item>
      <p>Click and drag the window towards the bottom right of the screen.</p>
    </item>
    <item>
      <p>Drop the window onto one of the workspaces in the <em>workspace
      selector</em> at the right-hand side of the window list. This workspace
      now contains the window you have dropped.</p>
    </item>
  </steps>
 </if:when>
 <if:when test="!platform:gnome-classic">
  <steps>
    <title>Med musen:</title>
    <item>
      <p>Åbn <gui xref="shell-introduction#activities">Aktivitetsoversigten</gui>.</p>
    </item>
    <item>
      <p>Klik og træk vinduet mod højre side af skærmen.</p>
    </item>
    <item>
      <p>The <em xref="shell-workspaces">workspace selector</em> will
      expand.</p>
    </item>
    <item>
      <p>Træk vinduet til et tomt arbejdsområde. Arbejdsområdet indeholder nu det vindue, du slap, og et nyt arbejdsområde vises nederst i <em>arbejdsområdevælgeren</em>.</p>
    </item>
  </steps>
 </if:when>
 </if:choose>

  <steps>
    <title>Med tastaturet:</title>
    <item>
      <p>Vælg vinduet du vil flytte (f.eks. med <keyseq><key xref="keyboard-key-super">Super</key><key>Tab</key></keyseq> <em xref="shell-windows-switching">vinduesskifteren</em>).</p>
    </item>
    <item>
      <p if:test="!platform:gnome-classic">Tryk på <keyseq><key>Super</key><key>Skift</key><key>Page Up</key></keyseq> for at flytte vinduet til et arbejdsområde, som er ovenover det nuværende arbejdsområde i <em>arbejdsområdevælgeren</em>.</p>
      <p if:test="!platform:gnome-classic">Tryk på <keyseq><key>Super</key><key>Skift</key><key>Page Down</key></keyseq> for at flytte vinduet til et arbejdsområde, som er nedenunder det nuværende arbejdsområde i <em>arbejdsområdevælgeren</em>.</p>
      <p if:test="platform:gnome-classic">Press <keyseq><key>Shift</key><key>Ctrl</key>
      <key>Alt</key><key>→</key></keyseq> to move the window to a workspace which
      is left of the current workspace on the <em>workspace selector</em>.</p>
      <p if:test="platform:gnome-classic">Press <keyseq><key>Shift</key><key>Ctrl</key>
      <key>Alt</key><key>←</key></keyseq> to move the window to a workspace which
      is right of the current workspace on the <em>workspace selector</em>.</p>
    </item>
  </steps>

</page>
