<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="a11y task" id="a11y-screen-reader" xml:lang="hu">

  <info>
    <link type="guide" xref="a11y#vision" group="blind"/>

    <revision pkgversion="3.13.92" date="2014-09-20" status="incomplete"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Jana Heves</name>
      <email>jsvarova@gnome.org</email>
    </credit>

    <desc>Az <app>Orka</app> képernyő-olvasóval felolvastatható a felhasználói felület.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Képernyő felolvastatása</title>

  <p>A GNOME alatt az <app>Orka</app> képernyőolvasó segítségével olvastatható fel a felhasználói felület. A GNOME telepítésének módjától függően lehet, hogy az Orka nincs telepítve. Ebben az esetben előbb telepítse az Orkát.</p>

  <p if:test="action:install"><link style="button" action="install:orca">Az Orka telepítése</link></p>
  
  <p>Az <app>Orka</app> indításához a billentyűzet használatával:</p>
  
  <steps>
    <item>
    <p>Nyomja meg a <key>Super</key>+<key>Alt</key>+<key>S</key> kombinációt.</p>
    </item>
  </steps>
  
  <p>Vagy az <app>Orka</app> elindítható billentyűzettel és egérrel is:</p>

  <steps>
    <item>
      <p>Nyissa meg a <gui xref="shell-introduction#activities">Tevékenységek</gui> áttekintést, és kezdje el begépelni az <gui>Akadálymentesítés</gui> szót.</p>
    </item>
    <item>
      <p>Kattintson az <gui>Akadálymentesítés</gui> elemre a panel megnyitásához.</p>
    </item>
    <item>
      <p>Kattintson a <gui>Képernyőolvasó</gui> elemre a <gui>Látás</gui> szakaszban, majd kapcsolja be a <gui>Képernyőolvasót</gui> az ablakban.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Képernyőolvasó gyors be- és kikapcsolása</title>
    <p>Be- és kikapcsolhatja a képernyőolvasót az <link xref="a11y-icon">akadálymentesítés ikonra</link> kattintva a felső sávon, és a <gui>Képernyőolvasó</gui> menüpontot kiválasztva.</p>
  </note>

  <p>További információt az <link href="help:orca">Orka súgójában</link> találhat.</p>
</page>
