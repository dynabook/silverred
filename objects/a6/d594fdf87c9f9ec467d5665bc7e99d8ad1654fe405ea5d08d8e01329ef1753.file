<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-suspendfail" xml:lang="pl">

  <info>
    <link type="guide" xref="power#problems"/>
    <link type="guide" xref="hardware-problems-graphics"/>
    <link type="seealso" xref="hardware-driver"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Niektóre urządzenia powodują problemy z usypianiem.</desc>

    <credit type="author">
      <name>Projekt dokumentacji GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  </info>

<title>Dlaczego komputer nie włącza się z powrotem po uśpieniu?</title>

<p><link xref="power-suspend">Uśpienie</link> komputera, a następnie przebudzenie go może nie zadziałać. Może tak być, ponieważ usypianie nie jest poprawnie obsługiwane przez komputer.</p>

<section id="resume">
  <title>Komputer jest uśpiony i nie chce się przebudzić</title>
  <p>Jeśli uśpiono komputer, a następnie naciśnięto klawisz lub kliknięto mysz, to powinien on się przebudzić i wyświetlić ekran proszący o hasło. Jeśli tak się nie dzieje, to spróbuj nacisnąć przycisk zasilania (nie przytrzymuj go, tylko wciśnij raz).</p>
  <p>Jeśli to nadal nie pomaga, to upewnij się, że monitor komputera jest włączony i jeszcze raz spróbuj nacisnąć jakiś klawisz.</p>
  <p>Jako ostatnia deska ratunku, wyłącz komputer przytrzymując przycisk zasilania przez 5-10 sekund, chociaż spowoduje to utratę niezapisanej pracy. Po tym komputer powinien dać się włączyć z powrotem.</p>
  <p>Jeśli zdarza się to za każdym razem, kiedy komputer jest usypiany, to funkcja uśpienia może działać ze sprzętem komputera.</p>
  <note style="warning">
    <p>Jeśli komputer utraci zasilanie i nie ma alternatywnego źródła zasilania (takiego jak działający akumulator), to się wyłączy.</p>
  </note>
</section>

<section id="hardware">
  <title>Połączenie bezprzewodowe (lub inny sprzęt) nie działa po przebudzeniu komputera</title>
  <p>Jeśli uśpiono komputer, a następnie go przebudzono, to może się okazać, że połączenie internetowe, mysz lub jakieś inne urządzenie nie działa poprawnie. Może to być spowodowane tym, że sterownik urządzenia nie obsługuje uśpienia. Jest to <link xref="hardware-driver">problem ze sterownikiem</link>, nie z urządzeniem.</p>
  <p>Jeśli urządzenie ma przełącznik zasilania, to spróbuj wyłączyć je i włączyć ponownie. W większości przypadków urządzenie zacznie działać. Jeśli jest podłączone kablem USB lub podobnym, to odłącz urządzenie, a następnie podłącz je ponownie i zobacz, czy działa.</p>
  <p>Jeśli nie można wyłączyć lub odłączyć urządzenia, lub jeśli to nie pomoże, to należy ponownie uruchomić komputer, aby urządzenie zaczęło działać.</p>
</section>

</page>
