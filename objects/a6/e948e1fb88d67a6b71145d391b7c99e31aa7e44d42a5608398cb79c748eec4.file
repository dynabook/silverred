<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="task" id="pref-profile-char-width" xml:lang="fr">

  <info>
    <link type="guide" xref="index#advanced"/>
    <link type="guide" xref="pref#profile"/>
    <revision pkgversion="3.14" date="2014-09-08" status="review"/>
    <revision version="0.2" pkgversion="3.34" date="2020-01-04" status="review"/>

    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <!--<credit type="copyright editor">
      <name></name>
      <email></email>
      <years></years>
    </credit>-->

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Afficher les caractères de largeur ambigüe en large plutôt qu’étroit.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Yoann Fievez</mal:name>
      <mal:email>yoann.fievez@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jérôme Sirgue</mal:name>
      <mal:email>jsirgue@free.fr</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Guillaume Bernard</mal:name>
      <mal:email>associations@guillaume-bernard.fr</mal:email>
      <mal:years>2015-2018</mal:years>
    </mal:credit>
  </info>

  <title>Le caractère parait trop étroit</title>

  <p>Certains caractères, tels certaines lettres grecques ou logogrames asiatiques, peuvent occuper une ou deux cellules dans la fenêtre de terminal. Ces caractères sont souvent désignés comme <em>caractères ambigus</em>. Par défaut, ces caractères sont affichés avec une largeur étroite dans <app>Terminal</app>, ce qui a meilleure apparence dans certaines situations ou l’alignement parfait est important, comme l’art ASCII. Vous pouvez changer vos préférences de profil pour afficher en large les caractères ambigus, ce qui peut être plus adapté si vous lisez un texte en prose.</p>

  <steps>
    <item>
      <p>Press the menu button in the top-right corner of the window and select
      <gui style="menuitem">Preferences</gui>.</p>
    </item>
    <item>
      <p>In the sidebar, select your current profile in the <gui>Profiles</gui>
      section.</p>
    </item>
    <item>
      <p>Open the <gui style="tab">Compatibility</gui> tab.</p>
    </item>
    <item>
      <p>Set <gui>Ambiguous-width characters</gui> to <gui>Wide</gui>.</p>
    </item>
  </steps>

</page>
