<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="power-nowireless" xml:lang="lv">

  <info>
    <link type="guide" xref="power#problems"/>
    <link type="seealso" xref="power-suspendfail"/>
    <link type="seealso" xref="hardware-driver"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <credit type="author">
      <name>GNOME dokumentācijas projekts</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Dažām tīkla ierīcēm ir grūtības ar datora iesnaušanos un atmošanos.</desc>
  </info>

  <title>Kad atmodinu datoru, nav bezvadu tīkla</title>

  <p>Ja dators tika iesnaudināts, var gadīties, ka pēc atmošanās nestrādā interneta pieslēgums. Tas notiek, kad bezvadu tīkla ierīces <link xref="hardware-driver">draiveris</link> pilnībā neatbalsta zināmas enerģijas taupīšanas iespējas. Parasti tad bezvadu savienojums netiek atkal ieslēgts, kad dators ir atmodināts.</p>

  <p>Ja tā notiek, pamēģiniet izslēgt un atkal ieslēgt bezvadu savienojumu:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Wi-Fi</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Wi-Fi</gui> to open the panel.</p>
    </item>
    <item>
      <p>Switch the <gui>Wi-Fi</gui> switch at the top-right of the window to
      off and then on again.</p>
    </item>
    <item>
      <p>If the wireless still does not work, switch the <gui>Airplane
      Mode</gui> switch to on and then switch it off again.</p>
    </item>
  </steps>

  <p>Ja tas nelīdz, datora pārstartēšanai vajadzētu iedarbināt bezvadu savienojumu. Ja jums vēl aizvien ir problēmas, savienojieties ar internetu, izmantojot Ethernet vadu un atjauniniet savu datoru.</p>

</page>
