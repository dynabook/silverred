<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-which-application" xml:lang="sv">

  <info>
    <link type="guide" xref="accounts"/>
    <link type="seealso" xref="accounts-disable-service"/>

    <revision pkgversion="3.8.2" date="2013-05-22" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="incomplete"/>

    <credit type="author copyright">
      <name>Baptiste Mille-Mathias</name>
      <email>baptistem@gnome.org</email>
      <years>2012, 2013</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Program kan använda de konton som skapats i <app>Nätkonton</app> och tjänsterna de utnyttjar.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Nättjänster och program</title>

  <p>När du har lagt till ett nätkonto kan vilket program som helst använda det kontot för vilken tillgänglig tjänst som helst som du inte har <link xref="accounts-disable-service">inaktiverat</link>. Olika leverantörer tillhandahåller olika tjänster. Denna sida listar de olika tjänsterna och några av programmen som använder dem.</p>

  <terms>
    <item>
      <title>Kalender</title>
      <p>Kalender-tjänsten låter dig visa, lägga till och redigera händelser i en nätkalender. Den används av program så som <app>Kalender</app>, <app>Evolution</app> och <app>California</app>.</p>
    </item>

    <item>
      <title>Chatt</title>
      <p>Chatt-tjänsten låter dig chatta med dina kontakter på populära snabbmeddelande-plattformar. Den används av programmet <app>Empathy</app>.</p>
    </item>

    <item>
      <title>Kontakter</title>
      <p>Kontakter-tjänsten låter dig se de publicerade detaljerna om dina kontakter på diverse tjänster. Den används av program så som <app>Kontakter</app> och <app>Evolution</app>.</p>
    </item>

    <item>
      <title>Dokument</title>
      <p>Dokument-tjänsten låter dig visa dina nätdokument, t.ex. de i Google docs. Du kan visa dina dokument via programmet <app>Dokument</app>.</p>
    </item>

    <item>
      <title>Filer</title>
      <p>Filer-tjänsten lägger till en plats för fjärrfiler, som om du hade lagt till en via funktionaliteten <link xref="nautilus-connect">Anslut till server</link> i filhanteraren. Du kan nå fjärrfiler genom filhanteraren, samt genom dialoger för att öppna och spara filer i vilket program som helst.</p>
    </item>

    <item>
      <title>E-post</title>
      <p>E-post-tjänsten låter dig skicka och ta emot e-post via en e-post-leverantör så som Google. Den används av <app>Evolution</app>.</p>
    </item>

<!-- TODO: Not sure what this does. Doesn't seem to do anything in Maps app.
    <item>
      <title>Maps</title>
    </item>
-->

    <item>
      <title>Foton</title>
      <p>Foton-tjänsten låter dig visa dina nätfoton så som de du postar på Facebook. Du kan visa dina foton via programmet <app>Foton</app>.</p>
    </item>

    <item>
      <title>Skrivare</title>
      <p>Skrivare-tjänsten låter dig skicka en PDF-kopia till en leverantör inifrån utskriftsdialogen i vilket program som helst. Leverantören kan tillhandahålla utskriftstjänster, eller så kan den bara agera som lagring av PDF:en som du senare kan hämta ner och skriva ut.</p>
    </item>

    <item>
      <title>Läs senare</title>
      <p>Läs senare-tjänsten låter dig spara en webbsida hos externa tjänster så att du kan läsa den senare på en annan enhet. Inga program använder för närvarande denna tjänst.</p>
    </item>

  </terms>

</page>
