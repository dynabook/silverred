<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-wireless" xml:lang="cs">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-batterylife"/>

    <revision pkgversion="3.20" date="2016-06-15" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2016</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak šetřit baterii vypnutím Bluetooth, Wi-Fi a mobilního připojení.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Vypnutí nepoužívaných bezdrátových technologií</title>

  <p>Spotřebu z baterie můžete snížit vypnutím Bluetooth, Wi-Fi nebo mobilního širokopásmového připojení v době, kdy je nevyužíváte.</p>

  <steps>

    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Napájení</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Napájení</gui> otevřete příslušný panel.</p>
    </item>
    <item>
      <p>V části <gui>Šetření energií</gui> najdete vypínače pro <gui>Wi-Fi</gui>, <gui>Mobilní připojení</gui> a <gui>Bluetooth</gui>. Nepoužívané věci vypněte. Když je budete později potřebovat, znovu je zapněte.</p>
    </item>

  </steps>

</page>
