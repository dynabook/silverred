<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" version="1.0 if/1.0" id="shell-introduction" xml:lang="sv">

  <info>
    <link type="guide" xref="shell-overview" group="#first"/>
    <link type="guide" xref="index" group="intro"/>

    <revision pkgversion="3.6.0" date="2012-10-13" status="review"/>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-28" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="review"/>
    <revision pkgversion="3.35.91" date="2020-07-19" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>En visuell överblick över ditt skrivbord, systemraden, och översiktsvyn <gui>Aktiviteter</gui>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Visuell överblick över GNOME</title>

  <p>GNOME 3 erbjuder ett fullständigt omarbetat användargränssnitt som designats för att inte vara i vägen, minimera distraktioner och hjälpa dig att få saker gjorda. När du loggar in första gången kommer du att se ett tomt skrivbord och systemraden.</p>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <media type="image" src="figures/shell-top-bar.png" width="600" if:test="!target:mobile">
      <p>GNOME-skalets systemrad</p>
    </media>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-top-bar-classic.png" width="500" if:test="!target:mobile">
      <p>GNOME-skalets systemrad</p>
    </media>
  </if:when>
</if:choose>

  <p>Systemraden erbjuder tillgång till dina fönster och program, din kalender och möten och <link xref="status-icons">systemegenskaper</link> som ljud, nätverk och ström. I systemmenyn i systemraden kan du ändra volym eller ljusstyrka för skärmen, redigera dina <gui>Trådlösa</gui> anslutningsdetaljer, kontrollera din batteristatus, logga ut eller växla användare och stänga av din dator.</p>

<links type="section"/>

<!-- TODO: Replace "Activities overview" title for classic mode with something
like "Application windows" by using if:when and if:else ? -->
<section id="activities">
  <title>Översiktsvyn <gui>Aktiviteter</gui></title>

<media type="image" src="figures/shell-activities-dash.png" height="530" style="floatstart floatleft" if:test="!target:mobile, !platform:gnome-classic">
  <p>Aktiviteter-knappen och snabbstartspanel</p>
</media>

  <p if:test="!platform:gnome-classic">För att nå dina fönster och program, klicka på knappen <gui>Aktiviteter</gui> eller bara flytta din musmarkör till övre vänstra hörnet. Du kan också trycka på tangenten <key xref="keyboard-key-super">Super</key> på ditt tangentbord. Du kan se dina fönster och program i översiktsvyn. Du kan också bara börja att skriva för att söka bland dina program, filer, mappar och på webben.</p>

  <p if:test="platform:gnome-classic">För att nå dina fönster och program, klicka på knappen i nedre vänstra hörnet av skärmen i fönsterlisten. Du kan också trycka på tangenten <key xref="keyboard-key-super">Super</key> för att se en översikt med levande miniatyrbilder över alla fönster på den aktuella arbetsytan.</p>

  <p if:test="!platform:gnome-classic">Till vänster i översiktsvyn hittar du <em>snabbstartspanelen</em>. Snabbstartspanelen visar dig dina favoritprogram och körande program. Klicka på vilken ikon som helst i favoriter för att öppna det programmet; om programmet redan körs kommer det att ha en liten punkt under sin ikon. Att klicka på dess ikon kommer att plocka fram det senast använda fönstret. Du kan också dra ikonen till översiktsvyn eller till en arbetsyta till höger.</p>

  <p if:test="!platform:gnome-classic">Om du högerklickar på ikonen visas en meny som låter dig välja vilket fönster som helst för ett körande program, eller öppna ett nytt fönster. Du kan också klicka på ikonen medan du håller ner <key>Ctrl</key> för att öppna ett nytt fönster.</p>

  <p if:test="!platform:gnome-classic">När du går in i översiktsvyn kommer du först att hamna i fönsteröversiktsvyn. Denna visar dig live-uppdaterade miniatyrbilder av alla fönster på den aktuella arbetsytan.</p>

  <p if:test="!platform:gnome-classic">Klicka på rutnätsknappen i botten av snabbstartspanelen för att visa programöversiktsvyn. Denna visar dig alla program som finns installerade på din dator. Klicka på vilket program som helst för att köra det eller dra ett program till översikten eller till miniatyrbilden för en arbetsyta. Du kan också dra ett program till snabbstartspanelen för att göra det till en favorit. Dina favoritprogram stannar kvar i snabbstartspanelen även om de inte kör, så att du kan nå dem snabbt.</p>

  <list style="compact">
    <item>
      <p><link xref="shell-apps-open">Läs mer om att starta program.</link></p>
    </item>
    <item>
      <p><link xref="shell-windows">Läs mer om fönster och arbetsytor.</link></p>
    </item>
  </list>

</section>

<section id="appmenu">
  <title>Programmeny</title>
  <if:choose>
    <if:when test="!platform:gnome-classic">
      <media type="image" src="figures/shell-appmenu-shell.png" width="250" style="floatend floatright" if:test="!target:mobile">
        <p>Programmenyn för <app>Terminal</app></p>
      </media>
      <p>Programmenyn, som finns placerad bredvid knappen <gui>Aktiviteter</gui>, visar namnet på det aktiva programmet intill dess ikon och erbjuder snabb tillgång till fönster och programdetaljer, samt möjlighet att avsluta.</p>
    </if:when>
    <!-- TODO: check how the app menu removal affects classic mode -->
    <if:when test="platform:gnome-classic">
      <media type="image" src="figures/shell-appmenu-classic.png" width="250" style="floatend floatright" if:test="!target:mobile">
        <p>Programmenyn för <app>Terminal</app></p>
      </media>
      <p>Programmenyn, som finns placerad bredvid menyerna <gui>Program</gui> och <gui>Platser</gui>, visar namnet på det aktiva programmet intill dess ikon och erbjuder snabb tillgång till programinställningar eller hjälp. Vilka objekt som finns tillgängliga i programmenyn beror på programmet.</p>
    </if:when>
  </if:choose>

</section>

<section id="clock">
  <title>Klocka, kalender och möten</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <media type="image" src="figures/shell-appts.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Klocka, kalender, möten och aviseringar</p>
    </media>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-appts-classic.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Klocka, kalender och möten</p>
    </media>
  </if:when>
</if:choose>

  <p>Klicka på klockan i systemraden för att se det aktuella datumet, en månadskalender och en lista på dina kommande möten och nya aviseringar. Du kan också öppna kalendern genom att trycka <keyseq><key>Super</key><key>M</key></keyseq>. Du kan nå datum- och tidsinställningar och öppna hela ditt kalenderprogram direkt från menyn.</p>

  <list style="compact">
    <item>
      <p><link xref="clock-calendar">Läs mer om kalendern och möten.</link></p>
    </item>
    <item>
      <p><link xref="shell-notifications">Läs mer om aviseringar och aviseringslistan.</link></p>
    </item>
  </list>

</section>


<section id="systemmenu">
  <title>Systemmeny</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <media type="image" src="figures/shell-exit.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Användarmeny</p>
    </media>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-exit-classic.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Användarmeny</p>
    </media>
  </if:when>
</if:choose>

  <p>Klicka på systemmenyn i övre högra hörnet för att hantera dina systeminställningar och din dator.</p>

<!-- TODO: Update for 3.36 UI option "Do Not Disturb" in calendar dropdown:

<p>If you set yourself to Unavailable, you won’t be bothered by message popups
at the bottom of your screen. Messages will still be available in the message
tray when you move your mouse to the bottom-right corner. But only urgent
messages will be presented, such as when your battery is critically low.</p>
-->

  <p>När du lämnar din dator kan du låsa din skärm för att förhindra att andra människor använder den. Du kan också snabbt växla användare utan att logga ut helt för att ge någon annan tillgång till datorn eller så kan du försätta datorn i vänteläge eller stänga av den från menyn. Om du har en skärm som stöder vertikal eller horisontell rotation kan du snabbt rotera skärmen från systemmenyn. Om din skärm inte stöder rotation kommer du inte att se knappen.</p>

  <list style="compact">
    <item>
      <p><link xref="shell-exit">Läs mer om att växla användare, logga ut och stänga av din dator.</link></p>
    </item>
  </list>

</section>

<section id="lockscreen">
  <title>Låsskärmen</title>

  <p>När du låser din skärm eller den låses automatiskt så visas låsskärmen. Förutom att skydda ditt skrivbord medan du är borta från datorn så visar låsskärmen datum och tid. Den visar också information om din batteri- och nätverksstatus.</p>

  <list style="compact">
    <item>
      <p><link xref="shell-lockscreen">Läs mer om låsskärmen.</link></p>
    </item>
  </list>

</section>

<section id="window-list">
  <title>Fönsterlist</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <p>GNOME erbjuder ett annat sätt att växla mellan fönster än en permanent synlig fönsterlist som brukar finnas i andra skrivbordsmiljöer. Detta låter dig fokusera på dina uppgifter utan distraktioner.</p>
    <list style="compact">
      <item>
        <p><link xref="shell-windows-switching">Läs mer om att växla fönster</link></p>
      </item>
    </list>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-window-list-classic.png" width="800" if:test="!target:mobile">
      <p>Fönsterlist</p>
    </media>
    <p>Fönsterlisten längst ner på skärmen ger tillgång till alla dina öppna fönster och program och låter dig snabbt minimera och återställa dem.</p>
    <p>På höger sida av fönsterlisten visar GNOME de fyra arbetsytorna. Välj arbetsytan du vill använda för att växla till den.</p>
  </if:when>
</if:choose>

</section>

</page>
