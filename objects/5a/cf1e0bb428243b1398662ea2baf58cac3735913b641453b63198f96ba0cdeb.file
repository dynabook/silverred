<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="processes" xml:lang="ca">

  <info>
    <link type="guide" xref="software#management"/>
    <link type="guide" xref="sundry#session"/>
    <revision pkgversion="3.12" date="2014-06-17" status="review"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Sindhu S</name>
      <email>sindhus@live.in</email>
    </credit>
    <credit type="editor">
      <name>Aruna Sankaranarayanan</name>
      <email>aruna.evam@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <desc>Quins processos hauria d’esperar veure en una sessió neta de GNOME?</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

  <title>Processos típics</title>

  <p>In a stock <app>GNOME</app> session, programs called daemons or services
  run on the system as background processes. You should find the following
  daemons running by default:</p>

   <terms>
     <item>
       <title>dbus-daemon</title>
       <p>El <app>dbus-daemon</app> proporciona un dimoni que els programes poden utilitzar per a l'intercanvi de missatges <app>dbus-daemon</app> està implementat amb la llibreria D-Bus que proporciona comunicació entre dues aplicacions.</p>
       <p>For extended information, see the man page for
       <link href="man:dbus-daemon">dbus-daemon</link>.</p>
     </item>
     <item>
       <title>gnome-keyring-daemon</title>
       <p>Credencials com nom d'usuari i contrasenya per a diversos programes i lloca web s'emmagatzemen de forma segura  utilitzant <app>gnome-keyring-daemon</app>. Aquesta informació s’escriu en un fitxer xifrat anomenat fitxer clauer i desat al directori de l’usuari.</p>
       <p>For extended information, see the man page for
       <link its:translate="no" href="man:gnome-keyring-daemon">gnome-keyring-daemon</link>.
       </p>
     </item>
     <item>
       <title>gnome-session</title>
       <p>L'aplicació <app>gnome-session</app> és responsable d'executar l'entorn d'escriptori de GNOME amb l'ajuda d'un gestor de visualització tipus <app>GDM</app>, <app>LightDM</app>, o <app>NODM</app>. La sessió per defecte de l'usuari es configura en el moment de la instal·lació per l'administrador del sistema. Normalment <app>gnome-session</app> carrega la darrera sessió que va funcionar correctament al sistema.</p>
       <p>For extended information, see the man page for
       <link its:translate="no" href="man:gnome-session">gnome-session</link>.
       </p>
     </item>
     <item>
       <title>gnome-settings-daemon</title>
       <p><app>gnome-settings-daemon</app> gestiona la configuració d'una sessió de GNOME i de tots els programes que s'hi executen.</p>
       <p>For extended information, see the man page for
       <link its:translate="no" href="man:gnome-settings-daemon">gnome-settings-daemon</link>.
       </p>
     </item>
     <item>
       <title>gnome-shell</title>
       <p><app>gnome-shell</app> proporciona les funcionalitats bàsiques de la interfície d'usurari de GNOME com el llançament d'aplicacions, navegació per directoris, visualització de fitxers, etc.</p>
       <p>For extended information, see the
       man page for <link its:translate="no" href="man:gnome-shell">gnome-shell</link>.
       </p>
     </item>
     <item>
       <title>pulseaudio</title>
       <p><app>PulseAudio</app> és un servidor de so per a Linux, POSIX i sistemes Windows que permet als programes generar àudio via el dimoni <app>Pulseaudio</app>.</p>
       <p>For extended information, see the man page for
       <link its:translate="no" href="man:pulseaudio">pulseaudio</link>.</p>
     </item>
   </terms>

  <p>Depenent de la configuració de l'usuari, també podreu veure alguns dels següents, entre d'altres:</p>
   <list ui:expanded="false">
   <title>Processos addicionals</title>
     <item><p><app>at-spi2-dbus-launcher</app></p></item>
     <item><p><app>at-spi2-registryd</app></p></item>
     <item><p><app>gnome-screensaver</app></p></item>
     <item><p><app>gnome-shell-calendar-server</app></p></item>
     <item><p><app>goa-daemon</app></p></item>
     <item><p><app>gsd-printer</app></p></item>
     <item><p>diversos processos de fàbrica d'<app>Evolution</app></p></item>
     <item><p>diversos processos <app>GVFS</app></p></item>
   </list>

</page>
