<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="sound-volume" xml:lang="lv">

  <info>
    <link type="guide" xref="media#sound"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Izvēlieties skaņas līmeni datoram un kontrolējiet katras lietotnes skaļumu.</desc>
  </info>

<title>Mainiet skaņas līmeni</title>

  <p>To change the sound volume, open the
  <gui xref="shell-introduction#systemmenu">system menu</gui> from the right
  side of the top bar and move the volume slider left or right. You can
  completely turn off sound by dragging the slider to the left.</p>

  <p>Dažām tastatūrām ir taustiņi, kuri ļauj kontrolēt skaļumu. Tie parasti izskatās pēc stilizētiem skaļruņiem ar skaņas viļņiem. Tie parasti ir tuvu “F” taustiņiem augšpusē. Uz klēpjdatoru tastatūrām šīs pogas parasti atrodas tieši uz “F” taustiņiem. Turiet piespiestu <key>Fn</key> taustiņu uz tastatūras, lai tās lietotu.</p>

  <p>Ja ir ārējie skaļruņi, varat mainīt skaļumu, izmantojot skaļruņu skaļuma kontroles pogas. Dažām austiņām arī ir skaļuma kontrole.</p>

<section id="apps">
 <title>Skaļuma mainīšana atsevišķām lietotnēm</title>

  <p>Varat mainīt vienas lietotnes skaļumu, nenomainot citu lietotņu skaļumus. Tas ir noderīgi, ja, piemēram, klausāties mūziku un sērfojat internetā. Jūs varētu vēlēties izslēgt skaņu no interneta pārlūka, lai skaņas no mājaslapām nepārtrauktu mūziku.</p>

  <p>Dažām lietotnēm ir skaļuma kontroles to galvenajos logos. Ja lietotnei ir skaļuma vadīklas, izmantojiet tās, lai mainītu skaļumu. Ja nav:</p>

    <steps>
    <item>
      <p>Atveriet <gui xref="shell-introduction#activities">Aktivitāšu</gui> pārskatu un sāciet rakstīt <gui>Skaņa</gui>.</p>
    </item>
    <item>
      <p>Spiediet <gui>Skaņa</gui>, lai atvērtu paneli.</p>
    </item>
    <item>
      <p>Under <gui>Volume Levels</gui>, change the volume of the application
      listed there.</p>

  <note style="tip">
    <p>Tikai lietotnes, kuras tobrīd atskaņo, tiks tur uzskaitītas. Ja lietotne atskaņo, bet nav tur uzskaitīta, tā varētu neatbalstīt šādu iespēju. Šādā gadījumā jūs nevarat nomainīt tās skaļumu.</p>
  </note>
    </item>

  </steps>

</section>

</page>
