<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-switch-tasks" version="1.0 if/1.0" xml:lang="hu">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>
    <link type="guide" xref="getting-started" group="videos"/>
    <title role="trail" type="link">Feladatváltás</title>
    <link type="seealso" xref="shell-windows-switching"/>
    <title role="seealso" type="link">Ismertető a feladatváltáshoz</title>
    <link type="next" xref="gs-use-windows-workspaces"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2013, 2015, 2018.</mal:years>
    </mal:credit>
  </info>

  <title>Feladatváltás</title>

  <ui:overlay width="812" height="452">
  <media type="video" its:translate="no" src="figures/gnome-task-switching.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-task-switching.svg"/>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>Feladatváltás</tt:p>
         </tt:div>
         <tt:div begin="5s" end="8s">
           <tt:p if:test="!platform:gnome-classic">Vigye az egérmutatót a képernyő bal felső sarkában lévő <gui>Tevékenységek</gui> feliratra.</tt:p>
           </tt:div>
         <tt:div begin="9s" end="12s">
           <tt:p>Kattintson egy ablakra az adott feladatra váltáshoz.</tt:p>
         </tt:div>
         <tt:div begin="12s" end="14s">
           <tt:p>Egy ablak a bal képernyőfélre való teljes méretűvé tételéhez fogja meg a címsorát, és húzza a képernyő bal széléhez.</tt:p>
         </tt:div>
         <tt:div begin="14s" end="16s">
           <tt:p>Amikor a képernyő fele kiemelésre kerül, ejtse rá az ablakot.</tt:p>
         </tt:div>
         <tt:div begin="16s" end="18">
           <tt:p>Egy ablak a jobb képernyőfélre való teljes méretűvé tételéhez fogja meg a címsorát, és húzza a képernyő jobb széléhez.</tt:p>
         </tt:div>
         <tt:div begin="18s" end="20s">
           <tt:p>Amikor a képernyő fele kiemelésre kerül, ejtse rá az ablakot.</tt:p>
         </tt:div>
         <tt:div begin="23s" end="27s">
           <tt:p>Nyomja meg a <keyseq><key href="help:gnome-help/keyboard-key-super">Super</key> <key>Tab</key></keyseq> billentyűkombinációt az <gui>ablakváltó</gui> megjelenítéséhez.</tt:p>
         </tt:div>
         <tt:div begin="27s" end="29s">
           <tt:p>Engedje fel a <key href="help:gnome-help/keyboard-key-super">Super</key> billentyűt a következő kiemelt ablak kiválasztásához.</tt:p>
         </tt:div>
         <tt:div begin="29s" end="32s">
           <tt:p>A nyitott ablakok listáján való végiglépkedéshez ne engedje fel a <key href="help:gnome-help/keyboard-key-super">Super</key> billentyűt, hanem tartsa lenyomva, és nyomja meg a <key>Tab</key> billentyűt.</tt:p>
         </tt:div>
         <tt:div begin="35s" end="37s">
           <tt:p>Nyomja meg a <key href="help:gnome-help/keyboard-key-super">Super</key> billentyűt a <gui>Tevékenységek áttekintés</gui> megjelenítéséhez.</tt:p>
         </tt:div>
         <tt:div begin="37s" end="40s">
           <tt:p>Kezdje el beírni az alkalmazás nevét az átváltáshoz.</tt:p>
         </tt:div>
         <tt:div begin="40s" end="43s">
           <tt:p>Amikor az alkalmazás megjelenik első találatként, nyomja meg az <key>Enter</key> billentyűt az átváltáshoz.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

  <section id="switch-tasks-overview">
    <title>Feladatváltás</title>

<if:choose>
<if:when test="!platform:gnome-classic">

  <steps>
    <item><p>Vigye az egérmutatót a képernyő bal felső sarkában lévő <gui>Tevékenységek</gui> feliratra a <gui>Tevékenységek áttekintés</gui> megjelenítéséhez, ahol az épp futó feladatokat kis ablakokként láthatja.</p></item>
     <item><p>Kattintson egy ablakra az adott feladatra váltáshoz.</p></item>
  </steps>

</if:when>
<if:when test="platform:gnome-classic">

  <steps>
    <item><p>A feladatok közt a képernyő alján lévő <gui>ablaklistával</gui> válthat. A nyitott feladatok gombokként jelennek meg az <gui>ablaklistában</gui>.</p></item>
     <item><p>Kattintson egy gombra az <gui>ablaklistában</gui> az adott feladatra váltáshoz.</p></item>
  </steps>

</if:when>
</if:choose>

  </section>

  <section id="switching-tasks-tiling">
    <title>Ablakok elrendezése</title>
    
    <steps>
      <item><p>Egy ablak valamelyik képernyőfélre való teljes méretűvé tételéhez fogja meg a címsorát, és húzza a képernyő bal vagy jobb széléhez.</p></item>
      <item><p>Amikor a képernyő fele kiemelésre kerül, engedje el az ablakot a maximalizálásához az adott képernyőoldal mentén.</p></item>
      <item><p>Két ablak egymás melletti teljes méretűvé tételéhez fogja meg a második ablak címsorát, és húzza a képernyő másik oldalára.</p></item>
       <item><p>Amikor a képernyő fele kiemelésre kerül, engedje el az ablakot a maximalizálásához a szemközti képernyőoldal mentén.</p></item>
    </steps>
    
  </section>
  
  <section id="switch-tasks-windows">
    <title>Váltás ablakok között</title>
    
  <steps>
   <item><p>Nyomja meg a <keyseq><key href="help:gnome-help/keyboard-key-super">Super</key> <key>Tab</key></keyseq> billentyűkombinációt az <gui>ablakváltó</gui> megjelenítéséhez, amely felsorolja az épp nyitott ablakokat.</p></item>
   <item><p>Engedje fel a <key href="help:gnome-help/keyboard-key-super">Super</key> billentyűt a következő kiemelt ablak kiválasztásához az <gui>ablakváltóban</gui>.</p>
   </item>
   <item><p>A nyitott ablakok listáján való végiglépkedéshez ne engedje fel a <key href="help:gnome-help/keyboard-key-super">Super</key> billentyűt, hanem tartsa lenyomva, és nyomja meg a <key>Tab</key> billentyűt.</p></item>
  </steps>

  </section>

  <section id="switch-tasks-search">
    <title>Váltás az alkalmazások között kereséssel</title>
    
    <steps>
      <item><p>Nyomja meg a <key href="help:gnome-help/keyboard-key-super">Super</key> billentyűt a <gui>Tevékenységek áttekintés</gui> megjelenítéséhez.</p></item>
      <item><p>Kezdje el beírni az alkalmazás nevét az átváltáshoz. A begépelt szövegnek megfelelő alkalmazások megjelennek a gépelés közben.</p></item>
      <item><p>Amikor a kívánt alkalmazás megjelenik első találatként, nyomja meg az <key>Enter</key> billentyűt az átváltáshoz.</p></item>
      
    </steps>
    
  </section>

</page>
