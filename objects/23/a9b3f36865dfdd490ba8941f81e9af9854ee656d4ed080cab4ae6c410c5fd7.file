<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-use-windows-workspaces" xml:lang="ca">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>
    <link type="guide" xref="getting-started" group="tasks"/>
    <title role="trail" type="link">Usar finestres i espais de treball</title>
    <link type="seealso" xref="shell-windows-switching"/>
    <title role="seealso" type="link">Un programa d'aprenentatge per utilitzar les finestres i els espais de treball</title>
    <link type="next" xref="gs-use-system-search"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Manel Vidal</mal:name>
      <mal:email>verduler@gmail.com</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  </info>

  <title>Utilitzar finestres i espais de treball</title>

  <ui:overlay width="812" height="452">
  <media type="video" its:translate="no" src="figures/gnome-windows-and-workspaces.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-windows-and-workspaces.svg"/>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>Finestres i espais de treball</tt:p>
         </tt:div>
         <tt:div begin="6s" end="10s">
           <tt:p>Per maximitzar una finestra, agafeu la finestra per la barra del títol i arrossegueu-la cap a la part superior de la pantalla.</tt:p>
           </tt:div>
         <tt:div begin="10s" end="13s">
           <tt:p>Quan es realci la pantalla, deixeu anar la finestra.</tt:p>
         </tt:div>
         <tt:div begin="14s" end="20s">
           <tt:p>Per desmaximitzar una finestra, agafeu la finestra per la barra del títol i arrossegueu-la cap al centre de la pantalla.</tt:p>
         </tt:div>
         <tt:div begin="25s" end="29s">
           <tt:p>També podeu desmaximitzar la finestra si feu clic en la barra superior i arrossegueu la finestra cap al centre de la pantalla.</tt:p>
         </tt:div>
         <tt:div begin="34s" end="38s">
           <tt:p>Per maximitzar una finestra i que ocupi la part esquerra de la pantalla, agafeu la finestra per la barra del títol i arrossegueu-la cap a l'esquerra.</tt:p>
         </tt:div>
         <tt:div begin="38s" end="40s">
           <tt:p>Quan es realci la meitat de la pantalla, deixeu anar la finestra.</tt:p>
         </tt:div>
         <tt:div begin="41s" end="44s">
           <tt:p>Per maximitzar una finestra i que ocupi la part dreta de la pantalla, agafeu la finestra per la barra del títol i arrossegueu-la cap a la dreta.</tt:p>
         </tt:div>
         <tt:div begin="44s" end="48s">
           <tt:p>Quan es realci la meitat de la pantalla, deixeu anar la finestra.</tt:p>
         </tt:div>
         <tt:div begin="54s" end="60s">
           <tt:p>Per maximitzar una finestra mitjançant el teclat, mantingueu premuda la tecla <key href="help:gnome-help/keyboard-key-super">Súper</key> i premeu <key>↑</key>.</tt:p>
         </tt:div>
         <tt:div begin="61s" end="66s">
           <tt:p>Per desmaximitzar la finestra mantingueu premuda la tecla <key href="help:gnome-help/keyboard-key-super">Súper</key> i premeu <key>↓</key>.</tt:p>
         </tt:div>
         <tt:div begin="66s" end="73s">
           <tt:p>Per maximitzar una finestra i que ocupi la part dreta de la pantalla, mantingueu premuda la tecla <key href="help:gnome-help/keyboard-key-super">Súper</key> i premeu <key>→</key>.</tt:p>
         </tt:div>
         <tt:div begin="76s" end="82s">
           <tt:p>Per maximitzar una finestra i que ocupi la part esquerra de la pantalla, mantingueu premuda la tecla <key href="help:gnome-help/keyboard-key-super">Súper</key> i premeu <key>←</key>.</tt:p>
         </tt:div>
         <tt:div begin="83s" end="89s">
           <tt:p>Si voleu anar a un espai de treball que es troba a sota de l'espai de treball actual, premeu <keyseq><key href="help:gnome-help/keyboard-key-super">Súper</key><key>Av Pàg</key></keyseq>.</tt:p>
         </tt:div>
         <tt:div begin="90s" end="97s">
           <tt:p>Si voleu anar a un espai de treball que es troba a sobre de l'espai de treball actual, premeu <keyseq><key href="help:gnome-help/keyboard-key-super">Súper</key><key>Re Pàg</key></keyseq>.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>
  
  <section id="use-workspaces-and-windows-maximize">
    <title>Maximitzeu i desmaximitzeu les finestres</title>
    <p/>
    
    <steps>
      <item><p>Per maximitzar una finestra, de forma que ocupi tot l'espai de l'escriptori, agafeu la finestra per la barra del títol i arrossegueu-la cap a la part superior de la pantalla.</p></item>
      <item><p>Quan es realci la pantalla, deixeu anar la finestra.</p></item>
      <item><p>Per restaurar la mida de la finestra minimitzada, agafeu la finestra per la barra del títol i arrossegueu-la cap al centre de la pantalla.</p></item>
    </steps>
    
  </section>

  <section id="use-workspaces-and-windows-tile">
    <title>Organitzar les finestres en mosaic</title>
    <p/>
    
    <steps>
      <item><p>Per maximitzar una finestra de forma que ocupi la meitat de la pantalla, agafeu la finestra per la barra del títol i arrossegueu-la cap a la part dreta o esquerra de la pantalla.</p></item>
      <item><p>Quan es realci la meitat de la pantalla, deixeu-la anar. La finestra ocuparà la meitat escollida de la pantalla.</p></item>
      <item><p>Per maximitzar dues finestres, una al costat de l'altra, agafeu la segona finestra per la barra del títol i arrossegueu-la cap a la meitat oposada de la pantalla.</p></item>
       <item><p>Quan es realci la meitat de la pantalla, deixeu-la anar. La finestra ocuparà la meitat oposada de la pantalla.</p></item>
    </steps>
    
  </section>
  
  <section id="use-workspaces-and-windows-maximize-keyboard">
    <title>Maximitzeu i desmaximitzeu les finestres mitjançant el teclat</title>
    
    <steps>
      <item><p>Per maximitzar una finestra mitjançant el teclat, mantingueu premuda la tecla <key href="help:gnome-help/keyboard-key-super">Súper</key> i premeu <key>↑</key>.</p></item>
      <item><p>Per desmaximitzar una finestra utilitzant el teclat, mantingueu premuda la tecla <key href="help:gnome-help/keyboard-key-super">Súper</key> i premeu <key>↓</key>.</p></item>
    </steps>
    
  </section>
  
  <section id="use-workspaces-and-windows-tile-keyboard">
    <title>Organitzar les finestres en mosaic mitjançant el teclat</title>
    
    <steps>
      <item><p>Per maximitzar una finestra i que ocupi la part dreta de la pantalla, mantingueu premuda la tecla <key href="help:gnome-help/keyboard-key-super">Súper</key> i premeu <key>→</key>.</p></item>
      <item><p>Per maximitzar una finestra i que ocupi la part esquerra de la pantalla, mantingueu premuda la tecla <key href="help:gnome-help/keyboard-key-super">Súper</key> i premeu <key>←</key>.</p></item>
    </steps>
    
  </section>
  
  <section id="use-workspaces-and-windows-workspaces-keyboard">
    <title>Canviar d'espai de treball mitjançant el teclat</title>
    
    <steps>
    
    <item><p>Si voleu anar a un espai de treball que es troba a sota de l'espai de treball actual, premeu <keyseq><key href="help:gnome-help/keyboard-key-super">Súper</key><key>Av Pàg</key></keyseq>.</p></item>
    <item><p>Si voleu anar a un espai de treball que es troba a sobre de l'espai de treball actual, premeu <keyseq><key href="help:gnome-help/keyboard-key-super">Súper</key><key>Re Pàg</key></keyseq>.</p></item>

    </steps>
    
  </section>

</page>
