<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-screen-lock" xml:lang="el">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="seealso" xref="session-screenlocks"/>
    <link type="seealso" xref="shell-exit#lock-screen"/>

    <revision pkgversion="3.8" date="2013-05-21" status="candidate"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit>
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Αποτρέψτε άλλα άτομα από τη χρήση της επιφάνειας εργασίας όταν φεύγετε από τον υπολογιστή σας.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Αυτόματο κλείδωμα της οθόνης</title>
  
  <p>When you leave your computer, you should
  <link xref="shell-exit#lock-screen">lock the screen</link> to prevent
  other people from using your desktop and accessing your files. If you
  sometimes forget to lock your screen, you may wish to have your computer’s
  screen lock automatically after a set period of time. This will help to
  secure your computer when you aren’t using it.</p>

  <note><p>Όταν η οθόνη σας είναι κλειδωμένη, οι εφαρμογές σας και οι διεργασίες του συστήματος θα συνεχίσουν να εκτελούνται, αλλά θα χρειαστείτε να εισάγετε τον κωδικό πρόσβασής σας για να αρχίσετε να τις χρησιμοποιείτε ξανά.</p></note>
  
  <steps>
    <title>Για να ορίσετε τη διάρκεια του χρόνου πριν να κλειδωθεί η οθόνη σας αυτόματα:</title>
    <item>
      <p>Ανοίξτε την επισκόπηση <gui xref="shell-introduction#activities">Δραστηριότητες</gui> και αρχίστε να πληκτρολογείτε <gui>Ιδιωτικότητα</gui>.</p>
    </item>
    <item>
      <p>Κάντε κλικ στο <gui>Ιδιωτικότητα</gui> για να ανοίξετε τον πίνακα.</p>
    </item>
    <item>
      <p>Επιλέξτε <gui>Κλείδωμα οθόνης</gui>.</p>
    </item>
    <item>
      <p>Make sure <gui>Automatic Screen Lock</gui> is switched on, then select
      a length of time from the drop-down list.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Applications can present notifications to you that are still displayed
    on your lock screen. This is convenient, for example, to see if you have
    any email without unlocking your screen. If you’re concerned about other
    people seeing these notifications, switch <gui>Show Notifications</gui>
    off.</p>
  </note>

  <p>Όταν η οθόνη σας είναι κλειδωμένη και θέλετε να την ξεκλειδώσετε, πατήστε το πλήκτρο <key>Esc</key>, ή σύρετε το ποντίκι σας από πάνω έως κάτω. Έπειτα εισάγετε τον κωδικό πρόσβασής σας και πατήστε το πλήκτρο <key>Enter</key> ή κάντε κλικ στο <gui>Ξεκλείδωμα</gui>. Εναλλακτικά, αρχίστε απλά να πληκτρολογείτε τον κωδικό πρόσβασής σας και θα αναιρεθεί αυτόματα το κλείδωμα καθώς πληκτρολογείτε.</p>

</page>
