<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-autosuspend" xml:lang="sr-Latn">

  <info>
     <link type="guide" xref="power#saving"/>
     <link type="seealso" xref="power-suspend"/>
     <link type="seealso" xref="shell-exit#suspend"/>

    <revision pkgversion="3.20" date="2016-06-15" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <credit type="author copyright">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
      <years>2016</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Podesite vaš računar da sam obustavlja rad.</desc>
  </info>

  <title>Podesite automatsko obustavljanje</title>

  <p>Možete podesiti računar da sam obustavi rad kada miruje. Različiti intervali mogu biti navedeni kada radi na bateriji ili kada se napaja iz mreže.</p>

  <steps>

    <item>
      <p>Otvorite pregled <gui xref="shell-introduction#activities">Aktivnosti</gui> i počnite da kucate <gui>Napajanje</gui>.</p>
    </item>
    <item>
      <p>Kliknite na <gui>Napajanje</gui> da otvorite panel.</p>
    </item>
    <item>
      <p>U odeljku <gui>Obustavljanje i dugme napajanja</gui>, kliknite na <gui>Automatsko obustavljanje</gui>.</p>
    </item>
    <item>
      <p>Choose <gui>On Battery Power</gui> or <gui>Plugged In</gui>, set the
      switch to on, and select a <gui>Delay</gui>. Both options can
      be configured.</p>

      <note style="tip">
        <p>Na stonim računarima, ima jedna opcija sa natpisom <gui>Kada miruje</gui>.</p>
      </note>
    </item>

  </steps>

</page>
