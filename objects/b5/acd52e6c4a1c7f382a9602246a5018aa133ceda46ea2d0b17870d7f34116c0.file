<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-apps-favorites" xml:lang="hu">

  <info>
    <link type="seealso" xref="shell-apps-open"/>
    <link type="guide" xref="shell-overview#desktop"/>

    <revision pkgversion="3.6.0" date="2012-10-14" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>GNOME dokumentációs projekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Gyakran használt programikonok hozzáadása az indítópanelhez.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Kedvenc alkalmazások hozzáadása az indítópanelhez</title>

  <p>Alkalmazás hozzáadása az <link xref="shell-introduction#activities">indítópanelhez</link> a könnyebb hozzáférés érdekében:</p>

  <steps>
    <item>
      <p if:test="!platform:gnome-classic">Nyissa meg a <gui xref="shell-introduction#activities">Tevékenységek</gui> áttekintést a képernyő bal felső sarkában lévő <gui>Tevékenységek</gui> gombra kattintással.</p>
      <p if:test="platform:gnome-classic">Kattintson a képernyő bal felső sarkában lévő <gui xref="shell-introduction#activities">Alkalmazások</gui> menüre, és válassza a <gui>Tevékenységek áttekintés</gui> menüpontot.</p></item>
    <item>
      <p>Kattintson a rács gombra az indítópanel alján, és keresse meg a felvenni kívánt alkalmazást.</p>
    </item>
    <item>
      <p>Kattintson a jobb egérgombbal az alkalmazás ikonjára, és válassza a <gui>Hozzáadás a kedvencekhez</gui> menüpontot.</p>
      <p>Ennek alternatívájaként az ikont át is húzhatja az indítópanelra.</p>
    </item>
  </steps>

  <p>Alkalmazásikon indítópanelről való eltávolításához kattintson a jobb egérgombbal az alkalmazásikonra, és válassza az <gui>Eltávolítás a kedvencekből</gui> menüpontot.</p>

  <note style="tip" if:test="platform:gnome-classic"><p>A kedvenc alkalmazások az <gui xref="shell-introduction#activities">Alkalmazások</gui> menü <gui>Kedvencek</gui> szakaszában is megjelennek.</p>
  </note>

</page>
