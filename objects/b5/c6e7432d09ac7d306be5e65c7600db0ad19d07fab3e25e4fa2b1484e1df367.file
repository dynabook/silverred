<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-install-flash" xml:lang="ca">

  <info>
    <link type="guide" xref="net-browser"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>És possible que necessiteu instal·lar Flash per poder visualitzar llocs web com YouTube, que mostren vídeos i pàgines web interactives.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

  <title>Instal·lar el complement Flash</title>

  <p><app>Flash</app> és un <em>connector</em> pel vostre navegador que permet veure vídeos i utilitzar pàgines web interactives d'algunes webs. Alguns llocs web no funcionaran sense Flash.</p>

  <p>Si no teniu Flash instal·lat, probablement veureu un missatge que us ho indicarà quan visiteu un lloc web que el necessiti. Flash està disponible com a descàrrega gratuïta (però no de codi obert) per a la majoria de navegadors web. La majoria de les distribucions de Linux tenen una versió de Flash que podeu instal·lar a través del seu instal·lador de programari (gestor de paquets).</p>

  <steps>
    <title>Si Flash està disponible des de l'instal·lador de programes:</title>
    <item>
      <p>Obriu l'aplicació de l'instal·lador de programari i cerqueu <input>flash</input>.</p>
    </item>
    <item>
      <p>Cerqueu el <gui>connector d'Adobe Flash</gui>, <gui>Adobe Flash Player</gui> o similar i feu clic a instal·lar.</p>
    </item>
    <item>
      <p>Si teniu obertes les finestres del navegador, tanqueu-les i torneu-les a obrir. El navegador hauria d'adonar-se que Flash s'ha instal·lat en obrir-lo de nou, i ja hauria de poder veure llocs web amb Flash.</p>
    </item>
  </steps>

  <steps>
    <title>Si Flash <em>no està</em> disponible des de l'instal·lador de programes:</title>
    <item>
      <p>Anar al <link href="http://get.adobe.com/flashplayer">lloc de descàrrega de Flash Player</link>. S'hauria de detectar automàticament el navegador i el sistema operatiu.</p>
    </item>
    <item>
      <p>Feu clic on diu <gui>Seleccioneu la versió a descarregar</gui> i trieu el tipus d'instal·lador de programari que funcioni per a la vostra distribució de Linux. Si no sabeu què utilitzar, seleccioneu l'opció <file>.tar.gz</file>.</p>
    </item>
    <item>
      <p>Mireu les <link href="http://kb2.adobe.com/cps/153/tn_15380.html">instruccions d'instal·lació per Flash</link> per aprendre a instal·lar-lo al vostre navegador.</p>
    </item>
  </steps>

<section id="alternatives">
  <title>Alternatives de codi obert a Flash</title>

  <p>Hi ha disponible un grapat d'alternatives de codi obert gratuïtes a Flash. En molts casos tendeixen a funcionar millor que el connector de Flash (per exemple, gestionant la reproducció de so), però pitjor en altres casos (per exemple, no podent mostrar algunes de les pàgines Flash més complicades de la web).</p>

  <p>És possible que en vulgueu provar una d'aquestes alternatives si no esteu satisfet amb el reproductor Flash, o si voleu utilitzar tant programari de codi obert com sigui possible a l'ordinador. Aquestes són algunes de les opcions:</p>

  <list style="compact">
    <item>
      <p>LightSpark</p>
    </item>
    <item>
      <p>Gnash</p>
    </item>
  </list>

</section>

</page>
