<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="fs-diskusage" xml:lang="pt">
  <info>
    <revision version="0.1" date="2014-01-27" status="review"/>
    <link type="guide" xref="index#filesystems" group="filesystems"/>
    <link type="seealso" xref="fs-info"/>
    <link type="seealso" xref="units"/>
    
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author copyright">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
      <years>2014</years>
    </credit>

    <desc>O separador de <gui>Sistemas de ficheiros</gui> mostra quanto espaço se está a usar na cada disco duro.</desc>
  </info>

  <title>Verificar quanto espaço em disco se está a usar</title>
  
  <p>Para verificar quanto espaço livre há disponível num disco, vá ao separador <gui>Sistemas de ficheiros</gui> e olhe as colunas <gui>Usado</gui> e <gui>Disponível</gui>.</p>
  
  <p>Se mostram-se vários dispositivos, e está a procurar o que contém sua pasta pessoal, procure os discos que contenham <file>/</file> ou <file>/home</file> em <gui>Pastas</gui>. Normalmente (mas não sempre) este será o disco que contenha seus ficheiros pessoais.</p>
  
  
  <section id="free-space">
    <title>Libertar espaço em disco</title>
    
    <p>Se não lhe fica muito espaço livre no disco, há algumas coisas que pode fazer para tentar libertar espaço.</p>
    
    <p>Uma delas é eliminar ficheiros desnecessários manualmente. Pode usar o <app>Analizador de uso do disco</app> para ver que ficheiros e pastas ocupam a maior parte do espaço. Após eliminar alguns ficheiros, deve esvaziar a <gui>Caixote do Lixo</gui> para assegurar-se de que se eliminam por completo do computador.</p>
  
    <p>Também pode tirar ficheiros temporários em várias aplicações. Por exemplo, os navegadores site e os gestores de software com frequência guardam muitos ficheiros. (Como os eliminar dependerá da aplicação).</p>
    
    <p>Pode tentar desinstalar alguns aplicações que não queira. Use o seu gestor de pacotes habitual para fazer isto.</p>
    
    <p>Outra maneira de libertar espaço em disco é «archivar» ficheiros antigos, movendo a um disco duro externo ou a um armazenamento em linha em «a nuvem», por exemplo.</p>
  
  </section>

</page>
