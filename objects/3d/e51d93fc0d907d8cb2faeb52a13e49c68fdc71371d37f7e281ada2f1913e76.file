<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="nautilus-bookmarks-edit" xml:lang="ca">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Afegiu, suprimiu i canvieu el nom dels marcadors al gestor de fitxers.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

  <title>Editar els marcadors de la carpeta</title>

  <p>Els vostres marcadors apareixen a la barra lateral del gestor de fitxers.</p>

  <steps>
    <title>Afegir un marcador:</title>
    <item>
      <p>Obriu la carpeta (o ubicació) que voleu marcar.</p>
    </item>
    <item>
      <p>Feu clic al menú de la finestra a la barra d'eines i seleccioneu <gui>Marca aquesta ubicació</gui>.</p>
    </item>
  </steps>

  <steps>
    <title>Esborrar un marcador:</title>
    <item>
      <p>Feu clic amb el botó dret al marcador a la barra lateral i seleccioneu <gui>Suprimeix</gui> del menú.</p>
    </item>
  </steps>

  <steps>
    <title>Reanomenar un marcador:</title>
    <item>
      <p>Feu clic amb el botó dret al marcador a la barra lateral i seleccioneu <gui>Canvia el nom…</gui>.</p>
    </item>
    <item>
      <p>A la caixa de text <gui>Nom</gui>, escriviu el nou nom del marcador.</p>
      <note>
        <p>Canviar el nom d'un marcador no canvia el nom de la carpeta. Si teniu adreces d'interès a dues carpetes diferents en dues ubicacions diferents, però que tenen el mateix nom, les adreces d'interès tindran el mateix nom i no podreu separar-les. En aquests casos, és útil donar a un marcador un nom que no sigui el nom de la carpeta al qual apunta.</p>
      </note>
    </item>
  </steps>

</page>
