<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="color-whyimportant" xml:lang="fr">

  <info>
    <link type="guide" xref="color"/>
    <desc>Gérer les couleurs est primordial pour les designers, les photographes et les artistes.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Importance de la gestion des couleurs</title>
  <p>La gestion des couleurs est le processus consistant à capter une couleur à l'aide d'un périphérique d'entrée, de l'afficher sur un écran et de l'imprimer tout en conservant la maîtrise des couleurs exactes et de la gamme de couleurs sur chaque média.</p>

  <p>La nécessité de la gestion des couleurs est probablement le mieux exprimée par la photo d'un oiseau par une journée de gel en hiver.</p>

  <figure>
    <desc>Image d'un oiseau sur un mur gelé vue sur l'écran d'un appareil photo</desc>
    <media its:translate="no" type="image" mime="image/png" src="figures/color-camera.png"/>
  </figure>

  <p>Les écrans saturent typiquement trop le canal bleu, donnant une couleur froide à l'image.</p>

  <figure>
    <desc>Affichage typique sur l'écran d'un ordinateur portable</desc>
    <media its:translate="no" type="image" mime="image/png" src="figures/color-display.png"/>
  </figure>

  <p>
    Notice how the white is not “paper white” and the black of the eye
    is now a muddy brown.
  </p>

  <figure>
    <desc>Après impression sur une imprimante à jet d'encre standard</desc>
    <media its:translate="no" type="image" mime="image/png" src="figures/color-printer.png"/>
  </figure>

  <p>Le principal problème que nous rencontrons ici est que chaque périphérique utilise une gamme de couleurs différente. Donc, même si vous savez prendre une photo bleue électrique, la plupart des imprimantes sont incapables de la reproduire.</p>
  <p>
    Most image devices capture in RGB (Red, Green, Blue) and have
    to convert to CMYK (Cyan, Magenta, Yellow, and Black) to print.
    Another problem is that you can’t have <em>white</em> ink, and so
    the whiteness can only be as good as the paper color.
  </p>

  <p>
    Another problem is units. Without specifying the scale on which a
    color is measured, we don’t know if 100% red is near infrared or
    just the deepest red ink in the printer. What is 50% red on one
    display is probably something like 62% on another display.
    It’s like telling a person that you’ve just driven 7 units of
    distance, without the unit you don’t know if that’s 7 kilometers or
    7 meters.
  </p>

  <p>
    In color, we refer to the units as gamut. Gamut is essentially the
    range of colors that can be reproduced.
    A device like a DSLR camera might have a very large gamut, being able
    to capture all the colors in a sunset, but a projector has a very
    small gamut and all the colors are going to look “washed out”.
  </p>

  <p>
    In some cases we can <em>correct</em> the device output by altering
    the data we send to it, but in other cases where that’s not
    possible (you can’t print electric blue) we need to show the user
    what the result is going to look like.
  </p>

  <p>
    For photographs it makes sense to use the full tonal range of a color
    device, to be able to make smooth changes in color.
    For other graphics, you might want to match the color exactly, which
    is important if you’re trying to print a custom mug with the Red Hat
    logo that <em>has</em> to be the exact Red Hat Red.
  </p>

</page>
