<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="net-macaddress" xml:lang="pl">

  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Unikalny identyfikator przydzielany sprzętowi sieciowemu.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  </info>

  <title>Czym jest adres MAC?</title>

  <p><em>Adres MAC</em> to unikalny identyfikator przydzielany sprzętowi sieciowemu (np. karcie bezprzewodowej lub karcie Ethernet) przez producenta. MAC to skrót od <em>Media Access Control</em>, a każdy identyfikator jest unikalny dla konkretnego urządzenia.</p>

  <p>Adres MAC składa się z sześciu zestawów po dwa znaki, każdy oddzielony dwukropkiem, np. <code>00:1B:44:11:3A:B7</code>.</p>

  <p>Aby zidentyfikować adres MAC własnego sprzętu sieciowego:</p>

  <steps>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Sieć</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Sieć</gui>, aby otworzyć panel.</p>
    </item>
    <item>
      <p>Wybierz urządzenie, <gui>Wi-Fi</gui> lub <gui>Przewodowe</gui>, z lewego panelu.</p>
      <p>Adres MAC urządzenia przewodowego jest wyświetlany jako <gui>Adres sprzętowy</gui> po prawej.</p>
      
      <p>Kliknij przycisk <media its:translate="no" type="image" src="figures/emblem-system.png"><span its:translate="yes">ustawienia</span></media>, aby wyświetlić adres MAC urządzenia bezprzewodowego, wyświetlany jako <gui>Adres sprzętowy</gui> w panelu <gui>Informacje</gui>.</p>
    </item>
  </steps>

  <p>Czasami potrzeba zmodyfikować adres MAC. Na przykład, niektórzy dostawcy Internetu wymagają konkretnego adresu MAC, aby używać ich usług. Jeśli karta sieciowa się zepsuła i trzeba ją wymienić, to Internet przestanie działać. W takim wypadku należy zmienić adres MAC.</p>

</page>
