<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-calibrate-camera" xml:lang="fi">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-calibrationtargets"/>
    <link type="seealso" xref="color-calibrate-printer"/>
    <link type="seealso" xref="color-calibrate-scanner"/>
    <link type="seealso" xref="color-calibrate-screen"/>
    <desc>Digitaalikameran kalibrointi on tärkeää värien tarkan tallennuksen vuoksi.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

  <title>Kuinka kalibroin kamerani?</title>

  <p>Kamerat kalibroidaan yleisesti ottamalla valokuva kohteesta hyvissä valo-olosuhteissa. Kun RAW-kuva muunnetaan TIFF-tiedostoksi, sitä voidaan käyttää kameran kalibrointiin värienhallinnassa.</p>
  <p>TIFF-kuva täytyy rajata siten, että vain kohde on näkyvissä. Varmista, että valkoiset tai mustat reunaviivat ovat näkyvissä. Kalibrointi ei toimi, jos kuva on ylösalaisin tai vääristynyt suuresti.</p>

  <note style="tip">
    <p>Valmis profiili on kelvollinen vain siinä valaistuksessa, jossa alkuperäinen kuva on otettu. Tämä tarkoittaa, että profilointi saattaa olla tarpeen tehdä useaan kertaan <em>studiokäyttöä</em>, <em>kirkasta päivänvaloa</em>, <em>pilvistä säätä</em> jne. varten.</p>
  </note>

</page>
