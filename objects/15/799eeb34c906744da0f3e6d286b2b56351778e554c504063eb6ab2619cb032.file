<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="clock-world" xml:lang="es">

  <info>
    <link type="guide" xref="clock" group="#last"/>
    <link type="seealso" href="help:gnome-clocks/index"/>

    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.28" date="2018-07-30" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhill@gnome.org</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Mostra rla hora de otras ciudades debajo del calendario.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Añadir un reloj del mundo</title>

  <p>Use <app>Relojes</app> para añadir la hora de otras ciudades.</p>

  <note>
    <p>Esto requiere que <app>Relojes</app> esté instalado en su equipo.</p>
    <p>La mayoría de las distribuciones tienen instalado <app>Relojes</app> de forma predeterminada. Si la suya no lo tiene, puede querer instalarlo usando el gestor de paquetes de su distribución.</p>
  </note>

  <p>Para añadir un reloj del mundo:</p>

  <steps>
    <item>
      <p>Pulse en el reloj en la barra superior.</p>
    </item>
    <item>
      <p>Pulse en el enlace<gui>Añadir relojes de mundo…</gui> debajo del calendario para lanzar <app>Relojes</app>.</p>

    <note>
       <p>Si ya tiene uno o más relojes del mundo, pulse sobre uno y <app>Relojes</app> se ejecutará.</p>
    </note>

    </item>
    <item>
      <p>En la ventana de <app>Relojes</app> pulse en el botón <gui style="button">Nuevo</gui> <keyseq><key>Ctrl</key><key>N</key></keyseq> para añadir una ciudad nueva.</p>
    </item>
    <item>
      <p>Empiece a escribir el nombre de la ciudad en la que buscar.</p>
    </item>
    <item>
      <p>Seleccione en la lista la ciudad correcta o la ubicación más cercana.</p>
    </item>
    <item>
      <p>Pulse <gui style="button">Añadir</gui> para terminar de añadir la ciudad.</p>
    </item>
  </steps>

  <p>Consulte la <link href="help:gnome-clocks">ayuda de Relojes</link> para conocer más posibilidades de <app>Relojes</app>.</p>

</page>
