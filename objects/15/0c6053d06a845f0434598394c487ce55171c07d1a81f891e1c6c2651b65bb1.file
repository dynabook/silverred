<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-use-system-search" xml:lang="eo">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub STEINER</name>
    </credit>
    <credit type="author">
      <name>Petr KOVAR</name>
    </credit>
    <credit type="author">
      <name>Hannie DUMOLEYN</name>
    </credit>
    <link type="guide" xref="getting-started" group="tasks"/>
    <title role="trail" type="link">Uzi la sisteman serĉon</title>
    <link type="seealso" xref="shell-apps-open"/>
    <title role="seealso" type="link">Instruilo pri uzi la sisteman serĉon</title>
    <link type="next" xref="gs-get-online"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Carmen Bianca BAKKER</mal:name>
      <mal:email>carmen@carmenbianca.eu</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

  <title>Uzi la sisteman serĉon</title>

    <media its:translate="no" type="image" mime="image/svg" src="gs-search1.svg" width="100%"/>
    
    <steps>
    <item><p>Malfermu la <gui>Aktivecoj</gui>-superrigardo per alklaki <gui>Aktivecoj</gui> ĉe la supra maldekstra angulo de la ekrano, aŭ per premi la <key href="help:gnome-help/keyboard-key-super">Super</key>-klavon. Komencu tajpadon por serĉi.</p>
    <p>Rezultoj kiuj kongruas tion, kion vi tajpis, aperos dum via tajpado. La unua rezulto ĉiam estas emfazita kaj montrita ĉe la supro.</p>
    <p>Premu <key>Enter</key> por ŝanĝi al la unua emfazita rezulto.</p></item>
    </steps>
    
    <media its:translate="no" type="image" mime="image/svg" src="gs-search2.svg" width="100%"/>
    <steps style="continues">
      <item><p>Eroj, kiuj povas aperi kiel serĉrezultoj, inkluzivas:</p>
      <list>
        <item><p>kongruaj aplikaĵoj, montritaj ĉe la supro de la serĉrezultoj,</p></item>
        <item><p>kongruaj agordoj,</p></item>
        <item><p>kongruaj kontakoj,</p></item>
        <item><p>kongruaj dokumentoj,</p></item>
        <item><p>kongrua kalendaro,</p></item>
        <item><p>kongrua kalkulilo,</p></item>
        <item><p>kongrua programaro,</p></item>
        <item><p>kongruaj dosieroj,</p></item>
        <item><p>kongrua terminalo,</p></item>
        <item><p>kongruaj pasvortoj kaj ŝlosiloj.</p></item>
      </list>
      </item>
      <item><p>En la serĉrezultoj, alklaku la eron por ŝanĝi al ĝi.</p>
      <p>Alternative, emfazi eron uzante la sagoklavojn kaj premu <key>Enter</key>.</p></item>
    </steps>

    <section id="use-search-inside-applications">
    
      <title>Serĉi el aplikaĵoj</title>
      
      <p>The system search aggregates results from various applications. On the
      left-hand side of the search results, you can see icons of applications
      that provided the search results. Click one of the icons to restart the
      search from inside the application associated with that icon. Because
      only the best matches are shown in the <gui>Activities Overview</gui>,
      searching from inside the application may give you better search results.
      </p>

    </section>

    <section id="use-search-customize">

      <title>Adapti serĉrezultojn</title>

      <media its:translate="no" type="image" mime="image/svg" src="gs-search-settings.svg" width="100%"/>

      <note style="important">
      <p>Your computer lets you customize what you want to display in the search
       results in the <gui>Activities Overview</gui>. For example, you can
        choose whether you want to show results for websites, photos, or music.
        </p>
      </note>


      <steps>
        <title>Por adapti tion, kiun oni montras kiel serĉrezultoj:</title>
        <item><p>Alklaku la <gui xref="shell-introduction#yourname">sistemmenuon</gui> ĉe la dekstra flanko de la supra breto.</p></item>
        <item><p>Click <gui>Settings</gui>.</p></item>
        <item><p>Alklaku <gui>Serĉo</gui> ĉe la maldekstra panelo.</p></item>
        <item><p>In the list of search locations, click the switch next to the
        search location you want to enable or disable.</p></item>
      </steps>

    </section>

</page>
