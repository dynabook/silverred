<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-browse" xml:lang="pt">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>
    <link type="seealso" xref="files-copy"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-16" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bulh</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hilh</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Gira e organize ficheiros com o gestor de ficheiros.</desc>
  </info>

<title>Examinar ficheiros e diretórios</title>

<p>Use a aplicação <app>Ficheiros</app> para navegar e gerir os ficheiros na sua computador. Também pode o usar para gerir ficheiros em dispositivos de armazenamento (como discos externos), em <link xref="nautilus-connect">servidores de ficheiros</link> e em recursos partilhados da rede.</p>

<p>Para abrir o gestor de ficheiros, abra <app>Ficheiros</app> na vista de <gui xref="shell-introduction#activities">Atividades</gui>. Também pode procurar ficheiros e diretórios na vista da mesma maneira que <link xref="shell-apps-open">procura aplicações</link>.</p>

<section id="files-view-folder-contents">
  <title>Explorar o conteúdo de diretórios</title>

<p>No gestor de ficheiros, carregue duas vezes sobre uma diretório para ver seu conteúdo e duas vezes ou com o <link xref="mouse-middleclick">botão central</link> sobre um ficheiro para abrí-lo com a aplicação predeterminado para esse ficheiro. Carregue com o botão central sobre uma diretório para abrir numa flange nova. Também pode carregar com o botão direito sobre uma diretório para abrí-la num separador ou uma janela nova.</p>

<p>Ao examinar os ficheiros numa diretório pode <link xref="files-preview">visualizar rapidamente a cada ficheiro</link> rapidamente clicando a barra de espaço para assegurar-se de que tem o ficheiro correto antes do abrir, o copiar ou o eliminar.</p>

<p>The <em>path bar</em> above the list of files and folders shows you which
folder you’re viewing, including the parent folders of the current folder.
Click a parent folder in the path bar to go to that folder. Right-click any
folder in the path bar to open it in a new tab or window, or access its
properties.</p>

<p>Se quer <link xref="files-search">procurar um ficheiro</link> rapidamente na diretório que está a ver, comece a escrever seu nome. Aparecerá uma <em>caixa de busca</em> na parte superior da janela e mostrar-se-ão só os ficheiros que coincidam com a sua busca. Carregue <key>Esc</key> para cancelar a busca.</p>

<p>You can quickly access common places from the <em>sidebar</em>. If you do
not see the sidebar, press the menu button in the top-right corner of the window and then select
<gui>Sidebar</gui>. You can add bookmarks to folders that you use often and
they will appear in the sidebar. Drag a folder to the sidebar, and drop it over
<gui>New bookmark</gui>, which appears dynamically, or click the current folder
in the path bar and then select <gui style="menuitem">Add to Bookmarks</gui>.</p>

</section>

</page>
