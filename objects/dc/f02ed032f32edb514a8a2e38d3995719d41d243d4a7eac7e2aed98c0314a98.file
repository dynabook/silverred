<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="ui" version="1.0 if/1.0" id="shell-workspaces" xml:lang="nl">

  <info>
    <link type="guide" xref="shell-windows#working-with-workspaces" group="#first"/>

    <revision pkgversion="3.8.0" date="2013-04-23" status="review"/>
    <revision pkgversion="3.10.3" date="2014-01-26" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.35.91" date="2020-02-27" status="candidate"/>

    <credit type="author">
      <name>Gnome-documentatieproject</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Werkbladen zijn een manier om vensters te groeperen op uw bureaublad.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

<title>Wat is een werkblad en hoe kan ik het gebruiken?</title>

    <media its:translate="no" type="image" src="figures/shell-workspaces.png" width="233" height="579" style="floatend floatright">
        <p>Workspace selector</p>
    </media>

  <p if:test="!platform:gnome-classic">De term werkbladen verwijst naar het groeperen van vensters op uw bureaublad. U kunt meerdere werkbladen aanmaken; deze fungeren als virtuele bureaubladen. Werkbladen zijn bedoeld om te voorkomen dat het rommelig wordt en zorgen ervoor dat het gemakkelijker is door het bureaublad te navigeren.</p>

  <p if:test="platform:gnome-classic">De term werkbladen verwijst naar het groeperen van vensters op uw bureaublad. U kunt meerdere werkbladen aanmaken; deze fungeren als virtuele bureaubladen. Werkbladen zijn bedoeld om te voorkomen dat het rommelig wordt en zorgen ervoor dat het gemakkelijker is door het bureaublad te navigeren.</p>

  <p>U zou werkbladen kunnen gebruiken om uw werk te organiseren. Zo zou u alle communicatievensters, zoals e-mail en chattoepassingen in één werkblad kunnen plaatsen en het werk waar u mee bezig bent op een ander werkblad. Uw muziekprogramma zou u in een derde werkblad kunnen plaatsen.</p>

<p>Werkbladen gebruiken:</p>

<list>
  <item>
    <p if:test="!platform:gnome-classic">Breng in het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht de cursor naar de rechterkant van het scherm.</p>
    <p if:test="platform:gnome-classic">Click the button at the bottom left of
    the screen in the window list, or press the
    <key xref="keyboard-key-super">Super</key> key to open the
    <gui>Activities</gui> overview.</p>
  </item>
  <item>
    <p if:test="!platform:gnome-classic">A vertical panel will expand showing
    workspaces in use, plus an empty workspace. This is the
    workspace selector.</p>
    <p if:test="platform:gnome-classic">In the bottom right corner, you see four
    boxes. This is the workspace selector.</p>
  </item>
  <item>
    <p if:test="!platform:gnome-classic">Om een werkruimte toe te voegen sleept u een venster van een bestaande werkruimte naar de lege werkruimte in de werkruimtekiezer.Deze werkruimte bevat nu het venster dat u heeft laten vallen en er zal eronder een nieuwe lege werkruimte verschijnen.</p>
    <p if:test="platform:gnome-classic">Drag and drop a window from your current
    workspace onto an empty workspace in the workspace selector. This workspace
    now contains the window you have dropped.</p>
  </item>
  <item if:test="!platform:gnome-classic">
    <p>Om een werkruimte te verwijderen sluit u gewoon alle vensters erin of verplaatst u ze naar andere werkruimtes.</p>
  </item>
</list>

<p if:test="!platform:gnome-classic">Er is altijd minstens één werkblad.</p>

</page>
