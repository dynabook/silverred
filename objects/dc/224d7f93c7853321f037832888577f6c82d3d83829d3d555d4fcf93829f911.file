<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-wireless-disconnecting" xml:lang="fi">

  <info>
    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-problem"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <desc>Signaali saattaa olla heikko tai verkkoa ei ole ehkä yhdistetty oikein.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

<title>Miksi langaton yhteys katkeaa?</title>

<p>You may find that you have been disconnected from a wireless network even
 though you wanted to stay connected. Your computer will normally try to
 reconnect to the network as soon as this happens (the network icon on the top
 bar will display three dots if it is trying to reconnect), but it can be
 annoying, especially if you were using the internet at the time.</p>

<section id="signal">
 <title>Heikko langaton signaali</title>

 <p>Yleisin syy yhteyden katkeamiseen on heikko signaali. Langattomilla tukiasemilla on tietty kantomatka, ja jos olet sen ulkopuolella, signaali ei ole ehkä tarpeeksi vahva tiedon siirtämisen. Seinät ja muut esteet tukiaseman ja tietokoneen välissä voivat heikentää signaalia merkittävästi.</p>

 <p>Verkkovalikon kuvake oikealla yläpalkissa ilmaisee signaalin vahvuuden. Jos verkkopalkkeja on vähän, yritä siirtyä mahdollisuuksien mukaan lähemmäksi langatonta tukiasemaa.</p>

</section>

<section id="network">
 <title>Verkkoyhteys ei muodostu kunnolla</title>

 <p>Sometimes, when you connect to a wireless network, it may appear that you
 have successfully connected at first, but then you will be disconnected soon
 after. This normally happens because your computer was only partially
 successful in connecting to the network — it managed to establish a connection,
 but was unable to finalize the connection for some reason and so was
 disconnected.</p>

 <p>Mahdollisia syitä tähän on muun muassa virheellinen salasana tai tietokonettasi ei sallittu käyttämään verkkoa (tämä taas voi johtua esimerkiksi siitä, että verkko vaatii rekisteröitymisen).</p>

</section>

<section id="hardware">
 <title>Langaton laite on epävakaa tai sen ajurit ovat huonot</title>

 <p>Verkkoihin kuuluu monta laitetta ja ajuria eri valmistajilta, joilla voi tulla ongelmia keskenään, jonka takia yhteydet voivat katkeilla ja olla hitaita. Tämä on ärsyttävää, mutta tapahtuu jos samaa verkkoa käyttää useampi laite. Jos yhteytesi katkeilee vähän väliä (yhteys muodostuu, katkeaa, muodostuu uudelleen...), tämä on todennäköisesti ongelman aiheuttaja. Jos tämä alkaa käydä mahdottomaksi, kannattaa sinun harkita verkkolaitteidesi (esim. WLAN-tukiaseman) vaihtamista.</p>

</section>

<section id="busy">
 <title>Ruuhkaiset langattomat verkot</title>

 <p>Langattomia verkkoja vilkkaissa paikoissa (esimerkiksi kahviloissa ja yliopistoissa) käyttää yleensä monta tietokonetta samaan aikaan. Joskus tällaiset verkot voivat ruuhkautua, eivätkä kykene enää tarjoamaan yhteyttä kaikille tietokoneille. Joidenkin tietokoneiden yhteys saattaa siis katketa yhtäkkiä.</p>

</section>

</page>
