<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-use-system-search" xml:lang="ko">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>
    <credit type="author">
      <name>Hannie Dumoleyn</name>
    </credit>
    <link type="guide" xref="getting-started" group="tasks"/>
    <title role="trail" type="link">시스템 검색 활용하기</title>
    <link type="seealso" xref="shell-apps-open"/>
    <title role="seealso" type="link">시스템 검색 활용 따라하기</title>
    <link type="next" xref="gs-get-online"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2014, 2016, 2017, 2018, 2019</mal:years>
    </mal:credit>
  </info>

  <title>시스템 검색 활용하기</title>

    <media its:translate="no" type="image" mime="image/svg" src="gs-search1.svg" width="100%"/>
    
    <steps>
    <item><p>화면 왼쪽 위에서 <gui>현재 활동</gui>을 누르거나 <key href="help:gnome-help/keyboard-key-super">Super</key>키를 눌러 <gui>현재 활동 개요</gui>를 여십시오. 검색을 시작하려면 입력하십시오.</p>
    <p>입력한 내용과 일치하는 결과는 입력한 대로 나타납니다. 처음 결과는 최상위에 항상 강조된 상태로 나타납니다.</p>
    <p><key>Enter</key> 키를 눌러 처음 강조한 결과로 전환하십시오.</p></item>
    </steps>
    
    <media its:translate="no" type="image" mime="image/svg" src="gs-search2.svg" width="100%"/>
    <steps style="continues">
      <item><p>검색 결과에 나타날 항목은 다음과 같습니다:</p>
      <list>
        <item><p>검색 결과 최상단에서 일치하는 프로그램,</p></item>
        <item><p>일치 설정,</p></item>
        <item><p>연락처 일치,</p></item>
        <item><p>문서 일치,</p></item>
        <item><p>달력 일치,</p></item>
        <item><p>계산기 일치,</p></item>
        <item><p>소프트웨어 일치,</p></item>
        <item><p>파일 일치,</p></item>
        <item><p>터미널 일치,</p></item>
        <item><p>암호와 키를 일치합니다.</p></item>
      </list>
      </item>
      <item><p>검색 결과에서 전환하려는 항목을 누르십시오.</p>
      <p>대신, 화살표 키를 사용해서 항목을 강조하고 <key>Enter</key>키를 누르셔도 됩니다.</p></item>
    </steps>

    <section id="use-search-inside-applications">
    
      <title>프로그램 안에서 선택하기</title>
      
      <p>시스템은 다양한 프로그램의 결과를 수집합니다. 검색 결과의 왼편에는 검색 결과로 제공한 프로그램 아이콘을 볼 수 있습니다. 아이콘과 관련된 프로그램의 검색을 다시 시작하려면 항목의 아이콘 중 하나를 누르십시오. <gui>현재 활동 요약</gui>에서 최상의 일치 요소만을 보여주기 때문에 프로그램에서 검색하면 더 나은 결과를 얻을 수 있습니다.</p>

    </section>

    <section id="use-search-customize">

      <title>검색 결과 편집하기</title>

      <media its:translate="no" type="image" mime="image/svg" src="gs-search-settings.svg" width="100%"/>

      <note style="important">
      <p>컴퓨터에서 <gui>현재 활동 요약</gui>에서 검색 결과에 나타낼 내용을 따로 지정할 수 있습니다. 예를 들어 어떤 웹사이트, 사진, 음악 검색 결과를 볼 지 선택할 수 있습니다.</p>
      </note>


      <steps>
        <title>검색 결과 표시 내용을 편집하려면:</title>
        <item><p>상단 표시줄의 우측에 있는 <gui xref="shell-introduction#yourname">시스템 메뉴</gui>를 누르십시오.</p></item>
        <item><p>Click <gui>Settings</gui>.</p></item>
        <item><p>왼쪽 창에서 <gui>검색</gui> 을 누르십시오.</p></item>
        <item><p>검색 위치 목록에서 켜거나 끌 검색 위치에 대해 각 항목 옆의 <gui>켬/끔</gui> 스위치를 누르십시오.</p></item>
      </steps>

    </section>

</page>
