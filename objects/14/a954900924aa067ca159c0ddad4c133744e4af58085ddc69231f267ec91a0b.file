<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-history-recent-off" xml:lang="lv">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.8" date="2013-03-11" status="final"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Pārtraukt vai ierobežot nesen izmantoto datņu sekošanai.</desc>
  </info>

  <title>Izslēgt vai ierobežot datņu vēstures izsekošanu</title>
  
  <p>Nesen lietoto datņu izsekošana var atvieglot to dokumentu, pie kuriem nesen strādājāt, meklēšanu datņu pārvaldniekā un datņu dialoglodziņos lietotnēs. Iespējams, vēlaties paturēt datņu lietošanas vēsturi privātu, vai tikai sekot ļoti nesenai vēsturei.</p>

  <steps>
    <title>Izslēgt datņu vēstures izsekošanu</title>
    <item>
      <p>Atveriet <gui xref="shell-introduction#activities">Aktivitāšu</gui> pārskatu un sāciet rakstīt <gui>Privātums</gui>.</p>
    </item>
    <item>
      <p>Spiediet <gui>Privātums</gui>, lai atvērtu paneli.</p>
    </item>
    <item>
      <p>Izvēlieties <gui>Lietojums un vēsture</gui>.</p>
    </item>
    <item>
     <p>Switch the <gui>Recently Used</gui> switch to off.</p>
     <p>To re-enable this feature, switch the <gui>Recently Used</gui> switch
     to on.</p>
    </item>
    <item>
      <p>Izmantojiet pogu <gui>Attīrīt neseno vēsturi</gui>, lai uzreiz attīrītu vēsturi.</p>
    </item>
  </steps>
  
  <note><p>Šis iestatījums neietekmēs to, kā tīmekļa pārlūks saglabā informāciju par apmeklētajām vietnēm.</p></note>

  <steps>
    <title>Ierobežot laiku, kādā tiek izsekotas datnes:</title>
    <item>
      <p>Atveriet <gui xref="shell-introduction#activities">Aktivitāšu</gui> pārskatu un sāciet rakstīt <gui>Privātums</gui>.</p>
    </item>
    <item>
      <p>Spiediet <gui>Privātums</gui>, lai atvērtu paneli.</p>
    </item>
    <item>
      <p>Izvēlieties <gui>Lietojums un vēsture</gui>.</p>
    </item>
    <item>
     <p>Ensure the <gui>Recently Used</gui> switch is set to on.</p>
    </item>
    <item>
     <p>Laika garumu norādiet sadaļā <gui>Nesenā vēsture</gui>. Izvēlieties <gui>1 diena</gui>, <gui>7 dienas</gui>, <gui>30 dienas</gui> vai <gui>Visu laiku</gui>.</p>
    </item>
    <item>
      <p>Izmantojiet pogu <gui>Attīrīt neseno vēsturi</gui>, lai uzreiz attīrītu vēsturi.</p>
    </item>
  </steps>

</page>
