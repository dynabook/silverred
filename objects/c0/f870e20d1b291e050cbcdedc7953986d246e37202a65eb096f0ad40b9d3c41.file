<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="keyboard-key-super" xml:lang="da">

  <info>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.7.91" version="0.2" date="2013-03-16" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-27" status="review"/>

    <credit type="author">
      <name>GNOMEs dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>The <key>Super</key> key opens the <gui>Activities</gui> overview.
    You can usually find it next to the <key>Alt</key> key on your
    keyboard.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years/>
    </mal:credit>
  </info>

  <title>Hvad er <key>Super</key>-tasten?</title>

  <p>When you press the <key>Super</key> key, the <gui>Activities</gui>
  overview is displayed. This key can usually be found on the bottom-left of
  your keyboard, next to the <key>Alt</key> key, and usually has a Windows logo
  on it. It is sometimes called the <em>Windows key</em> or system key.</p>

  <note>
    <p>If you have an Apple keyboard, you will have a <key>⌘</key> (Command)
    key instead of the Windows key, while Chromebooks have a magnifying glass
    instead.</p>
  </note>

  <p>To change which key is used to display the <gui>Activities</gui>
  overview:</p>

  <steps>
    <item>
      <p>Åbn <gui xref="shell-introduction#activities">Aktivitetsoversigten</gui> og begynd at skrive <gui>Indstillinger</gui>.</p>
    </item>
    <item>
      <p>Klik på <gui>Indstillinger</gui>.</p>
    </item>
    <item>
      <p>Klik på <gui>Enheder</gui> i sidebjælken.</p>
    </item>
    <item>
      <p>Klik på <gui>Tastatur</gui> i sidebjælken for at åbne panelet.</p>
    </item>
    <item>
      <p>In the <gui>System</gui> category, click the row with <gui>Show the
      activities overview</gui>.</p>
    </item>
    <item>
      <p>Hold down the desired key combination.</p>
    </item>
  </steps>

</page>
