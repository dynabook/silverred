<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-display" xml:lang="sv">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-display"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Styr ikonrubriker som används i filhanteraren.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

<title>Filhanterarens visningsinställningar</title>

<p>Du kan styra hur filhanteraren visar text under ikoner. Klicka på menyknappen i övre högra hörnet i fönstret och välj <gui>Inställningar</gui> och välj sedan fliken <gui>Vyer</gui>.</p>

<section id="icon-captions">
  <title>Ikonrubriker</title>
  <!-- TODO: update screenshot for 3.18 and above. -->
  <media type="image" src="figures/nautilus-icons.png" width="250" height="110" style="floatend floatright">
    <p>Filhanterarens ikoner med text</p>
  </media>
  <p>När du använder ikonvyn kan du välja att visa extra information om filer och mappar som en text under varje ikon. Detta är till exempel användbart om du ofta behöver se vem som äger en fil eller när den senast modifierades.</p>
  <p>Du kan zooma i en mapp genom att klicka på knappen visningsalternativ i verktygsfältet och välja en zoomnivå med skjutreglaget. När du zoomar in kommer filhanteraren att visa mer och mer information i rubriker. Du kan välja upp till tre saker att visa i rubrikerna. Den första kommer att visas på de flesta zoomnivåerna. Den sista kommer bara att visas vid väldigt stora storlekar.</p>
  <p>Informationen du kan visa i ikonrubriker är samma som kolumnerna du kan använda i listvyn. Se <link xref="nautilus-list"/> för mer information.</p>
</section>

<section id="list-view">

  <title>Listvy</title>

  <p>När du tittar på filer som en lista finns alternativet <gui>Tillåt mappar att expanderas</gui>. Detta visar expanderare för varje katalog i fillistan så att innehållet för flera kataloger kan visas samtidigt. Detta är användbart om mappstrukturen är relevant, t.ex. om dina musikfiler är organiserade med en mapp per artist och en undermapp per album.</p>

</section>

</page>
