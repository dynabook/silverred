<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-calibrate-camera" xml:lang="ca">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-calibrationtargets"/>
    <link type="seealso" xref="color-calibrate-printer"/>
    <link type="seealso" xref="color-calibrate-scanner"/>
    <link type="seealso" xref="color-calibrate-screen"/>
    <desc>Calibrar la seva càmera és important per capturar els colors precisos.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

  <title>Com puc calibrar la meva càmera?</title>

  <p>Els dispositius tipus càmera es calibren prenent una fotografia sota les condicions de llum desitjades. En convertir el fitxer RAW a un fitxer TIFF, aquest es pot utilitzar per calibrar la càmera en el tauler de control de color.</p>
  <p>Haureu de retallar el fitxer TIFF perquè només l'objectiu sigui visible. Assegureu-vos que les vores blanques o negres estiguin visibles. El calibratge no funcionarà si la imatge està al revés o està molt distorsionada.</p>

  <note style="tip">
    <p>El perfil resultant només és vàlid sota la condició d'il·luminació de la qual va adquirir la imatge original. Això vol dir que és possible que hàgiu de definir el perfil diverses vegades per diverses condicions de lluminositat:<em>estudi</em>, <em>llum solar</em> i <em>ennuvolat</em>.</p>
  </note>

</page>
