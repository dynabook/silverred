<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sound-usemic" xml:lang="fi">

  <info>
    <link type="guide" xref="media#sound"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Käytä analogista tai USB-mikrofonia ja valitse se oletuslaitteeksi.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

  <title>Käytä eri mikrofonia</title>

  <p>Voit käyttää ulkoista mikrofonia esimerkiksi puhelun soittamista varten. Vaikka tietokoneessasi olisi sisäänrakennettu mikrofoni, erillinen mikrofoni voi tarjota paremman äänenlaadun.</p>

  <p>If your microphone has a circular plug, just plug it into the appropriate
  audio socket on your computer. Most computers have two sockets: one for
  microphones and one for speakers. This socket is usually light red in color
  or is accompanied by a picture of a microphone. Microphones plugged
  into the appropriate socket are usually used by default. If not, see the
  instructions below for selecting a default input device.</p>

  <p>Jos sinulla on USB-mikrofoni, kiinnitä se tietokoneesi USB-porttiin. USB-mikrofonit näkyvät erillisinä äänilaitteina, joten oletusmikrofonin määritys ääniasetuksista voi olla tarpeen.</p>

  <steps>
    <title>Valitse oletuksena käytettävä äänen tallennuslaite</title>
    <item>
      <p>Avaa <gui xref="shell-introduction#activities">Toiminnot</gui>-yleisnäkymä ja ala kirjoittamaan <gui>Ääni</gui>.</p>
    </item>
    <item>
      <p>Napsauta <gui>Ääni</gui> avataksesi paneelin.</p>
    </item>
    <item>
      <p>In the <gui>Input</gui> tab, select the device that you want to use.
      The input level indicator should respond when you speak.</p>
    </item>
  </steps>

  <p>Voit muuttaa äänenvoimakkuutta ja sammuttaa mikrofonin tämän paneelin kautta.</p>

</page>
