<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting" xml:lang="ru">

  <info>
    <link type="guide" xref="net-wireless" group="first"/>
    <link type="guide" xref="hardware#problems" group="first"/>
    <link type="next" xref="net-wireless-troubleshooting-initial-check"/>

    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Люди, делающие вклад в wiki документации для Ubuntu</name>
    </credit>
    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Identify and fix problems with wireless connections.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Устранение неполадок с беспроводной сетью</title>

  <p>This is a step-by-step troubleshooting guide to help you identify and fix
  wireless problems. If you cannot connect to a wireless network for some
  reason, try following the instructions here.</p>

  <p>Чтобы добиться подключения вашего компьютера к Интернету, мы последовательно пройдём следующие этапы:</p>

  <list style="numbered compact">
    <item>
      <p>Выполнение первоначальной проверки</p>
    </item>
    <item>
      <p>Сбор информации о вашем оборудовании</p>
    </item>
    <item>
      <p>Проверка оборудования</p>
    </item>
    <item>
      <p>Попытка установить соединение с беспроводным маршрутизатором</p>
    </item>
    <item>
      <p>Проверка модема и маршрутизатора</p>
    </item>
  </list>

  <p>Для начала нажмите ссылку <em>Далее</em> в верхней правой части станицы. Эта ссылка, как и другие на следующих страницах, последовательно проведёт вас по настройкам этого руководства.</p>

  <note>
    <title>Использование командной строки</title>
    <p>Some of the instructions in this guide ask you to type commands into the
    <em>command line</em> (Terminal). You can find the <app>Terminal</app> application in
    the <gui>Activities</gui> overview.</p>
    <p>If you are not familiar with using a command line, don’t worry — this
    guide will direct you at each step. All you need to remember is that
    commands are case-sensitive (so you must type them <em>exactly</em> as they
    appear here), and to press <key>Enter</key> after typing each command to
    run it.</p>
  </note>

</page>
