<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="dconf-keyfiles" xml:lang="ko">

  <info>
    <link type="guide" xref="setup"/>
    <link type="seealso" xref="dconf"/>
    <link type="seealso" xref="dconf-profiles"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Ryan Lortie</name>
      <email>desrt@desrt.ca</email>
      <years>2012</years>
    </credit>
    <credit type="author">
      <name>Aruna Sankaranarayanan</name>
      <email>aruna.evam@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>텍스트 편집기에서 <sys its:translate="no">dconf</sys> <em>keyfiles</em> 을 열어 개별 설정값을 바꿉니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조재은</mal:name>
      <mal:email>ckr971028@gmail.com</mal:email>
      <mal:years>2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

  <title>키 파일에서 시스템 설정 조절</title>

  <p><file its:translate="no">/etc/dconf/db</file>에 있는 시스템 데이터베이스 파일은 GVDB 형식으로 기록했기 때문에 편집할 수 없습니다. 이 시스템 설정을 텍스트 편집기에서 바꾸려면 <em>키 파일 디렉터리</em>의 <em>keyfiles</em>을 수정하시면 됩니다. 각각의 키 파일 디렉터리는 시스템 데이터베이스 파일 일부에 해당하며, 데이터베이스 파일과 동일한 이름에 “.d” 확장자가 붙어있습니다(예: <file>/etc/dconf/db/local.d</file>). 모든 키 파일 디렉터리는 <file its:translate="no">/etc/dconf/db</file>에 있으며 각 디렉터리에 <sys its:translate="no">dconf</sys> 데이터베이스 형식으로 컴파일할 수 있는 특수 형식으로 저장한 키 파일이 있습니다.</p>

  <listing>
    <title>이 디렉터리의 키 파일 내용은 다음과 같습니다:</title>
      <code>
# Some useful default settings for our site

[system/proxy/http]
host='172.16.0.1'
enabled=true

[org/gnome/desktop/background]
picture-uri='file:///usr/local/rupert-corp/company-wallpaper.jpeg'
      </code>
  </listing>

  <note style="important">
    <p>키 파일을 수정하고 나면 <cmd>dconf update</cmd> 명령을 실행해야 합니다. 이 명령을 실행하면 <sys its:translate="no">dconf</sys>에서 각 키 파일 디렉터리의 시스템 데이터베이스 파일의 타임스탬프와 키 파일 디렉터리의 타임스탬프를 비교합니다. 키 파일 디렉터리의 타임스탬프가 데이터베이스 파일보다 더 최신이면, <sys its:translate="no">dconf</sys>에서 <code>system-db</code> 파일을 다시 만들고, 모든 프로그램으로 하여금 각 프로그램의 설정을 다시 읽도록 알리는 시스템  <sys>D-Bus</sys>에 알림을 보냅니다.</p>
  </note>

  <p>키 파일의 그룹 이름은 <link href="https://developer.gnome.org/GSettings/">GSettings 스키마 ID</link>를 참조합니다. 예로,  <code>org/gnome/desktop/background</code>는 <code>picture-uri</code> 키가 들어있는 <code>org.gnome.desktop.background</code> 스키마를 참조합니다.</p>

  <p>그룹에 있는 값은 <link href="https://developer.gnome.org/glib/stable/gvariant-text.html">직렬화한 GVariant 형태</link>를 갖춥니다.</p>

</page>
