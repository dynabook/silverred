<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="printing-booklet" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Como imprimir um folheto multipágina usando papel A4 ou Carta.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

  <title>Imprimindo um folheto</title>

  <p>Você pode imprimir um folheto de um PDF.</p>

  <p>Se você deseja imprimir um folheto a partir de um documento do <app>LibreOffice</app>, primeiro exporte-o para um PDF escolhendo <guiseq><gui>Arquivo</gui><gui>Exportar como PDF…</gui></guiseq>. Seu documento precisa ter um número de páginas múltiplo de 4 (4, 8, 12, 16, …). Você pode precisar adicionar até 3 páginas em branco.</p>

  <p>Se o número de páginas em seu documento PDF não é um múltiplo de 4, você deve adicionar o número adequado de páginas em branco (1, 2 ou 3) para torná-la múltiplo de 4. Para fazer isso, você pode:</p>

  <steps>
    <item>
      <p>Criar um documento <app>LibreOffice</app> com o número (1-3) de páginas em branco necessárias.</p>
    </item>
    <item>
      <p>Exportar as páginas em branco para um PDF escolhendo <guiseq><gui>Arquivo</gui><gui>Exportar como PDF…</gui></guiseq>.</p>
    </item>
    <item>
      <p>Mesclar as páginas em branco com seu documento PDF usando <app>PDF-Shuffler</app> ou <app>PDF Mod</app>, colocando as páginas em branco no final.</p>
    </item>
  </steps>

  <p>Selecione o tipo de impressora que você vai usar para imprimir da lista abaixo:</p>

</page>
