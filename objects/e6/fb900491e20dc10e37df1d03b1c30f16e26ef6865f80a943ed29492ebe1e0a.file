<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-switch-tasks" version="1.0 if/1.0" xml:lang="sk">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>
    <link type="guide" xref="getting-started" group="videos"/>
    <title role="trail" type="link">Prepínanie úloh</title>
    <link type="seealso" xref="shell-windows-switching"/>
    <title role="seealso" type="link">Návod na prepínanie úloh</title>
    <link type="next" xref="gs-use-windows-workspaces"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Richard Stanislavský</mal:name>
      <mal:email>kenny.vv@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>Prepínanie úloh</title>

  <ui:overlay width="812" height="452">
  <media type="video" its:translate="no" src="figures/gnome-task-switching.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-task-switching.svg"/>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>Prepínanie úloh</tt:p>
         </tt:div>
         <tt:div begin="5s" end="8s">
           <tt:p if:test="!platform:gnome-classic">Kurzor myši presunieme na <gui>Aktivity</gui>, do ľavého horného rohu obrazovky.</tt:p>
           </tt:div>
         <tt:div begin="9s" end="12s">
           <tt:p>Vyberieme si úlohu kliknutím na zodpovedajúce okno.</tt:p>
         </tt:div>
         <tt:div begin="12s" end="14s">
           <tt:p>To maximize a window along the left side of the screen, grab
            the window’s titlebar and drag it to the left.</tt:p>
         </tt:div>
         <tt:div begin="14s" end="16s">
           <tt:p>Keď je polovica obrazovky zvýraznená, pustíme okno.</tt:p>
         </tt:div>
         <tt:div begin="16s" end="18">
           <tt:p>To maximize a window along the right side, grab the window’s
            titlebar and drag it to the right.</tt:p>
         </tt:div>
         <tt:div begin="18s" end="20s">
           <tt:p>Keď je polovica obrazovky zvýraznená, pustíme okno.</tt:p>
         </tt:div>
         <tt:div begin="23s" end="27s">
           <tt:p><gui>Prepínanie okien</gui> zobrazíme stlačením kombinácie kláves <keyseq> <key href="help:gnome-help/keyboard-key-super">Super</key><key> Tab</key></keyseq>.</tt:p>
         </tt:div>
         <tt:div begin="27s" end="29s">
           <tt:p>Zvýraznené okno vyberieme pustením klávesu <key href="help:gnome-help/keyboard-key-super">Super </key>.</tt:p>
         </tt:div>
         <tt:div begin="29s" end="32s">
           <tt:p>V zozname otvorených okien sa pohybujeme tak, že nepúšťame kláves <key href="help:gnome-help/keyboard-key-super">Super</key>, ale držíme ho stlačený a zároveň stláčame kláves <key>Tab</key>.</tt:p>
         </tt:div>
         <tt:div begin="35s" end="37s">
           <tt:p><gui>Prehľad aktivít</gui> zobrazíme stlačením klávesu <key href="help:gnome-help/keyboard-key-super">Super</key>.</tt:p>
         </tt:div>
         <tt:div begin="37s" end="40s">
           <tt:p>Začneme písať názov aplikácie, na ktorú sa chceme prepnúť.</tt:p>
         </tt:div>
         <tt:div begin="40s" end="43s">
           <tt:p>Ak sa aplikácia zobrazí ako prvý výsledok, otvoríme ju stlačením klávesu <key>Enter</key>.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

  <section id="switch-tasks-overview">
    <title>Prepínanie úloh</title>

<if:choose>
<if:when test="!platform:gnome-classic">

  <steps>
    <item><p>Presunieme kurzor myši na <gui>Aktivity</gui> do ľavého horného rohu obrazovky, čím zobrazíme <gui>Prehľad aktivít</gui>, kde môžeme vidieť aktuálne spustené úlohy zobrazené v malých oknách.</p></item>
     <item><p>Vyberieme si úlohu kliknutím na zodpovedajúce okno.</p></item>
  </steps>

</if:when>
<if:when test="platform:gnome-classic">

  <steps>
    <item><p>iMedzi úlohami môžeme prepnúť použitím <gui>zoznamu okien</gui> na spodku obrazovky. Otvorené úlohy sú zobrazené ako tlačidlá v tomto zozname.</p></item>
     <item><p>Vyberieme si úlohu kliknutím na zodpovedajúce tlačidlo v <gui>zozname okien</gui>.</p></item>
  </steps>

</if:when>
</if:choose>

  </section>

  <section id="switching-tasks-tiling">
    <title>Usporiadanie okien do dlaždíc</title>
    
    <steps>
      <item><p>To maximize a window along a side of the screen, grab the window’s
       titlebar and drag it to the left or right side of the screen.</p></item>
      <item><p>Keď bude polovica obrazovky zvýraznená, pustíme okno aby sa maximalizovalo pozdĺž vybranej strany obrazovky.</p></item>
      <item><p>Keď chceme maximalizovať dve okná vedľa seba, chytíme titulok druhého okna a presunieme okno na opačný okraj obrazovky.</p></item>
       <item><p>Keď je polovica okna zvýraznená, pustíme okno aby sa maximalizovalo pozdĺž opačnej strany obrazovky.</p></item>
    </steps>
    
  </section>
  
  <section id="switch-tasks-windows">
    <title>Prepínanie medzi oknami</title>
    
  <steps>
   <item><p><gui>Prepínač okien</gui>, ktorý zobrazí zoznam aktuálne otvorených okien, zobrazíme stlačením kombinácie kláves <keyseq><key href="help:gnome-help/keyboard-key-super">Super </key><key>Tab</key></keyseq>.</p></item>
   <item><p>Nasledujúce zvýraznené okno v <gui>prepínači okien</gui> vyberieme tak, že pustíme kláves <key href="help:gnome-help/keyboard-key-super">Super</key>.</p>
   </item>
   <item><p>V zozname otvorených okien sa pohybujeme tak, že nepúšťame kláves <key href="help:gnome-help/keyboard-key-super">Super</key>, ale držíme ho stlačený a zároveň stláčame kláves <key>Tab</key>.</p></item>
  </steps>

  </section>

  <section id="switch-tasks-search">
    <title>Prepínanie aplikácií pomocou vyhľadávania</title>
    
    <steps>
      <item><p>Stlačením klávesu <key href="help:gnome-help/keyboard-key-super">Super</key> zobrazíme <gui>Prehľad aktivít</gui>.</p></item>
      <item><p>Stačí začať písať názov aplikácie, na ktorú sa chceme prepnúť. Ihneď ako začneme písať sa budú zobrazovať aplikácie, ktoré zodpovedajú tomu, čo sme zatiaľ napísali.</p></item>
      <item><p>Keď sa zobrazí aplikácia, na ktorú sa chceme prepnúť ako prvý výsledok, prepneme sa na ňu stlačením klávesu <key>Enter</key>.</p></item>
      
    </steps>
    
  </section>

</page>
