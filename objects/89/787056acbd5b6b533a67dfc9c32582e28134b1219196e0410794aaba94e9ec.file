<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="sharing-media" xml:lang="lv">

  <info>
    <link type="guide" xref="sharing"/>
    <link type="guide" xref="prefs-sharing"/>

    <revision pkgversion="3.10" version="0.2" date="2013-11-02" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-13" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Dalieties ar medijiem savā lokālajā tīklā, izmantojot UPnP.</desc>
  </info>

  <title>Dalieties ar mūziku, fotogrāfijām un video</title>

  <p>Jūs varat pārlūkot, meklēt un atskaņot multimedijus savā datorā, izmantojot ierīces ar <sys>UPnP</sys> vai <sys>DLNA</sys>, piemēram, tālrunis, TV vai spēļu konsole. Konfigurējiet <gui>multimediju koplietošanu</gui>, lai ļautu šīm lietotnēm piekļūt jūsu mūzikai, fotogrāfijām un video.</p>

  <note style="info package">
    <p>Lai <gui>Multimediju koplietošana</gui> būtu redzama, jābūt uzinstalētai pakotnei <app>Rygel</app>.</p>

    <if:choose xmlns:if="http://projectmallard.org/if/1.0/">
      <if:when test="action:install">
        <p><link action="install:rygel" style="button">Instalēt Rygel</link></p>
      </if:when>
    </if:choose>
  </note>

  <steps>
    <item>
      <p>Atveriet <gui xref="shell-introduction#activities">Aktivitāšu</gui> pārskatu un sāciet rakstīt <gui>Koplietošana</gui>.</p>
    </item>
    <item>
      <p>Spiediet <gui>Koplietošana</gui>, lai atvērtu paneli.</p>
    </item>
    <item>
      <p>If the <gui>Sharing</gui> switch in the top-right of the window is
      set to off, switch it to on.</p>

      <note style="info"><p>Ja teksts zem <gui>Datora nosaukuma</gui> jums to ļauj rediģēt, jūs varat <link xref="sharing-displayname">mainīt</link> nosaukumu, ar kādu dators būs redzams tīklā.</p></note>
    </item>
    <item>
      <p>Izvēlies <gui>Multimediju koplietošana</gui>.</p>
    </item>
    <item>
      <p>Switch the <gui>Media Sharing</gui> switch to on.</p>
    </item>
    <item>
      <p>Pēc noklusējuma, mapes <file>Mūzika</file>, <file>Attēli</file> un <file>Video</file> ir koplietoti. Lai kādu no tiem izņemtu, spiediet<gui>×</gui> pie mapes nosaukuma.</p>
    </item>
    <item>
      <p>Lai pievienotu jaunu mapi, spiediet <gui style="button">+</gui>, lai atvērtu logu <gui>Izvēlieties mapi</gui>. Dodieties <em>uz</em> vēlamo mapi, un spiediet <gui style="button">Atvērt</gui>.</p>
    </item>
    <item>
      <p>Spiediet <gui style="button">×</gui>. Jūs nevarēsiet pārlūkot vai atskaņot multimedijus izvēlētajās mapēs, izmantojot ārēju ierīci.</p>
    </item>
  </steps>

  <section id="networks">
  <title>Tīkli</title>

  <p>The <gui>Networks</gui> section lists the networks to which you are
  currently connected. Use the switch next to each to choose where your media
  can be shared.</p>

  </section>

</page>
