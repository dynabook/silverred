<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-booklet-duplex" xml:lang="pt">

  <info>
    <link type="guide" xref="printing-booklet"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="candidate"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Imprimir folhetos dobrados (tipo livro ou panfleto) desde um PDF utilizando um tamanho de papel normal A4/Carta.</desc>
  </info>

  <title>Imprimir um folheto numa impressora de dupla cara</title>

  <p>Pode fazer folhetos dobrados (como um pequeno livro ou panfleto) plotando as páginas dum documento numa ordem especial e mudando um par de opções de impressão.</p>

  <p>Estas instruções são para imprimir um folheto a partir dum documento PDF.</p>

  <p>Se quer imprimir um folheto desde um documento de <app>LibreOffice</app>, terá que o exportar primeiro a PDF elegendo <guiseq><gui>Ficheiro</gui><gui>Exportar como PDF…</gui></guiseq>. Seu documento deve ter um número de páginas múltiplo de 4 (4, 8, 12, 16,...). É possível que tenha que acrescentar até 3 páginas em alvo.</p>

  <p>Para imprimir um folheto:</p>

  <steps>
    <item>
      <p>Abra o diálogo de impressão. Isto se pode fazer normalmente mediante o elemento de menu <gui style="menuitem">Imprimir</gui> ou utilizando o atalho do teclado <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>Click the <gui>Properties…</gui> button </p>
      <p>Na lista desdobrável <gui>Orientação</gui>, assegure-se de que a opção <gui>Paisagem</gui> está selecionada</p>
      <p>Na lista desdobrável <gui>Duas caras</gui> escolha <gui>Margem curta</gui>.</p>
      <p>Carregue <gui>Aceitar</gui> para voltar ao diálogo de impressão.</p>
    </item>
    <item>
      <p>Em <gui>Faixa e cópias</gui>, escolha <gui>Páginas</gui>.</p>
    </item>
    <item>
      <p>Escreva os números das páginas nesta ordem (n é o número total de páginas, múltiplo de 4):</p>
      <p>n, 1, 2, n-1, n-2, 3, 4, n-3, n-4, 5, 6, n-5, n-6, 7, 8, n-7, n-8, 9, 10, n-9, n-10, 11, 12, n-11…</p>
      <p>Exemplos:</p>
      <list>
        <item><p>Folheto de 4 páginas: escreva <input>4,1,2,3</input></p></item>
        <item><p>Folheto de 8 páginas: escreva <input>8,1,2,7,6,3,4,5</input></p></item>
        <item><p>Folheto de 20 páginas: escreva <input>20,1,2,19,18,3,4,17,16,5,6,15,14,7,8,13,12,9,10,11</input></p></item>
      </list>
    </item>
    <item>
      <p>Selecione a flange <gui>Configuração de página</gui>.</p>
      <p>Em <gui>Distribuição</gui>, escolha <gui>Folheto</gui>.</p>
      <p>Em <gui>Margens</gui>, na lista desdobrável <gui>Includir</gui> escolha <gui>Todas as páginas</gui>.</p>
    </item>
    <item>
      <p>Carregue <gui>Imprimir</gui>.</p>
    </item>
  </steps>

</page>
