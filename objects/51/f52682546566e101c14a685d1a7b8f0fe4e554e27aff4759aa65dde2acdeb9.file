<?xml version="1.0" encoding="UTF-8"?>
<component type="addon">
  <id>network-manager-ssh</id>
  <project_license>GPL-2.0+</project_license>
  <metadata_license>CC0-1.0</metadata_license>
  <extends>nm-connection-editor.desktop</extends>
  <extends>gnome-control-center.desktop</extends>
  <name>SSH VPN Tunnel</name>
  <summary>SSH VPN integration for NetworkManager</summary>

  <keywords>
    <keyword>network</keyword>
    <keyword>manager</keyword>
    <keyword>NetworkManager</keyword>
    <keyword>connection</keyword>
    <keyword>VPN</keyword>
    <keyword>SSH</keyword>
  </keywords>

  <description>
    <p>Support for tunnelling IP traffic via SSH connections.</p>
    <p>This is useful to establish a VPN using tunnel capabilities of OpenSSH.</p>
  </description>

  <screenshots>
    <screenshot type="default">
      <image width="800" height="800">https://raw.githubusercontent.com/danfruehauf/NetworkManager-ssh/master/appdata/ssh.png</image>
    </screenshot>

    <screenshot type="default">
      <caption>The advanced options dialog</caption>
      <image width="800" height="450">https://raw.githubusercontent.com/danfruehauf/NetworkManager-ssh/master/appdata/ssh-advanced.png</image>
    </screenshot>
  </screenshots>

  <url type="homepage">https://github.com/danfruehauf/NetworkManager-ssh</url>
  <url type="bugtracker">https://github.com/danfruehauf/NetworkManager-ssh/issues</url>
  <update_contact>malkodan@gmail.com</update_contact>
  <translation type="gettext">NetworkManager-ssh</translation>
  <developer_name>Dan Fruehauf</developer_name>
</component>