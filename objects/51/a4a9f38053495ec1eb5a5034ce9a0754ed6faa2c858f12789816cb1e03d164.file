<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-wireless" xml:lang="fi">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-batterylife"/>

    <revision pkgversion="3.20" date="2016-06-15" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2016</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Bluetooth, wifi ja mobiililaajakaista voidaan sammuttaa akun varauksen säästämiseksi.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

  <title>Switch off unused wireless technologies</title>

  <p>You can reduce battery use by switching off bluetooth, wi-fi or mobile
  broadband when they are not in use.</p>

  <steps>

    <item>
      <p>Avaa <gui xref="shell-introduction#activities">Toiminnot</gui>-yleisnäkymä ja ala kirjoittamaan <gui>Virransäästö</gui>.</p>
    </item>
    <item>
      <p>Napsauta <gui>Virranhallinta</gui> avataksesi paneelin.</p>
    </item>
    <item>
      <p>The <gui>Power Saving</gui> section contains switches for
      <gui>Wi-Fi</gui>, <gui>Mobile broadband</gui>, and <gui>Bluetooth</gui>.
      Switch the unused services to off. Re-enable when needed by switching to
      on.</p>
    </item>

  </steps>

</page>
