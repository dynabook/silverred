<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-lost" xml:lang="pl">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="author">
      <name>Projekt dokumentacji GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Wskazówki co robić, jeśli nie można odnaleźć utworzonego lub pobranego pliku.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  </info>

<title>Wyszukiwanie zgubionego pliku</title>

<p>Jeśli utworzono lub pobrano plik, ale teraz nie można go odnaleźć, to można skorzystać z tych wskazówek.</p>

<list>
  <item><p>Jeśli nie pamiętasz miejsca zapisania pliku, ale coś pamiętasz o jego nazwie, to można <link xref="files-search">wyszukać go według nazwy</link>.</p></item>

  <item><p>Jeśli właśnie pobrano plik, to przeglądarka WWW mogła automatycznie zapisać go do jednego z często używanych katalogów. Sprawdź katalogi <file>Pulpit</file> i <file>Pobrane</file> w katalogu domowym.</p></item>

  <item><p>Być może przypadkowo usunięto plik. Usunięcie pliku powoduje jego przeniesienie do kosza, gdzie znajduje się do ręcznego opróżnienia kosza. <link xref="files-recover"/> zawiera informacje o odzyskiwaniu usuniętego pliku.</p></item>

  <item><p>Być może zmieniono nazwę pliku tak, że został ukryty. Pliki zaczynające się od znaku <file>.</file> lub kończące się znakiem <file>~</file> są ukrywane w menedżerze plików. Kliknij przycisk opcji widoku na pasku narzędziowym programu <app>Pliki</app> i zaznacz opcję <gui>Ukryte pliki</gui>, aby je wyświetlić. <link xref="files-hidden"/> zawiera więcej informacji.</p></item>
</list>

</page>
