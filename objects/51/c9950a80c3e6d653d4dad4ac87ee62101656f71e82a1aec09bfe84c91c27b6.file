<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="a11y task" id="a11y-screen-reader" xml:lang="sr">

  <info>
    <link type="guide" xref="a11y#vision" group="blind"/>

    <revision pkgversion="3.13.92" date="2014-09-20" status="incomplete"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Шон Мек Кенс</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Јана Хевес</name>
      <email>jsvarova@gnome.org</email>
    </credit>

    <desc>Користите читач екрана <app>Орку</app> да изговарате корисничко сучеље.</desc>
  </info>

  <title>Читајте екран на глас</title>

  <p>Гном обезбеђује читач екрана <app>Орку</app> за читање корисничког сучеља. Зависно од тога како сте инсталирали Гном, можда нећете имати инсталирану Орку. Ако је тако, прво инсталирајте Орку.</p>

  <p if:test="action:install"><link style="button" action="install:orca">Инсталирајте Орку</link></p>
  
  <p>Да покренете <app>Орку</app> користећи тастатуру:</p>
  
  <steps>
    <item>
    <p>Притисните <key>Супер</key>+<key>Алт</key>+<key>С</key>.</p>
    </item>
  </steps>
  
  <p>Или да покренете <app>Орку</app> користећи миша и тастатуру:</p>

  <steps>
    <item>
      <p>Отворите преглед <gui xref="shell-introduction#activities">Активности</gui> и почните да куцате <gui>Универзални приступ</gui>.</p>
    </item>
    <item>
      <p>Кликните на <gui>Универзални приступ</gui> да отворите панел.</p>
    </item>
    <item>
      <p>Притисните на <gui>Читач екрана</gui> у одељку <gui>Видети</gui>, затим укључите <gui>читача екрана</gui> у прозорчету.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Брзо укључите или искључите читача екрана</title>
    <p>Можете да укључите или да искључите читач екрана тако што ћете притиснути на <link xref="a11y-icon">иконицу приступачности</link> на горњој траци и изабрати <gui>Читач екрана</gui>.</p>
  </note>

  <p>Погледајте <link href="help:orca">Помоћ Орке</link> за више података.</p>
</page>
