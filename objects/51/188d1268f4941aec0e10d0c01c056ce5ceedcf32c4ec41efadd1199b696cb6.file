<?xml version="1.0" encoding="UTF-8"?>
<libosinfo version="0.0.1">

  <os id="http://endlessos.com/eos/3.1">
    <short-id>eos3.1</short-id>
    <name>Endless OS 3.1</name>
    <name xml:lang="fr">Endless OS 3.1</name>
    <name xml:lang="pl">Endless OS 3.1</name>
    <name xml:lang="uk">Endless OS 3.1</name>
    <version>3.1</version>
    <vendor>Endless Mobile, Inc</vendor>
    <vendor xml:lang="ca">Endless Mobile, Inc</vendor>
    <vendor xml:lang="fr">Endless Mobile, Inc</vendor>
    <vendor xml:lang="it">Endless Mobile, Inc</vendor>
    <vendor xml:lang="pl">Endless Mobile, Inc</vendor>
    <vendor xml:lang="uk">Endless Mobile, Inc</vendor>
    <family>linux</family>
    <distro>eos</distro>

    <release-date>2017-01-17</release-date>

    <eol-date>2017-07-18</eol-date>



    <variant id="ar">
      <name>Endless OS Arabic</name>
      <name xml:lang="fr">Endless OS Arabe</name>
      <name xml:lang="it">Endless OS (arabo)</name>
      <name xml:lang="pl">Endless OS (arabski)</name>
      <name xml:lang="uk">Endless OS Arabic</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="ar"/>
      <url>https://images-dl.endlessm.com/release/3.1.8/eos-amd64-amd64/ar/eos-eos3.1-amd64-amd64.170520-060632.ar.iso</url>
      <iso>
        <volume-id>Endless-OS-3-1-\d+-ar$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <variant id="base">
      <name>Endless OS Basic</name>
      <name xml:lang="fr">Endless OS Basique</name>
      <name xml:lang="it">Endless OS Basic</name>
      <name xml:lang="pl">Endless OS Basic</name>
      <name xml:lang="uk">Endless OS Basic</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="base"/>
      <url>https://images-dl.endlessm.com/release/3.1.8/eos-amd64-amd64/base/eos-eos3.1-amd64-amd64.170520-055517.base.iso</url>
      <iso>
        <volume-id>Endless-OS-3-1-\d+-base$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <variant id="bn">
      <name>Endless OS Bengali</name>
      <name xml:lang="fr">Endless OS Bengali</name>
      <name xml:lang="it">Endless OS (bengalese)</name>
      <name xml:lang="pl">Endless OS (bengalski)</name>
      <name xml:lang="uk">Endless OS Bengali</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="bn"/>
      <url>https://images-dl.endlessm.com/release/3.1.8/eos-amd64-amd64/bn/eos-eos3.1-amd64-amd64.170520-060733.bn.iso</url>
      <iso>
        <volume-id>Endless-OS-3-1-\d+-bn$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <variant id="en">
      <name>Endless OS English</name>
      <name xml:lang="fr">Endless OS Anglais</name>
      <name xml:lang="it">Endless OS (inglese)</name>
      <name xml:lang="pl">Endless OS (angielski)</name>
      <name xml:lang="uk">Endless OS English</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="en"/>
      <url>https://images-dl.endlessm.com/release/3.1.8/eos-amd64-amd64/en/eos-eos3.1-amd64-amd64.170520-071933.en.iso</url>
      <iso>
        <volume-id>Endless-OS-3-1-\d+-en$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <variant id="es">
      <name>Endless OS Spanish</name>
      <name xml:lang="fr">Endless OS Espagnol</name>
      <name xml:lang="it">Endless OS (spagnolo)</name>
      <name xml:lang="pl">Endless OS (hiszpański)</name>
      <name xml:lang="uk">Endless OS Spanish</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="es"/>
      <url>https://images-dl.endlessm.com/release/3.1.8/eos-amd64-amd64/es/eos-eos3.1-amd64-amd64.170520-061122.es.iso</url>
      <iso>
        <volume-id>Endless-OS-3-1-\d+-es$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <variant id="es_GT">
      <name>Endless OS Spanish (Guatemala)</name>
      <name xml:lang="fr">Endless OS Espagnol (Guatemala)</name>
      <name xml:lang="it">Endless OS (spagnolo, Guatemala)</name>
      <name xml:lang="pl">Endless OS (hiszpański, Gwatemala)</name>
      <name xml:lang="uk">Endless OS Spanish (Гватемала)</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="es_GT"/>
      <url>https://images-dl.endlessm.com/release/3.1.8/eos-amd64-amd64/es_GT/eos-eos3.1-amd64-amd64.170520-061652.es_GT.iso</url>
      <iso>
        <volume-id>Endless-OS-3-1-\d+-es_GT$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <variant id="es_MX">
      <name>Endless OS Spanish (Mexico)</name>
      <name xml:lang="fr">Endless OS Espagnol (Mexique)</name>
      <name xml:lang="it">Endless OS (spagnolo, Messico)</name>
      <name xml:lang="pl">Endless OS (hiszpański, Meksyk)</name>
      <name xml:lang="uk">Endless OS Spanish (Мексика)</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="es_MX"/>
      <url>https://images-dl.endlessm.com/release/3.1.8/eos-amd64-amd64/es_MX/eos-eos3.1-amd64-amd64.170520-082741.es_MX.iso</url>
      <iso>
        <volume-id>Endless-OS-3-1-\d+-es_MX$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <variant id="fr">
      <name>Endless OS French</name>
      <name xml:lang="fr">Endless OS Français</name>
      <name xml:lang="it">Endless OS (francese)</name>
      <name xml:lang="pl">Endless OS (francuski)</name>
      <name xml:lang="uk">Endless OS French</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="fr"/>
      <url>https://images-dl.endlessm.com/release/3.1.8/eos-amd64-amd64/fr/eos-eos3.1-amd64-amd64.170521-045821.fr.iso</url>
      <iso>
        <volume-id>Endless-OS-3-1-\d+-fr$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <variant id="id">
      <name>Endless OS Indonesian</name>
      <name xml:lang="fr">Endless OS Indonésien</name>
      <name xml:lang="it">Endless OS (indonesiano)</name>
      <name xml:lang="pl">Endless OS (indonezyjski)</name>
      <name xml:lang="uk">Endless OS Indonesian</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="id"/>
      <url>https://images-dl.endlessm.com/release/3.1.8/eos-amd64-amd64/id/eos-eos3.1-amd64-amd64.170521-050143.id.iso</url>
      <iso>
        <volume-id>Endless-OS-3-1-\d+-id$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <variant id="pt_BR">
      <name>Endless OS Portuguese (Brazil)</name>
      <name xml:lang="fr">Endless OS Portugais (Brésil)</name>
      <name xml:lang="it">Endless OS (portoghese, Brasile)</name>
      <name xml:lang="pl">Endless OS (brazylijski portugalski)</name>
      <name xml:lang="uk">Endless OS Portuguese (Бразилія)</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="pt_BR"/>
      <url>https://images-dl.endlessm.com/release/3.1.8/eos-amd64-amd64/pt_BR/eos-eos3.1-amd64-amd64.170520-101852.pt_BR.iso</url>
      <iso>
        <volume-id>Endless-OS-3-1-\d+-pt_BR$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <variant id="th">
      <name>Endless OS Thai</name>
      <name xml:lang="fr">Endless OS Thaï</name>
      <name xml:lang="it">Endless OS (tailandese)</name>
      <name xml:lang="pl">Endless OS (tajski)</name>
      <name xml:lang="uk">Endless OS Thai</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="th"/>
      <url>https://images-dl.endlessm.com/release/3.1.8/eos-amd64-amd64/th/eos-eos3.1-amd64-amd64.170521-050122.th.iso</url>
      <iso>
        <volume-id>Endless-OS-3-1-\d+-th$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <variant id="vi">
      <name>Endless OS Vietnamese</name>
      <name xml:lang="fr">Endless OS Vietnamien</name>
      <name xml:lang="it">Endless OS (vietnamita)</name>
      <name xml:lang="pl">Endless OS (wietnamski)</name>
      <name xml:lang="uk">Endless OS Vietnamese</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="vi"/>
      <url>https://images-dl.endlessm.com/release/3.1.8/eos-amd64-amd64/vi/eos-eos3.1-amd64-amd64.170520-090541.vi.iso</url>
      <iso>
        <volume-id>Endless-OS-3-1-\d+-vi$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <variant id="zh_CN">
      <name>Endless OS Chinese (China)</name>
      <name xml:lang="fr">Endless OS Chinois (Chine)</name>
      <name xml:lang="it">Endless OS (cinese, Cina)</name>
      <name xml:lang="pl">Endless OS (chiński, Chiny)</name>
      <name xml:lang="uk">Endless OS Chinese (Китай)</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="zh_CN"/>
      <url>https://images-dl.endlessm.com/release/3.1.8/eos-amd64-amd64/zh_CN/eos-eos3.1-amd64-amd64.170520-083345.zh_CN.iso</url>
      <iso>
        <volume-id>Endless-OS-3-1-\d+-zh_CN$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>


    <variant id="sea">
      <name>Endless OS Southeast Asia</name>
      <name xml:lang="fr">Endless OS Asie du Sud-Est</name>
      <name xml:lang="pl">Endless OS (Azja Południowo-Wschodnia)</name>
      <name xml:lang="uk">Endless OS Southeast Asia</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="sea"/>
      <iso>
        <volume-id>Endless-OS-3-1-\d+-sea$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <variant id="other">
      <name>Endless OS (other)</name>
      <name xml:lang="fr">Endless OS (autres)</name>
      <name xml:lang="pl">Endless OS (inne)</name>
      <name xml:lang="uk">Endless OS (інша)</name>
    </variant>

    <media live="true" arch="all">
      <variant id="other"/>
      <iso>
        <volume-id>\D+-3-1-\d+</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <resources arch="all">
      <minimum>
        <n-cpus>1</n-cpus>
        <cpu>1000000000</cpu>
        <ram>2147483648</ram>
        <storage>21474836480</storage>
      </minimum>

      <recommended>
        <ram>2147483648</ram>
        <storage>42949672960</storage>
      </recommended>
    </resources>

  </os>
</libosinfo>