<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" id="shell-apps-auto-start" xml:lang="de">

  <info>
    <link type="guide" xref="shell-overview#desktop"/>

    <revision pkgversion="3.33" date="2019-07-21" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Aruna Sankaranarayanan</name>
      <email>aruna.evam@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <desc>Nutzen Sie <app>Optimierungen</app>, um Anwendungen automatisch bei Anmeldung zu starten.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2019.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2019.</mal:years>
    </mal:credit>
  </info>

  <title>Anwendungen automatisch bei Anmeldung starten</title>

  <p>Bei Anmeldung startet Ihr Rechner automatisch einige Anwendungen im Hintergrund. Sie sind in der Regel wichtige Programme, die für einen reibungslosen Betrieb der Arbeitssitzung sorgen.</p>

  <p>Nutzen Sie die Anwendung <app>Optimierungen</app>, um weitere Anwendungen zur Liste der automatisch zu startenden Programme hinzuzufügen, die Sie oft einsetzen. Beispiele für Anwendungen sind Internet-Browser oder Textbearbeitungsprogramme.</p>

  <note style="important">
    <p>Das <app>Optimierungswerkzeug</app> muss auf Ihrem Rechner installiert sein, um diese Einstellung ändern zu können.</p>
    <if:if test="action:install">
      <p><link style="button" action="install:gnome-tweaks">Installieren Sie das <app>Optimierungswerkzeug</app></link></p>
    </if:if>
  </note>

  <steps>
    <title>So starten Sie eine Anwendung automatisch bei Anmeldung:</title>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Optimierungswerkzeug</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Optimierungswerkzeug</gui>, um die Anwendung zu öffnen.</p>
    </item>
    <item>
      <p>Wählen Sie den Reiter <gui>Startanwendungen</gui>.</p>
    </item>
    <item>
      <p>Klicken Sie auf den Knopf <gui style="button">+</gui>, um die Liste verfügbarer Anwendungen zu sehen.</p>
    </item>
    <item>
      <p>Klicken Sie auf den Knopf <gui style="button">Hinzufügen</gui>, um eine Anwendung Ihrer Wahl der Liste hinzuzufügen.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Entfernen Sie eine Anwendung aus der Liste, indem Sie auf den Knopf <gui style="button">Entfernen</gui> neben der jeweiligen Anwendung klicken.</p>
  </note>

</page>
