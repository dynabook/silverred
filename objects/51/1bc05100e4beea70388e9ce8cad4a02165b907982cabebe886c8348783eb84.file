<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="units" xml:lang="sv">
  <info>
    <revision version="0.2" pkgversion="3.11" date="2014-01-26" status="review"/>
    <link type="guide" xref="index#other" group="other"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author copyright">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
      <years>2011</years>
    </credit>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2011, 2014</years>
    </credit>

    <desc>Måttenheter för minne och diskutrymme</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isak Östlund</mal:name>
      <mal:email>translate@catnip.nu</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Är GiB detsamma som GB (gigabyte)?</title>

    <p>Statistik för minne och diskutrymme visas i <em>binära prefix enligt IEC</em>, KiB, MiB, GiB, TiB (för kibi, mebi, gibi, och tebi). Dessa är avsedda för att särskilja binär rapportering av storlekar som används i Systemövervakaren (multipel av 1024) från decimalstorlekar (multipel av 1000) som vanligtvis används vid förpackning av hårddiskar.</p>

    <p>Vanliga binära enheter:</p>
  <list>
    <item><p>1 KiB = 1024 byte</p></item>
    <item><p>1 MiB = 1048576 byte</p></item>
    <item><p>1 GiB = 1073741842 byte</p></item>
  </list>

    <p>En extern hårddisk, beskriven som 1.0 TB (terabyte), skulle visas som 0.909 TiB (tebibyte).</p>

</page>
