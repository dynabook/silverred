<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="getting-started" version="1.0 if/1.0" xml:lang="cs">

<info>
    <link type="guide" xref="index" group="gs"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <desc>Začínáte s GNOME? Naučte se základní triky.</desc>
    <title type="link">Začínáme s GNOME</title>
    <title type="text">Začínáme</title>
</info>

<title>Začínáme</title>

<if:choose>
<if:when test="!platform:gnome-classic">

  <ui:overlay width="235" height="145">
  <media type="video" its:translate="no" src="figures/gnome-launching-applications.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-launching-apps.svg">
      <ui:caption its:translate="yes"><desc style="center">Spouštění aplikací</desc></ui:caption>
    </ui:thumb>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>Spouštění aplikací</tt:p>
         </tt:div>
         <tt:div begin="5s" end="7.5s">
           <tt:p>Přejeďte ukazatelem myši do rohu <gui>Činnosti</gui> v levém horním rohu obrazovky.</tt:p>
           </tt:div>
         <tt:div begin="7.5s" end="9.5s">
           <tt:p>Klikněte na ikonu <gui>Zobrazit aplikace</gui>.</tt:p>
         </tt:div>
         <tt:div begin="9.5s" end="11s">
           <tt:p>Klikněte na aplikaci, kterou chcete spustit, například Nápověda.</tt:p>
         </tt:div>
         <tt:div begin="12s" end="21s">
           <tt:p>Případně použijte klávesnici k otevření <gui>Přehledu činností</gui> zmáčknutím klávesy <key href="help:gnome-help/keyboard-key-super">Super</key>.</tt:p>
         </tt:div>
         <tt:div begin="22s" end="29s">
           <tt:p>Začněte psát název aplikace, kterou chcete spustit.</tt:p>
         </tt:div>
         <tt:div begin="30s" end="33s">
           <tt:p>Zmáčknutím <key>Enter</key> aplikaci spustíte.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

</if:when>
</if:choose>

  <ui:overlay width="235" height="145">
  <media type="video" its:translate="no" src="figures/gnome-task-switching.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-task-switching.svg">
        <ui:caption its:translate="yes"><desc style="center">Přepínání úloh</desc></ui:caption>
    </ui:thumb>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>Přepínání úloh</tt:p>
         </tt:div>
         <tt:div begin="5s" end="8s">
           <tt:p if:test="!platform:gnome-classic">Přejeďte ukazatelem myši do rohu <gui>Činnosti</gui> v levém horním rohu obrazovky.</tt:p>
         </tt:div>
         <tt:div begin="9s" end="12s">
           <tt:p>Kliknutím na okno se přepnete na příslušnou úlohu.</tt:p>
         </tt:div>
         <tt:div begin="12s" end="14s">
           <tt:p>Aby se okno maximalizovalo podél levé strany obrazovky, uchopte jej za záhlaví a přetáhněte doleva.</tt:p>
         </tt:div>
         <tt:div begin="14s" end="16s">
           <tt:p>Až se zvýrazní polovina obrazovky, okno upusťte.</tt:p>
         </tt:div>
         <tt:div begin="16s" end="18">
           <tt:p>Aby se okno maximalizovalo podél pravé strany obrazovky, uchopte jej za záhlaví a přetáhněte doprava.</tt:p>
         </tt:div>
         <tt:div begin="18s" end="20s">
           <tt:p>Až se zvýrazní polovina obrazovky, okno upusťte.</tt:p>
         </tt:div>
         <tt:div begin="23s" end="27s">
           <tt:p>Zmáčknutím <keyseq><key href="help:gnome-help/keyboard-key-super">Super</key> <key>Tab</key></keyseq> zobrazte <gui>přepínač oken</gui>.</tt:p>
         </tt:div>
         <tt:div begin="27s" end="29s">
           <tt:p>Uvolněním klávesy <key href="help:gnome-help/keyboard-key-super">Super</key> se vybere následující zvýrazněné okno.</tt:p>
         </tt:div>
         <tt:div begin="29s" end="32s">
           <tt:p>Jestli chcete seznam otevřených oken postupně procházet, tak klávesu <key href="help:gnome-help/keyboard-key-super">Super</key> nepouštějte, ale stále ji držte a mačkejte <key>Tab</key>.</tt:p>
         </tt:div>
         <tt:div begin="35s" end="37s">
           <tt:p>Zmáčknutím klávesy <key href="help:gnome-help/keyboard-key-super">Super</key> zobrazte <gui>Přehled činností</gui>.</tt:p>
         </tt:div>
         <tt:div begin="37s" end="40s">
           <tt:p>Začněte psát název aplikace, na kterou se chcete přepnout.</tt:p>
         </tt:div>
         <tt:div begin="40s" end="43s">
           <tt:p>Až se požadovaná aplikace objeví ve výsledcích jako první, zmáčkněte <key>Enter</key> a tím se na ni přepnete.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

  <ui:overlay width="235" height="145">
  <media type="video" its:translate="no" src="figures/gnome-windows-and-workspaces.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-windows-and-workspaces.svg">
          <ui:caption its:translate="yes"><desc style="center">Používání oken a pracovních ploch</desc></ui:caption>
    </ui:thumb>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>Okna a pracovní plochy</tt:p>
         </tt:div>
         <tt:div begin="6s" end="10s">
           <tt:p>Když chcete okno maximalizovat, chytněte jej za záhlaví a táhněte jej k horní části obrazovky.</tt:p>
           </tt:div>
         <tt:div begin="10s" end="13s">
           <tt:p>Až se obrazovka zvýrazní, okno upusťte.</tt:p>
         </tt:div>
         <tt:div begin="14s" end="20s">
           <tt:p>Když chcete maximalizaci okna zrušit, chyťte jej za záhlaví a přetáhněte jej pryč od okrajů obrazovky.</tt:p>
         </tt:div>
         <tt:div begin="25s" end="29s">
           <tt:p>Pro přetažení okna pryč a zrušení jeho maximalizace můžete kliknou i na horní lištu, místo na záhlaví.</tt:p>
         </tt:div>
         <tt:div begin="34s" end="38s">
           <tt:p>Aby se okno maximalizovalo podél levé strany obrazovky, uchopte jej za záhlaví a přetáhněte doleva.</tt:p>
         </tt:div>
         <tt:div begin="38s" end="40s">
           <tt:p>Až se zvýrazní polovina obrazovky, okno upusťte.</tt:p>
         </tt:div>
         <tt:div begin="41s" end="44s">
           <tt:p>Obdobně pro maximalizaci okna podél pravé strany obrazovky jej uchopte za záhlaví a přetáhněte doprava.</tt:p>
         </tt:div>
         <tt:div begin="44s" end="48s">
           <tt:p>Až se zvýrazní polovina obrazovky, okno upusťte.</tt:p>
         </tt:div>
         <tt:div begin="54s" end="60s">
           <tt:p>Pomocí klávesnice se okno maximalizuje podržením <key href="help:gnome-help/keyboard-key-super">Super</key> a zmáčknutím <key>↑</key>.</tt:p>
         </tt:div>
         <tt:div begin="61s" end="66s">
           <tt:p>Pro obnovení původní velikosti okna z maximalizace podržte zmáčknutou klávesu <key href="help:gnome-help/keyboard-key-super">Super</key> a zmáčkněte <key>↓</key>.</tt:p>
         </tt:div>
         <tt:div begin="66s" end="73s">
           <tt:p>Pro maximalizaci okna podél pravé strany obrazovky podržte zmáčknutou klávesu <key href="help:gnome-help/keyboard-key-super">Super</key> a zmáčkněte <key>→</key>.</tt:p>
         </tt:div>
         <tt:div begin="76s" end="82s">
           <tt:p>Pro maximalizaci okna podél levé strany obrazovky podržte zmáčknutou klávesu <key href="help:gnome-help/keyboard-key-super">Super</key> a zmáčkněte <key>←</key>.</tt:p>
         </tt:div>
         <tt:div begin="83s" end="89s">
           <tt:p>K přesunu na pracovní plochu pod aktuální pracovní plochou použijte kombinaci <keyseq><key href="help:gnome-help/keyboard-key-super">Super</key><key>Page Down</key></keyseq>.</tt:p>
         </tt:div>
         <tt:div begin="90s" end="97s">
           <tt:p>K přesunu na pracovní plochu nad aktuální pracovní plochou použijte kombinaci <keyseq><key href="help:gnome-help/keyboard-key-super">Super</key><key>Page Up</key></keyseq>.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

<links type="topic" style="grid" groups="tasks">
<title style="heading">Obvyklé činnosti</title>
</links>

<links type="guide" style="heading nodesc">
<title/>
</links>

</page>
