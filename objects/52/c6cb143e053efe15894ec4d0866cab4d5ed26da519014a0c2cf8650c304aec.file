<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="music-player-ipodtransfer" xml:lang="cs">
  <info>
    <link type="guide" xref="media#music"/>


    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <desc>Ke zkopírování písniček a jejich pozdějšímu bezpečnému odstranění ze zařízení iPod použijte multimediální přehrávač.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

<title>Písničky se neobjeví v mém iPodu, když je do něj nakopíruji</title>

<p>Když ke svému počítači připojíte iPod, objeví se v aplikaci pro přehrávání hudby a také ve správci souborů (aplikaci <app>Soubory</app> v přehledu <gui>Činností</gui>). Skladby do něj musíte kopírovat v hudebním přehrávači – když je zkopírujete pomocí správce souborů, nebude to fungovat, protože skladby nebudou na správném místě. iPod má speciální umístění pro skladby, o kterém hudební přehrávač ví, ale správce souborů jej nezná.</p>

<p>Než iPod odpojíte, musíte počkat, než se kopírování skladeb dokončí. Než jej odpojíte, nezapomeňte použít <link xref="files-removedrive">bezpečné odebrání</link>. Tím se zajistí, že všechny skladby budou zkopírovány opravdu správně.</p>

<p>Dalším důvodem, proč se písničky nemusí objevit ve vašem iPodu je, že aplikace hudebního přehrávače, který používáte, nepodporuje převod písniček z jednoho zvukového formátu do jiného. Když kopírujete písničku uloženou ve zvukovém formátu, který iPod nepodporuje (například .oga – Ogg Vorbis), zkusí ji hudební přehrávač převést do formátu, kterému iPod rozumí, jako je třeba MP3. V případě, že příslušný převodní software (také nazývaný kodek nebo koder) není nainstalován, nebude moci hudební přehrávač převod provést a písničku tak nezkopíruje. Po příslušném kodeku se podívejte do instalátoru softwaru.</p>

</page>
