<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mem-swap" xml:lang="ko">

  <info>
    <revision pkgversion="3.11" date="2014-01-28" status="final"/>
    <link type="guide" xref="index#memory" group="memory"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author copyright">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
      <years>2011</years>
    </credit>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2011, 2014</years>
    </credit>

    <desc>스왑 메모리는 시스템 메모리에 맞춰 더 많은 프로그램을 컴퓨터에서 동작할 수 있게 합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>"스왑" 메모리란 무엇인가요?</title>

  <p>스왑 메모리 또는 <em>스왑 공간</em>은 가상 메모리 시스템의 디스크 구성 요소입니다. 리눅스를 처음 설치했을 때 <em>스왑 분할 영역</em> 또는 <em>스왑 파일</em>로 미리 설정하지만 나중에 추가할 수 있습니다.</p>

  <note>
    <p>디스크 접근은 메모리 접근에 비해 <em>매우</em> 느립니다. 시스템에서 충분한 메모리 공간을 찾을 수 없어 스와핑 내지는 <em>스레싱</em> 할 데이터가 많으면 시스템이 더 느리게 동작하는데, 이 경우 RAM을 더 증설하는 방법밖에 없습니다.</p>
  </note>

  <p>스와핑, <em>페이징</em>이 성능 문제와 관련있는지 보려면:</p>

  <steps>
    <item>
      <p><gui>자원</gui> 탭을 누르십시오.</p>
    </item>
    <item>
      <p><gui>메모리 및 스왑 사용 기록</gui> 그래프에서는 메모리 및 스왑 사용 내역을 백분율로 보여줍니다.</p>
    </item>
  </steps>

</page>
