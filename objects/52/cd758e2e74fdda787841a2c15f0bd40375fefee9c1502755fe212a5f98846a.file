<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="get-involved" xml:lang="da">

  <info>
    <link type="guide" xref="more-help"/>
    <desc>Hvordan og hvor problemer med hjælpeemnerne kan rapporteres.</desc>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years/>
    </mal:credit>
  </info>
  <title>Vær med til at forbedre vejledningen</title>

  <section id="submit-issue">

   <title>Indsend en problemstilling</title>

   <p>Hjælpedokumentationen er skabt af et fællesskab af frivillige. Du er velkommen til at være med. Hvis du støder på et problem med hjælpesiderne (såsom stavefejl, ukorrekte instruktioner eller emner som bør dækkes men ikke er), så kan du indsende et <em>nyt issue</em>. Gå til <link href="https://gitlab.gnome.org/GNOME/gnome-user-docs/issues">issue-trackeren</link> for at indsende et nyt issue.</p>

   <p>Du skal tilmelde dig for at kunne indsende et issue og modtage opdateringer via e-mail om dets status. Har du ikke allerede en konto, så klik på <gui><link href="https://gitlab.gnome.org/users/sign_in">Sign in/Register</link></gui>-knappen for at oprette en.</p>

   <p>Når du har en konto, så sørg for at du er logget ind og gå herefter tilbage til <link href="https://gitlab.gnome.org/GNOME/gnome-user-docs/issues">dokumentationens issue-tracker</link> og klik på <gui><link href="https://gitlab.gnome.org/GNOME/gnome-user-docs/issues/new">New issue</link></gui>. <link href="https://gitlab.gnome.org/GNOME/gnome-user-docs/issues">Gennemse</link> venligst først eksisterende issues for at se om der allerede findes noget lignende.</p>

   <p>Inden du indsender dit issue, så vælg den rette etiket i <gui>Labels</gui>-menuen. Hvis du udfylder et issue til dokumentationen, så skal du vælge <gui>gnome-help</gui>-etiketten. Hvis du er i tvivl om hvilken komponent dit issue hører til, så undlad at vælge nogen.</p>

   <p>Hvis du prøver at få hjælp til et emne som du føler ikke er dækket, så vælg <gui>Feature</gui> som etiketten. Udfyld Title og Description og klik på <gui>Submit issue</gui>.</p>

   <p>Dit issue får et id-nummer og dets status opdateres efterhånden som der arbejdes på det. Tak fordi du hjælper med at forbedre GNOME Hjælp!</p>

   </section>

   <section id="contact-us">
   <title>Kontakt os</title>

   <p>Du kan sende en <link href="mailto:gnome-doc-list@gnome.org">e-mail</link> til GNOME docs-mailinglisten for at læse mere om hvordan du kan være en del af dokumentationsholdet.</p>

   </section>
</page>
