<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-booklet-duplex" xml:lang="fr">

  <info>
    <link type="guide" xref="printing-booklet"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="candidate"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Print folded booklets (like a book or pamphlet) from a PDF using normal
    A4/Letter-size paper.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Impression d'une brochure avec une imprimante double face</title>

  <p>Vous pouvez faire des brochures reliées (à la manière d'un petit livre) en imprimant les pages d'un document dans un certain ordre et en modifiant quelques options d'impression.</p>

  <p>Ces instructions permettent d'imprimer un livret à partir d'un document PDF.</p>

  <p>Si vous voulez imprimer une brochure à partir d'un document <app>LibreOffice</app>, exportez-le d'abord en PDF en sélectionnant <guiseq><gui>Fichier</gui><gui>Exporter en PDF…</gui></guiseq>. Assurez-vous que le document contient un multiple de 4 pages (4, 8, 12, 16,…). Sinon, rajoutez autant de pages blanches pour obtenir un multiple de 4.</p>

  <p>Pour imprimer une brochure :</p>

  <steps>
    <item>
      <p>Ouvrez la fenêtre d'impression. Habituellement, cliquez pour cela sur <gui style="menuitem">Imprimer</gui> dans le menu ou servez-vous du raccourci clavier <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>Click the <gui>Properties…</gui> button </p>
      <p>In the <gui>Orientation</gui> drop-down list, make sure that
      <gui>Landscape</gui> is selected.</p>
      <p>In the <gui>Duplex</gui> drop-down list, select <gui>Short Edge</gui>.
      </p>
      <p>Click <gui>OK</gui> to go back to the print dialog.</p>
    </item>
    <item>
      <p>Under <gui>Range and Copies</gui>, choose <gui>Pages</gui>.</p>
    </item>
    <item>
      <p>Saisissez les numéros de pages dans cet ordre (n est le nombre total de pages et un multiple de 4) :</p>
      <p>n, 1, 2, n-1, n-2, 3, 4, n-3, n-4, 5, 6, n-5, n-6, 7, 8, n-7, n-8, 9, 10, n-9, n-10, 11, 12, n-11…</p>
      <p>Exemples :</p>
      <list>
        <item><p>brochure de 4 pages : saisissez <input>4,1,2,3</input></p></item>
        <item><p>brochure de 8 pages : saisissez <input>8,1,2,7,6,3,4,5</input></p></item>
        <item><p>brochure de 20 pages : saisissez <input>20,1,2,19,18,3,4,17,16,5,6,15,14,7,8,13,12,9,10,11</input></p></item>
      </list>
    </item>
    <item>
      <p>Choose the <gui>Page Layout</gui> tab.</p>
      <p>Under <gui>Layout</gui>, select <gui>Brochure</gui>.</p>
      <p>Under <gui>Page Sides</gui>, in the <gui>Include</gui> drop-down list,
      select <gui>All pages</gui>.</p>
    </item>
    <item>
      <p>Cliquez sur <gui>Imprimer</gui>.</p>
    </item>
  </steps>

</page>
