<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting-initial-check" xml:lang="ca">

  <info>
    <link type="next" xref="net-wireless-troubleshooting-hardware-info"/>
    <link type="guide" xref="net-wireless-troubleshooting"/>

    <revision pkgversion="3.4.0" date="2012-03-05" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <credit type="author">
      <name>Col·laboradors a la wiki de documentació d'Ubuntu</name>
    </credit>
    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Assegureu-vos que la configuració bàsica de la xarxa sigui correcta i prepareu-vos per als propers passos de resolució de problemes.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

  <title>Solució de problemes de xarxa sense fil</title>
  <subtitle>Realitzeu una verificació de la connexió inicial</subtitle>

  <p>En aquest pas, comprovareu la informació bàsica de la vostra connexió de xarxa sense fil. Això es fa per assegurar-se que el problema de la xarxa no estigui causat per un problema relativament senzill, com ara la connexió sense fil desactivada, i per preparar-vos per als propers passos de resolució de problemes.</p>

  <steps>
    <item>
      <p>Assegureu-vos que el portàtil no estigui connectat a una connexió a internet.a <em>cablejada</em>.</p>
    </item>
    <item>
      <p>Si teniu un adaptador sense fil extern (com ara un adaptador USB o una targeta PCMCIA que es connecta al vostre ordinador portàtil), assegureu-vos que s'insereixi a la ranura adequada a l'ordinador.</p>
    </item>
    <item>
      <p>Si la vostra targeta de xarxa està a l'<em>interior</em> del vostre ordinador, assegureu-vos que l'interruptor de la xarxa sense fil estigui activat (si n'hi ha). Els ordinadors portàtils solen tenir interruptors que podeu canviar prement una combinació de tecles de teclat.</p>
    </item>
    <item>
      <p>Feu clic a l'àrea d'estat del sistema a la barra superior i seleccioneu <gui>Wi-Fi</gui>, aleshores seleccioneu <gui>Configuració Wi-Fi</gui>. Assegureu-vos que la xarxa <gui>Wi-Fi</gui> està a <gui>ON</gui>. També podeu comprovar que el <link xref="net-wireless-airplane">Mode Avió</link> <em>no</em> estigui activat.</p>
    </item>
    <item>
      <p>Obriu un terminal, teclegeu <cmd>nmcli device</cmd> i premeu <key>Retorn</key>.</p>
      <p>Això mostrarà informació sobre les interfícies de xarxa i l'estat de la connexió. Mireu la llista d'informació i comproveu si hi ha algun element relacionat amb l'adaptador de xarxa sense fil. Si l'estat és <code>connectat</code>, vol dir que l'adaptador funciona i està connectat al vostre encaminador sense fil.</p>
    </item>
  </steps>

  <p>Si esteu connectat al vostre encaminador sense fil, però encara no podeu accedir a Internet, és possible que l'encaminador no estigui configurat correctament o que el vostre proveïdor de serveis d'Internet (ISP) tingui problemes tècnics. Reviseu les vostres guies de configuració de l'encaminador i de l'ISP per assegurar-vos que la configuració sigui correcta, o poseu-vos en contacte amb l'assistència del vostre proveïdor ISP.</p>

  <p>Si la informació de <cmd>nmcli device</cmd> no indica que esteu connectat a la xarxa, feu clic a <gui>Següent</gui> per passar a la següent part de la guia de resolució de problemes.</p>

</page>
