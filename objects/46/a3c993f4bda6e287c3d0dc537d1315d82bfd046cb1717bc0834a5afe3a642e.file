<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="bluetooth-remove-connection" xml:lang="gl">

  <info>
    <link type="guide" xref="bluetooth"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.12" date="2014-03-04" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Eliminar un dispositivo desde a lista de dispositivos Bluetooth.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2020</mal:years>
    </mal:credit>
  </info>

  <title>Desconectar un dispositivo Bluetooth</title>

  <p>Se non quere volver a conectarse a un dispositivo Bluetooth pode quitar a conexión. É útil se xa non quere volver a usar un dispositivo como un rato ou uns auriculares ou se xa non vai volver a transferir ficheiros a ou desde un dispositivo.</p>

  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Actividades</gui> e comece a escribir <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Prema <gui>Bluetooth</gui> no lateral dereito.</p>
    </item>
    <item>
      <p>Seleccione o dispositivo que quere desconectar desde a lista.</p>
    </item>
    <item>
      <p>Na caixa de diálogo do dispositivo, troque a <gui>Conexión</gui> desactivada, ou quitar o dispositivo desde a lista de <gui>Dispositivos</gui>, prema <gui>Quitar dispositivo</gui>.</p>
    </item>
  </steps>

  <p>Pode <link xref="bluetooth-connect-device">volver a conectarse a un dispositivo Bluetooth</link> máis tarde se o desexa.</p>

</page>
