<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="disk-benchmark" xml:lang="fi">

  <info>
    <link type="guide" xref="disk"/>

    <revision pkgversion="3.6.2" version="0.2" date="2012-11-16" status="review"/>
    <revision pkgversion="3.10" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Gnomen dokumentointiprojekti</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
   <credit type="editor">
     <name>Michael Hill</name>
     <email>mdhillca@gmail.com</email>
   </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Suorita levylle suorituskykytestejä mitataksesi levyn nopeus.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

<title>Testaa kiintolevyn suorituskyky</title>

  <p>Testaa kiintolevyn suorituskyky:</p>

  <steps>
    <item>
      <p>Open <app>Disks</app> from the
      <gui xref="shell-introduction#activities">Activities</gui> overview.</p>
    </item>
    <item>
      <p>Choose the disk from the list in the left pane.</p>
    </item>
    <item>
      <p>Click the menu button and select <gui>Benchmark disk…</gui> from the
      menu.</p>
    </item>
    <item>
      <p>Click <gui>Start Benchmark…</gui> and adjust the <gui>Transfer
      Rate</gui> and <gui>Access Time</gui> parameters as desired.</p>
    </item>
    <item>
      <p>Click <gui>Start Benchmarking</gui> to test how fast data can be read
      from the disk. <link xref="user-admin-explain">Administrative
      privileges</link> may be required. Enter your password, or the password
      for the requested administrator account.</p>
      <note>
        <p>If <gui>Perform write-benchmark</gui> is checked, the benchmark
        will test how fast data can be read from and written to the disk. This
        will take longer to complete.</p>
      </note>
    </item>
  </steps>

  <p>Mittauksen tulokset esitetään kuvaajina sen valmistumisen jälkeen. Vihreät pisteet ja niitä yhdistävät viivat ilmaisevat hakuaikamittauksen tuloksia mittauksen edetessä (vaaka-akselilla mittaushetki, oikealla pystyakselilla hakuajat). Sininen käyrä puolestaan ilmaisee lukunopeutta ja punainen käyrä kirjoitusnopeutta tietyllä etäisyydellä levyn akselista. Nopeuden arvot näkyvät vasemmalla pystyakselilla ja vaaka-akselin prosenttiluku ilmaisee mittauskohdan kohtisuoraa etäisyyttä levyn ulkoreunasta (0% on levyn ulkoreunalla ja 100% pyörimisakselilla).</p>

  <p>Graafin alla on näkyvillä luku- ja kirjoitusnopeuden minimi-, maksimi- ja keskiarvot, keskimääräinen yhteysaika ja edellisestä suorituskykytestistä kulunut aika.</p>

</page>
