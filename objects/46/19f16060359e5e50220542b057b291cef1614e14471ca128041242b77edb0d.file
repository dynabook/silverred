<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="session-screenlocks" xml:lang="de">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="guide" xref="hardware-problems-graphics"/>

    <revision pkgversion="3.8" version="0.3" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-03" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.28" date="2018-07-19" status="review"/>
    <revision pkgversion="3.34" date="2019-11-12" status="review"/>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ändern Sie in den <gui>Privatsphäre</gui>-Einstellungen, wie lange gewartet werden soll, bevor der Bildschirm gesperrt wird.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2019.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2019.</mal:years>
    </mal:credit>
  </info>

  <title>Der Bildschirm sperrt sich zu schnell selbst</title>

  <p>Wenn Sie Ihren Rechner für ein paar Minuten verlassen, sperrt sich der Bildschirm automatisch selbst, so dass Sie Ihr Passwort eingeben müssen, um ihn wieder zu benutzen. Dies geschieht aus Sicherheitsgründen (damit sich niemand an Ihren Sachen zu schaffen macht, wenn Sie den Rechner unbeaufsichtigt lassen), aber es kann lästig sein, wenn sich der Bildschirm zu schnell sperrt.</p>

  <p>So legen Sie eine längere Wartezeit vor dem automatischen Sperren des Bildschirms fest:</p>

  <steps>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Privatsphäre</gui> ein.</p>
    </item>

    <item>
      <p>Klicken Sie auf <gui>Privatsphäre</gui>, um das Panel zu öffnen.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Bildschirmsperre</gui>.</p>
    </item>
    <item>
      <p>Wenn <gui>Automatische Bildschirmsperre</gui> aktiviert ist, können Sie den Wert in der Auswahlliste <gui>Bildschirm sperren nach</gui> ändern.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Wenn Sie nicht wollen, dass der Bildschirm jemals automatisch gesperrt wird, schalten Sie den Schalter für <gui>Automatische Bildschirmsperre</gui> aus.</p>
  </note>

</page>
