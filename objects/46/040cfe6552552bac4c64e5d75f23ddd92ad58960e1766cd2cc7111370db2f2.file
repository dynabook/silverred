<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="overrides" xml:lang="gl">

  <info>
    <link type="guide" xref="setup"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

    <desc>GSettings overrides are used by distributions to adjust default
    settings.</desc>
   
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2010-2018</mal:years>
    </mal:credit>
  </info>

  <title>Why should I not use GSettings overrides?</title>

  <p>GSettings overrides are used by distributions to adjust default settings
  for the GNOME desktop and apps. dconf overrides were designed for system
  administrators to adjust default settings and set mandatory settings for the
  GNOME desktop and apps.</p>

  <section id="what-are-vendor-overrides">
  <title>What are vendor overrides?</title>

   <p>Default values are defined in the schemas that are installed by an
   application. Sometimes, it is necessary for a vendor or distributor to adjust
   these defaults.</p>

   <p>Since patching the XML source for the schema is inconvenient and
   error-prone, <link its:translate="no" href="man:glib-compile-schemas">
   <sys>glib-compile-schemas</sys></link> reads so-called <em>vendor override</em>
   files. These are keyfiles in the same directory as the XML schema sources,
   which can override default values.</p>

  </section>

</page>
