<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="nautilus-bookmarks-edit" xml:lang="gl">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Engadir, eliminar e renomear marcadores no xestor de ficheiros.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2020</mal:years>
    </mal:credit>
  </info>

  <title>Editar marcadores de cartafoles</title>

  <p>Os seus marcadores móstranse na barra lateral do xestor de ficheiros.</p>

  <steps>
    <title>Engadir un marcador:</title>
    <item>
      <p>Abrir o cartafol (ou a localización) que quere engadir aos marcadores.</p>
    </item>
    <item>
      <p>Prema o menú da xanela na barra de ferramentas e selecconie <gui>Marcar esta localización</gui>.</p>
    </item>
  </steps>

  <steps>
    <title>Eliminar un marcador:</title>
    <item>
      <p>Prema co botón dereito sobre o marcador na barra lateral e seleccione <gui>Eliminar</gui> desde o menú.</p>
    </item>
  </steps>

  <steps>
    <title>Renomear un marcador:</title>
    <item>
      <p>Prema co botón dereito no marcador dese panel lateral e seleccione <gui>Renomear…</gui>.</p>
    </item>
    <item>
      <p>Na casiña <gui>Nome</gui> escriba o nome novo para o marcador.</p>
      <note>
        <p>Renaming a bookmark does not rename the folder. If you have
        bookmarks to two different folders in two different locations, but
        which each have the same name, the bookmarks will have the same name,
        and you won’t be able to tell them apart. In these cases, it is useful
        to give a bookmark a name other than the name of the folder it points
        to.</p>
      </note>
    </item>
  </steps>

</page>
