<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-findip" xml:lang="es">

  <info>
    <link type="guide" xref="net-general"/>
    <link type="seealso" xref="net-what-is-ip-address"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Conocer su dirección IP le puede ayudar a resolver problemas de red.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Buscar su dirección IP</title>

  <p>Conocer su dirección IP le puede ayudar a solucionar problemas con su conexión a Internet. Es posible que se sorprenda al saber que tiene <em>dos</em> direcciones IP: una dirección IP de su equipo en la red interna y una dirección IP de su equipo en Internet.</p>

  <steps>
    <title>Buscar su dirección IP interna (de red)</title>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Red</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Red</gui> para abrir el panel.</p>
    </item>
    <item>
      <p>Elija la conexión <gui>Inalámbrica</gui> o <gui>Cableada</gui> en el panel de la izquierda.</p>
      <p>La IP de una conexión cableada se mostrará a la derecha.</p>
      
      <p>Pulse el botón <media its:translate="no" type="image" src="figures/emblem-system.png"><span its:translate="yes">configuración</span></media> para ver la dirección IP de la red inalámbrica en el panel <gui>Detalles</gui>.</p>
    </item>
  </steps>

  <steps>
  	<title>Buscar su dirección IP externa (Internet)</title>
    <item>
      <p>Visite <link href="http://whatismyipaddress.com/">whatismyipaddress.com</link>.</p>
    </item>
    <item>
      <p>El sitio le mostrará su dirección IP externa.</p>
    </item>
  </steps>

  <p>Dependiendo de cómo se conecte su equipo a Internet, estas direcciones pueden ser iguales.</p>

</page>
