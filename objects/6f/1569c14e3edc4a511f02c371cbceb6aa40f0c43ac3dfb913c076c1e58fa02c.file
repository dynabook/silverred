<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="video-sending" xml:lang="cs">

  <info>
    <link type="guide" xref="media#videos"/>
    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.12.1" date="2014-03-30" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.17.90" date="2015-08-30" status="final"/>

    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Zkontrolujte, jestli mají nainstalované správné kodeky videa.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Jiní lidé nemohou přehrát mnou vytvořená videa</title>

  <p>Když vytvoříte na svém počítači s Linuxem video a pošlete jej někomu, kdo používá Windows nebo Mac OS, můžete se setkat s tím, že bude mít problémy s jeho přehráním.</p>

  <p>Aby šlo video přehrát, musí mít osoba, které jej posíláte, nainstalovaný správný <em>kodek</em>. Kodek je kus softwaru, který ví, jak video zpracovat a zobrazit jej na obrazovce. Existuje spousta různých formátů videa a každý tento formát vyžaduje jiný kodek, aby šel přehrát. O jaký formát se jedná u vašeho videa, můžete zjistit následovně:</p>

  <list>
    <item>
      <p>Z přehledu <gui xref="shell-introduction#activities">Činnosti</gui> otevřete <app>Soubory</app>.</p>
    </item>
    <item>
      <p>Klikněte pravým tlačítkem na videosoubor a vyberte <gui>Vlastnosti</gui>.</p>
    </item>
    <item>
      <p>Přejděte na kartu <gui>Zvuk/video</gui> nebo <gui>Video</gui> a podívejte se, jaký <gui>Kodek</gui> je uveden v části <gui>Video</gui> a <gui>Zvuk</gui> (pokud je video ozvučené).</p>
    </item>
  </list>

  <p>Zeptejte se osoby, která má problém s přehráním, jestli má nainstalovaný správný kodek. Může ji pomoci, když si na Internetu vyhledá název kodeku spolu s název aplikace, kterou používá k přehrávání. Například, když vaše video používá formát <em>Theora</em> a váš kamarád používá Windows Media Player, ať zkusí hledat „theora windows media player“. Většinou je možnost si správný kodek zdarma stáhnou a nainstalovat.</p>

  <p>Pokud správný kodek nenajde, může zkusit <link href="http://www.videolan.org/vlc/">multimediální přehrávač VLC</link>. Funguje pod Windows i Mac OS, stejně jako v Linuxu, a podporuje spoustu různých formátů videa. Když i toto selže, zkuste převést své video do jiného formátu. To umí většina editorů videa a jsou k dispozici i specializované aplikace. Podívejte se do své aplikace pro instalaci softwaru, co máte k dispozici.</p>

  <note>
    <p>Existuje i pár dalších problémů, které mohou někomu bránit v přehrání vašeho videa. Video se může poškodit, když je pošlete (obvykle velké soubory se někdy mohou zkopírovat s chybou), nebo dotyčný může mít problémy se svým přehrávačem videí, nebo video nemusí být správně vytvořené (třeba mohla vzniknout nějaká chyba při ukládání videa).</p>
  </note>

</page>
