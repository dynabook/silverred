<?xml version="1.0" encoding="UTF-8"?>
<libosinfo version="0.0.1">

  <install-script id='http://microsoft.com/windows/unattend/jeos'>
    <profile>jeos</profile>
    <path-format>dos</path-format>
    <product-key-format>$$$$$-$$$$$-$$$$$-$$$$$-$$$$$</product-key-format>
   <expected-filename>autounattend.xml</expected-filename>
   <can-pre-install-drivers/>
    <config>
      <param policy="optional" name="admin-password"/>
      <param policy="optional" name="hardware-arch"/>
      <param value-map="http://microsoft.com/win/8/l10n-language-reverse" policy="optional" name="l10n-language"/>
      <param policy="optional" name="user-login"/>
      <param policy="optional" name="user-password"/>
      <param policy="optional" name="user-realname"/>
      <param policy="required" name="reg-product-key"/>
      <param policy="optional" name="target-disk"/>
      <param policy="optional" name="pre-install-drivers-disk"/>
      <param policy="optional" name="pre-install-drivers-location"/>
    </config>
    <injection-method>cdrom</injection-method>
    <injection-method>floppy</injection-method>
    <template>
      <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

        <xsl:output method="xml" indent="yes"/>

        <xsl:template name="target-disk">
          <xsl:choose>
            <xsl:when test="config/target-disk != ''">
              <xsl:value-of select="config/target-disk"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text>C</xsl:text>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:template>

        <xsl:template name="arch">
          <xsl:choose>
            <xsl:when test="config/hardware-arch = 'x86_64'">
              <xsl:text>amd64</xsl:text>
            </xsl:when>
            <xsl:when test="config/hardware-arch = 'i386'">
              <xsl:text>x86</xsl:text>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text>x86</xsl:text>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:template>

        <xsl:template name="pre-install-drivers-disk">
          <xsl:choose>
            <xsl:when test="config/pre-install-drivers-disk != ''">
              <xsl:value-of select="config/pre-install-drivers-disk"/>
            </xsl:when>
            <xsl:when test="script/preferred-injection-method = 'cdrom'">
              <xsl:text>E</xsl:text>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text>A</xsl:text>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:template>

        <xsl:template name="pre-install-drivers-location">
          <xsl:choose>
            <xsl:when test="config/pre-install-drivers-location != ''">
              <xsl:value-of select="config/pre-install-drivers-location"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text>\</xsl:text>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:template>

        <xsl:template name="ui-language">
          <xsl:choose>
            <xsl:when test="config/l10n-language = 'EN-GB'">
              <xsl:text>EN-US</xsl:text>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="config/l10n-language"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:template>

        <xsl:template match="/install-script-config">
          <unattend xmlns="urn:schemas-microsoft-com:unattend">
            <settings pass="windowsPE">
              <component xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:wcm="http://schemas.microsoft.com/WMIConfig/2002/State" versionScope="nonSxS" publicKeyToken="31bf3856ad364e35" name="Microsoft-Windows-PnpCustomizationsWinPE" language="neutral">
                <xsl:attribute name="processorArchitecture">
                  <xsl:call-template name="arch"/>
                </xsl:attribute>
                <DriverPaths>
                  <PathAndCredentials wcm:keyValue="1" wcm:action="add">
                    <Path><xsl:call-template name="pre-install-drivers-disk"/>:<xsl:call-template name="pre-install-drivers-location"/></Path>
                  </PathAndCredentials>
                </DriverPaths>
              </component>
              <component xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:wcm="http://schemas.microsoft.com/WMIConfig/2002/State" versionScope="nonSxS" publicKeyToken="31bf3856ad364e35" name="Microsoft-Windows-Setup" language="neutral">
                <xsl:attribute name="processorArchitecture">
                  <xsl:call-template name="arch"/>
                </xsl:attribute>
                <DiskConfiguration>
                  <WillShowUI>OnError</WillShowUI>
                  <Disk>
                    <CreatePartitions>
                      <CreatePartition>
                        <Order>1</Order>
                        <Size>1</Size>
                        <Type>Primary</Type>
                      </CreatePartition>
                    </CreatePartitions>
                    <DiskID>0</DiskID>
                    <WillWipeDisk>true</WillWipeDisk>
                    <ModifyPartitions>
                      <ModifyPartition>
                        <Active>true</Active>
                        <Extend>true</Extend>
                        <Format>NTFS</Format>
                        <Label><xsl:call-template name="target-disk"/> drive</Label>
                        <Letter><xsl:call-template name="target-disk"/></Letter>
                        <Order>1</Order>
                        <PartitionID>1</PartitionID>
                      </ModifyPartition>
                    </ModifyPartitions>
                  </Disk>
                </DiskConfiguration>
                <ImageInstall>
                  <OSImage>
                    <InstallTo>
                      <DiskID>0</DiskID>
                      <PartitionID>1</PartitionID>
                    </InstallTo>
                    <WillShowUI>OnError</WillShowUI>
                  </OSImage>
                </ImageInstall>
                <UserData>
                  <AcceptEula>true</AcceptEula>
                    <ProductKey>
                      <Key><xsl:value-of select="config/reg-product-key"/></Key>
                    </ProductKey>
                </UserData>
              </component>
              <component xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:wcm="http://schemas.microsoft.com/WMIConfig/2002/State" versionScope="nonSxS" publicKeyToken="31bf3856ad364e35" name="Microsoft-Windows-International-Core-WinPE" language="neutral">
                <xsl:attribute name="processorArchitecture">
                  <xsl:call-template name="arch"/>
                </xsl:attribute>
                <SetupUILanguage>
                  <UILanguage><xsl:call-template name="ui-language"/></UILanguage>
                </SetupUILanguage>
                <SystemLocale><xsl:value-of select="config/l10n-language"/></SystemLocale>
                <UILanguage><xsl:call-template name="ui-language"/></UILanguage>
                <UserLocale><xsl:value-of select="config/l10n-language"/></UserLocale>
              </component>
            </settings>
            <settings pass="oobeSystem">
              <component xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:wcm="http://schemas.microsoft.com/WMIConfig/2002/State" versionScope="nonSxS" publicKeyToken="31bf3856ad364e35" name="Microsoft-Windows-Shell-Setup" language="neutral">
                <xsl:attribute name="processorArchitecture">
                  <xsl:call-template name="arch"/>
                </xsl:attribute>
                <UserAccounts>
                  <AdministratorPassword>
                    <Value><xsl:value-of select="config/admin-password"/></Value>
                    <PlainText>true</PlainText>
                  </AdministratorPassword>

                  <xsl:if test="count(config/user-login) &gt; 0">
                    <LocalAccounts>
                      <LocalAccount wcm:action="add">
                        <Password>
                          <Value><xsl:value-of select="config/user-password"/></Value>
                          <PlainText>true</PlainText>
                        </Password>
                        <Name><xsl:value-of select="config/user-login"/></Name>
                        <DisplayName><xsl:value-of select="config/user-realname"/></DisplayName>
                        <Group>Users</Group>
                      </LocalAccount>
                    </LocalAccounts>
                  </xsl:if>
                </UserAccounts>
                <xsl:if test="count(config/user-login) &gt; 0">
                  <AutoLogon>
                    <Password>
                      <Value><xsl:value-of select="config/user-password"/></Value>
                      <PlainText>true</PlainText>
                    </Password>
                    <Enabled>true</Enabled>
                    <LogonCount>5</LogonCount>
                    <Username><xsl:value-of select="config/user-login"/></Username>
                  </AutoLogon>
                </xsl:if>
                <OOBE>
                  <NetworkLocation>Work</NetworkLocation>
                  <HideEULAPage>true</HideEULAPage>
                  <ProtectYourPC>3</ProtectYourPC>
                  <SkipMachineOOBE>true</SkipMachineOOBE>
                  <SkipUserOOBE>true</SkipUserOOBE>
                </OOBE>
                <FirstLogonCommands>
                  <SynchronousCommand wcm:action="add">
                    <Order>1</Order>
                    <Description>Turn Off Network Selection pop-up</Description>
                    <CommandLine>cmd /c reg add "HKLM\SYSTEM\CurrentControlSet\Control\Network\NewNetworkWindowOff"</CommandLine>
                  </SynchronousCommand>
                  <SynchronousCommand wcm:action="add">
                    <Order>2</Order>
                    <Description>Shutting down Windows</Description>
                    <CommandLine>cmd /C shutdown /s /t 0</CommandLine>
                  </SynchronousCommand>
                </FirstLogonCommands>
              </component>
            </settings>
            <settings pass="specialize">
              <component xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:wcm="http://schemas.microsoft.com/WMIConfig/2002/State" versionScope="nonSxS" publicKeyToken="31bf3856ad364e35" name="Microsoft-Windows-Shell-Setup" language="neutral">
                <xsl:attribute name="processorArchitecture">
                  <xsl:call-template name="arch"/>
                </xsl:attribute>

                <ProductKey><xsl:value-of select="config/reg-product-key"/></ProductKey>
              </component>
            </settings>
          </unattend>
        </xsl:template>
      </xsl:stylesheet>
    </template>
  </install-script>
</libosinfo>