<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="power-batterylife" xml:lang="de">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspend"/>
    <link type="seealso" xref="shell-exit#suspend"/>
    <link type="seealso" xref="shell-exit#shutdown"/>
    <link type="seealso" xref="display-brightness"/>
    <link type="seealso" xref="power-whydim"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Tipps zur Reduzierung des Energieverbrauchs Ihres Rechners.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2019.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2019.</mal:years>
    </mal:credit>
  </info>

  <title>Energie sparen und die Lebensdauer von Akkus verlängern</title>

  <p>Rechner können eine Menge an Energie verbrauchen. Mit einfachen Strategien zum Energiesparen können Sie Ihre Stromrechnung reduzieren und etwas für die Umwelt tun.</p>

<section id="general">
  <title>Allgemeine Tipps</title>

<list>
  <item>
    <p><link xref="shell-exit#suspend">Versetzen Sie Ihren Rechner in Bereitschaft</link>, wenn Sie ihn nicht verwenden. Dies reduziert den Energieverbrauch deutlich und der Rechner kann trotzdem schnell wieder in Betrieb genommen werden.</p>
  </item>
  <item>
    <p><link xref="shell-exit#shutdown">Schalten Sie den Rechner aus</link>, wenn Sie ihn für längere Zeit nicht verwenden. Einige Leute meinen, dass der Rechner durch häufiges Ausschalten schneller kaputtgeht, aber das ist nicht der Fall.</p>
  </item>
  <item>
    <p>Verwenden Sie die Einstellung <gui>Leistung</gui> in den <app>Einstellungen</app>, um Ihre Energieeinstellungen anzupassen. Es stehen einige Möglichkeiten zur Verfügung, mit denen Sie Energie sparen können: Sie können den Bildschirm nach einer bestimmten Zeit <link xref="display-blank">automatisch abdunkeln</link> lassen, die <link xref="display-brightness">Bildschirmhelligkeit</link> reduzieren und den Rechner <link xref="power-autosuspend">automatisch in Bereitschaft versetzen</link>, wenn Sie ihn für eine bestimmte Zeit nicht verwendet haben.</p>
  </item>
  <item>
    <p>Schalten Sie jegliche externe Geräte ab (wie Drucker und Scanner), wenn Sie sie nicht benötigen.</p>
  </item>
</list>

</section>

<section id="laptop">
  <title>Laptops, Netbooks und andere Geräte mit Akkus</title>

 <list>
   <item>
     <p>Verringern Sie die <link xref="display-brightness">Bildschirmhelligkeit</link>. Die Stromversorgung des Bildschirms macht einen beachtlichen Teil des Stromverbrauchs eines Laptops aus.</p>
     <p>Die meisten Laptops verfügen über Tasten auf der Tastatur (oder Tastenkombinationen), über die Sie die Helligkeit verringern können.</p>
   </item>
   <item>
     <p>Wenn Sie für einige Zeit keine Internetverbindung benötigen, schalten Sie die Funknetzwerk- und Bluetooth-Karten ab. Diese Geräte senden Funkwellen, was viel Energie benötigt.</p>
     <p>Einige Rechner besitzen einen physischen Schalter, der zum Ausschalten verwendet werden kann, während andere dafür eine Tastenkombination bereithalten. Sie können die Karte wieder einschalten, wenn Sie diese benötigen.</p>
   </item>
 </list>

</section>

<section id="advanced">
  <title>Fortgeschrittene Tipps</title>

 <list>
   <item>
     <p>Reduzieren Sie die Anzahl der Aufgaben, die im Hintergrund ausgeführt werden. Rechner verbrauchen mehr Energie, wenn sie mehr Arbeit zu verrichten haben.</p>
     <p>Die meisten ausgeführten Anwendungen tun wenig, wenn Sie diese nicht aktiv verwenden. Anwendungen, die jedoch regelmäßig Daten aus dem Internet holen, Musik oder Filme abspielen können den Stromverbrauch beeinflussen.</p>
   </item>
 </list>

</section>

</page>
