<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-slow" xml:lang="ta">

  <info>
    <link type="guide" xref="net-problem"/>

    <revision pkgversion="3.4.0" date="2012-02-21" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>ஃபில் புல்</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>மற்றவை ஏதேனும் பதிவிறக்கிக் கொண்டிருக்கப்படலாம், அல்லது உங்கள் இணைப்பு சரியாக இல்லாதிருக்கலாம் அல்லது அந்த நேரத்தில் அது பணிமிகுதியாக இருக்கலாம்.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>இணையம் வேகம் குறைவு போலுள்ளது</title>

  <p>நீங்கள் இணையத்தைப் பயன்படுத்தும் போது, அது வேகம் குறைவாக இருப்பதாகத் தோன்றினால், அதற்கு பல காரணங்கள் இருக்கலாம்.</p>

  <p>வலை உலாவியை மூடிவிட்டு திறக்கவும், இணைய இணைப்பை துண்டித்து விட்டு மீண்டும் இணைக்கவும். (இப்படிச் செய்வதால், இணைய வேகத்தைக் குறைக்கும் பல விஷயங்கள் மீட்டமைக்கப்படும்).</p>

  <list>
    <item>
      <p><em style="strong">பணிமிகுதியான நேரம்</em></p>
      <p>Internet service providers commonly setup internet connections so that
      they are shared between several households. Even though you connect
      separately, through your own phone line or cable connection, the
      connection to the rest of the internet at the telephone exchange might
      actually be shared. If this is the case and lots of your neighbors are
      using the internet at the same time as you, you might notice a slow-down.
      You’re most likely to experience this at times when your neighbors are
      probably on the internet (in the evenings, for example).</p>
    </item>
    <item>
      <p><em style="strong">ஒரே சமயத்தில் பல பதிவிறக்கங்கள் செய்வது</em></p>
      <p>நீங்கள் அல்லது உங்கள் இணைய இணைப்பைப் பயன்படுத்தும் பிறர் ஒரே சமயத்தில் பல கோப்புகளை பதிவிறக்கினால், அல்லது வீடியோ பார்த்தால் இணைய இணைப்பு வேகம் குறையலாம்.</p>
    </item>
    <item>
      <p><em style="strong">நம்பகமற்ற இணைப்பு</em></p>
      <p>சில இணைய இணைப்புகள் நம்ப முடியாதவ, குறிப்பாக தற்காலிக இணைப்புகள் அல்லது அதிக பயன்பாடுள்ள பகுதிகளில் உள்ளவை. நீங்கள் பலர் வந்து போகும் காஃபி ஷாப்பிலோ அல்லது மாநாட்டிலோ இருந்தால் உங்கள் இணைப்பு அதிக பணிமிகுதியாக அல்லது நம்ப முடியாததாக இருக்கலாம்.</p>
    </item>
    <item>
      <p><em style="strong">வயர்லெஸ் இணைப்பு சிக்னல் குறைவு</em></p>
      <p>If you are connected to the internet by wireless (Wi-Fi), check the
      network icon on the top bar to see if you have good wireless signal. If
      not, the internet may be slow because you don’t have a very strong
      signal.</p>
    </item>
    <item>
      <p><em style="strong">வேகம் குறைவான மொபைல் இணைய இணைப்பைப் பயன்படுத்துதல்</em></p>
      <p>If you have a mobile internet connection and notice that it is slow,
      you may have moved into an area where signal reception is poor. When this
      happens, the internet connection will automatically switch from a fast
      “mobile broadband” connection like 3G to a more reliable, but slower,
      connection like GPRS.</p>
    </item>
    <item>
      <p><em style="strong">வலை உலாவியில் சிக்கல் இருக்கலாம்</em></p>
      <p>Sometimes web browsers encounter a problem that makes them run slow.
      This could be for any number of reasons — you could have visited a
      website that the browser struggled to load, or you might have had the
      browser open for a long time, for example. Try closing all of the
      browser’s windows and then opening the browser again to see if this makes
      a difference.</p>
    </item>
  </list>

</page>
