<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-browse" xml:lang="id">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>
    <link type="seealso" xref="files-copy"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-16" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Kelola dan organisasikan berkas dengan pengelola berkas.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

<title>Meramban berkas dan folder</title>

<p>Gunakan <app>File</app> Manajer file untuk mencari dan mengatur file di komputer Anda. Anda juga dapat menggunakannya untuk mengelola file pada perangkat penyimpanan (seperti hard disk eksternal), pada <link xref="nautilus-connect"> file server </link>, dan pada jaringan berbagi.</p>

<p>Untuk memulai pengelola berkas, buka <app>Berkas</app> pada ringkasan <gui xref="shell-introduction#activities">Aktivitas</gui>. Anda juga dapat mencari berkas dan folder-folder melalui ringkasan dengan cara yang serupa dengan <link xref="shell-apps-open">cari aplikasi</link>.</p>

<section id="files-view-folder-contents">
  <title>Mengeksplorasi isi folder</title>

<p>Dalam file manager, klik ganda folder untuk melihat isinya, dan klik ganda atau <link xref="mouse-middleclick">klik tombol tengah</link> pada berkas apapun untuk membukanya dengan aplikasi yang baku untuk berkas tersebut. Anda juga dapat klik kanan folder untuk membukanya di tab baru atau jendela baru.</p>

<p>Apabila melihat ke berkas-berkas di dalam folder, Anda dapat dengan cepat <link xref="files-preview">meninjau tiap berkas</link> dengan cara menekan tombol spasi untk memastikan berkas yang tepat sebelum membukanya, menyalinnya atau menghapusnya.</p>

<p><em>baris alamat</em> di atas daftar berkas dan folder menampilkan kepada Anda di mana folder yang sedang Anda lihat, termasuk induk folder dari folder yang dimaksud. Klik induk folder di baris alamat untuk pergi ke folder tersebut. Klik kanan pada folder di baris alamat untuk membukanya pada tab baru atau jendela, atau mengakses propertinya.</p>

<p>Jika Anda ingin secara cepat <link xref="files-search">mencari berkas</link>, di dalam atau di bawah folder yang sedang Anda lihat, mulai ketik namanya. <em>baris pencarian</em> akan tampil di atas jende dan hanya berkas yang cocok dengan pencarian Anda yang akan di tampilkan. Tekan <key>Esc</key> untuk membatalkan pencarian.</p>

<p>You can quickly access common places from the <em>sidebar</em>. If you do
not see the sidebar, press the menu button in the top-right corner of the window and then select
<gui>Sidebar</gui>. You can add bookmarks to folders that you use often and
they will appear in the sidebar. Drag a folder to the sidebar, and drop it over
<gui>New bookmark</gui>, which appears dynamically, or click the current folder
in the path bar and then select <gui style="menuitem">Add to Bookmarks</gui>.</p>

</section>

</page>
