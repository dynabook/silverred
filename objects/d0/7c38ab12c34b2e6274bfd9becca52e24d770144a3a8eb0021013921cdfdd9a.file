<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-workspaces-switch" xml:lang="fr">

  <info>
    <link type="guide" xref="shell-windows#working-with-workspaces"/>
    <link type="seealso" xref="shell-workspaces"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.35.91" date="2020-02-27" status="candidate"/>

    <credit type="author">
      <name>Le projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Utiliser le sélecteur d'espace de travail.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

 <title>Changement d'espace de travail</title>

 <steps>  
 <title>Avec la souris :</title>
 <item>
  <p if:test="!platform:gnome-classic">Open the
  <gui xref="shell-introduction#activities">Activities</gui> overview.</p>
  <p if:test="platform:gnome-classic">At the bottom right of the screen, click
  on one of the four workspaces to activate the workspace.</p>
 </item>
 <item if:test="!platform:gnome-classic">
  <p>Click on a workspace in the <link xref="shell-workspaces">workspace
  selector</link> on the right side of the screen to view the open windows on
  that workspace.</p>
 </item>
 <item if:test="!platform:gnome-classic">
  <p>Cliquez sur n'importe quelle miniature de fenêtre pour activer l'espace de travail.</p>
 </item>
 </steps>
 
 <list>
 <title>Avec le clavier :</title>  
  <item>
    <p if:test="!platform:gnome-classic">Press
    <keyseq><key xref="keyboard-key-super">Super</key><key>Page Up</key></keyseq>
    or <keyseq><key>Ctrl</key><key>Alt</key><key>Up</key></keyseq> to move to
    the workspace shown above the current workspace in the workspace selector.
    </p>
    <p if:test="platform:gnome-classic">Press <keyseq><key>Ctrl</key>
    <key>Alt</key><key>←</key></keyseq> to move to the workspace shown left
    of the current workspace on the <em>workspace selector</em>.</p>
  </item>
  <item>
    <p if:test="!platform:gnome-classic">Press <keyseq><key>Super</key><key>Page Down</key></keyseq> or
    <keyseq><key>Ctrl</key><key>Alt</key><key>Down</key></keyseq> to move to the
    workspace shown below the current workspace in the workspace selector.</p>
    <p if:test="platform:gnome-classic">Press <keyseq><key>Ctrl</key>
    <key>Alt</key><key>→</key></keyseq> to move to the workspace shown right of
    the current workspace on the <em>workspace selector</em>.</p>
  </item>
 </list>

</page>
