<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-vpn-connect" xml:lang="pt">

  <info>
    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-wired"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.10" date="2013-12-05" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>

    <credit type="author">
      <name>Projeto de documentação de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hilh</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Aprenda como configurar uma conexão VPN a uma rede local mediante Internet.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Ligar a uma VPN</title>

<p>A VPN (or <em>Virtual Private Network</em>) is a way of connecting to a
 local network over the internet. For example, say you want to connect to the
 local network at your workplace while you’re on a business trip. You would
 find an internet connection somewhere (like at a hotel) and then connect to
 your workplace’s VPN. It would be as if you were directly connected to the
 network at work, but the actual network connection would be through the
 hotel’s internet connection. VPN connections are usually <em>encrypted</em>
 to prevent people from accessing the local network you’re connecting to
 without logging in.</p>

<p>There are a number of different types of VPN. You may have to install some
 extra software depending on what type of VPN you’re connecting to. Find out
 the connection details from whoever is in charge of the VPN and see which
 <em>VPN client</em> you need to use. Then, go to the software installer
 application and search for the <app>NetworkManager</app> package which works
 with your VPN (if there is one) and install it.</p>

<note>
 <p>If there isn’t a NetworkManager package for your type of VPN, you will
 probably have to download and install some client software from the company
 that provides the VPN software. You’ll probably have to follow some different
 instructions to get that working.</p>
</note>

<p>Para configurar uma conexão VPN:</p>

  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Atividades</gui> e comece a escrever <gui>Rede</gui>.</p>
    </item>
    <item>
      <p>Carregue em <gui>Rede</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Na parte inferior da lista, à esquerda, carregue o botão <gui>+</gui> para acrescentar uma conexão nova.</p>
    </item>
    <item>
      <p>Escolha <gui>VPN</gui> na lista de interfaces.</p>
    </item>
    <item>
      <p>Selecione que tipo de conexão VPN tem.</p>
    </item>
    <item>
      <p>Recheie os detalhes da conexão VPN e carregue <gui>Acrescentar</gui> quando tenha terminado.</p>
    </item>
    <item>
      <p>When you have finished setting up the VPN, open the
      <gui xref="shell-introduction#systemmenu">system menu</gui> from the right side of
      the top bar, click <gui>VPN off</gui> and select <gui>Connect</gui>. You
      may need to enter a password for the connection before it is established.
      Once the connection is made, you will see a lock shaped icon in the top
      bar.</p>
    </item>
    <item>
      <p>Se todo vai bem, ligar-se-á corretamente à VPN. Caso contrário, pode que precise voltar a verificar a configuração VPN que introduziu. Pode fazê-lo clicando desde o painel de <gui>Rede</gui> que usou para criar a conexão, selecionando a conexão VPN na lista e clicando o botão <media its:translate="no" type="image" src="figures/emblem-system.png"><span its:translate="yes">configuração</span></media> para revisar a configuração.</p>
    </item>
    <item>
      <p>Para desligar-se da VPN, carregue no menu do sistema na barra superior e carregue <gui>Apagar</gui> sob o nome da sua conexão VPN.</p>
    </item>
  </steps>

</page>
