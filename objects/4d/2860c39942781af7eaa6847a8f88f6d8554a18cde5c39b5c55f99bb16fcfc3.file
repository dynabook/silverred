<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-what" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Back up anything that you cannot bear to lose if something goes
    wrong.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>备份对象</title>

  <p>您首先应备份您<link xref="backup-thinkabout">最重要的文件</link>以及难以重新创建的文件。最重要到最不重要文件示例：</p>

<terms>
 <item>
  <title>您的个人文件</title>
   <p>这包括文档、电子表格、电子邮件、日程安排、财务数据、家庭照片，和其他您认为不可替代的个人文件。</p>
 </item>

 <item>
  <title>您的个人设置文件</title>
   <p>其中包括您可能对您桌面上的颜色、背景、屏幕分辨率和鼠标设置做出的更改。另外还包括应用程序首选项，如对 <app>LibreOffice</app>、您的音乐播放器以及您的电子邮件程序的设置。这些是可替代的，但可能需要一段时间才能重新创建。</p>
 </item>

 <item>
  <title>系统设置</title>
   <p>大多数人从不更改安装期间创建的设置。如果您确实定制了您的系统或者将计算机作为服务器使用，您可能希望备份这些设置。</p>
 </item>

 <item>
  <title>安装的软件</title>
   <p>软件一般可以通过重新安装来恢复。</p>
 </item>
</terms>

  <p>通常，您会想要备份不可替代的文件以及如果没有备份需要投入大量时间才能恢复的文件。另一方面，如果内容非常简单替代，您可能不希望备份挤占磁盘空间。</p>

</page>
