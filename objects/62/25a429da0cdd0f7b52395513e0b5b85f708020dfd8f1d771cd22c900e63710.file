<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="ui" id="shell-windows" xml:lang="de">

  <info>
    <link type="guide" xref="shell-overview#apps"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Verschieben und Organisieren Ihrer Fenster.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2019.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2019.</mal:years>
    </mal:credit>
  </info>

  <title>Fenster und Arbeitsflächen</title>

  <p>Wie andere Arbeitsumgebungen auch verwendet GNOME Fenster zur Anzeige von laufenden Anwendungen. Mit der <gui xref="shell-introduction#activities">Aktivitäten-Übersicht</gui> und dem <em>Dash</em> können Sie neue Anwendungen starten und steuern, welche Anwendung aktiv ist.</p>

  <p>Zusätzlich zu den Fenstern können Sie Ihre Anwendungen innerhalb von Arbeitsflächen anordnen. Lesen Sie hierzu die nachfolgenden Abschnitte zu Fenstern und Arbeitsflächen, um mehr über diese Funktionsmerkmale zu erfahren.</p>
<!-- 
  <p>In the <gui>Activities</gui> overview, the <gui>dash</gui> displays your 
  favorite applications as well as your running applications. 
  The <gui>dash</gui> will place a slight glow behind any running applications.
  </p>
      
  <p>Clicking the application icon will launch it if it is not running.  
  If it is already running, clicking the application will open the last used 
  window of that application.</p>

  <p>Right clicking the application icon for a running application will 
  bring all windows for that application forward. A menu with the titles of your
  windows will be displayed. You can select a window from this menu. It also 
  provides options to open a new window for that application and to remove or 
  add that application to favorites depending on its current status.</p>

  <p>Windows are shown on their corresponding 
  <link xref="shell-windows-workspaces">workspaces</link>.</p>
-->
<section id="working-with-windows" style="2column">
  <info>
    <title type="link" role="trail">Fenster</title>
  </info>
 <title>Arbeiten mit Fenstern</title>
</section>

<section id="working-with-workspaces" style="2column">
  <info>
    <title type="link" role="trail">Arbeitsflächen</title>
  </info>
 <title>Arbeiten mit Arbeitsflächen</title>
</section>

</page>
