<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="keyboard-key-super" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.7.91" version="0.2" date="2013-03-16" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-27" status="review"/>

    <credit type="author">
      <name>Gnomov projekat dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Taster <key>Super</key> otvara pregled <gui>Aktivnosti</gui>. Obično se na tastaturi nalazi pored tastera <key>Alt</key>.</desc>
  </info>

  <title>Šta je to taster <key>Super</key>?</title>

  <p>Kada pritisnete taster <key>Super</key>, biva prikazan pregled <gui>Aktivnosti</gui>. Ovaj taster se obično na tastaturi nalazi na levoj strani pri dnu, pored tastera <key>Alt</key>, i na njemu se obično nalazi logotip Vindouza. Ponekad se naziva <em>Vindouz tasterom</em> ili sistemskim tasterom.</p>

  <note>
    <p>Ako imate Eplovu tastaturu, na njoj će se nalaziti taster <key>⌘</key> (Command) umesto tastera Vindouza, dok „Chromebooks“ ima lupu umesto toga.</p>
  </note>

  <p>Da izmenite koji taster će biti korišćen za prikazivanje pregleda <gui>Aktivnosti</gui>:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Devices</gui> in the sidebar.</p>
    </item>
    <item>
      <p>Click <gui>Keyboard</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>In the <gui>System</gui> category, click the row with <gui>Show the
      activities overview</gui>.</p>
    </item>
    <item>
      <p>Pritisnite željenu kombinaciju tastera.</p>
    </item>
  </steps>

</page>
