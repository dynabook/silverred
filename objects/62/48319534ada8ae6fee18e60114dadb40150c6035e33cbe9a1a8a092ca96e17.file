<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="tip" version="1.0 if/1.0 ui/1.0" id="shell-keyboard-shortcuts" xml:lang="ca">

  <info>
    <link type="guide" xref="tips"/>
    <link type="guide" xref="keyboard"/>
    <link type="guide" xref="shell-overview#apps"/>
    <link type="seealso" xref="keyboard-key-super"/>

    <revision pkgversion="3.8.0" version="0.4" date="2013-04-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.20" date="2016-08-13" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-27" status="review"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Moure's per l'escriptori amb el teclat.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

<title>Dreceres de teclat útils</title>

<p>Aquesta pàgina ofereix una visió general de les dreceres de teclat que us poden ajudar a utilitzar l'escriptori i les aplicacions de forma més eficient. Si no podeu utilitzar un ratolí o un dispositiu apuntador, consulteu <link xref="keyboard-nav"/> per obtenir més informació sobre la navegació per la interfície d'usuari amb només el teclat.</p>

<table rules="rows" frame="top bottom" ui:expanded="true">
<title>Moure's per l'escriptori</title>
  <tr xml:id="alt-f1">
    <td><p><keyseq><key>Alt</key><key>F1</key></keyseq> o la</p>
      <p>tecla <key xref="keyboard-key-super">Súper</key></p></td>
    <td><p>Canviar entre la vista general d'<gui>Activitats</gui> i l'escriptori. A la visita general, comenceu a escriure per cercar instantàniament les vostres aplicacions, contactes i documents.</p></td>
  </tr>
  <tr xml:id="alt-f2">
    <td><p><keyseq><key>Alt</key><key>F2</key></keyseq></p></td>
    <td><p>Finestra d'ordres emergents (per a executar ordres ràpidament).</p>
    <p>Utilitzeu les tecles de fletxa per accedir ràpidament a ordres executades anteriorment.</p></td>
  </tr>
  <tr xml:id="super-tab">
    <td><p><keyseq><key>Súper</key><key>Tab</key></keyseq></p></td>
    <td><p><link xref="shell-windows-switching">Canvieu ràpidament entre finestres</link>. Manteniu premuda la tecla <key>Maj</key> per a l'ordre inversa.</p></td>
  </tr>
  <tr xml:id="super-tick">
    <td><p><keyseq><key>Súper</key><key>`</key></keyseq></p></td>
    <td>
      <p>Canvieu entre finestres de la mateixa aplicació, o des de l'aplicació seleccionada fer <keyseq><key>Súper</key><key>Tab</key></keyseq>.</p>
      <p>Aquesta drecera utilitza <key>`</key> als teclats USA, quan la tecla <key>`</key> està sobre el <key>Tab</key>. A tots els altres teclats, la drecera és <key>Súper</key> més la tecla que està sobre <key>Tab</key>.</p>
    </td>
  </tr>
  <tr xml:id="alt-escape">
    <td><p><keyseq><key>Alt</key><key>Esc</key></keyseq></p></td>
    <td>
      <p>Canvieu ràpidament entre finestres a l'espai de treball actual. Manteniu premuda la tecla <key>Maj</key> per a l'ordre inversa.</p>
    </td>
  </tr>
  <tr xml:id="ctrl-alt-tab">
    <!-- To be updated to <key>Tab</key> in the future. -->
    <td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq></p></td>
    <td>
      <p>Doneu el focus del teclat a la barra superior. A la vista general d'<gui>Activitats</gui>, canvieu el focus del teclat entre la barra superior, el tauler, la vista general de les finestres, la llista d'aplicacions, el camp de cerca i la safata de missatges. Utilitzeu les tecles de fletxa per navegar.</p>
    </td>
  </tr>
  <tr xml:id="super-a">
    <td><p><keyseq><key>Súper</key><key>A</key></keyseq></p></td>
    <td><p>Mostrar la llista d'aplicacions.</p></td>
  </tr>
  <tr xml:id="super-updown">
    <td>
      <p if:test="!platform:gnome-classic"><keyseq><key>Súper</key><key>Re Pàg</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>Ctrl</key><key>Alt</key><key>→</key></keyseq></p>
      <p>i</p>
      <p if:test="!platform:gnome-classic"><keyseq><key>Súper</key><key>Av Pàg</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>Ctrl</key><key>Alt</key><key>←</key></keyseq></p>
    </td>
    <td><p><link xref="shell-workspaces-switch">Canvieu entre espais de treball</link>.</p></td>
  </tr>
  <tr xml:id="shift-super-updown">
    <td>
      <p if:test="!platform:gnome-classic"><keyseq><key>Maj</key><key>Súper</key><key>Re Pàg</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>Maj</key><key>Ctrl</key><key>Alt</key><key>→</key></keyseq></p>
      <p>i</p>
      <p if:test="!platform:gnome-classic"><keyseq><key>Maj</key><key>Súper</key><key>Av Pàg</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>Maj</key><key>Ctrl</key><key>Alt</key><key>←</key></keyseq></p>
    </td>
    <td><p><link xref="shell-workspaces-movewindow">Moure la finestra actual a un altre espai de treball</link>.</p></td>
  </tr>
  <tr xml:id="shift-super-left">
    <td><p><keyseq><key>Maj</key><key>Súper</key><key>←</key></keyseq></p></td>
    <td><p>Moure la finestra actual un monitor a l'esquerra.</p></td>
  </tr>
  <tr xml:id="shift-super-right">
    <td><p><keyseq><key>Maj</key><key>Súper</key><key>→</key></keyseq></p></td>
    <td><p>Mou la finestra un monitor a la dreta.</p></td>
  </tr>
  <tr xml:id="ctrl-alt-Del">
    <td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Supr</key></keyseq></p></td>
    <td><p><link xref="shell-exit#logout">Sortir</link>.</p></td>
  </tr>
  <tr xml:id="super-l">
    <td><p><keyseq><key>Súper</key><key>L</key></keyseq></p></td>
    <td><p><link xref="shell-exit#lock-screen">Bloquejar la pantalla.</link></p></td>
  </tr>
  <tr xml:id="super-v">
    <td><p><keyseq><key>Súper</key><key>V</key></keyseq></p></td>
    <td><p>Mostra <link xref="shell-notifications#notificationlist">la llista de notificacions</link>. Premeu <keyseq><key>Súper</key><key>V</key> </keyseq> de nou o <key>Esc</key> per tancar.</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Dreceres d'edició comunes</title>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>A</key></keyseq></p></td>
    <td><p>Seleccioneu tot el text o els elements d'una llista.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>X</key></keyseq></p></td>
    <td><p>Retalla (elimina) el text o els elements seleccionats i col·loqueu-los al porta-retalls.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>C</key></keyseq></p></td>
    <td><p>Copieu el text o els elements seleccionats al porta-retalls.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>V</key></keyseq></p></td>
    <td><p>Enganxa els continguts del porta-retalls.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>Z</key></keyseq></p></td>
    <td><p>Desfer la darrera acció.</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Capturar des de la pantalla</title>
  <tr>
    <td><p><key>Prnt Scrn</key></p></td>
    <td><p><link xref="screen-shot-record#screenshot">Fer una captura de pantalla.</link></p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>Prnt Scrn</key></keyseq></p></td>
    <td><p><link xref="screen-shot-record#screenshot">Fer una captura de pantalla d'una finestra.</link></p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Maj</key><key>Prnt Scrn</key></keyseq></p></td>
    <td><p><link xref="screen-shot-record#screenshot">Feu una captura de pantalla d'una àrea de la pantalla.</link> El punter canvia a una creu. Feu clic i arrossegueu per seleccionar l'àrea.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Maj</key><key>R</key></keyseq></p></td>
    <td><p><link xref="screen-shot-record#screencast">Iniciar i aturar una gravació de vídeo.</link></p></td>
  </tr>
</table>

</page>
