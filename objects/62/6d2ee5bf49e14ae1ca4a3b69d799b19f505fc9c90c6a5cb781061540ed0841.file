<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-left-handed" xml:lang="sv">

  <info>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.28" date="2018-07-22" status="review"/>
    <revision pkgversion="3.33" date="2019-07-21" status="candidate"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Växla Wacom-plattan till <gui>Vänsterhänt orientering</gui>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Använd plattan med vänster hand</title>

  <p>Vissa plattor har hårdvaruknappar på en sida. Plattan kan roteras 180 grader för att positionera dessa knappar för vänsterhänta. För att växla till orientering för vänsterhänta:</p>

<steps>
  <item>
    <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Wacom-platta</gui>.</p>
  </item>
  <item>
    <p>Klicka på <gui>Wacom-ritplatta</gui> för att öppna panelen.</p>
  </item>  <item>
    <p>Klicka på knappen <gui>Ritplatta</gui> i rubrikraden.</p>
    <note style="tip"><p>Om ingen platta detekteras kommer du att bli ombedd att <gui>Anslut eller aktivera din Wacom-ritplatta</gui>. Klicka på länken <gui>Bluetooth-inställningar</gui> för att ansluta en trådlös platta.</p></note>
  </item>
  <item><p>Slå på <gui>Vänsterhänt orientering</gui>.</p></item>
</steps>

</page>
