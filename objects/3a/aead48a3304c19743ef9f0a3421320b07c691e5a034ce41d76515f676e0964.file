<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-stylus" xml:lang="id">

  <info>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.28" date="2018-07-22" status="review"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Mendefinisikan fungsi tombol dan rasa tekanan dari stylus Wacom.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

  <title>Tata konfigurasi tylus</title>

<steps>
  <item>
    <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
    start typing <gui>Settings</gui>.</p>
  </item>
  <item>
    <p>Click on <gui>Settings</gui>.</p>
  </item>
  <item>
    <p>Click <gui>Devices</gui> in the sidebar.</p>
  </item>
  <item>
    <p>Click <gui>Wacom Tablet</gui> in the sidebar to open the panel.</p>
  </item>
  <item>
    <p>Click the <gui>Stylus</gui> button in the header bar.</p>
    <note style="tip"><p>If no stylus is detected, you’ll be asked to
    <gui>Please move your stylus to the proximity of the tablet to configure
    it</gui>.</p></note>
  </item>
  <item><p>The panel contains details and settings specific to your stylus,
   with the device name (the stylus class) and diagram to the left. These
   settings can be adjusted:</p>
    <list>
      <item><p><gui>Eraser Pressure Feel:</gui> use the slider to adjust the
       “feel” (how physical pressure is translated to digital values) between
       <gui>Soft</gui> and <gui>Firm</gui>.</p></item>
      <item><p>Konfigurasi <gui>Tombol/Roda Penggulung</gui> (perubahan ini untuk mencerminkan stylus). Klik menu di sebelah setiap label untuk memilih satu dari fungsi-fungsi ini: Tanpa Aksi, Klik Tombol Kiri Tetikus, Klik Tombol Tengah Tetikus, Klik Tombol Kanan Tetikus, Gulung Naik, Gulung Turun, Gulung Kiri, Gulung Kanan, Mundur, atau Maju.</p></item>
      <item><p><gui>Tip Pressure Feel:</gui> use the slider to adjust the
       “feel” between <gui>Soft</gui> and <gui>Firm</gui>.</p></item>
    </list>
  </item>
</steps>

<note style="info">
  <p>If you have more than one stylus, use the pager next to
  the stylus device name to choose which stylus to configure.</p>
</note>

</page>
