<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-preview" xml:lang="de">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-preview"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Legen Sie fest, wann Vorschaubilder für Dateien angezeigt werden sollen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2019.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2019.</mal:years>
    </mal:credit>
  </info>

<title>Einstellungen für die Vorschauen in der Dateiverwaltung</title>

<p>Die Dateiverwaltung erstellt Vorschaubilder für Bild-, Video- und Textdateien. Vorschaubilder können für große Dateien oder über Netzwerke langsam sein, deshalb können Sie festlegen, wann Vorschauen erstellt werden sollen. Klicken Sie dazu auf den Menüknopf rechts oben im Fenster, wählen Sie <gui>Einstellungen</gui> und dann den Reiter <gui>Suche und Vorschau</gui>.</p>

<terms>
  <item>
    <title><gui>Dateien</gui></title>
    <p>Standardmäßig werden für alle Vorschauen <gui>nur Dateien auf diesem Rechner</gui> oder damit verbundenen externen Laufwerken verwendet. Sie können dies auf <gui>Alle Dateien</gui> oder <gui>Nie</gui> festlegen. Die Dateiverwaltung kann auch <link xref="nautilus-connect">Dateien auf anderen Rechnern durchsuchen</link>, beispielsweise in einem lokalen Netzwerk oder im Internet. Wenn Sie oft Dateien in einem lokalen Netzwerk durchsuchen und das Netzwerk eine hohe Bandbreite aufweist, möchten Sie vielleicht einige oder alle Vorschaueinstellungen auf <gui>Alle Dateien</gui> setzen.</p>
    <p>Zusätzlich können Sie <gui>Nur für Dateien kleiner als:</gui> aktivieren, um die Größe der Dateien zu begrenzen, für die Vorschauen angezeigt werden.</p>
  </item>
  <item>
    <title><gui>Dateizähler</gui></title>
    <p>Wenn Sie die Dateigröße in einer Spalte der <link xref="nautilus-list">Listenansicht</link> oder in <link xref="nautilus-display#icon-captions">Symbolbeschriftungen</link> anzeigen, werden Ordner mit der Anzahl der Dateien und Ordner dargestellt, die sie enthalten. Das Zählen dieser Objekte kann langsam sein, besonders bei sehr großen Ordnern oder über ein Netzwerk.</p>
    <p>Sie können diese Funktion ein- oder ausschalten oder nur für Dateien auf Ihrem Rechner und auf lokalen, externen Laufwerken einschalten.</p>
  </item>
</terms>
</page>
