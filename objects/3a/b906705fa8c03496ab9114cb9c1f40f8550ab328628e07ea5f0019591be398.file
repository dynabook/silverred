<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-use-system-search" xml:lang="sk">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>
    <credit type="author">
      <name>Hannie Dumoleyn</name>
    </credit>
    <link type="guide" xref="getting-started" group="tasks"/>
    <title role="trail" type="link">Používanie systémového hľadania</title>
    <link type="seealso" xref="shell-apps-open"/>
    <title role="seealso" type="link">Návod na používanie systémového hľadania</title>
    <link type="next" xref="gs-get-online"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Richard Stanislavský</mal:name>
      <mal:email>kenny.vv@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>Používanie systémového hľadania</title>

    <media its:translate="no" type="image" mime="image/svg" src="gs-search1.svg" width="100%"/>
    
    <steps>
    <item><p>Open the <gui>Activities</gui> overview by clicking <gui>Activities</gui> 
    at the top left of the screen, or by pressing the
    <key href="help:gnome-help/keyboard-key-super">Super</key> key. 
    Start typing to search.</p>
    <p>Počas písania sa zobrazujú výsledky zodpovedajúce tomu čo sme napísali. Prvý výsledok je vždy zvýraznený a zobrazuje sa hore.</p>
    <p>Stlačením klávesu <key>Enter</key> sa prepneme na prvý zvýraznený výsledok.</p></item>
    </steps>
    
    <media its:translate="no" type="image" mime="image/svg" src="gs-search2.svg" width="100%"/>
    <steps style="continues">
      <item><p>Medzi výsledkami vyhľadávania sa môžu objaviť nasledujúce položky: </p>
      <list>
        <item><p>zodpovedajúce aplikácie, zobrazené navrchu výsledkov vyhľadávania,</p></item>
        <item><p>zodpovedajúce nastavenia,</p></item>
        <item><p>matching contacts,</p></item>
        <item><p>matching documents,</p></item>
        <item><p>matching calendar,</p></item>
        <item><p>matching calculator,</p></item>
        <item><p>matching software,</p></item>
        <item><p>matching files,</p></item>
        <item><p>matching terminal,</p></item>
        <item><p>matching passwords and keys.</p></item>
      </list>
      </item>
      <item><p>Na položku vo výsledkoch vyhľadávania sa prepneme kliknutím.</p>
      <p>Položku tiež môžeme vybrať pomocou šípok a stlačením klávesu <key>Enter</key>.</p></item>
    </steps>

    <section id="use-search-inside-applications">
    
      <title>Vyhľadávanie zvnútra aplikácií</title>
      
      <p>Systém vyhľadávania zoskupuje výsledky z rôznych aplikácií. Na ľavej strane výsledkov vyhľadávania môžeme vidieť ikony aplikácií, ktoré poskytli výsledky hľadania. Kliknutím na jednu z ikon spustíme vyhľadávanie zvnútra aplikácie spojenej s týmto symbolom. Pretože výsledky zobrazené v <gui>Prehľade aktivít</gui> sú len tie najzhodnejšie, hľadanie zvnútra aplikácie vám môže poskytnúť lepšie výsledky vyhľadávania.</p>

    </section>

    <section id="use-search-customize">

      <title>Prispôsobenie výsledkov vyhľadávania</title>

      <media its:translate="no" type="image" mime="image/svg" src="gs-search-settings.svg" width="100%"/>

      <note style="important">
      <p>Your computer lets you customize what you want to display in the search
       results in the <gui>Activities Overview</gui>. For example, you can
        choose whether you want to show results for websites, photos, or music.
        </p>
      </note>


      <steps>
        <title>To, čo chceme zobraziť vo výsledkoch vyhľadávania môžeme prispôsobiť takto:</title>
        <item><p>Klikneme na ponuku <gui xref="shell-introduction#yourname">Systém </gui> v pravej časti horného panelu.</p></item>
        <item><p>Click <gui>Settings</gui>.</p></item>
        <item><p>Click <gui>Search</gui> in the left panel.</p></item>
        <item><p>In the list of search locations, click the switch next to the
        search location you want to enable or disable.</p></item>
      </steps>

    </section>

</page>
