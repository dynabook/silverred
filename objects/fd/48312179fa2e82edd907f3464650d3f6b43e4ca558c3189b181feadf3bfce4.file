<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-wireless-noconnection" xml:lang="pl">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" version="0.2" date="2013-11-11" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <desc>Sprawdź hasło jeszcze raz, i inne rzeczy do spróbowania.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  </info>

<title>Nie mogę się połączyć nawet po wpisaniu poprawnego hasła</title>

<p>Jeśli ma się pewność, że wpisano poprawne <link xref="net-wireless-wepwpa">hasło sieci bezprzewodowej</link>, ale nadal nie można się z nią połączyć, można spróbować poniższe wskazówki.</p>

<list>
 <item>
  <p>Jeszcze raz sprawdź, czy hasło jest poprawne</p>
  <p>Wielkość znaków (małe i wielkie znaki) w haśle ma znaczenie, więc sprawdź, czy wszystkie znaki mają właściwą wielkość.</p>
 </item>

 <item>
  <p>Wypróbuj klucz szesnastkowy lub ASCII</p>
  <p>Wpisywane hasło może być przedstawiane na inny sposób: jako ciąg znaków w systemie szesnastkowym (cyfry 0-9 i litery a-f), zwany kluczem. Każde hasło ma odpowiedni klucz. Jeśli ma się do niego dostęp, to spróbuj go wpisać. Upewnij się, że wybrano właściwą opcję <gui>zabezpieczeń bezprzewodowych</gui> (np. <gui>40/128-bitowy klucz WEP</gui>, jeśli wpisywany jest 40-znakowy klucz dla połączenia zaszyfrowanego protokołem WEP).</p>
 </item>

 <item>
  <p>Spróbuj wyłączyć i włączyć kartę bezprzewodową</p>
  <p>Czasami karty bezprzewodowe mogą się zaciąć lub mieć inny problem uniemożliwiający połączenie. Spróbuj wyłączyć kartę, a następnie włączyć, aby ją zresetować. <link xref="net-wireless-troubleshooting"/> zawiera więcej informacji.</p>
 </item>

 <item>
  <p>Sprawdź, czy używany jest właściwy typ zabezpieczeń bezprzewodowych</p>
  <p>Kiedy użytkownik zostaje zapytany o hasło zabezpieczeń bezprzewodowych, może wybrać ich typ. Upewnij się, że wybrano typ używany przez router lub bezprzewodową stację bazową. Powinien on być domyślnie zaznaczony, ale czasami tak się nie dzieje (z różnych przyczyn). Jeśli nie wiadomo, jaki to typ, wypróbuj różne opcje, aż trafisz na właściwy.</p>
 </item>

 <item>
  <p>Sprawdź, czy karta bezprzewodowa jest właściwie obsługiwana</p>
  <p>Niektóre karty bezprzewodowe nie są zbyt dobrze obsługiwane. Są wyświetlane jako połączenie bezprzewodowe, ale nie mogą połączyć się z siecią, ponieważ ich sterowniki tego nie obsługują. Sprawdź, czy można pobrać alternatywny sterownik, albo czy potrzebna jest dodatkowa konfiguracja (np. zainstalowanie innego <em>oprogramowania sprzętowego</em>). <link xref="net-wireless-troubleshooting"/> zawiera więcej informacji.</p>
 </item>

</list>

</page>
