<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="printing-select" xml:lang="it">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Stampare solo pagine specifiche o un intervallo di pagine.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Stampare solo determinate pagine</title>

  <p>Per stampare solo determinate pagine del documento:</p>

  <steps>
    <item>
      <p>Open the print dialog by pressing
      <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>In the <gui>General</gui> tab, choose <gui>Pages</gui> from the
      <gui>Range</gui> section.</p>
    </item>
    <item><p>Digitare nel campo di testo i numeri delle pagine da stampare, separati da virgole; usare un trattino per indicare un intervallo di pagine.</p></item>
  </steps>

  <note>
    <p>For example, if you enter “1,3,5-7” in the <gui>Pages</gui> text box,
    pages 1,3,5,6 and 7 will be printed.</p>
    <media type="image" src="figures/printing-select.png"/>
  </note>

</page>
