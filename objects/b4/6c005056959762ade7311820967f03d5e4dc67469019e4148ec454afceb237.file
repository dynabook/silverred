<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-closelid" xml:lang="ru">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspendfail"/>
    <link type="seealso" xref="power-suspend"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.10" date="2013-11-08" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.26" date="2017-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author editor">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Петр Ковар (Petr Kovar)</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ноутбуки переходят в ждущий режим при закрытии крышки для экономии электроэнергии.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Почему мой компьютер отключается, когда я закрываю его крышку?</title>

  <p>When you close the lid of your laptop, your computer will
  <link xref="power-suspend"><em>suspend</em></link> in order to save power.
  This means that the computer is not actually turned off — it has just gone to
  sleep. You can resume it by opening the lid. If it does not resume, try
  clicking the mouse or pressing a key. If that still does not work, press the
  power button.</p>

  <p>Некоторые компьютеры не способны правильно переходить в ждущий режим, обычно из-за того, что их компоненты не полностью поддерживаются операционной системой (например, вследствие несовершенства драйверов для Linux). В таком случае вы можете обнаружить, что компьютер не возвращается в рабочий режим после закрытия крышки. Можно попробовать <link xref="power-suspendfail">устранить эту проблему</link> или предотвратить переход компьютера в ждущий режим при закрытии крышки.</p>

<section id="nosuspend">
  <title>Запрет перевода компьютера в ждущий режим при закрытии крышки</title>

  <note style="important">
    <p>Эти инструкции сработают только если в системе используется <app>systemd</app>. Свяжитесь с разработчиками вашего дистрибутива для получения дополнительной информации.</p>
  </note>

  <note style="important">
    <p>You need to have <app>Tweaks</app> installed on your computer to
    change this setting.</p>
    <if:if xmlns:if="http://projectmallard.org/if/1.0/" test="action:install">
      <p><link style="button" action="install:gnome-tweaks">Install
      <app>Tweaks</app></link></p>
    </if:if>
  </note>

  <p>If you do not want the computer to suspend when you close the lid, you can
  change the setting for that behavior.</p>

  <note style="warning">
    <p>Будьте осторожны при изменении этого параметра. Некоторые ноутбуки могут перегреваться, если оставить их включёнными с закрытой крышкой, особенно если они находятся в замкнутом пространстве, например, в рюкзаке.</p>
  </note>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
      overview and start typing <gui>Tweaks</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Tweaks</gui> to open the application.</p>
    </item>
    <item>
      <p>Select the <gui>General</gui> tab.</p>
    </item>
    <item>
      <p>Switch the <gui>Suspend when laptop lid is closed</gui> switch to
      off.</p>
    </item>
    <item>
      <p>Close the <gui>Tweaks</gui> window.</p>
    </item>
  </steps>

</section>

</page>
