<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="task" id="printing-to-file" xml:lang="sr">

  <info>
    <link type="guide" xref="printing" group="#last"/>

    <revision pkgversion="3.8" date="2013-03-29" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author copyright">
      <name>Екатерина Герасимова</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Сачувајте документ као ПДФ, Посткрипт или СВГ датотеку уместо да га шаљете штампачу.</desc>
  </info>

  <title>Штампајте у датотеку</title>

  <p>Можете да изаберете да штампате документ у датотеку уместо да га шаљете штампачу на штампање. Штампање у датотеку ће направити <sys>ПДФ</sys>, <sys>Посткрипт</sys> или <sys>СВГ</sys> датотеку која садржи документ. Ово може бити корисно ако желите да пребаците документ на други рачунар или да га поделите с неким.</p>

  <steps>
    <title>Да штампате у датотеку:</title>
    <item>
      <p>Отворите прозорче штампања тако што ћете притиснути <keyseq><key>Ктрл</key><key>П</key></keyseq>.</p>
    </item>
    <item>
      <p>Изаберите <gui>Штампај у датотеку</gui> под <gui>Штампач</gui> у језичку <gui style="tab">Опште</gui>.</p>
    </item>
    <item>
      <p>Да измените задати назив датотеке и место чувања датотеке, кликните на назив датотеке испод избора штампача. Кликните <gui style="button">Изабери</gui> када завршите са бирањем.</p>
    </item>
    <item>
      <p><sys>ПДФ</sys> је основна врста датотеке за документа. Ако желите да користите другачији <gui>Излазни запис</gui>, Изаберите или <sys>Постскрипт</sys> или <sys>СВГ</sys>.</p>
    </item>
    <item>
      <p>Изаберите друге поставке странице.</p>
    </item>
    <item>
      <p>Притисните <gui style="button">Штампај</gui> да сачувате датотеку.</p>
    </item>
  </steps>

</page>
