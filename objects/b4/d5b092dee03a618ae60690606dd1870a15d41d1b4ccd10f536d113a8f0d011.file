<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="color-calibrationcharacterization" xml:lang="te">

  <info>

    <link type="guide" xref="color#calibration"/>

    <desc>కాలిబరేషన్ మరియు కారెక్టరైజేషన్ అనునవి పూర్తిగా వేరువేరు.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Praveen Illa</mal:name>
      <mal:email>mail2ipn@gmail.com</mal:email>
      <mal:years>2011, 2014. </mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>కృష్ణబాబు క్రొత్తపల్లి</mal:name>
      <mal:email>kkrothap@redhat.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

  <title>What’s the difference between calibration and characterization?</title>
  <p>చాలామంది కాలిబ్రేషన్ మరియు కారెక్టరైజేషన్ మద్యని తేడా గురించి తికమకపడతారు. కాలిబ్రేషన్ అనగా వొక పరికరం యొక్క రంగు ప్రవర్తనను సవరించే కార్యక్రమం. ఇది రెండు మెకానిజమ్స్ వుపయోగించి జరుగును:</p>
  <list>
    <item><p>అది కలిగివున్న నియంత్రికలను లేదా అంతర్గత అమరికలు మార్చుట</p></item>
    <item><p>దాని రంగు చానళ్ళకు వక్రాలను వర్తింపచేయుట</p></item>
  </list>
  <p>కాలిబ్రేషన్ యొక్క ముఖ్యోద్దేశం ఒక పరికరపు రంగు స్పందనను అనుసరించి దానిని నిర్వచిత స్థితినందు వుంచడం. తరచుగా దీనిని రోజువారీ అదే ప్రవర్తనను నిర్వహించుటకు వాడుతారు. కాలిబ్రేషన్ అనునది పరికరం నందు లేదా వ్యవస్థల ప్రత్యేక ఫైల్ ఫార్మాట్ల నందు నిల్వవుంచబడును అది పరికర అమరికలు లేదా ప్రి-చానల్ కాలిబ్రేషన్ వక్రాలు నమోదుచేయును.</p>
  <p>కారెక్టరైజేషన్ (లేదా ప్రొఫైలింగ్) అనగా ఒక పరికరం ఒక రంగును ఎలా తిరిగివుత్పన్నచేస్తుంది లేదా రంగుకు ఎలా స్పందిస్తుందో <em>రికార్డు</em> చేయడం. ఫలితం అనునది పరికరపు ICC ప్రొఫైల్ నందు నిల్వవుండును. అటువంటి ప్రొఫైల్ దానినందలి రంగు దానంతటదే సవరించదు. ఏదైనా ఇతర పరికర ప్రొఫైల్‌తో కలిసినప్పుడు రంగును సవరించుటకు అది CMM (కలర్ మేనేజ్‌మెంట్ మాడ్యూల్) లేదా రంగు తెలిసిన అనువర్తనంను అనుమతించును. రెండు పరికరాల లక్షణాలు తెలిసినంత మాత్రాన, ఒక పరికరం రిప్రజంటేషన్ నుండి వేరొక దానికి రంగును బదిలీ చేయుట సాధించవచ్చు.</p>
  <note>
    <p>
      Note that a characterization (profile) will only be valid for a device
      if it’s in the same state of calibration as it was when it was
      characterized.
    </p>
  </note>
  <p>ప్రొఫైళ్ళను ప్రదర్శించుటలో కొంత అదనపు అస్పష్టత వుంది ఎంచేతంటే తరచుగా కాలిబ్రేషన్ సమాచారం అనునది వెసులుబాటు కొరకు ప్రొఫైల్ నందు నిల్వవుంచబడును. మార్పు కొరకు అది <em>vcgt</em> టాగ్ లా పిలువబడు టాగ్ నందు నిల్వవుంది. అది ప్రొఫైల్ నందు నిల్వ వున్నప్పటికీ, ICC ఆధార సాధనాలు గానీ లేదా అనువర్తనాలు గాని దానిగురించి ఎరుగవు, లేదా దానితో ఏమీ చేయవు. అదేవిధంగా, ప్రదర్శన కాలిబ్రేషన్ సాధనాలు మరియు అనువర్తనాలకూ తెలియదు, లేదా ICC కారెక్టరైజేషన్ (ప్రొఫైల్) సమాచారంతో ఏమీ చేయవు.</p>

</page>
