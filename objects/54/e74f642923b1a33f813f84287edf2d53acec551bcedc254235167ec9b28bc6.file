<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="backup-thinkabout" xml:lang="vi">

  <info>
    <link type="guide" xref="files#backup"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Dự án tài liệu GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Danh sách các thư mục bạn có thể tìm thấy tài liệu, tập tin và các thiết lập bạn muốn sao lưu.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nguyễn Thái Ngọc Duy</mal:name>
      <mal:email>pclouds@gmail.com</mal:email>
      <mal:years>2011-2012.</mal:years>
    </mal:credit>
  </info>

  <title>Tìm tập tin cần sao lưu chỗ nào?</title>

  <p>Quyết định tập tin nào cần sao lưu và tìm vị trí của những tập tin đó là bước khó khăn nhất khi sao lưu. Bên dưới là danh sách nơi những tập tin và thiết lập quan trọng mà bạn có thể muốn sao lưu.</p>

<list>
 <item>
  <p>Tập tin cá nhân (tài liệu, nhạc, ảnh và phim)</p>
  <p its:locNote="translators: xdg dirs are localised by package xdg-user-dirs and need to be translated.  You can find the correct translations for your language here: http://translationproject.org/domain/xdg-user-dirs.html">These are usually stored in your home folder (<file>/home/your_name</file>).
  They could be in subfolders such as <file>Desktop</file>,
  <file>Documents</file>, <file>Pictures</file>, <file>Music</file> and
  <file>Videos</file>.</p>
  <p>If your backup medium has sufficient space (if it is an external hard
  disk, for example), consider backing up the entire Home folder. You can find
  out how much disk space your Home folder takes up by using the
  <app>Disk Usage Analyzer</app>.</p>
 </item>

 <item>
  <p>Tập tin ẩn</p>
  <p>Any file or folder name that starts with a period (.) is hidden by
  default. To view hidden files, click the
  <gui><media its:translate="no" type="image" src="figures/go-down.png"><span its:translate="yes">View options</span></media></gui>
  button in the toolbar, and then choose <gui>Show Hidden Files</gui>, or press
  <keyseq><key>Ctrl</key><key>H</key></keyseq>. You can copy these to a
  backup location like any other file.</p>
 </item>

 <item>
  <p>Thiết lập cá nhân (tuỳ thích môi trường làm việc, sắc thái, thiết lập phần mềm)</p>
  <p>Most applications store their settings in hidden folders inside your Home
 folder (see above for information on hidden files).</p>
  <p>Most of your application settings will be stored in the hidden folders
 <file>.config</file> and <file>.local</file> in your Home folder.</p>
 </item>

 <item>
  <p>Thiết lập toàn hệ thống</p>
  <p>Settings for important parts of the system are not stored in your Home
  folder. There are a number of locations that they could be stored, but most
  are stored in the <file>/etc</file> folder. In general, you will not need to
  back up these files on a home computer. If you are running a server, however,
  you should back up the files for the services that it is running.</p>
 </item>
</list>

</page>
