<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-vpn-connect" xml:lang="nl">

  <info>
    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-wired"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.10" date="2013-12-05" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>

    <credit type="author">
      <name>Gnome-documentatieproject</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Met VPN kunt u verbinden met een lokaal netwerk via internet.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

<title>Verbinden met een VPN</title>

<p>Een VPN (of <em>Virtual Private Network</em>) is een manier om te verbinden met een lokaal netwerk via internet. Stel, bijvoorbeeld, dat u verbinding wilt maken met het lokale netwerk op uw werkplek terwijl u op zakenreis bent. U zou ergens (zoals in een hotel) een internetverbinding kunnen maken en dan verbinding maken met het VPN van uw werkplek. Het zou zijn alsof u direct verbonden was met het netwerk op het werk, maar de eigenlijke netwerkverbinding zou zijn via de internetverbinding van het hotel. VPN-verbindingen zijn meestal <em>versleuteld</em> om te voorkomen dat mensen zonder in te loggen toegang krijgen tot het lokale netwerk waarmee u verbinding heeft.</p>

<p>Er zijn verschillende soorten VPN. U dient mogelijk enige extra software te installeren, afhankelijk van het type VPN waarmee u verbindt. Ga na wat de verbindingsdetails zijn bij degene die verantwoordelijk is voor het VPN en kijk welke <em>VPN-client</em> u moet gebruiken. Ga daarna naar de software-installatie-toepassing en zoek naar het <app>netwerkbeheer</app>-pakket dat werkt met uw VPN (als er een is) en installeer dat.</p>

<note>
 <p>Als er geen netwerkbeheerpakket is voor uw type VPN, dan zult u waarschijnlijk client-software moeten downloaden en installeren van het bedrijf dat de VPN-software levert. U zult mogelijk wat andere instructies moeten volgen om dat werkend te krijgen.</p>
</note>

<p>Om de VPN-verbinding in te stellen:</p>

  <steps>
    <item>
      <p>Open het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht en typ <gui>Netwerk</gui>.</p>
    </item>
    <item>
      <p>Klik op <gui>Netwerk</gui> om het paneel te openen.</p>
    </item>
    <item>
      <p>Aan de onderkant van de lijst aan de linkerkant, klik op de <key>+</key>-knop om een nieuwe verbinding toe te voegen.</p>
    </item>
    <item>
      <p>Kies <gui>VPN</gui> in de interface-lijst.</p>
    </item>
    <item>
      <p>Kies welk soort VPN-verbinding u heeft.</p>
    </item>
    <item>
      <p>Vul de VPN-verbindingsdetails in en druk daarna op <gui>Toevoegen</gui> als u klaar bent.</p>
    </item>
    <item>
      <p>When you have finished setting up the VPN, open the
      <gui xref="shell-introduction#systemmenu">system menu</gui> from the right side of
      the top bar, click <gui>VPN off</gui> and select <gui>Connect</gui>. You
      may need to enter a password for the connection before it is established.
      Once the connection is made, you will see a lock shaped icon in the top
      bar.</p>
    </item>
    <item>
      <p>Hopelijk zult u met succes verbinding maken met het VPN. Zo niet, dan moet u misschien de VPN-instellingen die u ingevoerd heeft controleren. U doet dit vanuit het paneel <gui>Netwerk</gui> dat u gebruikt heeft om de verbinding aan te maken. Kies uit de lijst VPN-verbinding en druk op de knop <media its:translate="no" type="image" src="figures/emblem-system.png"><span its:translate="yes">instellingen</span></media> om de instellingen te bekijken.</p>
    </item>
    <item>
      <p>Om de verbinding met het VPN te verbreken, klik in de bovenbalk op het systeemmenu en kies <gui>Verbinding verbreken</gui> onder de naam van uw VPN-verbinding.</p>
    </item>
  </steps>

</page>
