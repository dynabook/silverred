<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" id="shell-apps-auto-start" xml:lang="da">

  <info>
    <link type="guide" xref="shell-overview#desktop"/>

    <revision pkgversion="3.33" date="2019-07-21" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Aruna Sankaranarayanan</name>
      <email>aruna.evam@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <desc>Use <app>Tweaks</app> to start applications automatically on
    login.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years/>
    </mal:credit>
  </info>

  <title>Have applications start automatically on log in</title>

  <p>When you log in, your computer automatically starts some applications and
  runs them in the background. These are usually important programs that help
  your desktop session to run smoothly.</p>

  <p>You can use the <app>Tweaks</app> application to add other applications
  that you use frequently, such as web browsers or editors, to the list of
  programs that start automatically on login.</p>

  <note style="important">
    <p>You need to have <app>Tweaks</app> installed on your computer to
    change this setting.</p>
    <if:if test="action:install">
      <p><link style="button" action="install:gnome-tweaks">Installér <app>Tilpasninger</app></link></p>
    </if:if>
  </note>

  <steps>
    <title>To start an application automatically on login:</title>
    <item>
      <p>Åbn <gui xref="shell-introduction#activities">Aktivitetsoversigten</gui> og begynd at skrive <gui>Tilpasninger</gui>.</p>
    </item>
    <item>
      <p>Klik på <gui>Tilpasninger</gui> for at åbne programmet.</p>
    </item>
    <item>
      <p>Klik på fanebladet <gui>Opstartsprogrammer</gui>.</p>
    </item>
    <item>
      <p>Klik på <gui style="button">+</gui>-knappen for at få en liste over tilgængelige programmer.</p>
    </item>
    <item>
      <p>Click <gui style="button">Add</gui> to add an application of your
      choice to the list.</p>
    </item>
  </steps>

  <note style="tip">
    <p>You can remove an application from the list by clicking the
    <gui style="button">Remove</gui> button next to the application.</p>
  </note>

</page>
