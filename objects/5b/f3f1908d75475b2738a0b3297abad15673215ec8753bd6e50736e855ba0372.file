<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-othersconnect" xml:lang="ta">

  <info>
    <link type="guide" xref="net-problem"/>
    <link type="seealso" xref="net-othersedit"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>ஃபில் புல்</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>மைக்கேல் ஹில்</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>எக்காட்டெரினா ஜெராசிமோவா</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>நீங்கள் (கடவுச்சொல் போன்றவை) அமைவுகளை சேமிக்கலாம், இதனால் கணினியைப் பயன்படுத்தும் அனைவரும் பிணையத்துடன் இணைக்க முடியும்.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>Other users can’t connect to the internet</title>

  <p>When you set up a network connection, all other users on your computer
  will normally be able to use it. If the connection information is not shared,
  you should check the connection settings.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Network</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Network</gui> to open the panel.</p>
    </item>
    <item>
      <p>இடப்புறம் உள்ள பட்டியலில் இருந்து <gui>Wi-Fi</gui> ஐ தேர்ந்தெடுக்கவும்.</p>
    </item>
    <item>
      <p>Click the 
      <media its:translate="no" type="image" src="figures/emblem-system.png"><span its:translate="yes">settings</span></media> button to open the connection
      details.</p>
    </item>
    <item>
      <p>Select <gui>Identity</gui> from the pane on the left.</p>
    </item>
    <item>
      <p>At the bottom of the <gui>Identity</gui> panel, check the <gui>Make
      available to other users</gui> option to allow other users to use the
      network connection.</p>
    </item>
    <item>
      <p>Press <gui style="button">Apply</gui> to save the changes.</p>
    </item>
  </steps>

  <p>இப்போது கணினியின் பிற பயனர்கள் மேலும் விவரங்கள் எதையும் உள்ளிட வேண்டிய அவசியம் இல்லாமலே இந்த இணைப்பை பயன்படுத்த முடியும்.</p>

  <note>
    <p>Any user can change this setting.</p>
  </note>

</page>
