<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="gsettings-browse" xml:lang="es">

  <info>
    <link type="guide" xref="setup"/>
    <link type="seealso" xref="dconf"/>
    <link type="seealso" xref="overrides"/>
    <revision pkgversion="3.30" date="2019-02-08" status="draft"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="author copyright">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>¿Qué herramienta puedo usar para navegar por los ajustes del sistema y de las aplicaciones?</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Oliver Gutiérrez</mal:name>
      <mal:email>ogutsua@gmail.com</mal:email>
      <mal:years>2018 - 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2017 - 2019</mal:years>
    </mal:credit>
  </info>

  <title>Navegar por los valores GSettings de sus aplicaciones</title>

  <p>Hay otras dos herramientas que puede usar para navegar por las preferencias de sistema y aplicaciones almacenadas como valores de GSettings. La utilidad gráfica <app>dconf-editor</app> y la utilidad de línea de comandos <cmd>gsettings</cmd>.</p>

  <p>Tanto <app>dconf-editor</app> como <cmd>gsettings</cmd> le permiten cambiar ajustes para el usuario actual.</p>

  <note style="warning"><p>Tenga en cuenta que estas utilidades siempre funcionan usando la base de datos de GSettings del usuario actual, así que no debería ejecutar dichos comandos como root.</p>
  
  <p>Tanto <app>dconf-editor</app> como <cmd>gsettings</cmd> requieren un bus de sesión de D-Bus para hacer cualquier cambio. Esto se debe a que el demonio <sys>dconf</sys> se debe activar usando D-Bus.</p>

  <p>Puede conseguir el bus de sesión necesario ejecutando <cmd>gsettings</cmd> bajo la utilidad <sys>dbus-launch</sys> como sigue:</p>

  <screen><output>$ </output><input>dbus-launch gsettings set org.gnome.desktop.background draw-background true</input></screen>
  </note>

  <p>Podría ser mejor utilizar <app>dconf-editor</app> si no está familiarizado con las opciones disponibles en una aplicación. La aplicación muestra una lista de los ajustes en una vista de arbol y también muestra información adicional acerca de cada ajuste, incluida la descripción, el tipo y el valor predeterminado.</p>

  <p><cmd>gsettings</cmd> es más potente que <app>dconf-editor</app>. <cmd>gsettings</cmd> permite completado de comandos en Bash, y puede escribir guiones de comandos que incluyan comandos de <cmd>gsettings</cmd> para automatizar configuraciones.</p>

  <p>Para una lista completa de opciones de <cmd>gsettings</cmd>, consulte la página de manual <link its:translate="no" href="man:gsettings"><cmd>gsettings</cmd>(1)</link>.</p>

</page>
