<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-removedrive" xml:lang="pl">

  <info>
    <link type="guide" xref="files#removable"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <revision pkgversion="3.6.0" version="0.2" date="2012-10-08" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Wysuwanie lub odmontowywanie dysku USB, płyty CD i DVD lub innego urządzenia.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  </info>

  <title>Bezpieczne usuwanie zewnętrznego dysku</title>

  <p>Podczas używania zewnętrznych urządzeń do przechowywania danych, takich jak dyski USB, należy je bezpiecznie usuwać przed odłączeniem. Jeśli urządzenie zostanie po prostu odłączone, to istnieje ryzyko, że jakiś program nadal go używa. Może to spowodować utratę lub uszkodzenie części plików. Kiedy używana jest płyta, taka jak CD lub DVD, to można skorzystać z tych samych instrukcji, aby ją wysunąć z komputera.</p>

  <steps>
    <title>Aby wysunąć urządzenie wymienne:</title>
    <item>
      <p>Z <gui xref="shell-introduction#activities">ekranu podglądu</gui> otwórz program <app>Pliki</app>.</p>
    </item>
    <item>
      <p>Znajdź urządzenie na panelu bocznym. Obok jego nazwy powinna być mała ikona wysunięcia. Kliknij ją, aby bezpiecznie usunąć lub wysunąć urządzenie.</p>
      <p>Można także kliknąć nazwę urządzenia na panelu bocznym prawym przyciskiem myszy i wybrać <gui>Wysuń</gui>.</p>
    </item>
  </steps>

  <section id="remove-busy-device">
    <title>Bezpieczne usuwanie obecnie używanego urządzenia</title>

  <p>Jeśli jakieś pliki na urządzeniu są otwarte lub używane przez program, to nie będzie go można bezpiecznie usunąć. Zostanie wyświetlone okno z informacją, że <gui>Wolumin jest zajęty</gui>. Aby bezpiecznie usunąć urządzenie:</p>

  <steps>
    <item><p>Kliknij przycisk <gui>Anuluj</gui>.</p></item>
    <item><p>Zamknij wszystkie pliki na urządzeniu.</p></item>
    <item><p>Kliknij ikonę wysunięcia, aby bezpiecznie usunąć lub wysunąć urządzenie.</p></item>
    <item><p>Można także kliknąć nazwę urządzenia na panelu bocznym prawym przyciskiem myszy i wybrać <gui>Wysuń</gui>.</p></item>
  </steps>

  <note style="warning"><p>Można także kliknąć przycisk <gui>Wysuń mimo to</gui>, aby usunąć urządzenie bez zamykania plików. Może to spowodować błędy programów, w których te pliki są otwarte.</p></note>

  </section>

</page>
