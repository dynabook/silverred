<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="login-logo" xml:lang="es">

  <info>
    <link type="guide" xref="login#appearance"/>
    <link type="seealso" xref="login-banner"/>
    <!--<link type="seealso" xref="gdm-restart"/>-->
    <revision pkgversion="3.11" date="2014-01-29" status="draft"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="author copyright editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2013</years>
    </credit>

    <desc>Edite un perfil <sys its:translate="no">GDM</sys> <sys its:translate="no">dconf</sys> para mostrar una imagen en la pantalla de inicio de sesión.</desc>
   
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Oliver Gutiérrez</mal:name>
      <mal:email>ogutsua@gmail.com</mal:email>
      <mal:years>2018 - 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2017 - 2019</mal:years>
    </mal:credit>
  </info>

  <title>Mostrar logotipo en la pantalla de inicio de sesión.</title>

  <p>El logotipo en la pantalla de inicio de sesión se controla con la clave <sys>org.gnome.login-screen.logo</sys>. Como <sys>GDM</sys> usa su propio perfil de <sys>dconf</sys>, puede añadir un logo cabiando los ajustes en ese perfil.</p>
  
  <p>Cuando elija una imagen apropiada para el logo de su pantalla de inicio de sesión, considere los siguientes requisitos de imagen:</p>
  
  <list>
  <item><p>Están soportados todos los formatos más utilizados: ANI, BPM, GIF, ICNS, ICO, JPEG, JPEG 2000, PCX, PNM, PBM, PGM, PPM, GTIFF, RAS, TGA, TIFF, XBM, WBMP, XPM, y SVG.</p></item>
  <item><p>El tamaño de la imagen escala proporcionalmente a un alto de 48 píxeles. Así, si configura el logo a 1920x1080, por ejemplo, cambiará a una miniatura de la imagen original de 85x48.</p></item>
  </list>
  
  <steps>
  <title>Configure la clave org.gnome.login-screen.logo</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-gdm'])"/>
  <item><p>Cree una base de datos <sys>gdm</sys> para opciones que apliquen a todo el sistema en <file>/etc/dconf/db/gdm.d/<var>01-logo</var></file>:</p>
  <code>[org/gnome/login-screen]
  logo='<var>/usr/share/pixmaps/logo/greeter-logo.png</var>'
  </code>
  <p>Reemplace <var>/usr/share/pixmaps/logo/greeter-logo.png</var> por la ruta de la imagen que quiere usar como logotipo de la pantalla de inicio de sesión.</p></item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  </steps>

  <section id="login-logo-not-update">
  <title>¿Qué hacer si el logotipo no se actualiza?</title>

  <p>Asegúrese de que ha ejecutado el comando <cmd>dconf update</cmd> para actualizar las bases de datos del sistema.</p>

  <p>Si el logo no se actualiza, pruebe a reniciar <sys>GDM</sys>.</p>
  </section>

</page>
