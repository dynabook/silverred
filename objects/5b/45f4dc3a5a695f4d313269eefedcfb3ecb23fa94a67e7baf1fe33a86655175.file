<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="power-batterylife" xml:lang="gu">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspend"/>
    <link type="seealso" xref="shell-exit#suspend"/>
    <link type="seealso" xref="shell-exit#shutdown"/>
    <link type="seealso" xref="display-brightness"/>
    <link type="seealso" xref="power-whydim"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>

    <credit type="author">
      <name>GNOME દસ્તાવેજીકરણ પ્રોજેક્ટ</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>ફીલ બુલ</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>ઍકાટેરીના ગેરાસીમોવા</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>માઇકલ હીલ</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>તમારાં કમ્પ્યૂટરનાં પાવરનો વપરાશ ઘટાડવા ટિપ્પણીઓ.</desc>
  </info>

  <title>ઓછો પાવર વાપરો અને બેટરી જીંદગીને સુધારો</title>

  <p>કમ્પ્યૂટર ઘણો પાવર વાપરી શકે છે. કેટલાક સરળ ઊર્જા બચત વ્યૂહરચનાઓ ઉપયોગ કરીને, તમે તમારા ઊર્જા બિલ ઘટાડવા અને પર્યાવરણ મદદ કરી શકો છો.</p>

<section id="general">
  <title>સામાન્ય ટિપ્પણીઓ</title>

<list>
  <item>
    <p><link xref="shell-exit#suspend">તમારાં કમ્પ્યૂટરને સ્થગિત કરો</link> જ્યારે તમે તેને વાપરી રહ્યા ન હોય. આ પાવરને ઘટાડે છે જે તે વાપરે છે, અને તે ઝલ્દીથી ઊઠી શકે છે.</p>
  </item>
  <item>
    <p>કમ્પ્યૂટરને <link xref="shell-exit#shutdown">બંધ કરો</link> જ્યારે તમે લાંબા સમયગાળા માટે તે તમે વાપરશો નહિં તો. અમુક લોકો ચિંતા કરે છે કે નિયમિત રીતે કમ્પ્યૂટરને બંધ કરવાથી તેને ઝડપીનું કારણ બની શકે છે, પરંતુ આ સ્થિતિમાં નથી.</p>
  </item>
  <item>
    <p>Use the <gui>Power</gui> panel in <app>Settings</app> to change your
    power settings. There are a number of options that will help to save power:
    you can <link xref="display-blank">automatically blank the screen</link>
    after a certain time, reduce the <link xref="display-brightness">screen
    brightness</link>, and have the computer
    <link xref="power-autosuspend">automatically suspend</link> if you have not
    used it for a certain period of time.</p>
  </item>
  <item>
    <p>કોઇપણ બહારનાં ઉપકરણોને બંધ કરો (જેમ કે પ્રિન્ટર અને સ્કેનર) જ્યારે તમે તેઓને વાપરી રહ્યા ન હોય.</p>
  </item>
</list>

</section>

<section id="laptop">
  <title>બેટરી સાથે લેપટોપ, નેટબુક, અને બીજા ઉપકરણો</title>

 <list>
   <item>
     <p>Reduce the <link xref="display-brightness">screen
     brightness</link>. Powering the screen accounts for a significant fraction
     of a laptop power consumption.</p>
     <p>મોટાભાગનાં લેપટોપ પાસે કિબોર્ડ પર બટનો છે (અથવા કિબોર્ડ ટૂંકાણ) કે જે તમે પ્રકાશતાને ઘટાડવા વાપરી શકાય છે.</p>
   </item>
   <item>
     <p>If you do not need an Internet connection for a little while, turn off
     the wireless or Bluetooth cards. These devices work by broadcasting radio
     waves, which takes quite a bit of power.</p>
     <p>અમુક કમ્પ્યૂટરો પાસે ભૌતિક સ્વીચ હોય છે કે જે તેને બંધ કરવા વાપરી શકાય છે, જ્યાં બીજા પાસે કિબોર્ડ ટૂંકાણ હોય છે કે જે તમે તેને બદલે વાપરી શકો છો. તમે તેને ફરી ચલાવી શકો છો જ્યારે તેની જરૂરિયાત હોય.</p>
   </item>
 </list>

</section>

<section id="advanced">
  <title>વધારે ઉન્નત ટિપ્પણીઓ</title>

 <list>
   <item>
     <p>કાર્યોની સંખ્યાને ઘટાડો કે જે પાશ્ર્વભાગમાં ચાલી રહ્યુ છે. કમ્પ્યૂટર વધારે પાવરને વાપરે છે જ્યારે તેઓ પાસે કામ કરવા માટે વધારે કામ છે.</p>
     <p>Most of your running applications do very little when you are not
     actively using them. However, applications that frequently grab data from
     the internet or play music or movies can impact your power consumption.</p>
   </item>
 </list>

</section>

</page>
