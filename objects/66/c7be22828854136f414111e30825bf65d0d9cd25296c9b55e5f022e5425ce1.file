<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-repair" xml:lang="gl">
  <info>
    <link type="guide" xref="disk"/>


    <credit type="author">
      <name>Proxecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <revision pkgversion="3.25.90" date="2017-08-17" status="review"/>

    <desc>Comprobar se un sistema de ficheiros está danado e devolvelo a un estado estábel.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2020</mal:years>
    </mal:credit>
  </info>

<title>Reparar un sistema de ficheiros danado</title>

  <p>Os sistemas de ficheiros poden corromperse debido á perda de enerxía, fallos do sistema ou eliminación de dispositivos non seguras. Despois de ditos incidentes é recomendábel <em>reparalos</em> ou cando menos <em>facer unha comprobación</em> do sistema de ficheiros para evitar unha perda de datos futura.</p>
  <p>A veces requírese unha reparación para montar ou modificar un sistema de ficheiros. Incluso se a <em>comprobación</em> non informe de calquera dano no sistema de ficheiros podería marcarse internamente como «sucio» e requirir unha reparación.</p>

<steps>
  <title>Comprobar se un sistema de ficheiros está danado</title>
  <item>
    <p>Abra <app>Discos</app> desde a vista de <gui>Actividades</gui>.</p>
  </item>
  <item>
    <p>Select the disk containing the filesystem in question from the list of
       storage devices on the left. If there is more than one volume on the
       disk, select the volume which contains the filesystem.</p>
  </item>
  <item>
    <p>In the toolbar underneath the <gui>Volumes</gui> section, click the
    menu button. Then click <gui>Check Filesystem…</gui>.</p>
  </item>
  <item>
    <p>Dependendo de cantos datos se gardan no sistema de ficheiros unha comprobación pode levar unha boa miga. Confirme o inicio da acción no diálogo emerxente.</p>
   <p>A acción non modificará o sistema de ficheiros pero desmontarao se é preciso. Teña paciencia mentres se comproba o sistema de ficheiros.</p>
  </item>
  <item>
    <p>Despois de que se complete notificaráselle se o sistema de ficheiros está danado. Teña en conta que nalgúns casos incluso se o sistema de ficheiro non está danado é preciso facer unha reparación para retabelecer o marcador interno de «sucio».</p>
  </item>
</steps>

<note style="warning">
 <title>Posíbel perda de datos na reparación</title>
  <p>If the filesystem structure is damaged it can affect the files stored
     in it. In some cases these files can not be brought into a valid form
     again and will be deleted or moved to a special directory. It is normally
     the <em>lost+found</em> folder in the top level directory of the filesystem
     where these recovered file parts can be found.</p>
  <p>If the data is too valuable to be lost during this process, you are
     advised to back it up by saving an image of the volume before
     repairing.</p>
  <p>This image can be then processed with forensic analysis tools like
     <app>sleuthkit</app> to further recover missing files and data parts
     which were not restored during the repair, and also previously removed
     files.</p>
</note>

<steps>
  <title>Arranxar un sistema de ficheiros</title>
  <item>
    <p>Abra <app>Discos</app> desde a vista de <gui>Actividades</gui>.</p>
  </item>
  <item>
    <p>Select the disk containing the filesystem in question from the list
       of storage devices on the left. If there is more than one volume on
       the disk, select the volume which contains the filesystem.</p>
  </item>
  <item>
    <p>Na barra de ferramentas debaixo da sección <gui>Volumes</gui>, prema o botón de menú. Logo prema <gui>Reparar sistema de ficheiros…</gui>.</p>
  </item>
  <item>
    <p>Dependendo de cantos datos se gardan no sistema de ficheiros unha reparación pode levar unha boa miga. Confirme o inicio da acción no diálogo emerxente.</p>
   <p>A acción desmontará o sistema de ficheiros se é preciso. A acción de reparación tentará levar ao sistema de ficheiros a un estado consistente e move ficheiros que foron danados a un cartafol especial. Sexa paciente mentres o sistema de ficheiros se repara.</p>
  </item>
  <item>
    <p>Despois do completado notificado se o sistema de ficheiros foi reparado con éxito. En caso de éxito poderá usalo de novo de forma normal.</p>
    <p>Se non se puido reparar o sistema de ficheiros, faga unha copia de seguranza gardando unha imaxe do volume para poder recuperar ficheiros importantes máis tarde. Isto pode facelo montando a imaxe en modo só lectura e usando ferramentas de análise forense como <app>sleuthkit</app>.</p>
    <p>Para usar o volume de novo debe <link xref="disk-format">formatalo</link> con un novo sistema de ficheiros. Perderanse todos os datos.</p>
  </item>
</steps>

</page>
