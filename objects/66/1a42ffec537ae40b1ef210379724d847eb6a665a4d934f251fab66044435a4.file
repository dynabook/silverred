<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="process-priority-change" xml:lang="cs">
  <info>
    <revision version="0.1" date="2014-01-26" status="review"/>
    <link type="guide" xref="index#processes-tasks" group="processes-tasks"/>
    <link type="seealso" xref="cpu-check"/>
    <link type="seealso" xref="process-identify-hog"/>
    <link type="seealso" xref="process-priority-what"/>
    
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    
    <credit type="author copyright">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
      <years>2014</years>
    </credit>

    <desc>Jak se rozhodnout, jestli by měl proces dostat větší nebo menší podíl procesorového času.</desc>
  </info>

  <title>Změna priority procesu</title>

  <p>Můžete počítači říct, že určitý proces by měl mít vyšší prioritu než ostatní procesy a díky tomu by měl dostat větší podíl z dostupného výpočetního času. Může pak běžet rychleji, ale jen v určitých případech. Můžete také naopak procesu přidělit <em>nižší</em> prioritu, pokud si myslíte, že odebírá příliš mnoho procesorového výkonu.</p>
  
  <steps>
    <item><p>Přejděte na kartu <gui>Procesy</gui> a klikněte na proces, u kterého chcete mít jinou prioritu.</p></item>
    <item><p>Klikněte na proces pravým tlačítkem a použijte nabídku <gui>Změnit prioritu</gui> k přiřazení vyšší nebo nižší priority procesu.</p></item>
  </steps>
  
  <p>Běžně bývá málokdy zapotřebí měnit prioritu procesu ručně. Počítač obvykle odvádí dobrou práci při jejich správě. (Systém pro správu priority procesů se nazývá <link xref="process-priority-what">nice</link>.)</p>
  

  <section id="faster">
    <title>Zajistí vyšší priorita, že proces poběží rychleji?</title>
    
    <p>Počítač rozděluje procesorový čas mezi všechny běžící procesy. Normálně je rozdělován inteligentně tak, aby programy, které dělají více práce, dostávaly automaticky větší podíl prostředků. Po většinou dostane proces tolik procesorového času, kolik potřebuje, a proto běží tak rychle, jak je zapotřebí. Změnou jeho priority neuvidíte žádný rozdíl.</p>
    
    <p>Když na vašem počítači běží několik výpočetně náročných programů naráz, může být však jejich procesorový čas „beznadějně rozebraný“ (tj. bude využita <link xref="process-identify-hog">plná kapacita procesoru</link>). Můžete zaznamenat, že ostatní programy běží pomaleji než je obvyklé, protože není dostatek procesorového času na podělení všech.</p>
    
    <p>V takovém případě může změna priority procesu pomoct. Můžete snížit prioritu jednomu z výpočetně náročných procesů, abyste uvolnili více procesorového času pro jiné programy. Jinou možností je zvýšit prioritu procesu, který je pro vás podstatný a chcete, aby běžel rychleji.</p>
    
  </section>

</page>
