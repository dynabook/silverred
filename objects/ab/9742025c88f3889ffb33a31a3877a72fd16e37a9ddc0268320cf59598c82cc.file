<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="keyboard-key-super" xml:lang="sv">

  <info>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.7.91" version="0.2" date="2013-03-16" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-27" status="review"/>

    <credit type="author">
      <name>Dokumentationsprojekt för GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc><key>Super</key>-tangenten öppnar översiktsvyn <gui>Aktiviteter</gui>. Du kan vanligtvis hitta den intill <key>Alt</key>-tangenten på ditt tangentbord.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Vad är <key>Super</key>-knappen?</title>

  <p>När du trycker ner <key>Super</key>-knappen så visas översiktsvyn <gui>Aktiviteter</gui>. Denna tangent hittas vanligtvis i nedre vänstra hörnet på ditt tangentbord, intill <key>Alt</key>-tangenten och har vanligtvis en Windows-logotyp. Den kallas ibland <em>Windows-tangenten</em> eller systemtangenten.</p>

  <note>
    <p>Om du har ett Apple-tangentbord så har du en <key>⌘</key>-tangent (Kommandotangent) istället för Windows-tangenten medan Chromebook:ar har ett förstoringsglas istället.</p>
  </note>

  <p>För att ändra vilken tangent som används för att visa översiktsvyn <gui>Aktiviteter</gui>:</p>

  <steps>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Inställningar</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Inställningar</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Enheter</gui> i sidopanelen.</p>
    </item>
    <item>
      <p>Klicka på <gui>Tangentbord</gui> i sidopanelen för att öppna panelen.</p>
    </item>
    <item>
      <p>I kategorin <gui>System</gui>, klicka på raden med <gui>Visa aktivitetsöversiktsvyn</gui>.</p>
    </item>
    <item>
      <p>Håll ner den önskade tangentkombinationen.</p>
    </item>
  </steps>

</page>
