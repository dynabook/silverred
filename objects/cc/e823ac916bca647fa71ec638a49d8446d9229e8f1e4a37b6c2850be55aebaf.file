<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="process-identify-hog" xml:lang="de">
  <info>
    <revision version="0.2" pkgversion="3.11" date="2014-01-26" status="review"/>
    <link type="guide" xref="index#processes-tasks" group="processes-tasks"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author copyright">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
      <years>2011</years>
    </credit>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2011, 2014</years>
    </credit>

    <desc>Die Prozessliste nach <gui>% CPU</gui> sortieren, um zu sehen, welche Anwendung die Ressourcen des Rechners verwendet.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Welches Programm verlangsamt den Rechner?</title>

  <p>Ein Programm, dass mehr als seinen Anteil an CPU-Zeit nutzt, kann den ganzen Rechner verlangsamen. So finden Sie heraus, welcher Prozess dies verursachen könnte:</p>

  <steps>	
    <item>
      <p>Klicken Sie auf den Reiter <gui>Prozesse</gui>.</p>
    </item>
    <item>
      <p>Klicken Sie auf die Spaltenüberschrift <gui>% CPU</gui>, um die Prozesse nach deren CPU-Verwendung zu sortieren.</p>
      <note>
	<p>Der Pfeil in der Spaltenüberschrift zeigt Ihnen die Sortierungsreihenfolge an. Klicken Sie wiederholt, um die Richtung umzukehren. Der Pfeil sollte nach oben zeigen.</p>
      </note>
   </item>
  </steps>

  <p>Die Prozesse oben in der Liste belegen die meiste CPU-Zeit. Sobald Sie denjenigen herausgefunden haben, der mehr Ressourcen belegt als erwartet, können Sie entscheiden, ob Sie das Programm beenden wollen oder andere Programme zu schließen, um die CPU-Last zu reduzieren.</p>

  <note style="tip">
    <p>Ein Prozess, der nicht mehr reagiert oder abgestürzt ist, belegt eventuell 100% CPU-Zeit. In diesem Fall hilft vermutlich den Prozess <link xref="process-kill">abzuwürgen</link>.</p>
  </note>

</page>
