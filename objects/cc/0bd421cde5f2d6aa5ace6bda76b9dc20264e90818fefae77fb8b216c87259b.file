<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-connect-online-accounts" xml:lang="ja">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>
    <link type="guide" xref="getting-started" group="tasks"/>
    <title role="trail" type="link">オンラインアカウントに接続する</title>
    <link type="seealso" xref="accounts"/>
    <title role="seealso" type="link">オンラインアカウントへの接続に関するチュートリアル</title>
    <link type="next" xref="gs-change-date-time-timezone"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>松澤 二郎</mal:name>
      <mal:email>jmatsuzawa@gnome.org</mal:email>
      <mal:years>2013, 2015, 2016</mal:years>
    </mal:credit>
  </info>

  <title>オンラインアカウントに接続する</title>

    <media its:translate="no" type="image" mime="image/svg" src="gs-goa1.svg" width="100%"/>

    <steps>
      <item><p>トップバー右側の<gui xref="shell-introduction#yourname">システムメニュー</gui>を開きます。</p></item>
      <item><p>Click <gui>Settings</gui>.</p></item>
    </steps>

    <media its:translate="no" type="image" mime="image/svg" src="gs-goa2.svg" width="100%"/>
    
    <steps style="continues">
      <item><p>Click the <gui>Online Accounts</gui> panel, then click the online
      account service you want to use.</p>

      <p>This will open a new window where you can sign in to your online
      account.</p></item>
    </steps>
      
    <media its:translate="no" type="image" mime="image/svg" src="gs-goa4.svg" width="100%"/>

    <steps style="continues">
      <item><p>たいていの場合、利用を始めるためには、サインイン後に、アプリケーションによるオンラインアカウントへのアクセスを承認する必要があります。</p></item>
      <item><p>たとえば、Google アカウントに接続する場合、<gui>承認</gui>ボタンを押す必要があります。</p></item>
    </steps>

    <media its:translate="no" type="image" mime="image/svg" src="gs-goa5.svg" width="100%"/>

    <steps style="continues">
      <item><p>多くのオンラインアカウントで、そのアカウントで使用するサービスを選択できます。使用したくないサービスがあれば、ウィンドウ右側にある<gui>オン/オフ</gui>のスイッチを切り替えて、そのサービスを無効にしてください。</p></item>
    </steps>

</page>
