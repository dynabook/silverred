<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting" xml:lang="es">

  <info>
    <link type="guide" xref="net-wireless" group="first"/>
    <link type="guide" xref="hardware#problems" group="first"/>
    <link type="next" xref="net-wireless-troubleshooting-initial-check"/>

    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Contribuyentes al wiki de documentación de Ubuntu</name>
    </credit>
    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Identificar y arreglar problemas con conexiones inalámbricas.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Solucionador de problemas de red inalámbrica</title>

  <p>Esta es una guía paso a paso para resolver problemas que le ayudará a identificar y solucionar problemas con su conexión inalámbrica. Si no puede conectarse a una red inalámbrica, por alguna razón, trate de seguir estas instrucciones.</p>

  <p>Se realizarán los siguientes pasos para conectar su equipo a Internet:</p>

  <list style="numbered compact">
    <item>
      <p>Realizar una comprobación inicial</p>
    </item>
    <item>
      <p>Recopilar información de su hardware</p>
    </item>
    <item>
      <p>Comprobar su hardware</p>
    </item>
    <item>
      <p>Intentar crear una conexión con su enrutador inalámbrico</p>
    </item>
    <item>
      <p>Comprobar su módem y su enrutador</p>
    </item>
  </list>

  <p>Para comenzar, pulse en el enlace <em>Siguiente</em> en la parte superior derecha de la página. Este enlace, y otros como él en las siguientes páginas, le llevarán a través de cada paso en la guía.</p>

  <note>
    <title>Usar la línea de comandos</title>
    <p>Algunas de las instrucciones de esta guía le piden teclear comandos en la <em>línea de comandos</em> (terminal). Puede encontrar la aplicación <app>Terminal</app> en la vista <gui>Actividades</gui>.</p>
    <p>Si no está familiarizado con el uso de la línea de comandos, no se preocupe, esta guía le ayudará en cada paso. Todo lo que necesita es recordar que los comandos distinguen entre mayúsculas o minúsculas (de manera que debe teclearlos <em>exactamente</em> como aparecen aquí), y pulsar <key>Intro</key> después de teclear cada comando para ejecutarlo.</p>
  </note>

</page>
