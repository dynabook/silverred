<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="lockdown-repartitioning" xml:lang="cs">

  <info>
    <link type="guide" xref="user-settings#lockdown"/>
    <link type="seealso" xref="dconf-lockdown"/>
    <revision pkgversion="3.14" date="2014-12-10" status="review"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="copyright editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak zabránit uživatelům měnit diskové oddíly.</desc>
  </info>

  <title>Zamezení ve změnách oddílů</title>

  <p><sys>polkit</sys> umožňuje nastavit oprávnění pro jednotlivé operace. Pro <sys>udisks2</sys>, což je nástroj pro služby spravující disky, se nastavení nachází v <file>/usr/share/polkit-1/actions/org.freedesktop.udisks2.policy</file>. Tento soubor obsahuje sadu činností a výchozích hodnot, které může správce systému přepsat.</p>

  <note style="tip">
    <p>Nastavení <sys>polkit</sys> v <file>/etc</file> přepisuje nastavení v <file>/usr/share</file> šířené v balíčku.</p>
  </note>

  <steps>
    <title>Zamezení ve změnách oddílů</title>
    <item>
      <p>Vytvořte soubor se stejným obsahem, jako je v <file>/usr/share/polkit-1/actions/org.freedesktop.udisks2.policy</file>: <cmd>cp /usr/share/polkit-1/actions/org.freedesktop.udisks2.policy /etc/share/polkit-1/actions/org.freedesktop.udisks2.policy</cmd></p>
      <note style="important">
        <p>Neměňte soubor <file>/usr/share/polkit-1/actions/org.freedesktop.udisks2.policy</file>, změny by se přepsaly při příští aktualizaci balíčku.</p>
      </note>
    </item>
    <item>
      <p>Smažte z prvku <code>policyconfig</code> případné činnosti, které nepotřebujete, a naopak do souboru <file>/etc/polkit-1/actions/org.freedesktop.udisks2.policy</file> přidejte následující řádky:</p>
      <listing>
<code>
  &lt;action id="org.freedesktop.udisks2.modify-device"&gt;
     &lt;description&gt;Modify the drive settings&lt;/description&gt;
     &lt;message&gt;Authentication is required to modify drive settings&lt;/message&gt;
    &lt;defaults&gt;
      &lt;allow_any&gt;no&lt;/allow_any&gt;
      &lt;allow_inactive&gt;no&lt;/allow_inactive&gt;
      &lt;allow_active&gt;yes&lt;/allow_active&gt;
    &lt;/defaults&gt;
&lt;/action&gt;
</code>
      </listing>
      <p>Jestli chcete, aby tuto činnost mohl provádět výhradně uživatel root, nahraďte <code>no</code> za <code>auth_admin</code>.</p>
    </item>
    <item>
      <p>Uložte změny.</p>
    </item>
  </steps>

  <p>Když se uživatel pokusí změnit nastavení disku, zobrazí se mu následující zpráva: <gui>Pro změnu nastavení disku je vyžadováno ověření</gui>.</p>

</page>
