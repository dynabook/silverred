<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="cpu-mem-normal" xml:lang="pt">
  <info>
    <revision version="0.1" date="2014-01-28" status="review"/>
    <link type="guide" xref="index#cpu" group="cpu"/>
    
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    
    <credit type="author copyright">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
      <years>2014</years>
    </credit>

    <desc>Se o computador está a fazer uma grande quantidade de trabalho, utilizará mais tempo de processador e mais memória.</desc>
  </info>

  <title>É normal que o meu computador esteja a usar muito tempo de CPU ou muita memória?</title>
    
  <p>Todos os programas que se executam no o seu computador partilham tempo de CPU (processador) e memória. Se executa vários programas ao mesmo tempo, ou se vários programas estão a fazer muito trabalho, usar-se-á uma percentagem muito alta de ambas coisas.</p>

  <p>Se o uso da CPU está em torno do 100%, isto significa que o computador está a tentar fazer mais trabalho do que pode. Normalmente isto é correcto, mas significa que os programas podem ir um pouco mais lentos. As computadores têm a usar o 100% da CPU quando fazem tarefas que requerem muito processamento, como podem ser os jogo.</p>

  <p>Se o processador está ao 100% durante muito momento, isto pode fazer que o computador vá surpreendentemente lento. Neste caso, deveria <link xref="process-identify-hog">procurar que programa está a usar muito tempo de CPU</link>.</p>

  <p>Se o uso da memória é próximo ao 100%, isto pode reduzir muito as coisas. Isto é porque o computador tentará usar o disco duro como memória temporária, o que se chama <link xref="mem-swap">memória de swap</link>. Os discos duros são bem mais lentos que a memória do sistema.</p>
  
  <p>Pode tentar libertar alguma da memória do sistema fechando alguns programas. Se o computador segue indo lento, prove a reiniciá-lo.</p>
  
  <note>
    <p>A memória mostrada no separador <gui>Recursos</gui> é a <em>memória do sistema</em> (também chamada memória RAM). Isto se utiliza para alojar programas temporariamente, enquanto se executam no computador. Isto não é o mesmo que um disco duro ou que outros tipos de memórias, que se usam para alojar programas e ficheiros de maneira mais permanente.</p>
  </note>

</page>
