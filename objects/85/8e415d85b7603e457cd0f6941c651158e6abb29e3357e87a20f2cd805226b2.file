<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-sensitivity" xml:lang="ca">

  <info>
    <link type="guide" xref="mouse" group="#first"/>
    <link type="guide" xref="a11y#mobility" group="pointing"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.10" date="2013-10-29" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-05" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit>
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit>
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Canvieu la rapidesa amb què es mou el punter quan feu servir el ratolí o el tauler tàctil.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

  <title>Ajustar la velocitat del ratolí i del tauler tàctil</title>

  <p>Si el punter es mou massa ràpid o lent quan moveu el ratolí o utilitzeu el vostre panell tàctil, podeu ajustar la velocitat del punter per a aquests dispositius.</p>

  <steps>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a teclejar <gui>Ratolí &amp; tàctil</gui>.</p>
    </item>
    <item>
      <p>Feu clic sobre <gui>Ratolí i Tauleta</gui> per obrir el quadre.</p>
    </item>
    <item>
      <p>Ajusteu el control lliscant de <gui>Velocitat del ratolí</gui> o <gui>Velocitat del ratolí tàctil</gui> fins que el moviment del punter sigui còmode per a vosaltres. De vegades, els paràmetres més còmodes per a un tipus de dispositiu no són els millors per a un altre.</p>
    </item>
  </steps>

  <note>
    <p>La secció <gui>Tauler tàctil</gui> només apareix si el vostre sistema té un tauler tàctil, mentre que la secció <gui>Ratolí</gui> només és visible quan hi ha un ratolí connectat.</p>
  </note>

</page>
