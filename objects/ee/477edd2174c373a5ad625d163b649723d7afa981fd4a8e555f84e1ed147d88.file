<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-slowkeys" xml:lang="ta">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <credit type="author">
      <name>ஷான் மெக்கேன்ஸ்</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>ஃபில் புல்</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>மைக்கேல் ஹில்</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>எக்காட்டெரினா ஜெராசிமோவா</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>ஒரு விசையை நீங்கள் அழுத்துவதற்கும் அது திரையில் தோன்றுவதற்கும் இடையே ஒரு நேர தாமதத்தையும் அமைக்கலாம்.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>மெதுவான விசைகள் அம்சத்தை இயக்குங்கள்</title>

  <p>ஒரு எழுத்தை நீங்கள் அழுத்துவதற்கும் அது திரையில் தோன்றுவதற்கும் இடையே ஒரு நேர தாமதம் இருக்க நீங்கள் விரும்பினால், <em>மெதுவான விசைகள்</em> அம்சத்தை இயக்கலாம். அதாவது இதை இயக்கினால் நீங்கள் ஒவ்வொரு எழுத்தையும் தட்டச்சு செய்யும் போது அது திரையில் தோன்றுவதற்கு நீங்கள் சற்று நேரம் விசையை அழுத்தியபடி பிடித்திருக்க வேண்டும். தட்டச்சு செய்யும் போது ஒரே சமயத்தில் எதிர்பாராமல் பல விசைகளை அழுத்துபவர் என்றால் அல்லது முதல் முறையே சரியான விசையை அழுத்துவது உங்களுக்கு சிரமம் என்றால் மெதுவான விசைகள் அம்சத்தைப் பயன்படுத்திக்கொள்ளலாம்.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Universal Access</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p><gui>அனைவருக்குமான அணுகல்</gui> ஐத் திறந்து <gui>தட்டச்சு</gui> கீற்றைத் தேர்ந்தெடுக்கவும்.</p>
    </item>
    <item>
      <p>Switch the <gui>Slow Keys</gui> switch to on.</p>
    </item>
  </steps>

  <note style="tip">
    <title>மெதுவான விசைகள் அம்சத்தை விரைவாக இயக்குதலும் அணைத்தலும்</title>
    <p>விசைப்பலகையில் இருந்து, மெதுவான விசைகளை செயல்படுத்தவும் அணைக்கவும் <gui>விசைப்பலகை மூலம் செயல்படுத்து</gui> என்பதன் கீழ், <gui>விசைப்பலகையில் இருந்து அணுகல் வசதி அம்சங்களை இயக்கு</gui> என்பதைத் தேர்ந்தெடுக்கவும். இந்த விருப்பம் தேர்ந்தெடுக்கப்படும் போது, நீங்கள் <key>Shift</key> விசையை எட்டு வினாடிகளுக்கு அழுத்திப் பிடித்திருப்பதன் மூலம் மெதுவான விசைகளை இயக்க அல்லது முடக்கலாம்.</p>
    <p>மேல் பட்டியில் உள்ள<link xref="a11y-icon">அணுகல் வசதி சின்னத்தைச்</link> சொடுக்கி, <gui>மெதுவனா விசைகள்</gui> என்பதைத் தேர்ந்தெடுப்பதன் மூலமும் நீங்கள் மெதுவான விசைகளை இயக்கலாம், அணைக்கலாம். <gui>அனைவருக்குமான அணுகல்</gui> பலகத்தில் ஒன்று அல்லது அதிகமான அமைவுகள் செயல்படுத்தப்பட்டிருந்தால், அணுகல் வசதி சின்னம் புலப்படும்.</p>
  </note>

  <p>ஒரு விசையை நீங்கள் அழுத்துகையில், அந்த விசை ஏற்றுக்கொள்ளப்பட எவ்வளவு நேரம் அதை அழுத்திப் பிடித்திருக்க வேண்டும் என்ற நேரத்தைக் கட்டுப்படுத்த, <gui>ஏற்றலில் தாமதம்</gui> அளவுகோலைப் பயன்படுத்தலாம்.</p>

  <p>You can have your computer make a sound when you press a key, when a key
  press is accepted, or when a key press is rejected because you didn’t hold
  the key down long enough.</p>

</page>
