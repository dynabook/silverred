<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-calibrate-printer" xml:lang="gl">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-calibrate-scanner"/>
    <link type="seealso" xref="color-calibrate-screen"/>
    <link type="seealso" xref="color-calibrate-camera"/>
    <desc>Calibrar a súa impresora é importante para imprimir cores precisos.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2020</mal:years>
    </mal:credit>
  </info>

  <title>Como calibrar a miña impresora?</title>

  <p>Hai dúas formas de calibrar un dispositivo de impresión:</p>

  <list>
    <item><p>Usando un fotoespectómetro como o Pantone ColorMunki</p></item>
    <item><p>Descargando un ficheiro de referencia de impresión desde unha empresa de cor</p></item>
  </list>

  <p>Usar unha empresa de cor para xerar un perfil de impresora é a opción máis barata se ten un ou dous tipos de papel diferentes. Descargando o gráfico de referencia desde o sitio web da empresa, pode despois enviarlles a impresión nun sobre acolchado que eles analizaran, xerarán o perfil e enviaranlle un perfil ICC preciso.</p>
  <p>Usar un dispositivo caro tal como ColorMunki só compensa se está perfilando un gran número de conxuntos de tinta ou tipos de papeis.</p>

  <note style="tip">
    <p>Se cambia o fornecedor de tinta, asegúrese de recalibrar a impresora!</p>
  </note>

</page>
