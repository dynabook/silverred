<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-purge" xml:lang="es">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.10" date="2013-09-29" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shaun McCance</name>
      <email>mdhillca@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Establecer con qué frecuencia se limpian la papelera y los archivos temporales de su equipo.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Limpiar la papelera y los archivos temporales</title>

  <p>Limpiar la papelera y los archivos temporales elimina archivos innecesarios del equipo, y también libera espacio en el disco duro. Puede vaciar la papelera y limpiar los archivos temporales manualmente, pero también puede configurar el equipo para que lo haga automáticamente.</p>

  <p>Los archivos temporales son archivos creados automáticamente por las aplicaciones en segundo plano. Pueden mejorar el rendimiento, proporcionando una copia de datos que se hayan descargado o procesado.</p>

  <steps>
    <title>Vaciar automáticamente la papelera y limpiar los archivos temporales</title>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> empiece a escribir <gui>Privacidad</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Privacidad</gui> para abrir el panel.</p>
    </item>
    <item>
      <p>Seleccione <gui>Limpiar la papelera y los archivos temporales</gui>.</p>
    </item>
    <item>
      <p>Active uno o ambos interruptores <gui>Vaciar la papelera automáticamente</gui> o <gui>Limpiar los archivos temporales automáticamente</gui>.</p>
    </item>
    <item>
      <p>Establezca la frecuencia con la que quiere que se limpien la <em>Papelera</em> y los <em>Archivos temporales</em> cambiando el valor de <gui>Limpiar después de</gui>.</p>
    </item>
    <item>
      <p>Use los <gui>Vaciar papelera</gui> o <gui>Limpiar archivos temporales</gui> para realizar estas acciones inmediatamente.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Puede eliminar archivos inemdiata y permanentemente si usar la papelera. Consulte la sección <link xref="files-delete#permanent"/> para obtener más información.</p>
  </note>

</page>
