<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="bluetooth-visibility" xml:lang="ca">

  <info>
    <link type="guide" xref="bluetooth" group="#last"/>
    <link type="seealso" xref="bluetooth-device-specific-pairing"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-04" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Si altres dispositius poden detectar l'ordinador.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

  <title>Què és la visibilitat del Bluetooth?</title>

  <p>La visibilitat de Bluetooth fa referència a si altres dispositius poden detectar l'equip quan cerquen dispositius Bluetooth. Quan Bluetooth està actiu i el quadre de <gui>Bluetooth</gui> està obert, el seu equip es publicarà a tots els altres dispositius dins del rang, permetent-los intentar connectar-se al seu ordinador.</p>

  <note style="tip">
    <p>Pot <link xref="sharing-displayname">canviar</link> el nom que l'ordinador mostra als altres dispositius.</p>
  </note>

  <p>Un cop <link xref="bluetooth-connect-device">connectat a un dispositiu</link>, ni l'ordinador ni el dispositiu necessiten ser visibles per comunicar-se entre ells.</p>

  <p>Els dispositius sense pantalla solen tenir un mode de sincronització que es pot activar prement un botó o bé una combinació de botons durant un determinat temps, ja sigui quan ja han estat engegats o quan s'engeguin.</p>

  <p>La millor manera de trobar com accedir a aquest mode és consultar el manual del dispositiu. Per a alguns dispositius, el procediment podria ser <link xref="bluetooth-device-specific-pairing"> lleugerament diferent de l'habitual </link>.</p>

</page>
