<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="solaris-mode" xml:lang="pt">
  <info>
    <revision version="0.2" pkgversion="3.11" date="2014-01-26" status="review"/>
    <link type="guide" xref="index#other" group="other"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author copyright">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
      <years>2011</years>
    </credit>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2011, 2014</years>
    </credit>

    <desc>Use Solaris mode to reflect the number of CPUs.</desc>
  </info>

  <title>Que é o modo Solaris?</title>

  <p>Em sistemas que têm várias CPU ou vários <link xref="cpu-multicore">núcleos</link>, os processos podem usar mais de um ao mesmo tempo. É possível que a coluna <gui>% CPU</gui> mostre valores superiores ao 100% (ej 400% num sistema de 4 CPU). O <gui>modo Solaris</gui> divide o <gui>% CPU</gui> da cada processo entre o número de CPU do sistema, pelo que o total será o 100%.</p>

  <p>Para mostrar o <gui>% CPU</gui> no <gui>modo Solaris</gui>:</p>

  <steps>
    <item><p>Carregue em <gui>Preferências</gui> no menu da aplicação.</p></item>
    <item><p>Carregue no separador <gui>Processos</gui>.</p></item>
    <item><p>Seleccione <gui>Dividir o uso de CPU entre o número de CPU</gui>.</p></item>
  </steps>

    <note><p>O termo <gui>modo Solaris</gui> deriva do UNIX de Sun, comparado com o modo IRIX predeterminado de Linux, nomeado para UNIX SGI.</p></note>

</page>
