<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-capacity" xml:lang="es">
  <info>
    <link type="guide" xref="disk"/>

    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <revision pkgversion="3.4.3" date="2012-06-15" status="review"/>
    <revision pkgversion="3.13.91" date="2014-09-05" status="review"/>

    <desc>Use el <gui>Analizador de uso del disco</gui> o el <gui>Monitor del sistema</gui> para comprobar el espacio y la capacidad.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>Comprobar cuánto espacio de disco hay disponible</title>

  <p>Puede comprobar el espacio de disco libre y la capacidad del disco usando el <app>Analizador de uso del disco</app> y el <app>Monitor del sistema</app>.</p>

<section id="disk-usage-analyzer">
<title>Comprobar con el analizador de uso de disco</title>

  <p>Para comprobar el espacio de disco libre y la capacidad del disco usando el <gui>Analizador de uso del disco</gui>:</p>

  <list>
    <item>
      <p>Abra el <app>Analizador de uso de discos</app> desde la vista general de <gui>Actividades</gui>. La ventana mostrará una lista de los sistemas de archivos y el uso y la capacidad de cada uno de ellos.</p>
    </item>
    <item>
      <p>Pulse uno de los elementos en la lista para ver un resumen detallado del uso de cada elemento. Pulse el botón del menú y elija <gui>Analizar carpeta…</gui> o <gui>Analizar carpeta remota…</gui> para analizar una ubicación diferente.</p>
    </item>
  </list>
  <p>La información se muestra según la <gui>Carpeta</gui>, el <gui>Tamaño</gui>, y el <gui>Contenido</gui> y cuándo se <gui>Modificaron</gui> los datos última vez. Puede consultar más detalles en el <link href="help:baobab"><app>Analizador de uso del disco</app></link>.</p>

</section>

<section id="system-monitor">

<title>Comprobar con el monitor del sistema</title>

  <p>Para comprobar el espacio libre del disco y la capacidad con el <gui>Monitor del sistema</gui>:</p>

<steps>
 <item>
  <p>Abra la aplicación <app>Monitor del sistema</app> desde la vista de <gui>Actividades</gui>.</p>
 </item>
 <item>
  <p>Seleccione la pestaña <gui>Sistemas de archivos</gui> para ver las particiones del sistema y el espacio usado en el disco en <gui>Total</gui>, <gui>Libre</gui>, <gui>Disponible</gui> y <gui>Usado</gui>.</p>
 </item>
</steps>
</section>

<section id="disk-full">

<title>¿Qué pasa si el disco está demasiado lleno?</title>

  <p>Si el disco está muy lleno, puede:</p>

 <list>
  <item>
   <p>Eliminar archivos que no son importantes o que no va a usar más.</p>
  </item>
  <item>
   <p>Haga <link xref="backup-why">copias de seguridad</link> de los archivos importantes que no vaya a necesitar durante un tiempo y elimínelos del disco duro.</p>
  </item>
 </list>
</section>

</page>
