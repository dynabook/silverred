<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-get-online" xml:lang="fi">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>
    <link type="guide" xref="getting-started" group="videos"/>
    <title role="trail" type="link">Yhdistä verkkoon</title>
    <link type="seealso" xref="net"/>
    <title role="seealso" type="link">Opas verkkoon yhdistämisestä</title>
    <link type="next" xref="gs-browse-web"/>
  </info>

  <title>Yhdistä verkkoon</title>
  
  <note style="important">
      <p>Näet verkkoyhteytesi tilan yläpalkin oikeasta laidasta.</p>
  </note>  

  <section id="going-online-wired">

    <title>Yhdistä kiinteään verkkoon</title>

    <media its:translate="no" type="image" mime="image/svg" src="gs-go-online1.svg" width="100%"/>

    <steps>
      <item><p>Verkkoyhteyden kuvake yläpalkin oikeassa kulmassa näyttää, että olet yhteydettömässä tilassa.</p>
      <p>Yhteydetön tila voi johtua monesta syystä, esimerkiksi verkkokaapeli saattaa olla irti, tietokone voi olla asetettu <em>lentokonetilaan</em> tai wifi-verkkoja ei ole käytettävissä sijainnissasi.</p>
      <p>Jos haluat käyttää langallista yhteyttä, kytke johto tietokoneeseen. Tietokone yrittää määrittää ja muodostaa verkkoyhteyden automaattisesti.</p>
      <p>Kun tietokone muodostaa verkkoyhteyttä, verkkoyhteyden kuvake näyttää kolmea pistettä.</p></item>
      <item><p>Kun verkkoyhteys on muodostettu onnistuneesti, verkkoyhteyden kuvake muuttuu osoittamaan kiinteän verkon yhteyttä.</p></item>
    </steps>
    
  </section>

  <section id="going-online-wifi">

    <title>Yhdistä wifi-verkkoon</title>

    <media its:translate="no" type="image" mime="image/svg" src="gs-go-online2.svg" width="100%"/>
    
    <steps>
      <title>Yhdistä langattomaan wifi-verkkoon seuraavasti:</title>
      <item>
        <p>Napsauta <gui xref="shell-introduction#yourname">järjestelmävalikkoa</gui> yläpalkin oikeasta laidasta.</p>
      </item>
      <item>
        <p>Valitse <gui>Wi-Fi - Ei yhdistetty</gui>. Valikon wifi-osio laajenee.</p>
      </item>
      <item>
        <p>Napsauta <gui>Valitse verkko</gui>.</p>
      </item>
    </steps>

     <note style="important">
       <p>Voit yhdistää langattomaan wifi-verkkoon vain jos tietokoneesi laitteisto tukee sitä ja olet wifi-verkon kantoalueella.</p>
     </note>

    <media its:translate="no" type="image" mime="image/svg" src="gs-go-online3.svg" width="100%"/>

    <steps style="continues">
    <item><p>Valitse haluamasi wifi-verkko ja napsauta <gui>Yhdistä</gui> vahvistaaksesi yhteyden muodostamisen.</p>
    <p>Riippuen verkon asetuksista, sinulta saatetaan kysyä salasanaa verkkoon liittymiseksi.</p></item>
    </steps>

  </section>

</page>
