<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-browse-web" version="1.0 if/1.0" xml:lang="fi">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>
    <link type="guide" xref="getting-started" group="tasks"/>
    <title role="trail" type="link">Selaa verkkoa</title>
    <link type="seealso" xref="net-browser"/>
    <title role="seealso" type="link">Opas internetin selaamiseen</title>
    <link type="next" xref="gs-connect-online-accounts"/>
  </info>

  <title>Selaa verkkoa</title>

<if:choose>
<if:when test="platform:gnome-classic">

    <media its:translate="no" type="image" mime="image/svg" src="gs-web-browser1-firefox-classic.svg" width="100%"/>

    <steps>
      <item><p>Napsauta <gui>Sovellukset</gui>-valikkoa näytön vasemmasta yläkulmasta.</p></item>
      <item><p>Valitse valikosta <guiseq><gui>Internet</gui><gui>Firefox</gui> </guiseq>.</p></item>
    </steps>

</if:when>

<!--Distributors might want to add their distro here if they ship Firefox by default.-->
<if:when test="platform:centos, platform:debian, platform:fedora, platform:rhel, platform:ubuntu, platform:opensuse, platform:sled, platform:sles">

    <media its:translate="no" type="image" mime="image/svg" src="gs-web-browser1-firefox.svg" width="100%"/>

    <steps>
      <item><p>Siirrä hiiren osoitin näytön vasemman yläreunan <gui>Toiminnot</gui>-kulmaan avataksesi <gui>Toiminnot-yleisnäkymän</gui>.</p></item>
      <item><p>Valitse <app>Firefox</app>-selaimen kuvake näytön vasemman reunan palkista.</p></item>
    </steps>

    <note><p>Vaihtoehtoisesti voit käynnistää selaimen <link xref="gs-use-system-search">kirjoittamalla</link> <em>Firefox</em> <gui>Toiminnot-yleisnäkymässä</gui>.</p></note>

</if:when>
<if:else>

    <media its:translate="no" type="image" mime="image/svg" src="gs-web-browser1.svg" width="100%"/>

    <steps>
      <item><p>Siirrä hiiren osoitin näytön vasemman yläreunan <gui>Toiminnot</gui>-kulmaan avataksesi <gui>Toiminnot-yleisnäkymän</gui>.</p></item>
      <item><p>Napsauta näytön vasemmassa laidassa olevaa <app>Verkko</app>-selaimen kuvaketta.</p></item>
    </steps>

    <note><p>Vaihtoehtoisesti voit käynnistää selaimen <link xref="gs-use-system-search">kirjoittamalla</link> <em>verkko</em> <gui>Toiminnot</gui>-yleisnäkymässä.</p></note>

    <media its:translate="no" type="image" mime="image/svg" src="gs-web-browser2.svg" width="100%"/>

</if:else>
</if:choose>

<!--Distributors might want to add their distro here if they ship Firefox by default.-->
<if:if test="platform:centos, platform:debian, platform:fedora,platform:rhel, platform:ubuntu, platform:opensuse, platform:sled, platform:sles">

    <media its:translate="no" type="image" mime="image/svg" src="gs-web-browser2-firefox.svg" width="100%"/>

</if:if>

    <steps style="continues">
      <item><p>Napsauta osoitepalkkia selainikkunan yläosassa ja aloita haluamasi verkkosivun osoitteen kirjoittaminen.</p></item>
      <item><p>Verkkosivun kirjoittamisen myötä selain alkaa etsiä selaushistoriaa ja kirjanmerkkejä, joten sinun ei tarvitse muistaa sivun tarkkaa osoitetta.</p>
        <p>Jos verkkosivu löytyy selaushistoriasta tai kirjanmerkeistä, pudotusvalikko ilmestyy osoitepalkin alapuolelle.</p></item>
      <item><p>Voit valita verkkosivun pudotusvalikosta nopeasti käyttämällä nuolinäppäimiä.</p>
      </item>
      <item><p>Kun olet valinnut verkkosivun, paina <key>Enter</key> siirtyäksesi kyseiselle sivulle.</p>
      </item>
    </steps>

</page>
