<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-get-online" xml:lang="mr">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>
    <link type="guide" xref="getting-started" group="videos"/>
    <title role="trail" type="link">ऑनलाईन या</title>
    <link type="seealso" xref="net"/>
    <title role="seealso" type="link">ऑनलाईन येण्याबाबत वर्गपाठ</title>
    <link type="next" xref="gs-browse-web"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande</mal:name>
      <mal:email>djaniketster@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>ऑनलाईन या</title>
  
  <note style="important">
      <p>नेटवर्कची स्थिती तुम्ही उजव्या हाताला सगळ्यात वरच्या पट्टीवर पाहू शकता.</p>
  </note>  

  <section id="going-online-wired">

    <title>वायर्ड नेटवर्कशी जोडा</title>

    <media its:translate="no" type="image" mime="image/svg" src="gs-go-online1.svg" width="100%"/>

    <steps>
      <item><p>The network connection icon on the right-hand side of the top
       bar shows that you are offline.</p>
      <p>The offline status can be caused by a number of reasons: for
       example, a network cable has been unplugged, the computer has been set
       to run in <em>airplane mode</em>, or there are no available Wi-Fi
       networks in your area.</p>
      <p>तुम्हाला वायर्ड जोडणी वापरायची असेल तर, ऑनलाईन जायला फक्त जोडणी तार लावा. कम्प्युटर तुमच्यासाठी आपोआप नेटवर्क जोडणी स्थापित करण्याचा प्रयत्न करेल.</p>
      <p>संगणक तुमच्यासाठी नेटवर्कची जोडणी करत असतांना, नेटवर्कची जोडणी चिन्ह तीन टिंब दाखवतं.</p></item>
      <item><p>एकदा नेटवर्क जोडणी स्थापित झाली की, नेटवर्क जोडणी चिन्ह हे कमप्यूटर चिन्हामध्ये बदलतं.</p></item>
    </steps>
    
  </section>

  <section id="going-online-wifi">

    <title>वाय-फाय नेटवर्कशी जोडणी करा</title>

    <media its:translate="no" type="image" mime="image/svg" src="gs-go-online2.svg" width="100%"/>
    
    <steps>
      <title>To connect to a Wi-Fi (wireless) network:</title>
      <item>
        <p>Click the <gui xref="shell-introduction#yourname">system menu</gui>
        on the right side of the top bar.</p>
      </item>
      <item>
        <p>Select <gui>Wi-Fi Not Connected</gui>. The Wi-Fi section of the menu
        will expand.</p>
      </item>
      <item>
        <p>Click <gui>Select Network</gui>.</p>
      </item>
    </steps>

     <note style="important">
       <p>तुम्ही फक्त वाय-फाय नेटवर्कशी जोडणी करू शकता, संगणकावरील हार्डवेअर त्याकरिता समर्थन पुरवत अल्यास आणि तुम्ही वाय-फाय व्याप्ति अंतर्गत असाल.</p>
     </note>

    <media its:translate="no" type="image" mime="image/svg" src="gs-go-online3.svg" width="100%"/>

    <steps style="continues">
    <item><p>उपलब्ध वाय-फाय नेटवर्क्सच्या सूचीतून, जोडणीजोगी नेटवर्क पसंत करा, आणि खात्रीकरिता <gui>जोडणी करा</gui> क्लिक करा.</p>
    <p>नेटवर्क संरचनावर आधारित, तुम्हाला नेटवर्क श्रेयकरिता विनंती केली जाईल.</p></item>
    </steps>

  </section>

</page>
