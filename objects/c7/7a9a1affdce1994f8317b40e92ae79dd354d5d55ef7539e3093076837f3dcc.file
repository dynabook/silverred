<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-default-email" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="net-email"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Altere o cliente padrão de e-mail indo em <gui>Detalhes</gui> dentro de <gui>Configurações do sistema</gui>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

  <title>Alterando qual aplicativo de correio é usado para escrever e-mails</title>

  <p>Quando você clica em um botão ou link para enviar um novo e-mail (por exemplo, em seu aplicativo processador de textos), seu aplicativo padrão de correio irá se abrir com uma mensagem em branco, pronta para você escrever. Se você tiver mais que um aplicativo de correio instalado, contudo, o aplicativo de correio errado pode se abrir. Você pode corrigir isso alterando qual é o aplicativo de e-mail padrão:</p>

  <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Detalhes</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Detalhes</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Escolha <gui>Aplicativos padrões</gui> na lista do lado esquerdo da janela.</p>
    </item>
    <item>
      <p>Escolha qual cliente de e-mail você gostaria que fosse usado por padrão alterando a opção <gui>Correio</gui>.</p>
    </item>
  </steps>

</page>
