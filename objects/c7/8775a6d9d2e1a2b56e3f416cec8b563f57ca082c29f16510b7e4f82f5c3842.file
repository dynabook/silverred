<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-what" xml:lang="id">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Proyek Dokumentasi GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Back up apapun yang Anda tidak ingin kehilangan bila terjadi masalah.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

  <title>Apa yang akan dibackup</title>

  <p>Prioritas Anda mestilah untuk membackup <link xref="backup-thinkabout">berkas-berkas paling penting</link> milik Anda dan juga yang sulit dibuat ulang. Sebagai contoh, diurutkan dari paling penting ke kurang penting:</p>

<terms>
 <item>
  <title>Berkas pribadi Anda</title>
   <p>Ini mungkin termasuk dokumen, lembar kerja, surel, janji temu, data finansial, foto keluarga, atau sebarang berkas pribadi lain yang Anda anggap tak tergantikan.</p>
 </item>

 <item>
  <title>Pengaturan pribadi Anda</title>
   <p>Ini termasuk perubahan yang mungkin telah Anda buat untuk warna, latar belakang, resolusi layar dan pengaturan tetikus pada desktop Anda. Ini juga termasuk preferensi aplikasi, seperti pengaturan untuk <app>LibreOffice</app>, pemutar musik Anda, dan program surel Anda. Mereka dapat diganti, tetapi mungkin memerlukan waktu untuk menciptakan ulang.</p>
 </item>

 <item>
  <title>Pengaturan sistem</title>
   <p>Kebanyakan orang tak pernah mengubah pengaturan sistem yang dibuat saat instalasi. Bila Anda mengkustomisasi pengaturan sistem Anda untuk beberapa alasan, atau bila Anda memakai komputer Anda sebagai server, maka Anda mungkin ingin membackup pengaturan-pengaturan ini.</p>
 </item>

 <item>
  <title>Perangkat lunak terpasang</title>
   <p>Perangkat lunak yang Anda pakai biasanya dapat dipulihkan dengan cukup cepat setelah masalah komputer serius dengan memasang ulang.</p>
 </item>
</terms>

  <p>Secara umum, Anda akan ingin membackup berkas-berkas yang tak tergantikan dan berkas-berkas yang memerlukan investasi waktu sangat besar untuk menggantikannya tanpa backup. Bila berbagai hal mudah digantikan, di lain pihak, Anda mungkin tak mau menghabiskan ruang disk untuk backup mereka.</p>

</page>
