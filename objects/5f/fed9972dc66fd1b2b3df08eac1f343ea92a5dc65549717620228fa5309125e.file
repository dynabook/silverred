<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-launch-applications" xml:lang="cs">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovář</name>
    </credit>

    <link type="guide" xref="getting-started" group="videos"/>
    <title role="trail" type="link">Spouštění aplikací</title>
    <link type="seealso" xref="shell-apps-open"/>
    <title role="seealso" type="link">Vysvětlení, jak spouštět aplikace</title>
    <link type="next" xref="gs-switch-tasks"/>
  </info>

  <title>Spouštění aplikací</title>

  <ui:overlay width="812" height="452">
   <media type="video" its:translate="no" src="figures/gnome-launching-applications.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-launching-apps.svg"/>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>Spouštění aplikací</tt:p>
         </tt:div>
         <tt:div begin="5s" end="7.5s">
           <tt:p>Přejeďte ukazatelem myši do rohu <gui>Činnosti</gui> v levém horním rohu obrazovky.</tt:p>
           </tt:div>
         <tt:div begin="7.5s" end="9.5s">
           <tt:p>Klikněte na ikonu <gui>Zobrazit aplikace</gui>.</tt:p>
         </tt:div>
         <tt:div begin="9.5s" end="11s">
           <tt:p>Klikněte na aplikaci, kterou chcete spustit, například Nápověda.</tt:p>
         </tt:div>
         <tt:div begin="12s" end="21s">
           <tt:p>Případně použijte klávesnici k otevření <gui>Přehledu činností</gui> zmáčknutím klávesy <key href="help:gnome-help/keyboard-key-super">Super</key>.</tt:p>
         </tt:div>
         <tt:div begin="22s" end="29s">
           <tt:p>Začněte psát název aplikace, kterou chcete spustit.</tt:p>
         </tt:div>
         <tt:div begin="30s" end="33s">
           <tt:p>Zmáčknutím <key>Enter</key> aplikaci spustíte.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
   </media>
  </ui:overlay>

  <section id="launch-apps-mouse">
    <title>Spouštění aplikací pomocí myši</title>
 
  <steps>
    <item><p>Přejeďte ukazatelem myši do rohu <gui>Činnosti</gui> v levém horním rohu obrazovky a zobrazí se vám <gui>Přehled činností</gui>.</p></item>
    <item><p>Klikněte na ikonu <gui>Zobrazit aplikace</gui>, kterou najdete v dolní části lišty na levé straně obrazovky.</p></item>
    <item><p>Zobrazí se seznam aplikací. Klikněte na aplikaci, kterou chcete spustit, například Nápověda.</p></item>
  </steps>

  </section>

  <section id="launch-app-keyboard">
    <title>Spouštění aplikací pomocí klávesnice</title>

  <steps>
    <item><p>Otevřete <gui>Přehled činností</gui> zmáčknutím klávesy <key href="help:gnome-help/keyboard-key-super">Super</key>.</p></item>
    <item><p>Začněte psát název aplikace, kterou chcete spustit. Aplikace se ihned začne vyhledávat.</p></item>
    <item><p>Až se vám zobrazí ikona požadované aplikace, vyberte ji a zmáčknutím <key>Enter</key> aplikaci spusťte.</p></item>
  </steps>

  </section>

</page>
