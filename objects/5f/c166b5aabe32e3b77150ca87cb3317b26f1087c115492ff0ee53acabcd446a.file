<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="net-macaddress" xml:lang="it">

  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>L'identificativo univoco assegnato all'hardware di rete.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Cos'è un indirizzo MAC?</title>

  <p>Un <em>indirizzo MAC</em> è l'identificativo univoco assegnato dal produttore a un'unità hardware di rete (come una scheda wireless o ethernet). MAC sta per <em>Media Access Control</em> e ogni identificativo è assegnato esclusivamente a un particolare dispositivo.</p>

  <p>Un indirizzo MAC è composto da sei gruppi di due caratteri, ciascuno separato da due punti: <code>00:1B:44:11:3A:B7</code> è un esempio di indirizzo MAC.</p>

  <p>Per identificare l'indirizzo MAC del proprio hardware di rete:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Network</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Network</gui> to open the panel.</p>
    </item>
    <item>
      <p>Choose which device, <gui>Wi-Fi</gui> or <gui>Wired</gui>, from
      the left pane.</p>
      <p>The MAC address for the wired device will be displayed as the
      <gui>Hardware Address</gui> on the right.</p>
      
      <p>Click the
      <media its:translate="no" type="image" src="figures/emblem-system.png"><span its:translate="yes">settings</span></media>
      button to see the MAC address for the wireless device displayed as the
      <gui>Hardware Address</gui> in the <gui>Details</gui> panel.</p>
    </item>
  </steps>

  <p>In practice, you may need to modify or “spoof” a MAC address. For example,
  some internet service providers may require that a specific MAC address be
  used to access their service. If the network card stops working, and you need
  to swap a new card in, the service won’t work anymore. In such cases, you
  would need to spoof the MAC address.</p>

</page>
