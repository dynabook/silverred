<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="getting-started" version="1.0 if/1.0" xml:lang="nl">

<info>
    <link type="guide" xref="index" group="gs"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <desc>Nieuw bij Gnome? Leer hoe u het onder de knie krijgt.</desc>
    <title type="link">Getting started with GNOME</title>
    <title type="text">Aan de slag</title>
</info>

<title>Aan de slag</title>

<if:choose>
<if:when test="!platform:gnome-classic">

  <ui:overlay width="235" height="145">
  <media type="video" its:translate="no" src="figures/gnome-launching-applications.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-launching-apps.svg">
      <ui:caption its:translate="yes"><desc style="center">Toepassingen starten</desc></ui:caption>
    </ui:thumb>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>Toepassingen starten</tt:p>
         </tt:div>
         <tt:div begin="5s" end="7.5s">
           <tt:p>Breng de muisaanwijzer naar de <gui>Activiteiten</gui>-hoek linksboven in het scherm.</tt:p>
           </tt:div>
         <tt:div begin="7.5s" end="9.5s">
           <tt:p>Klik op het pictogram <gui>Toepassingen tonen</gui>.</tt:p>
         </tt:div>
         <tt:div begin="9.5s" end="11s">
           <tt:p>Klik op de toepassing die u wilt starten, bijvoorbeeld Hulp.</tt:p>
         </tt:div>
         <tt:div begin="12s" end="21s">
           <tt:p>U kunt ook het toetsenbord gebruiken voor het openen van het <gui>Activiteitenoverzicht</gui> door de <key href="help:gnome-help/keyboard-key-super">Super</key>-toets in te drukken.</tt:p>
         </tt:div>
         <tt:div begin="22s" end="29s">
           <tt:p>Begin met het intypen van de toepassing die u wilt starten.</tt:p>
         </tt:div>
         <tt:div begin="30s" end="33s">
           <tt:p>Druk op <key>Enter</key> om de toepassing te starten.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

</if:when>
</if:choose>

  <ui:overlay width="235" height="145">
  <media type="video" its:translate="no" src="figures/gnome-task-switching.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-task-switching.svg">
        <ui:caption its:translate="yes"><desc style="center">Van taak wisselen</desc></ui:caption>
    </ui:thumb>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>Van taak wisselen</tt:p>
         </tt:div>
         <tt:div begin="5s" end="8s">
           <tt:p if:test="!platform:gnome-classic">Breng de muisaanwijzer naar de <gui>Activiteiten</gui>-hoek linksboven in het scherm.</tt:p>
         </tt:div>
         <tt:div begin="9s" end="12s">
           <tt:p>Klik op een venster om over te schakelen naar die taak.</tt:p>
         </tt:div>
         <tt:div begin="12s" end="14s">
           <tt:p>Om een venster links te maximaliseren: pak de titelbalk van het venster en sleep het naar de linkerkant.</tt:p>
         </tt:div>
         <tt:div begin="14s" end="16s">
           <tt:p>Laat het venster los zodra de linkerhelft van het scherm oplicht.</tt:p>
         </tt:div>
         <tt:div begin="16s" end="18">
           <tt:p>Om een venster rechts te maximaliseren: pak de titelbalk van het venster en sleep het naar de rechterkant.</tt:p>
         </tt:div>
         <tt:div begin="18s" end="20s">
           <tt:p>Laat het venster los zodra de linkerhelft van het scherm oplicht.</tt:p>
         </tt:div>
         <tt:div begin="23s" end="27s">
           <tt:p>Druk op <keyseq> <key href="help:gnome-help/keyboard-key-super">Super</key><key> Tab</key></keyseq> voor het tonen van de <gui>vensterwisselaar</gui>.</tt:p>
         </tt:div>
         <tt:div begin="27s" end="29s">
           <tt:p>Laat <key href="help:gnome-help/keyboard-key-super">Super </key> los om het volgende opgelichte venster te selecteren.</tt:p>
         </tt:div>
         <tt:div begin="29s" end="32s">
           <tt:p>Om de lijst van geopende vensters te doorlopen: laat <key href="help:gnome-help/keyboard-key-super">Super</key> niet los maar houd hem ingedrukt en druk op <key>Tab</key>.</tt:p>
         </tt:div>
         <tt:div begin="35s" end="37s">
           <tt:p>Druk op de <key href="help:gnome-help/keyboard-key-super">Super</key>-toets voor het tonen van het <gui>activiteitenoverzicht</gui>.</tt:p>
         </tt:div>
         <tt:div begin="37s" end="40s">
           <tt:p>Begin met het intypen van de naam van de toepassing waar u naartoe wilt.</tt:p>
         </tt:div>
         <tt:div begin="40s" end="43s">
           <tt:p>Wanneer de toepassing bovenaan staat drukt u op <key> Enter</key> om erheen te gaan.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

  <ui:overlay width="235" height="145">
  <media type="video" its:translate="no" src="figures/gnome-windows-and-workspaces.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-windows-and-workspaces.svg">
          <ui:caption its:translate="yes"><desc style="center">Vensters en werkbladen gebruiken</desc></ui:caption>
    </ui:thumb>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>Vensters en werkbladen</tt:p>
         </tt:div>
         <tt:div begin="6s" end="10s">
           <tt:p>Om een venster te maximaliseren: pak de titelbalk van het venster en sleep het naar de bovenkant van het scherm  </tt:p>
           </tt:div>
         <tt:div begin="10s" end="13s">
           <tt:p>Laat het venster los zodra het oplicht.</tt:p>
         </tt:div>
         <tt:div begin="14s" end="20s">
           <tt:p>Om een venster weer te verkleinen: pak de titelbalk van het venster en sleep het weg van de randen van het scherm.</tt:p>
         </tt:div>
         <tt:div begin="25s" end="29s">
           <tt:p>U kunt ook klikken op de bovenste balk om het venster weg te slepen en te verkleinen.</tt:p>
         </tt:div>
         <tt:div begin="34s" end="38s">
           <tt:p>Om een venster links te maximaliseren: pak de titelbalk van het venster en sleep het naar de linkerkant.</tt:p>
         </tt:div>
         <tt:div begin="38s" end="40s">
           <tt:p>Laat het venster los zodra de linkerhelft van het scherm oplicht.</tt:p>
         </tt:div>
         <tt:div begin="41s" end="44s">
           <tt:p>Om een venster rechts te maximaliseren: pak de titelbalk van het venster en sleep het naar de rechterkant.</tt:p>
         </tt:div>
         <tt:div begin="44s" end="48s">
           <tt:p>Laat het venster los zodra de linkerhelft van het scherm oplicht.</tt:p>
         </tt:div>
         <tt:div begin="54s" end="60s">
           <tt:p>Om een venster te maximaliseren via het toetsenbord: houd de <key href="help:gnome-help/keyboard-key-super">Super</key>-toets ingedrukt en druk op <key>↑</key>.</tt:p>
         </tt:div>
         <tt:div begin="61s" end="66s">
           <tt:p>Om het venster weer te verkleinen: houd de <key href="help:gnome-help/keyboard-key-super">Super</key>-toets ingedrukt en druk op <key>↓</key>.</tt:p>
         </tt:div>
         <tt:div begin="66s" end="73s">
           <tt:p>Om een venster rechts te maximaliseren: houd de <key href="help:gnome-help/keyboard-key-super">Super</key>-toets ingedrukt en druk op <key>→</key>.</tt:p>
         </tt:div>
         <tt:div begin="76s" end="82s">
           <tt:p>Om een venster links te maximaliseren: houd de <key href="help:gnome-help/keyboard-key-super">Super</key>-toets ingedrukt en druk op <key>←</key>.</tt:p>
         </tt:div>
         <tt:div begin="83s" end="89s">
           <tt:p>Om naar een werkblad onder het huidige werkblad te gaan: druk op <keyseq><key href="help:gnome-help/keyboard-key-super">Super </key><key>Page Down</key></keyseq>.</tt:p>
         </tt:div>
         <tt:div begin="90s" end="97s">
           <tt:p>Om naar een werkblad boven het huidige werkblad te gaan: druk op <keyseq><key href="help:gnome-help/keyboard-key-super">Super </key><key>Page Down</key></keyseq>.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

<links type="topic" style="grid" groups="tasks">
<title style="heading">Common tasks</title>
</links>

<links type="guide" style="heading nodesc">
<title/>
</links>

</page>
