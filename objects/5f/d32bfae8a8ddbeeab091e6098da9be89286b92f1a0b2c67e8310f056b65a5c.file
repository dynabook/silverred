<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="task" id="printing-to-file" xml:lang="as">

  <info>
    <link type="guide" xref="printing" group="#last"/>

    <revision pkgversion="3.8" date="2013-03-29" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author copyright">
      <name>একাটেৰিনা গেৰাচিমভা</name>
      <email>kittykat3756@gmail.com</email>
      <years>২০১৩</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Save a document as a PDF, PostScript or SVG file instead of sending
    it to a printer.</desc>
  </info>

  <title>ফাইললৈ প্ৰিন্ট কৰক</title>

  <p>You can choose to print a document to a file instead of sending it to
  print from a printer. Printing to file will create a <sys>PDF</sys>, a
  <sys>PostScript</sys> or a <sys>SVG</sys> file that contains the document.
  This can be useful if you want to transfer the document to another machine
  or to share it with someone.</p>

  <steps>
    <title>To print to file:</title>
    <item>
      <p>Open the print dialog by pressing
      <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p><gui style="tab">সাধাৰণ</gui> টেবত <gui>প্ৰিন্টাৰ</gui> ৰ অন্তৰ্গত <gui>ফাইললৈ প্ৰিন্ট কৰক</gui> বাছক।</p>
    </item>
    <item>
      <p>অবিকল্পিত ফাইলনাম আৰু ফাইল ক'ত সংৰক্ষণ হৈছে পৰিবৰ্তন কৰিবলৈ, প্ৰিন্টাৰ নিৰ্বাচনৰ তলত ফাইলনাম বাছক। আপোনাৰ নিৰ্বাচন কৰা সম্পূৰ্ণ হলে <gui style="button">বাছক</gui> ক্লিক কৰক।</p>
    </item>
    <item>
      <p><sys>PDF</sys> is the default file type for the document. If you want
      to use a different <gui>Output format</gui>, select either
      <sys>PostScript</sys> or <sys>SVG</sys>.</p>
    </item>
    <item>
      <p>Choose your other page preferences.</p>
    </item>
    <item>
      <p>ফাইল সংৰক্ষণ কৰিবলৈ <gui style="button">প্ৰিন্ট কৰক</gui> টিপক।</p>
    </item>
  </steps>

</page>
