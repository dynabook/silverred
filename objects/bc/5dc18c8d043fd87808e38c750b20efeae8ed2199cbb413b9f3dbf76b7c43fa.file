<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-setup-default-printer" xml:lang="mr">

  <info>
    <link type="guide" xref="printing#setup"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.7.1" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>२०१३</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>वारंवार वापरण्याजोगी छपाईयंत्र निवडा.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

  <title>पुर्वनिर्धारित प्रिंटर ठरवा</title>

  <p>एकापेक्षा जास्त छपाईयंत्र उपलब्ध असल्यास, पूर्वनिर्धारित छपाईयंत्राची निवड करणे शक्य आहे. वारंवार वापरण्याजोगी छपाईयंत्राची निवड करायला आवडेल.</p>

  <note>
    <p>You need <link xref="user-admin-explain">administrative privileges</link>
    on the system to set the default printer.</p>
  </note>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Printers</gui>.</p>
    </item>
    <item>
      <p><gui>प्रिंटर्स</gui> क्लिक करा.</p>
    </item>
    <item>
      <p>उपलब्ध छपाईयंत्रांच्या सूचीपासून पूर्वनिर्धारित छपाईयंत्र निवडा.</p>
    </item>
    <item>
      <p>Press <gui style="button">Unlock</gui> in the top right corner and
      type in your password when prompted.</p>
    </item>
    <item>
      <p>Select the <gui>Default printer</gui> checkbox.</p>
    </item>
  </steps>

  <p>When you print in an application, the default printer is automatically
  used, unless you choose a different printer.</p>

</page>
