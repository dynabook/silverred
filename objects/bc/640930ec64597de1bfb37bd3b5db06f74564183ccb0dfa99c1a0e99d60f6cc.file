<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="fs-device" xml:lang="cs">

  <info>
    <revision version="0.1" date="2014-01-26" status="review"/>
    <link type="guide" xref="index#filesystems" group="filesystems"/>
    <link type="seealso" xref="fs-info"/>
    
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    
    <credit type="author copyright">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
      <years>2014</years>
    </credit>

    <desc>Každé zařízení odpovídá <em>oddílu</em> na pevném disku.</desc>
  </info>

  <title>Jaká různá zařízení jsou na kartě Souborové systémy?</title>

  <p>Všechna zařízení uvedená na kartě <gui>Souborové systémy</gui> jsou úložné disky (jako pevné disky nebo flash disky USB) nebo diskové oddíly. U každého zařízení můžete vidět celkovou kapacitu, kolik je z celkové kapacity využito a některé technické údaje o <link xref="fs-info">typu souborového systému</link> a <link xref="fs-info">kam je připojen</link>.</p>
  
  <p>Diskový prostor na jednom fyzickém pevném disku může být rozdělen na více částí nazývaných <em>oddíly</em>, z nichž každý může být použit, jako by to byl samostatný disk. Pokud je váš disk rozdělen na oddíly (ať už vámi nebo výrobcem počítače), každý oddíl je v seznamu souborových systémů uveden zvlášť.</p>
  
  <note>
    <p>Spravovat disky a oddíly a zjišťovat si podrobnější informace o discích můžete v aplikaci <app>Disky</app>.</p>
  </note>

</page>
