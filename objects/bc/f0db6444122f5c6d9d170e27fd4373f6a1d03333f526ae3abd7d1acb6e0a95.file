<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" version="1.0 if/1.0" id="shell-introduction" xml:lang="hr">

  <info>
    <link type="guide" xref="shell-overview" group="#first"/>
    <link type="guide" xref="index" group="intro"/>

    <revision pkgversion="3.6.0" date="2012-10-13" status="review"/>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-28" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="review"/>
    <revision pkgversion="3.35.91" date="2020-07-19" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Vizualni pregled vaše radne površine, gornje trake i <gui> Aktivnosti</gui> pregleda.</desc>
  </info>

  <title>Vizualni pregled GNOMA</title>

  <p>GNOME 3 odlikuje potpuno novo korisničko sučelje dizajnirano da vas ne ometa i pomogne vam u obavljanju vašeg posla i zadataka. Kada se prvi puta prijavite, vidjet ćete praznu radnu površinu i gornju traku.</p>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <media type="image" src="figures/shell-top-bar.png" width="600" if:test="!target:mobile">
      <p>Gornja traka GNOME ljuske</p>
    </media>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-top-bar-classic.png" width="500" if:test="!target:mobile">
      <p>Gornja traka GNOME ljuske</p>
    </media>
  </if:when>
</if:choose>

  <p>Gornja traka omogućuje pristup vašim prozorima i aplikacijama, vašim kalendarima i sastancima, <link xref="status-icons">postavkama sustava</link> poput zvuka, umrežavanja i upravljanja energijom. U izborniku stanja na gornjoj traci, možete promijeniti glasnoću zvuka ili svjetlinu zaslona, urediti pojedinosti svoje <gui>bežične</gui> mreže, provjeriti stanje energije baterije, odjaviti se ili zamijeniti korisnika i isključiti svoje računalo.</p>

<links type="section"/>

<!-- TODO: Replace "Activities overview" title for classic mode with something
like "Application windows" by using if:when and if:else ? -->
<section id="activities">
  <title><gui>Aktivnosti</gui> pregled</title>

<media type="image" src="figures/shell-activities-dash.png" height="530" style="floatstart floatleft" if:test="!target:mobile, !platform:gnome-classic">
  <p>Aktivnosti tipka i Pokretač</p>
</media>

  <p if:test="!platform:gnome-classic">Kako bi pristupili svojim prozorima i aplikacijama, kliknite <gui>Aktivnosti</gui> tipku, ili jednostavno pomaknite svoj pokazivač miša u gornji lijevi kut. Isto tako možete pritisnuti <key xref="keyboard-key-super">Super</key> tipku na vašoj tipkovnici. Možete vidjeti pregled svojih prozora i aplikacija. Jednostavno možete započeti upisivanjem pretragu vaših aplikacija, datoteka, mapa i weba.</p>

  <p if:test="platform:gnome-classic">Kako bi pristupili svojim prozorima i aplikacijama, kliknite na tipku u dnu lijeve strane zaslona u prikazu prozora. Isto tako možete pritisnuti <key xref="keyboard-key-super">Super</key> tipku kako bi vidjeli pregled minijatura svih prozora na trenutnom radnom prostoru.</p>

  <p if:test="!platform:gnome-classic">Lijevo od pregleda, nalazi se <em>pokretač</em>. Pokretač vam pokazuje vaše omiljene i pokrenute aplikacije. Kliknite na jednu ikonu u pokretaču za otvaranje odabrane aplikacije; ako je aplikacija već pokrenuta, imati će malu točku ispod ikone. Klik na ikonu će uzdignuti najkorišteniji prozor. Možete povlačiti ikonu po pregledu ili na bilo koji radni prostor s desne strane.</p>

  <p if:test="!platform:gnome-classic">Desni klik na ikonu prikazuje izbornik koji vam omogućuje odabir bilo kojeg otvorenog prozora pokrenute aplikacije ili otvaranje novog prozora. Možete kliknuti na ikonu dok držite pritisnutu tipku <key>Ctrl</key> za otvaranje novog prozora.</p>

  <p if:test="!platform:gnome-classic">Kada pokrenete pregled, početno ćete biti u pregledu prozora. U njemu su prikazane žive minijature svih prozora na trenutnom radnom prostoru.</p>

  <p if:test="!platform:gnome-classic">Klikom na tipku mreže u dnu pokretača prikazat će se pregled aplikacija. Tu su prikazane sve aplikacije instalirane na vašem računalu. Kliknite na odabranu aplikaciju kako bi ju pokrenuli ili povucite aplikaciju na pregled ili na minijaturu radnog prostora. Možete povući aplikaciju na pokretač kako bi ju dodali u omiljene. Vaše omiljene aplikacije ostaju u pokretaču čak i kada nisu pokrenute tako da im možete brže pristupiti.</p>

  <list style="compact">
    <item>
      <p><link xref="shell-apps-open">Saznajte više o pokretanju aplikacija.</link></p>
    </item>
    <item>
      <p><link xref="shell-windows">Saznajte više o prozorima i radnim prostorima.</link></p>
    </item>
  </list>

</section>

<section id="appmenu">
  <title>Izbornik aplikacije</title>
  <if:choose>
    <if:when test="!platform:gnome-classic">
      <media type="image" src="figures/shell-appmenu-shell.png" width="250" style="floatend floatright" if:test="!target:mobile">
        <p>Izbornik aplikacije <app>Terminal</app></p>
      </media>
      <p>Izbornik aplikacije, smješten pokraj <gui>Aktivnosti</gui> tipke, prikazuje naziv aktivne aplikacije uz njenu ikonu i omogućuje brz pristup prozorima i pojedinostima aplikacije, kao i mogućnost njihova zatvaranja.</p>
    </if:when>
    <!-- TODO: check how the app menu removal affects classic mode -->
    <if:when test="platform:gnome-classic">
      <media type="image" src="figures/shell-appmenu-classic.png" width="250" style="floatend floatright" if:test="!target:mobile">
        <p>Izbornik aplikacije <app>Terminal</app></p>
      </media>
      <p>Izbornik aplikacije, smješten pokraj <gui>Aktivnosti</gui> i <gui>Prečaci</gui> izbornika, prikazuje naziv aktivne aplikacije uz njenu ikonu i omogućuje brz pristup osobitostima ili priručniku aplikacije. Stavke dostupne u izborniku aplikacije mogu se razlikovati ovisno o aplikaciji.</p>
    </if:when>
  </if:choose>

</section>

<section id="clock">
  <title>Sat, kalendar i sastanci</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <media type="image" src="figures/shell-appts.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Sat, kalendar i sastanci i obavijesti</p>
    </media>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-appts-classic.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Clock, calendar, and appointments</p>
    </media>
  </if:when>
</if:choose>

  <p>Kliknite na sat u gornjoj traci kako bi vidjeli trenutni datum, mjesečni kalendar i popis vaših zakazanih sastanaka i novih obavijesti. Kalendar možete otvoriti pritiskom <keyseq><key>Super</key><key>M</key></keyseq> tipke. Možete pristupiti postavkama datume i vremena, otvoriti svoju aplikaciju kalendara izravno iz izbornika.</p>

  <list style="compact">
    <item>
      <p><link xref="clock-calendar">Saznajte više o kalendaru i sastancima.</link></p>
    </item>
    <item>
      <p><link xref="shell-notifications">Saznajte više o obavijestima i popisu obavijesti.</link>\</p>
    </item>
  </list>

</section>


<section id="systemmenu">
  <title>Izbornik sustava</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <media type="image" src="figures/shell-exit.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Korisnički izbornik</p>
    </media>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-exit-classic.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Korisnički izbornik</p>
    </media>
  </if:when>
</if:choose>

  <p>Kliknite na izbornik sustava gore desno u kutu kako bi upravljali vašim postavkama sustava i računalom.</p>

<!-- TODO: Update for 3.36 UI option "Do Not Disturb" in calendar dropdown:

<p>If you set yourself to Unavailable, you won’t be bothered by message popups
at the bottom of your screen. Messages will still be available in the message
tray when you move your mouse to the bottom-right corner. But only urgent
messages will be presented, such as when your battery is critically low.</p>
-->

  <p>Kada napuštate vaše računalo, možete zaključati svoj zaslon kako bi spriječili druge osobe da ga koriste. Možete i brzo zamijeniti korisnika bez potpunog odjavljivanja kako bi nekom drugom omogućili pristup računalu ili suspendirati i isključiti računalo iz izbornika. Ako imate računalo koje podržava okomito i vodoravno zakretanje, možete brzo zakretati zaslon iz izbornika sustava. Ako vaš zaslon ne podržava zakretanje, nećete vidjeti tipku zakretanja.</p>

  <list style="compact">
    <item>
      <p><link xref="shell-exit">Saznajte više o zamijeni korisnika, odjavljivanju i isključivanju vašeg računala.</link></p>
    </item>
  </list>

</section>

<section id="lockscreen">
  <title>Zaključavanje zaslona</title>

  <p>Kada zaključate svoj zaslon ili se automatski zaključa, prikazan je zaslon zaključavanja. Kao dodatak zaštiti vaše radne površine kada niste prisutni u blizini računala, zaslon zaključavanja prikazuje datum i vrijeme. Prikazuje i informacije o vašoj energiji baterije i stanju mreža.</p>

  <list style="compact">
    <item>
      <p><link xref="shell-lockscreen">Saznajte više o zaključavanju zaslona.</link></p>
    </item>
  </list>

</section>

<section id="window-list">
  <title>Popis prozora</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <p>GNOME odlikuje drugačiji pristup prebacivanju prozora od stalno vidljivog popisa prozora u drugim radnim okruženjima. To vam omogućuje da se usredotočite na vaš zadatak bez ometanja.</p>
    <list style="compact">
      <item>
        <p><link xref="shell-windows-switching">Saznajte više o prebacivanju prozora.</link></p>
      </item>
    </list>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-window-list-classic.png" width="800" if:test="!target:mobile">
      <p>Popis prozora</p>
    </media>
    <p>The window list at the bottom of the screen provides access to all your
    open windows and applications and lets you quickly minimize and restore
    them.</p>
    <p>At the right-hand side of the window list, GNOME displays the four
    workspaces. To switch to a different workspace, select the workspace you
    want to use.</p>
  </if:when>
</if:choose>

</section>

</page>
