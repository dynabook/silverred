<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-cancel-job" xml:lang="lv">

  <info>
    <link type="guide" xref="printing#problems"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Atceliet rindā esošos drukāšanas darbus un noņemiet tos no saraksta.</desc>
  </info>

  <title>Atcelt, pauzēt vai atlaist drukāšanas darbus</title>

  <p>Printera iestatījumos jūs varat atcelt rindā esošos drukāšanas darbus un noņemt tos no saraksta.</p>

  <section id="cancel-print-job">
    <title>Atcelt drukāšanas darbus</title>

  <p>Ja nejauši esat sākuši drukāšanas dokumentus, jūs varat atcelt drukāšanu, lai neizniekotu tinti vai papīru.</p>

  <steps>
    <title>Kā atcelt drukāšanas darbu:</title>
    <item>
      <p>Atveriet <gui xref="shell-introduction#activities">Aktivitāšu</gui> pārskatu un sāciet rakstīt <gui>Printeri</gui>.</p>
    </item>
    <item>
      <p>Spiediet <gui>Printeri</gui>, lai atvērtu paneli.</p>
    </item>
    <item>
      <p>Spiediet pogu <gui>Rādīt darbus</gui> dialoglodziņa <gui>Printeri</gui> labajā pusē.</p>
    </item>
    <item>
      <p>Atceliet drukāšanas darbu, spiežot apturēšanas pogu.</p>
    </item>
  </steps>

  <p>Ja tas neatceļ drukāšanas darbus tā kā jūs gaidījāt, mēģiniet turēt <gui>atcelšanas</gui> pogu uz sava printera.</p>

  <p>Kā pēdējo līdzekli, it īpaši, ja jums ir liels drukas darbs ar daudz lapām, kuras nevar atcelt, izņemiet papīru no printera papīra padeves. Printerim vajadzētu saprast, ka nav papīra un pārtraukt drukāšanu. Pēc tam jūs varat mēģināt atcelt drukāšanas darbus atkārtoti, vai mēģiniet izslēgt un ieslēgt printeri.</p>

  <note style="warning">
    <p>Esiet uzmanīgi, lai nesabojātu printeri, kad izņemat papīru, lai gan — ja jums ir jāpielieto spēks, lai izņemtu papīru, iespējams, labāk vienkārši atstāt to tur, kur tas ir.</p>
  </note>

  </section>

  <section id="pause-release-print-job">
    <title>Pauzēt un atlaist drukāšanas darbus</title>

  <p>Ja vēlaties pauzēt vai atlaist drukāšanas darbu, jūs varat to izdarīt, printera iestatījumos ejot uz darbu dialoglodziņu un spiežot atbilstošo pogu.</p>

  <steps>
    <item>
      <p>Atveriet <gui xref="shell-introduction#activities">Aktivitāšu</gui> pārskatu un sāciet rakstīt <gui>Printeri</gui>.</p>
    </item>
    <item>
      <p>Spiediet <gui>Printeri</gui>, lai atvērtu paneli.</p>
    </item>
    <item>
      <p>Spiediet pogu <gui>Rādīt darbus</gui> dialoglodziņa <gui>Printeri</gui> labajā pusē un vai nu pauzēt vai atlaist drukāšanas darbu, atkarībā no jūsu vajadzībām.</p>
    </item>
  </steps>

  </section>

</page>
