<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-multi-monitor" xml:lang="zh-CN">

  <info>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.28" date="2018-07-22" status="review"/>
    <revision pkgversion="3.33" date="2019-07-21" status="candidate"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>将手写板映射到指定的显示器上。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>选择显示器</title>

<steps>
  <item>
    <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
    overview and start typing <gui>Wacom Tablet</gui>.</p>
  </item>
  <item>
    <p>Click on <gui>Wacom Tablet</gui> to open the panel.</p>
  </item>
  <item>
    <p>Click the <gui>Tablet</gui> button in the header bar.</p>
    <note style="tip"><p>If no tablet is detected, you’ll be asked to
    <gui>Please plug in or turn on your Wacom tablet</gui>. Click the
    <gui>Bluetooth Settings</gui> link to connect a wireless tablet.</p></note>
  </item>
  <item><p>点击<gui>映射到显示器…</gui></p></item>
  <item><p>选中<gui>映射到单个显示器</gui>。</p></item>
  <item><p>在<gui>输出</gui>的旁边，选择您想让手写板连接的显示器。</p>
     <note style="tip"><p>配置好找显示器才能选中。</p></note>
  </item>
  <item>
    <p>Switch the <gui>Keep aspect ratio (letterbox)</gui> switch to on to
    match the drawing area of the tablet to the proportions of the monitor.
      This setting, also called <em>force proportions</em>,
      “letterboxes” the drawing area on a tablet to correspond more
      directly to a display. For example, a 4∶3 tablet would be mapped so that
      the drawing area would correspond to a widescreen display.</p>
  </item>
  <item><p>点<gui>关闭</gui>。</p></item>
</steps>

</page>
