<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="get-involved" xml:lang="es">

  <info>
    <link type="guide" xref="more-help"/>
    <desc>Cómo y dónde informar de un error en estos temas de ayuda.</desc>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>
  <title>Participar para mejorar esta guía</title>

  <section id="submit-issue">

   <title>Informar de un error</title>

   <p>Esta documentación la ha creado por una comunidad de voluntarios. Si quiere participar es bienvenido. Si encuentra un problema con estas páginas de ayuda (errores ortográficos, instrucciones incorrectas, o temas que se deberían cubrir pero no están), puede enviar un <em>informe de error</em>. Para informar de un error, vaya a <link href="https://gitlab.gnome.org/GNOME/gnome-user-docs/issues">gestor de incidencias</link>.</p>

   <p>Debe registrarse para abrir un informe de error y recibir actualizaciones por correo electrónico acerca de su estado. Pulse el botón <gui><link href="https://gitlab.gnome.org/users/sign_in">Iniciar sesión / Registrarse</link></gui> para registrar una nueva cuenta.</p>

   <p>Una vez que tenga una cuenta, inicie sesión y vuelva al <link href="https://gitlab.gnome.org/GNOME/gnome-user-docs/issues">gestor de incidencias de la documentación</link> y pulse en <gui><link href="https://gitlab.gnome.org/GNOME/gnome-user-docs/issues/new">New issue</link></gui>. Antes de enviar una incidencia nueva <link href="https://gitlab.gnome.org/GNOME/gnome-user-docs/issues">examine</link> si el error se parece a algún informe que ya exista.</p>

   <p>Antes de informar de un error, elija la etiqueta adecuada en el menú <gui>Labels</gui>. Si va a informar de un erro en esta documentación, debe elegir la etiqueta <gui>gnome-help</gui>. Si no está seguro de a qué componente pertenece el error, no elija ninguno.</p>

   <p>Si está solicitando ayuda acerca de un tema que cree que no está cubierto, seleccione la etiqueta <gui>Feature</gui>. Rellene las secciones «Title» y «Description» y pulse <gui>Submit issue</gui>.</p>

   <p>Su informe obtendrá un número de ID, y su estado se actualizará a medida que se trata. Gracias por ayudar a mejorar el proyecto Ayuda de GNOME.</p>

   </section>

   <section id="contact-us">
   <title>Contactar</title>

   <p>Puede enviar un <link href="mailto:gnome-doc-list@gnome.org">correo-e</link> a la lista de correo GNOME docs para aprender como colaborar con el equipo de documentación.</p>

   </section>
</page>
