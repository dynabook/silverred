<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-fingerprint" xml:lang="gl">

  <info>
    <link type="guide" xref="hardware-auth"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-03" status="review"/>
    <revision pkgversion="3.12" date="2014-06-16" status="final"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="final"/>

    <credit type="author">
      <name>Proxecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
      <years>2011</years>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Pode iniciar sesión no seu sistema usando un lector de pegadas dixitais compatíbel no lugar de escribir o seu contrasinal.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2020</mal:years>
    </mal:credit>
  </info>

  <title>Iniciar sesión con unha pegada dixital</title>

  <p>Se o seu sistema ten un lector de pegadas dixitais, pode gravar a súa pegada e usala para iniciar sesión.</p>

<section id="record">
  <title>Rexistrar unha pegada dixital</title>

  <p>Antes de iniciar sesión coa súa pegada, debe gravala para que o sistema lle poida identificar.</p>

  <note style="tip">
    <p>Se o seu dedo está demasiado seco, pode que teña dificultade para rexistrar a súa pegada dixital. Se isto acontece, humedeza lixeiramente o dedo, séqueo cun pano limpo e sen pelusa, e volva a tentalo.</p>
  </note>

  <p>You need <link xref="user-admin-explain">administrator privileges</link>
  to edit user accounts other than your own.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Users</gui>.</p>
    </item>
    <item>
      <p>Prema en <gui>Usuarios</gui> para abrir o panel.</p>
    </item>
    <item>
      <p>Press on <gui>Disabled</gui>, next to <gui>Fingerprint Login</gui> to
      add a fingerprint for the selected account. If you are adding the
      fingerprint for a different user, you will first need to
      <gui>Unlock</gui> the panel.</p>
    </item>
    <item>
      <p>Seleccione o dedo que quere usar para a pegada dixital, logo prema <gui style="button">Seguinte</gui>.</p>
    </item>
    <item>
      <p>Follow the instructions in the dialog and swipe your finger at a
      <em>moderate speed</em> over your fingerprint reader. Once the computer
      has a good record of your fingerprint, you will see a <gui>Done!</gui>
      message.</p>
    </item>
    <item>
      <p>Select <gui>Next</gui>. You will see a confirmation message that
      your fingerprint was saved successfully. Select <gui>Close</gui> to
      finish.</p>
    </item>
  </steps>

</section>

<section id="verify">
  <title>Comprobar que a súa pegada dixital funciona</title>

  <p>Agora comprobe que funciona o seu novo inicio de sesión con pegada dixital. Se rexistra unha pegada dixital, aínda ten a opción de iniciar sesión co seu contrasinal.</p>

  <steps>
    <item>
      <p>Garde calquera traballo aberto, e logo <link xref="shell-exit">peche a sesión</link>.</p>
    </item>
    <item>
      <p>Na pantalla de inicio de sesión, seleccione o seu nome de usuario da lista. Aparecerá o formulario de entrada de contrasinal.</p>
    </item>
    <item>
      <p>No lugar de escribir o seu contrasinal, debería poder deslizar os seus dedos no lector de pegadas dixistais.</p>
    </item>
  </steps>

</section>

</page>
