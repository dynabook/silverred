<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting-initial-check" xml:lang="gl">

  <info>
    <link type="next" xref="net-wireless-troubleshooting-hardware-info"/>
    <link type="guide" xref="net-wireless-troubleshooting"/>

    <revision pkgversion="3.4.0" date="2012-03-05" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <credit type="author">
      <name>Contribuíntes da wiki de documentación de Ubuntu</name>
    </credit>
    <credit type="author">
      <name>Proxecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Asegúrese de que a configuración básica da rede é correcta e prepárese para os seguintes pasos na resolución de problemas.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2020</mal:years>
    </mal:credit>
  </info>

  <title>Resolución de problemas en redes sen fíos</title>
  <subtitle>Levar a cabo unha comprobación inicial da conexión</subtitle>

  <p>Neste paso probará algunha información básica sobre a súa conexión de rede sen fíos. Isto faise para asegurarse que o seu problema de rede non é debido a un problema relativamente simple, como unha conexión de rede apagada, e para prepararse para os seguintes pasos na resolución de problemas.</p>

  <steps>
    <item>
      <p>Asegúrese que o seu portátil non está conectado a unha conexión de Internet <em>con fíos</em>.</p>
    </item>
    <item>
      <p>Se ten un adaptador sen fíos externo (como un adaptador USB, ou unha tarxeta PCMCIA que se conecte ao seu portátil), asegúrese que está firmemente conectada ao porto correcto do seu computador.</p>
    </item>
    <item>
      <p>Se a súa conexión de rede está <em>dentro</em> do seu computador, asegúrese que o interruptor de rede está acendido (se ten un). Os portátiles normalmente teñen interruptores para a rede sen fíos que pode trocar premendo unha combinación de teclas no teclado.</p>
    </item>
    <item>
      <p>Abra o <gui xref="shell-introduction#systemmenu">menú do sistema</gui> desde a parte dereita da barra sueprior e seleccione a rede Wifi, logo seleccione <gui>Preferencias da wifi</gui>. Asegúrese que o <gui>Wifi</gui> está activado. Tamén debería comprobar que <link xref="net-wireless-airplane">o modo avión</link> <em>non</em> está activado.</p>
    </item>
    <item>
      <p>Abra o Terminal, escriba <cmd>nmcli device</cmd> e prema <key>Intro</key>.</p>
      <p>This will display information about your network interfaces and
      connection status. Look down the list of information and see if there is
      an item related to the wireless network adapter. If the state is
      <code>connected</code>, it means that the adapter is working and connected
      to your wireless router.</p>
    </item>
  </steps>

  <p>Se se conectou ao seu enrutador sen fíos, pero aínda así non ten acceso a internet, o seu enrutador podería non estar configurado conrrectamente, ou o seu Fornecedor de servizos de Internet (ISP) podería estar experimentando problemas técnicos. Revise as guías de configuración do seu enrutador e ISP para asegurarse de que as configuracións son as correctas, ou contacte coa asistencia técnica do seu ISP.</p>

  <p>Se a información desde <cmd>nmcli device</cmd> non indica que estea conectado á rede, prema <gui>Seguinte</gui> para continuar coa seguinte parte da guía de resolución de problemas.</p>

</page>
