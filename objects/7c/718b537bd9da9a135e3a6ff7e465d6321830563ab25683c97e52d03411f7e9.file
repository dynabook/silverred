<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sound-usespeakers" xml:lang="id">

  <info>
    <link type="guide" xref="media#sound"/>
    <link type="seealso" xref="sound-usemic"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Connect speakers or headphones and select a default audio output
    device.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

  <title>Gunakan speaker atau headphone lain</title>

  <p>You can use external speakers or headphones with your computer. Speakers
  usually either connect using a circular TRS (<em>tip, ring, sleeve</em>) plug
  or a USB.</p>

  <p>If your speakers or headphones have a TRS plug, plug it into the
  appropriate socket on your computer. Most computers have two sockets: one for
  microphones and one for speakers. This socket is usually light green in color
  or is accompanied by a picture of headphones. Speakers or headphones
  plugged into a TRS socket are usually used by default. If not, see the
  instructions below for selecting the default device.</p>

  <p>Some computers support multi-channel output for surround sound. This
  usually uses multiple TRS jacks, which are often color-coded. If you are
  unsure which plugs go in which sockets, you can test the sound output in the
  sound settings.</p>

  <p>If you have USB speakers or headphones, or analog headphones plugged into
  a USB sound card, plug them into any USB port. USB speakers act as separate
  audio devices, and you may have to specify which speakers to use by
  default.</p>

  <steps>
    <title>Pilih suatu perangkat masukan audio baku</title>
    <item>
      <p>Buka ringkasan <gui xref="shell-introduction#activities">Aktivitas</gui> dan mulai mengetik <gui>Suara</gui>.</p>
    </item>
    <item>
      <p>Klik pada <gui>Suara</gui> untuk membuka panel.</p>
    </item>
    <item>
      <p>Dalam tab <gui>Keluaran</gui>, pilih perangkat yang ingin Anda pakai.</p>
    </item>
  </steps>

  <p>Use the <gui style="button">Test Speakers</gui> button to check that all
  speakers are working and are connected to the correct socket.</p>

</page>
