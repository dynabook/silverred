<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-booklet-singlesided" xml:lang="ca">

  <info>
    <link type="guide" xref="printing-booklet"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="candidate"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Impressió d'un llibret d'un PDF utilitzant una impressora d'una cara.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

  <title>Imprimir un llibret en una impressora d'una cara</title>

  <note>
    <p>Aquestes instruccions són per a imprimir un llibret des d'un document PDF.</p>
    <p>Si voleu imprimir un llibret des d'un document <app>LibreOffice</app>, primer exporteu-lo a un PDF triant a <guiseq><gui>Fitxer</gui><gui>Exporta com a PDF…</gui></guiseq>. El document ha de tenir un nombre múltiple de 4 pàgines (4, 8, 12, 16,...). És possible que hàgiu d'afegir fins a 3 pàgines en blanc.</p>
  </note>

  <p>Imprimir:</p>

  <steps>
    <item>
      <p>Obriu el diàleg d'impressió. Normalment, es pot fer a través del menú d'<gui style="menuitem">Imprimeix</gui> o utilitzant la drecera de teclat <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>Feu clic al botó <gui>Propietats…</gui></p>
      <p>A la llista desplegable <gui>Orientació</gui>, assegureu-vos que està seleccionat a <gui>Apaïsat</gui>.</p>
      <p>Feu clic a <gui>D'acord</gui> per a tornar al menú d'impressió.</p>
    </item>
    <item>
      <p>A <gui>Rang i còpies</gui>, seleccioneu <gui>Pàgines</gui>.</p>
      <p>Escriviu el nombre de pàgines en aquest ordre (n és el nombre total de pàgines i múltiple de 4):</p>
      <p>n, 1, 2, n-1, n-2, 3, 4, n-3, n-4, 5, 6, n-5, n-6, 7, 8, n-7, n-8, 9, 10, n-9, n-10, 11, 12, n-11…</p>
      <p>... fins que hàgiu escrit totes les pàgines.</p>
    <note>
      <p>Exemples:</p>
      <p>Llibret de 4 pàgines: Tipus <input>4,1,2,3</input></p>
      <p>Llibret de 8 pàgines: Tipus <input>8,1,2,7,6,3,4,5</input></p>
      <p>Llibret de 12 pàgines: Tipus <input>12,1,2,11,10,3,4,9,8,5,6,7</input></p>
      <p>Llibret de 16 pàgines: Tipus <input>16,1,2,15,14,3,4,13,12,5,6,11,10,7,8,9</input></p>
      <p>Llibret de 20 pàgines: Tipus <input>20,1,2,19,18,3,4,17,16,5,6,15,14,7,8,13,12,9,10,11</input></p>
     </note>
    </item>
    <item>
      <p>Trieu la pestanya <gui>Disseny de pàgina</gui>.</p>
      <p>A <gui>Disseny</gui>, seleccioneu <gui>Fulletó</gui>.</p>
      <p>A <gui>Cares de la pàgina</gui>, a la llista desplegable <gui>Incloure</gui>, seleccioneu <gui>Cares frontals / pàgines de la dreta</gui>.</p>
    </item>
    <item>
      <p>Feu clic a <gui>Imprimeix</gui>.</p>
    </item>
    <item>
      <p>Quan s'hagin imprès totes les pàgines, gireu les pàgines i torneu-les a col·locar a la impressora.</p>
    </item>
    <item>
      <p>Obriu el diàleg d'impressió. Normalment, es pot fer a través del menú d'<gui style="menuitem">Imprimeix</gui> o utilitzant la drecera de teclat <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>Trieu la pestanya <gui>Disseny de pàgina</gui>.</p>
      <p>A <gui>Cares de la pàgina</gui>, a la llista desplegable <gui>Incloure</gui>, seleccioneu <gui>Cares de darrere / pàgines de l'esquerra</gui>.</p>
    </item>
    <item>
      <p>Feu clic a <gui>Imprimeix</gui>.</p>
    </item>
  </steps>

</page>
