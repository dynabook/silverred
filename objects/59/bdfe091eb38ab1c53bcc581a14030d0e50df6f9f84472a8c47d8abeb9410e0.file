<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="lockdown-single-app-mode" xml:lang="cs">

  <info>
    <link type="guide" xref="user-settings#lockdown"/>
    <link type="guide" xref="sundry#session"/>
    <link type="seealso" xref="lockdown-printing"/>
    <link type="seealso" xref="lockdown-file-saving"/>
    <link type="seealso" xref="lockdown-repartitioning"/>
    <link type="seealso" xref="lockdown-command-line"/>
    <link type="seealso" xref="login-automatic"/>
    <link type="seealso" xref="session-custom"/>
    <link type="seealso" xref="session-user"/>

    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>mclasen@redhat.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
        <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
       
    <desc>Jak nastavit kioskový režim s jedinou aplikací.</desc>
  </info>

  <title>Nastavení režimu s jedinou aplikací</title>

  <p>Režim se jedinou aplikací je upravený GNOME Shell, který funguje jako interaktivní kiosek. Administrátor zablokuje některá chování, aby omezil uživateli v uživatelském prostředí některé činnosti a nasměroval jej jen na vybrané funkce.</p>

  <p>Režim s jedinou aplikací můžete použít pro širokou škálu funkcí z různých oblastní nasazení (od komunikací po zábavu a výuku) a využívat jej jako bezúdržbovou stanici, řízení událostí, registrační místo apod.</p>

  <steps>
  <title>Nastavení režimu s jedinou aplikací</title>
    <item>
      <p>Uzamkněte nastavení, aby se zabránilo tisku, přístupu do terminálu atd.</p>
      <list type="disc">
        <item><p><link xref="lockdown-command-line"/></p></item>
        <item><p><link xref="lockdown-printing"/></p></item>
        <item><p><link xref="lockdown-file-saving"/></p></item>
        <item><p><link xref="lockdown-repartitioning"/></p></item>
      </list>
    </item>
    <item>
      <p>Nastavte pro uživatele automatické přihlašování v souboru <file>/etc/gdm/custom.conf</file>.</p>
      <p>Další podrobnosti viz <link xref="login-automatic"/>.</p>
    </item>
    <item>
      <p>Vytvořte nového uživatele se jméno podle zaběhnutých zvyklostí (bez mezer a speciálních znaků, nezačínající číslicí nebo pomlčkou). Také zajistěte, že se jméno bude vztahovat k příslušnému názvu, jako je třeba související sezení. Dobrým příkladem je třeba <em>kiosk-user</em>.</p>
    </item>
    <item>
      <p>Vytvořte sezení se názvem odpovídajícím uživatelskému jménu (například pro uživatele <em>kiosk-user</em>, navrženého výše, je vhodným názvem <em>kiosk</em>. Konkrétně se to provede tak, že se vytvoří soubor <file>/usr/share/xsessions/<var>kiosk</var>.desktop</file> a v něm se řádek <code>Exec</code> nastaví následovně:</p>
<code>
Exec=gnome-session --session kiosk
</code>
    </item>
    <item>
      <p>Nastavte uživateli <em>kiosk-user</em> výchozí sezení přidáním následujícího řádku do souboru <file>/var/lib/AccountsService/users/<var>kiosk-user</var></file></p>
<code>
XSession=kiosk
</code>
    </item>
    <item>
      <p>Nadefinujte sezení <em>kiosk</em> pomocí vlastní definice sezení obsahující následující řádek:</p>
<code>
RequiredComponents=kiosk-app;gnome-settings-daemon;kiosk-shell;
</code>
      <p>Tím se vytvoří sezení, které spustí tři programy: <sys>kiosk-app</sys> (ukázková aplikace), <sys>gnome-setting-daemon</sys> (standardní součást sezení GNOME) a <sys>kiosk-shell</sys> (což je upravené verze GNOME Shell).</p>
    </item>
    <item>
      <p>Pro <sys>kiosk-shell</sys> vytvořte soubor <file>/usr/share/applications/kiosk-shell.desktop</file>, který bude obsahovat následující řádky:</p>
<code>
[Desktop Entry]
Exec=gnome-shell --mode=kiosk
</code>
    </item>
    <item>
      <p>Vytvořte definici režimu <file>/usr/share/gnome-shell/modes/kiosk.json</file>. Jedná se o ukázkový soubor JSON, který definuje dostupné uživatelské rozhraní <file>gnome-shell</file>.</p>
      <p>Jako výchozí bod vám mohou posloužit příklady <file>/usr/share/gnome-shell/modes/classic.json</file> a <file>/usr/share/gnome-shell/modes/initial-setup.json</file>.</p>
    </item>
  </steps>

</page>
