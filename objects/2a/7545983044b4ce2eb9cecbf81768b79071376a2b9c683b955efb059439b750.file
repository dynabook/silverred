<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-stickykeys" xml:lang="pt">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bulh</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hilh</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Clicar combinações de teclas de forma seguida em vez de ter que manter todas as teclas clicadas ao mesmo tempo.</desc>
  </info>

  <title>Ativar as teclas persistentes</title>

  <p>As <em>teclas persistentes</em> permitem-lhe usar combinações de teclas clicándolas tecla a tecla, em vez de ter que manter clicadas todas as teclas ao mesmo tempo. Por exemplo, a combinação <keyseq><key xref="keyboard-key-super">Super</key><key>Tab</key></keyseq> serve para passar duma janela a outra. Sem as teclas persistentes ativadas tendria que manter clicadas ambas teclas ao mesmo tempo; com as teclas persistentes, pode carregar a tecla <key>Super</key>, soltá-la e depois carregar <key>Tab</key> para obter o mesmo resultado.</p>

  <p>Pode querer ativar as teclas persistentes se encontra dificuldade para manter clicadas várias teclas ao mesmo tempo.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Universal Access</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Carregue <gui>Assistente de editável</gui> na seção <gui>Digitar</gui>.</p>
    </item>
    <item>
      <p>Switch the <gui>Sticky Keys</gui> switch to on.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Ativar e desativar rapidamente as teclas lentas</title>
    <p>Em <gui>Ativar por teclado</gui>, selecione <gui>Ativar as caraterísticas de acessibilidade desde o teclado</gui> para ativar e desativar as teclas persistentes desde o teclado. Quando esta opção está selecionada pode carregar a tecla <key>Shift</key> cinco vezes seguidas para ativar ou desativar as teclas persistentes.</p>
    <p>Também pode ativar e desativar as teclas lentas clicando o <link xref="a11y-icon">ícone de acesso universal</link> na barra superior e mudando as <gui>Teclas lentas</gui>. O ícone de acessibilidade está visível quando se ativaram uma ou mas opções no painel de <gui>Acesso universal</gui>.</p>
  </note>

  <p>Se clica duas teclas ao mesmo tempo, pode fazer que as teclas persistentes se desativem temporariamente para introduzir um atalho de teclado da maneira habitual.</p>

  <p>Por exemplo, se ativa as teclas persistentes mas clica <key>Super</key> e <key>Tab</key> simultáneamente, as teclas persistentes não esperassén a que carregue outra tecla se tenia essa opção ativado. Em mudança, <em>sé podria</em> esperar se só se clica uma tecla. Isto é útil se pode carregar alguns atalhos de teclado simultáneamente (por exemplo, com teclas que estão juntas), mas não outras.</p>

  <p>Selecione <gui>Desativar se clicam-se duas teclas ao mesmo tempo</gui> para ativar isto.</p>

  <p>You can have the computer make a “beep” sound when you start typing a
  keyboard shortcut with sticky keys turned on. This is useful if you want to
  know that sticky keys is expecting a keyboard shortcut to be typed, so the
  next key press will be interpreted as part of a shortcut. Select <gui>Beep
  when a modifier key is pressed</gui> to enable this.</p>

</page>
