<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="look-resolution" xml:lang="es">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="seealso" xref="look-display-fuzzy"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.28" date="2018-07-19" status="review"/>
    <revision pkgversion="3.33" date="2019-07-19" status="candidate"/>
    <revision pkgversion="3.34" date="2019-11-12" status="review"/>

    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Cambiar la resolución de la pantalla y su orientación (rotación).</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Cambiar la resolución o la orientación de la pantalla</title>

  <p>Puede cambiar cómo de grandes (o detalladas), aparecen las cosas en la pantalla cambiando la <em>resolución de pantalla</em>. Puede cambiar la manera en la que se muestran las cosas (por ejemplo, si tiene una pantalla giratoria) cambiando la <em>rotación</em>.</p>

  <steps>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Pantallas</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Pantallas</gui> para abrir el panel.</p>
    </item>
    <item>
      <p>Si tiene varias pantallas y no están replicadas puede tener diferentes configuraciones en cada una. Seleccione una pantalla en el área de previsualización.</p>
    </item>
    <item>
      <p>Seleccione la orientación, la resolución o el escalado y la tasa de refresco..</p>
    </item>
    <item>
      <p>Pulse <gui>Aplicar</gui>. La nueva configuración se aplicará durante 20 segundos antes de revertirse. De esta forma, si no puede ver nada con la nueva configuración, se restaurará automáticamente su antigua configuración. Si está satisfecho con la nueva configuración, pulse en <gui>Mantener cambios</gui>.</p>
    </item>
  </steps>

<section id="resolution">
  <title>Resolución</title>

  <p>La resolución es el número de píxeles (puntos en la pantalla) que se muestran en cada dirección. Cada resolución tiene una <em>proporción de aspecto</em>, que es la proporción entre la altura y la anchura. Las pantallas anchas usan una proporción de aspecto de 16:9, mientras que las tradicionales usan 4:3. Si elige una resolución que no coincide con la proporción de aspecto de su pantalla, se añadirán bandas negras superiores e inferiores en su pantalla para evitar distorsionar la imagen.</p>

  <p>Puede elegir la opción que prefiera en el menú desplegable <gui>Resolución</gui>. Si escoge una resolución que no sea adecuada para su pantalla es posible que la vea <link xref="look-display-fuzzy">borrosa o pixelada</link>.</p>

</section>

<section id="native">
  <title>Resolución nativa</title>

  <p>La <em>resolución nativa</em> de la pantalla de un portátil o monitor LCD es la que mejor funciona: los píxeles en la señal de vídeo se alinearán con precisión con los píxeles en la pantalla. Cuando se requiere que la pantalla muestre otras resoluciones, es necesaria la interpolación para representar los píxeles, causando una pérdida de calidad de imagen.</p>

</section>

<section id="refresh">
  <title>Tasa de refresco</title>

  <p>La tasa de refresco es el número de veces por segundo que se dibuja o actualiza la imagen en la pantalla.</p>
</section>

<section id="scale">
  <title>Escalar</title>

  <p>La configuración del escalado aumenta el tamaño de los objetos mostrados en la pantalla para que coincida con la densidad de la misma, haciendo que sean más fáciles de leer. Elija <gui>100%</gui> o <gui>200%</gui>.</p>

</section>

<section id="orientation">
  <title>Orientación</title>

  <p>En algunos dispositivos puede rotar físicamente la pantalla en varias direcciones. Pulse en <gui>Orientación</gui> en el panel y elija <gui>Horizontal</gui>, <gui>Vertical derecha</gui>, <gui>Vertical izquierda</gui>, u <gui>Horizontal (dada la vuelta)</gui>.</p>

  <note style="tip">
    <p>Si su dispositivo rota la pantalla automáticamente puede bloquear la rotación actual usando el botón <media its:translate="no" type="image" src="figures/rotation-locked-symbolic.svg"><span its:translate="yes">bloquear rotación</span></media> en la parte inferior del <gui xref="shell-introduction#systemmenu">menú del sistema</gui>. Para desbloquear pulse el botón <media its:translate="no" type="image" src="figures/rotation-allowed-symbolic.svg"><span its:translate="yes">desbloquear rotación</span></media></p>
  </note>

</section>

</page>
