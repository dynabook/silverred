<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="tip" version="1.0 if/1.0 ui/1.0" id="shell-keyboard-shortcuts" xml:lang="fr">

  <info>
    <link type="guide" xref="tips"/>
    <link type="guide" xref="keyboard"/>
    <link type="guide" xref="shell-overview#apps"/>
    <link type="seealso" xref="keyboard-key-super"/>

    <revision pkgversion="3.8.0" version="0.4" date="2013-04-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.20" date="2016-08-13" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-27" status="review"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Se déplacer dans le bureau à l'aide du clavier.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Les raccourcis clavier utiles</title>

<p>Cette page fournit un aperçu des raccourcis clavier qui peuvent vous aider à utiliser votre bureau et les applications plus efficacement. Si vous ne pouvez pas utiliser de souris ni d'appareil de pointage, consultez <link xref="keyboard-nav"/> pour savoir comment naviguer dans les interfaces uniquement via le clavier.</p>

<table rules="rows" frame="top bottom" ui:expanded="true">
<title>Déplacement dans le bureau</title>
  <tr xml:id="alt-f1">
    <td><p>
      <keyseq><key>Alt</key><key>F1</key></keyseq> or the</p>
      <p><key xref="keyboard-key-super">Super</key> key
    </p></td>
    <td><p>Alterner entre la vue générale des <gui>Activités</gui> et le bureau. Dans la vue générale, commencez à saisir un mot-clé pour chercher instantanément vos applications, contacts et documents.</p></td>
  </tr>
  <tr xml:id="alt-f2">
    <td><p><keyseq><key>Alt</key><key>F2</key></keyseq></p></td>
    <td><p>Pop up command window (for quickly running commands).</p>
    <p>Use the arrow keys to quickly access previously run commands.</p></td>
  </tr>
  <tr xml:id="super-tab">
    <td><p><keyseq><key>Logo</key><key>Tab</key></keyseq></p></td>
    <td><p><link xref="shell-windows-switching">Changer rapidement de fenêtre</link>. Maintenez la touche <key>Maj</key> enfoncée pour inverser l'ordre de parcours.</p></td>
  </tr>
  <tr xml:id="super-tick">
    <td><p><keyseq><key>Logo</key><key>`</key></keyseq></p></td>
    <td>
      <p>Changer de fenêtre dans une même application, ou depuis l'application sélectionnée après <keyseq><key>Logo</key><key>Tab</key></keyseq>.</p>
      <p>Ce raccourci utilise la touche <key>`</key> des claviers américains pour lesquels la touche <key>`</key> est au-dessus de la touche <key>Tab</key>. Pour tous les autres claviers, le raccourci est <key>Logo</key> plus la touche se trouvant au dessus de <key>Tab</key>.</p>
    </td>
  </tr>
  <tr xml:id="alt-escape">
    <td><p><keyseq><key>Alt</key><key>Échap</key></keyseq></p></td>
    <td>
      <p>Switch between windows in the current workspace. Hold down
      <key>Shift</key> for reverse order.</p>
    </td>
  </tr>
  <tr xml:id="ctrl-alt-tab">
    <!-- To be updated to <key>Tab</key> in the future. -->
    <td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq></p></td>
    <td>
      <p>Give keyboard focus to the top bar. In the <gui>Activities</gui>
      overview, switch keyboard focus between the top bar, dash, windows
      overview, applications list, and search field. Use the arrow keys to
      navigate.</p>
    </td>
  </tr>
  <tr xml:id="super-a">
    <td><p><keyseq><key>Logo</key><key>A</key></keyseq></p></td>
    <td><p>Afficher la liste des applications.</p></td>
  </tr>
  <tr xml:id="super-updown">
    <td>
      <p if:test="!platform:gnome-classic"><keyseq><key>Logo</key><key>Page haut</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>Ctrl</key><key>Alt</key><key>→</key></keyseq></p>
      <p>and</p>
      <p if:test="!platform:gnome-classic"><keyseq><key>Logo</key><key>Page bas</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>Ctrl</key><key>Alt</key><key>←</key></keyseq></p>
    </td>
    <td><p><link xref="shell-workspaces-switch">Basculer entre les espaces de travail</link>.</p></td>
  </tr>
  <tr xml:id="shift-super-updown">
    <td>
      <p if:test="!platform:gnome-classic"><keyseq><key>Shift</key><key>Super</key><key>Page Up</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>Maj</key><key>Ctrl</key><key>Alt</key><key>→</key></keyseq></p>
      <p>and</p>
      <p if:test="!platform:gnome-classic"><keyseq><key>Maj</key><key>Logo</key><key>page bas</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>Maj</key><key>Ctrl</key><key>Alt</key><key>←</key></keyseq></p>
    </td>
    <td><p><link xref="shell-workspaces-movewindow">Déplacer la fenêtre actuelle vers un espace de travail différent</link>.</p></td>
  </tr>
  <tr xml:id="shift-super-left">
    <td><p><keyseq><key>Shift</key><key>Super</key><key>←</key></keyseq></p></td>
    <td><p>Move the current window one monitor to the left.</p></td>
  </tr>
  <tr xml:id="shift-super-right">
    <td><p><keyseq><key>Shift</key><key>Super</key><key>→</key></keyseq></p></td>
    <td><p>Move the current window one monitor to the right.</p></td>
  </tr>
  <tr xml:id="ctrl-alt-Del">
    <td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Suppr</key></keyseq></p></td>
    <td><p><link xref="shell-exit#logout">Log Out</link>.</p></td>
  </tr>
  <tr xml:id="super-l">
    <td><p><keyseq><key>Logo</key><key>L</key></keyseq></p></td>
    <td><p><link xref="shell-exit#lock-screen">Verrouiller l'écran</link>.</p></td>
  </tr>
  <tr xml:id="super-v">
    <td><p><keyseq><key>Super</key><key>V</key></keyseq></p></td>
    <td><p>Show <link xref="shell-notifications#notificationlist">the notification
    list</link>. Press <keyseq><key>Super</key><key>V</key></keyseq> again or
    <key>Esc</key> to close.</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Raccourcis courants de modification</title>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>A</key></keyseq></p></td>
    <td><p>Sélectionner tout le texte ou tous les éléments d'une liste.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>X</key></keyseq></p></td>
    <td><p>Couper (enlever) le texte ou les éléments sélectionnés et les mettre dans le presse-papiers.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>C</key></keyseq></p></td>
    <td><p>Copier le texte ou les éléments sélectionnés dans le presse-papiers.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>V</key></keyseq></p></td>
    <td><p>Coller le contenu du presse-papiers.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>Z</key></keyseq></p></td>
    <td><p>Annuler la dernière action.</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Capture d'écran</title>
  <tr>
    <td><p><key>Impr. écran</key></p></td>
    <td><p><link xref="screen-shot-record#screenshot">Fait une capture d'écran</link>.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>Impr. écran</key></keyseq></p></td>
    <td><p><link xref="screen-shot-record#screenshot">Faire une capture d'écran d'une fenêtre</link>.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Maj</key><key>Impr. écran</key></keyseq></p></td>
    <td><p><link xref="screen-shot-record#screenshot">Faire une capture d'écran d'une zone de la fenêtre</link>. Le pointeur se change en croix. Cliquez et sélectionnez une zone.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Maj</key><key>R</key></keyseq></p></td>
    <td><p><link xref="screen-shot-record#screencast">Start and stop screencast
     recording.</link></p></td>
  </tr>
</table>

</page>
