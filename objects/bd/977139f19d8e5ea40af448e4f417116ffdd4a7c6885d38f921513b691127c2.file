<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="task" version="1.0 ui/1.0" id="keyboard-shortcuts-set" xml:lang="fr">

  <info>
    <link type="guide" xref="keyboard"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-23" status="review"/>
    <revision pkgversion="3.9.92" date="2013-10-11" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.17.90" date="2015-08-30" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-27" status="review"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Julita Inca</name>
      <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Juanjo Marín</name>
      <email>juanj.marin@juntadeandalucia.es</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Définir ou modifier les raccourcis clavier dans les paramètres <gui>Clavier</gui>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Définition de raccourcis clavier</title>

<p>Pour modifier la ou les touches d'un raccourci clavier :</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Devices</gui> in the sidebar.</p>
    </item>
    <item>
      <p>Click <gui>Keyboard</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Click the row for the desired action. The <gui>Set shortcut</gui>
      window will be shown.</p>
    </item>
    <item>
      <p>Hold down the desired key combination, or press <key>Backspace</key> to
      reset, or press <key>Esc</key> to cancel.</p>
    </item>
  </steps>


<section id="defined">
<title>Raccourcis courants prédéfinis</title>
  <p>Un certain nombre de raccourcis pré-configurés et qui peuvent être modifiés sont regroupés dans ces catégories :</p>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Les lanceurs</title>
  <tr>
	<td><p>Dossier personnel</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-folder.svg"> <key>Explorer</key> key symbol</media> or <media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-computer.svg"> <key>Explorer</key> key symbol</media> or <key>Explorer</key></p></td>
  </tr>
  <tr>
	<td><p>Lancer la calculatrice</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-calculator.svg"> <key>Calculator</key> key symbol</media> or <key>Calculator</key></p></td>
  </tr>
  <tr>
	<td><p>Démarrer le client de messagerie</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-mail.svg"> <key>Mail</key> key symbol</media> or <key>Mail</key></p></td>
  </tr>
  <tr>
	<td><p>Démarrer le navigateur d'aide</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Démarrer le navigateur Web</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-world.svg"> <key>WWW</key> key symbol</media> or <media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-home.svg"> <key>WWW</key> key symbol</media> or <key>WWW</key></p></td>
  </tr>
  <tr>
	<td><p>Recherche</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-search.svg"> <key>Search</key> key symbol</media> or <key>Search</key></p></td>
  </tr>
  <tr>
	<td><p>Settings</p></td>
	<td><p><key>Tools</key></p></td>
  </tr>

</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Navigation</title>
  <tr>
	<td><p>Masquer toutes les fenêtres normales</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Basculer vers l'espace de travail au dessus</p></td>
	<td><p><keyseq><key>Logo</key><key>Page haut</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Basculer vers l'espace de travail en dessous</p></td>
	<td><p><keyseq><key>Logo</key><key>Page bas</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move window one monitor down</p></td>
	<td><p><keyseq><key>Shift</key><key xref="keyboard-key-super">Super</key><key>↓</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move window one monitor to the left</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>←</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move window one monitor to the right</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>→</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move window one monitor up</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>↑</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Déplacer la fenêtre d'un espace de travail vers le bas</p></td>
	<td><p><keyseq><key>Maj</key><key>Logo</key><key>page bas</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Déplacer la fenêtre d'un espace de travail vers le haut</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key>
  <key>Page Up</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move window to last workspace</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>End</key></keyseq>
  </p></td>
  </tr>
  <tr>
	<td><p>Déplacer la fenêtre vers l'espace de travail 1</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>Home</key></keyseq>
  </p></td>
  </tr>
  <tr>
	<td><p>Déplacer la fenêtre vers l'espace de travail 2</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Déplacer la fenêtre vers l'espace de travail 3</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Déplacer la fenêtre vers l'espace de travail 4</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Basculer entre les applications</p></td>
	<td><p><keyseq><key>Logo</key><key>Tab</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Basculer les contrôles système</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Changer les contrôles système directement</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Échap</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Switch to last workspace</p></td>
	<td><p><keyseq><key>Super</key><key>End</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Basculer vers l'espace de travail 1</p></td>
	<td><p><keyseq><key>Super</key><key>Home</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Basculer vers l'espace de travail 2</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Basculer vers l'espace de travail 3</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Basculer vers l'espace de travail 4</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
        <td><p>Basculement entre fenêtres</p></td>
        <td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Basculer entre les fenêtres directement</p></td>
	<td><p><keyseq><key>Alt</key><key>Échap</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Basculer entre les fenêtres d'une application directement</p></td>
	<td><p><keyseq><key>Alt</key><key>F6</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Basculer entre les fenêtres d'une application</p></td>
	<td><p>Désactivé</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Captures d'écran</title>
  <tr>
	<td><p>Copier une capture d'écran d'une fenêtre dans le presse-papiers</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Impr. écran</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Copier une capture d'une partie de l'écran dans le presse-papiers</p></td>
	<td><p><keyseq><key>Maj</key><key>Ctrl</key><key>Impr. écran</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Copier une capture d'écran dans le presse-papiers</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Impr. écran</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Record a short screencast</p></td>
        <td><p><keyseq><key>Maj</key><key>Ctrl</key><key>Alt</key><key>R</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Save a screenshot of a window to Pictures</p></td>
	<td><p><keyseq><key>Alt</key><key>Impr. écran</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Save a screenshot of an area to Pictures</p></td>
	<td><p><keyseq><key>Maj</key><key>Impr. écran</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Save a screenshot to Pictures</p></td>
	<td><p><key>Impr. écran</key></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Son et média</title>
  <tr>
	<td><p>Éjecter</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-eject.svg"> <key>Eject</key> key symbol</media> (Eject)</p></td>
  </tr>
  <tr>
	<td><p>Lancer le lecteur multimédia</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-media.svg"> <key>Media</key> key symbol</media> (Audio media)</p></td>
  </tr>
  <tr>
	<td><p>Morceau suivant</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-next.svg"> <key>Next</key> key symbol</media> (Audio next)</p></td>
  </tr>  <tr>
	<td><p>Mettre en pause la lecture</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-pause.svg"> <key>Pause</key> key symbol</media> (Audio pause)</p></td>
  </tr>
  <tr>
	<td><p>Lire (ou lecture/pause)</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-play.svg"> <key>Play</key> key symbol</media> (Audio play)</p></td>
  </tr>
  <tr>
	<td><p>Morceau précédent</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-previous.svg"> <key>Previous</key> key symbol</media> (Audio previous)</p></td>
  </tr>
  <tr>
	<td><p>Stopper la lecture</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-stop.svg"> <key>Stop</key> key symbol</media> (Audio stop)</p></td>
  </tr>
  <tr>
	<td><p>Baisser le volume</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-voldown.svg"> <key>Volume Down</key> key symbol</media> (Audio lower volume)</p></td>
  </tr>
  <tr>
	<td><p>Couper le volume</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-mute.svg"> <key>Mute</key> key symbol</media> (Audio mute)</p></td>
  </tr>
  <tr>
	<td><p>Augmenter le volume</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-volup.svg"> <key>Volume Up</key> key symbol</media> (Audio raise volume)</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Système</title>
  <tr>
        <td><p>Focaliser sur la notification active</p></td>
        <td><p><keyseq><key>logo</key><key>N</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Verrouillage de l'écran</p></td>
	<td><p><keyseq><key>Logo</key><key>L</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Se déconnecter</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Suppr</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Ouvrir le menu de l'application</p></td>
        <td><p><keyseq><key>logo</key><key>F10</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Restore the keyboard shortcuts</p></td>
        <td><p><keyseq><key>Super</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Afficher toutes les applications</p></td>
        <td><p><keyseq><key>Logo</key><key>A</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Afficher la vue d'ensemble des activités</p></td>
	<td><p><keyseq><key>Alt</key><key>F1</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Show the notification list</p></td>
	<td><p><keyseq><key>Super</key><key>V</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Show the overview</p></td>
	<td><p><keyseq><key>Super</key><key>S</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Afficher la fenêtre de saisie de commande</p></td>
	<td><p><keyseq><key>Alt</key><key>F2</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Saisie</title>
  <tr>
  <td><p>Passer à la source d'entrées suivante</p></td>
  <td><p><keyseq><key>Logo</key><key>Barre d'espace</key></keyseq></p></td>
  </tr>

  <tr>
  <td><p>Passer à la source d'entrées précédente</p></td>
  <td><p><keyseq><key>Shift</key><key>Super</key><key>Space</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Accès universel</title>
  <tr>
	<td><p>Diminuer la taille du texte</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Activer ou désactiver le contraste élevé</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Augmenter la taille du texte</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Activer ou désactiver le clavier visuel</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Activer ou désactiver le lecteur d'écran</p></td>
	<td><p><keyseq><key>Alt</key><key>Super</key><key>S</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Activer ou désactiver le zoom</p></td>
	<td><p><keyseq><key>Alt</key><key>Logo</key><key>8</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Zoom avant</p></td>
  <td><p><keyseq><key>Alt</key><key>Logo</key><key>=</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Zoom arrière</p></td>
  <td><p><keyseq><key>Alt</key><key>Logo</key><key>-</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Fenêtres</title>
  <tr>
	<td><p>Activer le menu de la fenêtre</p></td>
	<td><p><keyseq><key>Alt</key><key>Barre d'espace</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Fermer la fenêtre</p></td>
	<td><p><keyseq><key>Alt</key><key>F4</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Masquer la fenêtre</p></td>
        <td><p><keyseq><key>Logo</key><key>H</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Placer la fenêtre au-dessous des autres</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Maximiser la fenêtre</p></td>
	<td><p><keyseq><key>Logo</key><key>↑</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Maximiser la fenêtre horizontalement</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Maximiser la fenêtre verticalement</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Déplacer la fenêtre</p></td>
	<td><p><keyseq><key>Alt</key><key>F7</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Placer la fenêtre au-dessus des autres</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Placer la fenêtre au-dessus des autres si elle est recouverte, sinon au-dessous</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Redimensionner la fenêtre</p></td>
	<td><p><keyseq><key>Alt</key><key>F8</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Restaurer la fenêtre</p></td>
        <td><p><keyseq><key>Logo</key><key>↓</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Activer ou désactiver le mode plein écran</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Activer ou désactiver l'état maximisé</p></td>
	<td><p><keyseq><key>Alt</key><key>F10</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Afficher la fenêtre sur tous les espaces de travail ou sur un seul</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
        <td><p>Séparer l'affichage sur la gauche</p></td>
        <td><p><keyseq><key>Logo</key><key>←</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Séparer l'affichage sur la droite</p></td>
        <td><p><keyseq><key>Logo</key><key>→</key></keyseq></p></td>
  </tr>
</table>

</section>

<section id="custom">
<title>Raccourcis personnalisés</title>

  <p>Pour créer vos propres raccourcis clavier dans les paramètres <gui>Clavier</gui> :</p>

  <steps>
    <item>
      <p>Click the <gui style="button">+</gui> button. The <gui>Add Custom
      Shortcut</gui> window will appear.</p>
    </item>
    <item>
      <p>Type a <gui>Name</gui> to identify the shortcut, and a
      <gui>Command</gui> to run an application.
      For example, if you wanted the shortcut to open <app>Rhythmbox</app>, you
      could name it <input>Music</input> and use the <input>rhythmbox</input>
      command.</p>
    </item>
    <item>
      <p>Click the row that was just added. When the
      <gui>Set Custom Shortcut</gui> window opens, hold down the desired
      shortcut key combination.</p>
    </item>
    <item>
      <p>Cliquez sur <gui>Ajouter</gui>.</p>
    </item>
  </steps>

  <p>Le nom de la commande que vous saisissez doit être une commande système valide. Vous pouvez vérifier que la commande fonctionne en ouvrant un Terminal et en la saisissant dedans. Il est possible que la commande ouvrant une application n'aie pas exactement le même nom que l'application elle-même.</p>

  <p>If you want to change the command that is associated with a custom
  keyboard shortcut, click the <em>name</em> of the shortcut. The
  <gui>Set Custom Shortcut</gui> window will appear, and you can edit the
  command.</p>

</section>

</page>
