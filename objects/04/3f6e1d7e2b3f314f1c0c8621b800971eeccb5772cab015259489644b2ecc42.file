<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="session-screenlocks" xml:lang="pt">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="guide" xref="hardware-problems-graphics"/>

    <revision pkgversion="3.8" version="0.3" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-03" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.28" date="2018-07-19" status="review"/>
    <revision pkgversion="3.34" date="2019-11-12" status="review"/>

    <credit type="author">
      <name>Projeto de documentação de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hilh</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Mude o tempo de espera antes de que se bloqueie o ecrã na configuração de <gui>Privacidade</gui>.</desc>
  </info>

  <title>O ecrã bloqueia-se demasiado depressa</title>

  <p>Se deixa a computador por uns minutos, o ecrã bloquear-se-á automaticamente, pelo que terá que introduzir a sua palavra-passe para começar ao utilizar de novo. Isto se faz por razões de segurança (para que ninguém possa estragar seu trabalho se deixa a computador sem vigilância), mas pode ser incómodo se o ecrã se bloqueia com demasiada rapidez.</p>

  <p>Para esperar um período mais longo antes de que o ecrã se bloqueie automaticamente:</p>

  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Atividades</gui> comece a escrever <gui>Privacidade</gui>.</p>
    </item>

    <item>
      <p>Carregue em <gui>Privacidade</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Carregue em <gui>Bloquear ecrã</gui>.</p>
    </item>
    <item>
      <p>Se o <gui>Bloqueio automático do ecrã</gui> está ativado, pode mudar o valor na lista desdobrável <gui>Bloquear o ecrã depois de</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <p>If you don’t ever want the screen to lock itself automatically, switch
    the <gui>Automatic Screen Lock</gui> switch to off.</p>
  </note>

</page>
