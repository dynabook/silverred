<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="keyboard-osk" xml:lang="fi">

  <info>
    <link type="guide" xref="keyboard" group="a11y"/>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>

    <credit type="author">
      <name>Jeremy Bicha</name>
      <email>jbicha@ubuntu.com</email>
    </credit>
    <credit type="author">
      <name>Julita Inca</name>
      <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Käytä virtuaalinäppäimistöä kirjoittaaksesi tekstiä hiiren tai kosketuslevyn avulla.</desc>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

  <title>Käytä virtuaalinäppäimistöä</title>

  <p>Jos tietokoneessasi ei ole näppäimistöä tai et jostain syystä pysty tai halua käyttää näppäimistöä, voit käyttää <em>näyttönäppäimistöä</em> eli virtuaalinäppäimistöä tekstin kirjoittamiseen.</p>

  <note>
    <p>Virtuaalinäppäimistö on automaattisesti käytössä, jos käytät kosketusnäyttöä</p>
  </note>

  <steps>
    <item>
      <p>Avaa <gui xref="shell-introduction#activities">Toiminnot</gui>-yleisnäkymä ja ala kirjoittamaan <gui>Asetukset</gui>.</p>
    </item>
    <item>
      <p>Napsauta <gui>Asetukset</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Universal Access</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Switch on <gui>Screen Keyboard</gui> in the <gui>Typing</gui>
      section.</p>
    </item>
  </steps>

  <p>When you next have the opportunity to type, the on-screen keyboard will
  open at the bottom of the screen.</p>

  <p>Press the <gui style="button">?123</gui> button to enter numbers and
  symbols. More symbols are available if you then press the
  <gui style="button">=/&lt;</gui> button. To return to the alphabet keyboard,
  press the <gui style="button">ABC</gui> button.</p>

  <p>You can press the
  <gui style="button"><media its:translate="no" type="image" src="figures/go-down-symbolic.svg" width="16" height="16"><span its:translate="yes">down</span></media></gui>
  button to hide the keyboard temporarily. The keyboard will show again
  automatically when you next press on something where you can use it.</p>
  <p>Press the
  <gui style="button"><media its:translate="no" type="image" src="figures/emoji-flags-symbolic.svg" width="16" height="16"><span its:translate="yes">flag</span></media></gui>
  button to change your settings for
  <link xref="session-language">Language</link> or
  <link xref="keyboard-layouts">Input Sources</link>.</p>
  <!-- obsolete <p>To make the keyboard show again, open the
  <link xref="shell-notifications">messagetray</link> (by moving your mouse to
  the bottom of the screen), and press the keyboard icon.</p> -->

</page>
