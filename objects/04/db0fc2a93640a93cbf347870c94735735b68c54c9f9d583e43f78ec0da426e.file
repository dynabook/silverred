<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-willnotturnon" xml:lang="fi">

  <info>
    <link type="guide" xref="power#problems"/>
    <link type="guide" xref="hardware-problems-graphics" group="#last"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Irrallaan olevat johdot tai laitteisto-ongelmat ovat mahdollisia syitä.</desc>
    <credit type="author">
      <name>Gnomen dokumentointiprojekti</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

<title>Tietokone ei käynnisty</title>

<p>Se, ettei tietokone käynnisty, voi johtua monesta eri syystä. Seuravaaksi käsitellään yleisimpiä mahdollisia syitä.</p>
	
<section id="nopower">
  <title>Tietokoneen johdot eivät ole kunnolla kiinni tai akku on loppu</title>
  <p>Make sure that the power cables of the computer are firmly plugged in and
  the power outlets are switched on. Make sure that the monitor is plugged in
  and switched on too. If you have a laptop, connect the charging cable (in
  case it has run out of battery). You may also want to check that the battery
  is correctly fitted in place (check the underside of the laptop) if it is
  removable.</p>
</section>

<section id="hardwareproblem">
  <title>Ongelma tietokoneen osan kanssa</title>
  <p>A component of your computer may be broken or malfunctioning. If this is
  the case, you will need to get your computer repaired. Common faults include
  a broken power supply unit, incorrectly-fitted components (such as the
  memory or RAM) and a faulty motherboard.</p>
</section>

<section id="beeps">
  <title>Tietokone piippaa ja sammuu sen jälkeen</title>
  <p>If the computer beeps several times when you turn it on and then turns off
  (or fails to start), it may be indicating that it has detected a problem.
  These beeps are sometimes referred to as <em>beep codes</em>, and the pattern
  of beeps is intended to tell you what the problem with the computer is.
  Different manufacturers use different beep codes, so you will have to consult
  the manual for your computer’s motherboard, or take your computer in for
  repairs.</p>
</section>

<section id="fans">
  <title>Tietokoneen tuulettimet pyörivät, mutta näytöllä ei näy mitään</title>
  <p>Varmista ensin, että näytön johdot on kiinnitetty oikein ja että näyttö on päällä.</p>
  <p>Tämä voi johtua myös laitteistoviasta. Vaikka tuuletin käynnistyy virtapainiketta painamalla, jotkin muut keskeiset osat eivät välttämättä lähde käyntiin. Tässä tapauksessa vie kone huoltoon.</p>
</section>

</page>
