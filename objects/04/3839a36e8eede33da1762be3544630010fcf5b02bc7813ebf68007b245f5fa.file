<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-admin-change" xml:lang="sv">

  <info>
    <link type="guide" xref="user-accounts#privileges"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Dokumentationsprojekt för GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Du kan tillåta användare att göra ändringar för systemet genom att ge dem administratörsbehörighet.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Ändra vem som har administratörsbehörighet</title>

  <p>Administratörsbehörighet är ett sätt att bestämma vem som kan göra ändringar i viktiga delar av systemet. Du kan ändra vilka användare som har administratörsbehörighet och vilka som inte har det. De är ett bra sätt att hålla ditt system säkert och förhindra eventuellt skadliga, obehöriga ändringar.</p>

  <p>Du behöver <link xref="user-admin-explain">administratörsbehörighet</link> för att ändra kontotyp.</p>

  <steps>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Användare</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Användare</gui> för att öppna panelen.</p>
    </item>
    <item>
      <p>Tryck på <gui style="button">Lås upp</gui> i övre högra hörnet och skriv in ditt lösenord när du blir tillfrågad.</p>
    </item>
    <item>
      <p>Markera användaren vars behörigheter du vill ändra.</p>
    </item>
    <item>
      <p>Klicka på etiketten <gui>Standard</gui> intill <gui>Kontotyp</gui> och välj <gui>Administratör</gui>.</p>
    </item>
    <item>
      <p>Användarens behörighet kommer att ändras när de loggar in nästa gång.</p>
    </item>
  </steps>

  <note>
    <p>Det första användarkontot på systemet är vanligtvis det som har administratörsbehörighet. Detta är användarkontot som skapades när du först installerade systemet.</p>
    <p>Det är oklokt att ge alltför många användare behörigheten <gui>Administratör</gui> på ett system.</p>
  </note>

</page>
