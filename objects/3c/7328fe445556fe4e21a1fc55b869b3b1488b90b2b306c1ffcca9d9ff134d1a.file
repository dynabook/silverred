<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="net-wireless-wepwpa" xml:lang="sv">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Dokumentationsprojekt för GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>WEP och WPA är sätt att kryptera data på trådlösa nätverk.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Vad betyder WEP och WPA?</title>

  <p>WEP och WPA (tillsammans med WPA2) är namn på olika krypteringsverktyg som används för att säkra din trådlösa anslutning. Kryptering förvränger nätverksanslutningen så att ingen kan ”lyssna” på den och till exempel se vilka webbsidor som du tittar på. WEP står för <em>Wired Equivalent Privacy</em> och WPA står för <em>Wireless Protected Access</em>. WPA2 är den andra versionen av WPA-standarden.</p>

  <p>Att använda <em>någon</em> form av kryptering är alltid bättre än att inte använda någon alls, men WEP är den minst säkra av dessa standarder, så du bör inte använda den om du kan undvika det. WPA2 är den säkraste av de tre. Om ditt trådlösa kort och router har stöd för WPA2 är det vad du bör använda när du ställer in ditt trådlösa nätverk.</p>

</page>
