<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="tips-specialchars" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="tips"/>
    <link type="seealso" xref="keyboard-layouts"/>

    <revision pkgversion="3.8.2" version="0.3" date="2013-05-18" status="review"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.26" date="2017-11-27" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Digite caracteres não disponíveis no seu teclado, incluindo alfabetos estrangeiros, símbolos matemáticos e caracteres decorativos.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

  <title>Inserindo caracteres especiais</title>

  <p>Você pode inserir e visualizar milhares de caracteres da maioria dos sistemas de escritas do mundo, mesmo aqueles não encontrados no seu teclado. Esta página lista algumas diferentes maneiras de como inserir caracteres especiais.</p>

  <links type="section">
    <title>Métodos para inserir caracteres</title>
  </links>

  <section id="characters">
    <title>Caracteres</title>
    <p>O GNOME vem com um aplicativo de mapa de caracteres que permite que você localize e insira caracteres incomuns, incluindo emoji, navegando pelas categorias de caracteres ou pesquisa por palavras-chaves.</p>

    <p>Você pode iniciar <app>Caracteres</app> do panorama de Atividades.</p>

  </section>

  <section id="compose">
    <title>Tecla de composição</title>
    <p>Uma tecla de composição é uma tecla especial que lhe permite pressionar múltiplas teclas de uma vez para obter um caractere especial. Por exemplo, para digitar a letra acentuada <em>é</em>, você pode pressionar <key>composição</key>, <key>'</key> e então <key>e</key>.</p>
    <p>Os teclados não possuem teclas específicas para composição. Em vez disso, você pode definir uma das existentes no seu teclado como uma de composição.</p>

    <note style="important">
      <p>Você precisa ter <app>Ajustes</app> instalado em seu computador para alterar essa configuração.</p>
      <if:if xmlns:if="http://projectmallard.org/if/1.0/" test="action:install">
        <p><link style="button" action="install:gnome-tweaks">Instalar <app>Ajustes</app></link></p>
      </if:if>
    </note>

    <steps>
      <title>Definir uma tecla de composição</title>
      <item>
        <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Ajustes</gui>.</p>
      </item>
      <item>
        <p>Clique em <gui>Ajustes</gui> para abrir o aplicativo.</p>
      </item>
      <item>
        <p>Clique na aba <gui>Teclado &amp; mouse</gui>.</p>
      </item>
      <item>
        <p>Clique em <gui>Desabilitado</gui> próximo à configuração <gui>Tecla de composição</gui>.</p>
      </item>
      <item>
        <p>Ative o botão no diálogo e escolha o atalho de teclado que você deseja usar.</p>
      </item>
      <item>
        <p>Marque a caixa de seleção da tecla que você deseja definir como tecla de composição.</p>
      </item>
      <item>
        <p>Feche o diálogo.</p>
      </item>
      <item>
        <p>Feche a janela do <gui>Ajustes</gui>.</p>
      </item>
    </steps>

    <p>Você pode digitar muitos caracteres comuns usando a tecla de composição; por exemplo:</p>

    <list>
      <item><p>Pressione <key>composição</key>, <key>'</key> (apóstrofo) e uma letra para colocar um acento agudo sobre essa letra, como em <em>é</em>.</p></item>
      <item><p>Pressione <key>composição</key>, <key>`</key> (acento grave) e uma letra para colocar um acento grave sobre essa letra, como em <em>è</em>.</p></item>
      <item><p>Pressione <key>composição</key>, <key>"</key> e uma letra para colocar um trema sobre essa letra, como em <em>ë</em>.</p></item>
      <item><p>Pressione <key>composição</key>, <key>-</key> e uma letra para colocar um mácron sobre essa letra, como em <em>ē</em>.</p></item>
    </list>
    <p>Para mais sequências com teclas de composição, veja <link href="http://en.wikipedia.org/wiki/Compose_key#Common_compose_combinations">a página sobre tecla de composição na Wikipédia</link>.</p>
  </section>

<section id="ctrlshiftu">
  <title>Pontos de código</title>

  <p>Você pode digitar qualquer caractere Unicode usando os pontos de códigos numéricos do caractere em seu teclado. Todo caractere é identificado por um ponto de código de quatro caracteres. Para descobrir o ponto de código de um caractere, procure no aplicativo <app>Caracteres</app>. O ponto de código encontra-se quatro caracteres após <gui>U+</gui>.</p>

  <p>Para inserir um caractere por meio do seu ponto de código, pressione <keyseq><key>Ctrl</key><key>Shift</key><key>U</key></keyseq>, então digite o código de quatro caracteres e pressione <key>Espaço</key> ou <key>Enter</key>. Se você usa com frequência os caracteres que você não consegue acessar facilmente com os outros métodos, pode lhe ser útil memorizar o ponto de código para aqueles caracteres, de forma que você possa inseri-los rapidamente.</p>

</section>

  <section id="layout">
    <title>Disposições de teclado</title>
    <p>Você pode fazer seu teclado se comportar como um de outro idioma, independentemente das letras impressas nas teclas. Você pode até mesmo alternar entre disposições de teclado facilmente usando um ícone na barra superior. Para aprender como, veja <link xref="keyboard-layouts"/>.</p>
  </section>

<section id="im">
  <title>Métodos de entrada</title>

  <p>Um método de entrada expande os métodos anteriores ao permitir inserir caracteres não somente com o teclado, mas também por qualquer dispositivo de entrada. Por exemplo, você poderia inserir caracteres com um mouse usando um método de gestos ou inserir caracteres japoneses usando um teclado ABNT.</p>

  <p>Para escolher um método de entrada, clique com o botão do meio do mouse sobre um widget de texto e no menu <gui>Método de entrada</gui>, escolha um método de entrada que você queira usar. Não há um método de entrada padrão. Então, lei a documentação sobre métodos de entrada para saber como usá-los.</p>

</section>

</page>
