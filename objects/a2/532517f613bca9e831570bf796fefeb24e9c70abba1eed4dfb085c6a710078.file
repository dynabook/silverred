<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-mag" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="a11y#vision" group="lowvision"/>

    <revision pkgversion="3.7.1" date="2012-11-10" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Amplie sua tela de forma que seja mais fácil ver as coisas.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

  <title>Ampliando uma área da tela</title>

  <p>Ampliar a tela é diferente de simplesmente aumentar o <link xref="a11y-font-size">tamanho do texto</link>. Este recurso é como ter uma lupa de aumento, que permite que você a mova para ampliar partes da tela.</p>

  <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Acessibilidade</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Acessibilidade</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Pressione <gui>Ampliação</gui> na seção <gui>Visão</gui>.</p>
    </item>
    <item>
      <p>Alterne <gui>Ampliação</gui> no canto superior direito da janela <gui>Opções de ampliação</gui> para ligado.</p>
      <!--<note>
        <p>The <gui>Zoom</gui> section lists the current settings for the
        shortcut keys, which can be set in the <gui>Universal Access</gui>
        section of the <link xref="keyboard-shortcuts-set">Shortcuts</link> tab
        on the <gui>Keyboard</gui> panel.</p>
      </note>-->
    </item>
  </steps>

  <p>Você agora pode mover pela área da tela. Movendo o mouse para os cantos da tela, você moverá a área ampliada em direções diferentes, permitindo que veja sua área de escolha.</p>

  <note style="tip">
    <p>Você pode ativar e desativar rapidamente o ampliador clicando no <link xref="a11y-icon">ícone de acessibilidade</link> na barra superior e selecionando <gui>Ampliador</gui>.</p>
  </note>

  <p>Você pode alterar o fator de ampliação, a mira do mouse e a posição da visão ampliada na tela. Ajuste-as na aba <gui>Ampliação</gui> da janela <gui>Opções de ampliação</gui>.</p>

  <p>Você pode ativar miras para ajudar você a achar o ponteiro do mouse ou touchpad. Ativa-as e ajuste seu comprimento, cor e espessura na aba <gui>Mira</gui> da janela de configurações de <gui>Ampliação</gui>.</p>

  <p>Você pode ativar o vídeo invertido ou <gui>Preto no branco</gui> e ajustar opções de brilho, contraste e níveis de cinza para a ampliação. A combinação dessas opções é útil para pessoas com baixa visão, com algum grau de fotofobia ou apenas usando o computador sob condições de luminosidade adversa. Selecione a aba <gui>Efeitos de cor</gui> na janela de configurações de <gui>Ampliação</gui> para habilitar e alterar essas configurações.</p>

</page>
