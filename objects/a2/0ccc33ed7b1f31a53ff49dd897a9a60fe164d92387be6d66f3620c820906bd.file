<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-mag" xml:lang="id">

  <info>
    <link type="guide" xref="a11y#vision" group="lowvision"/>

    <revision pkgversion="3.7.1" date="2012-11-10" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Zum perbesar layar Anda sehingga lebih mudah melihat berbagai hal.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

  <title>Perbesar suatu wilayah layar</title>

  <p>Memperbesar layar berbeda dengan hanya memperbesar <link xref="a11y-font-size">ukuran teks</link>. Fitur ini seperti memiliki kaca pembesar, memungkinkan Anda berpindah-pindah dengan menzum pada berbagai bagian layar.</p>

  <steps>
    <item>
      <p>Buka ringkasan <gui xref="shell-introduction#activities">Aktivitas</gui> dan mulai mengetik <gui>Akses Universal</gui>.</p>
    </item>
    <item>
      <p>Klik pada <gui>Akses Universal</gui> untuk membuka panel.</p>
    </item>
    <item>
      <p>Tekan <gui>Zum</gui> dalam bagian <gui>Melihat</gui>.</p>
    </item>
    <item>
      <p>Switch the <gui>Zoom</gui> switch in the top-right corner of the
      <gui>Zoom Options</gui> window to on.</p>
      <!--<note>
        <p>The <gui>Zoom</gui> section lists the current settings for the
        shortcut keys, which can be set in the <gui>Universal Access</gui>
        section of the <link xref="keyboard-shortcuts-set">Shortcuts</link> tab
        on the <gui>Keyboard</gui> panel.</p>
      </note>-->
    </item>
  </steps>

  <p>Anda kini dapat berpindah-pindah pada area layar. Dengan memindah tetikus Anda ke tepi layar, Anda akan memindah area yang diperbesar ke arah berbeda, memungkinkan Anda melihat area pilihan Anda.</p>

  <note style="tip">
    <p>Anda dapat secara cepat menyalakan dan mematikan zum dengan mengklik <link xref="a11y-icon">ikon aksesibilitas</link> pada bilah puncak dan memilih <gui>Zum</gui>.</p>
  </note>

  <p>Anda dapat mengubah faktor perbesaran, pelacakan tetikus, dan posisi tilikan yang diperbesar pada layar. Atur ini dalam tab <gui>Pembesar</gui> dari jendela <gui>Opsi Zum</gui>.</p>

  <p>Anda dapat mengaktifkan pembidik silang untuk membantu Anda menemukan penunjuk tetikus atau touchpad. Nyalakan dan atur panjang, warna, dan ketebalannya dalam tab <gui>Pembidik Silang</gui> dari jendela pengaturan <gui>Zum</gui>.</p>

  <p>Anda dapat beralih ke video terbalik atau <gui>Putih di atas hitam</gui>, dan mengatur opsi kecerahan, kontras, serta tingkat abu-abu bagi pembesar. Kombinasi opsi-opsi ini berguna bagi orang yang memiliki penglihatan lemah, sebarang tingkat fotofobia, atau sekedar untuk memakai komputer di bawah kondisi pencahayaan ekstrim. Pilih tab <gui>Efek Warna</gui> dalam jendela pengaturan <gui>Zum</gui> untuk mengaktifkan dan mengubah opsi-opsi ini.</p>

</page>
