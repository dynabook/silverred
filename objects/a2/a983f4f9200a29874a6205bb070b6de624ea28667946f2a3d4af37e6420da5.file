<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="contacts-link-unlink" xml:lang="ca">

  <info>
    <link type="guide" xref="contacts"/>
    <revision pkgversion="3.5.5" date="2012-02-19" status="review"/>
    <revision pkgversion="3.8" date="2013-04-27" status="review"/>
    <revision pkgversion="3.12" date="2014-02-27" status="final"/>
    <revision pkgversion="3.15" date="2015-01-28" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Combineu la informació d'un contacte de diverses fonts.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

<title>Enllaçar i desenllaçar contactes</title>

<section id="link-contacts">
  <title>Enllaçar contactes</title>

  <p>Podeu combinar contactes duplicats des de la vostra llibreta d'adreces locals i comptes en línia en una entrada al <app>Contactes</app>. Aquesta funció us ajudarà a mantenir organitzada la vostra llibreta d'adreces, amb tots els detalls del contacte en un sol lloc.</p>

  <steps>
    <item>
      <p>Activa el <em>mode de selecció</em> prement el botó marcar a sobre de la llista de contactes.</p>
    </item>
    <item>
      <p>Apareixerà una casella de selecció al costat de cada contacte. Marqueu les caselles de selecció al costat dels contactes que voleu combinar.</p>
    </item>
    <item>
      <p>Premeu <gui style="button">Enllaça</gui> per enllaçar els contactes seleccionats.</p>
    </item>
  </steps>

</section>

<section id="unlink-contacts">
  <title>Desenllaçar contactes</title>

  <p>És possible que vulgueu desenllaçar contactes si accidentalment heu enllaçat contactes que no s'havien d'enllaçar.</p>

  <steps>
    <item>
      <p>Trieu el contacte que voleu desenllaçar de la llista de contactes.</p>
    </item>
    <item>
      <p>Premeu <gui style="button">Edita</gui> a la cantonada superior dreta de <app>Contactes</app>.</p>
    </item>
    <item>
      <p>Premeu <gui style="button">Comptes enllaçats</gui>.</p>
    </item>
    <item>
      <p>Premeu <gui style="button">Desenllaça</gui> per desenllaçar l'entrada del contacte.</p>
    </item>
    <item>
      <p>Tanqueu la finestra un cop hàgiu acabat de desenllaçar les entrades.</p>
    </item>
    <item>
      <p>Premeu <gui style="button">Fet</gui> per acabar d'editar el contacte.</p>
    </item>
  </steps>

</section>

</page>
