<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="ui" version="1.0 if/1.0" id="shell-workspaces" xml:lang="fi">

  <info>
    <link type="guide" xref="shell-windows#working-with-workspaces" group="#first"/>

    <revision pkgversion="3.8.0" date="2013-04-23" status="review"/>
    <revision pkgversion="3.10.3" date="2014-01-26" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.35.91" date="2020-02-27" status="candidate"/>

    <credit type="author">
      <name>Gnomen dokumentointiprojekti</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Työtilojen avulla voit ryhmitellä avoinna olevia ikkunoita.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

<title>Mikä ihmeen työtila, ja miten voin hyödyntää sitä?</title>

    <media its:translate="no" type="image" src="figures/shell-workspaces.png" width="233" height="579" style="floatend floatright">
        <p>Workspace selector</p>
    </media>

  <p if:test="!platform:gnome-classic">Työtilat viittaavat ikkunoiden ryhmittämiseen työpöydällä. Voit luoda useita työtiloja, jotka toimivat kuten virtuaaliset työpöydät. Työpöytien on tarkoitus helpottaa ikkunoiden kanssa työskentelyä ja tehdä työpöydällä liikkumisesta aiempaa helpompaa.</p>

  <p if:test="platform:gnome-classic">Työtilat viittaavat ikkunoiden ryhmittämiseen työpöydällä. Voit käyttää useita työtiloja, jotka toimivat kuten virtuaaliset työpöydät. Työpöytien on tarkoitus helpottaa ikkunoiden kanssa työskentelyä ja tehdä työpöydällä liikkumisesta aiempaa helpompaa.</p>

  <p>Järjestä esimerkiksi tiettyyn käyttöön tarkoitetut sovellukset omiin työtiloihin. Sähköposti ja pikaviestin ensimmäisessä työtilassa, toisessa työtilassa musiikkisovellus, kolmannessa työtilassa kuvat jne.</p>

<p>Työtilojen käyttäminen:</p>

<list>
  <item>
    <p if:test="!platform:gnome-classic">Siirrä hiiren osoitin näytön oikeaan laitaan <gui xref="shell-introduction#activities">Toiminnot</gui>-yleisnäkymässä.</p>
    <p if:test="platform:gnome-classic">Click the button at the bottom left of
    the screen in the window list, or press the
    <key xref="keyboard-key-super">Super</key> key to open the
    <gui>Activities</gui> overview.</p>
  </item>
  <item>
    <p if:test="!platform:gnome-classic">A vertical panel will expand showing
    workspaces in use, plus an empty workspace. This is the
    workspace selector.</p>
    <p if:test="platform:gnome-classic">In the bottom right corner, you see four
    boxes. This is the workspace selector.</p>
  </item>
  <item>
    <p if:test="!platform:gnome-classic">Lisää uusi työtila vetämällä ja pudottamalla ikkuna olemassa olevasta työtilasta tyhjään työtilaan työtilanvaihtimessa. Työtilassa on nyt se ikkuna, jonka sinne pudotit, ja tämän työtilan alle ilmestyy tyhjä työtila.</p>
    <p if:test="platform:gnome-classic">Drag and drop a window from your current
    workspace onto an empty workspace in the workspace selector. This workspace
    now contains the window you have dropped.</p>
  </item>
  <item if:test="!platform:gnome-classic">
    <p>Poista työtila sulkemalla kaikki siinä olevat ikkunat tai siirtämällä kyseiset ikkunat toiseen työtilaan.</p>
  </item>
</list>

<p if:test="!platform:gnome-classic">Työtiloja on aina vähintään yksi.</p>

</page>
