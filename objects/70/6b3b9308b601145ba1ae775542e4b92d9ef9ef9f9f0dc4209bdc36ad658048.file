<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="sound-crackle" xml:lang="de">

  <info>
    <link type="guide" xref="sound-broken"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Überprüfen Sie die Audio-Anschlusskabel und die Treiber für die Soundkarte.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2019.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2019.</mal:years>
    </mal:credit>
  </info>

<title>Ich höre Kratzen oder Brummen bei der Klangwiedergabe</title>

  <p>Falls Sie Kratzen oder Brummen bei der Klangwiedergabe Ihres Rechners hören, könnte dies an der Kabeln oder Steckern oder auch an den Treibern für die Soundkarte liegen.</p>

<list>
 <item>
  <p>Überprüfen Sie, ob die Lautsprecher korrekt angeschlossen sind.</p>
  <p>Falls die Lautsprecher nicht vollständig eingesteckt sind oder sich in der falschen Buchse befinden, könnten Sie ein Brummen hören.</p>
 </item>

 <item>
  <p>Stellen Sie sicher, dass das Kabel der Lautsprecher oder des Kopfhörers nicht beschädigt ist.</p>
  <p>Audiokabel und -verbinder können mit der Zeit verschleißen. Versuchen Sie, das Kabel oder den Kopfhörer an einer anderen Buchse (beispielsweise an einen MP3-Player oder CD-Spieler) anzuschließen, om zu hören, ob immer noch Kratzgeräusche vorhanden sind. Falls dem so ist, werden Sie möglicherweise das Kabel oder den Kopfhörer ersetzen müssen.</p>
 </item>

 <item>
  <p>Überprüfen Sie die Qualität der Audio-Treiber.</p>
  <p>Einige Soundkarten funktionieren nicht besonders gut unter Linux, weil die Treiber dafür nicht besonders gut sind. Dieses Problem lässt sich nur schwierig eingrenzen. Versuchen Sie, im Internet nach Hersteller und Modellbezeichnung Ihrer Soundkarte zu suchen, wobei Sie den Suchbegriff »Linux« hinzufügen sollten. So sehen Sie vielleicht, ob das Problem noch bei anderen Benutzern besteht.</p>
  <p>Mit dem Befehl <cmd>lspci</cmd> erhalten Sie weitere Informationen zu Ihrer Soundkarte.</p>
 </item>
</list>

</page>
