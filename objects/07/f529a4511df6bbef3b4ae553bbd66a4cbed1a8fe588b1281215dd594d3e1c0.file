<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-recover" xml:lang="cs">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>
    <link type="seealso" xref="files-lost"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="review"/>

    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Mazané soubory se normálně posílají do koše, ale mohou být obnoveny.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Obnovení souboru z koše</title>

  <p>Když ve správci souborů smažete soubor, normálně se přesune do <gui>Koše</gui> a mělo by být možné ho obnovit.</p>

  <steps>
    <title>Když chcete obnovit soubor z koše:</title>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <app>Soubory</app>.</p>
    </item>
    <item>
      <p>Kliknutím na <app>Soubory</app> otevřete správce souborů.</p>
    </item>
    <item>
      <p>V postranním panelu klikněte na <gui>Koš</gui>. Pokud postranní panel nevidíte, zmáčkněte tlačítko nabídky v pravém horním rohu okna a vyberte <gui>Postranní panel</gui>.</p>
    </item>
    <item>
      <p>Pokud zde je smazaný soubor, klikněte na něj a vyberte <gui>Obnovit</gui>. Obnoven bude do složky, ze které byl vymazán.</p>
    </item>
  </steps>

  <p>V případě, že smažete soubor zmáčknutím <keyseq><key>Shift</key><key>Delete</key></keyseq> nebo pomocí příkazové řádky, soubor bude smazán trvale. Soubory smazané trvale nelze obnovit z <gui>Koše</gui>.</p>

  <p>Existuje řada nástrojů pro obnovu, které někdy dokáží obnovit i soubory, které byly smazané trvale. Obecně tyto nástroje ale nejsou jednoduché na používání. Pokud si omylem soubor smažete trvale, je asi nejlepší zeptat se na radu na diskuzním fóru podpory, jestli půjde obnovit.</p>

</page>
