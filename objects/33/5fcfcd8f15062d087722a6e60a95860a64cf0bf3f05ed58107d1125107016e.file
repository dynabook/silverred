<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-get-online" xml:lang="pl">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>
    <link type="guide" xref="getting-started" group="videos"/>
    <title role="trail" type="link">Łączenie z siecią</title>
    <link type="seealso" xref="net"/>
    <title role="seealso" type="link">Samouczek łączenia z siecią</title>
    <link type="next" xref="gs-browse-web"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2013-2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2013-2020</mal:years>
    </mal:credit>
  </info>

  <title>Łączenie z siecią</title>
  
  <note style="important">
      <p>Stan połączenia sieciowego jest wyświetlany po prawej stronie górnego paska.</p>
  </note>  

  <section id="going-online-wired">

    <title>Łączenie z siecią przewodową</title>

    <media its:translate="no" type="image" mime="image/svg" src="gs-go-online1.svg" width="100%"/>

    <steps>
      <item><p>Ikona połączenia sieciowego po prawej stronie górnego paska pokazuje, że komputer nie jest połączony (tryb offline).</p>
      <p>Stan offline może mieć wiele przyczyn: na przykład kabel mógł zostać odłączony, komputer został ustawiony w <em>trybie samolotowym</em> lub brak dostępnych sieci Wi-Fi w okolicy.</p>
      <p>Aby użyć połączenia przewodowego, podłącz kabel sieciowy. Komputer spróbuje automatycznie ustawić połączenie sieciowe.</p>
      <p>Kiedy komputer ustawia połączenie sieciowe, ikona połączenia wyświetla trzy kropki.</p></item>
      <item><p>Po pomyślnym ustawieniu połączenia sieciowego ikona połączenia zmienia się w symbol komputera podłączonego do sieci.</p></item>
    </steps>
    
  </section>

  <section id="going-online-wifi">

    <title>Łączenie z siecią Wi-Fi</title>

    <media its:translate="no" type="image" mime="image/svg" src="gs-go-online2.svg" width="100%"/>
    
    <steps>
      <title>Aby połączyć z siecią Wi-Fi (bezprzewodową):</title>
      <item>
        <p>Kliknij <gui xref="shell-introduction#yourname">menu systemowe</gui> po prawej stronie górnego paska.</p>
      </item>
      <item>
        <p>Wybierz <gui>Nie połączono z siecią Wi-Fi</gui>. Sekcja menu Wi-Fi się rozszerzy.</p>
      </item>
      <item>
        <p>Kliknij <gui>Wybierz sieć</gui>.</p>
      </item>
    </steps>

     <note style="important">
       <p>Można łączyć się z sieciami Wi-Fi tylko, jeśli komputer je obsługuje oraz jest w zasięgu jednej z nich.</p>
     </note>

    <media its:translate="no" type="image" mime="image/svg" src="gs-go-online3.svg" width="100%"/>

    <steps style="continues">
    <item><p>Z listy dostępnych sieci Wi-Fi wybierz sieć, z którą połączyć, i kliknij <gui>Połącz</gui>, aby potwierdzić.</p>
    <p>W zależności od konfiguracji sieci, może być wymagane podanie danych uwierzytelniających sieci.</p></item>
    </steps>

  </section>

</page>
