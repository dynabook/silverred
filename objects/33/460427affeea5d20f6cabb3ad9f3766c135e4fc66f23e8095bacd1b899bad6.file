<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="process-files" xml:lang="fr">
  <info>
    <revision version="0.1" date="2014-01-25" status="review"/>
    <link type="guide" xref="index#processes-tasks" group="processes-tasks"/>
    
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    
    <credit type="author copyright">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
      <years>2014</years>
    </credit>

    <desc>Afficher les fichiers auxquels les processus accèdent.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>naybnet</mal:name>
      <mal:email>naybnet@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leonor Palazzo</mal:name>
      <mal:email>leonor.palazzo@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Liste des fichiers ouverts par un processus</title>

  <p>Les processus ont parfois besoin de garder des fichiers ouverts. Ceux-ci peuvent être des fichiers que vous consultez ou que vous modifiez, ou alors des fichiers temporaires ou systèmes dont le processus se sert pour fonctionner correctement.</p>
  
  <p>Pour savoir quels fichiers ont été ouverts par un processus :</p>
  <steps>
    <item><p>cherchez le processus dans l'onglet <gui>Processus</gui> et cliquez dessus pour le sélectionner,</p></item>
    <item><p>faites un clic droit et sélectionnez <gui>Fichiers ouverts</gui>.</p></item>
  </steps>
  
  <p>Dans la liste des fichiers ouverts qui s'affiche, vous pouvez éventuellement trouver des fichiers spéciaux appelés <em>sockets</em>. Ils sont en fait un moyen pour les processus de communiquer entre eux, et ne sont pas des fichiers normaux que vous pouvez visualiser ou modifier.</p>

</page>
