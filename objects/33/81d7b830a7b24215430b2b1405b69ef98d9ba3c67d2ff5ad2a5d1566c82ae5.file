<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-fingerprint" xml:lang="ca">

  <info>
    <link type="guide" xref="hardware-auth"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-03" status="review"/>
    <revision pkgversion="3.12" date="2014-06-16" status="final"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="final"/>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
      <years>2011</years>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Podeu iniciar sessió al vostre sistema mitjançant un escàner de la vostra empremta digital en comptes d'escriure la vostra contrasenya.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

  <title>Registrar-se mitjançant l'empremta digital</title>

  <p>Si el vostre sistema té un escàner d'empremtes digitals compatible, podeu gravar la vostra empremta digital i usar-la per iniciar sessió.</p>

<section id="record">
  <title>Enregistrar una empremta digital</title>

  <p>Abans de poder iniciar sessió amb la vostra empremta digital, heu de gravar-la perquè el sistema la pugui utilitzar per identificar-vos.</p>

  <note style="tip">
    <p>Si el vostre dit està massa sec, és possible que tingueu dificultats per registrar la vostra empremta digital. Si això passa, humitegeu el dit una mica, assequeu-lo amb un drap net i sense borrissol i torneu-ho a provar.</p>
  </note>

  <p>Necessiteu <link xref="user-admin-explain">privilegis d'administrador </link> per editar comptes d'usuari que no siguin vostres.</p>

  <steps>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a teclejar <gui>Usuaris</gui>.</p>
    </item>
    <item>
      <p>Feu clic a <gui>Usuaris</gui> per obrir el quadre.</p>
    </item>
    <item>
      <p>Feu clic a <gui>Desactiva</gui>, al costat d'<gui>Entrada amb empremta dactilar</gui> per a afegir una empremta digital per al compte seleccionat. Si afegiu l'empremta digital per a un usuari diferent, primer haureu de <gui>Desbloqueja</gui> el quadre.</p>
    </item>
    <item>
      <p>Seleccioneu el dit que voleu utilitzar per a l'empremta digital, aleshores <gui style="button">Següent</gui>.</p>
    </item>
    <item>
      <p>Seguiu les instruccions del diàleg i feu lliscar el dit a <em>velocitat moderada</em> sobre el lector d'empremtes. Un cop l'ordinador tingui una bona gravació de la vostra empremta, veureu el missatge <gui>Fet!</gui>.</p>
    </item>
    <item>
      <p>Seleccioneu <gui>Següent</gui>. Veureu un missatge de confirmació que la vostra empremta digital s'ha desat correctament. Seleccioneu <gui>Tanca</gui> per acabar.</p>
    </item>
  </steps>

</section>

<section id="verify">
  <title>Comprovar que la vostra empremta funciona</title>

  <p>Ara comproveu que l'inici de sessió amb l'empremta digital funcioni. Si registreu una empremta digital, encara teniu l'opció d'iniciar sessió amb la vostra contrasenya.</p>

  <steps>
    <item>
      <p>Deseu qualsevol treball obert i després <link xref="shell-exit#logout">tanqueu la sessió</link>.</p>
    </item>
    <item>
      <p>A la pantalla d'inici de sessió, seleccioneu el vostre nom de la llista. Apareixerà el formulari d'entrada de la contrasenya.</p>
    </item>
    <item>
      <p>En lloc d'escriure la vostra contrasenya, hauríeu de poder lliscar el dit al lector d'empremtes.</p>
    </item>
  </steps>

</section>

</page>
