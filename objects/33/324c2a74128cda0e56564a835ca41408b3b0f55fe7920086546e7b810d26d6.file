<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="login-banner" xml:lang="gl">

  <info>
    <link type="guide" xref="login#appearance"/>
    <!--<link type="seealso" xref="gdm-restart"/>-->
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="author copyright editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>davidk@gnome.org</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Mostrar un tipo de letra adicional na pantalla de incio de sesión.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2010-2018</mal:years>
    </mal:credit>
  </info>

  <title>Mostrar un anuncio de texto na pantalla de inicio de sesión</title>

  <p>You can display extra text on the login screen, such as who to contact for
  support, by setting the
  <sys>org.gnome.login-screen.banner-message-enable</sys> and
  <sys>org.gnome.login-screen.banner-message-text</sys> GSettings keys.</p>

  <steps>
    <title>Mostrar un anuncio de texto na pantalla de inicio de sesión:</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-gdm'])"/>
    <item><p>Create a <sys>gdm</sys> keyfile for machine-wide settings in
    <file its:translate="no">/etc/dconf/db/gdm.d/01-banner-message</file>:</p>
      <code its:translate="no">[org/gnome/login-screen]
banner-message-enable=true
banner-message-text='<input its:translate="yes">Escriba o mensaxe de anuncio aquí.</input>'
</code>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  </steps>

  <note>
    <p>There is no character limit for the banner message. <sys>gnome-shell</sys>
    autodetects longer stretches of text and enters two column mode.</p>
    <p>The banner message cannot be read from an external file.</p>
  </note>

</page>
