<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-delete" xml:lang="as">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>
    <link type="seealso" xref="files-recover"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-16" status="review"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="candidate"/>
    <revision pkgversion="3.16" date="2015-02-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>ক্ৰিস্টফাৰ থমাচ</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>শ্বণ মেকেন্স</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>জিম কেম্পবেল</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>মাইকেল হিল</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>আপোনাৰ প্ৰয়োজন নহোৱা ফাইলসমূহ অথবা ফোল্ডাৰসমূহ আতৰাওক।</desc>
  </info>

<title>ফাইলসমূহ আৰু ফোল্ডাৰসমূহ মচি পেলাওক</title>

  <p>If you do not want a file or folder any more, you can delete it. When you
  delete an item it is moved to the <gui>Trash</gui> folder, where it is stored
  until you empty the trash. You can <link xref="files-recover">restore
  items</link> in the <gui>Trash</gui> folder to their original location if you
  decide you need them, or if they were accidentally deleted.</p>

  <steps>
    <title>এটা ফাইলক আবৰ্জনালৈ পঠাবলৈ:</title>
    <item><p>আপুনি আবৰ্জনাত থব খোজা বস্তু এবাৰ ক্লিক কৰি বাছক।</p></item>
    <item><p>Press <key>Delete</key> on your keyboard. Alternatively, drag the
    item to the <gui>Trash</gui> in the sidebar.</p></item>
  </steps>

  <p>The file will be moved to the trash, and you’ll be presented with an
  option to <gui>Undo</gui> the deletion. The <gui>Undo</gui> button will appear
  for a few seconds. If you select <gui>Undo</gui>, the file will be restored
  to its original location.</p>

  <p>ফাইলসমূহ চিৰস্থায়ীভাৱে মচি পেলাবলৈ, আৰু আপোনাৰ কমপিউটাৰত ডিস্ক স্থান খালি কৰিবলৈ, আপুনি আবৰ্জনা খালি কৰিব লাগিব। আবৰ্জনা খালি কৰিবলৈ, কাষবাৰত <gui>আবৰ্জনা</gui> ৰাইট-ক্লিক কৰক আৰু <gui>আবৰ্জনা ৰিক্ত কৰক</gui> বাছক।</p>

  <section id="permanent">
    <title>এটা ফাইলক স্থায়ীভাৱে মচি পেলাওক</title>
    <p>আপুনি তৎক্ষনাত এটা ফাইলক চিৰকালৰ বাবে মচিব পাৰিব, ইয়াক প্ৰথমতে আবৰ্জনালৈ পঠোৱাৰ প্ৰয়োজনীয়তা নহোৱাকৈ।</p>

  <steps>
    <title>এটা ফাইলক স্থায়ীভাৱে মচিবলৈ আপুনি নিম্নলিখিত কাৰ্য্য কৰিব লাগিব:</title>
    <item><p>আপুনি মচিব খোজা বস্তু বাছক।</p></item>
    <item><p><key>Shift</key> কি টিপি ধৰি থওক, তাৰ পিছত <key>Delete</key> কি টিপক।</p></item>
    <item><p>যিহেতু আপুনি ইয়াক নহোৱা কৰিব নোৱাৰে, আপুনি ফাইল অথবা ফোল্ডাৰ মচিব বিচাৰে সুনিশ্চিত কৰিবলৈ কোৱা হব।</p></item>
  </steps>

  <note><p>এটা <link xref="files#removable">আতৰাব পৰা ডিভাইচ</link> ত মচি পেলোৱা ফাইলসমূহ অন্য অপাৰেটিং চিস্টেম, যেনে Windows অথবা Mac OS ত দৃশ্যমান নহবও পাৰে। ফাইলসমূহ এতিয়াও আছে আৰু আপুনি ডিভাইচক আপোনাৰ কমপিউটাৰত প্লাগ কৰোতে দেখি পাব।</p></note>

  </section>

</page>
