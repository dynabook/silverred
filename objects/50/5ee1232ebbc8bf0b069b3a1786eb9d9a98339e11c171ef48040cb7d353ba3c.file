<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="sharing-desktop" xml:lang="sr">

  <info>
    <link type="guide" xref="sharing"/>
    <link type="guide" xref="prefs-sharing"/>

    <revision pkgversion="3.14" date="2015-01-25" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.29" date="2018-08-28" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <credit type="author">
      <name>Екатерина Герасимова</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Мајкл Хил</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Џим Кембел</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Допустите другима да прегледају и посредују са вашом радном површи користећи ВНЦ.</desc>
  </info>

  <title>Делите радну површ</title>

  <p>Можете да допустите другима да прегледају и управљају вашом радном површином са другог рачунара са програмом за преглед радне површи. Подесите <gui>Дељење екрана</gui> да дозволите другима да приступе вашој радној површи и да подесе поставке безбедности.</p>

  <note style="info package">
    <p>Морате имати инсталран пакет <app>Вино</app> да би <gui>Дељење екрана</gui> било видљиво.</p>

    <if:choose xmlns:if="http://projectmallard.org/if/1.0/">
      <if:when test="action:install">
        <p><link action="install:vino" style="button">Инсталирајте Вино</link></p>
      </if:when>
    </if:choose>
  </note>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Sharing</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>If the <gui>Sharing</gui> switch at the top-right of the window is set
      to off, switch it to on.</p>

      <note style="info"><p>Ако вам текст испод <gui>назива рачунара</gui> допушта да га уређујете, можете да <link xref="sharing-displayname">измените</link> назив под којим се ваш рачунар приказује на мрежи.</p></note>
    </item>
    <item>
      <p>Изаберите <gui>Дељење екрана</gui>.</p>
    </item>
    <item>
      <p>To let others view your desktop, switch the <gui>Screen Sharing</gui>
      switch to on. This means that other people will be able to attempt to
      connect to your computer and view what’s on your screen.</p>
    </item>
    <item>
      <p>To let others interact with your desktop, ensure that <gui>Allow
      connections to control the screen</gui> is checked. This may allow the
      other person to move your mouse, run applications, and browse files on
      your computer, depending on the security settings which you are currently
      using.</p>
    </item>
  </steps>

  <section id="security">
  <title>Безбедност</title>

  <p>Врло је важно да размотрите читав обим сваке безбедносне опције пре него ли је измените.</p>

  <terms>
    <item>
      <title>Нове везе морају да затраже приступ</title>
      <p>Ако желите да будете у могућности да одлучите да ли ћете дозволити некоме да приступи вашој радној површи, укључите <gui>Нове везе морају да затраже приступ</gui>. Ако искључите ову опцију, нећете бити упитани да ли желите да дозволите некоме да се повеже на ваш рачунар.</p>
      <note style="tip">
        <p>Ова опција је унапред изабрана.</p>
      </note>
    </item>
    <item>
      <title>Захтевајте лозинку</title>
      <p>Да захтевате да други људи користе лозинку приликом повезивања на вашу радну површ, укључите <gui>Захтевај лозинку</gui>. Ако не користите ову опцију, свако може да покуша да види вашу радну површ.</p>
      <note style="tip">
        <p>Ова опција је унапред искључена, али бисте требали да је укључите и да поставите безбедносну лозинку.</p>
      </note>
    </item>
<!-- TODO: check whether this option exists.
    <item>
      <title>Allow access to your desktop over the Internet</title>
      <p>If your router supports UPnP Internet Gateway Device Protocol and it
      is enabled, you can allow other people who are not on your local network
      to view your desktop. To allow this, select <gui>Automatically configure
      UPnP router to open and forward ports</gui>. Alternatively, you can
      configure your router manually.</p>
      <note style="tip">
        <p>This option is disabled by default.</p>
      </note>
    </item>
-->
  </terms>
  </section>

  <section id="networks">
  <title>Мреже</title>

  <p>The <gui>Networks</gui> section lists the networks to which you are
  currently connected. Use the switch next to each to choose where your
  desktop can be shared.</p>
  </section>

  <section id="disconnect">
  <title>Зауставите дељење радне површи</title>

  <p>To disconnect someone who is viewing your desktop:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Sharing</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p><gui>Screen Sharing</gui> will show as <gui>Active</gui>. Click on
      it.</p>
    </item>
    <item>
      <p>Toggle the switch at the top to off.</p>
    </item>
  </steps>

  </section>


</page>
