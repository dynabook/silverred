<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-batterywindows" xml:lang="ca">

  <info>
    <link type="guide" xref="power#faq"/>
    <link type="seealso" xref="power-batteryestimate"/>
    <link type="seealso" xref="power-batterylife"/>
    <link type="seealso" xref="power-batteryslow"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>

    <desc>Desajustos entre el fabricant i les diferents estimacions de vida de la bateria poden ser causa d'aquest problema.</desc>
    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

<title>Per què tinc menys bateria si utilitzo Windows / Mac OS?</title>

<p>Les bateries d'alguns equips sembla que tenen una vida més curta quan s'executen amb Linux que no pas amb Windows o Mac OS. Una de les raons d'això és que els proveïdors instal·len un programari especial per a Windows / Mac OS que optimitza diverses configuracions de maquinari / programari per a un determinat model d'ordinador. Aquests ajustos solen ser altament específics i no es poden documentar, de manera que incloure'ls en Linux és difícil.</p>

<p>Malauradament, no hi ha una manera senzilla d'aplicar aquests ajustaments sense saber exactament què són. Podeu trobar que, tot i això, l'ús d'alguns <link xref="power-batterylife">mètodes d'estalvi d'energia</link> ajudi. Si el computador té un <link xref="power-batteryslow">processador de velocitat variable</link>, és possible que trobeu que canviar la configuració us sigui útil.</p>

<p>Una altra possible raó de la discrepància és que el mètode d'estimació de la durada de la bateria és diferent a Windows / Mac OS que a Linux. La vida real de la bateria podria ser exactament la mateixa, però els diferents mètodes donen estimacions diferents.</p>
	
</page>
