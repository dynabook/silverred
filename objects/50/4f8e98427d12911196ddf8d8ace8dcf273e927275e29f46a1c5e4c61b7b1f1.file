<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-mag" xml:lang="ta">

  <info>
    <link type="guide" xref="a11y#vision" group="lowvision"/>

    <revision pkgversion="3.7.1" date="2012-11-10" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>ஷான் மெக்கேன்ஸ்</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>மைக்கேல் ஹில்</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>எக்காட்டெரினா ஜெராசிமோவா</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>திரையைப் பெரிதாக்கி திரையிலுள்ளவற்றை வசதியாகப் பார்க்க முடியும்.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>திரைப் பகுதியைப் உருப்பெருக்குதல்</title>

  <p>திரையை உருப்பெருக்கிப் பார்க்கும் அம்சமானது <link xref="a11y-font-size">உரை அளவை</link> பெரிதாக்குவதிலிருந்து வேறுபட்டது. இந்த அம்சத்தில் பார்ப்பது உருப்பெருக்கக் கண்ணாடி மூலம் பார்ப்பது போன்றது, இதில் பார்க்கும் போது நீங்கள் திரையின் ஒவ்வொரு பகுதியையும் இன்னும் பெரிதாக்கியபடி திரையில் நகர முடியும்.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Universal Access</gui>.</p>
    </item>
    <item>
      <p><gui>அனைவருக்குமான அணுகல்</gui> ஐத் திறந்து <gui>தட்டச்சு</gui> கீற்றைத் தேர்ந்தெடுக்கவும்.</p>
    </item>
    <item>
      <p><gui>அனைவருக்குமான அணுகல்</gui> ஐத் திறந்து <gui>பார்த்தல்</gui> கீற்றைத் தேர்ந்தெடுக்கவும்.</p>
    </item>
    <item>
      <p>Switch the <gui>Zoom</gui> switch in the top-right corner of the
      <gui>Zoom Options</gui> window to on.</p>
      <!--<note>
        <p>The <gui>Zoom</gui> section lists the current settings for the
        shortcut keys, which can be set in the <gui>Universal Access</gui>
        section of the <link xref="keyboard-shortcuts-set">Shortcuts</link> tab
        on the <gui>Keyboard</gui> panel.</p>
      </note>-->
    </item>
  </steps>

  <p>இப்போது நீங்கள் திரைப் பகுதியை அங்குமிங்கும் நகர்த்தலாம். சொடுக்கியை திரையின்  விளிம்புகளுக்குக் கொண்டு செல்வதன் மூலம் நீங்கள் உருப்பெருக்கப்பட்ட பகுதியை வெவ்வேறு திசைகளில் நகர்த்த முடியும், இதனால் நீங்கள் விரும்பும் பகுதியைக் காணலாம்.</p>

  <note style="tip">
    <p>நீங்கள் மேல் பட்டியில் உள்ள <link xref="a11y-icon">அணுகல் வசதி சின்னத்தை</link> சொடுக்கி <gui>பெரிதாக்கு</gui> என்பதைத் தேர்ந்தெடுப்பதன் மூலம் பெரிதாக்கல் வசதியை இயக்கலாம், அணைக்கலாம்.</p>
  </note>

  <p>You can change the magnification factor, the mouse tracking, and the
  position of the magnified view on the screen. Adjust these in the
  <gui>Magnifier</gui> tab of the <gui>Zoom Options</gui> window.</p>

  <p>You can activate crosshairs to help you find the mouse or touchpad
  pointer. Switch them on and adjust their length, color, and thickness in the
  <gui>Crosshairs</gui> tab of the <gui>Zoom</gui> settings window.</p>

  <p>உருப்பெருக்கிக்கு நீங்கள் தலைகீழ் காணொளி அல்லது <gui>கருப்பு வெள்ளை மாற்றம்</gui> விளைவை அமைக்கலாம், ஒளிர்வு, நிறமாறுபாடு மற்றும் கிரேஸ்கேல் விருப்பங்களை மாற்றலாம். இந்த விருப்பங்கள் பார்வைக் குறைவு உள்ளவர்களுக்கும் ஃபோட்டோஃபோபியா பிரச்சனை உள்ளவர்களுக்கும் ஒளி அமைப்பு சரியில்லாத சூழலில் கணினியைப் பயன்படுத்துபவர்களுக்கும் பயனுள்ளதாக இருக்கும். <gui>விருப்பங்கள்</gui> பொத்தானை சொடுக்கி, <gui>நிற விளைவுகள்</gui> கீற்றைத் தேர்வு செய்யவும்.</p>

</page>
