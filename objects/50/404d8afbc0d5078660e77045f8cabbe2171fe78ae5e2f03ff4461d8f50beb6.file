<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-hotcomputer" xml:lang="ca">

  <info>
    <link type="guide" xref="power#problems"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Normalment, els ordinadors s'escalfen, però si s'escalfen massa es poden sobreescalfar, cosa que pot perjudicar-los.</desc>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

<title>El meu ordinador s'escalfa moltíssim</title>

<p>La majoria dels ordinadors s'escalfen al cap d'un temps, i algunes poden escalfar-se moltíssim. Això és normal: forma simplement part de la manera com l'equip es refreda. Tanmateix, si l'equip s'escalfa molt, podria ser un senyal que està sobreescalfat, cosa que pot causar-li danys.</p>

<p>La majoria d'ordinadors portàtils s'escalfen de manera raonable quan els utilitzeu durant un temps. En general, no cal preocupar-s'hi: els ordinadors produeixen molta calor i els ordinadors portàtils són molt compactes, de manera que necessiten retirar ràpidament la calor i com a resultat la seva carcassa exterior s'escalfa. Tanmateix, alguns ordinadors portàtils s'escalfen i poden ser incòmodes d'utilitzar. Això normalment és el resultat d'un sistema de refrigeració mal dissenyat. De vegades podeu obtenir accessoris addicionals de refrigeració que s'adapten a la part inferior de l'ordinador portàtil i proporcionen un refredament més eficient.</p>

<p>Si teniu un ordinador de sobretaula que es nota calent quan es toca, és possible que no tingueu suficient refrigeració. Si això us preocupa, podeu comprar ventiladors de refrigeració addicionals o comprovar que els ventiladors de refrigeració i els respiradors estiguin lliures de pols i altres bloqueigs. És possible que vulgueu tenir en compte posar l'ordinador en una zona amb millor ventilació, si es manté en espais reduïts (per exemple, en un armari), el sistema de refrigeració de l'ordinador pot no ser capaç de treure la calor i fer circular l'aire fresc amb la suficient rapidesa.</p>

<p>Alguns estan preocupats pels riscos per a la salut d'usar ordinadors portàtils calents. Hi ha insinuacions sobre que l'ús perllongat d'un ordinador portàtil calent a la falda pot fer redueixi la fertilitat (masculina), i també hi ha informes sobre cremades menors (en casos extrems). Si us preocupa aquests problemes potencials, us recomanem que consulteu un metge per obtenir consell. Per descomptat, podeu optar per no deixar l'ordinador portàtil a la vostra falda.</p>

<p>La majoria dels ordinadors moderns es tancaran si s'escalfen massa per evitar que es facin malbé. Si l'ordinador continua apagant-se, aquest podria ser-ne el motiu. Si l'equip està sobreescalfat, és probable que hàgiu de reparar-lo.</p>

</page>
