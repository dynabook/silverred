<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-resize" xml:lang="cs">
  <info>
    <link type="guide" xref="disk"/>


    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <revision pkgversion="3.25.90" date="2017-08-17" status="review"/>

    <desc>Jak zmenšit nebo zvětšit souborový systém a jeho oddíl.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

<title>Úprava velikost souborového systému</title>

  <p>Souborový systém se dá zvětšit, aby využil volné místo ve svém oddílu. Často to lze udělat, i když je souborový systém připojený.</p>
  <p>Naopak, když třeba chcete uvolnit místo pro jiný oddíl, můžete velikost souborového systému zmenšit.</p>
  <p>Ne u všech souborových systému je změna velikosti podporovaná.</p>
  <p>Velikost oddílu se změní spolu s velikostí souborového systému. Stejným způsobem se dá také změnit velikost oddílu bez změny souborového systému.</p>

<steps>
  <title>Změna velikosti souborového systému/oddílu</title>
  <item>
    <p>Z přehledu <gui>Činnosti</gui> otevřete <app>Disky</app>.</p>
  </item>
  <item>
    <p>V seznamu úložných zařízení nalevo vyberte disk, který obsahuje dotčený souborový systém. Pokud je na disku více svazků, vyberte svazek, který jej obsahuje.</p>
  </item>
  <item>
    <p>Na nástrojové liště pod částí <gui>Svazky</gui> klikněte na nabídkové tlačítko. Pak klikněte na <gui>Změnit velikost souborového systému…</gui> nebo, pokud není žádný souborový systém vytvořený, na <gui>Změnit velikost…</gui>.</p>
  </item>
  <item>
    <p>Otevře se dialogové okno, kde si můžete zvolit novou velikost. Souborový systém se připojí, aby se vypočítala minimální potřebná velikost. Pokud není zmenšení velikosti podporováno, bude minimální velikost stejná jako aktuální. Když zmenšujete souborový systém, ponechte dostatečné místo, aby mohl fungovat dostatečně rychle a spolehlivě.</p>
    <p>V závislosti na tom, kolik dat se při zmenšování oddílu přesouvá, může změna velikosti trvat dlouho.</p>
    <note style="warning">
      <p>Změna velikosti souborového systému automaticky zavolá <link xref="disk-repair">opravu</link> souborového systému. Proto je doporučeno před spuštěním změny velikosti zálohovat důležitá data. Probíhající změnu nesmíte nijak přerušit, jinak dojde k poškození souborového systému.</p>
    </note>
  </item>
  <item>
      <p>Spuštění akce potvrďte zmáčknutím tlačítka <gui style="button">Změnit velikost</gui>.</p>
   <p>V případě, že není podporovaná změna velikosti na připojeném souborovém systému, souborový systém se během akce odpojí. Během operace buďte trpěliví.</p>
  </item>
  <item>
    <p>Po dokončení požadované změny velikosti a opravných činností je souborový systém opět připravený k používání.</p>
  </item>
</steps>

</page>
