<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-what" xml:lang="ru">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Back up anything that you cannot bear to lose if something goes
    wrong.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Что копировать</title>

  <p>В первую очередь следует сделать резервные копии <link xref="backup-thinkabout">самых важных файлов</link>, а также тех, которые трудно создать заново. Например (в порядке убывания важности):</p>

<terms>
 <item>
  <title>Ваши личные файлы</title>
   <p>Это могут быть документы, электронные таблицы, письма, события календаря, финансовые данные, семейные фотографии и любые другие личные файлы, которые невозможно создать заново.</p>
 </item>

 <item>
  <title>Ваши личные параметры</title>
   <p>Изменения, внесённые вами в параметры рабочего стола (цвета, фон, разрешение экрана и параметры мыши). В эту же группу входят параметры приложений, например настройки для <app>LibreOffice</app>, музыкального проигрывателя и почтовой программы. Их можно создать заново, но на это может потребоваться много времени.</p>
 </item>

 <item>
  <title>Параметры системы</title>
   <p>Большинство людей никогда не меняет параметры системы, созданные во время установки. Если вы по какой-то причине вносили в них изменения, или если компьютер используется в качестве сервера, можно сделать резервные копии этих параметров.</p>
 </item>

 <item>
  <title>Установленные программы</title>
   <p>Программы, как правило, легко восстановить после сбоя компьютера, просто переустановив их.</p>
 </item>
</terms>

  <p>В общем, следует сделать резервные копии файлов, которые невозможно создать заново, и файлов, на повторное создание которых уйдёт много времени. Если файлы легко создать заново, то можно и не тратить дисковое пространство на хранение их резервных копий.</p>

</page>
