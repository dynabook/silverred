<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="backup-check" xml:lang="es">

  <info>
    <link type="guide" xref="files#backup"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>davidk@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Verifique que su copia de respaldo sea correcta.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Compruebe su copia de respaldo</title>

  <p>Una vez que haya respaldado los archivos, debería asegurarse de que la copia se realizó correctamente. Si no funcionó correctamente puede perder datos importantes, dado que algunos archivos pueden perderse en el respaldo.</p>

   <p>Cuando usa <app>Archivos</app> para copiar o mover archivos, el equipo comprueba que todos los datos se transfieren correctamente. No obstante, si está transfiriendo datos muy importantes, puede querer realizar comprobaciones adicionales para confirmar que sus datos se transfirieron correctamente.</p>

  <p>Puede hacer una comprobación adicional mirando a través de los archivos y carpetas copiadas en el medio de destino. Comprobando que los archivos y carpetas realmente se han trasladado a la copia de seguridad, puede tener la confianza adicional de que el proceso se ha realizado correctamente.</p>

  <note style="tip"><p>Si observa que suele hacer a menudo copias de respaldo de grandes cantidades de datos, puede que le resulte más fácil usar un programa específico de copias de respaldo, como <app>Déjà Dup</app>. Ese programa es más potente y fiable que simplemente copiar y pegar los archivos.</p></note>

</page>
