<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-behavior" xml:lang="ja">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-behavior"/>

    <desc>シングルクリックによるファイルオープン、実行可能なテキストファイルの起動、およびゴミ箱関連の動作の設定を行います。</desc>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Sindhu S</name>
      <email>sindhus@live.in</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>松澤 二郎</mal:name>
      <mal:email>jmatsuzawa@gnome.org</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>赤星 柔充</mal:name>
      <mal:email>yasumichi@vinelinux.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kentaro KAZUHAMA</mal:name>
      <mal:email>kazken3@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shushi Kurose</mal:name>
      <mal:email>md81bird@hitaki.net</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Noriko Mizumoto</mal:name>
      <mal:email>noriko@fedoraproject.org</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>坂本 貴史</mal:name>
      <mal:email>o-takashi@sakamocchi.jp</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>日本GNOMEユーザー会</mal:name>
      <mal:email>http://www.gnome.gr.jp/</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>ファイルマネージャーの動作設定</title>
<p>You can control whether you single-click or double-click files, how
executable text files are handled, and the trash behavior. Click the menu
button in the top-right corner of the window and select <gui>Preferences</gui>,
and select the <gui>Behavior</gui> tab.</p>

<section id="behavior">
<title>動作</title>
<terms>
 <item>
  <title><gui>シングルクリックでアイテムを開く</gui></title>
  <title><gui>ダブルクリックでアイテムを開く</gui></title>
  <p>デフォルトでは、シングルクリックでファイルを選択し、ダブルクリックでファイルを開きます。代わりに、シングルクリックでファイルやフォルダーを開くようにもできます。シングルクリックモードでは、<key>Ctrl</key> キーを押しながらクリックすることで、1 つ以上のファイルを選択できます。</p>
 </item>
</terms>

</section>
<section id="executable">
<title>実行可能なテキストファイル</title>
 <p>An executable text file is a file that contains a program that you can run
 (execute). The <link xref="nautilus-file-properties-permissions">file
 permissions</link> must also allow for the file to run as a program. The most
 common are <sys>Shell</sys>, <sys>Python</sys> and <sys>Perl</sys> scripts.
 These have extensions <file>.sh</file>, <file>.py</file> and <file>.pl</file>,
 respectively.</p>
 
 <p>When you open an executable text file, you can select from:</p>
 
 <list>
  <item>
    <p><gui>Run executable text files when they are opened</gui></p>
  </item>
  <item>
    <p><gui>View executable text files when they are opened</gui></p>
  </item>
  <item>
    <p><gui>Ask each time</gui></p>
  </item>
 </list>

 <p>If <gui>Ask each time</gui> is selected, a dialog will pop up asking if you
 wish to run or view the selected text file.</p>

 <p>Executable text files are also called <em>scripts</em>. All scripts in the
 <file>~/.local/share/nautilus/scripts</file> folder will appear in the context
 menu for a file under the <gui style="menuitem">Scripts</gui> submenu. When a
 script is executed from a local folder, all selected files will be pasted to
 the script as parameters. To execute a script on a file:</p>

<steps>
  <item>
    <p>Navigate to the desired folder.</p>
  </item>
  <item>
    <p>Select the desired file.</p>
  </item>
  <item>
    <p>Right click on the file to open the context menu and select the desired
    script to execute from the <gui style="menuitem">Scripts</gui> menu.</p>
  </item>
</steps>

 <note style="important">
  <p>A script will not be passed any parameters when executed from a remote
  folder such as a folder showing web or <sys>ftp</sys> content.</p>
 </note>

</section>

<section id="trash">
<info>
<link type="seealso" xref="files-delete"/>
<title type="link">ファイルマネージャーのゴミ箱設定</title>
</info>
<title>ゴミ箱</title>

<terms>
 <item>
  <title><gui>Ask before emptying the Trash</gui></title>
  <p>このオプションはデフォルトで選択されています。ゴミ箱を空にする操作が行われると、確認メッセージが表示され、本当にゴミ箱を空にする/ファイルを削除するか尋ねられます。</p>
 </item>
</terms>
</section>

</page>
