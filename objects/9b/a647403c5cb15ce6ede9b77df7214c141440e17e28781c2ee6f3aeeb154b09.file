<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-switch-tasks" version="1.0 if/1.0" xml:lang="gl">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>
    <link type="guide" xref="getting-started" group="videos"/>
    <title role="trail" type="link">Cambiar entre tarefas</title>
    <link type="seealso" xref="shell-windows-switching"/>
    <title role="seealso" type="link">Un titorial sobre como cambiar entre tarefas</title>
    <link type="next" xref="gs-use-windows-workspaces"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2013-2020.</mal:years>
    </mal:credit>
  </info>

  <title>Cambiar entre tarefas</title>

  <ui:overlay width="812" height="452">
  <media type="video" its:translate="no" src="figures/gnome-task-switching.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-task-switching.svg"/>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>Cambiando entre tarefas</tt:p>
         </tt:div>
         <tt:div begin="5s" end="8s">
           <tt:p if:test="!platform:gnome-classic">Mova o punteiro do seu rato á esquina <gui>Actividades</gui> na parte superior esquerda da pantalla.</tt:p>
           </tt:div>
         <tt:div begin="9s" end="12s">
           <tt:p>Prema unha xanela para cambiar a dita tarefa.</tt:p>
         </tt:div>
         <tt:div begin="12s" end="14s">
           <tt:p>Para maximizar unha xanela á parte esquerda da pantalla, arrastre a barra de título da xanela e arrástrea cara a esquerda.</tt:p>
         </tt:div>
         <tt:div begin="14s" end="16s">
           <tt:p>Cando se realce a metade da pantalla, solte a xanela.</tt:p>
         </tt:div>
         <tt:div begin="16s" end="18">
           <tt:p>Para maximizar unha xanela á parte dereita, arrastre a barra de título da xanela e arrástrea cara a dereita.</tt:p>
         </tt:div>
         <tt:div begin="18s" end="20s">
           <tt:p>Cando se realce a metade da pantalla, solte a xanela.</tt:p>
         </tt:div>
         <tt:div begin="23s" end="27s">
           <tt:p>Prema <keyseq><key href="help:gnome-help/keyboard-key-super">Super</key><key> Tab</key></keyseq> para mostrar o <gui>trocador de xanelas</gui>.</tt:p>
         </tt:div>
         <tt:div begin="27s" end="29s">
           <tt:p>Solte a tecla <key href="help:gnome-help/keyboard-key-super">Super</key> para seleccionar a seguinte xanela realzada.</tt:p>
         </tt:div>
         <tt:div begin="29s" end="32s">
           <tt:p>Para moverse pola lista de xanelas abertas, prema e manteña a tecla <key href="help:gnome-help/keyboard-key-super">Super</key> e logo prema <key>Tab</key>.</tt:p>
         </tt:div>
         <tt:div begin="35s" end="37s">
           <tt:p>Prema a tecla <key href="help:gnome-help/keyboard-key-super">Super</key> para mostrar a <gui>Vista xeral de actividades</gui>.</tt:p>
         </tt:div>
         <tt:div begin="37s" end="40s">
           <tt:p>Comece a escribir o nome da aplicación ao que quere cambiar.</tt:p>
         </tt:div>
         <tt:div begin="40s" end="43s">
           <tt:p>Cando a aplicación apareza como primeiro resultado, prema <key>Intro</key> para cambiar a el.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

  <section id="switch-tasks-overview">
    <title>Cambiar entre tarefas</title>

<if:choose>
<if:when test="!platform:gnome-classic">

  <steps>
    <item><p>Mova o seu punteiro de rato á esquina de <gui>Actividades</gui> na parte superior esquerda da pantalla  para mostrar a <gui>Vista xeral de actividades</gui> onde pode ver as tarefas executándose actualmente mostradas como xanelas pequenas.</p></item>
     <item><p>Prema unha xanela para cambiar a dita tarefa.</p></item>
  </steps>

</if:when>
<if:when test="platform:gnome-classic">

  <steps>
    <item><p>Pode cambiar entre tarefas usando a <gui>lista de xanelas</gui> na parte inferior da xanela. As tarefas abertas aparecen como botones na <gui>lista de xanelas</gui>.</p></item>
     <item><p>Prema un botón na <gui>lista de xanelas</gui> para cambiar a dita tarefa.</p></item>
  </steps>

</if:when>
</if:choose>

  </section>

  <section id="switching-tasks-tiling">
    <title>Poñer as xanelas en mosaico</title>
    
    <steps>
      <item><p>Para maximizar unha xanela a longo dunha parte da pantalla, arrastre a barra de título da xanela cara a parte esquerda ou dereita da pantalla.</p></item>
      <item><p>Cando estea realzada a pantalla, solte a xanela para maximizar ao longo do lado seleccionado da pantalla.</p></item>
      <item><p>Para maximizar dúas xanelas unha ao carón da outra, arrastre a barra de título da segunda xanela ao lado oposto da pantalla.</p></item>
       <item><p>Cando estea realzada a pantalla, solte a xanela para maximizar ao longo do lado oposto da pantalla.</p></item>
    </steps>
    
  </section>
  
  <section id="switch-tasks-windows">
    <title>Cambiar entre xanelas</title>
    
  <steps>
   <item><p>Prema <keyseq><key href="help:gnome-help/keyboard-key-super">Super</key> <key>Tab</key></keyseq> para mostrar o <gui>Trocador de xanelas</gui>, que mostra unha lista das xanelas abertas actualmente.</p></item>
   <item><p>Solte a tecla <key href="help:gnome-help/keyboard-key-super">Super</key> para seleccionar a xanela realzada no Trocador de xanelas.</p>
   </item>
   <item><p>Para moverse pola lista de xanelas abertas, prema e manteña a tecla <key href="help:gnome-help/keyboard-key-super">Super</key> e logo prema <key>Tab</key>.</p></item>
  </steps>

  </section>

  <section id="switch-tasks-search">
    <title>Usar a busca para trocar entre as aplicacións</title>
    
    <steps>
      <item><p>Prema a tecla <key href="help:gnome-help/keyboard-key-super">Super</key> para mostrar a <gui>Vista xeral de actividades</gui>.</p></item>
      <item><p>Simplemente comece a escribir o nome da aplicación ao que quere cambiar. Os aplicacións que coincidan co que escribiu aparecerán mentres escriba.</p></item>
      <item><p>Cando a aplicación ao que quere cambiar apareza como primeiro resultado, prema <key>Intro</key> para cambiar a el.</p></item>
      
    </steps>
    
  </section>

</page>
