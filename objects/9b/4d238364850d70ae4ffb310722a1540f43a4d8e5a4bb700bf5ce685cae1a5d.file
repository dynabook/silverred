<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="net-what-is-ip-address" xml:lang="da">

  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>An IP Address is like a phone number for your computer.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years/>
    </mal:credit>
  </info>

  <title>Hvad er en IP-adresse?</title>

  <p>“IP-adresse” står for <em>internetprotokol-adresse</em>, og hver enhed som er forbundet til et netværk (såsom internettet) har en.</p>

  <p>An IP address is similar to your phone number. Your phone number is a
  unique set of numbers that identifies your phone so that other people can
  call you. Similarly, an IP address is a unique set of numbers that identifies
  your computer so that it can send and receive data with other computers.</p>

  <p>Currently, most IP addresses consist of four sets of numbers, each
  separated by a period. <code>192.168.1.42</code> is an example of an IP
  address.</p>

  <note style="tip">
    <p>An IP address can either be <em>dynamic</em> or <em>static</em>. Dynamic
    IP addresses are temporarily assigned each time your computer connects to a
    network. Static IP addresses are fixed, and do not change. Dynamic IP
    addresses are more common that static addresses — static addresses are
    typically only used when there is a special need for them, such as in the
    administration of a server.</p>
  </note>

</page>
