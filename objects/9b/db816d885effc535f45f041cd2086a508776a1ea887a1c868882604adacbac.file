<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="printing-2sided" xml:lang="ru">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Фил Булл (Phil Bull)</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Джим Кэмпбелл (Jim Campbell)</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Печать на обеих сторонах бумаги или печать нескольких страниц на листе.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Двусторонняя печать и печать нескольких страниц на листе</title>

  <p>Чтобы печатать на обеих сторонах каждого листа бумаги:</p>

  <steps>
    <item>
      <p>Open the print dialog by pressing
      <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>Перейдите на вкладку <gui>Параметры страницы</gui> окна печати и выберите нужное значение из выпадающего списка <gui>Двусторонняя печать</gui>. Если он неактивен, значит ваш принтер не поддерживает двустороннюю печать.</p>
      <p>Printers handle two-sided printing in different ways. It is a good
      idea to experiment with your printer to see how it works.</p>
    </item>
    <item>
      <p>Можно также распечатать несколько страниц на одной <em>стороне</em> листа бумаги. Используйте для этого параметр <gui>Страниц на сторону</gui>.</p>
    </item>
  </steps>

  <note>
    <p>Доступность этих параметров может зависеть от используемого типа принтера, а также от используемого приложения. Не всегда все из этих параметров могут быть доступны.</p>
  </note>

</page>
