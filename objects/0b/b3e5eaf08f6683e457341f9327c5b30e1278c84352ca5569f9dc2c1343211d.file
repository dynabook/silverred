<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="extensions" xml:lang="ca">

  <info>
    <link type="guide" xref="software#extension"/>
    <link type="seealso" xref="extensions-lockdown"/>
    <link type="seealso" xref="extensions-enable"/>
    <revision pkgversion="3.9" date="2013-08-07" status="review"/>

    <credit type="author copyright">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Les extensions del GNOME Shell permet personalitzar la interfície del GNOME Shell.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

  <title>Què són les extensions del GNOME Shell?</title>
  
  <p>Les extensions del GNOME Shell permeten personalitzar la interfície i les seves parts, com la gestió de les finestres i l'arrencada de les aplicacions.</p>
  
  <p>Cada extensió del GNOME Shell s'identifica amb un identificador únic, el uuid. El uuid també s'utilitza com a nom del directori on s'ha instal·lat l'extensió. També es pot instal·lar l'extensió per usuari a <file>~/.local/share/gnome-shell/extensions/&lt;uuid&gt;</file>, o per l'equip a <file>/usr/share/gnome-shell/extensions/&lt;uuid&gt;</file>.</p>
  
  <p>Per veure les extensions instal·lades, es pot utilitzar la comanda de depuració integrada i l'eina d'inspecció de GNOME <app>Looking Glass</app>.</p>
  
  <steps>
    <title>Mostrar les extensions instal·lades</title>
    <item>
      <p>Premeu <keyseq type="combo"><key>Alt</key><key>F2</key></keyseq>, teclegeu <em>lg</em> i premeu <key>Retorn</key> per obrir <app>Looking Glass</app>.</p>
    </item>
    <item>
      <p>A la barra superior de <app>Looking Glass</app>, feu clic a <gui>Extensions</gui> per obrir la llista d'extensions instal·lades.</p>
    </item>
  </steps>

</page>
