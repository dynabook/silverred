<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-delete" xml:lang="cs">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>
    <link type="seealso" xref="files-recover"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-16" status="review"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="candidate"/>
    <revision pkgversion="3.16" date="2015-02-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak odstranit soubory a složky, které nadále nepotřebujete.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

<title>Mazání souborů a složek</title>

  <p>Pokud některý soubor nebo složku již nechcete, můžete je smazat. Když položku vymažete, přesune se do složky <gui>Koš</gui>, ve které bude uchována, dokud koš nevysypete. V případě, že zjistíte, že je přece jen potřebujete, nebo je smažete omylem, můžete ze složky <gui>Koš</gui> <link xref="files-recover">položky obnovit</link> na jejich původní umístění.</p>

  <steps>
    <title>Když chcete soubor vyhodit do koše:</title>
    <item><p>Jedním kliknutím vyberte položku, kterou chcete vyhodit do koše.</p></item>
    <item><p>Zmáčkněte klávesu <key>Delete</key>. Případně můžete položku přetáhnout do <gui>Koše</gui> na postranní liště.</p></item>
  </steps>

  <p>Soubor bude přesunut do koše a vám bude nabídnuta možnost smazání <gui>Vrátit</gui>. Tlačítko <gui>Vrátit</gui> se objeví jen na pár vteřin. Pokud jej použijete, soubor se obnoví na své původní místo.</p>

  <p>Abyste soubory vymazali trvale a uvolnili místo na disku, musíte koš vysypat. Dělá se to tak, že se klikne pravým tlačítkem na <gui>Koš</gui> v postranním panelu a vybere se <gui>Vyprázdnit koš</gui>.</p>

  <section id="permanent">
    <title>Trvalé smazání souboru</title>
    <p>Soubor můžete vymazat trvale také hned, bez mezikroku s jeho posíláním do koše.</p>

  <steps>
    <title>Když chcete soubor trvale smazat:</title>
    <item><p>Vyberte položku, kterou chcete smazat.</p></item>
    <item><p>Zmáčkněte a držte klávesu <key>Shift</key> a pak zmáčkněte klávesu <key>Delete</key>.</p></item>
    <item><p>Protože tuto operaci nelze vrátit zpět, budete dotázáni na potvrzení, že soubor nebo složku opravdu chcete smazat.</p></item>
  </steps>

  <note><p>Smazané soubory na <link xref="files#removable">výměnných médiích</link> nebudou vidět v jiných operačních systémech, jako jsou Windows nebo Mac OS. Soubory na disku ale stále budou a dostanete se k nim, jakmile zařízení přípojíte opět ke svému počítači.</p></note>

  </section>

</page>
