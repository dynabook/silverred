<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="color-whyimportant" xml:lang="ru">

  <info>
    <link type="guide" xref="color"/>
    <desc>Управление цветом важно для дизайнеров, фотографов и художников.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Почему важно управление цветом?</title>
  <p>Управление цветом — это процесс получения цвета устройством ввода, отображения его на экране и вывода на печать, сохраняя точность передачи цветов и их диапазон для каждого устройства.</p>

  <p>Потребность в управлении цветом проще всего проиллюстрировать следующей фотографией птицы в морозный зимний день.</p>

  <figure>
    <desc>Птица в морозный зимний день. Как это выглядит в видоискателе камеры.</desc>
    <media its:translate="no" type="image" mime="image/png" src="figures/color-camera.png"/>
  </figure>

  <p>Мониторы как правило перенасыщают синий канал, что делает изображение более холодным.</p>

  <figure>
    <desc>Так это выглядит на экране типичного ноутбука.</desc>
    <media its:translate="no" type="image" mime="image/png" src="figures/color-display.png"/>
  </figure>

  <p>
    Notice how the white is not “paper white” and the black of the eye
    is now a muddy brown.
  </p>

  <figure>
    <desc>Так это выглядит на при печати на типичном струйном принтере.</desc>
    <media its:translate="no" type="image" mime="image/png" src="figures/color-printer.png"/>
  </figure>

  <p>Основная проблема заключается в том, что каждое устройство передаёт различный диапазон цветов. Даже если вам удастся сфотографировать предмет цвета электрик, большинство принтеров не сможет его воспроизвести.</p>
  <p>
    Most image devices capture in RGB (Red, Green, Blue) and have
    to convert to CMYK (Cyan, Magenta, Yellow, and Black) to print.
    Another problem is that you can’t have <em>white</em> ink, and so
    the whiteness can only be as good as the paper color.
  </p>

  <p>
    Another problem is units. Without specifying the scale on which a
    color is measured, we don’t know if 100% red is near infrared or
    just the deepest red ink in the printer. What is 50% red on one
    display is probably something like 62% on another display.
    It’s like telling a person that you’ve just driven 7 units of
    distance, without the unit you don’t know if that’s 7 kilometers or
    7 meters.
  </p>

  <p>
    In color, we refer to the units as gamut. Gamut is essentially the
    range of colors that can be reproduced.
    A device like a DSLR camera might have a very large gamut, being able
    to capture all the colors in a sunset, but a projector has a very
    small gamut and all the colors are going to look “washed out”.
  </p>

  <p>
    In some cases we can <em>correct</em> the device output by altering
    the data we send to it, but in other cases where that’s not
    possible (you can’t print electric blue) we need to show the user
    what the result is going to look like.
  </p>

  <p>
    For photographs it makes sense to use the full tonal range of a color
    device, to be able to make smooth changes in color.
    For other graphics, you might want to match the color exactly, which
    is important if you’re trying to print a custom mug with the Red Hat
    logo that <em>has</em> to be the exact Red Hat Red.
  </p>

</page>
