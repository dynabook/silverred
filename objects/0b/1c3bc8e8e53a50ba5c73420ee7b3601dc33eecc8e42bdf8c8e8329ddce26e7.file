<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="contacts-link-unlink" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="contacts"/>
    <revision pkgversion="3.5.5" date="2012-02-19" status="review"/>
    <revision pkgversion="3.8" date="2013-04-27" status="review"/>
    <revision pkgversion="3.12" date="2014-02-27" status="final"/>
    <revision pkgversion="3.15" date="2015-01-28" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Combine informações para um contato de múltiplas origens.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

<title>Vinculando e desvinculando contatos</title>

<section id="link-contacts">
  <title>Vincular contatos</title>

  <p>Você pode combinar contatos do seu catálogo de endereços local e contas on-line em uma só entrada em <app>Contatos</app>. Esse recurso ajuda você a manter seu catálogo de endereços organizado, com todos os detalhes sobre um contato em um só lugar.</p>

  <steps>
    <item>
      <p>Habilite o <em>modo de seleção</em> pressionando o botão de seleção acima da lista de contatos.</p>
    </item>
    <item>
      <p>Uma caixa de seleção vai aparecer ao lado de cada contato. Marque as caixas de seleção ao lado dos contatos que você deseja mesclar.</p>
    </item>
    <item>
      <p>Pressione <gui style="button">Link</gui> para vincular os contatos selecionados.</p>
    </item>
  </steps>

</section>

<section id="unlink-contacts">
  <title>Desvincular contatos</title>

  <p>Você pode querer desvincular contatos, caso você tenha acidentalmente vinculado contatos que não deveriam estar vinculados.</p>

  <steps>
    <item>
      <p>Selecione o contato que você gostaria de desvincular da sua lista de contatos.</p>
    </item>
    <item>
      <p>Pressione <gui style="button">Editar</gui> no canto superior direito de <app>Contatos</app>.</p>
    </item>
    <item>
      <p>Pressione <gui style="button">Contas vinculadas</gui>.</p>
    </item>
    <item>
      <p>Pressione <gui style="button">Desvincular</gui> para desvincular o registro do contato.</p>
    </item>
    <item>
      <p>Feche a janela assim que você tiver finalizado de desvincular os registros.</p>
    </item>
    <item>
      <p>Pressione <gui style="button">Concluído</gui> para finalizar a edição do contato.</p>
    </item>
  </steps>

</section>

</page>
