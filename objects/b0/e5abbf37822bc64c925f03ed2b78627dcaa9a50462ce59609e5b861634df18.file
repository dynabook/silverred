<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="sound-nosound" xml:lang="as">

  <info>
    <link type="guide" xref="sound-broken"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.18" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>GNOME তথ্যচিত্ৰ প্ৰকল্প</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>মাইকেল হিল</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Check that the sound is not muted, that cables are plugged in properly,
    and that the sound card is detected.</desc>
  </info>

<title>I cannot hear any sounds on the computer</title>

  <p>If you cannot hear any sounds on your computer, for example when you try
  to play music, go through the following troubleshooting tips.</p>

<section id="mute">
  <title>শব্দ মৌন হয় নে পৰিক্ষা কৰক</title>

  <p>Open the <gui xref="shell-introduction#systemmenu">system menu</gui> and make sure that
  the sound is not muted or turned down.</p>

  <p>Some laptops have mute switches or keys on their keyboards — try pressing
  that key to see if it unmutes the sound.</p>

  <p>You should also check that you have not muted the application that you are
  using to play sound (for example, your music player or movie player). The
  application may have a mute or volume button in its main window, so check
  that.</p>

  <p>Also, you can check the <gui>Applications</gui> tab in the <gui>Sound</gui>
  GUI:</p>
  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
      overview and start typing <gui>Sound</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Sound</gui> to open the panel.</p>
    </item>
    <item>
      <p>Under <gui>Volume Levels</gui>, check that your application is not
      muted.</p>
    </item>
  </steps>

</section>

<section id="speakers">
  <title>স্পিকাৰসমূহ অন আৰু সঠিকভাৱে সংযুক্ত নে পৰিক্ষা কৰক</title>
  <p>If your computer has external speakers, make sure that they are turned on
  and that the volume is turned up. Make sure that the speaker cable is securely
  plugged into the “output” audio socket on your computer. This socket
  is usually light green in color.</p>

  <p>Some sound cards can switch between the socket they use for output
  (to the speakers) and the socket for input (from a microphone, for instance).
  The output socket may be different when running Linux, Windows or Mac OS.
  Try connecting the speaker cable to a different audio socket on your
  computer.</p>

 <p>A final thing to check is that the audio cable is securely plugged into the
 back of the speakers. Some speakers have more than one input, too.</p>
</section>

<section id="device">
  <title>Check that the correct sound device is selected</title>

  <p>Some computers have multiple “sound devices” installed. Some of these are
  capable of outputting sound and some are not, so you should check that you
  have the correct sound device selected. This might involve some
  trial-and-error to choose the right one.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
      overview and start typing <gui>Sound</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Sound</gui> to open the panel.</p>
    </item>
    <item>
      <p>Under <gui>Output</gui>, change the <gui>Profile</gui> settings for the
      selected device and play a sound to see if it works. You might need to go
      through the list and try each profile.</p>

      <p>If that does not work, you might want to try doing the same for any
      other devices that are listed.</p>
    </item>
  </steps>

</section>

<section id="hardware-detected">

 <title>শব্দ কাৰ্ড সঠিকভাৱে চিনাক্ত কৰা হৈছে নে পৰিক্ষা কৰক</title>

  <p>Your sound card may not have been detected properly probably because 
  the drivers for the card are not installed. You may need to install the drivers
  for the card manually. How you do this depends on the type of the card. </p>

  <p>Run the <cmd>lspci</cmd> command in the Terminal to find out what sound
  card you have:</p>
  <steps>
    <item>
      <p><gui>কাৰ্য্যসমূহ</gui> অভাৰভিউত যাওক আৰু এটা টাৰ্মিনেল খোলক।</p>
    </item>
    <item>
      <p>Run <cmd>lspci</cmd> as <link xref="user-admin-explain">superuser</link>;
      either type <cmd>sudo lspci</cmd> and type your password, or type
      <cmd>su</cmd>, enter the <em>root</em> (administrative) password,
      then type <cmd>lspci</cmd>.</p>
    </item>
    <item>
      <p>Check if an <em>audio controller</em> or <em>audio device</em> is listed:
      in such case you should see the make and model number of the sound card. 
      Also, <cmd>lspci -v</cmd> shows a list with more detailed information.</p>
    </item>
  </steps>

  <p>You may be able to find and install drivers for your card. It is best to
  ask on support forums (or otherwise) for your Linux distribution for
  instructions.</p>

  <p>If you cannot get drivers for your sound card, you might prefer to buy a
  new sound card. You can get sound cards that can be installed inside the
  computer and external USB sound cards.</p>

</section>

</page>
