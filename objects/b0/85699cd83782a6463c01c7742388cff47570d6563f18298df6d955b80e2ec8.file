<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-dwellclick" xml:lang="id">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="clicking"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-21" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Fitur <gui>Klik Mengambang</gui> (Klik Menetap) memungkinkan Anda mengklik dengan cara menahan tetikus tetap diam.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

  <title>Mensimulasi klik dengan mengambang</title>

  <p>Anda dapat mengklik atau menyeret hanya dengan mengambangkan pointer tetikus Anda di atas suatu kendali atau objek pada layar. Ini berguna bila Anda merasa sulit untuk menggerakkan tetikus dan mengklik pada saat yang sama. Fitur ini dinamai <gui>Klik Mengambang</gui> atau Klik Menetap.</p>

  <p>Ketika <gui>Klik Mengambang</gui> difungsikan, Anda dapat memindah pointer tetikus Anda lewat suatu kendali, melepas tetikus, lalu menunggu sejenak sebelum tombol akan diklikkan bagi Anda.</p>

  <steps>
    <item>
      <p>Buka ringkasan <gui xref="shell-introduction#activities">Aktivitas</gui> dan mulai mengetik <gui>Akses Universal</gui>.</p>
    </item>
    <item>
      <p>Klik <gui>Akses Universal</gui> untuk membuka panel.</p>
    </item>
    <item>
      <p>Tekan <gui>Bantu Klik</gui> dalam seksi <gui>Menunjuk &amp; Mengklik</gui>.</p>
    </item>
    <item>
      <p>Switch <gui>Hover Click</gui> to on.</p>
    </item>
  </steps>

  <p>Jendela <gui>Klik Mengambang</gui> akan terbuka, dan akan bertahan di atas semua jendela lain Anda. Anda dapat menggunakan ini untuk memilih apa jenis klik yang harus terjadi ketika Anda mengambang. Misalnya, jika Anda memilih <gui>Klik Sekunder</gui>, Anda akan klik kanan ketika Anda mengambang. Setelah Anda klik ganda, klik kanan, atau menyeret, Anda akan secara otomatis kembali ke mengklik.</p>

  <p>Ketika Anda mengambangkan penunjuk tetikus Anda di atas suatu tombol dan tidak menggerakkannya, itu akan berangsur-angsur berubah warna. Ketika warnanya sudah berubah sepenuhnya, tombol akan diklik.</p>

  <p>Setel pengaturan <gui>Tundaan</gui> untuk mengubah berapa lama Anda mesti menahan penunjuk tetikus tetap diam sebelum mengklik.</p>

  <p>Anda tak perlu menahan tetikus diam sempurna ketika mengambang untuk mengklik. Penunjuk boleh bergerak sedikit dan masih akan mengklik setelah beberapa saat. Namun bila bergerak terlalu banyak, klik tak akan terjadi.</p>

  <p>Setel pengaturan <gui>Ambang pergerakan</gui> untuk mengubah seberapa banyak penunjuk boleh bergerak dan masih dianggap mengambang.</p>

</page>
