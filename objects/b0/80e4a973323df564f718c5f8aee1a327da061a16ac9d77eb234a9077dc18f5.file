<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-calibrate-camera" xml:lang="id">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-calibrationtargets"/>
    <link type="seealso" xref="color-calibrate-printer"/>
    <link type="seealso" xref="color-calibrate-scanner"/>
    <link type="seealso" xref="color-calibrate-screen"/>
    <desc>Mengkalibrasi kamera Anda penting untuk dapat menangkap warna-warna yang akurat.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

  <title>Bagaimana saya mengkalibrasi kamera saya?</title>

  <p>Perangkat kameda dikalibrasi dengan cara mengambil foto dari suatu target di bawah kondisi pencahayaan yang diinginkan. Dengan mengonversi berkas RAW ke berkas TIFF, itu dapat dipakai untuk mengkalibrasi perangkat kamera dalam panel kendali warna.</p>
  <p>Anda akan perlu memotong berkas TIFF sehingga hanya target yang nampak. Pastikan bahwa tepi putih atau hitam masih nampak. Kalibrasi tak akan bekerja bila gambar terbalik atau cukup banyak terdistorsi.</p>

  <note style="tip">
    <p>Profil hasil hanya valid di bawah kondisi pencahayaan yang Anda peroleh dari asal gambar asli. Ini berarti Anda mungkin perlu membuat profil beberapa kali bagi kondisi pencahayaan <em>studio</em>, <em>cahaya terang matahari</em>, dan <em>berawan</em>.</p>
  </note>

</page>
