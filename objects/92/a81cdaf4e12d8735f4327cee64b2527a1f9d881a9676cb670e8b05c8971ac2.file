<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-findip" xml:lang="el">

  <info>
    <link type="guide" xref="net-general"/>
    <link type="seealso" xref="net-what-is-ip-address"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Γνωρίζοντας την διεύθυνση IP σας μπορεί να σας βοηθήσει να ανιχνεύσετε τα προβλήματα του δικτύου.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Βρείτε τη διεύθυνση IP σας</title>

  <p>Γνωρίζοντας τη διεύθυνση IP σας μπορεί να σας βοηθήσει να ανιχνεύσετε προβλήματα με την σύνδεσή σας στο διαδίκτυο. Μπορεί να εκπλαγείτε μαθαίνοντας ότι έχετε <em>δύο</em> διευθύνσεις IP: μια διεύθυνση IP για τον υπολογιστή σας στο εσωτερικό δίκτυο και μια διεύθυνση IP για τον υπολογιστή σας στο διαδίκτυο.</p>

  <steps>
    <title>Βρείτε την εσωτερική σας (δικτυακή) διεύθυνση IP</title>
    <item>
      <p>Ανοίξτε την επισκόπηση <gui xref="shell-introduction#activities">Δραστηριότητες</gui> και αρχίστε να πληκτρολογείτε <gui>Δίκτυο</gui>.</p>
    </item>
    <item>
      <p>Κάντε κλικ στο <gui>Δίκτυο</gui> για να ανοίξετε τον πίνακα.</p>
    </item>
    <item>
      <p>Επιλέξτε μια σύνδεση, <gui>Wi-Fi</gui> ή <gui>Ενσύρματη</gui>, από τον αριστερό πίνακα.</p>
      <p>Η διεύθυνση IP  για μια ενσύρματη σύνδεση εμφανίζεται στα δεξιά.</p>
      
      <p>Κάντε κλικ στο κουμπί <media its:translate="no" type="image" src="figures/emblem-system.png"><span its:translate="yes">ρυθμίσεις</span></media> για να δείτε την διεύθυνση IP του ασύρματου δικτύου στον πίνακα <gui>Λεπτομέρειες</gui>.</p>
    </item>
  </steps>

  <steps>
  	<title>Βρείτε την εξωτερική σας (διαδικτυακή) διεύθυνση IP</title>
    <item>
      <p>Επισκεφθείτε το <link href="http://whatismyipaddress.com/">whatismyipaddress.com</link>.</p>
    </item>
    <item>
      <p>Ο ιστότοπος θα εμφανίσει την εξωτερική σας διεύθυνση IP για σας.</p>
    </item>
  </steps>

  <p>Ανάλογα με το πώς ο υπολογιστής σας συνδέεται στο διαδίκτυο, και οι δυο διευθύνσεις μπορεί να είναι οι ίδιες.</p>

</page>
