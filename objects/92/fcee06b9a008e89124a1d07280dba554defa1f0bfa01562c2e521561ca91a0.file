<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-mode" xml:lang="sr-Latn">

  <info>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.28" date="2018-07-22" status="review"/>
    <revision pkgversion="3.33" date="2019-07-21" status="candidate"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Prebacujte tablicu između režima tablice i režima miša.</desc>
  </info>

  <title>Podesite režim praćenja Vakom tablice</title>

<p><gui>Režim praćenja</gui> određuje način mapiranja pokazivača na ekranu.</p>

<steps>
  <item>
    <p>Otvorite pregled <gui xref="shell-introduction#activities">Aktivnosti</gui> i počnite da kucate <gui>Vakom tablica</gui>.</p>
  </item>
  <item>
    <p>Kliknite na <gui>Vakom tablica</gui> da otvorite panel.</p>
  </item>
  <item>
    <p>Click the <gui>Tablet</gui> button in the header bar.</p>
    <note style="tip"><p>Ako tablica nije otkrivena, od vas će biti zatraženo da <gui>priključite ili upalite vašu Vakom tablicu</gui>. Kliknite na vezu <gui>Podešavanja blututa</gui> da povežete bežičnu tablicu.</p></note>
  </item>
  <item><p>Pored <gui>Režima praćenja</gui>, izaberite <gui>Tablica (apsolutno)</gui> ili <gui>Dodirna tabla (relativno)</gui>.</p></item>
</steps>

<note style="info"><p>U <em>apsolutnom</em> režimu, svaka tačka na tablici se mapira na tačku na ekranu. Gornji levi ugao ekrana, na primer, uvek odgovara istoj tački na tablici.</p>
 <p>U <em>relativnom</em> režimu, ako podignete pokazivač tablice i spustite ga na drugo mesto, kursor na ekranu se ne pomera. Ovo je način na koji radi miš.</p>
  </note>

</page>
