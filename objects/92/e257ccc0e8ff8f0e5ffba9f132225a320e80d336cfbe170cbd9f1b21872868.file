<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="bluetooth-visibility" xml:lang="id">

  <info>
    <link type="guide" xref="bluetooth" group="#last"/>
    <link type="seealso" xref="bluetooth-device-specific-pairing"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-04" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Apakah perangkat lain dapat menemukan komputer Anda.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

  <title>Apa itu kenampakan Bluetooth?</title>

  <p>Bluetooth visibility refers to whether other devices can discover
  your computer when searching for Bluetooth devices. When Bluetooth is turned
  on and the <gui>Bluetooth</gui> panel is open, your computer will advertise
  itself to all other devices within range, allowing them to attempt to connect
  to your computer.</p>

  <note style="tip">
    <p>Anda dapat <link xref="sharing-displayname">mengubah</link> nama komputer Anda yang ditampilkan ke perangkat lain.</p>
  </note>

  <p>Setelah Anda <link xref="bluetooth-connect-device">tersambung ke suatu perangkat</link>, komputer Anda maupun perangkat tak perlu nampak untuk saling berkomunikasi.</p>

  <p>Devices without a display usually have a pairing mode that can be entered
  by pressing a button, or a combination of buttons for a while, whether when
  they’ve already been turned on, or as they are being turned on.</p>

  <p>The best way to find out how to enter that mode is to refer to the device’s
  manual. For some devices, the procedure might be
  <link xref="bluetooth-device-specific-pairing">slightly different from usual</link>.
  </p>

</page>
