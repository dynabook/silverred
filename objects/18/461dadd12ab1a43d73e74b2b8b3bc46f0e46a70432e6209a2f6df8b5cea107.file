<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-wireless-disconnecting" xml:lang="cs">

  <info>
    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-problem"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <desc>Možná máte slabý signál nebo vám síť neumožňuje správné připojení.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

<title>Proč se moje síť pořád odpojuje?</title>

<p>Můžete se setkat s tím, že jste odpojeni od bezdrátové sítě, ačkoliv jste chtěli zůstat připojeni. Když se to stane, počítač se normálně bude snažit k síti připojit znovu (ikona sítě v horní liště bude během pokusu o znovupřipojení zobrazovat tři tečky), ale takové situace mohou být otravné, pokud například zrovna něco děláte na Internetu.</p>

<section id="signal">
 <title>Slabý bezdrátový signál</title>

 <p>Nejčastějším důvodem odpojení od bezdrátové sítě je, že máte slabý signál. Bezdrátové sítě mají omezený dosah, takže když jste příliš daleko od základny s vysílačem, nezískáte dostatečně silný signál pro obsluhu spojení. Navíc zdi a další objekty v přímé trase k vysílači signál zeslabují.</p>

 <p>Ikona sítě na horní liště zobrazuje, jak silný signál je. Pokud je slabý, pokuste se přesunout blíže k základní stanici s vysílačem.</p>

</section>

<section id="network">
 <title>Síťové spojení nebylo správně sestaveno</title>

 <p>Někdy se může stát, že když se připojujete k bezdrátové síti, vypadá to na první pohled, že jste se úspěšně připojili, ale vzápětí jste odpojeni. Je to proto, že počítač se připojil úspěšně jen z části – sice ustanovil spojení, ale nebylo možné z nějakého důvodu celou operaci dotáhnout do konce.</p>

 <p>Mezi možné důvody patří, že jste zadali nesprávné heslo nebo počítač nemá dovoleno se připojit do této sítě (například proto, že síť požaduje pro přihlášení uživatelské jméno).</p>

</section>

<section id="hardware">
 <title>Nespolehlivý bezdrátový hardware/ovladač</title>

 <p>Některý bezdrátový hardware může být lehce nespolehlivý. Bezdrátové sítě jsou složité, takže bezdrátové karty a stanice občas fungují s drobnými problémy, které ale mohou vést k zahození spojení. Je to sice nepříjemné, ale u řady zařízení se to stává. Pokud dojde k odpojení jen sem tam, může to být tento důvod. Jestli se to ale stává příliš často, je na zvážení výměna hardwaru.</p>

</section>

<section id="busy">
 <title>Přetížená bezdrátová síť</title>

 <p>Do bezdrátové sítě na vytíženém místě (např. univerzita nebo kavárna) se často snaží připojit hodně počítačů naráz. Takováto síť je pak někdy přetížená a nedokáže obsloužit všechny počítače, které se do ní snaží připojit, takže některé odmítne.</p>

</section>

</page>
