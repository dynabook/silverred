<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting" xml:lang="sr">

  <info>
    <link type="guide" xref="net-wireless" group="first"/>
    <link type="guide" xref="hardware#problems" group="first"/>
    <link type="next" xref="net-wireless-troubleshooting-initial-check"/>

    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Приложници викија Убунтуове документације</name>
    </credit>
    <credit type="author">
      <name>Гномов пројекат документације</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Одредите и исправите проблеме са бежичним везама.</desc>
  </info>

  <title>Решавање проблема бежичне мреже</title>

  <p>Ово је корак по корак водич за решавање проблема који ће вам помоћи да одредите и исправите бежичне проблеме. Ако не можете да се повежете на бежичну мрежу из неких разлога, покушајте да испратите ова упутства.</p>

  <p>Наставићемо са следећим корацима да бисмо остварили повезивање вашег рачунара на интернет:</p>

  <list style="numbered compact">
    <item>
      <p>Извршавање почетне провере</p>
    </item>
    <item>
      <p>Прикупљање података о вашим компонентама</p>
    </item>
    <item>
      <p>Проверавање ваших компоненти</p>
    </item>
    <item>
      <p>Покушај стварања везе са вашим бежичним усмеривачем</p>
    </item>
    <item>
      <p>Извршавање провере вашег модема и усмеривача</p>
    </item>
  </list>

  <p>Да започнете, кликните на везу <em>Следеће</em> горе десно на страници. Ова веза, и друге као она на наредним страницама, ће вас провести кроз све кораке у водичу.</p>

  <note>
    <title>Коришћење линије наредби</title>
    <p>Нека од упутстава у овом водичу траже од вас да уписујете наредбе у <em>линији наредби</em> (Терминал). Можете да пронађете програм <app>Терминал</app> у прегледу <gui>Активности</gui>.</p>
    <p>Ако нисте искусни у коришћењу линије наредби, не брините — овај водич ће вас водити при сваком кораку. Све што треба да запамтите је да су наредбе осетљиве на величину слова (зато морате да их уписујете <em>тачно</em> онако како се овде појављују), и да притиснете <key>Унеси</key> након уписивања сваке наредбе да је покренете.</p>
  </note>

</page>
