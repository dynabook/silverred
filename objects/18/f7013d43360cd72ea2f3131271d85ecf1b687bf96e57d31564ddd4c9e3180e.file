<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-calibrate-printer" xml:lang="es">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-calibrate-scanner"/>
    <link type="seealso" xref="color-calibrate-screen"/>
    <link type="seealso" xref="color-calibrate-camera"/>
    <desc>Calibrar su impresora es importante para imprimir colores precisos.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>¿Cómo calibrar la impresora?</title>

  <p>Existen dos formas de perfilar una impresora:</p>

  <list>
    <item><p>Usar un dispositivo espectrofotómetro como el Pantone ColorMunki</p></item>
    <item><p>Descargar un archivo de referencia de impresión de una empresa de color</p></item>
  </list>

  <p>Usar una empresa de color para generar un perfil de impresora es la opción más barata si tiene uno o dos tipos de papel diferentes. Descargando el gráfico de referencia desde el sitio web de la empresa, puede después enviarles la impresión en un sobre acolchado que ellos analizarán, generarán el perfil y le enviarán un perfil ICC preciso.</p>
  <p>Usar un dispositivo caro tal como ColorMunki sólo compensa si está perfilando un gran número de conjuntos de tinta o tipos de papeles.</p>

  <note style="tip">
    <p>Si cambia de proveedor de tinta, asegúrese de recalibrar la impresora.</p>
  </note>

</page>
