<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-language" xml:lang="it">

  <info>
    <link type="guide" xref="prefs-language"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Progetto documentazione di GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Usare un'altra lingua per l'interfaccia utente.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Cambiare la lingua utilizzata</title>

  <p>È possibile utilizzare il proprio sistema operativo in molte lingue diverse, assicurandosi di avere le corrette traduzioni installate nel sistema.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Region &amp; Language</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Region &amp; Language</gui> to open the panel.</p>
    </item>
    <item>
      <p>Click <gui>Language</gui>.</p>
    </item>
    <item>
      <p>Select your desired region and language. If your region and language
      are not listed, click
      <gui><media its:translate="no" type="image" mime="image/svg" src="figures/view-more-symbolic.svg"><span its:translate="yes">…</span></media></gui>
      at the bottom of the list to select from all available regions and
      languages.</p>
    </item>
    <item>
      <p>Click <gui style="button">Done</gui> to save.</p>
    </item>
    <item>
      <p>Respond to the prompt, <gui>Your session needs to be restarted for
      changes to take effect</gui> by clicking
      <gui style="button">Restart Now</gui>, or click
      <gui style="button">×</gui> to restart later.</p>
    </item>
  </steps>

  <p>Some translations may be incomplete, and certain applications may not
  support your language at all. Any untranslated text will appear in the
  language in which the software was originally developed, usually American
  English.</p>

  <p>Nella propria cartella utente sono presenti alcune cartelle speciali in cui le applicazioni salvano la musica, le immagini e i documenti. Queste cartelle utilizzano dei nomi standard basati sulla propria lingua: quando si esegue nuovamente l'accesso, viene chiesto se rinominare le cartelle in base alla lingua corrente. Nel caso la nuova lingua venga usata come quella predefinita per il sistema, è utile fare ciò.</p>

  <note style="tip">
    <p>If there are multiple user accounts on your system, there is a separate
    instance of the <gui>Region &amp; Language</gui> panel for the login screen.
    Click the <gui>Login Screen</gui> button at the top right to toggle between
    the two instances.</p>
  </note>

</page>
