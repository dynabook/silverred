<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-calibrate-printer" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-calibrate-scanner"/>
    <link type="seealso" xref="color-calibrate-screen"/>
    <link type="seealso" xref="color-calibrate-camera"/>
    <desc>Calibrar sua impressora é importante para imprimir cores de forma acurada.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

  <title>Como calibro minha impressora?</title>

  <p>Existem duas maneiras de gerar o perfil de uma impressora:</p>

  <list>
    <item><p>Usando um espectrofotômetro como o ColorMunki da Pantone</p></item>
    <item><p>Baixando e imprimindo um arquivo de referência de uma empresa de cores</p></item>
  </list>

  <p>Usar uma empresa de cores para gerar um perfil de impressora é normalmente a opção mais barata se você só tem um ou dois tipos diferentes de papel. Ao baixar a carta de referência do sítio web das empresas, você pode enviar-lhes de volta a impressão em um envelope acolchoado para que eles escaneiem o papel, gerem um perfil e lhe enviem um e-mail com o perfil ICC preciso.</p>
  <p>Usar um dispositivo caro como um ColorMunki sai barato só se você estiver gerando perfis de uma grande quantidade de conjunto de tintas e tipos de papel.</p>

  <note style="tip">
    <p>Se você trocar de fornecedor de tintas, certifique-se de recalibrar sua impressora!</p>
  </note>

</page>
