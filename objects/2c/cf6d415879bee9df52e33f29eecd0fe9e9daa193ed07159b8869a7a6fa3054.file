<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task a11y" id="mouse-doubleclick" xml:lang="gl">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="clicking"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9" date="2013-10-16" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="20156-06-15" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Controlar a velocidade de pulsación do botón do rato para efectuar unha pulsación dobre.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2020</mal:years>
    </mal:credit>
  </info>

  <title>Axustar a velocidade do dobre clic</title>

  <p>Double-clicking only happens when you press the mouse button twice
  quickly enough. If the second press is too long after the first, you’ll
  just get two separate clicks, not a double click. If you have difficulty
  pressing the mouse button quickly, you should increase the timeout.</p>

  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Actividades</gui> e comece a escribir <gui>Acceso universal</gui>.</p>
    </item>
    <item>
      <p>Prema <gui>Acceso universal</gui> para abrir o panel.</p>
    </item>
    <item>
      <p>Under <gui>Pointing &amp; Clicking</gui>, adjust the
      <gui>Double-Click Delay</gui> slider to a value you find comfortable.</p>
    </item>
  </steps>

  <p>Se o seu rato fai unha dobre pulsación cando vostede quere facer unha sola pulsación, aínda que incrementara o tempo de espera da pulsación dobre, é posíbel que estea defectuoso. Probe a conectar outro computador e vexa se funciona correctamente. Ou tamén, conecte o seu rato noutro computador distinto e comprobe se o problema persiste.</p>

  <note>
    <p>Este axuste afectará tanto ao seu rato como a súa área táctil, así como a calquera outro dispositivo apuntador.</p>
  </note>

</page>
