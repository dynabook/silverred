<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-disable-service" xml:lang="gl">

  <info>
    <link type="guide" xref="accounts"/>

    <revision pkgversion="3.5.5" date="2012-08-14" status="review"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Algunhas contas en liña poden usarse para acceder a múltiples servizos (como calendario e correo electrónico). Tamén pode controlar que servizos poden ser usados polas aplicacións.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2020</mal:years>
    </mal:credit>
  </info>

  <title>Controle que servizos en liña dunha conta poden usarse para acceder</title>

  <p>Algúns tipos de conta en liña permítenlle acceder a varios servizos coa mesma conta de usuario. Por exemplo, as contas de Google fornecen acceso ao calendario, correo-e, contactos e chat. Pode querer usar a súa conta para algúns servizos e para outros non. Por exemplo, pode querer usar a súa conta de Google para correo-e pero non para chat, xa que ten unha conta en liña distinta para usala para chatear.</p>

  <p>Pode desactivar algúns servizos fornecidos por cada servizo en liña:</p>

  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Actividades</gui> e comece a escribir <gui>Contas en liña</gui>.</p>
    </item>
    <item>
      <p>Prema <gui>Contas en liñla</gui> para abrir o panel.</p>
    </item>
    <item>
      <p>Seleccione a conta que quere cambiar da lista da dereita.</p>
    </item>
    <item>
      <p>A lista dos servizos servizos dispoñíbeis con esta conta mostraranse baixo <gui>Usar esta conta para</gui>. Vexa <link xref="accounts-which-application"/> para ver que aplicacións acceden a estos servizos.</p>
    </item>
    <item>
      <p>Desactive calquera dos servizos que non quere que sexan usados.</p>
    </item>
  </steps>

  <p>Unha vez que teña desactivado un servizo para unha conta, as aplicacións do seu computador non poderán usar a conta para volver a conectarse a este servizo.</p>

  <p>Para activar un servizo para teña desactivado, volva ao panel <gui>Contas en liña</gui> e actíveo.</p>

</page>
