<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-templates" xml:lang="it">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Anita Reitere</name>
      <email>nitalynx@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Creare velocemente nuovi documenti a partire da modelli di file personalizzati.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Modelli per documenti usati abitualmente</title>

  <p>Se si creano spesso documenti basati sullo stesso contenuto, potrebbe essere utile usare i modelli. Un file modello può essere un documento di qualsiasi tipo, con formattazione o contenuto da riutilizzare. Per esempio, è possibile creare un modello di una carta intestata.</p>

  <steps>
    <title>Creare un nuovo modello</title>
    <item>
      <p>Creare un documento per poterlo usare come modello. Per esempio, è possibile creare la carta intestata con un'applicazione di elaborazione testi.</p>
    </item>
    <item>
      <p>Save the file with the template content in the <file>Templates</file>
      folder in your <file>Home</file> folder. If the <file>Templates</file>
      folder does not exist, you will need to create it first.</p>
    </item>
  </steps>

  <steps>
    <title>Usare un modello per creare un documento</title>
    <item>
      <p>Aprire la cartella dove salvare il nuovo documento.</p>
    </item>
    <item>
      <p>Right-click anywhere in the empty space in the folder, then choose
      <gui style="menuitem">New Document</gui>. The names of available
      templates will be listed in the submenu.</p>
    </item>
    <item>
      <p>Selezionare il modello dall'elenco.</p>
    </item>
    <item>
      <p>Double-click the file to open it and start editing. You may wish to
      <link xref="files-rename">rename the file</link> when you are
      finished.</p>
    </item>
  </steps>

</page>
