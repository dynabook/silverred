<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-calibrationdevices" xml:lang="ru">

  <info>

    <link type="guide" xref="color#calibration"/>

    <desc>Мы поддерживаем большое количество калибровочных устройств.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Какие инструменты для измерения цвета поддерживаются?</title>

  <p>Для поддержки работы с измерительными инструментами в GNOME используется система управления цветом Argyll. Благодаря ей поддерживаются следующие устройства:</p>

  <list>
    <item><p>Gretag-Macbeth i1 Pro (спектрометр)</p></item>
    <item><p>Gretag-Macbeth i1 Monitor (спектрометр)</p></item>
    <item><p>Gretag-Macbeth i1 Display 1, 2 или LT (колориметр)</p></item>
    <item><p>X-Rite i1 Display Pro (колориметр)</p></item>
    <item><p>X-Rite ColorMunki Design or Photo (спектрометр)</p></item>
    <item><p>X-Rite ColorMunki Create (колориметр)</p></item>
    <item><p>X-Rite ColorMunki Display (колориметр)</p></item>
    <item><p>Pantone Huey (колориметр)</p></item>
    <item><p>MonacoOPTIX (колориметр)</p></item>
    <item><p>ColorVision Spyder 2 and 3 (колориметр)</p></item>
    <item><p>Colorimètre HCFR (колориметр)</p></item>
  </list>

  <note style="tip">
   <p>В настоящее время Pantone Huey является наиболее дешёвым устройством, которое в силу своей простоты лучше всего поддерживается в Linux.</p>
  </note>

  <p>
    Thanks to Argyll there’s also a number of spot and strip reading
    reflective spectrometers supported to help you calibrating and
    characterizing your printers:
  </p>

  <list>
    <item><p>X-Rite DTP20 “Pulse” (“swipe” type reflective spectrometer)</p></item>
    <item><p>X-Rite DTP22 Digital Swatchbook (спектрометр для точечных измерений в отражённом свете)</p></item>
    <item><p>X-Rite DTP41 (спектрометр для точечных и полосовых измерений в отражённом свете)</p></item>
    <item><p>X-Rite DTP41T (спектрометр для точечных и полосовых измерений в отражённом свете)</p></item>
    <item><p>X-Rite DTP51 (спектрометр для точечных измерений в отражённом свете)</p></item>
  </list>

</page>
