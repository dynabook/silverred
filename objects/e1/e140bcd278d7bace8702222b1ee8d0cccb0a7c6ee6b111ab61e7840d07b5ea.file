<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-install-flash" xml:lang="lv">

  <info>
    <link type="guide" xref="net-browser"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jums var būt nepieciešams instalēt Flash, lai apskatītu tādas lapas kā YouTube, kas attēlo video un interaktīvās tīmekļa lapas.</desc>
  </info>

  <title>Instalēt Flash spraudni</title>

  <p><app>Flash</app> ir tīmekļa pārlūkprogrammas<em>spraudnis</em>, kas ļauj skatīties video un lietot interaktīvas tīmekļa lapas dažās vietnēs. Dažas vietnes bez Flash spraudņa nestrādās.</p>

  <p>Ja jums nav ieinstalēts Flash, jūs, iespējams, redzēsit ziņojumu, ka vietnei ir nepieciešams Flash. Flash ir pieejams kā bezmaksas (bet ne brīvā koda programma) lejupielāde daudzām pārlūkprogrammām. Lielākajai daļai Linux distributīvu ir pieejams Flash caur to programmatūras instalētāju (pakotņu pārvaldnieku).</p>

  <steps>
    <title>Ja Flash ir pieejams programmu instalētājā:</title>
    <item>
      <p>Atveriet programmatūras instalētāja lietotni un meklējiet <input>flash</input>.</p>
    </item>
    <item>
      <p>Meklējiet <gui>Adobe Flash plug-in</gui>, <gui>Adobe Flash Player</gui> vai līdzīgi un spiediet instalēt.</p>
    </item>
    <item>
      <p>Ja ir atvērta kāda pārlūkprogramma, tad aizveriet to un atkal atveriet. Pārlūkprogrammai, kad jūs to par jaunu atvērsit, vajadzētu saprast, ka Flash ir ieinstalēts un jums tagad vajadzētu varēt aplūkot vietnes izmantojot Flash.</p>
    </item>
  </steps>

  <steps>
    <title>Ja Flash <em>nav</em> pieejams programmatūras instalētājā:</title>
    <item>
      <p>Dodieties uz <link href="http://get.adobe.com/flashplayer">Flash Player lejupielādes vietni</link>. Jūsu pārlūkprogrammai un operētājsistēmai vajadzētu tikt automātiski noteiktai.</p>
    </item>
    <item>
      <p>Tur, kur rakstīts <gui>Select version to download</gui>, izvēlieties programmatūras instalētāju, kas der jūsu Linux distributīvam. Ja nezināt, kuru izvēlēties, ņemiet <file>.tar.gz</file> opciju.</p>
    </item>
    <item>
      <p>Sekojiet <link href="http://kb2.adobe.com/cps/153/tn_15380.html">Flash instalācijas instrukcijām</link>, lai uzzinātu, kā to instalēt jūsu tīmekļa pārlūkprogrammai.</p>
    </item>
  </steps>

<section id="alternatives">
  <title>Flash atvērtā koda alternatīva</title>

  <p>Vairākas brīvas, atvērtā koda Flash alternatīvas ir pieejamas. Tās mēdz dažos aspektos strādāt labāk, nekā Flash spraudnis (piemēram, labāk atskaņo skaņu), bet sliktāk citos veidos (piemēram, nav iespējams parādīt dažas sarežģītākas Flash vietnes).</p>

  <p>Jūs varētu vēlēties izmēģināt arī vienu no šiem, ja neesat apmierināts ar Flash atskaņotāju, vai, ja jūs vēlaties izmantot pēc iespējas vairāk atvērtā koda programmatūru savā datorā. Šeit ir pāris iespējas:</p>

  <list style="compact">
    <item>
      <p>LightSpark</p>
    </item>
    <item>
      <p>Gnash</p>
    </item>
  </list>

</section>

</page>
