<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="lockdown-file-saving" xml:lang="ca">

  <info>
    <link type="guide" xref="user-settings#lockdown"/>
    <link type="seealso" xref="dconf-lockdown"/>
    <revision pkgversion="3.11" date="2014-10-14" status="candidate"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
        
    <desc>Evitar que l'usuari desi fitxers al disc.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>
  
  <title>Desactiva desar fitxers</title>

  <p>Podeu desactivar els diàlegs <gui>Desa</gui> i <gui>Anomena i desa</gui>. Això pot ser útil si esteu donant accés temporal a un usuari o no voleu que l’usuari guardi fitxers a l’ordinador.</p>

  <note style="warning">
    <p>Aquesta funció només funciona en aplicacions que el suportin! No totes les aplicacions de GNOME i de tercers tenen aquesta funció activada. Aquests canvis no tindran efecte a les aplicacions que no admeten aquesta funció.</p>
  </note>

  <steps>
    <title>Desactiva desar fitxers</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
    <item>
      <p>Creeu el fitxer clau <file>/etc/dconf/db/local.d/00-filesaving</file> per proporcionar informació a la base de dades <sys>local</sys>.</p>
      <listing>
        <title><file>/etc/dconf/db/local.d/00-filesaving</file></title>
<code>
# Especifica el camí a dconf
[org/gnome/desktop/lockdown]

# Eviteu que l'usuari desi fitxers al disc
disable-save-to-disk=true
</code>
     </listing>
    </item>
    <item>
      <p>Per evitar que l'usuari suprimeixi aquesta configuració, creeu el fitxer <file>/etc/dconf/db/local.d/locks/filesaving</file> amb el contingut següent:</p>
      <listing>
        <title><file>/etc/dconf/db/local.db/locks/filesaving</file></title>
<code>
# Lock file saving settings
/org/gnome/desktop/lockdown/disable-save-to-disk
</code>
      </listing>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
  </steps>

</page>
