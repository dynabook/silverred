<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-location" xml:lang="ro">
  <info>
    <link type="guide" xref="privacy" group="#last"/>

    <revision pkgversion="3.14" date="2014-10-13" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Enable or disable geolocation.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Șerbănescu</mal:name>
      <mal:email>daniel [at] serbanescu [dot] dk</mal:email>
      <mal:years>2016, 2019</mal:years>
    </mal:credit>
  </info>

  <title>Control location services</title>

  <p>Geolocation, or location services, uses cell tower positioning, GPS, and
  nearby Wi-Fi access points to determine your current location for use in
  setting your timezone and by applications such as <app>Maps</app>. When
  enabled, it is possible for your location to be shared over the network with
  a great deal of precision.</p>

  <steps>
    <title>Turn off the geolocation features of your desktop</title>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Privacy</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Privacy</gui> to open the panel.</p>
    </item>
    <item>
     <p>Switch the <gui>Location Services</gui> switch to off.</p>
     <p>To re-enable this feature, set the <gui>Location Services</gui> switch
     to on.</p>
    </item>
  </steps>

</page>
