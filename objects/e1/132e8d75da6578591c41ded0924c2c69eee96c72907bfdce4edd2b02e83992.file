<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-launch-applications" xml:lang="el">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>

    <link type="guide" xref="getting-started" group="videos"/>
    <title role="trail" type="link">Εκκίνηση εφαρμογών</title>
    <link type="seealso" xref="shell-apps-open"/>
    <title role="seealso" type="link">Ένα μάθημα στην εκκίνηση εφαρμογών</title>
    <link type="next" xref="gs-switch-tasks"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2013, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gmail.com</mal:email>
      <mal:years>2012, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Εκκίνηση εφαρμογών</title>

  <ui:overlay width="812" height="452">
   <media type="video" its:translate="no" src="figures/gnome-launching-applications.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-launching-apps.svg"/>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>Εκκίνηση εφαρμογών</tt:p>
         </tt:div>
         <tt:div begin="5s" end="7.5s">
           <tt:p>Μετακινήστε τον δείκτη του ποντικιού στη γωνία <gui>Δραστηριότητες</gui> στα πάνω αριστερά της οθόνης.</tt:p>
           </tt:div>
         <tt:div begin="7.5s" end="9.5s">
           <tt:p>Πατήστε το εικονίδιο <gui>Εμφάνιση εφαρμογών</gui>.</tt:p>
         </tt:div>
         <tt:div begin="9.5s" end="11s">
           <tt:p>Πατήστε την εφαρμογή που θέλετε να εκτελέσετε, για παράδειγμα, «Βοήθεια».</tt:p>
         </tt:div>
         <tt:div begin="12s" end="21s">
           <tt:p>Εναλλακτικά, χρησιμοποιήστε το πληκτρολόγιο για να ανοίξετε την επισκόπηση <gui>Δραστηριότητες</gui> πατώντας το πλήκτρο <key href="help:gnome-help/keyboard-key-super">Λογότυπο</key>.</tt:p>
         </tt:div>
         <tt:div begin="22s" end="29s">
           <tt:p>Αρχίστε να πληκτρολογείτε το όνομα της εφαρμογής που θέλετε να εκκινήσετε.</tt:p>
         </tt:div>
         <tt:div begin="30s" end="33s">
           <tt:p>Πατήστε <key>Enter</key> για να ξεκινήσετε την εφαρμογή.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
   </media>
  </ui:overlay>

  <section id="launch-apps-mouse">
    <title>Έναρξη εφαρμογών με το ποντίκι</title>
 
  <steps>
    <item><p>Μετακινήστε τον δείκτη του ποντικιού στη γωνία <gui>Δραστηριότητες</gui> πάνω αριστερά της οθόνης για να εμφανίσετε την επισκόπηση <gui>Δραστηριότητες</gui>.</p></item>
    <item><p>Πατήστε το εικονίδιο <gui>Εμφάνιση εφαρμογών</gui> που προβάλλεται στο κάτω μέρος της γραμμής στα αριστερά της οθόνης.</p></item>
    <item><p>Εμφανίζεται μια λίστα εφαρμογών. Κάντε κλικ στην εφαρμογή που θέλετε να εκτελέσετε, για παράδειγμα, «Βοήθεια».</p></item>
  </steps>

  </section>

  <section id="launch-app-keyboard">
    <title>Έναρξη εφαρμογών με το πληκτρολόγιο</title>

  <steps>
    <item><p>Ανοίξτε την επισκόπηση <gui>Δραστηριότητες</gui> πατώντας το πλήκτρο <key href="help:gnome-help/keyboard-key-super">Λογότυπο</key>.</p></item>
    <item><p>Αρχίστε να πληκτρολογείτε το όνομα της εφαρμογής που θέλετε να εκκινήσετε. Η αναζήτηση για την εφαρμογή ξεκινά αμέσως.</p></item>
    <item><p>Μόλις το εικονίδιο της εφαρμογής εμφανιστεί και επιλεγεί, πατήστε το πλήκτρο <key>Enter</key> για την εκκίνηση της εφαρμογής.</p></item>
  </steps>

  </section>

</page>
