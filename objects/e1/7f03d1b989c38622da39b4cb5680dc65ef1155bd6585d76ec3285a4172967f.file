Landscape misorder from TeX/dvips to mpage
==========================================
    When (La)TeX output is 'upside down' when printing it with mpage the
    following might help (from Ted Stern <stern@amath.washington.edu>).

    I found that part of the problem was that my latex
    document was being rotated by -90 degrees by dvips when the postscript was
    produced in landscape form.  By putting the starred line in at the top of
    my document (some slides, for example): 

      \documentclass[landscape]{slides} 
      %% ...
      \begin{document}
      \special{! /landplus90 true store}%         * * * line to include * * *
      %% line above has to be on the first page!
      %%
      %% ...
      \end{document}

    and processing the document with

      dvips -t landscape filename.dvi -o filename.ps

    , dvips will instead rotate the page by +90 degrees.  The result is that I
    get a postscript file that can be printed out using your current landscape
    handling option with no other modifications required. 

    This information is buried in the dvips manual somewhere, but is not easy
    to find!
