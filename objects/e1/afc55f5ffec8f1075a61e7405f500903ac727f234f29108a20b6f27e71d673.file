<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>aasink: GStreamer Good Plugins 1.0 Plugins Reference Manual</title>
<meta name="generator" content="DocBook XSL Stylesheets Vsnapshot">
<link rel="home" href="index.html" title="GStreamer Good Plugins 1.0 Plugins Reference Manual">
<link rel="up" href="ch01.html" title="gst-plugins-good Elements">
<link rel="prev" href="gst-plugins-good-plugins-aacparse.html" title="aacparse">
<link rel="next" href="gst-plugins-good-plugins-ac3parse.html" title="ac3parse">
<meta name="generator" content="GTK-Doc V1.32 (XML mode)">
<link rel="stylesheet" href="style.css" type="text/css">
</head>
<body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF">
<table class="navigation" id="top" width="100%" summary="Navigation header" cellpadding="2" cellspacing="5"><tr valign="middle">
<td width="100%" align="left" class="shortcuts">
<a href="#" class="shortcut">Top</a><span id="nav_description">  <span class="dim">|</span> 
                  <a href="#gst-plugins-good-plugins-aasink.description" class="shortcut">Description</a></span><span id="nav_hierarchy">  <span class="dim">|</span> 
                  <a href="#gst-plugins-good-plugins-aasink.object-hierarchy" class="shortcut">Object Hierarchy</a></span><span id="nav_properties">  <span class="dim">|</span> 
                  <a href="#gst-plugins-good-plugins-aasink.properties" class="shortcut">Properties</a></span>
</td>
<td><a accesskey="h" href="index.html"><img src="home.png" width="16" height="16" border="0" alt="Home"></a></td>
<td><a accesskey="u" href="ch01.html"><img src="up.png" width="16" height="16" border="0" alt="Up"></a></td>
<td><a accesskey="p" href="gst-plugins-good-plugins-aacparse.html"><img src="left.png" width="16" height="16" border="0" alt="Prev"></a></td>
<td><a accesskey="n" href="gst-plugins-good-plugins-ac3parse.html"><img src="right.png" width="16" height="16" border="0" alt="Next"></a></td>
</tr></table>
<div class="refentry">
<a name="gst-plugins-good-plugins-aasink"></a><div class="titlepage"></div>
<div class="refnamediv"><table width="100%"><tr>
<td valign="top">
<h2><span class="refentrytitle"><a name="gst-plugins-good-plugins-aasink.top_of_page"></a>aasink</span></h2>
<p>aasink</p>
</td>
<td class="gallery_image" valign="top" align="right"></td>
</tr></table></div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-aasink.properties"></a><h2>Properties</h2>
<div class="informaltable"><table class="informaltable" border="0">
<colgroup>
<col width="150px" class="properties_type">
<col width="300px" class="properties_name">
<col width="200px" class="properties_flags">
</colgroup>
<tbody>
<tr>
<td class="property_type"><span class="type">gint</span></td>
<td class="property_name"><a class="link" href="gst-plugins-good-plugins-aasink.html#GstAASink--brightness" title="The “brightness” property">brightness</a></td>
<td class="property_flags">Read / Write</td>
</tr>
<tr>
<td class="property_type"><span class="type">gint</span></td>
<td class="property_name"><a class="link" href="gst-plugins-good-plugins-aasink.html#GstAASink--contrast" title="The “contrast” property">contrast</a></td>
<td class="property_flags">Read / Write</td>
</tr>
<tr>
<td class="property_type"><span class="type">GstAASinkDitherers</span></td>
<td class="property_name"><a class="link" href="gst-plugins-good-plugins-aasink.html#GstAASink--dither" title="The “dither” property">dither</a></td>
<td class="property_flags">Read / Write</td>
</tr>
<tr>
<td class="property_type"><span class="type">GstAASinkDrivers</span></td>
<td class="property_name"><a class="link" href="gst-plugins-good-plugins-aasink.html#GstAASink--driver" title="The “driver” property">driver</a></td>
<td class="property_flags">Read / Write</td>
</tr>
<tr>
<td class="property_type"><span class="type">gint</span></td>
<td class="property_name"><a class="link" href="gst-plugins-good-plugins-aasink.html#GstAASink--frame-time" title="The “frame-time” property">frame-time</a></td>
<td class="property_flags">Read</td>
</tr>
<tr>
<td class="property_type"><span class="type">gint</span></td>
<td class="property_name"><a class="link" href="gst-plugins-good-plugins-aasink.html#GstAASink--frames-displayed" title="The “frames-displayed” property">frames-displayed</a></td>
<td class="property_flags">Read</td>
</tr>
<tr>
<td class="property_type"><span class="type">gfloat</span></td>
<td class="property_name"><a class="link" href="gst-plugins-good-plugins-aasink.html#GstAASink--gamma" title="The “gamma” property">gamma</a></td>
<td class="property_flags">Read / Write</td>
</tr>
<tr>
<td class="property_type"><span class="type">gint</span></td>
<td class="property_name"><a class="link" href="gst-plugins-good-plugins-aasink.html#GstAASink--height" title="The “height” property">height</a></td>
<td class="property_flags">Read / Write</td>
</tr>
<tr>
<td class="property_type"><span class="type">gboolean</span></td>
<td class="property_name"><a class="link" href="gst-plugins-good-plugins-aasink.html#GstAASink--inversion" title="The “inversion” property">inversion</a></td>
<td class="property_flags">Read / Write</td>
</tr>
<tr>
<td class="property_type"><span class="type">gint</span></td>
<td class="property_name"><a class="link" href="gst-plugins-good-plugins-aasink.html#GstAASink--randomval" title="The “randomval” property">randomval</a></td>
<td class="property_flags">Read / Write</td>
</tr>
<tr>
<td class="property_type"><span class="type">gint</span></td>
<td class="property_name"><a class="link" href="gst-plugins-good-plugins-aasink.html#GstAASink--width" title="The “width” property">width</a></td>
<td class="property_flags">Read / Write</td>
</tr>
</tbody>
</table></div>
</div>
<a name="GstAASink"></a><div class="refsect1">
<a name="gst-plugins-good-plugins-aasink.other"></a><h2>Types and Values</h2>
<div class="informaltable"><table class="informaltable" width="100%" border="0">
<colgroup>
<col width="150px" class="other_proto_type">
<col class="other_proto_name">
</colgroup>
<tbody><tr>
<td class="datatype_keyword">struct</td>
<td class="function_name"><a class="link" href="gst-plugins-good-plugins-aasink.html#GstAASink-struct" title="struct GstAASink">GstAASink</a></td>
</tr></tbody>
</table></div>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-aasink.object-hierarchy"></a><h2>Object Hierarchy</h2>
<pre class="screen">    GObject
    <span class="lineart">╰──</span> GInitiallyUnowned
        <span class="lineart">╰──</span> GstObject
            <span class="lineart">╰──</span> GstElement
                <span class="lineart">╰──</span> GstBaseSink
                    <span class="lineart">╰──</span> GstVideoSink
                        <span class="lineart">╰──</span> GstAASink
</pre>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-aasink.description"></a><h2>Description</h2>
<p>Displays video as b/w ascii art.</p>
<div class="refsect2">
<a name="id-1.2.4.7.3"></a><h3>Example launch line</h3>
<div class="informalexample">
  <table class="listing_frame" border="0" cellpadding="0" cellspacing="0">
    <tbody>
      <tr>
        <td class="listing_lines" align="right"><pre>1</pre></td>
        <td class="listing_code"><pre class="programlisting"><span class="n">gst</span><span class="o">-</span><span class="n">launch</span><span class="o">-</span><span class="mf">1.0</span> <span class="n">filesrc</span> <span class="n">location</span><span class="o">=</span><span class="n">test</span><span class="p">.</span><span class="n">avi</span> <span class="o">!</span> <span class="n">decodebin</span> <span class="o">!</span> <span class="n">videoconvert</span> <span class="o">!</span> <span class="n">aasink</span></pre></td>
      </tr>
    </tbody>
  </table>
</div>
 This pipeline renders a video to ascii art into a separate window.
<div class="informalexample">
  <table class="listing_frame" border="0" cellpadding="0" cellspacing="0">
    <tbody>
      <tr>
        <td class="listing_lines" align="right"><pre>1</pre></td>
        <td class="listing_code"><pre class="programlisting"><span class="n">gst</span><span class="o">-</span><span class="n">launch</span><span class="o">-</span><span class="mf">1.0</span> <span class="n">filesrc</span> <span class="n">location</span><span class="o">=</span><span class="n">test</span><span class="p">.</span><span class="n">avi</span> <span class="o">!</span> <span class="n">decodebin</span> <span class="o">!</span> <span class="n">videoconvert</span> <span class="o">!</span> <span class="n">aasink</span> <span class="n">driver</span><span class="o">=</span><span class="n">curses</span></pre></td>
      </tr>
    </tbody>
  </table>
</div>
 This pipeline renders a video to ascii art into the current terminal.
</div>
<div class="refsynopsisdiv">
<h2>Synopsis</h2>
<div class="refsect2">
<a name="id-1.2.4.7.4.1"></a><h3>Element Information</h3>
<div class="variablelist"><table border="0" class="variablelist">
<colgroup>
<col align="left" valign="top">
<col>
</colgroup>
<tbody>
<tr>
<td><p><span class="term">plugin</span></p></td>
<td>
            <a class="link" href="gst-plugins-good-plugins-plugin-aasink.html#plugin-aasink">aasink</a>
          </td>
</tr>
<tr>
<td><p><span class="term">author</span></p></td>
<td>Wim Taymans &lt;wim.taymans@chello.be&gt;</td>
</tr>
<tr>
<td><p><span class="term">class</span></p></td>
<td>Sink/Video</td>
</tr>
</tbody>
</table></div>
</div>
<hr>
<div class="refsect2">
<a name="id-1.2.4.7.4.2"></a><h3>Element Pads</h3>
<div class="variablelist"><table border="0" class="variablelist">
<colgroup>
<col align="left" valign="top">
<col>
</colgroup>
<tbody>
<tr>
<td><p><span class="term">name</span></p></td>
<td>sink</td>
</tr>
<tr>
<td><p><span class="term">direction</span></p></td>
<td>sink</td>
</tr>
<tr>
<td><p><span class="term">presence</span></p></td>
<td>always</td>
</tr>
<tr>
<td><p><span class="term">details</span></p></td>
<td>video/x-raw, format=(string)I420, width=(int)[ 1, 2147483647 ], height=(int)[ 1, 2147483647 ], framerate=(fraction)[ 0/1, 2147483647/1 ]</td>
</tr>
</tbody>
</table></div>
</div>
</div>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-aasink.functions_details"></a><h2>Functions</h2>
<p></p>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-aasink.other_details"></a><h2>Types and Values</h2>
<div class="refsect2">
<a name="GstAASink-struct"></a><h3>struct GstAASink</h3>
<pre class="programlisting">struct GstAASink;</pre>
</div>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-aasink.property-details"></a><h2>Property Details</h2>
<div class="refsect2">
<a name="GstAASink--brightness"></a><h3>The <code class="literal">“brightness”</code> property</h3>
<pre class="programlisting">  “brightness”               <span class="type">gint</span></pre>
<p>brightness.</p>
<p>Owner: GstAASink</p>
<p>Flags: Read / Write</p>
<p>Default value: 0</p>
</div>
<hr>
<div class="refsect2">
<a name="GstAASink--contrast"></a><h3>The <code class="literal">“contrast”</code> property</h3>
<pre class="programlisting">  “contrast”                 <span class="type">gint</span></pre>
<p>contrast.</p>
<p>Owner: GstAASink</p>
<p>Flags: Read / Write</p>
<p>Default value: 0</p>
</div>
<hr>
<div class="refsect2">
<a name="GstAASink--dither"></a><h3>The <code class="literal">“dither”</code> property</h3>
<pre class="programlisting">  “dither”                   <span class="type">GstAASinkDitherers</span></pre>
<p>dither.</p>
<p>Owner: GstAASink</p>
<p>Flags: Read / Write</p>
<p>Default value: no dithering</p>
</div>
<hr>
<div class="refsect2">
<a name="GstAASink--driver"></a><h3>The <code class="literal">“driver”</code> property</h3>
<pre class="programlisting">  “driver”                   <span class="type">GstAASinkDrivers</span></pre>
<p>driver.</p>
<p>Owner: GstAASink</p>
<p>Flags: Read / Write</p>
<p>Default value: X11 driver 1.1</p>
</div>
<hr>
<div class="refsect2">
<a name="GstAASink--frame-time"></a><h3>The <code class="literal">“frame-time”</code> property</h3>
<pre class="programlisting">  “frame-time”               <span class="type">gint</span></pre>
<p>frame time.</p>
<p>Owner: GstAASink</p>
<p>Flags: Read</p>
<p>Default value: 0</p>
</div>
<hr>
<div class="refsect2">
<a name="GstAASink--frames-displayed"></a><h3>The <code class="literal">“frames-displayed”</code> property</h3>
<pre class="programlisting">  “frames-displayed”         <span class="type">gint</span></pre>
<p>frames displayed.</p>
<p>Owner: GstAASink</p>
<p>Flags: Read</p>
<p>Default value: 0</p>
</div>
<hr>
<div class="refsect2">
<a name="GstAASink--gamma"></a><h3>The <code class="literal">“gamma”</code> property</h3>
<pre class="programlisting">  “gamma”                    <span class="type">gfloat</span></pre>
<p>gamma.</p>
<p>Owner: GstAASink</p>
<p>Flags: Read / Write</p>
<p>Allowed values: [0,5]</p>
<p>Default value: 1</p>
</div>
<hr>
<div class="refsect2">
<a name="GstAASink--height"></a><h3>The <code class="literal">“height”</code> property</h3>
<pre class="programlisting">  “height”                   <span class="type">gint</span></pre>
<p>height.</p>
<p>Owner: GstAASink</p>
<p>Flags: Read / Write</p>
<p>Default value: 0</p>
</div>
<hr>
<div class="refsect2">
<a name="GstAASink--inversion"></a><h3>The <code class="literal">“inversion”</code> property</h3>
<pre class="programlisting">  “inversion”                <span class="type">gboolean</span></pre>
<p>inversion.</p>
<p>Owner: GstAASink</p>
<p>Flags: Read / Write</p>
<p>Default value: TRUE</p>
</div>
<hr>
<div class="refsect2">
<a name="GstAASink--randomval"></a><h3>The <code class="literal">“randomval”</code> property</h3>
<pre class="programlisting">  “randomval”                <span class="type">gint</span></pre>
<p>randomval.</p>
<p>Owner: GstAASink</p>
<p>Flags: Read / Write</p>
<p>Default value: 0</p>
</div>
<hr>
<div class="refsect2">
<a name="GstAASink--width"></a><h3>The <code class="literal">“width”</code> property</h3>
<pre class="programlisting">  “width”                    <span class="type">gint</span></pre>
<p>width.</p>
<p>Owner: GstAASink</p>
<p>Flags: Read / Write</p>
<p>Default value: 0</p>
</div>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-aasink.see-also"></a><h2>See Also</h2>
<p><a class="link" href="gst-plugins-good-plugins-cacasink.html#GstCACASink"><span class="type">GstCACASink</span></a></p>
</div>
</div>
<div class="footer">
<hr>Generated by GTK-Doc V1.32</div>
</body>
</html>