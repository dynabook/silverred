<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="nautilus-bookmarks-edit" xml:lang="pt">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hilh</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Acrescentar, eliminar e renomear marcadores no gestor de ficheiros.</desc>

  </info>

  <title>Editar marcadores de diretório</title>

  <p>Seus marcadores listam-se em barra-a lateral do gestor de ficheiros.</p>

  <steps>
    <title>Acrescentar um marcador:</title>
    <item>
      <p>Abra a diretório (ou a localização) que queira acrescentar aos marcadores.</p>
    </item>
    <item>
      <p>Carregue no menu da janela na barra de ferramentas e escolha <gui>Acrescentar este lugar a marcadores</gui>.</p>
    </item>
  </steps>

  <steps>
    <title>Eliminar um marcador:</title>
    <item>
      <p>Carregue com o botão direito do rato sobre o marcador em barra-a lateral e escolha <gui>Eliminar</gui> no menu.</p>
    </item>
  </steps>

  <steps>
    <title>Renomear um marcador:</title>
    <item>
      <p>Carregue com o botão direito do rato sobre o marcador em barra-a lateral e selecione <gui>Renomear…</gui>.</p>
    </item>
    <item>
      <p>Na lacuna <gui>Nome</gui> escreva o nome novo para o marcador.</p>
      <note>
        <p>Renaming a bookmark does not rename the folder. If you have
        bookmarks to two different folders in two different locations, but
        which each have the same name, the bookmarks will have the same name,
        and you won’t be able to tell them apart. In these cases, it is useful
        to give a bookmark a name other than the name of the folder it points
        to.</p>
      </note>
    </item>
  </steps>

</page>
