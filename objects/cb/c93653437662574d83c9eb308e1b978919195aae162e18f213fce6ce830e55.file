<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-inklevel" xml:lang="ja">

  <info>
    <link type="guide" xref="printing"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Anita Reitere</name>
      <email>nitalynx@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>プリンターのカートリッジ内のインクまたはトナーの残量を確認します。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>松澤 二郎</mal:name>
      <mal:email>jmatsuzawa@gnome.org</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>赤星 柔充</mal:name>
      <mal:email>yasumichi@vinelinux.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kentaro KAZUHAMA</mal:name>
      <mal:email>kazken3@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shushi Kurose</mal:name>
      <mal:email>md81bird@hitaki.net</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Noriko Mizumoto</mal:name>
      <mal:email>noriko@fedoraproject.org</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>坂本 貴史</mal:name>
      <mal:email>o-takashi@sakamocchi.jp</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>日本GNOMEユーザー会</mal:name>
      <mal:email>http://www.gnome.gr.jp/</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>How can I check my printer’s ink or toner levels?</title>

  <p>インクやトナーの残量を確認する方法についてはお使いのプリンターのモデルや製造元、コンピューターにインストールされているドライバーやアプリケーションなどによって異なります。 </p>

  <p>インク残量やその他の情報を表示するディスプレイを搭載しているプリンターもあります。</p>

  <p>Some printers report toner or ink levels to the computer, which can be
  found in the <gui>Printers</gui> panel in <app>Settings</app>. The ink
  level will be shown with the printer details if it is available.</p>

  <p>たいていの HP 製プリンター用のドライバーとステータスツールが、HP Linux Imaging and Printing (HPLIP) プロジェクトから提供されています。HP 以外のメーカーでは、同様の機能を備えたプロプライエタリのドライバーを提供している場合があります。</p>

  <p>Alternatively, you can install an application to check or monitor
  ink levels. <app>Inkblot</app> shows ink status for many HP, Epson
  and Canon printers. See if your printer is on the <link href="http://libinklevel.sourceforge.net/#supported">list of
  supported models</link>. Another ink levels application for Epson and
  some other printers is <app>mtink</app>.</p>

  <p>プリンターによっては、Linux への対応が不十分だったり、設計上インク残量を確認できないものもあります。</p>

</page>
