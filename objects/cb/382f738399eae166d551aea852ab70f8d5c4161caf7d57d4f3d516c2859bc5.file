<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-bouncekeys" xml:lang="lv">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Ignorēt ātri atkārtotu tā paša taustiņa nospiešanu.</desc>
  </info>

  <title>Ieslēgt atlecošos taustiņus</title>

  <p>Ieslēgt <em>atlecošos taustiņus</em>, lai ignorētu ātri atkārtotu taustiņu piespiešanu. Piemēram, ja jums ir roku trīce, kas izraisa vairākkārtīgu taustiņa piespiešanu, kad vēlaties piespiest tikai vienreiz, jums vajadzētu ieslēgt atlecošos taustiņus.</p>

  <steps>
    <item>
      <p>Atveriet <gui xref="shell-introduction#activities">aktivitāšu</gui> pārskatu un sāciet rakstīt <gui>Iestatījumi</gui>.</p>
    </item>
    <item>
      <p>Spiediet <gui>Iestatījumi</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Universal Access</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Spiediet <gui>Rakstīšanas asistents (AccessX)</gui> un sadaļā <gui>Rakstīšana</gui>.</p>
    </item>
    <item>
      <p>Switch the <gui>Bounce Keys</gui> switch to on.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Ātri ieslēgt vai izslēgt atlecošos taustiņus</title>
    <p>Jūs varat ieslēgt un izslēgt atlecošos taustiņus, spiežot uz <link xref="a11y-icon">pieejamības ikonas</link> augšējā joslā un izvēloties <gui>Atlecošie taustiņi</gui>. Pieejamības ikona ir redzama, kad viens vai vairāki iestatījumi ir aktivēti <gui>universālās piekļuves</gui> panelī.</p>
  </note>

  <p>Lietojiet <gui>Pieņemšanas aizture</gui> slīdni, lai norādītu aizturi no taustiņa piespiešanas pirmās reizes līdz tiek reģistrēts jauna taustiņa piespiešana. Izvēlieties <gui>Pīkstēt, kad taustiņš tiek noraidīts</gui>, ja vēlaties, lai dators rada skaņu katru reizi, kad tiek ignorēta taustiņa piespiešana, kad tas notiek pārāk ātri pēc iepriekšējās taustiņa piespiešanas.</p>

</page>
