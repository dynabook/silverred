<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-autorun" xml:lang="lv">

  <info>
    <link type="guide" xref="media#photos"/>
    <link type="guide" xref="media#videos"/>
    <link type="guide" xref="media#music"/>
    <link type="guide" xref="files#removable"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-10-04" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="author">
      <name>GNOME dokumentācijas projekts</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Automātiski palaist lietotnes, kad atrasti CD, DVD, fotoaparāti, audio atskaņotāji vai citas ierīces vai datu nesēji.</desc>
  </info>

  <!-- TODO: fix bad UI strings, then update help -->
  <title>Atvērt lietotnes ierīcēm vai diskiem</title>

  <p>Var iestatīt, lai kāda lietotne automātiski tiek palaista, kad pieslēgta ierīce vai ievietots disks vai zibatmiņa. Piemēram, jums varētu būt ērti, ja fotoaparāta pievienošana palaistu fotogrāfiju pārvaldnieku. Šo iespēju var arī atslēgt, lai, pievienojot ierīces, nekādas darbības netiktu veiktas.</p>

  <p>Lai norādītu, kuras lietotnes jāpalaiž, kad pieslēgtas dažādas ierīces:</p>

<steps>
  <item>
    <p>Atveriet <link xref="shell-introduction#activities">aktivitāšu</link> pārskatu un sāciet rakstīt <gui>Sīkāka informācija</gui>.</p>
  </item>
  <item>
    <p>Spiediet <gui>Sīkāka informācija</gui>, lai atvērtu paneli.</p>
  </item>
  <item>
    <p>Spiediet <gui>Noņemamie datu nesēji</gui>.</p>
  </item>
  <item>
    <p>Atrodiet vēlamo ierīci vai datu nesēja tipu, tad izvēlieties atbilstošo lietotni vai darbību. Skatiet dažādu ierīču un datu nesēju tipu aprakstu zemāk.</p>
    <p>Lietotnes palaišanas vietā var arī izvēlēties, lai ierīci parāda datņu pārvaldnieks ar <gui>Atvērt mapi</gui> opciju. Tad jums pajautās, ko darīt, vai arī neveiks nekādas automātiskas darbības.</p>
  </item>
  <item>
    <p>Ja jūs sarakstā neredzat vajadzīgo ierīču vai datu nesēju tipu (piemēram, Blu-ray diskus vai e-grāmatu lasītājus), spiediet <gui>Citi datu nesēji…</gui>, lai redzētu plašāku sarakstu. Izvēlieties vēlamo ierīču vai datu nesēju tipu no izkrītošā saraksta <gui>Tips</gui> un lietotni vai darbību no saraksta <gui>Darbība</gui>.</p>
  </item>
</steps>

  <note style="tip">
    <p>Ja jūs nevēlaties automātiski palaist lietotnes, pievienojot datoram jebkādas ierīces vai datu nesējus, izvēlieties <gui>Nekad nevaicāt, vai nepalaist programmas, ievietojot datu nesēju</gui> loga <gui>Sistēmas informācija</gui> apakšā.</p>
  </note>

<section id="files-types-of-devices">
  <title>Ierīču un datu nesēju tipi</title>
<terms>
  <item>
    <title>Audio diski</title>
    <p>Izvēlieties savu iemīļoto mūzikas lietotni vai CD audio izvilcēju, lai atvērtu audio kompaktdiskus. Ja jūs lietojat audio DVD (DVD-A), iestatiet to atvēršanu, nospiežot pogu <gui>Citi datu nesēji…</gui>. Ja jūs atvērsiet audio disku ar datņu pārvaldnieku, celiņi parādīsies kā WAV datnes, kurus var noklausīties ar jebkuru audio atskaņošanas lietotni.</p>
  </item>
  <item>
    <title>Video diski</title>
    <p>Izvēlieties lietotni, ar ko atvērt video DVD. Izmantojiet pogu <gui>Citi datu nesēji…</gui>, lai iestatītu lietotnes Blu-ray, HD DVD, video CD (VCD) un super video CD (SVCD) atvēršanai. Ja DVD vai citi diski nestrādā pareizi, skatiet <link xref="video-dvd"/>.</p>
  </item>
  <item>
    <title>Tukši diski</title>
    <p>Izmantojiet pogu <gui>Citi datu nesēji…</gui>, lai izvēlētos lietotni CD, DVD, Blu-ray disku un HD DVD ierakstīšanai.</p>
  </item>
  <item>
    <title>Fotoaparāti un fotogrāfijas</title>
    <p>No izkrītošā saraksta <gui>Fotogrāfijas</gui> izvēlieties fotogrāfiju pārvaldnieku, ko palaist, kad datoram pievienots fotoaparāts vai ievietota fotoaparāta atmiņas karte, piemēram, CF, SD, MMC vai MS. Varat arī vienkārši pārlūkot fotogrāfijas ar datņu pārvaldnieku.</p>
    <p>Nospiežot <gui>Citi datu nesēji…</gui>, jūs varat izvēlēties lietotni Kodak attēlu CD atvēršanai. Tie ir parasti datu kompaktdiski ar JPEG attēliem mapē <file>Attēli</file>.</p>
  </item>
  <item>
    <title>Mūzikas atskaņotāji</title>
    <p>Izvēlieties mūzikas lietotni, lai pārvaldītu atskaņotājā atrodamo mūzikas kolekciju, vai arī izmantojiet parasto datņu pārvaldnieku.</p>
    </item>
    <item>
      <title>E-grāmatu lasītāji</title>
      <p>Izmantojiet pogu <gui>Citi datu nesēji…</gui>, lai izvēlētos īpašu lietotni e-grāmatu pārvaldībai, vai arī lietojiet e-grāmatām parasto datņu pārvaldnieku.</p>
    </item>
    <item>
      <title>Programmatūra</title>
      <p>Daži diski un noņemamie datu nesēji satur programmatūru, ko paredzēts palaist automātiski, kad datu nesējs ievietots datorā. Norādiet, kas jādara, kad ievietoti datu nesēji ar šādām programmām, sadaļā <gui>Programmatūra</gui>. Jums vienmēr prasīs apstiprinājumu, pirms tās palaist.</p>
      <note style="warning">
        <p>Drošības apsvērumu dēļ ir svarīgi palaist programmatūru <em>tikai</em> no uzticamiem datu nesējiem.</p>
      </note>
   </item>
</terms>

</section>

</page>
