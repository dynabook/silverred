<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mime-types-custom" xml:lang="ca">

  <info>
    <link type="guide" xref="software#management"/>
    <link type="seealso" xref="mime-types"/>
    <link type="seealso" xref="mime-types-custom-user"/>
    <revision pkgversion="3.12" date="2014-06-17" status="review"/>

    <credit type="author copyright">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Create a MIME type specification and register a default
    application.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

    <title>Afegir un MIME personalitzat per a tots els usuaris</title>
    <p>Per afegir un tipus MIME personalitzat per a tots els usuaris del sistema i definir una aplicació determinada per aquest, heu de crear una nova especificació del tipus MIME a la carpeta <file>/usr/share/mime/packages/</file> i un fitxer <file>.desktop</file> a la carpeta <file>/usr/share/applications/</file>.</p>
    <steps>
      <title>Afegir un tipus MIME personalitzat <sys>application/x-newtype</sys> per a tots els usuaris</title>
      <item>
        <p>Creeu el fitxer <file>/usr/share/mime/packages/application-x-newtype.xml</file>:</p>
        <code mime="application/xml">&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;mime-info xmlns="http://www.freedesktop.org/standards/shared-mime-info"&gt;
  &lt;mime-type type="application/x-newtype"&gt;
    &lt;comment&gt;new mime type&lt;/comment&gt;
    &lt;glob pattern="*.xyz"/&gt;
  &lt;/mime-type&gt;
&lt;/mime-info&gt;</code>
      <p>El fitxer d'exemple <file>application-x-newtype.xml</file> de dalt, defineix un nou tipus MIME <sys>application/x-newtype</sys> i assigna els arxius amb extensió <file>.xyz</file> a aquest tipus.</p>
      </item>
      <item>
        <p>Creeu un nou fitxer <file>.desktop</file> anomenat, per exemple, <file>myapplication1.desktop</file>, i situeu-lo a la carpeta <file>/usr/share/applications/</file>:</p>
        <code>[Entrada Escriptori]
Type=Application
MimeType=application/x-newtype
Name=<var>My Application 1</var>
Exec=<var>myapplication1</var></code>
      <p>El fitxer d'exemple <file>myapplication1.desktop</file> a dalt, associa el tipus MIME <sys>application/x-newtype</sys> amb una aplicació anomenada <app>La meva aplicació 1</app>, que s'executa amb la comanda <cmd>lamevaaplicacio1</cmd>.</p>
      </item>
      <item>
        <p>Com a arrel, actualitzar la base de dades MIME per a què els seus canvis entrin en vigor:</p>
        <screen><output># </output><input>update-mime-database /usr/share/mime</input>
        </screen>
      </item>
      <item>
        <p>Actualitzi la base de dades de l'aplicació com a root:</p>
        <screen><output># </output><input>update-desktop-database /usr/share/applications</input>
        </screen>
      </item>
      <item>
        <p>Per tal de verificar que s'han associat els fitxers <file>*.xyz</file> amb el tipus MIME <sys>application/x-newtype</sys>, primer creeu un fitxer buit, per exemple <file>test.xyz</file>:</p>
        <screen><output>$ </output><input>touch test.xyz</input></screen>
        <p>Aleshores executeu la comanda <cmd>gio info</cmd>:</p>
        <screen><output>$ </output><input>gio info test.xyz | grep "standard::content-type"</input>
  standard::content-type: application/x-newtype</screen>
        </item>
        <item>
          <p>Per verificar que <file>myapplication1.desktop</file> s'ha registrat com a aplicació predeterminada correctament per al tipus MIME <sys>application/x-newtype</sys>, executeu la comanda <cmd>gio mime</cmd>:</p>
        <screen><output>$ </output><input>gio mime application/x-newtype</input>
Default application for “application/x-newtype”: myapplication1.desktop
Registered applications:
	myapplication1.desktop
Recommended applications:
	myapplication1.desktop</screen>
      </item>
    </steps>
</page>
