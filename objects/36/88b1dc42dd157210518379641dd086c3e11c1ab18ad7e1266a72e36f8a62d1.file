<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:itst="http://itstool.org/extensions/" type="topic" style="task" id="net-proxy" xml:lang="ca">

  <info>
    <its:rules xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Baptiste Mille-Mathias</name>
      <email>baptistem@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Un proxy és un intermediari per al trànsit web, es pot utilitzar per accedir a serveis web de manera anònima, per motius de control o seguretat.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

  <title>Defineix la configuració del proxy</title>

<section id="what">
  <title>Què és un proxy?</title>

  <p>Un <em>proxy web</em> filtra els llocs web que consulteu, rep les peticions del vostre navegador per obtenir pàgines web i els seus elements i, seguint una política decidirà si retornar-vos-les. S'utilitzen habitualment a les empreses i als punts d'accés públics sense fil per controlar quins llocs web podeu veure, impedir-vos l'accés a Internet sense iniciar sessió o fer controls de seguretat als llocs web.</p>

</section>

<section id="change">
  <title>Canviar el mètode proxy</title>

  <steps>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a escriure <gui>Xarxa</gui>.</p>
    </item>
    <item>
      <p>Feu clic a <gui>Xarxa</gui> per obrir el quadre.</p>
    </item>
    <item>
      <p>Seleccioneu <gui>Servidor intermediari de xarxa</gui> de la llista de l'esquerra.</p>
    </item>
    <item>
      <p>Trieu el mètode de proxy que voleu utilitzar des de:</p>
      <terms>
        <item>
          <title><gui itst:context="proxy">Cap</gui></title>
          <p>Les aplicacions utilitzaran una connexió directa per obtenir el contingut de la web.</p>
        </item>
        <item>
          <title><gui>Manual</gui></title>
          <p>Per a cada protocol proxy, definiu l'adreça d'un proxy i un port per als protocols. Els protocols són <gui>HTTP</gui>, <gui>HTTPS</gui>, <gui>FTP</gui> i <gui>SOCKS</gui>.</p>
        </item>
        <item>
          <title><gui>Automàtic</gui></title>
          <p>Un URL que apunta a un recurs que conté la configuració adequada per al vostre sistema.</p>
        </item>
      </terms>
    </item>
  </steps>

  <p>Les aplicacions que utilitzin la connexió de xarxa usaran la configuració de proxy especificada.</p>

</section>

</page>
