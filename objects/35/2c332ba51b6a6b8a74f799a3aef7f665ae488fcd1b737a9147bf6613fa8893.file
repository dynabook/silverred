<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="printing-order" xml:lang="gu">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>ફીલ બુલ</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>જીમ કેમ્પબેલ</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>પ્રિન્ટ ક્રમને વિપરીત અને ભેગુ કરો.</desc>
  </info>

  <title>વિવિધ ક્રમમાં પાનાંને છાપો</title>

  <section id="reverse">
    <title>વિપરીત</title>

    <p>પ્રિન્ટર સામાન્ય રીતે પહેલાં પાનાંને છાપો, અને છેલ્લા પાનાંને છેલ્લુ, તેથી પાનાં વિપરીત ક્રમમાં અંત થાય છે જ્યારે તેઓને તમે પસંદ કરો તો. જો જરૂરી હોય તો, તમે આ છાપવાનાં ક્રમને વિપરીત કરી શકો છો.</p>

    <steps>
      <title>ક્રમને વિપરીત કરવા માટે:</title>
      <item>
        <p>Press <keyseq><key>Ctrl</key><key>P</key></keyseq> to open the Print
        dialog.</p>
      </item>
      <item>
        <p>In the <gui>General</gui> tab, under <gui>Copies</gui>, check
        <gui>Reverse</gui>. The last page will be printed first, and so on.</p>
      </item>
    </steps>

  </section>

  <section id="collate">
    <title>ક્રમમાં ગોઠવવું</title>

  <p>If you are printing more than one copy of the document, the print-outs
  will be grouped by page number by default (that is, all of the copies of page
  one come out, then the copies of page two, and so on). <em>Collating</em>
  will make each copy come out with its pages grouped together in the right
  order instead.</p>

  <steps>
    <title>To collate:</title>
    <item>
     <p>Press <keyseq><key>Ctrl</key><key>P</key></keyseq> to open the Print
     dialog.</p>
    </item>
    <item>
      <p>In the <gui>General</gui> tab, under <gui>Copies</gui>, check
      <gui>Collate</gui>.</p>
    </item>
  </steps>

</section>

</page>
