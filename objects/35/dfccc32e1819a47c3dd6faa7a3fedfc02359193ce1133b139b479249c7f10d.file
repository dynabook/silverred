<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="power-batteryoptimal" xml:lang="hu">

  <info>
    <link type="guide" xref="power"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Tippek, úgy mint „Ne hagyja az akkumulátort túlságosan lemerülni”.</desc>

    <credit type="author">
      <name>GNOME dokumentációs projekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

<title>Hozza ki a legtöbbet a laptopja akkumulátorából</title>

<p>Ahogy a laptopakkumulátorok öregszenek, egyre rosszabbul tartják a töltést, és a teljesítményük fokozatosan csökken. Van néhány módszer, amelyekkel meghosszabbíthatja a hasznos élettartamot, de ne számítson nagy különbségre.</p>

<list>
  <item>
    <p>Ne hagyja az akkumulátort teljesen lemerülni. Mindig kezdje tölteni <em>mielőtt</em> az akkumulátor kezd lemerülni; hatékonyabb utántölteni, mikor még csak részben sült ki, de jobban árt az akkumulátornak, ha akkor tölti újra, amikor még csak egy kicsit sült ki.</p>
  </item>
  <item>
    <p>A hő rossz hatással van az akkumulátortöltés hatásfokára. Ne hagyja az akkumulátort a kelleténél jobban melegedni.</p>
  </item>
  <item>
    <p>Az akkumulátorok akkor is öregszenek, ha csak tárolja azokat. Nincs sok értelme egyből tartalék akkumulátort is vásárolni, amikor az eredeti akkumulátort kapja – mindig csak akkor vásároljon csereakkumulátorokat, amikor szüksége van rájuk.</p>
  </item>
</list>

<note>
  <p>Ez a tanács kimondottan lítiumion (Li-Ion) akkumulátorokra vonatkozik, ami a legelterjedtebb típus. Egyéb akkumulátortípusok számára másfajta kezelés lehet előnyös.</p>
</note>

</page>
