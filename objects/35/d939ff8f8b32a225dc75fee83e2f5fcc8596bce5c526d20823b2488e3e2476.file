<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="shell-exit" xml:lang="sl">

  <info>
    <link type="guide" xref="shell-overview"/>
    <link type="guide" xref="power"/>
    <link type="guide" xref="index" group="#first"/>

    <revision pkgversion="3.6.0" date="2012-09-15" status="review"/>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.24.2" date="2017-06-11" status="candidate"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="author">
      <name>Alexandre Franke</name>
      <email>afranke@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David Faour</name>
      <email>dfaour.gnome@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Naučite se kako se odjavite, preklapljate med uporabniki in še več.</desc>
    <!-- Should this be a guide which links to other topics? -->
  </info>

  <title>Log out, power off or switch users</title>

  <p>When you have finished using your computer, you can turn it off, suspend
  it (to save power), or leave it powered on and log out.</p>

<section id="logout">
  <title>Odjava ali preklop uporabnika</title>

  <p>Da lahko vaš računalnik uporabljajo tudi drugi uporabniki se lahko odjavite ali ostanete prijavljeni in enostavno preklopite uporabnike. V primeru da enostavno preklopite uporabnike, se bodo vsi vaši programi še naprej izvajali in bo ob ponovni prijavi vse ostalo, kjer je bilo.</p>

  <p>To <gui>Log Out</gui> or <gui>Switch User</gui>, click the
  <link xref="shell-introduction#systemmenu">system menu</link> on the right
  side of the   top bar, click your name and then choose the correct option.</p>

  <note if:test="!platform:gnome-classic">
    <p>Ukaza <gui>Odjavi</gui> ali <gui>Preklopi uporabnika</gui> se v meniju pojavita le, če ima sistem več uporabniških računov.</p>
  </note>

  <note if:test="platform:gnome-classic">
    <p>The <gui>Switch User</gui> entry only appears in the menu if you have
    more than one user account on your system.</p>
  </note>

</section>

<section id="lock-screen">
  <info>
    <link type="seealso" xref="session-screenlocks"/>
  </info>

  <title>Zaklepanje zaslona</title>

  <p>If you’re leaving your computer for a short time, you should lock your
  screen to prevent other people from accessing your files or running
  applications. When you return, raise the
  <link xref="shell-lockscreen">lock screen</link> curtain and enter your
  password to log back in. If you don’t lock your screen, it will lock
  automatically after a certain amount of time.</p>

  <p>To lock your screen, click the system menu on the right side of the top
  bar and press the lock screen button at the bottom of the menu.</p>

  <p>When your screen is locked, other users can log in to their own accounts
  by clicking <gui>Log in as another user</gui> on the password screen. You
  can switch back to your desktop when they are finished.</p>

</section>

<section id="suspend">
  <info>
    <link type="seealso" xref="power-suspend"/>
  </info>

  <title>V pripravljenost</title>

  <p>To save power, suspend your computer when you are not using it. If you use
  a laptop, GNOME, by default, suspends your computer automatically when you
  close the lid.
  This saves your state to your computer’s memory and powers off most of the
  computer’s functions. A very small amount of power is still used during
  suspend.</p>

  <p>To suspend your computer manually, click the system menu on the right side
  of the top bar. From there you may either hold down the <key>Alt</key> key and 
  click the power off button, or simply long-click the power off button.</p>

</section>

<section id="shutdown">
<!--<info>
  <link type="seealso" xref="power-off"/>
</info>-->

  <title>Izklop ali ponoven zagon</title>

  <p>If you want to power off your computer entirely, or do a full restart,
  click the system menu on the right side of the top bar and press the power
  off button at the bottom of the menu. A dialog will open offering you the
  options to either <gui>Restart</gui> or <gui>Power Off</gui>.</p>

  <p>If there are other users logged in, you may not be allowed to power off or
  restart the computer because this will end their sessions.  If you are an
  administrative user, you may be asked for your password to power off.</p>

  <note style="tip">
    <p>You may want to power off your computer if you wish to move it and do
    not have a battery, if your battery is low or does not hold charge well. A
    powered off computer also uses <link xref="power-batterylife">less
    energy</link> than one which is suspended.</p>
  </note>

</section>

</page>
