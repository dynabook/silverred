<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sound-usespeakers" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="media#sound"/>
    <link type="seealso" xref="sound-usemic"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>

    <credit type="author">
      <name>Šon Mek Kens</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Povežite zvučnike ili slušalice i izaberite osnovni izlazni zvučni uređaj.</desc>
  </info>

  <title>Koristite druge zvučnike ili slušalice</title>

  <p>Možete da koristite spoljne zvučnike ili slušalice sa vašim računarom. Zvučnici se obično priključuju korišćenjem kružnog TRS (<em>špic, prsten, naglavak</em>) priključka ili pomoću USB-a.</p>

  <p>Ako vaš mikrofon ima kružni priključak, samo ga priključite u odgovarajću zvučnu utičnicu na vašem računaru. Većina računara ima dve utičnice: jedna je za mikrofone a druga za zvučnike. Ta utičnica je obično svetlo-zelene boje ili se pored nje nalazi slika slušalica. Zvučnici ili slušalice uključene u TRS utičnicu obično će biti korišćeni po osnovi. Ako ne, pogledajte uputstva ispod za izbor osnovnog ulaznog uređaja.</p>

  <p>Neki računari podržavaju višekanalni izlaz za zvuk okruženja. Oni obično koriste više TRS utikača, koji su često kodirani bojom. Ako niste sigurni gde šta ide, možete da isprobate izlaz zvuka u podešavanjima.</p>

  <p>Ako imate USB zvučnike ili slušalice, ili analogne slušalice priključene na USB zvučnu karticu, priključite ih na neki USB priključak. USB zvučnici deluju kao zasebni zvučni uređaji, i moraćete da odredite koji zvučnici će biti korišćeni kao osnovni.</p>

  <steps>
    <title>Izaberite osnovni ulazni zvučni uređaj</title>
    <item>
      <p>Otvorite pregled <gui xref="shell-introduction#activities">Aktivnosti</gui> i počnite da kucate <gui>Zvuk</gui>.</p>
    </item>
    <item>
      <p>Kliknite na <gui>Zvuk</gui> da otvorite panel.</p>
    </item>
    <item>
      <p>U jezičku <gui>Izlaz</gui>, izaberite uređaj koji želite da koristite.</p>
    </item>
  </steps>

  <p>Koristite dugme <gui style="button">Isprobaj zvučnike</gui> da proverite da li svi zvučnici rade i da li su priključeni na odgovarajuću priključnicu.</p>

</page>
