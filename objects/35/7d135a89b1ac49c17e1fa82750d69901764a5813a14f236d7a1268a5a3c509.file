<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sound-usespeakers" xml:lang="fr">

  <info>
    <link type="guide" xref="media#sound"/>
    <link type="seealso" xref="sound-usemic"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Brancher des haut-parleurs ou des écouteurs et sélectionner un périphérique audio de sortie par défaut.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Utilisation d'autres haut-parleurs ou écouteurs.</title>

  <p>You can use external speakers or headphones with your computer. Speakers
  usually either connect using a circular TRS (<em>tip, ring, sleeve</em>) plug
  or a USB.</p>

  <p>If your speakers or headphones have a TRS plug, plug it into the
  appropriate socket on your computer. Most computers have two sockets: one for
  microphones and one for speakers. This socket is usually light green in color
  or is accompanied by a picture of headphones. Speakers or headphones
  plugged into a TRS socket are usually used by default. If not, see the
  instructions below for selecting the default device.</p>

  <p>Certains ordinateurs acceptent des entrées multiples pour créer du son multi-canaux. Ils utilisent généralement des fiches rondes TRS multiples qui comportent des codes couleurs. Si vous ne savez pas quel fiche va dans quel connecteur, vous pouvez faire des essais de sortie son dans les paramètres.</p>

  <p>Si vous posséder des haut-parleurs ou des écouteurs USB, ou des écouteurs analogiques branchés sur une carte son USB, branchez-les dans n'importe quel port USB. Les haut-parleurs USB fonctionnent comme des périphériques audio à part et vous devrez définir les haut-parleurs à utiliser par défaut.</p>

  <steps>
    <title>Sélection d'un périphérique d'entrée par défaut</title>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
      overview and start typing <gui>Sound</gui>.</p>
    </item>
    <item>
      <p>Cliquez sur <gui>Son</gui> pour ouvrir le panneau.</p>
    </item>
    <item>
      <p>Dans l'onglet <gui>Sortie</gui>, sélectionnez le périphérique que vous souhaitez utiliser.</p>
    </item>
  </steps>

  <p>Appuyez sur le bouton <gui style="button">Test des haut-parleurs</gui> pour vérifier que tous les haut-parleurs fonctionnent et qu'ils sont correctement branchés.</p>

</page>
