<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>avimux: GStreamer Good Plugins 1.0 Plugins Reference Manual</title>
<meta name="generator" content="DocBook XSL Stylesheets Vsnapshot">
<link rel="home" href="index.html" title="GStreamer Good Plugins 1.0 Plugins Reference Manual">
<link rel="up" href="ch01.html" title="gst-plugins-good Elements">
<link rel="prev" href="gst-plugins-good-plugins-avidemux.html" title="avidemux">
<link rel="next" href="gst-plugins-good-plugins-avisubtitle.html" title="avisubtitle">
<meta name="generator" content="GTK-Doc V1.32 (XML mode)">
<link rel="stylesheet" href="style.css" type="text/css">
</head>
<body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF">
<table class="navigation" id="top" width="100%" summary="Navigation header" cellpadding="2" cellspacing="5"><tr valign="middle">
<td width="100%" align="left" class="shortcuts">
<a href="#" class="shortcut">Top</a><span id="nav_description">  <span class="dim">|</span> 
                  <a href="#gst-plugins-good-plugins-avimux.description" class="shortcut">Description</a></span><span id="nav_hierarchy">  <span class="dim">|</span> 
                  <a href="#gst-plugins-good-plugins-avimux.object-hierarchy" class="shortcut">Object Hierarchy</a></span><span id="nav_interfaces">  <span class="dim">|</span> 
                  <a href="#gst-plugins-good-plugins-avimux.implemented-interfaces" class="shortcut">Implemented Interfaces</a></span><span id="nav_properties">  <span class="dim">|</span> 
                  <a href="#gst-plugins-good-plugins-avimux.properties" class="shortcut">Properties</a></span>
</td>
<td><a accesskey="h" href="index.html"><img src="home.png" width="16" height="16" border="0" alt="Home"></a></td>
<td><a accesskey="u" href="ch01.html"><img src="up.png" width="16" height="16" border="0" alt="Up"></a></td>
<td><a accesskey="p" href="gst-plugins-good-plugins-avidemux.html"><img src="left.png" width="16" height="16" border="0" alt="Prev"></a></td>
<td><a accesskey="n" href="gst-plugins-good-plugins-avisubtitle.html"><img src="right.png" width="16" height="16" border="0" alt="Next"></a></td>
</tr></table>
<div class="refentry">
<a name="gst-plugins-good-plugins-avimux"></a><div class="titlepage"></div>
<div class="refnamediv"><table width="100%"><tr>
<td valign="top">
<h2><span class="refentrytitle"><a name="gst-plugins-good-plugins-avimux.top_of_page"></a>avimux</span></h2>
<p>avimux</p>
</td>
<td class="gallery_image" valign="top" align="right"></td>
</tr></table></div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-avimux.properties"></a><h2>Properties</h2>
<div class="informaltable"><table class="informaltable" border="0">
<colgroup>
<col width="150px" class="properties_type">
<col width="300px" class="properties_name">
<col width="200px" class="properties_flags">
</colgroup>
<tbody><tr>
<td class="property_type"><span class="type">gboolean</span></td>
<td class="property_name"><a class="link" href="gst-plugins-good-plugins-avimux.html#GstAviMux--bigfile" title="The “bigfile” property">bigfile</a></td>
<td class="property_flags">Read / Write</td>
</tr></tbody>
</table></div>
</div>
<a name="GstAviMux"></a><div class="refsect1">
<a name="gst-plugins-good-plugins-avimux.other"></a><h2>Types and Values</h2>
<div class="informaltable"><table class="informaltable" width="100%" border="0">
<colgroup>
<col width="150px" class="other_proto_type">
<col class="other_proto_name">
</colgroup>
<tbody><tr>
<td class="datatype_keyword">struct</td>
<td class="function_name"><a class="link" href="gst-plugins-good-plugins-avimux.html#GstAviMux-struct" title="struct GstAviMux">GstAviMux</a></td>
</tr></tbody>
</table></div>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-avimux.object-hierarchy"></a><h2>Object Hierarchy</h2>
<pre class="screen">    GObject
    <span class="lineart">╰──</span> GInitiallyUnowned
        <span class="lineart">╰──</span> GstObject
            <span class="lineart">╰──</span> GstElement
                <span class="lineart">╰──</span> GstAviMux
</pre>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-avimux.implemented-interfaces"></a><h2>Implemented Interfaces</h2>
<p>
GstAviMux implements
 GstTagSetter.</p>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-avimux.description"></a><h2>Description</h2>
<p>Muxes raw or compressed audio and/or video streams into an AVI file.</p>
<div class="refsect2">
<a name="id-1.2.34.8.3"></a><h3>Example launch lines</h3>
<p>(write everything in one line, without the backslash characters)</p>
<div class="informalexample">
  <table class="listing_frame" border="0" cellpadding="0" cellspacing="0">
    <tbody>
      <tr>
        <td class="listing_lines" align="right"><pre>1
2
3
4
5
6</pre></td>
        <td class="listing_code"><pre class="programlisting"><span class="n">gst</span><span class="o">-</span><span class="n">launch</span><span class="o">-</span><span class="mf">1.0</span> <span class="n">videotestsrc</span> <span class="n">num</span><span class="o">-</span><span class="n">buffers</span><span class="o">=</span><span class="mi">250</span> \
<span class="o">!</span> <span class="err">&#39;</span><span class="n">video</span><span class="o">/</span><span class="n">x</span><span class="o">-</span><span class="n">raw</span><span class="p">,</span><span class="n">format</span><span class="o">=</span><span class="p">(</span><span class="n">string</span><span class="p">)</span><span class="n">I420</span><span class="p">,</span><span class="n">width</span><span class="o">=</span><span class="mi">320</span><span class="p">,</span><span class="n">height</span><span class="o">=</span><span class="mi">240</span><span class="p">,</span><span class="n">framerate</span><span class="o">=</span><span class="p">(</span><span class="n">fraction</span><span class="p">)</span><span class="mi">25</span><span class="o">/</span><span class="mi">1</span><span class="err">&#39;</span> \
<span class="o">!</span> <span class="n">queue</span> <span class="o">!</span> <span class="n">mux</span><span class="p">.</span> \
<span class="n">audiotestsrc</span> <span class="n">num</span><span class="o">-</span><span class="n">buffers</span><span class="o">=</span><span class="mi">440</span> <span class="o">!</span> <span class="n">audioconvert</span> \
<span class="o">!</span> <span class="err">&#39;</span><span class="n">audio</span><span class="o">/</span><span class="n">x</span><span class="o">-</span><span class="n">raw</span><span class="p">,</span><span class="n">rate</span><span class="o">=</span><span class="mi">44100</span><span class="p">,</span><span class="n">channels</span><span class="o">=</span><span class="mi">2</span><span class="err">&#39;</span> <span class="o">!</span> <span class="n">queue</span> <span class="o">!</span> <span class="n">mux</span><span class="p">.</span> \
<span class="n">avimux</span> <span class="n">name</span><span class="o">=</span><span class="n">mux</span> <span class="o">!</span> <span class="n">filesink</span> <span class="n">location</span><span class="o">=</span><span class="n">test</span><span class="p">.</span><span class="n">avi</span></pre></td>
      </tr>
    </tbody>
  </table>
</div>
 This will create an .AVI file containing an uncompressed video stream
with a test picture and an uncompressed audio stream containing a
test sound.
<div class="informalexample">
  <table class="listing_frame" border="0" cellpadding="0" cellspacing="0">
    <tbody>
      <tr>
        <td class="listing_lines" align="right"><pre>1
2
3
4
5
6</pre></td>
        <td class="listing_code"><pre class="programlisting"><span class="n">gst</span><span class="o">-</span><span class="n">launch</span><span class="o">-</span><span class="mf">1.0</span> <span class="n">videotestsrc</span> <span class="n">num</span><span class="o">-</span><span class="n">buffers</span><span class="o">=</span><span class="mi">250</span> \
<span class="o">!</span> <span class="err">&#39;</span><span class="n">video</span><span class="o">/</span><span class="n">x</span><span class="o">-</span><span class="n">raw</span><span class="p">,</span><span class="n">format</span><span class="o">=</span><span class="p">(</span><span class="n">string</span><span class="p">)</span><span class="n">I420</span><span class="p">,</span><span class="n">width</span><span class="o">=</span><span class="mi">320</span><span class="p">,</span><span class="n">height</span><span class="o">=</span><span class="mi">240</span><span class="p">,</span><span class="n">framerate</span><span class="o">=</span><span class="p">(</span><span class="n">fraction</span><span class="p">)</span><span class="mi">25</span><span class="o">/</span><span class="mi">1</span><span class="err">&#39;</span> \
<span class="o">!</span> <span class="n">xvidenc</span> <span class="o">!</span> <span class="n">queue</span> <span class="o">!</span> <span class="n">mux</span><span class="p">.</span> \
<span class="n">audiotestsrc</span> <span class="n">num</span><span class="o">-</span><span class="n">buffers</span><span class="o">=</span><span class="mi">440</span> <span class="o">!</span> <span class="n">audioconvert</span> <span class="o">!</span> <span class="err">&#39;</span><span class="n">audio</span><span class="o">/</span><span class="n">x</span><span class="o">-</span><span class="n">raw</span><span class="p">,</span><span class="n">rate</span><span class="o">=</span><span class="mi">44100</span><span class="p">,</span><span class="n">channels</span><span class="o">=</span><span class="mi">2</span><span class="err">&#39;</span> \
<span class="o">!</span> <span class="n">lame</span> <span class="o">!</span> <span class="n">queue</span> <span class="o">!</span> <span class="n">mux</span><span class="p">.</span> \
<span class="n">avimux</span> <span class="n">name</span><span class="o">=</span><span class="n">mux</span> <span class="o">!</span> <span class="n">filesink</span> <span class="n">location</span><span class="o">=</span><span class="n">test</span><span class="p">.</span><span class="n">avi</span></pre></td>
      </tr>
    </tbody>
  </table>
</div>
 This will create an .AVI file containing the same test video and sound
as above, only that both streams will be compressed this time. This will
only work if you have the necessary encoder elements installed of course.
</div>
<div class="refsynopsisdiv">
<h2>Synopsis</h2>
<div class="refsect2">
<a name="id-1.2.34.8.4.1"></a><h3>Element Information</h3>
<div class="variablelist"><table border="0" class="variablelist">
<colgroup>
<col align="left" valign="top">
<col>
</colgroup>
<tbody>
<tr>
<td><p><span class="term">plugin</span></p></td>
<td>
            <a class="link" href="gst-plugins-good-plugins-plugin-avi.html#plugin-avi">avi</a>
          </td>
</tr>
<tr>
<td><p><span class="term">author</span></p></td>
<td>GStreamer maintainers &lt;gstreamer-devel@lists.freedesktop.org&gt;</td>
</tr>
<tr>
<td><p><span class="term">class</span></p></td>
<td>Codec/Muxer</td>
</tr>
</tbody>
</table></div>
</div>
<hr>
<div class="refsect2">
<a name="id-1.2.34.8.4.2"></a><h3>Element Pads</h3>
<div class="variablelist"><table border="0" class="variablelist">
<colgroup>
<col align="left" valign="top">
<col>
</colgroup>
<tbody>
<tr>
<td><p><span class="term">name</span></p></td>
<td>audio_%u</td>
</tr>
<tr>
<td><p><span class="term">direction</span></p></td>
<td>sink</td>
</tr>
<tr>
<td><p><span class="term">presence</span></p></td>
<td>request</td>
</tr>
<tr>
<td><p><span class="term">details</span></p></td>
<td>audio/x-raw, format=(string){ U8, S16LE }, rate=(int)[ 1000, 96000 ], channels=(int)[ 1, 2 ]</td>
</tr>
<tr>
<td><p><span class="term"></span></p></td>
<td> audio/mpeg, mpegversion=(int)1, layer=(int)[ 1, 3 ], rate=(int)[ 1000, 96000 ], channels=(int)[ 1, 2 ]</td>
</tr>
<tr>
<td><p><span class="term"></span></p></td>
<td> audio/mpeg, mpegversion=(int)4, stream-format=(string)raw, rate=(int)[ 1000, 96000 ], channels=(int)[ 1, 2 ]</td>
</tr>
<tr>
<td><p><span class="term"></span></p></td>
<td> audio/x-ac3, rate=(int)[ 1000, 96000 ], channels=(int)[ 1, 6 ]</td>
</tr>
<tr>
<td><p><span class="term"></span></p></td>
<td> audio/x-alaw, rate=(int)[ 1000, 48000 ], channels=(int)[ 1, 2 ]</td>
</tr>
<tr>
<td><p><span class="term"></span></p></td>
<td> audio/x-mulaw, rate=(int)[ 1000, 48000 ], channels=(int)[ 1, 2 ]</td>
</tr>
<tr>
<td><p><span class="term"></span></p></td>
<td> audio/x-wma, rate=(int)[ 1000, 96000 ], channels=(int)[ 1, 2 ], wmaversion=(int)[ 1, 2 ]</td>
</tr>
</tbody>
</table></div>
<div class="variablelist"><table border="0" class="variablelist">
<colgroup>
<col align="left" valign="top">
<col>
</colgroup>
<tbody>
<tr>
<td><p><span class="term">name</span></p></td>
<td>video_%u</td>
</tr>
<tr>
<td><p><span class="term">direction</span></p></td>
<td>sink</td>
</tr>
<tr>
<td><p><span class="term">presence</span></p></td>
<td>request</td>
</tr>
<tr>
<td><p><span class="term">details</span></p></td>
<td>video/x-raw, format=(string){ YUY2, I420, BGR, BGRx, BGRA, GRAY8, UYVY }, width=(int)[ 16, 4096 ], height=(int)[ 16, 4096 ], framerate=(fraction)[ 0/1, 2147483647/1 ]</td>
</tr>
<tr>
<td><p><span class="term"></span></p></td>
<td> image/jpeg, width=(int)[ 16, 4096 ], height=(int)[ 16, 4096 ], framerate=(fraction)[ 0/1, 2147483647/1 ]</td>
</tr>
<tr>
<td><p><span class="term"></span></p></td>
<td> video/x-divx, width=(int)[ 16, 4096 ], height=(int)[ 16, 4096 ], framerate=(fraction)[ 0/1, 2147483647/1 ], divxversion=(int)[ 3, 5 ]</td>
</tr>
<tr>
<td><p><span class="term"></span></p></td>
<td> video/x-msmpeg, width=(int)[ 16, 4096 ], height=(int)[ 16, 4096 ], framerate=(fraction)[ 0/1, 2147483647/1 ], msmpegversion=(int)[ 41, 43 ]</td>
</tr>
<tr>
<td><p><span class="term"></span></p></td>
<td> video/mpeg, width=(int)[ 16, 4096 ], height=(int)[ 16, 4096 ], framerate=(fraction)[ 0/1, 2147483647/1 ], mpegversion=(int){ 1, 2, 4 }, systemstream=(boolean)false</td>
</tr>
<tr>
<td><p><span class="term"></span></p></td>
<td> video/x-h263, width=(int)[ 16, 4096 ], height=(int)[ 16, 4096 ], framerate=(fraction)[ 0/1, 2147483647/1 ]</td>
</tr>
<tr>
<td><p><span class="term"></span></p></td>
<td> video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, width=(int)[ 16, 4096 ], height=(int)[ 16, 4096 ], framerate=(fraction)[ 0/1, 2147483647/1 ]</td>
</tr>
<tr>
<td><p><span class="term"></span></p></td>
<td> video/x-dv, width=(int)720, height=(int){ 576, 480 }, framerate=(fraction)[ 0/1, 2147483647/1 ], systemstream=(boolean)false</td>
</tr>
<tr>
<td><p><span class="term"></span></p></td>
<td> video/x-huffyuv, width=(int)[ 16, 4096 ], height=(int)[ 16, 4096 ], framerate=(fraction)[ 0/1, 2147483647/1 ]</td>
</tr>
<tr>
<td><p><span class="term"></span></p></td>
<td> video/x-wmv, width=(int)[ 16, 4096 ], height=(int)[ 16, 4096 ], framerate=(fraction)[ 0/1, 2147483647/1 ], wmvversion=(int)[ 1, 3 ]</td>
</tr>
<tr>
<td><p><span class="term"></span></p></td>
<td> image/x-jpc, width=(int)[ 1, 2147483647 ], height=(int)[ 1, 2147483647 ], framerate=(fraction)[ 0/1, 2147483647/1 ]</td>
</tr>
<tr>
<td><p><span class="term"></span></p></td>
<td> video/x-vp8, width=(int)[ 1, 2147483647 ], height=(int)[ 1, 2147483647 ], framerate=(fraction)[ 0/1, 2147483647/1 ]</td>
</tr>
<tr>
<td><p><span class="term"></span></p></td>
<td> image/png, width=(int)[ 16, 4096 ], height=(int)[ 16, 4096 ], framerate=(fraction)[ 0/1, 2147483647/1 ]</td>
</tr>
</tbody>
</table></div>
<div class="variablelist"><table border="0" class="variablelist">
<colgroup>
<col align="left" valign="top">
<col>
</colgroup>
<tbody>
<tr>
<td><p><span class="term">name</span></p></td>
<td>src</td>
</tr>
<tr>
<td><p><span class="term">direction</span></p></td>
<td>source</td>
</tr>
<tr>
<td><p><span class="term">presence</span></p></td>
<td>always</td>
</tr>
<tr>
<td><p><span class="term">details</span></p></td>
<td>video/x-msvideo</td>
</tr>
</tbody>
</table></div>
</div>
</div>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-avimux.functions_details"></a><h2>Functions</h2>
<p></p>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-avimux.other_details"></a><h2>Types and Values</h2>
<div class="refsect2">
<a name="GstAviMux-struct"></a><h3>struct GstAviMux</h3>
<pre class="programlisting">struct GstAviMux;</pre>
</div>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-avimux.property-details"></a><h2>Property Details</h2>
<div class="refsect2">
<a name="GstAviMux--bigfile"></a><h3>The <code class="literal">“bigfile”</code> property</h3>
<pre class="programlisting">  “bigfile”                  <span class="type">gboolean</span></pre>
<p>Support for openDML-2.0 (big) AVI files.</p>
<p>Owner: GstAviMux</p>
<p>Flags: Read / Write</p>
<p>Default value: TRUE</p>
</div>
</div>
</div>
<div class="footer">
<hr>Generated by GTK-Doc V1.32</div>
</body>
</html>