<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-states" xml:lang="nl">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>

    <revision pkgversion="3.4.0" date="2012-03-24" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Rangschik vensters in een werkruimte om efficiënter te kunnen werken.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Vensters verplaatsen en van grootte veranderen</title>

  <p>U kunt vensters verplaatsen en vergroten/verkleinen om efficiënter te werken. Naast het sleepgedrag dat u zou kunnen verwachten, beschikt Gnome over snelkoppelingen en wijzigingstoetsen om venster snel te kunnen ordenen.</p>

  <list>
    <item>
      <p>Verplaats een venster door de titelbalk te verslepen, of houd <key xref="keyboard-key-super">Super</key> ingedrukt terwijl u het venster versleept. Houd <key>Shift</key> ingedrukt tijdens het verplaatsen om het venster naar de randen van het scherm en andere vensters te brengen.</p>
    </item>
    <item>
      <p>Pas de grootte van een scherm aan door de randen of hoeken van het scherm te verslepen. Houd de <key>Shift</key>-toets ingedrukt terwijl u de grootte aanpast om het venster vast te maken aan de randen van het scherm en andere vensters.</p>
      <p if:test="platform:gnome-classic">U kunt de grootte van een gemaximaliseerd venster ook aanpassenen door te klikken op de maximaliseerknop in de titelbalk.</p>
    </item>
    <item>
      <p>De afmeting van een venster wijzigen of het venster verplaatsen via het toetsenbord. Druk op <keyseq><key>Alt</key><key>F7</key></keyseq> om een venster te verplaatsen of <keyseq><key>Alt</key><key>F8</key></keyseq> om de afmeting te wijzigen. Gebruik de pijltjestoetsen om te verplaatsen of de afmeting te wijzigen en druk daarna op <key>Enter</key> om de actie te voltooien, of druk op <key>Esc</key> om het venster terug te zetten op de oorspronkelijke plaats.</p>
    </item>
    <item>
      <p><link xref="shell-windows-maximize">Een venster maximaliseren</link> door het naar de bovenkant van het scherm te slepen. Sleep een venster naar één kant van het scherm om het aan die kant te maximaliseren; hiermee kunt u <link xref="shell-windows-tiled">vensters naast elkaar plaatsen</link>.</p>
    </item>
  </list>

</page>
