<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-rename" xml:lang="el">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Έργο Τεκμηρίωσης GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Αλλάξτε το όνομα αρχείου ή φακέλου</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Μετονομασία αρχείου ή φακέλου</title>

  <p>Όπως με άλλους διαχειριστές αρχείων, μπορείτε να χρησιμοποιήσετε το <app>Αρχεία</app> για να αλλάξετε το όνομα ενός αρχείου ή φακέλου.</p>

  <steps>
    <title>Για να μετονομάσετε ένα αρχείο ή φάκελο:</title>
    <item><p>Κάντε δεξί κλικ στο στοιχείο και επιλέξτε <gui>Μετονομασία</gui> ή επιλέξτε το αρχείο και πατήστε <key>F2</key>.</p></item>
    <item><p>Πληκτρολογήστε το νέο όνομα και πατήστε το πλήκτρο <key>Enter</key> ή κάντε κλικ στο <gui>Μετονομασία</gui>.</p></item>
  </steps>

  <p>Μπορείτε επίσης να μετονομάσετε ένα αρχείο από το παράθυρο <link xref="nautilus-file-properties-basic">ιδιότητες</link>.</p>

  <p>Όταν μετονομάζετε ένα αρχείο, επιλέγεται μόνο το πρώτο μέρος του ονόματος αρχείου και όχι η επέκταση του (το μέρος μετά τη <file>.</file>). Η επέκταση κανονικά δηλώνει τον τύπο του αρχείου (για παράδειγμα, <file>αρχείο.pdf</file> είναι ένα έγγραφο PDF) και συνήθως δεν θέλετε να το αλλάξετε. Αν χρειάζεστε επίσης να αλλάξετε την επέκταση, επιλέξτε όλο το όνομα αρχείου και αλλάξτε το.</p>

  <note style="tip">
    <p>Εάν μετονομάσατε λάθος αρχείο, ή ονομάσατε το αρχείο σας λαθεμένα, μπορείτε να αναιρέσετε την μετονομασία. Για να αντιστρέψετε την ενέργεια, κάντε κλικ αμέσως το κουμπί μενού στην εργαλειοθήκη και επιλέξτε <gui>Αναίρεση μετονομασίας</gui>, ή πατήστε <keyseq><key>Ctrl</key><key>Z</key></keyseq> για να επαναφέρετε το προηγούμενο όνομα.</p>
  </note>

  <section id="valid-chars">
    <title>Έγκυροι χαρακτήρες για ονόματα αρχείων</title>

    <p>Μπορείτε να χρησιμοποιήσετε οποιοδήποτε χαρακτήρα εκτός από του <file>/</file> (πλαγιοκάθετο) σε ονόματα αρχείου. Μερικές συσκευές, χρησιμοποιούν ένα <em>σύστημα αρχείων</em> που έχει περισσότερους περιορισμούς σε ονόματα αρχείων. Συνεπώς, είναι καλύτερη πρακτική να αποφύγετε τους ακόλουθους χαρακτήρες στα ονόματα αρχείων: <file>|</file>, <file>\</file>, <file>?</file>, <file>*</file>, <file>&lt;</file>, <file>"</file>, <file>:</file>, <file>&gt;</file>, <file>/</file>.</p>

    <note style="warning">
    <p>Αν ονομάσετε ένα αρχείο με μια <file>.</file> ως τον πρώτο χαρακτήρα του, τότε το αρχείο θα είναι <link xref="files-hidden">κρυφό</link> όταν προσπαθήσετε να το δείτε στον διαχειριστή αρχείων.</p>
    </note>

  </section>

  <section id="common-probs">
    <title>Κοινά προβλήματα</title>

    <terms>
      <item>
        <title>Το όνομα αρχείου χρησιμοποιείται ήδη</title>
        <p>Δεν μπορείτε να έχετε δύο αρχεία ή φακέλους με το ίδιο όνομα στον ίδιο φάκελο. Αν δοκιμάσετε να μετονομάσετε ένα αρχείο με ένα όνομα που ήδη υπάρχει στο φάκελο που δουλεύετε, ο διαχειριστής αρχείων δεν θα το επιτρέψει.</p>
        <p>Τα ονόματα αρχείου και φακέλου εξαρτώνται από πεζά/κεφαλαία, έτσι το όνομα αρχείου <file>Αρχείο.txt</file> δεν είναι το ίδιο με το <file>ΑΡΧΕΙΟ.txt</file>. Η χρήση διαφορετικών ονομάτων αρχείου όπως αυτό επιτρέπεται, όμως δεν συνιστάται.</p>
      </item>
      <item>
        <title>Το όνομα είναι υπερβολικά μεγάλο</title>
        <p>Σε μερικά συστήματα αρχείων, τα ονόματα αρχείων δεν έχουν περισσότερους από 255 χαρακτήρες. Αυτό το όριο των 255 χαρακτήρων περιλαμβάνει και τα ονόματα αρχείων και τη διαδρομή προς το αρχείο (για παράδειγμα., <file>/home/wanda/Documents/work/business-proposals/…</file>), έτσι θα πρέπει να αποφεύγετε μεγάλα ονόματα αρχείων και φακέλων όπου είναι δυνατό.</p>
      </item>
      <item>
        <title>Η επιλογή για μετονομασία είναι αχνή</title>
        <p>Εάν η <gui>Μετονομασία</gui> είναι αχνή, δεν έχετε την άδεια να μετονομάσετε το αρχείο. Θα πρέπει να είσαστε προσεκτικοί με την μετονομασία τέτοιων αρχείων, καθώς η μετονομασία μερικών προστατευμένων αρχείων μπορεί να προκαλέσει αστάθεια στο σύστημα. Για περισσότερες πληροφορίες δείτε <link xref="nautilus-file-properties-permissions"/>.</p>
      </item>
    </terms>

  </section>

</page>
