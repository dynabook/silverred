<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="question" version="1.0 if/1.0" id="gnome-classic" xml:lang="cs">

  <info>
    <link type="guide" xref="shell-overview"/>

    <revision pkgversion="3.10.1" date="2013-10-28" status="review"/>
    <revision pkgversion="3.29" date="2018-08-28" status="review"/>

    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Petr Kovář</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jestli dáváte přednost používání tradičního uživatelského prostředí, zvažte přepnutí na GNOME klasické.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

<title>Co je to GNOME klasické?</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
  <p><em>GNOME klasické</em> je vlastnost pro uživatele, kteří dávají přednost více tradičnímu ovládání uživatelského prostředí. I když je založené na technologiích <em>GNOME 3</em>, poskytuje řadu změn v uživatelském rozhraní, jako jsou nabídky <gui>Aplikace</gui> a <gui>Místa</gui> v horní liště a seznam oken v dolní části obrazovky.</p>
  </if:when>
  <if:when test="platform:gnome-classic">
<p><em>GNOME klasické</em> je vlastnost pro uživatele, kteří dávají přednost více tradičnímu ovládání uživatelského prostředí. I když je založené na technologiích <em>GNOME 3</em>, poskytuje řadu změn v uživatelském rozhraní, jako jsou nabídky <gui xref="shell-introduction#activities">Aplikace</gui> a <gui>Místa</gui> v horní liště a seznam oken v dolní části obrazovky.</p>
  </if:when>
</if:choose>

<p>Ke spuštění aplikací můžete použít nabídku <gui>Aplikace</gui> na horní liště. Přehled <gui xref="shell-introduction#activities">Činností</gui> je přístupný přes položku <gui>Přehled činností</gui> v nabídce.</p>

<p>K přístupu do <em>přehledu <gui>Činností</gui></em> můžete použít také zmáčknutí klávesy <key xref="keyboard-key-super">Super</key>.</p>

<section id="gnome-classic-window-list">
<title>Seznam oken</title>

<p>Seznam oken ve spodní části obrazovky poskytuje přístup ke všem otevřeným oknům a aplikacím a umožňuje vám je rychle minimalizovat a obnovit do původní velikosti.</p>

<p>Na pravé straně seznamu oken zobrazuje GNOME krátkou identifikaci aktuální pracovní plochy, např. <gui>1</gui> pro první (horní) pracovní plochu. Navíc zobrazuje identifikátor také celkový počet dostupných pracovních ploch. Když se chcete přepnout na jinou pracovní plochu, stačí kliknout na identifikátor a vybrat z nabídky pracovní plochu, kterou chcete.</p>

</section>

<section id="gnome-classic-switch">
<title>Přepnutí na GNOME klasické a zpět</title>

<note if:test="!platform:gnome-classic" style="important">
<p>GNOME klasické je dostupné jen na systémech s nainstalovanými konkrétními rozšířeními GNOME Shellu. V některýc linuxových distribucích nemusí mí tato rozšíření k dispozici nebo nemusí být nainstalována ve výchozím stavu.</p>
</note>

  <steps>
    <title>Když se chcete přepnout z <em>GNOME</em> na <em>GNOME klasické</em>:</title>
    <item>
      <p>Uložte otevřenou práci a odhlaste se. Klikněte na systémovou nabídku na pravé straně horní lišty, klikněte na své jméno a pak vyberte příslušnou volbu.</p>
    </item>
    <item>
      <p>Objeví se potvrzovací zpráva. Potvrďte volbou <gui>Odhlásit se</gui>.</p>
    </item>
    <item>
      <p>Na přihlašovací obrazovce vyberte v seznamu své jméno.</p>
    </item>
    <item>
      <p>Do vstupního pole pro heslo zadejte své heslo.</p>
    </item>
    <item>
      <p>Klikněte na ikonu voleb, která je zobrazená vedle tlačítka <gui>Přihlásit</gui> a vyberte <gui>GNOME klasické</gui>.</p>
    </item>
    <item>
      <p>Klikněte na tlačítko <gui>Přihlásit se</gui>.</p>
    </item>
  </steps>

  <steps>
    <title>Když se chcete přepnout z <em>GNOME klasického</em> na <em>GNOME</em>:</title>
    <item>
      <p>Uložte otevřenou práci a odhlaste se. Klikněte na systémovou nabídku na pravé straně horní lišty, klikněte na své jméno a pak vyberte příslušnou volbu.</p>
    </item>
    <item>
      <p>Objeví se potvrzovací zpráva. Potvrďte volbou <gui>Odhlásit se</gui>.</p>
    </item>
    <item>
      <p>Na přihlašovací obrazovce vyberte v seznamu své jméno.</p>
    </item>
    <item>
      <p>Do vstupního pole pro heslo zadejte své heslo.</p>
    </item>
    <item>
      <p>Klikněte na ikonu voleb, která je zobrazená vedle tlačítka <gui>Přihlásit</gui> a vyberte <gui>GNOME</gui>.</p>
    </item>
    <item>
      <p>Klikněte na tlačítko <gui>Přihlásit se</gui>.</p>
    </item>
  </steps>
  
</section>

</page>
