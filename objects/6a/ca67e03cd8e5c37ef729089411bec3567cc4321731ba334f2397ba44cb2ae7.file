<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-findip" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="net-general"/>
    <link type="seealso" xref="net-what-is-ip-address"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Šon Mek Kens</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Džim Kembel</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Poznavanje vaše IP adrese može da vam pomogne u rešavanju mrežnih problema.</desc>
  </info>

  <title>Saznajte vašu IP adresu</title>

  <p>Poznavanje vaše IP adrese može da vam pomogne da rešite probleme vaše veze na internet. Možda ćete biti iznenađeni kada budete saznali da imate <em>dve</em> IP adrese: jedna IP adresa za vaš računar na unutrašnjoj mreži i jedna IP adresa za vaš računar na internetu.</p>

  <steps>
    <title>Saznajte vašu unutrašnju (mrežnu) IP adrsesu</title>
    <item>
      <p>Otvorite pregled <gui xref="shell-introduction#activities">Aktivnosti</gui> i počnite da kucate <gui>Mreža</gui>.</p>
    </item>
    <item>
      <p>Kliknite na <gui>Mreža</gui> da otvorite panel.</p>
    </item>
    <item>
      <p>Izaberite vrstu veze, <gui>bežičnu</gui> ili <gui>žičanu</gui> sa spiska na levoj strani.</p>
      <p>IP adresa za žičanu vezu biće prikazana sa desne strane.</p>
      
      <p>Pritisnite na dugme <media its:translate="no" type="image" src="figures/emblem-system.png"><span its:translate="yes">podešavanja</span></media> da vidite IP adresu za bežičnu mrežu u panelu <gui>pojedinosti</gui>.</p>
    </item>
  </steps>

  <steps>
  	<title>Saznajte vašu spoljnu (internet) IP adrsesu</title>
    <item>
      <p>Posetite <link href="http://whatismyipaddress.com/">whatismyipaddress.com</link>.</p>
    </item>
    <item>
      <p>Sajt će vam prikazati vašu spoljnu IP adresu.</p>
    </item>
  </steps>

  <p>U zavisnosti kako se vaš računar povezuje na internet, ove adrese mogu biti iste.</p>

</page>
