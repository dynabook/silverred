<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="process-identify-hog" xml:lang="pt">
  <info>
    <revision version="0.2" pkgversion="3.11" date="2014-01-26" status="review"/>
    <link type="guide" xref="index#processes-tasks" group="processes-tasks"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author copyright">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
      <years>2011</years>
    </credit>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2011, 2014</years>
    </credit>

    <desc>Ordene a lista de processos por <gui>% CPU</gui> para ver que aplicação está a consumir os recursos do sistema.</desc>
  </info>

  <title>Que programa está a fazer que o computador esteja lento?</title>

  <p>Um programa que esteja a consumir mais memória da que lhe corresponde pode reduzir toda o computador. Para saber que processo pode estar a fazer isto:</p>

  <steps>	
    <item>
      <p>Carregue no separador <gui>Processos</gui>.</p>
    </item>
    <item>
      <p>Carregue na cabeceira da coluna <gui>% CPU</gui> para ordenar os processos por uso de CPU.</p>
      <note>
	<p>A seta na cabeceira da coluna mostra a direcção da classificação; carregue-a outra vez para investir a ordem. A seta deveria apontar para acima.</p>
      </note>
   </item>
  </steps>

  <p>Os processos da parte superior da lista estão a usar a maior percentagem de CPU. Uma vez que tenha identificado qual pode estar a usar mais recursos dos que deveria, pode decidir se fechar o programa em si ou se fechar outros programas para reduzir o ónus da CPU.</p>

  <note style="tip">
    <p>Um processo que se bloqueou ou que tem falhado pode usar o 100% da CPU. Se isto sucede, deverá <link xref="process-kill">matar</link> o processo.</p>
  </note>

</page>
