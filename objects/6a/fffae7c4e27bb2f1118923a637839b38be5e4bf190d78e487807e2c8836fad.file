<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-canshareprofiles" xml:lang="ru">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <desc>Делиться профилями с другими не имеет смысла, поскольку характеристики оборудования со временем меняются.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Могу ли я поделиться своим цветовым профилем?</title>

  <p>Созданные вами цветовые профили специфичны для именно вашего аппаратного обеспечения в именно ваших условиях освещения. Цветопередача монитора, проработавшего пару сотен часов, будет отличаться от следующего по серийному номеру монитора, проработавшего несколько тысяч часов.</p>
  <p>
    This means if you share your color profile with somebody, you might
    be getting them <em>closer</em> to calibration, but it’s misleading
    at best to say that their display is calibrated.
  </p>
  <p>
    Similarly, unless everyone has recommended controlled lighting
    (no sunlight from windows, black walls, daylight bulbs etc.) in a
    room where viewing and editing images takes place, sharing a profile
    that you created in your own specific lighting conditions doesn’t make
    a lot of sense.
  </p>

  <note style="warning">
    <p>Необходимо также проверять условия распространения профилей, как созданных вами, так и скачанных с сайта производителя используемого вами оборудования.</p>
  </note>

</page>
