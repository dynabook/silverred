<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wired-connect" xml:lang="hu">

  <info>
    <link type="guide" xref="net-wired" group="#first"/>

    <revision pkgversion="3.4" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.28" date="2018-03-28" status="review"/>

    <credit type="author">
      <name>GNOME dokumentációs projekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>A legtöbb vezetékes kapcsolat beállításához csak egy hálózati kábelt kell csatlakoztatnia.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Csatlakozás vezetékes (Ethernet) hálózathoz</title>

  <!-- TODO: create icon manually because it is one overlayed on top of another
       in real life. -->
  <p>A legtöbb vezetékes kapcsolat beállításához csak egy hálózati kábelt kell a gépbe dugni. A felső sávon lévő vezetékes hálózat ikon (<media its:translate="no" type="image" src="figures/network-wired-symbolic.svg"><span its:translate="yes">beállítások</span></media>) három pontként jelenik meg, amíg létrejön a kapcsolat. A pontok a kapcsolat létrejöttekor eltűnnek.</p>

  <p>Ha ez nem történik meg, akkor elsőként ellenőrizze, hogy a hálózati kábel be van-e dugva. A kábel egyik végének a számítógép egy négyszög alakú Ethernet (hálózati) portjába kell csatlakoznia, a másiknak pedig egy switchbe, routerbe, hálózati fali csatlakozóba vagy hasonlóba (a hálózat kiépítésétől függően). Néha az Ethernet csatlakozó mellett egy fény jelzi, hogy az be van dugva és aktív.</p>

  <note>
    <p>Az egyik számítógépet nem csatlakoztathatja közvetlenül a hálózati kábellel egy másikba (legalábbis további beállítások nélkül nem). Két számítógép összekapcsolásához azokat hálózati hub, router vagy switch eszközhöz kell csatlakoztatni.</p>
  </note>

  <p>Ha még mindig nem jött létre a kapcsolat, akkor a hálózata lehet, hogy nem támogatja az automatikus beállítást (DHCP). Ebben az esetben <link xref="net-manual">saját kezűleg kell beállítania</link> azt.</p>

</page>
