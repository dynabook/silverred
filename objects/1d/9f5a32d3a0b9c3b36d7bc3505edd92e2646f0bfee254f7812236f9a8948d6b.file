<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="video-dvd" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="media#videos"/>
    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.12.1" date="2014-03-30" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Talvez você não tenha os codecs corretos instalados ou o DVD pode ser de uma região errada.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

  <title>Por que os DVDs não tocam?</title>

  <p>Se você inserir um DVD no seu computador e não conseguir reproduzi-lo, você pode não ter os <em>codecs</em> corretos de DVD instalados, ou o DVD pode ser de uma <em>região</em> diferente.</p>

<section id="codecs">
  <title>Instalando os codecs corretos para reprodução de DVD</title>

  <p>Para reproduzir DVDs, você precisa ter os <em>codecs</em> corretos instalados. Um codec é um software que permite aos aplicativos lerem um formato de vídeo ou de áudio. Se o seu programa reprodutor de filmes não localizar os codecs corretos, ele pode se oferecer para instalá-los para você. Se isso não acontecer, você terá que instalá-los manualmente — peça ajuda sobre como fazer isso, por exemplo nos fóruns de suporte da sua distribuição Linux.</p>

  <p>Os DVDs são também <em>protegidos contra cópia</em> usando um sistema chamado CSS. Isso impede a cópia dos DVDs, mas também impede a sua reprodução a menos que você tenha algum programa extra para lidar com a proteção contra cópias. Esse software está disponível em várias distribuições Linux, mas não pode ser legalmente usado em todos países. Você pode comprar um decodificador comercial de DVD que pode lidar com essa proteção em <link href="https://fluendo.com/en/products/multimedia/oneplay-dvd-player/">Fluendo</link>. Ele funciona com Linux e seu uso deveria ser legalmente permitido em todos os países.</p>

  </section>

<section id="region">
  <title>Verificando a região do DVD</title>

  <p>Os DVDs têm um <em>código de região</em>, que lhe informa em que região do mundo você tem permissão de reproduzi-los. Se a região do reprodutor de DVD do seu computador não coincide com a região do DVD que você está tentando reproduzir, você não será capaz de reproduzi-lo. Por exemplo, se você tem um aparelho da região 1, só poderá reproduzir DVDs da América do Norte.</p>

  <p>É frequentemente possível alterar a região usada pelo seu reprodutor de DVD, mas só pode ser feito algumas poucas vezes antes que ele se trave em uma região permanentemente. Para alterar a região do reprodutor de DVD do seu computador, use <link href="http://linvdr.org/projects/regionset/">regionset</link>.</p>

  <p>Você pode encontrar <link href="https://en.wikipedia.org/wiki/DVD_region_code">mais informações sobre códigos de região no Wikipédia</link>.</p>

</section>

</page>
