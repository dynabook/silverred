<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="power-batteryestimate" xml:lang="gl">

  <info>

    <link type="guide" xref="power#faq"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>

    <desc>A vida da batería que se mostra só é unha estimación.</desc>

    <credit type="author">
      <name>Proxecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2020</mal:years>
    </mal:credit>
  </info>

<title>A vida estimada da batería é incorrecta</title>

<p>Cando comprobe a batería que lle falta, pode atopar que o tempo que indican os reportes que falta é diferente do que realmente lle queda á batería. Isto é porque a cantidade de batería que falta só é unha estimación. Normalmente, as estimacións melloran co paso do tempo.</p>

<p>In order to estimate the remaining battery life, a number of factors must be
taken into account. One is the amount of power currently being used by the
computer: power consumption varies depending on how many programs you have
open, which devices are plugged in, and whether you are running any intensive
tasks (like watching high-definition video or converting music files, for
example). This changes from moment to moment, and is difficult to predict.</p>

<p>Outro factor é como se descarga unha batería. Algunhas baterías perden carga moito máis rápido canto máis valeiras estean. Se o coñecemento preciso de como se descarga a batería, só se pode facer unha estimación aproximada da carga de batería que falta.</p>

<p>Mentres se descarga a batería, o xestor de batería adiviñará as súas propiedades de descarga e aprenderá como facer mellor estimacións da vida da batería. Aínda que nunca serán completamente exactas.</p>

<note>
  <p>Se obtén unha estimación de batería completamente ridícula (digamos, centos de días), o xestor de enerxía probabelmente estea omitindo algún dato que precisa para facer unha mellor estimación.</p>
  <p>Se desconecta a enerxía e ten o portátil empregando a batería por un tempo, e logo conecta para recargalo de novo, o xestor de enerxía debería saber obter os datos que precisa.</p>
</note>

</page>
