<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-name-location" xml:lang="ru">

  <info>
    <link type="guide" xref="printing#setup"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author copyright">
      <name>Яна Сварова (Jana Svarova)</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Джим Кэмпбелл (Jim Campbell)</name>
      <email>jcampbell@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Изменение имени и местоположения принтера в параметрах принтера.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>
  
  <title>Как изменить имя или местоположение принтера</title>

  <p>Местоположение принтера можно изменить в настройках принтера.</p>

  <note>
    <p>You need <link xref="user-admin-explain">administrative privileges</link>
    on the system to change the name or location of a printer.</p>
  </note>

  <section id="printer-name-change">
    <title>Изменение имени принтера</title>

  <p>Для изменения названия принтера потребуются следующие шаги:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Printers</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Printers</gui> to open the panel.</p>
    </item>
    <item>
      <p>Нажмите <gui style="button">Разблокировать</gui> в верхнем правом углу и затем по запросу системы введите свой пароль.</p>
    </item>
    <item>
      <p>Click the name of your printer, and start typing a new name for
      the printer.</p>
    </item>
    <item>
      <p>Нажмите <key>Enter</key>, чтобы сохранить изменения.</p>
    </item>
  </steps>

  </section>

  <section id="printer-location-change">
    <title>Изменение местоположения принтера</title>

  <p>Чтобы сменить местоположение принтера:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Printers</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Printers</gui> to open the panel.</p>
    </item>
    <item>
      <p>Нажмите <gui style="button">Разблокировать</gui> в верхнем правом углу и затем по запросу системы введите свой пароль.</p>
    </item>
    <item>
      <p>Click the location, and start editing the location.</p>
    </item>
    <item>
      <p>Нажмите <key>Enter</key> чтобы сохранить изменения.</p>
    </item>
  </steps>

  </section>

</page>
