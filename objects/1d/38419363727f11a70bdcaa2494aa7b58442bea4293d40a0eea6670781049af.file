<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" version="1.0 if/1.0" id="shell-introduction" xml:lang="pt">

  <info>
    <link type="guide" xref="shell-overview" group="#first"/>
    <link type="guide" xref="index" group="intro"/>

    <revision pkgversion="3.6.0" date="2012-10-13" status="review"/>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-28" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="review"/>
    <revision pkgversion="3.35.91" date="2020-07-19" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hilh</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>A visual overview of your desktop, the top bar, and the
    <gui>Activities</gui> overview.</desc>
  </info>

  <title>Visual overview of GNOME</title>

  <p>GNOME 3 conta com uma interface de utilizador completamente reinventada, desenhada para permanecer fora da sua vista, minimizar as distrações, e ajudar-lhe a trabalhar. A primeira vez que inicie uma sessão, verá um escritório vazio e a barra superior.</p>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <media type="image" src="figures/shell-top-bar.png" width="600" if:test="!target:mobile">
      <p>Barra superior de GNOME Shelh</p>
    </media>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-top-bar-classic.png" width="500" if:test="!target:mobile">
      <p>Barra superior de GNOME Shelh</p>
    </media>
  </if:when>
</if:choose>

  <p>The top bar provides access to your windows and applications, your
  calendar and appointments, and
  <link xref="status-icons">system properties</link> like sound, networking,
  and power. In the system menu in the top bar, you can change the volume or
  screen brightness, edit your <gui>Wi-Fi</gui> connection details, check your
  battery status, log out or switch users, and turn off your computer.</p>

<links type="section"/>

<!-- TODO: Replace "Activities overview" title for classic mode with something
like "Application windows" by using if:when and if:else ? -->
<section id="activities">
  <title>Vista de <gui>Atividades</gui></title>

<media type="image" src="figures/shell-activities-dash.png" height="530" style="floatstart floatleft" if:test="!target:mobile, !platform:gnome-classic">
  <p>Activities button and Dash</p>
</media>

  <p if:test="!platform:gnome-classic">Para aceder a suas janelas e aplicações, carregue o botão <gui>Atividades</gui>, ou simplesmente leve o ponteiro do rato ao canto superior esquerda ativa. Também pode carregar a tecla <key xref="keyboard-key-super">Super</key> em seu teclado. Pode ver suas janelas e aplicações na vista de atividades. Também pode começar a escrever para procurar aplicações, ficheiros ou diretórios e no site.</p>

  <p if:test="platform:gnome-classic">To access your windows and applications,
  click the button at the bottom left of the screen in the window list. You can
  also press the <key xref="keyboard-key-super">Super</key> key to see an
  overview with live thumbnails of all the windows on the current workspace.</p>

  <p if:test="!platform:gnome-classic">On the left of the overview, you will find the <em>dash</em>. The dash
  shows you your favorite and running applications. Click any icon in the
  dash to open that application; if the application is already running, it will
  have a small dot below its icon. Clicking its icon will bring up the most
  recently used window. You can also drag the icon to the overview, or onto any
  workspace on the right.</p>

  <p if:test="!platform:gnome-classic">Clicar com o botão direito mostra um menu que lhe permite escolher qualquer janela dum aplicação em execução, ou abrir uma janela nova. Também pode carregar no ícone enquanto mantém premida a tecla <key>Ctrl</key> para abrir uma janela nova.</p>

  <p if:test="!platform:gnome-classic">Quando entre na vista, inicialmente estará na vista das janelas. Isto mostra as miniaturas de todas as janelas no área de trabalho atual.</p>

  <p if:test="!platform:gnome-classic">Click the grid button at the bottom of the dash to display the
  applications overview. This shows you all the applications installed on your
  computer. Click any application to run it, or drag an application to the
  overview or onto a workspace thumbnail. You can also drag an application onto
  the dash to make it a favorite. Your favorite applications stay in the dash
  even when they’re not running, so you can access them quickly.</p>

  <list style="compact">
    <item>
      <p><link xref="shell-apps-open">Aprender mais sobre iniciar aplicações.</link></p>
    </item>
    <item>
      <p><link xref="shell-windows">Aprender mais sobre as janelas e as áreas de trabalho.</link></p>
    </item>
  </list>

</section>

<section id="appmenu">
  <title>Menú de aplicações</title>
  <if:choose>
    <if:when test="!platform:gnome-classic">
      <media type="image" src="figures/shell-appmenu-shell.png" width="250" style="floatend floatright" if:test="!target:mobile">
        <p>Menu de aplicação da <app>Terminal</app></p>
      </media>
      <p>Application menu, located beside the <gui>Activities</gui> button,
      shows the name of the active application alongside with its icon and
      provides quick access to windows and details of the application, as well
      as a quit item.</p>
    </if:when>
    <!-- TODO: check how the app menu removal affects classic mode -->
    <if:when test="platform:gnome-classic">
      <media type="image" src="figures/shell-appmenu-classic.png" width="250" style="floatend floatright" if:test="!target:mobile">
        <p>Menu de aplicação da <app>Terminal</app></p>
      </media>
      <p>O menu de aplicação, situado junto aos menus de <gui>Atividades</gui> e <gui>Lugares</gui>, mostra o nome do aplicação junto com seu ícone e proporciona um acesso rápido às preferências do aplicação ou a sua ajuda. Os elementos disponíveis neste menu variam em função do aplicação.</p>
    </if:when>
  </if:choose>

</section>

<section id="clock">
  <title>Relógio, calendário e citas</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <media type="image" src="figures/shell-appts.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Relógio, calendário, citas e notificações</p>
    </media>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-appts-classic.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Relógio, calendário e citas</p>
    </media>
  </if:when>
</if:choose>

  <p>Click the clock on the top bar to see the current date, a month-by-month
  calendar, a list of your upcoming appointments and new notifications. You can
  also open the calendar by pressing
  <keyseq><key>Super</key><key>M</key></keyseq>. You can access the date and
  time settings and open your full calendar application directly from
  the menu.</p>

  <list style="compact">
    <item>
      <p><link xref="clock-calendar">Aprender mais sobre o calendário e marcação-las.</link></p>
    </item>
    <item>
      <p><link xref="shell-notifications">Learn more about notifications and
      the notification list.</link></p>
    </item>
  </list>

</section>


<section id="systemmenu">
  <title>System menu</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <media type="image" src="figures/shell-exit.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Menu do utilizador</p>
    </media>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-exit-classic.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Menu do utilizador</p>
    </media>
  </if:when>
</if:choose>

  <p>Carregue no menu do sistema no canto superior direita do ecrã para a configuração do sistema e o seu computador.</p>

<!-- TODO: Update for 3.36 UI option "Do Not Disturb" in calendar dropdown:

<p>If you set yourself to Unavailable, you won’t be bothered by message popups
at the bottom of your screen. Messages will still be available in the message
tray when you move your mouse to the bottom-right corner. But only urgent
messages will be presented, such as when your battery is critically low.</p>
-->

  <p>When you leave your computer, you can lock your screen to prevent other
  people from using it. You can also quickly switch users without logging out
  completely to give somebody else access to the computer, or you can
  suspend or power off the computer from the menu. If you have a screen 
  that supports vertical or horizontal rotation, you can quickly rotate the 
  screen from the system menu. If your screen does not support rotation, 
  you will not see the button.</p>

  <list style="compact">
    <item>
      <p><link xref="shell-exit">Aprender mais sobre mudar de utilizador, fechar a sessão e apagar a computador.</link></p>
    </item>
  </list>

</section>

<section id="lockscreen">
  <title>Bloquear o ecrã</title>

  <p>When you lock your screen, or it locks automatically, the lock screen is
  displayed. In addition to protecting your desktop while you’re away from your
  computer, the lock screen displays the date and time. It also shows
  information about your battery and network status.</p>

  <list style="compact">
    <item>
      <p><link xref="shell-lockscreen">Aprenda mais sobre o ecrã de bloqueio.</link></p>
    </item>
  </list>

</section>

<section id="window-list">
  <title>Lista de janelas</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <p>GNOME implementa uma maneira de mudar entre janelas diferente à a da lista de janelas visível em outros meios de escritório. Isto lhe permite centrar nas tarefas que tem a mão sem distrações.</p>
    <list style="compact">
      <item>
        <p><link xref="shell-windows-switching">Aprender mais sobre mudar entre janelas. </link></p>
      </item>
    </list>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-window-list-classic.png" width="800" if:test="!target:mobile">
      <p>Lista de janelas</p>
    </media>
    <p>A lista de janelas na parte inferior do ecrã proporciona acesso a todas suas janelas e aplicações abertos e lhe permite as minimizar e as restaurar rapidamente.</p>
    <p>At the right-hand side of the window list, GNOME displays the four
    workspaces. To switch to a different workspace, select the workspace you
    want to use.</p>
  </if:when>
</if:choose>

</section>

</page>
