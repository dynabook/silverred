<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-admin-change" xml:lang="de">

  <info>
    <link type="guide" xref="user-accounts#privileges"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Sie können ändern, welche Benutzer Änderungen am System vornehmen dürfen, indem Sie Ihnen Systemverwalterrechte geben.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2019.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2019.</mal:years>
    </mal:credit>
  </info>

  <title>Ändern der Inhaber von Systemverwalterrechten</title>

  <p>Systemverwalterrechte ermöglichen eine Entscheidung darüber, wer Änderungen an wichtigen Stellen des Systems vornehmen darf. Sie können ändern, welche Benutzer Systemverwalterrechte haben und welche nicht. Dies ist eine gute Möglichkeit, Ihr System sicher zu halten und potenziell schädliche, nicht autorisierte Änderungen zu verhindern.</p>

  <p>Sie müssen über <link xref="user-admin-explain">Systemverwalterrechte verfügen</link>, um Kontentypen ändern zu können.</p>

  <steps>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Benutzer</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Benutzer</gui>, um das Panel zu öffnen.</p>
    </item>
    <item>
      <p>Klicken Sie auf den Knopf <gui style="button">Entsperren</gui> in der rechten oberen Ecke und geben Sie Ihr Passwort ein.</p>
    </item>
    <item>
      <p>Wählen Sie den Benutzer aus, dessen Rechte Sie ändern wollen.</p>
    </item>
    <item>
      <p>Klicken Sie auf den Knopf <gui>Standard</gui> neben <gui>Kontentyp</gui> und wählen Sie <gui>Systemverwalter</gui>.</p>
    </item>
    <item>
      <p>Die Rechte der Benutzer werden geändert, wenn diese sich das nächste Mal anmelden.</p>
    </item>
  </steps>

  <note>
    <p>Das erste Benutzerkonto des Systems ist üblicherweise dasjenige, das Systemverwalterrechte hat. Es ist das Benutzerkonto, das erstellt wurde, als Sie das System zuerst installiert haben.</p>
    <p>Es ist nicht sinnvoll, zu viele Benutzer eines Systems mit Systemverwalterrechten auszustatten.</p>
  </note>

</page>
