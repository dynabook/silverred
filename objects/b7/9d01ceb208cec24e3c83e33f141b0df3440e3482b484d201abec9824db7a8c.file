<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-language" xml:lang="pt">

  <info>
    <link type="guide" xref="prefs-language"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Projeto de documentação de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="editor">
      <name>Michael Hilh</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Alterar para um idioma diferente a interface de utilizador e o texto da ajuda.</desc>
  </info>

  <title>Mudar o idioma que usa</title>

  <p>Pode usar seu escritório e aplicações em dezenas de idiomas, sempre e quando tenha instalados na sua computador os pacotes de idiomas apropriados.</p>

  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Atividades</gui> e comece a escrever <gui>Región e idioma</gui>.</p>
    </item>
    <item>
      <p>Carregue em <gui>Región e idioma</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Carregue <gui>Idioma</gui>.</p>
    </item>
    <item>
      <p>Selecione a sua região e seu idioma. Se a sua região e idioma não aparecem na lista, carregue <gui><media its:translate="no" type="image" mime="image/svg" src="figures/view-more-symbolic.svg"><span its:translate="yes">…</span></media></gui> na parte inferior da lista para seleciona dentre todas as regiões e idiomas disponíveis.</p>
    </item>
    <item>
      <p>Carregue <gui style="button">Feito</gui> para guardar.</p>
    </item>
    <item>
      <p>Respond to the prompt, <gui>Your session needs to be restarted for
      changes to take effect</gui> by clicking
      <gui style="button">Restart Now</gui>, or click
      <gui style="button">×</gui> to restart later.</p>
    </item>
  </steps>

  <p>Algumas traduções podem estar incompletas e certos aplicações podem não suportar seu idioma por completo. Qualquer texto não traduzido aparecerá no idioma no que se desenvolveu o software de maneira original, geralmente inglês americano.</p>

  <p>Há algumas diretórios especiais na diretório de início onde as aplicações podem alojar coisas como música, imagens e documentos. Estas diretórios usam nomes regular de acordo a seu idioma. Quando volte a entrar, perguntar-se-lhe-á se quer mudar o nome destas diretórios com os nomes regular para o idioma selecionado. Se vai utilizar o novo idioma o tempo todo, deve atualizar os nomes das diretórios.</p>

  <note style="tip">
    <p>If there are multiple user accounts on your system, there is a separate
    instance of the <gui>Region &amp; Language</gui> panel for the login screen.
    Click the <gui>Login Screen</gui> button at the top right to toggle between
    the two instances.</p>
  </note>

</page>
