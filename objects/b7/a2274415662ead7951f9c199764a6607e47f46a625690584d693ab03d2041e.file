<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-what" xml:lang="cs">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Zálohujte cokoliv, u čeho byste utrpěli ztrátu, když by se něco pokazilo.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Co zálohovat</title>

  <p>Vaší prioritou by měla být záloha vašich <link xref="backup-thinkabout">nejdůležitějších souborů</link> a souborů, které by bylo náročné vytvořit znovu. Zde jsou příklady seřazené od nejdůležitějších po méně důležité:</p>

<terms>
 <item>
  <title>Vaše osobní soubory</title>
   <p>Mohou to být dokumenty, tabulky, e-maily, kalendářová data, finanční údaje, rodinné fotografie nebo jiné osobní soubory, které považujete za nenahraditelné.</p>
 </item>

 <item>
  <title>Vaše osobní nastavení</title>
   <p>Do tohoto spadají změny, které jste provedli v barvách, pozadí, rozlišení obrazovky a nastavení myši ve vašem uživatelském prostředí. Rovněž sem patří předvolby aplikací, jako jsou <app>LibreOffice</app>, hudební přehrávač nebo poštovní program. To vše lze sice vytvořit znovu, ale zabere to čas.</p>
 </item>

 <item>
  <title>Systémová nastavení</title>
   <p>Většina lidí nikdy nemění systémová nastavení, která se vytvořila při instalaci. Pokud jste si ale tato nastavení z nějakého důvodu přizpůsobili nebo se jedná o server, můžete chtít zálohovat i tato nastavení.</p>
 </item>

 <item>
  <title>Nainstalovaný software</title>
   <p>Software, který používáte, lze za normálních okolností po nějakých problémech s počítačem docela rychle znovu nainstalovat.</p>
 </item>
</terms>

  <p>Obecně řečeno, chcete zálohovat soubory, které jsou nenahraditelné a soubory, u kterých je třeba věnovat velké množství času k jejich obnovení bez zálohy. Na druhou stranu, pokud jsou snadno nahraditelné, nemusíte chtít, aby vám zabírali místo v úložišti se zálohou.</p>

</page>
