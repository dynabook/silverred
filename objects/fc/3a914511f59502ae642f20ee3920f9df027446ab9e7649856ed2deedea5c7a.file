<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-bouncekeys" xml:lang="sr">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Шон Мек Кенс</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Фил Бул</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Мајкл Хил</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Занемарите брза понављања притисака тастера истог тастера.</desc>
  </info>

  <title>Укључи одскочне тастере</title>

  <p>Укључите <em>одскочне тастере</em> да занемарите притиске који се брзо понављају. На пример, ако вам дрхте руке и због тога притиснете тастер неколико пута иако желите да га притиснете само једном, требали бисте да укључите одскочне тастере.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Universal Access</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Притисните <gui>Помоћ куцања (Икс приступ)</gui> у одељку <gui>Куцање</gui>.</p>
    </item>
    <item>
      <p>Switch the <gui>Bounce Keys</gui> switch to on.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Брзо укључите или искључите одскочне тастере</title>
    <p>Можете да укључите или да искључите одскочне тастере тако што ћете кликнути на <link xref="a11y-icon">иконицу приступачности</link> на горњој траци и изабрати <gui>Одскочни тастери</gui>. Иконица приступачности је видљива када је укључено једно или више подешавања са панела <gui>Универзалног приступа</gui>.</p>
  </note>

  <p>Користите клизач <gui>Кашњење прихватања</gui> да измените колико дуго ће одскочни тастери чекати пре регистровања следећег притиска након што сте притиснули тастер по први пут. Изаберите <gui>Запишти када тастер није прихваћен</gui> ако желите да се рачунар звучно огласи сваки пут када занемари притисак тастера зато што се десио превише рано након претходног притиска тастера.</p>

</page>
