<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-frequency" xml:lang="pt">

  <info>
    <link type="guide" xref="files#backup"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Projeto de documentação de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Aprenda com que frequência deveria guardar seus ficheiros importantes para se assegurar de que estão a bom arrecado.</desc>
  </info>

<title>Frequência das cópias de segurança</title>

  <p>A frequência com a que deve fazer cópias de segurança dependerá do tipo de dados que vá guardar. Por exemplo, se está trabalhando num meio de rede com dados criticos alojados em seus servidores, é possível que nem sequer resulte suficiente fazer uma copia de copia de segurança todas as noites.</p>

  <p>Por outro lado, se está fazendo a copia de copia de segurança dos dados na sua computador pessoal então os respaldos a cada hora são desnecessários. Pode que considere de ajuda os seguintes pontos ao planificar um copia de segurança:</p>

<list style="compact">
<item><p>A quantidade de tempo que passa com o seu computador.</p></item>
<item><p>Com que frequência e em que medida mudam os dados da sua computador.</p></item>
</list>

  <p>Se os dados que quer guardar são de pouca importância, ou estão sujeitos a poucas mudanças, como música, correios eletrónicos e fotos de família, fazer cópias semanais ou inclusive mensais pode ser suficiente. Não obstante, se está no meio duma auditoria promotora, pode que seja necessário fazer cópias mais frequentes.</p>

  <p>Como regra geral, a quantidade de tempo entre cópias de segurança não deve ser maior que a quantidade de tempo que podria estar parado refazendo qualquer trabalho perdido. Por exemplo, se empregar uma semana escrevendo documentos perdidos é demasiado, deveria fazer uma copia de copia de segurança ao menos uma vez por semana.</p>

</page>
