<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="ui" version="1.0 if/1.0" id="shell-workspaces" xml:lang="sv">

  <info>
    <link type="guide" xref="shell-windows#working-with-workspaces" group="#first"/>

    <revision pkgversion="3.8.0" date="2013-04-23" status="review"/>
    <revision pkgversion="3.10.3" date="2014-01-26" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.35.91" date="2020-02-27" status="candidate"/>

    <credit type="author">
      <name>Dokumentationsprojekt för GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Arbetsytor är ett sätta gruppera fönster på ditt skrivbord.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

<title>Vad är en arbetsyta, och hur hjälper den mig?</title>

    <media its:translate="no" type="image" src="figures/shell-workspaces.png" width="233" height="579" style="floatend floatright">
        <p>Workspace selector</p>
    </media>

  <p if:test="!platform:gnome-classic">Arbetsytor refererar till grupperingen av fönster på ditt skrivbord. Du kan skapa flera arbetsytor som fungerar som virtuella skrivbord. Arbetsytor är avsedda att minska röran och göra navigering av skrivbordet enklare.</p>

  <p if:test="platform:gnome-classic">Arbetsytor refererar till grupperingen av fönster på ditt skrivbord. Du kan använda flera arbetsytor som fungerar som virtuella skrivbord. Arbetsytor är avsedda att minska röran och göra navigering av skrivbordet enklare.</p>

  <p>Arbetsytor kan användas för att organisera ditt arbete. Du kan till exempel ha alla dina kommunikationsfönster, som exempelvis e-post- och chatt-program, på en arbetsyta och arbetet du håller på med på en annan arbetsyta. Din musikhanterare skulle kunna vara på en tredje arbetsyta.</p>

<p>Använda arbetsytor:</p>

<list>
  <item>
    <p if:test="!platform:gnome-classic">I översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui>, flytta din markör till den högra sidan av skärmen.</p>
    <p if:test="platform:gnome-classic">Klicka på knappen längst ner till vänster på skärmen i fönsterlisten, eller tryck på tangenten <key xref="keyboard-key-super">Super</key> för att öppna översiktsvyn <gui>Aktiviteter</gui>.</p>
  </item>
  <item>
    <p if:test="!platform:gnome-classic">En vertikal panel kommer att expanderas och visa de arbetsytor som används, samt en tom arbetsyta. Detta är arbetsyteväxlaren.</p>
    <p if:test="platform:gnome-classic">I det nedre högra hörnet ser du fyra rutor. Detta är arbetsyteväxlaren.</p>
  </item>
  <item>
    <p if:test="!platform:gnome-classic">För att lägga till en arbetsyta, dra och släpp ett fönster från en existerande arbetsyta till den tomma arbetsytan i arbetsyteväxlaren. Denna arbetsyta innehåller nu fönstret du släppte, och en ny tom arbetsyta kommer att visas nedanför den.</p>
    <p if:test="platform:gnome-classic">Dra och släpp ett fönster från din aktuella arbetsyta till en tom arbetsyta i arbetsyteväxlaren. Denna arbetsyta innehåller nu fönstret du släppte.</p>
  </item>
  <item if:test="!platform:gnome-classic">
    <p>För att ta bort en arbetsyta, stäng helt enkelt alla dess program eller flytta dem till andra arbetsytor.</p>
  </item>
</list>

<p if:test="!platform:gnome-classic">Det finns alltid minst en arbetsyta.</p>

</page>
