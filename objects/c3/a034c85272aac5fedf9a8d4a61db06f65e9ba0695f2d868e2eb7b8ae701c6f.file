<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-willnotturnon" xml:lang="fr">

  <info>
    <link type="guide" xref="power#problems"/>
    <link type="guide" xref="hardware-problems-graphics" group="#last"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Des câbles mal branchés ou des problèmes de matériel sont des raisons probables.</desc>
    <credit type="author">
      <name>Le projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Mise en marche de l'ordinateur impossible</title>

<p>Si votre ordinateur refuse de se mettre en marche, voici un bref aperçu des diverses raisons possibles.</p>
	
<section id="nopower">
  <title>Ordinateur non branché, batterie vide, câble mal branché</title>
  <p>Vérifiez que le câble secteur de l'ordinateur est bien branché et que les prises de courant sont allumées. Vérifiez que le moniteur est branché et allumé aussi. Si c'est un ordinateur portable, branchez le chargeur (au cas où la batterie soit vide). Vérifiez aussi que la batterie est correctement en place dans son logement (regardez sous l'ordinateur) si elle est amovible.</p>
</section>

<section id="hardwareproblem">
  <title>Problème de matériel</title>
  <p>A component of your computer may be broken or malfunctioning. If this is
  the case, you will need to get your computer repaired. Common faults include
  a broken power supply unit, incorrectly-fitted components (such as the
  memory or RAM) and a faulty motherboard.</p>
</section>

<section id="beeps">
  <title>Bips et extinction de l'ordinateur</title>
  <p>If the computer beeps several times when you turn it on and then turns off
  (or fails to start), it may be indicating that it has detected a problem.
  These beeps are sometimes referred to as <em>beep codes</em>, and the pattern
  of beeps is intended to tell you what the problem with the computer is.
  Different manufacturers use different beep codes, so you will have to consult
  the manual for your computer’s motherboard, or take your computer in for
  repairs.</p>
</section>

<section id="fans">
  <title>Ventilateurs en fonctionnement, mais pas d'affichage</title>
  <p>Vérifiez d'abord que votre moniteur est branché et allumé.</p>
  <p>Il peut aussi s'agir d'une panne matérielle. Quand vous appuyez sur le bouton de mise en marche, les ventilateurs se mettent à tourner, mais le reste ne fonctionne pas. Emmenez votre ordinateur chez un réparateur.</p>
</section>

</page>
