<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-changepassword" xml:lang="de">

  <info>
    <link type="guide" xref="user-accounts#passwords"/>
    <link type="seealso" xref="user-goodpassword"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Halten Sie Ihr Konto sicher, indem Sie Ihr Passwort häufig in den Benutzereinstellungen ändern.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2019.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2019.</mal:years>
    </mal:credit>
  </info>

  <title>Ändern Ihres Passworts</title>

  <p>Es ist generell eine gute Idee, Ihr Passwort von Zeit zu Zeit zu ändern, insbesondere dann, wenn Sie vermuten, dass jemand Ihr Passwort kennen könnte.</p>

  <p>Sie benötigen <link xref="user-admin-explain">Systemverwalterrechte</link>, um fremde Benutzerkonten zu bearbeiten.</p>

  <steps>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Benutzer</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Benutzer</gui>, um das Panel zu öffnen.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>·····</gui> neben <gui>Passwort</gui>. Wenn Sie das Passwort für einen anderen Benutzer ändern, müssen Sie die Eingabe zuerst <gui>entsperren</gui>.</p>
    </item>
    <item>
      <p>Geben Sie Ihr aktuelles Passwort und dann ein neues Passwort ein. Geben Sie danach im Feld <gui>Passwort bestätigen</gui> Ihr neues Passwort erneut ein.</p>
      <p>Klicken Sie auf das Symbol <gui style="button"><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16">
      <span its:translate="yes">Ein Passwort erzeugen</span></media></gui>, um automatisch ein zufälliges Passwort zu erzeugen.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Ändern</gui>.</p>
    </item>
  </steps>

  <p>Stellen Sie sicher, dass Sie <link xref="user-goodpassword">ein gutes Passwort gewählt haben</link>. Das hilft Ihnen, die Sicherheit Ihres Benutzerkontos zu gewährleisten.</p>

  <note>
    <p>Wenn Sie Ihr Anmeldepasswort aktualisieren, wird das Passwort für den Anmelde-Schlüsselbund automatisch ebenfalls auf dieses Passwort geändert.</p>
  </note>

  <p>Wenn Sie Ihr Passwort vergessen, kann jeder Benutzer mit Systemverwalterrechten es für Sie ändern.</p>

</page>
