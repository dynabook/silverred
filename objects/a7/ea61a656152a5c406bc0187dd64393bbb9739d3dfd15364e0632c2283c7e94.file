<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="power-dim-screen" xml:lang="hu">

  <info>
    <link type="guide" xref="user-settings"/>
    <link type="seealso" xref="dconf-profiles"/>
    <link type="seealso" xref="dconf-lockdown"/>
    <revision pkgversion="3.12" date="2014-06-20" status="candidate"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>

    <desc>A képernyő elhomályosíttatása egy megadott idő után, amikor a felhasználó tétlen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Úr Balázs</mal:name>
      <mal:email>ur.balazs@fsf.hu</mal:email>
      <mal:years>2018, 2019, 2020.</mal:years>
    </mal:credit>
  </info>

  <title>Képernyő elhomályosítása, amikor a felhasználó tétlen</title>

  <p>Elhomályosíttathatja a számítógép képernyőjét, miután a számítógép üresjáratba került (nincs használva) egy kis idő elteltével.</p>

  <steps>
    <title>Képernyő elhomályosítása üresjáratú számítógépen</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
    <item>
      <p>Hozza létre az <file>/etc/dconf/db/local.d/00-power</file> kulcsfájlt, hogy információkat biztosítson a <sys>local</sys> adatbázisnak.</p>
      <listing>
        <title><file>/etc/dconf/db/local.d/00-power</file></title>
<code>
# A dconf útvonal megadása
[org/gnome/settings-daemon/plugins/power]

# Képernyő elhomályosításának engedélyezése
idle-dim=true

# Fényesség beállítása az elhomályosítás után
idle-brightness=30
</code>
      </listing>
    </item>
    <item>
      <p>Hozza létre az <file>/etc/dconf/db/local.d/00-session</file> kulcsfájlt, hogy információkat biztosítson a <sys>local</sys> adatbázisnak.</p>
      <listing>
        <title><file>/etc/dconf/db/local.d/00-session</file></title>
<code>
# A dconf útvonal megadása
[org/gnome/desktop/session]

# A tétlenség ideje másodpercben, mielőtt a munkamenetet tétlennek tekintik
idle-delay=uint32 300
</code>
      </listing>
      <p>Fel kell vennie az <code>uint32</code> típust is az egész szám kulcsértéke mellé, amint látható.</p>
    </item>
    <item>
      <p>Hogy megakadályozza a felhasználót a beállítások felülbírálásában, hozza létre az <file>/etc/dconf/db/local.d/locks/power-saving</file> fájlt a következő tartalommal:</p>
      <listing>
        <title><file>/etc/dconf/db/local.db/locks/power-saving</file></title>
<code>
# Képernyő elhomályosításának és tétlenség időkorlátjának zárolása
/org/gnome/settings-daemon/plugins/power/idle-dim
/org/gnome/settings-daemon/plugins/power/idle-brightness
/org/gnome/desktop/session/idle-delay
</code>
      </listing>
      <p>Ha meg szeretné engedni a felhasználónak a beállítások megváltoztatását, akkor hagyja ki ezt a lépést.</p>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
  </steps>

</page>
