<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mem-check" xml:lang="cs">

  <info>
    <revision pkgversion="3.11" date="2014-01-28" status="candidate"/>
    <link type="guide" xref="index#memory" group="memory"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author copyright">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
      <years>2011</years>
    </credit>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2011, 2014</years>
    </credit>

    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Karta <gui>Prostředky</gui> vám říká, kolik operační paměti počítače (RAM) je použito.</desc>
  </info>

  <title>Kolik paměti je používáno?</title>

   <p>Když chcete vědět aktuální využití paměti počítače:</p>

  <steps>
    <item>
      <p>Klikněte na kartu <gui>Prostředky</gui>.</p>
    </item>
  </steps>

  <p><gui>Historie použití paměti a odkládacího prostoru</gui> zobrazuje průběžný čárový graf pro paměť a odkládací prostor jako procentuální podíl s celkově dostupné kapacity. Je vykreslován vůči plynutí času, s aktuálním časem napravo.</p>

  <note style="tip">
    <p>Barva každé z čar koresponduje s koláčovými grafy níže. Kliknutím na koláčový graf můžete změnit i barvu čárového grafu.</p>
  </note>

  <p>Koláčový graf <gui>Paměť</gui> ukazuje využití paměti v <link xref="units">GiB</link> a jako procentuální podíl z celkové kapacity paměti.</p>

  <p>Abyste změnili <gui>interval aktualizace</gui>:</p>

  <steps>
    <item>
      <p>Klikněte na <guiseq><gui>Sledování systému</gui><gui>Předvolby</gui></guiseq>.</p>
    </item>
    <item>
      <p>Klikněte na kartu <gui>Prostředky</gui>.</p>
    </item>
    <item>
      <p>Zadejte hodnotu do <gui>Interval aktualizace v sekundách</gui>.</p>
    </item>
  </steps>

<section id="highusage">
  <title>Který proces využívá nejvíce paměti?</title>

  <p>Když chcete vědět, který proces využívá nejvíce paměti:</p>

  <steps>	
    <item>
      <p>Klikněte na kartu <gui>Procesy</gui>.</p>
    </item>
    <item>
      <p>Klikněte na záhlaví sloupce <gui>Paměť</gui>, aby se procesy seřadily podle množství použité paměti.</p>
      <note>
      	<p>Šipka v záhlaví sloupce ukazuje směr řazení. Novým kliknutím směr otočíte. Když šipka ukazuje nahoru, procesy využívající nejvíce paměti se objeví v horní části seznamu.</p>
      </note>
    </item>
  </steps>
</section>

</page>
