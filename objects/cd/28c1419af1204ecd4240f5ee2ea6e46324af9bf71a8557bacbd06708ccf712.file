<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting-initial-check" xml:lang="nl">

  <info>
    <link type="next" xref="net-wireless-troubleshooting-hardware-info"/>
    <link type="guide" xref="net-wireless-troubleshooting"/>

    <revision pkgversion="3.4.0" date="2012-03-05" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <credit type="author">
      <name>Mensen die bijdragen aan de Ubuntu-documentatie wiki</name>
    </credit>
    <credit type="author">
      <name>Gnome-documentatieproject</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Zorg ervoor dat de instellingen voor een eenvoudig netwerk juist zijn, en bereid u voor op de volgende stappen om het probleem op te lossen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Probleemoplosser draadloos netwerk</title>
  <subtitle>Voer eerst een vebindingscontrole uit</subtitle>

  <p>In deze stap zult u enige basisinformatie controleren over uw draadloze netwerkverbinding. Dit is bedoeld om na te gaan of het misschien om een relatief eenvoudig netwerkprobleem gaat, bijvoorbeeld dat het draadloos netwerk niet ingeschakeld is, en om u voor te bereiden op de volgende stappen om het probleem op te lossen.</p>

  <steps>
    <item>
      <p>Zorg ervoor dat uw laptop geen <em>bekabelde</em> internetverbinding heeft.</p>
    </item>
    <item>
      <p>Als u een externe draadloze netwerkadapter heeft (zoals een draadloze USB-adapter of een PCMCIA-kaart die in uw laptop gestoken moet worden), controleer dan of deze in de juiste sleuf op uw computer is gestoken.</p>
    </item>
    <item>
      <p>Als uw draadloos netwerkkaart zich <em>in</em> uw computer bevindt, controleer dan of de schakelaar (indien aanwezig) van uw draadloos netwerkadapter aanstaat. Laptops hebben vaak schakelaars die aan- en uitgezet kunnen worden via een toetsencombinatie op het toetsenbord.</p>
    </item>
    <item>
      <p>Open the
      <gui xref="shell-introduction#systemmenu">system menu</gui> from the right
      side of the top bar and select the Wi-Fi network, then select <gui>Wi-Fi
      Settings</gui>. Make sure that the <gui>Wi-Fi</gui> switch is set to on.
      You should also check that <link xref="net-wireless-airplane">Airplane
      Mode</link> is <em>not</em> switched on.</p>
    </item>
    <item>
      <p>Open een terminalvenster, typ <cmd>lspci device</cmd> in en druk daarna op <key>Enter</key>.</p>
      <p>Dit zal informatie tonen over uw netwerkhardware en verbindingsstatus. Zoek naar informatie in de lijst en kijk of er een sectie is over de draadloze netwerkadapter. Als de staat <code>Connected</code> is, betekent dit dat de adapter werkt en verbonden is met uw draadloze router.</p>
    </item>
  </steps>

  <p>Als u verbonden bent met uw draadloze router, maar u heeft nog steeds geen toegang tot het internet, dan kan het zijn dat uw router niet goed ingesteld is, of uw Internet Service Provider (ISP) mogelijk technische problemen heeft. Raadpleeg de handleidingen van uw router en ISP-setup of neem contact op met uw ISP voor hulp.</p>

  <p>Als de informatie van <cmd>nmcli device</cmd> niet aangaf dat u met een netwerk verbonden bent, klik dan op <gui>Volgende</gui> om door te gaan naar het volgende deel van problemen oplossen.</p>

</page>
