<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="video-sending" xml:lang="ca">

  <info>
    <link type="guide" xref="media#videos"/>
    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.12.1" date="2014-03-30" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.17.90" date="2015-08-30" status="final"/>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Comproveu que tinguin els còdecs de vídeo correctes instal·lats.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

  <title>Altres persones no poden reproduir els vídeos que he realitzat</title>

  <p>Si heu fet un vídeo a un ordinador amb Linux i l'heu enviat a algú que fa servir Windows o Mac OS, és possible que es trobin amb problemes per reproduir el vídeo.</p>

  <p>Per poder reproduir el seu vídeo, la persona a qui l'enviï ha de tenir els <em>còdecs</em> correctament instal·lats. Un còdec és una petita peça de programari que sap com extreure el vídeo i mostrar-lo a la pantalla. Hi ha molts formats de vídeo diferents i cadascun requereix un còdec diferent per reproduir-lo. Podeu comprovar quin és el format del vostre vídeo:</p>

  <list>
    <item>
      <p>Obriu <app>Fitxers</app> des de la vista general d'<gui xref="shell-introduction#activities">Activitats</gui>.</p>
    </item>
    <item>
      <p>Feu clic amb el botó dret sobre el fitxer de vídeo i seleccioneu <gui>Propietats</gui>.</p>
    </item>
    <item>
      <p>Aneu a la pestanya <gui>Àudio/Vídeo</gui> o <gui>Vídeo</gui> i mireu quins <gui>Còdecs</gui> hi ha instal·lats per <gui>Vídeo</gui> i <gui>Àudio</gui> (si el vídeo també té àudio).</p>
    </item>
  </list>

  <p>Pregunteu a la persona que tingui problemes amb la reproducció si té instal·lat el còdec correcte. Pot ser útil cercar a la web el nom del còdec i el nom de la seva aplicació de reproducció de vídeo. Per exemple, format <em>Theora</em> i teniu un amic que fa servir Windows Media Player per intentar veure-ho, cerqueu "theora windows media player". Sovint podreu descarregar el còdec adequat de forma gratuïta si no està instal·lat.</p>

  <p>Si no trobeu el còdec correcte, proveu el <link href="http://www.videolan.org/vlc/">Reproductor Multimèdia VLC</link>. Funciona a Windows, Mac OS i Linux, i admet molts formats de vídeo diferents. Si falla, proveu de convertir el vostre vídeo en un format diferent. La majoria d'editors de vídeo poden fer-ho, i hi ha aplicacions específiques de conversió de vídeo disponibles. Consulteu l'aplicació de l'instal·lador de programari per veure què hi ha disponible.</p>

  <note>
    <p>Hi ha alguns altres problemes que poden fer que no es reprodueixi el vostre vídeo. El vídeo pot haver estat malmès quan l'heu enviat (de vegades, els fitxers grans no es copien perfectament), hi podria haver problemes amb l'aplicació de reproducció de vídeo, o potser el vídeo no s'ha creat correctament (podria haver-hi hagut errors quan l'heu desat).</p>
  </note>

</page>
