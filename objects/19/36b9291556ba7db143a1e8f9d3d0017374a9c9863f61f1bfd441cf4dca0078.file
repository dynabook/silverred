<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-recover" xml:lang="te">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>
    <link type="seealso" xref="files-lost"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="review"/>

    <credit type="author">
      <name>గ్నోమ్ పత్రీకరణ పరియోజన</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>తొలగించిన ఫైళ్ళు సాధారణంగా చెత్తబుట్టకు పంపబడును, అయితే మరలా తిరిగి పొందవచ్చు.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Praveen Illa</mal:name>
      <mal:email>mail2ipn@gmail.com</mal:email>
      <mal:years>2011, 2014. </mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>కృష్ణబాబు క్రొత్తపల్లి</mal:name>
      <mal:email>kkrothap@redhat.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

  <title>చెత్తబుట్ట నుండి ఫైలును తిరిగిపొందుము</title>

  <p>ఒకవేళ మీరు ఫైలును ఫైల్ నిర్వాహికతో తొగించితే, ఫైలు సాధారణంగా <gui>చెత్తబుట్ట</gui> నందు వుంచబడును, మరియు దానిని తిరిగిపొందవచ్చు.</p>

  <steps>
    <title>చెత్తబుట్ట నుండి ఫైల్ తిరిగిపొందుట:</title>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <app>Files</app>.</p>
    </item>
    <item>
      <p>Click on <app>Files</app> to open the file manager.</p>
    </item>
    <item>
      <p>Click <gui>Trash</gui> in the sidebar. If you do not see the sidebar,
      press the menu button in the top-right corner of the window and select
      <gui>Sidebar</gui>.</p>
    </item>
    <item>
      <p>If your deleted file is there, click on it and select
      <gui>Restore</gui>. It will be restored to the folder from where it was
      deleted.</p>
    </item>
  </steps>

  <p>If you deleted the file by pressing <keyseq><key>Shift</key><key>Delete
  </key></keyseq>, or by using the command line, the file has been permanently
  deleted. Files that have been permanently deleted can’t be recovered from the
  <gui>Trash</gui>.</p>

  <p>There are a number of recovery tools available that are sometimes able to
  recover files that were permanently deleted. These tools are generally not
  very easy to use, however. If you accidentally permanently deleted a file,
  it’s probably best to ask for advice on a support forum to see if you can
  recover it.</p>

</page>
