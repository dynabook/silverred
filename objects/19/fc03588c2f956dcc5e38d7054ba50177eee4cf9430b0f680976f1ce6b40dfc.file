<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-fingerprint" xml:lang="sr">

  <info>
    <link type="guide" xref="hardware-auth"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-03" status="review"/>
    <revision pkgversion="3.12" date="2014-06-16" status="final"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="final"/>

    <credit type="author">
      <name>Гномов пројекат документације</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Пол В. Фрилдс</name>
      <email>stickster@gmail.com</email>
      <years>2011</years>
    </credit>
    <credit type="author editor">
      <name>Екатерина Герасимова</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Џим Кембел</name>
      <email>jcampbell@gnome.org</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Можете да се пријавите на ваш систем користећи подржане читаче отисака прстију уместо да куцате вашу лозинку.</desc>
  </info>

  <title>Пријавите се читачем отисака</title>

  <p>Ако ваш систем има подржан читач отисака прстију, можете да забележите ваш отисак прста и да га користите за пријављивање.</p>

<section id="record">
  <title>Забележите отисак прста</title>

  <p>Да бисте могли да се пријављујете отиском прста, морате као прво да га забележите како би систем могао да га користи када вас препознаје.</p>

  <note style="tip">
    <p>Ако је ваш прст превише сув, можете имати потешкоћа приликом бележења вашег отиска прста. Ако до овога дође, навлажите мало ваш прст, обришите га чистом крпом, без влакана, и покушајте поново.</p>
  </note>

  <p>Потребна су вам <link xref="user-admin-explain">Администраторска права</link> за уређивање корисничких налога који нису ваши.</p>

  <steps>
    <item>
      <p>Отворите преглед <gui xref="shell-introduction#activities">Активности</gui> и почните да куцате <gui>Корисници</gui>.</p>
    </item>
    <item>
      <p>Кликните на <gui>Корисници</gui> да отворите панел.</p>
    </item>
    <item>
      <p>Притисните <gui>Искључено</gui>, поред <gui>Пријављивање отиском прста</gui> да додате отисак прста за изабрани налог. Ако додајете отисак прста за неког другог корисника, мораћете као прво да <gui>Откључате</gui> панел.</p>
    </item>
    <item>
      <p>Изаберите прст који желите да користите за отисак прста, затим <gui style="button">Следеће</gui>.</p>
    </item>
    <item>
      <p>Пратите упутства у прозорчету и превуците прст <em>умереном брзином</em> преко читача отисака прстију. Када рачунар буде имао добар снимак вашег отиска прста, видећете поруку <gui>Готово!</gui>.</p>
    </item>
    <item>
      <p>Изаберите <gui>Следеће</gui>. Видећете поруку потврдну поруку о томе да је ваш отисак прста успешно сачуван. Изаберите <gui>Затвори</gui> да завршите.</p>
    </item>
  </steps>

</section>

<section id="verify">
  <title>Проверите да ли ваши отисци прстију раде</title>

  <p>Сада проверите да ли ваше пријављивање отиском прста ради. Ако сте забележили отисак прста, још увек ћете имати опцију пријављивања вашом лозинком.</p>

  <steps>
    <item>
      <p>Сачувајте све отворене радове, затим <link xref="shell-exit#logout">се одјавите</link>.</p>
    </item>
    <item>
      <p>На екрану за пријављивање, изаберите ваше име са списка. Појавиће се прозорче за унос лозинке.</p>
    </item>
    <item>
      <p>Уместо да укуцате вашу лозинку, превуците ваш прст преко читача отисака.</p>
    </item>
  </steps>

</section>

</page>
