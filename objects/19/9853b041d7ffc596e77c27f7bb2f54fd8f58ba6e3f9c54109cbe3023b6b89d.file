<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-notifications" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="color#problems"/>
    <link type="seealso" xref="color-why-calibrate"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>您会被提醒颜色配置文件是陈旧的和不准确的。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>当我的颜色配置文件不准确时，会显示提示信息吗？</title>

  <p>You can be reminded to recalibrate your devices after a specific period of
  time. Unfortunately, it is not possible to tell without recalibrating whether
  a device profile is accurate, so it is best to recalibrate devices
  regularly.</p>

  <p>Some companies have very specific calibration expiry policies for
  profiles, as an inaccurate color profile can make a huge difference to an end
  product.</p>

  <p>If you set the timeout policy and a profile is older than the policy then
  a red warning triangle will be shown in the <gui>Color</gui> panel next to
  the profile. A warning notification will also be shown every time you log
  into your computer.</p>

  <p>要设置显示器或打印机设备的策略，您要在一天中指定大量的配置文件：</p>

<screen>
<output style="prompt">$ </output><input>gsettings set org.gnome.settings-daemon.plugins.color recalibrate-printer-threshold 180</input>
<output style="prompt">$ </output><input>gsettings set org.gnome.settings-daemon.plugins.color recalibrate-display-threshold 90</input>
</screen>

</page>
