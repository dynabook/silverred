<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-add" xml:lang="sr">

  <info>
    <link type="guide" xref="accounts" group="add"/>

    <revision pkgversion="3.5.5" date="2012-08-14" status="draft"/>
    <revision pkgversion="3.9.92" date="2012-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <credit type="author">
      <name>Џим Кембел</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Мајкл Хил</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Шон Мек Кенс</name>
      <email>shaunm@gnome.org</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Дозволите програмима да приступе вашим налозима на мрежи због фотографија, контаката, календара и осталог.</desc>
</info>

  <title>Додајте налог</title>

  <p>Додавање налога ће помоћи у повезивању ваших налога на мрежи са вашом Гномовом радном површи. Зато ће вам, ваш програм за пошту, програм за ћаскање и остали односни програми бити подешени.</p>

  <steps>
    <item>
      <p>Отворите преглед <gui xref="shell-terminology">Активности</gui> и почните да куцате <gui>Налози на мрежи</gui>.</p>
    </item>
    <item>
      <p>Кликните на <gui>налоге на мрежи</gui> да отворите панел.</p>
    </item>
    <item>
      <p>Select an account from the list on the right.</p>
    </item>
    <item>
      <p>Изаберите врсту налога који желите да додате.</p>
    </item>
    <item>
      <p>Отвориће се прозорче веб сајта у коме ћете моћи да унесете податке вашег налога на мрежи. На пример, ако подешавате Гуглов налог, унесите ваше корисничко име и лозинку за Гугл. Неки достављачи вам омогућавају да направите нови налог из прозорчета за пријављивање.</p>
    </item>
    <item>
      <p>Ако сте исправно унели ваше податке, биће вам затражено да дозволите Гномов приступ вашем налогу на мрежи. Овластите приступ да наставите.</p>
    </item>
    <item>
      <p>All services that are offered by an account provider will be enabled
      by default. <link xref="accounts-disable-service">Switch</link>
      individual services to off to disable them.</p>
    </item>
  </steps>

  <p>Након што додате налоге, програми ће моћи да користе те налоге за услуге које сте изабрали. Видите <link xref="accounts-disable-service"/> о томе како да одлучите које услуге ћете омогућити.</p>

  <note style="tip">
    <p>Многе услуге на мрежи обезбеђују чин потврђивања идентитета који Гном чува уместо ваше лозинке. Ако уклоните налог, такође треба да опозовете то уверење са услуге на мрежи. Видите <link xref="accounts-remove"/> за више података.</p>
  </note>

</page>
