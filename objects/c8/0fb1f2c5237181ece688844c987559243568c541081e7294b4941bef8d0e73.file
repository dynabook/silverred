<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-location" xml:lang="ca">
  <info>
    <link type="guide" xref="privacy" group="#last"/>

    <revision pkgversion="3.14" date="2014-10-13" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Activeu o desactiveu la geolocalització.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

  <title>Controlar els serveis d'ubicació</title>

  <p>La localització o els serveis de geolocalització utilitzen les torres de telefonia, el GPS, i els punts d'accés Wi-Fi propers per determinar la vostra ubicació actual i utilitzar-la per configurar la zona horària i per a aplicacions com ara <app>Mapes</app>. Quan està habilitada, és possible que la vostra ubicació es comparteixi a la xarxa amb molta precisió.</p>

  <steps>
    <title>Desactiveu les funcions de geolocalització de l'escriptori</title>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a escriure <gui>Privacitat</gui>.</p>
    </item>
    <item>
      <p>Feu clic a <gui>Privacitat</gui> per obrir el quadre.</p>
    </item>
    <item>
     <p>Canvieu <gui>Serveis d'ubicació</gui> a <gui>OFF</gui>.</p>
     <p>Per a tornar a activar aquesta funcionalitat poseu els <gui>Serveis d'ubicació</gui> a <gui>ON</gui>.</p>
    </item>
  </steps>

</page>
