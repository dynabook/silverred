<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-airplane" xml:lang="cs">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.24" date="2017-03-26" status="final"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>

    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2015</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2015</years>
    </credit>

    <desc>Otevřete nastavení sítě a přepněte Režim „letadlo“ do polohy zapnuto.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

<title>Vypnutí bezdrátových zařízení (režim letadlo)</title>

<p>Když jste se svým počítačem v letadle (nebo někde jinde, kde není dovoleno používat bezdrátová zařízení), měli byste bezdrátové funkce vypnout. Můžete je chtít vypnout také z jiných důvodů (například kvůli úspoře energie z baterie).</p>

  <note>
    <p>Použití <em>režimu letadlo</em> úplně vypne bezdrátová připojení, včetně Wi-Fi, 3G a Bluetooth.</p>
  </note>

  <p>Když chcete zapnout režim letadlo:</p>

  <steps>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Wi-Fi</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Wi-Fi</gui> otevřete příslušný panel.</p>
    </item>
    <item>
      <p>Přepněte vypínač <gui>Režim „letadlo“</gui> do polohy zapnuto. Tím se vypnou všechna bezdrátová připojení, dokud režim letadlo zase nevypnete.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Své připojení přes Wi-Fi můžete vypnout v <gui xref="shell-introduction#systemmenu">nabídce systému</gui> kliknutím na název připojení a zvolením <gui>Vypnout</gui>.</p>
  </note>

</page>
