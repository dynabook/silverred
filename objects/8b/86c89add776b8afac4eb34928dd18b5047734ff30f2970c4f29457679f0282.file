<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="solaris-mode" xml:lang="pt-BR">
  <info>
    <revision version="0.2" pkgversion="3.11" date="2014-01-26" status="review"/>
    <link type="guide" xref="index#other" group="other"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author copyright">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
      <years>2011</years>
    </credit>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2011, 2014</years>
    </credit>

    <desc>Use modo Solaris para refletir o número de CPUs.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>O que é o modo Solaris?</title>

  <p>Em um sistema que possua múltiplas CPUs ou <link xref="cpu-multicore">núcleos</link>, processos podem usar mais de uma ao mesmo tempo. É possível para a coluna <gui>% CPU</gui> para exibir valores que totalizam mais do que 100% (i.e. 400% em um sistema com 4 CPUs). O <gui>Modo Solaris</gui> divide o <gui>% CPU</gui> para cada processo pelo número de CPUs no sistema de forma que o total será 100%.</p>

  <p>Para exibir o <gui>% CPU</gui> no <gui>Modo Solaris</gui>:</p>

  <steps>
    <item><p>Clique em <gui>Preferências</gui> no menu de aplicativo.</p></item>
    <item><p>Clique na aba <gui>Processos</gui>.</p></item>
    <item><p>Selecione<gui>Dividir uso da CPU pela contagem de CPU</gui>.</p></item>
  </steps>

    <note><p>O termo <gui>Modo Solaris</gui> deriva do UNIX da Sun, comparado ao padrão do Linux de modo IRIX, nomeado assim para UNIX da SGI.</p></note>

</page>
