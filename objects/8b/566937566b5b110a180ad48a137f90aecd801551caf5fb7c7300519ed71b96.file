<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="sound-nosound" xml:lang="nl">

  <info>
    <link type="guide" xref="sound-broken"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.18" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Gnome-documentatieproject</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc> Zorg ervoor dat Dempen niet is aangevinkt, de kabels op de juiste manier zijn aangesloten en dat de geluidskaart gedetecteerd is.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

<title>Ik hoor helemaal geen geluid op de computer</title>

  <p>Als u geen geluid hoort op uw computer, bijvoorbeeld bij het afspelen van muziek, probeer dan de volgende stappen uit om te zien of u het probleem kunt oplossen.</p>

<section id="mute">
  <title>Zorg ervoor dat het geluid niet gedempt is.</title>

  <p>Open the <gui xref="shell-introduction#systemmenu">system menu</gui> and make sure that
  the sound is not muted or turned down.</p>

  <p>Sommige laptops hebben schakelaars of toetsen op hun toetsenbord om het geluid te dempen—probeer op die toets te drukken om te kijken of het geluid daardoor niet meer gedempt wordt.</p>

  <p>U dient ook te controleren of u het geluid van de toepassing die u gebruikt voor het afspelen van geluid (b.v. uw muziekspeler of mediaspeler) niet heeft gedempt. Mogelijk heeft de toepassing een knop voor het dempen of een volumeknop in haar hoofdvenster, dus controleer dit a.u.b.</p>

  <p>Ook kunt u het tabblad <gui>Toepassingen</gui> in de <gui>Geluid</gui>-gui controleren:</p>
  <steps>
    <item>
      <p>Open het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht en typ <gui>Geluid</gui>.</p>
    </item>
    <item>
      <p>Klik op <gui>Geluid</gui> om het paneel te openen.</p>
    </item>
    <item>
      <p>Under <gui>Volume Levels</gui>, check that your application is not
      muted.</p>
    </item>
  </steps>

</section>

<section id="speakers">
  <title>Controleer of de luidsprekers op de juiste manier verbonden en ingeschakeld zijn.</title>
  <p>Als uw computer externe luidsprekers heeft, controleer dan of ze ingeschakeld zijn en het volume hoog genoeg staat. Controleer of de luidsprekerkabel goed is aangesloten op de ‘uitvoer’-audioaansluiting op uw computer. Deze aansluiting is gewoonlijk lichtgroen van kleur.</p>

  <p>Sommige geluidskaarten kunnen de aansluitingen die ze voor uitvoer (naar de luidsprekers) en invoer (van bijvoorbeeld een microfoon) gebruiken omwisselen. De uitvoeraansluiting kan anders zijn op Linux dan op Windows of Mac OS. Probeer de luidsprekerkabel op een andere audio-aansluiting op uw computer aan te sluiten.</p>

 <p>Een laatste stap is het controleren of de audiokabel goed aangesloten is op de achterkant van de luidsprekers. Sommige luidsprekers hebben ook meer dan één invoer.</p>
</section>

<section id="device">
  <title>Controleer of het juiste geluidsapparaat is geselecteerd</title>

  <p>Op sommige computers zijn meerdere “geluidsapparaten” geïnstalleerd. Sommige hiervan kunnen geluid uitvoeren en sommige niet, dus moet u ervoor zorgen dat u het juiste apparaat geselecteerd heeft. Dit kan inhouden dat u wat moet uitproberen voordat u het juiste apparaat heeft.</p>

  <steps>
    <item>
      <p>Open het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht en typ <gui>Geluid</gui>.</p>
    </item>
    <item>
      <p>Klik op <gui>Geluid</gui> om het paneel te openen.</p>
    </item>
    <item>
      <p>Under <gui>Output</gui>, change the <gui>Profile</gui> settings for the
      selected device and play a sound to see if it works. You might need to go
      through the list and try each profile.</p>

      <p>Als dat niet werkt, dan kunt u proberen hetzelfde te doen voor elk ander apparaat dat in de lijst staat.</p>
    </item>
  </steps>

</section>

<section id="hardware-detected">

 <title>Controleren of de geluidskaart op de juiste manier werd gedetecteerd</title>

  <p>Het kan zijn dat uw geluidskaart niet gedetecteerd werd omdat er geen stuurprogramma voor de kaart geïnstalleerd is. U dient mogelijk het stuurprogramma voor de kaart handmatig te installeren. Hoe u dat doet hangt af van de kaart die u heeft.</p>

  <p>Voer de opdracht <cmd>lspci</cmd> uit in de <app>Terminal</app> om er achter te komen welke geluidskaart u heeft:</p>
  <steps>
    <item>
      <p>Ga naar het <gui>Activiteiten</gui>-overzicht en open een terminalvenster.</p>
    </item>
    <item>
      <p>Voer <cmd>lspci</cmd> uit als <link xref="user-admin-explain">superuser</link>; typ <cmd>sudo lspci</cmd> en voer uw wachtwoord in, of typ <cmd>su</cmd>, geef het <em>root</em>(beheerders)-wachtwoord op, en typ dan <cmd>lspci</cmd>.</p>
    </item>
    <item>
      <p>Ga na of er een <em>audiocontroller</em> of <em>audio-apparaat</em> in de lijst staat: in zo een geval zou u het merk- en modelnummer van de geluidskaart moeten zien. Ook toont <cmd>lspci -v</cmd> een lijst met meer gedetailleerde informatie.</p>
    </item>
  </steps>

  <p>U kunt mogelijk stuurprogramma's voor uw kaart vinden en installeren door te zoeken op het internet. Het beste kunt u ondersteuningsforums (of iets dergelijks) voor uw Linux-distributie benaderen voor instructies.</p>

  <p>Als u geen stuurprogramma's voor uw geluidskaart kunt krijgen, dan kunt u overwegen een nieuwe geluidskaart aan te schaffen. U kunt externe USB-geluidskaarten kopen, of geluidskaarten die in de computer kunnen worden geplaatst.</p>

</section>

</page>
