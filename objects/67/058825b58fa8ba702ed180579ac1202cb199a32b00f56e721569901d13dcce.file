<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="printing-2sided" xml:lang="it">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Stampa fronte e retro o più pagine per foglio.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Stampare fronte-retro e più pagine per foglio</title>

  <p>Per stampare su entrambi i lati di un foglio:</p>

  <steps>
    <item>
      <p>Open the print dialog by pressing
      <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>Andare nella scheda <gui>Impostazione pagina</gui> nella finestra Stampa e scegliere un'opzione dal menù a discesa <gui>Fronte-retro</gui>. Se l'opzione è disattivata, la stampa fronte-retro non è disponibile per la propria stampante.</p>
      <p>Printers handle two-sided printing in different ways. It is a good
      idea to experiment with your printer to see how it works.</p>
    </item>
    <item>
      <p>È possibile stampare più di una pagina del documento per <em>lato</em> del foglio. Utilizzare l'opzione <gui>Pagine per facciata</gui> a questo scopo.</p>
    </item>
  </steps>

  <note>
    <p>La disponibilità di queste opzioni può dipendere dal tipo di stampante che si possiede, così come dall'applicazione che si sta utilizzando. Quest'opzione potrebbe non essere sempre disponibile.</p>
  </note>

</page>
