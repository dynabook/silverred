<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>apev2mux: GStreamer Good Plugins 1.0 Plugins Reference Manual</title>
<meta name="generator" content="DocBook XSL Stylesheets Vsnapshot">
<link rel="home" href="index.html" title="GStreamer Good Plugins 1.0 Plugins Reference Manual">
<link rel="up" href="ch01.html" title="gst-plugins-good Elements">
<link rel="prev" href="gst-plugins-good-plugins-apedemux.html" title="apedemux">
<link rel="next" href="gst-plugins-good-plugins-aspectratiocrop.html" title="aspectratiocrop">
<meta name="generator" content="GTK-Doc V1.32 (XML mode)">
<link rel="stylesheet" href="style.css" type="text/css">
</head>
<body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF">
<table class="navigation" id="top" width="100%" summary="Navigation header" cellpadding="2" cellspacing="5"><tr valign="middle">
<td width="100%" align="left" class="shortcuts">
<a href="#" class="shortcut">Top</a><span id="nav_description">  <span class="dim">|</span> 
                  <a href="#gst-plugins-good-plugins-apev2mux.description" class="shortcut">Description</a></span><span id="nav_hierarchy">  <span class="dim">|</span> 
                  <a href="#gst-plugins-good-plugins-apev2mux.object-hierarchy" class="shortcut">Object Hierarchy</a></span><span id="nav_interfaces">  <span class="dim">|</span> 
                  <a href="#gst-plugins-good-plugins-apev2mux.implemented-interfaces" class="shortcut">Implemented Interfaces</a></span>
</td>
<td><a accesskey="h" href="index.html"><img src="home.png" width="16" height="16" border="0" alt="Home"></a></td>
<td><a accesskey="u" href="ch01.html"><img src="up.png" width="16" height="16" border="0" alt="Up"></a></td>
<td><a accesskey="p" href="gst-plugins-good-plugins-apedemux.html"><img src="left.png" width="16" height="16" border="0" alt="Prev"></a></td>
<td><a accesskey="n" href="gst-plugins-good-plugins-aspectratiocrop.html"><img src="right.png" width="16" height="16" border="0" alt="Next"></a></td>
</tr></table>
<div class="refentry">
<a name="gst-plugins-good-plugins-apev2mux"></a><div class="titlepage"></div>
<div class="refnamediv"><table width="100%"><tr>
<td valign="top">
<h2><span class="refentrytitle"><a name="gst-plugins-good-plugins-apev2mux.top_of_page"></a>apev2mux</span></h2>
<p>apev2mux</p>
</td>
<td class="gallery_image" valign="top" align="right"></td>
</tr></table></div>
<a name="GstApev2Mux"></a><div class="refsect1">
<a name="gst-plugins-good-plugins-apev2mux.other"></a><h2>Types and Values</h2>
<div class="informaltable"><table class="informaltable" width="100%" border="0">
<colgroup>
<col width="150px" class="other_proto_type">
<col class="other_proto_name">
</colgroup>
<tbody><tr>
<td class="datatype_keyword">struct</td>
<td class="function_name"><a class="link" href="gst-plugins-good-plugins-apev2mux.html#GstApev2Mux-struct" title="struct GstApev2Mux">GstApev2Mux</a></td>
</tr></tbody>
</table></div>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-apev2mux.object-hierarchy"></a><h2>Object Hierarchy</h2>
<pre class="screen">    GObject
    <span class="lineart">╰──</span> GInitiallyUnowned
        <span class="lineart">╰──</span> GstObject
            <span class="lineart">╰──</span> GstElement
                <span class="lineart">╰──</span> GstTagMux
                    <span class="lineart">╰──</span> GstApev2Mux
</pre>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-apev2mux.implemented-interfaces"></a><h2>Implemented Interfaces</h2>
<p>
GstApev2Mux implements
 GstTagSetter.</p>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-apev2mux.description"></a><h2>Description</h2>
<p>This element adds APEv2 tags to the beginning of a stream using the taglib
library.</p>
<p>Applications can set the tags to write using the <span class="type">GstTagSetter</span> interface.
Tags sent by upstream elements will be picked up automatically (and merged
according to the merge mode set via the tag setter interface).</p>
<div class="refsect2">
<a name="id-1.2.13.7.4"></a><h3>Example pipelines</h3>
<div class="informalexample">
  <table class="listing_frame" border="0" cellpadding="0" cellspacing="0">
    <tbody>
      <tr>
        <td class="listing_lines" align="right"><pre>1</pre></td>
        <td class="listing_code"><pre class="programlisting"><span class="n">gst</span><span class="o">-</span><span class="n">launch</span><span class="o">-</span><span class="mf">1.0</span> <span class="o">-</span><span class="n">v</span> <span class="n">filesrc</span> <span class="n">location</span><span class="o">=</span><span class="n">foo</span><span class="p">.</span><span class="n">ogg</span> <span class="o">!</span> <span class="n">decodebin</span> <span class="o">!</span> <span class="n">audioconvert</span> <span class="o">!</span> <span class="n">lame</span> <span class="o">!</span> <span class="n">apev2mux</span> <span class="o">!</span> <span class="n">filesink</span> <span class="n">location</span><span class="o">=</span><span class="n">foo</span><span class="p">.</span><span class="n">mp3</span></pre></td>
      </tr>
    </tbody>
  </table>
</div>
 A pipeline that transcodes a file from Ogg/Vorbis to mp3 format with an
APEv2 that contains the same as the the Ogg/Vorbis file. Make sure the
Ogg/Vorbis file actually has comments to preserve.
<div class="informalexample">
  <table class="listing_frame" border="0" cellpadding="0" cellspacing="0">
    <tbody>
      <tr>
        <td class="listing_lines" align="right"><pre>1</pre></td>
        <td class="listing_code"><pre class="programlisting"><span class="n">gst</span><span class="o">-</span><span class="n">launch</span><span class="o">-</span><span class="mf">1.0</span> <span class="o">-</span><span class="n">m</span> <span class="n">filesrc</span> <span class="n">location</span><span class="o">=</span><span class="n">foo</span><span class="p">.</span><span class="n">mp3</span> <span class="o">!</span> <span class="n">apedemux</span> <span class="o">!</span> <span class="n">fakesink</span> <span class="n">silent</span><span class="o">=</span><span class="n">TRUE</span> <span class="mi">2</span><span class="o">&gt;</span> <span class="o">/</span><span class="n">dev</span><span class="o">/</span><span class="n">null</span> <span class="o">|</span> <span class="n">grep</span> <span class="n">taglist</span></pre></td>
      </tr>
    </tbody>
  </table>
</div>
 Verify that tags have been written.
</div>
<div class="refsynopsisdiv">
<h2>Synopsis</h2>
<div class="refsect2">
<a name="id-1.2.13.7.5.1"></a><h3>Element Information</h3>
<div class="variablelist"><table border="0" class="variablelist">
<colgroup>
<col align="left" valign="top">
<col>
</colgroup>
<tbody>
<tr>
<td><p><span class="term">plugin</span></p></td>
<td>
            <a class="link" href="gst-plugins-good-plugins-plugin-taglib.html#plugin-taglib">taglib</a>
          </td>
</tr>
<tr>
<td><p><span class="term">author</span></p></td>
<td>Sebastian Dröge &lt;slomo@circular-chaos.org&gt;</td>
</tr>
<tr>
<td><p><span class="term">class</span></p></td>
<td>Formatter/Metadata</td>
</tr>
</tbody>
</table></div>
</div>
<hr>
<div class="refsect2">
<a name="id-1.2.13.7.5.2"></a><h3>Element Pads</h3>
<div class="variablelist"><table border="0" class="variablelist">
<colgroup>
<col align="left" valign="top">
<col>
</colgroup>
<tbody>
<tr>
<td><p><span class="term">name</span></p></td>
<td>sink</td>
</tr>
<tr>
<td><p><span class="term">direction</span></p></td>
<td>sink</td>
</tr>
<tr>
<td><p><span class="term">presence</span></p></td>
<td>always</td>
</tr>
<tr>
<td><p><span class="term">details</span></p></td>
<td>ANY</td>
</tr>
</tbody>
</table></div>
<div class="variablelist"><table border="0" class="variablelist">
<colgroup>
<col align="left" valign="top">
<col>
</colgroup>
<tbody>
<tr>
<td><p><span class="term">name</span></p></td>
<td>src</td>
</tr>
<tr>
<td><p><span class="term">direction</span></p></td>
<td>source</td>
</tr>
<tr>
<td><p><span class="term">presence</span></p></td>
<td>always</td>
</tr>
<tr>
<td><p><span class="term">details</span></p></td>
<td>application/x-apetag</td>
</tr>
</tbody>
</table></div>
</div>
</div>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-apev2mux.functions_details"></a><h2>Functions</h2>
<p></p>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-apev2mux.other_details"></a><h2>Types and Values</h2>
<div class="refsect2">
<a name="GstApev2Mux-struct"></a><h3>struct GstApev2Mux</h3>
<pre class="programlisting">struct GstApev2Mux;</pre>
</div>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-apev2mux.see-also"></a><h2>See Also</h2>
<p><span class="type">GstTagSetter</span></p>
</div>
</div>
<div class="footer">
<hr>Generated by GTK-Doc V1.32</div>
</body>
</html>