<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-autorun" xml:lang="nl">

  <info>
    <link type="guide" xref="media#photos"/>
    <link type="guide" xref="media#videos"/>
    <link type="guide" xref="media#music"/>
    <link type="guide" xref="files#removable"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-10-04" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="author">
      <name>Gnome-documentatieproject</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Het automatisch draaien van toepassingen voor cd's en dvd's, camera's, audiospelers, en andere apparaten en media.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <!-- TODO: fix bad UI strings, then update help -->
  <title>Toepassingen voor apparaten of schijven openen</title>

  <p>U kunt een toepassing automatisch laten starten wanneer u een apparaat aansluit of een schijf of kaart in de computer plaatst. Zo zou u misschien willen dat uw fotobeheerprogramma start wanneer u een digitale camera aansluit. U kunt dit ook uitschakelen, zodat er niets gebeurt wanneer u iets aansluit.</p>

  <p>Om te beslissen welke toepassing gestart moet worden wanneer u verschillende apparaten aansluit:</p>

<steps>
  <item>
    <p>Open het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht en typ <gui>Details</gui>.</p>
  </item>
  <item>
    <p>Klik op <gui>Details</gui> om het paneel te openen.</p>
  </item>
  <item>
    <p>Klik op <gui>Verwijderbare media</gui>.</p>
  </item>
  <item>
    <p>Zoek het gewenste apparaat mediatype, en kies vervolgens een toepassing of handeling voor dat mediatype. Hieronder vindt u een beschrijving van de verschillende apparaat- en mediatypen.</p>
    <p>In plaats van een toepassing te starten kun u het zo instellen dat het apparaat in de bestandsbeheerder wordt weergegeven, met de optie <gui>Map openen</gui>. Als dat gebeurt, zal u worden gevraagd wat u wil doen, of er gebeurt niets automatisch.</p>
  </item>
  <item>
    <p>Als u het apparaat of mediatype dat u wilt wijzigen (zoals Blu-ray schijven of e-boek-lezers) niet ziet staan in de lijst, klik dan op <gui>Andere media…</gui> voor een gedetailleerdere lijst met apparaten. Selecteer het apparaat- of mediatype uit de <gui>Type</gui>-keuzelijst en de toepassing of actie uit de <gui>Actie</gui>-keuzelijst.</p>
  </item>
</steps>

  <note style="tip">
    <p>Als u niet wilt dat toepassingen automatisch geopend worden, wat u ook aansluit, plaats dan een vinkje bij <gui>Nooit vragen en ook geen toepassingen starten als media ingevoerd worden</gui> onderaan het venster <gui>Details</gui>.</p>
  </note>

<section id="files-types-of-devices">
  <title>Apparaat- en mediatypen</title>
<terms>
  <item>
    <title>Audioschijven</title>
    <p>Kies uw favoriete muziektoepassing of cd-ripper om audio-cd's te verwerken. Als u audio-DVD's (DVD-A) gebruikt, moet u onder <gui>Overige media…</gui> aangeven hoe u ze wil openen. Als u een audio-cd met de bestandsbeheerder opent, zullen de tracks als WAV-bestanden worden weergegeven die met elke audiospeler-toepassing af te spelen zijn.</p>
  </item>
  <item>
    <title>Videoschijven</title>
    <p>Kies uw favoriete videotoepassing om DVD's te verwerken. Gebruik de <gui>Overige media…</gui>-knop om een toepassing voor Blu-ray, HD DVD, video CD (VCD), en super video cd (SVCD) in te stellen. Zie <link xref="video-dvd"/> als DVD's of andere videoschijven niet correct functioneren als u ze invoert.</p>
  </item>
  <item>
    <title>Blanco schijven</title>
    <p>Gebruik de <gui>Overige media…</gui>-knop om een brandprogramma voor blanco cd's, blanco DVD's, blanco Blu-ray schijven, en blanco HD DVDs te kiezen.</p>
  </item>
  <item>
    <title>Camera's en fotos</title>
    <p>Gebruik het <gui>Foto's</gui>-keuzemenu om een fotobeheertoepassing te kiezen dat uitgevoerd moet worden als er een digitale camera wordt aangesloten, of als u een mediakaart uit uw camera invoert, zoals een CF, SD, MMC, of MS-kaart. Ook kunt u eenvoudigweg met de bestandsbeheerder door uw bestanden bladeren.</p>
    <p>Onder <gui>Overige media…</gui>, kunt u een toepassing kiezen waarmee Kodak foto-cd's moeten worden geopend, zoals u die in een winkel kunt laten maken. Dit zijn gewone gegevens-cd's waarbij JPEG-afbeeldingen in een map met de naam <file>PICTURES</file> zijn opgeslagen.</p>
  </item>
  <item>
    <title>Muziekspelers</title>
    <p>Kies een toepassing om de muziekbibliotheek op uw draagbare muziekspeler te beheren, of beheer met behulp van de bestandsbeheerder de bestanden zelf.</p>
    </item>
    <item>
      <title>E-boek-lezers</title>
      <p>Gebruik de <gui>Overige media…</gui>-knop om een toepassing te kiezen voor het beheer van de boeken op uw e-boek-lezer, of beheer met behulp van de bestandsbeheerder de bestanden zelf.</p>
    </item>
    <item>
      <title>Software</title>
      <p>Sommige schijven en verwisselbare media bevatten software waarvan het de bedoeling is dat het automatisch wordt uitgevoerd zodra de media wordt ingevoerd. Gebruik de optie <gui>Software</gui> om te bepalen wat er moet gebeuren als er media met autorun software wordt ingevoerd. Er zal u altijd om bevestiging worden gevraagd voordat software wordt uitgevoerd.</p>
      <note style="warning">
        <p>Voer nooit software uit vanaf een medium dat u niet vertrouwt.</p>
      </note>
   </item>
</terms>

</section>

</page>
