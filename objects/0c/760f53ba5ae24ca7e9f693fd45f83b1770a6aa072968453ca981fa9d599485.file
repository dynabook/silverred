<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="clock-world" xml:lang="hu">

  <info>
    <link type="guide" xref="clock" group="#last"/>
    <link type="seealso" href="help:gnome-clocks/index"/>

    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.28" date="2018-07-30" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhill@gnome.org</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Más városok idejének megjelenítése a naptár alatt.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Világóra hozzáadása</title>

  <p>Az <app>Órák</app> használatával más városok idejét nézheti meg.</p>

  <note>
    <p>Ehhez telepítve kell lennie az <app>Órák</app> alkalmazásnak.</p>
    <p>A legtöbb disztribúción az <app>Órák</app> alapesetben telepítve van. Ellenkező esetben telepítenie kell a disztribúció csomagkezelőjével.</p>
  </note>

  <p>Világóra hozzáadásához:</p>

  <steps>
    <item>
      <p>Kattintson az órára a felső sávban.</p>
    </item>
    <item>
      <p>Kattintson a <gui>Világóra hozzáadása</gui> hivatkozásra a naptár alatt az <app>Órák</app> elindításához.</p>

    <note>
       <p>Ha már be van állítva világórája, akkor kattintson rá, és elindul az <app>Órák</app>.</p>
    </note>

    </item>
    <item>
      <p>Az <app>Órák</app> ablakban kattintson az <gui style="button">Új</gui> gombra, vagy nyomja meg a <keyseq><key>Ctrl</key><key>N</key></keyseq> kombinációt új város hozzáadásához.</p>
    </item>
    <item>
      <p>Kezdje el beírni a város nevét a keresőmezőbe.</p>
    </item>
    <item>
      <p>Válassza ki a megfelelő várost vagy az Önhöz legközelebbi helyet a listából.</p>
    </item>
    <item>
      <p>Kattintson a <gui style="button">Hozzáadás</gui> gombra a város felvételének befejezéséhez.</p>
    </item>
  </steps>

  <p>Az <app>Órák</app> további képességeivel kapcsolatban nézze meg az <link href="help:gnome-clocks">Órák súgóját</link>.</p>

</page>
