<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="mouse-problem-notmoving" xml:lang="gu">

  <info>
    <link type="guide" xref="mouse#problems"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <!-- TODO: reorganise page and tidy because it's one ugly wall of text -->
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
        <name>ફીલ બુલ</name>
        <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>How to check why your mouse is not working.</desc>
  </info>

<title>માઉસ પોઇંટર ખસી રહ્યુ નથી</title>

<links type="section"/>

<section id="plugged-in">
 <title>ચકાસો કે માઉસ  તેમાં પ્લગ થયેલ છે</title>
 <p>જો તમારી પાસે કેબલ સાથે માઉસ હોય તો, ચકાસો કે તે તમારાં કમ્પ્યૂટર સાથે પ્લગ થયેલ છે.</p>
 <p>જો તે USB માઉસ હોય તો (લંબચોરસ જોડાણકર્તા સાથે), વિવિધ USB પોર્ટમાં તેને પ્લગ કરવાનો પ્રયત્ન કરો. જો તે PS/2 માઉસ હોય તો (નાનાં સાથે, છ પીન સાથે ગોળ જોડાણકર્તા), ખાતરી કરો કે તે જાંબલી  કિબોર્ડ પોર્ટ કરતા લીલુ માઉસ પોર્ટમાં પ્લગ થયેલ છે. તમારે કમ્પ્યૂટરને પુન:શરૂ કરવાની જરૂર પડી શકે છે જો તે પ્લગ થયેલ ન હોય તો.</p>
</section>

<section id="broken">
 <title>ચકાસો કે માઉસ સાચે જ કામ કરે છે</title>
 <p>વિવિધ કમ્પ્યૂટરમાં માઉસને પ્લગ કરો અને જુઓ જો તે કામ કરે તો.</p>

 <p>If the mouse is an optical or laser mouse, a light should be shining out
 of the bottom of the mouse if it is turned on.  If there is no light, check
 that it is turned on.  If it is and there is still no light, the mouse may be
 broken.</p>
</section>

<section id="wireless-mice">
 <title>વાયરલેસ માઉસને ચકાસી રહ્યા છે</title>

  <list>
    <item>
      <p>Make sure the mouse is turned on. There is often a switch on the
      bottom of the mouse to turn the mouse off completely, so you can take it
      with you without it constantly waking up.</p>
    </item>
   <item><p>જો તમે બ્લુટુથ માઉસને વાપરી રહ્યા હોય ત્યારે, ખાતરી કરો કે તમારાં કમ્પ્યૂટર સાથે માઉસની જોડી હોય. <link xref="bluetooth-connect-device"/> જુઓ.</p></item>
  <item>
   <p>બટન પર ક્લિક કરો અને જુઓ જો માઉસ પોઇંટર હવે ખસેડે તો. અમુક વાયરલેસ માઉસ પાવરને સંગ્રહવા માટે સૂઇ જાય છે, તેથી જવાબ આપી શકતુ નથી જ્યાં સુધી તમે બટન પર ક્લિક કરો. <link xref="mouse-wakeup"/> જુઓ.</p>
  </item>
  <item>
   <p>ચકાસો કે માઉસની બેટરી ચાર્જ થયેલ છે.</p>
  </item>
  <item>
   <p>ખાતરી કરો કે રિસીવર (ડોંગલ) એ કમ્પ્યૂટરમાં પ્લગ થયેલ છે.</p>
  </item>
  <item>
   <p>જો તમારું માઉસ અને રિસીવર વિવિધ રેડિયો ચેનલ પર કામ કરી શકતુ હોય તો, ખાતરી કરો કે તેઓ એજ ચેનલમાં બંને સુયોજિત છે.</p>
  </item>
  <item>
   <p>તમારે જોડાણને સ્થાપિત કરવા માટે બંને માઉસ, રિસીવર પર બટનને દબાવવાની જરૂર પડી શકે છે. તમારાં માઉસની સૂચના પુસ્તિકા પાસે વધારે વિગતો છે જો આ સ્થિતિ છે.</p>
  </item>
 </list>

 <p>
 Most RF (radio) wireless mice should work automatically when you plug them
 into your computer. If you have a Bluetooth or IR (infrared) wireless mouse,
 you may need to perform some extra steps to get it working. The steps might
 depend on the make or model of your mouse.
 </p>
</section>

</page>
