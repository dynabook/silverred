<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-icon" xml:lang="hu">

  <info>
    <link type="guide" xref="a11y"/>

    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <desc>Az akadálymentesítés menü a felső sávon látható, emberi alakot ábrázoló ikon.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Az akadálymentesítés menü megtalálása</title>

  <p>Az <em>akadálymentesítés</em> menüben néhány akadálymentesítési beállítást módosíthat. Ezt a felső sávon látható, körben lévő emberi alakot ábrázoló ikonra kattintással érheti el.</p>

  <figure>
    <desc>Az akadálymentesítési menü a felső sávon található.</desc>
    <media its:translate="no" type="image" mime="image/png" src="figures/universal-access-menu.png"/>
  </figure>

  <p>Ha nem látja az akadálymentesítés menüt, akkor bekapcsolhatja az <gui>Akadálymentesítés</gui> panelen:</p>

  <steps>
    <item>
      <p>Nyissa meg a <gui xref="shell-introduction#activities">Tevékenységek</gui> áttekintést, és kezdje el begépelni az <gui>Akadálymentesítés</gui> szót.</p>
    </item>
    <item>
      <p>Kattintson az <gui>Akadálymentesítés</gui> elemre a panel megnyitásához.</p>
    </item>
    <item>
      <p>Switch the <gui>Always Show Universal Access Menu</gui> switch to
      on.</p>
    </item>
  </steps>

  <p>Ezt a menüt az egér helyett a billentyűzettel is elérheti a <keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq> megnyomásával, ekkor a billentyűzetfókusz átkerül a felső sávra. Egy fehér vonal jelenik meg a <gui>Tevékenységek</gui> gomb alatt – ez jelzi, hogy a felső sávon melyik elem van kijelölve. A nyílbillentyűk használatával mozgassa a fehér vonalat az akadálymentesítési menü ikonja alá, és nyomja meg az <key>Enter</key> billentyűt a megnyitásához. A menü elemeit a fel és le nyílbillentyűkkel választhatja ki. A kijelölt elem átváltáshoz nyomja meg az <key>Enter</key> billentyűt.</p>

</page>
