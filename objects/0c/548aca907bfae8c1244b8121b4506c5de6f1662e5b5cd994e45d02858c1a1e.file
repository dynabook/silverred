<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-admin-change" xml:lang="nl">

  <info>
    <link type="guide" xref="user-accounts#privileges"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Gnome-documentatieproject</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>U kunt wijzigen welke gebruikers wijzigingen in het systeem mogen aanbrengen door hen beheerdersrechten te verlenen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>De persoon met beheersrechten wijzigen</title>

  <p>Door het toekennen van beheersrechten wordt bepaald wie er belangrijke delen van het systeem mogen wijzigen. U kunt wijzigen welke gebruikers wel en welke geen beheersrechten hebben. Het is een goede manier om uw systeem veilig te houden en te voorkomen dat eventuele ongeautoriseerde wijzigingen worden aangebracht.</p>

  <p>Om een accounttype te wijzigen dient u <link xref="user-admin-explain">beheerder</link> te zijn. </p>

  <steps>
    <item>
      <p>Open het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht en typ <gui>Gebruikers</gui>.</p>
    </item>
    <item>
      <p>Klik op <gui>Gebruikersaccounts</gui> om het paneel te openen.</p>
    </item>
    <item>
      <p>Klik op <gui style="button">Ontgrendelen</gui> rechtsboven in de hoek en voer uw wachtwoord in wanneer daar om gevraagd wordt.</p>
    </item>
    <item>
      <p>Selecteer de gebruiker wiens rechten u wilt wijzigen.</p>
    </item>
    <item>
      <p>Klik op het label <gui>Standaard</gui> naast <gui>Accounttype</gui> en selecteer <gui>Beheerder</gui>.</p>
    </item>
    <item>
      <p>De rechten van de gebruiker zullen gewijzigd worden bij de eerstvolgende aanmelding.</p>
    </item>
  </steps>

  <note>
    <p>Het account van de hoofdgebruiker van het systeem heeft doorgaans beheerdersrechten. Dit is het gebruikersaccount dat aangemaakt werd toen u het systeem voor het eerst installeerde.</p>
    <p>Het is onverstandig om veel gebruikers met <gui>Beheerders</gui>-rechten op een systeem te hebben.</p>
  </note>

</page>
