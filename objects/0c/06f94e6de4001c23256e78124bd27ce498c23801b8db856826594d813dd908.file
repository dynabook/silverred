<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="tips-specialchars" xml:lang="fr">

  <info>
    <link type="guide" xref="tips"/>
    <link type="seealso" xref="keyboard-layouts"/>

    <revision pkgversion="3.8.2" version="0.3" date="2013-05-18" status="review"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.26" date="2017-11-27" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Saisir des caractères absents de votre clavier, y compris des alphabets étrangers, des symboles mathématiques et des caractères Unicode.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Saisie de caractères spéciaux</title>

  <p>You can enter and view thousands of characters from most of the world’s
  writing systems, even those not found on your keyboard. This page lists
  some different ways you can enter special characters.</p>

  <links type="section">
    <title>Méthodes de saisie des caractères</title>
  </links>

  <section id="characters">
    <title>Characters</title>
    <p>GNOME comes with a character map application that allows you to
    find and insert unusual characters, including emoji, by browsing
    character categories or searching for keywords.</p>

    <p>You can launch <app>Characters</app> from the Activities overview.</p>

  </section>

  <section id="compose">
    <title>La touche « compose »</title>
    <p>Une touche « compose » est une touche spéciale qui vous permet d'appuyer successivement sur une combinaison de touches afin d'obtenir un caractère spécial. Par exemple, pour obtenir la lettre accentuée <em>é</em>, pressez sur <key>compose</key>, puis sur <key>'</key> et enfin sur <key>e</key>.</p>
    <p>Keyboards don’t have specific compose keys. Instead, you can define
    one of the existing keys on your keyboard as a compose key.</p>

    <note style="important">
      <p>You need to have <app>Tweaks</app> installed on your computer to
      change this setting.</p>
      <if:if xmlns:if="http://projectmallard.org/if/1.0/" test="action:install">
        <p><link style="button" action="install:gnome-tweaks">Install
        <app>Tweaks</app></link></p>
      </if:if>
    </note>

    <steps>
      <title>Configuration d'une touche « compose »</title>
      <item>
        <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
        overview and start typing <gui>Tweaks</gui>.</p>
      </item>
      <item>
        <p>Click <gui>Tweaks</gui> to open the application.</p>
      </item>
      <item>
        <p>Click the <gui>Keyboard &amp; Mouse</gui> tab.</p>
      </item>
      <item>
        <p>Click <gui>Disabled</gui> next to the <gui>Compose Key</gui>
        setting.</p>
      </item>
      <item>
        <p>Turn the switch on in the dialog and pick the keyboard shortcut you
        want to use.</p>
      </item>
      <item>
        <p>Tick the checkbox of the key that you want to set as the Compose
        key.</p>
      </item>
      <item>
        <p>Close the dialog.</p>
      </item>
      <item>
        <p>Close the <gui>Tweaks</gui> window.</p>
      </item>
    </steps>

    <p>Vous pouvez saisir beaucoup de caractères usuels à l'aide de la touche compose, comme par exemple :</p>

    <list>
      <item><p>appuyer sur <key>compose</key>, puis sur <key>'</key> et enfin sur une lettre pour y placer un accent aigu, comme <em>é</em>,</p></item>
      <item><p>appuyer sur <key>compose</key>, puis sur <key>`</key> (accent grave) et enfin sur une lettre pour y placer un accent grave comme <em>è</em>,</p></item>
      <item><p>appuyer sur <key>compose</key>, puis sur <key>"</key> et enfin sur une lettre pour y placer un tréma, comme <em>ë</em>,</p></item>
      <item><p>appuyer sur <key>compose</key>, puis sur <key>-</key> et enfin sur une lettre pour y placer un macron, comme <em>ē</em>.</p></item>
    </list>
    <p>Pour d'autres séquences de compositions, consultez <link href="http://doc.ubuntu-fr.org/caracteres_etendus">cette page de la documentation Ubuntu francophone sur les caractères étendus</link>.</p>
  </section>

<section id="ctrlshiftu">
  <title>Points de code</title>

  <p>You can enter any Unicode character using only your keyboard with the
  numeric code point of the character. Every character is identified by a
  four-character code point. To find the code point for a character, look it up
  in the <app>Characters</app> application. The code point is the four characters
  after <gui>U+</gui>.</p>

  <p>To enter a character by its code point, press
  <keyseq><key>Ctrl</key><key>Shift</key><key>U</key></keyseq>, then type the
  four-character code and press <key>Space</key> or <key>Enter</key>. If you often
  use characters that you can’t easily access with other methods, you might find
  it useful to memorize the code point for those characters so you can enter
  them quickly.</p>

</section>

  <section id="layout">
    <title>Agencements du clavier</title>
    <p>Vous pouvez modifier le comportement de votre clavier pour qu'il ressemble à celui d'une autre langue, sans tenir compte des signes tracés sur les touches. Vous pouvez aussi facilement passer d'un clavier à un autre en le matérialisant par une icône dans la barre supérieure. Pour savoir comment procéder, consultez <link xref="keyboard-layouts"/>.</p>
  </section>

<section id="im">
  <title>Méthodes de saisie</title>

  <p>Une méthode de saisie étend les méthodes précédentes en permettant de saisir des caractères non seulement via le clavier mais aussi via n'importe quel périphérique de saisie. Par exemple, vous pouvez saisir des caractères avec une souris en utilisant une méthode gestuelle, ou saisir des caractères japonais via le clavier latin.</p>

  <p>Pour choisir une méthode de saisie, effectuez un clic-droit sur une sélection de texte, puis dans le menu <gui>Méthode de saisie</gui> choisissez la méthode que vous souhaitez. Aucune méthode par défaut n'est fournie, donc consultez la documentation des méthodes de saisie pour savoir comment les utiliser.</p>

</section>

</page>
