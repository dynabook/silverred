<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="keyboard-compose-key" xml:lang="de">

  <info>
    <link type="guide" xref="user-settings"/>
    <revision pkgversion="3.30" date="2019-02-08" status="draft"/>

    <credit type="author copyright">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Die Compose-Taste per Vorgabe für alle Benutzer aktivieren.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2017, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

  <title>Die Compose-Taste aktivieren</title>

  <p>Um die Compose-Taste zu aktivieren und eine bestimmte Taste auf der Tastatur als Compose-Taste festzulegen, setzen Sie den GSettings-Schlüssel <sys>org.gnome.desktop.input-sources.xkb-options</sys>. Auf diese Weise wird die Einstellung per Vorgabe für alle Benutzer Ihres Systems aktiviert.</p>

  <steps>
    <title>Die rechte Alt-Taste als Compose-Taste festlegen</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
    <item>
      <p>Erstellen Sie eine <sys>local</sys>-Datenbank für systemweite Einstellungen in <file>/etc/dconf/db/local.d/00-input-sources</file>:</p>
        <code>[org/gnome/desktop/input-sources]
# Die rechte Alt-Taste als Compose-Taste festlegen und diese aktivieren
xkb-options=['compose:<var>ralt</var>']</code>
      <p>Wenn Sie eine andere als die rechte <key>Alt</key>-Taste festlegen wollen, ersetzen Sie <var>ralt</var> durch den Namen der Taste, der in der Handbuchseite zu <link href="man:xkeyboard-config"><cmd>xkeyboard-config</cmd>(7)</link> im Abschnitt <em>Position of Compose key</em> angegeben ist.</p>
    </item>
    <item>
      <p>So setzen Sie die Benutzereinstellung außer Kraft und verhindern, dass der Benutzer sie in <file>/etc/dconf/db/local.d/locks/input-sources</file> ändert:</p>
      <code># Liste der aktivierten XKB-Erweiterungen sperren
/org/gnome/desktop/input-sources/xkb-options
</code>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
  </steps>

</page>
