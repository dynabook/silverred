<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-autosuspend" xml:lang="ca">

  <info>
     <link type="guide" xref="power#saving"/>
     <link type="seealso" xref="power-suspend"/>
     <link type="seealso" xref="shell-exit#suspend"/>

    <revision pkgversion="3.20" date="2016-06-15" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2016</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Configureu l'ordinador per aturar-lo automàticament.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

  <title>Configuració automàtica de la suspensió</title>

  <p>Podeu configurar l'ordinador per entrar en suspensió automàticament quan estigui inactiu. Es poden especificar diferents intervals per executar-se amb bateria o connectat a la xarxa.</p>

  <steps>

    <item>
      <p>Obriu la vista general <gui xref="shell-introduction#activities">Activitats</gui> i comenceu a escriure <gui>Energia</gui>.</p>
    </item>
    <item>
      <p>Feu clic a <gui>Energia</gui> per obrir el quadre.</p>
    </item>
    <item>
      <p>A la secció <gui>Suspendre i Botó Energia</gui>, feu clic a <gui>Suspensió automàtica</gui>.</p>
    </item>
    <item>
      <p>Trieu <gui>Amb bateria</gui> o <gui>endollat</gui>, canvieu a <gui>ON</gui>, i seleccioneu un <gui>Retard</gui>. Es poden configurar les dues opcions.</p>

      <note style="tip">
        <p>En els ordinadors de sobretaula hi ha una opció etiquetada com a <gui>Quan està inactiu</gui>.</p>
      </note>
    </item>

  </steps>

</page>
