<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="session-screenlocks" xml:lang="sr">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="guide" xref="hardware-problems-graphics"/>

    <revision pkgversion="3.8" version="0.3" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-03" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.28" date="2018-07-19" status="review"/>
    <revision pkgversion="3.34" date="2019-11-12" status="review"/>

    <credit type="author">
      <name>Гномов пројекат документације</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Мајкл Хил</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Измените након ког времена ће се закључати екран у подешавањима <gui>Приватности</gui>.</desc>
  </info>

  <title>Екран се превише брзо закључава</title>

  <p>Ако оставите ваш рачунар на неколико минута, екран ће се сам закључати тако да ћете морати да унесете лозинку да бисте почели поново да га користите. Ово се ради из безбедносних разлога (тако да нико не може да поремети ваш рад ако оставите рачунар без надзора), али може бити замарајуће ако се екран пребрзо закључава.</p>

  <p>Да би се екран закључавао након дужег времена:</p>

  <steps>
    <item>
      <p>Отворите преглед <gui xref="shell-introduction#activities">Активности</gui> и почните да куцате <gui>Приватност</gui>.</p>
    </item>

    <item>
      <p>Кликните на <gui>Приватност</gui> да отворите панел.</p>
    </item>
    <item>
      <p>Притисните <gui>Закључај екран</gui>.</p>
    </item>
    <item>
      <p>Ако је укључено <gui>Сам закључај екран</gui>, можете да измените вредност у падајућем списку <gui>Закључај екран након мировања од</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <p>If you don’t ever want the screen to lock itself automatically, switch
    the <gui>Automatic Screen Lock</gui> switch to off.</p>
  </note>

</page>
