<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-display" xml:lang="it">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-display"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Control icon captions used in the file manager.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>Preferenze di visualizzazione del gestore di file</title>

<p>You can control how the file manager displays captions under icons. Click
the menu button in the top-right corner of the window and select
<gui>Preferences</gui>, then select the <gui>Views</gui> tab.</p>

<section id="icon-captions">
  <title>Didascalie delle icone</title>
  <!-- TODO: update screenshot for 3.18 and above. -->
  <media type="image" src="figures/nautilus-icons.png" width="250" height="110" style="floatend floatright">
    <p>Icone del gestore di file con didascalie</p>
  </media>
  <p>Quando viene utilizzata la visualizzazione per icone, è possibile informazioni rispetto a file e cartelle in una didascalia che apparirà sotto ciascuna icona. Questo è utile, per esempio, se spesso si ha la necessità di sapere a chi appartiene un file o quando è stato modificato l'ultima volta.</p>
  <p>You can zoom in a folder by clicking the view options button in the
  <!-- FIXME: Get a tooltip added for "View options" -->
  toolbar and choosing a zoom level with the slider. As you zoom in, the
  file manager will display more and more information in captions. You can
  choose up to three things to show in captions. The first will be displayed at
  most zoom levels. The last will only be shown at very large sizes.</p>
  <p>Le informazioni che è possibile mostrare nelle didascalie delle icone sono le stesse delle colonne presenti nella visualizzazione per liste. Per maggiori informazioni, consultare <link xref="nautilus-list"/>.</p>
</section>

<section id="list-view">

  <title>List View</title>

  <p>When viewing files as a list, you can <gui>Allow folders to be
  expanded</gui>. This shows expanders on each directory in the file list, so
  that the contents of several folders can be shown at once. This is useful if
  the folder structure is relevant, such as if your music files are organized
  with a folder per artist, and a subfolder per album.</p>

</section>

</page>
