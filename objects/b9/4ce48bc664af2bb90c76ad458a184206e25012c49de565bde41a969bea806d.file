<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-closelid" xml:lang="gl">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspendfail"/>
    <link type="seealso" xref="power-suspend"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.10" date="2013-11-08" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.26" date="2017-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <credit type="author">
      <name>Proxecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Os portátiles suspéndense cando se pecha a tapa para aforrar enerxía.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2020</mal:years>
    </mal:credit>
  </info>

  <title>Porque se apaga o meu computador cando pecha a tapa?</title>

  <p>When you close the lid of your laptop, your computer will
  <link xref="power-suspend"><em>suspend</em></link> in order to save power.
  This means that the computer is not actually turned off — it has just gone to
  sleep. You can resume it by opening the lid. If it does not resume, try
  clicking the mouse or pressing a key. If that still does not work, press the
  power button.</p>

  <p>Algúns portátiles non so capaces de suspender correctamente, normalmente porque o seu hardware non é completamente compatíbel co sistema operativo (por exemplo, os controladores de Linux están incompletos). Neste caso, pode atopar que non é capaz de espertar o seu portátil despois de pechar a tapa. Pode probar a <link xref="power-suspendfail">arranxar o problema coa suspension</link>, ou previr que o computador tente suspenderse cando peche a tapa.</p>

<section id="nosuspend">
  <title>Parar a suspensión do equipo cando se pecha a tapa</title>

  <note style="important">
    <p>These instructions will only work if you are using <app>systemd</app>.
    Contact your distribution for more information.</p>
  </note>

  <note style="important">
    <p>You need to have <app>Tweaks</app> installed on your computer to
    change this setting.</p>
    <if:if xmlns:if="http://projectmallard.org/if/1.0/" test="action:install">
      <p><link style="button" action="install:gnome-tweaks">Instalar <app>Axustes</app></link></p>
    </if:if>
  </note>

  <p>Se non quere que o seu computador se suspenda cando pecha a tapa, pode cambiar a preferencia para ese comportamento.</p>

  <note style="warning">
    <p>Teña moito coidado se cambia esta configuración. Algúns portátiles poden sobrequentarse se están coa tapa pechada, especialmente se están nun lugar pechado como nunha mochila.</p>
  </note>

  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Actividades</gui> e comece a escribir <gui>Axustes</gui>.</p>
    </item>
    <item>
      <p>Prema <gui>Axustes</gui> para abrir a aplicación.</p>
    </item>
    <item>
      <p>Seleccione a lapela <gui>Xeral</gui>.</p>
    </item>
    <item>
      <p>Cambie <gui>Suspender cando a tapa do portátil se peche</gui> a desactivado.</p>
    </item>
    <item>
      <p>Seleccione na xanela <gui>Retoques</gui>.</p>
    </item>
  </steps>

</section>

</page>
