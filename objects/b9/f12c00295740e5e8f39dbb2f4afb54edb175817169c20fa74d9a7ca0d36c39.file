<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-slowkeys" xml:lang="gu">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>ફીલ બુલ</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>માઇકલ હીલ</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>ઍકાટેરીના ગેરાસીમોવા</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>કી દબાવવા વચ્ચે વિલંબ રાખો કે અક્ષર એ સ્ક્રીન પર દેખાય છે.</desc>
  </info>

  <title>ધીમી કી ચાલુ કરો</title>

  <p><em>સ્લો કી</em> ને ચાલુ કરો જો તમને કી દબાવવા વચ્ચેનાં વિલંબ માટે ત્યાં જવુ હોય કે જે અક્ષર સ્ક્રીન પર દર્શાવી દેવામાં આવ્યો છે. આનો મતલબ એ થાય કે તમારે દરેક કીને પકડવી જ પડશે તમે થોડાક માટે ટાઇપ કરવા માંગો છો તે દેખાય તે પહેલાં. સ્લો કીને વાપરો જો તમે આકસ્મિક રીતે એક જ સમયે બહુ બધી કીને દબાવી દીધી હોય જ્યારે તમે ટાઇપ કરો, અથવા જો તમને પહેલી વખત કિબોર્ડ પર જમણી કી ને દબાવવા મુશ્કેલી અનુભવતા હોય.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Universal Access</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Press <gui>Typing Assist (AccessX)</gui> in the <gui>Typing</gui>
      section.</p>
    </item>
    <item>
      <p>Switch the <gui>Slow Keys</gui> switch to on.</p>
    </item>
  </steps>

  <note style="tip">
    <title>ઝડપથી સ્લો કીને ચાલુ અને બંધ કરો</title>
    <p><gui>કિબોર્ડ દ્દારા સક્રિય કરો</gui> હેઠળ, કિબોર્ડમાંથી સ્લો કીને ચાલુ અને બંધ કરવા માટે <gui>કિબોર્ડમાંથી સુલભતા લક્ષણોને ચાલુ કરો</gui> ને પસંદ કરો. જ્યારે આ વિકલ્પ પસંદ થયેલ હોય તો, તમે સ્લો કીને સક્રિય અથવા નિષ્ક્રિય કરવા માટે આઠ સેકંડ માટે <key>Shift</key> ને દબાવી અને પકડી રાખી શકો છો.</p>
    <p>ટોચની પટ્ટી પર <link xref="a11y-icon">સુલભતા ચિહ્ન</link> ને ક્લિક કરવા દ્દારા તમે સ્લો કીને ચાલુ અને બંધ કરી શકો છો અને <gui>સ્લો કી</gui> ને પસંદ કરી રહ્યા છે. સુલભતા ચિહ્ન એ દૃશ્યમાન છે જ્યારે એક અથવા વધારે સુયોજનો <gui>સાર્વત્રિક વપરાશ</gui> પેનલમાંથી સક્રિય કરી દેવામાં આવ્યુ છે.</p>
  </note>

  <p>રજીસ્ટર કરવા માટે નીચે કીને પકડવા માટે તમને કેટલો સમય લાગશે તે નિયંત્રિત કરવા માટે <gui>સ્વીકૃતિ વિલંબ</gui> ને વાપરો.</p>

  <p>You can have your computer make a sound when you press a key, when a key
  press is accepted, or when a key press is rejected because you didn’t hold
  the key down long enough.</p>

</page>
