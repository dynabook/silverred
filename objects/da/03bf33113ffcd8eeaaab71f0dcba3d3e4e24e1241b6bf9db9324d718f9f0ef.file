<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-language" xml:lang="vi">

  <info>
    <link type="guide" xref="prefs-language"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Dự án tài liệu GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Chuyển ngôn ngữ của giao diện hiển thị và trợ giúp.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nguyễn Thái Ngọc Duy</mal:name>
      <mal:email>pclouds@gmail.com</mal:email>
      <mal:years>2011-2012.</mal:years>
    </mal:credit>
  </info>

  <title>Đổi ngôn ngữ bạn dùng</title>

  <p>Bạn có thể dùng màn hình làm việc và ứng dụng với hàng chục ngôn ngữ khác nhau, với điều kiện bạn đã cài các gói ngôn ngữ cần thiết.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Region &amp; Language</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Region &amp; Language</gui> to open the panel.</p>
    </item>
    <item>
      <p>Click <gui>Language</gui>.</p>
    </item>
    <item>
      <p>Select your desired region and language. If your region and language
      are not listed, click
      <gui><media its:translate="no" type="image" mime="image/svg" src="figures/view-more-symbolic.svg"><span its:translate="yes">…</span></media></gui>
      at the bottom of the list to select from all available regions and
      languages.</p>
    </item>
    <item>
      <p>Click <gui style="button">Done</gui> to save.</p>
    </item>
    <item>
      <p>Respond to the prompt, <gui>Your session needs to be restarted for
      changes to take effect</gui> by clicking
      <gui style="button">Restart Now</gui>, or click
      <gui style="button">×</gui> to restart later.</p>
    </item>
  </steps>

  <p>Some translations may be incomplete, and certain applications may not
  support your language at all. Any untranslated text will appear in the
  language in which the software was originally developed, usually American
  English.</p>

  <p>Có một số thư mục đặc biệt trong thư mục riêng của bạn mà ứng dụng dùng để lưu những thứ như nhạc, hình ảnh, tài liệu... Những thư mục này dùng tên chuẩn theo ngôn ngữ của bạn. Khi bạn đăng nhập lại, bạn sẽ được hỏi bạn có muốn đổi tên thư mục sang tên chuẩn của ngôn ngữ bạn chọn hay không. Nếu bạn định dùng ngôn ngữ này lâu dài, bạn nên cập nhật tên thư mục.</p>

  <note style="tip">
    <p>If there are multiple user accounts on your system, there is a separate
    instance of the <gui>Region &amp; Language</gui> panel for the login screen.
    Click the <gui>Login Screen</gui> button at the top right to toggle between
    the two instances.</p>
  </note>

</page>
