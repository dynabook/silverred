<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="bluetooth-turn-on-off" xml:lang="ca">

  <info>
    <link type="guide" xref="bluetooth" group="#first"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-04" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-21" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Activar o desactivar el dispositiu Bluetooth de l'ordinador.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

<title>Activar o desactivar el Bluetooth</title>

  <p>Podeu activar Bluetooth per connectar-vos a altres dispositius Bluetooth o desactivar-lo per estalviar bateria. Per a activar Bluetooth:</p>

  <steps>
    <item>
      <p>Obriu la vista d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a escriure <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Feu clic a <gui>Bluetooth</gui> per obrir el quadre.</p>
    </item>
    <item>
      <p>Establiu l'interruptor a la part superior a on.</p>
    </item>
  </steps>

  <p>Molts portàtils tenen un commutador de maquinari o combinació de tecles per a activar i desactivar Bluetooth. Busqueu un interruptor a l'ordinador o una tecla del vostre teclat. Sovint s'hi pot accedir des del teclat amb l'ajuda de la tecla <key>Fn</key>.</p>

  <p>Per desactivar el Bluetooth:</p>
  <steps>
    <item>
      <p>Obriu el <gui xref="shell-introduction#yourname">menú de sistema</gui> des de la part dreta a la barra superior.</p>
    </item>
    <item>
      <p>Seleccioneu <gui><media its:translate="no" type="image" mime="image/svg" src="figures/bluetooth-active-symbolic.svg"/> Sense usar</gui>. La secció Bluetooth del menú s'expandirà.</p>
    </item>
    <item>
      <p>Seleccioneu <gui>Apaga</gui>.</p>
    </item>
  </steps>

  <note><p>El vostre ordinador estarà <link xref="bluetooth-visibility">visible</link> tant de temps com el quadre <gui>Bluetooth</gui> estigui obert.</p></note>

</page>
