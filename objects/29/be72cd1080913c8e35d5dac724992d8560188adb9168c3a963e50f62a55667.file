<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="color-why-calibrate" xml:lang="es">

  <info>
    <link type="guide" xref="color#calibration"/>
    <desc>La calibración es importante si se preocupa por los colores que muestra o imprime.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>¿Por qué necesito realizar la calibración yo mismo?</title>

  <p>Generalmente los perfiles genéricos son malos. Cuando un fabricante crea un modelo nuevo, simplemente toman algunos elementos de la línea de producción y miden su media:</p>

  <media its:translate="no" type="image" src="figures/color-average.png">
    <p its:translate="yes">Perfiles medios</p>
  </media>

  <p>Las pantallas difieren bastante de una unidad a otra y pueden cambiar sustancialmente a lo largo del tiempo. También es más difícil para las impresoras, ya que el simple cambio de tipo o peso del papel pueden cambiar el estado de caracterización y hacer que el perfil no sea preciso.</p>

  <p>La mejor forma de asegurarse de que el perfil que tiene es preciso es realizando la calibración usted mismo, o permitiendo que una empresa externa le proporcione un perfil basado en su estado de caracterización exacto.</p>

</page>
