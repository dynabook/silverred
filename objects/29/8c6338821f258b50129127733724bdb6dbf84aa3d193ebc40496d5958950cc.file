<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-autobrightness" xml:lang="cs">

  <info>
    <link type="guide" xref="power#saving"/>
    <link type="seealso" xref="display-brightness"/>

    <revision pkgversion="3.20" date="2016-06-15" status="candidate"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2016</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Automatické řízení jasu obrazovky může snížit spotřebu z baterie.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Povolení automatického jasu</title>

  <p>V případě, že má váš počítač zabudované světelné čidlo, může jej používat ke automatické regulaci jasu obrazovky. Díky tomu je pak obrazovka vždy dobře čitelná za různých světelných podmínek, ale zároveň se šetří spotřeba baterie.</p>

  <steps>

    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Napájení</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Napájení</gui> otevřete příslušný panel.</p>
    </item>
    <item>
      <p>V části <gui>Šetření energií</gui> zkontrolujte, jestli je vypínč <gui>Automatický jas</gui> přepnutý do polohy zapnuto.</p>
    </item>

  </steps>

  <p>Pokud chcete automatický jas obrazovky zakázat, přepněte jej do polohy vypnuto.</p>

</page>
