<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-resize" xml:lang="sr">
  <info>
    <link type="guide" xref="disk"/>


    <credit type="author">
      <name>Гномов пројекат документације</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <revision pkgversion="3.25.90" date="2017-08-17" status="review"/>

    <desc>Смањите или повећајте систем датотека и његову партицију.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Подесите величину система датотека</title>

  <p>Систем датотека се може повећати како би се искористио слободни простор након његове партиције. Често ово је могуће чак и када је систем датотека прикачен.</p>
  <p>Да направите простор за другу партицију након система датотека, може се смањити према слободном простору унутар њега.</p>
  <p>Немају сви системи датотека подршку промене величчине.</p>
  <p>Величина партиције ће се променити заједно са величином система датотека. Такође је могуће променити величину партиције без система датотека на исти начин.</p>

<steps>
  <title>Промените величину система датотека/партиције</title>
  <item>
    <p>Отворите програм <app>Дискови</app> из прегледа <gui>Активности</gui>.</p>
  </item>
  <item>
    <p>Изаберите диск који желите да проверите са списка складишних уређаја на левој страни. Ако има више од једног волумена на диску, изаберите онај који садржи систем датотека.</p>
  </item>
  <item>
    <p>На траци алата испод одељка <gui>Волумени</gui>, кликните на дугме изборника. Након тога кликните <gui>Промени величину система датотека…</gui> или <gui>Промени величину…</gui> ако не постоји систем датотека.</p>
  </item>
  <item>
    <p>Отвориће се прозорче у коме се може изабрати нова величина. Систем датотека биће прикачен да би се израчунала најмања величина према количини тренутног садржаја. Ако смањивање није подржано најмања величина је тренутна величина. Оставите довољно простора унутар система датотека приликом смањивања да осигурате да може радити брзо и поуздано.</p>
    <p>У зависности од тога колико података мора бити премештено из смањеног дела, промена величине система датотека може потрајати дуже.</p>
    <note style="warning">
      <p>Промена величине система датотека аутоматски укључује <link xref="disk-repair">поправку</link> система датотека. Због тога се саветује да направите резервни примерак важних података пре него што почнете. Радња се не сме зауставити јер ће резултирати оштећеним системом датотека.</p>
    </note>
  </item>
  <item>
      <p>Потврдите почетак радње кликом на <gui style="button">Промени величину</gui>.</p>
   <p>Радња ће откачити систем датотека ако промена величине прикаченог система датотека није подржана. Будите стрпљиви док се мења величина система датотека.</p>
  </item>
  <item>
    <p>Након завршетка потребних радњи промене величине и поправаке, систем датотека је спреман за поновно коришћење.</p>
  </item>
</steps>

</page>
