<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="solaris-mode" xml:lang="hu">
  <info>
    <revision version="0.2" pkgversion="3.11" date="2014-01-26" status="review"/>
    <link type="guide" xref="index#other" group="other"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author copyright">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
      <years>2011</years>
    </credit>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2011, 2014</years>
    </credit>

    <desc>A Solaris mód használata a CPU-k számának.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  </info>

  <title>Mi az a Solaris mód?</title>

  <p>Egy több processzorral vagy <link xref="cpu-multicore">maggal</link> rendelkező rendszeren a folyamatok egyszerre több magot is használhatnak. Előfordulhat, hogy a <gui>% CPU</gui> oszlop olyan értékeket jelenít meg, amelyek összesen 100%-nál nagyobbak (például egy 4 magos rendszeren 400%). A <gui>Solaris mód</gui> minden folyamat esetén elosztja a <gui>% CPU</gui> értéket a rendszer CPU-inak számával, így az összeg 100% lesz.</p>

  <p>A <gui>% CPU</gui> megjelenítéséhez <gui>Solaris módban</gui>:</p>

  <steps>
    <item><p>Kattintson az alkalmazásmenü <gui>Beállítások</gui> menüpontjára.</p></item>
    <item><p>Kattintson a <gui>Folyamatok</gui> lapra.</p></item>
    <item><p>Jelölje be a <gui>CPU használat osztása a CPU-k számával</gui> jelölőnégyzetet.</p></item>
  </steps>

    <note><p>A <gui>Solaris mód</gui> kifejezés a Sun cég UNIX operációs rendszeréből ered, szemben a Linux alapértelmezett IRIX módjával, amelyet az SGI cég UNIX rendszeréről neveztek el.</p></note>

</page>
