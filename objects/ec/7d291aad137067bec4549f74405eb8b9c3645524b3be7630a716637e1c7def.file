<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-browse-web" version="1.0 if/1.0" xml:lang="sr">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Јакуб Штајнер (Jakub Steiner)</name>
    </credit>
    <credit type="author">
      <name>Петр Ковар (Petr Kovar)</name>
    </credit>
    <link type="guide" xref="getting-started" group="tasks"/>
    <title role="trail" type="link">Прегледање интернета</title>
    <link type="seealso" xref="net-browser"/>
    <title role="seealso" type="link">Упутство за прегледање интернета</title>
    <link type="next" xref="gs-connect-online-accounts"/>
  </info>

  <title>Прегледање интернета</title>

<if:choose>
<if:when test="platform:gnome-classic">

    <media its:translate="no" type="image" mime="image/svg" src="gs-web-browser1-firefox-classic.svg" width="100%"/>

    <steps>
      <item><p>Кликните на изборник <gui>Програми</gui> у горњем левом делу екрана.</p></item>
      <item><p>Из изборника изаберите <guiseq><gui>Интернет</gui><gui>Firefox</gui> </guiseq>.</p></item>
    </steps>

</if:when>

<!--Distributors might want to add their distro here if they ship Firefox by default.-->
<if:when test="platform:centos, platform:debian, platform:fedora, platform:rhel, platform:ubuntu, platform:opensuse, platform:sled, platform:sles">

    <media its:translate="no" type="image" mime="image/svg" src="gs-web-browser1-firefox.svg" width="100%"/>

    <steps>
      <item><p>Померите ваш показивач миша у горњи леви угао екрана где пише <gui>Активности</gui> да би вам се приказао <gui>преглед активности</gui>.</p></item>
      <item><p>Изаберите <app>Firefox</app> иконицу прегледача са траке на левој страни екрана.</p></item>
    </steps>

    <note><p>Постоји и други начин за покретање прегледача, <link xref="gs-use-system-search">куцањем речи</link> <em>Firefox</em> у <gui>прегледу активности</gui>.</p></note>

</if:when>
<if:else>

    <media its:translate="no" type="image" mime="image/svg" src="gs-web-browser1.svg" width="100%"/>

    <steps>
      <item><p>Померите ваш показивач миша у горњи леви угао екрана где пише <gui>Активности</gui> да би вам се приказао <gui>преглед активности</gui>.</p></item>
      <item><p>Изаберите иконицу прегледача <app>Веб</app> у траци на левој страни екрана.</p></item>
    </steps>

    <note><p>Постоји и други начин да покренете прегледач, тако што ћете у <gui>прегледу активности </gui> само <link xref="gs-use-system-search">укуцати</link> <em>веб</em>.</p></note>

    <media its:translate="no" type="image" mime="image/svg" src="gs-web-browser2.svg" width="100%"/>

</if:else>
</if:choose>

<!--Distributors might want to add their distro here if they ship Firefox by default.-->
<if:if test="platform:centos, platform:debian, platform:fedora,platform:rhel, platform:ubuntu, platform:opensuse, platform:sled, platform:sles">

    <media its:translate="no" type="image" mime="image/svg" src="gs-web-browser2-firefox.svg" width="100%"/>

</if:if>

    <steps style="continues">
      <item><p>Кликните на траку са адресом у врху прозора прегледача и почните са куцањем веб странице коју желите да погледате.</p></item>
      <item><p>Куцањем веб странице се започиње претраживање историје прегледања и обележивача, тако да није потребно запамтити тачну адресу.</p>
        <p>Ако Гном пронађе веб страницу у историјату или међу обележивачима, падајући списак ће се приказати испод траке са адресом.</p></item>
      <item><p>Можете брзо изабрати веб страницу из падајућег списка користећи стрелице на тастатури.</p>
      </item>
      <item><p>Након што изаберете веб страницу, притисните тастер <key>Enter</key> да бисте је отворили.</p>
      </item>
    </steps>

</page>
