<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:ui="http://projectmallard.org/ui/1.0/" type="guide" style="task" id="files-search" xml:lang="sv">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-25" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.34" date="2019-07-20" status="draft"/>

    <credit type="author">
      <name>Dokumentationsprojekt för GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>
    <credit type="editor">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Hitta filer baserat på filnamn och typ.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Sök efter filer</title>

  <p>Du kan söka efter filer baserat på deras namn eller filtyp direkt i filhanteraren.</p>

  <links type="topic" style="linklist">
    <title>Andra sökprogram</title>
    <!-- This is an extension point where search apps can add
    their own topics. It's empty by default. -->
  </links>

  <steps>
    <title>Sök</title>
    <item>
      <p>Öppna programmet <app>Filer</app> från översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui>.</p>
    </item>
    <item>
      <p>Om du vet att filerna du letar efter finns i en viss mapp, gå till den mappen.</p>
    </item>
    <item>
      <p>Skriv ett eller flera ord som du vet förekommer i filnamnet så kommer de att visas i sökraden. Om du till exempel ger namn på alla dina fakturor med ordet ”Faktura”, skriv <input>faktura</input>. Ord matchar oberoende av skiftläge.</p>
      <note>
        <p>Istället för att skriva ord direkt för att visa sökraden, kan du klicka på <media its:translate="no" type="image" mime="image/svg" src="figures/edit-find-symbolic.svg"> <key>Search</key> key symbol
        </media> i verktygsfältet eller trycka <keyseq><key>Ctrl</key><key>F</key></keyseq>.</p>
      </note>
    </item>
    <item>
      <p>Du kan begränsa dina resultat efter datum, efter filtyp och efter huruvida filens fullständiga text ska genomsökas, eller endast filnamnen ska genomsökas.</p>
      <p>För att tillämpa filter, välj rullgardinsmenyknappen till vänster om filhanterarens <media its:translate="no" type="image" mime="image/svg" src="figures/edit-find-symbolic.svg"> <key>Search</key> key symbol</media>-ikon, och välj bland de tillgängliga filtren:</p>
      <list>
        <item>
          <p><gui>När</gui>: Hur långt tillbaka i tiden vill du söka?</p>
        </item>
        <item>
          <p><gui>Vad</gui>: Vad är detta för typ av objekt?</p>
        </item>
        <item>
          <p>Ska din sökning inkludera en fulltextsökning eller bara söka genom filnamnen?</p>
        </item>
      </list>
    </item>
    <item>
      <p>För att ta bort ett filter, välj <gui>X</gui>:et bredvid filtertaggen som du vill ta bort.</p>
    </item>
    <item>
      <p>Du kan öppna, kopiera, ta bort eller på annat sätt arbeta med dina filer från sökresultatet, precis som du skulle ha gjort från vilken mapp som helst i filhanteraren.</p>
    </item>
    <item>
      <p>Klicka på <media its:translate="no" type="image" mime="image/svg" src="figures/edit-find-symbolic.svg"> <key>Search</key> key symbol
      </media> i verktygsfältet igen för att avsluta sökningen och återgå till mappen.</p>
    </item>
  </steps>

<section id="customize-files-search">
  <title>Anpassa filsökningar</title>

<p>Eventuellt vill du att vissa kataloger ska inkluderas eller exkluderas från sökningar i programmet <app>Filer</app>. För att anpassa vilka kataloger som genomsöks:</p>

  <steps>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Sök</gui>.</p>
    </item>
    <item>
      <p>Välj <guiseq><gui>Inställningar</gui><gui>Sök</gui></guiseq> från resultaten. Detta kommer att öppna panelen <gui>Sökinställningar</gui>.</p>
    </item>
    <item>
      <p>Välj <media its:translate="no" type="image" mime="image/svg" src="figures/emblem-system-symbolic.svg"> <key>Preferences</key> key
      symbol</media>-ikonen längst ner i inställningspanelen <gui>Sök</gui>.</p>
    </item>
  </steps>

<p>Detta kommer att öppna en separat inställningspanel som låter dig växla huruvida katalogsökningar är på eller av. Du kan växla sökningar för vardera av de tre flikarna:</p>

  <list>
    <item>
      <p><gui>Platser</gui>: Listar vanliga hemkatalogplatser</p>
    </item>
    <item>
      <p><gui>Bokmärken</gui>: Listar katalogplatser som du har bokmärkt i programmet <app>Filer</app></p>
    </item>
    <item>
      <p><gui>Andra</gui>: Listar katalogplatser som du inkluderar via <gui>+</gui>-knappen.</p>
    </item>
  </list>

</section>

</page>
