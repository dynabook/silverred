<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="files-preview" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>
    <link type="seealso" xref="nautilus-preview"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-10-06" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Mostra e esconde rapidamente as visualizações de documentos, imagens, vídeos e mais.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

<title>Visualizando arquivos e pastas</title>

<note if:test="platform:ubuntu" style="important">
  <p>Você precisa ter <app>Sushi</app> instalado em seu computador para realizar essas etapas.</p>
  <p its:locNote="Translators: This button is only shown on Ubuntu, where this   link format is preferred over 'install:gnome-sushi'."><link style="button" href="apt:gnome-sushi">Instalar <app>Sushi</app></link></p>
</note>

<p>Você pode pré-visualizar rapidamente arquivos sem abri-los em um aplicativo maduro. Selecione qualquer arquivo e pressione a barra de espaço. O arquivo vai abrir em uma janela simples de pré-visualização. Pressione a barra de espaço novamente para fechar a pré-visualização.</p>

<p>A pré-visualização embutida suporta a maioria dos formatos de arquivos de documentos, imagens, vídeos e áudio. Na pré-visualização, você pode navegador nos documentos ou buscar no vídeo e áudio.</p>

<p>Para uma pré-visualização em tela cheia, pressione <key>F</key> ou <key>F11</key>. Pressione <key>F</key> ou <key>F11</key> novamente para sair da tela cheia ou pressione barra de espaço para sair por completo da pré-visualização.</p>

</page>
