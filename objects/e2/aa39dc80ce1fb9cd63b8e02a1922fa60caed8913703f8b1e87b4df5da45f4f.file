<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:itst="http://itstool.org/extensions/" type="topic" style="task" id="net-proxy" xml:lang="hu">

  <info>
    <its:rules xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Baptiste Mille-Mathias</name>
      <email>baptistem@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>A proxy a webes forgalom közvetítője, és webes szolgáltatások – felügyeleti vagy biztonsági okokból – névtelen elérésére használható.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Proxy beállításainak megadása</title>

<section id="what">
  <title>Mi az a proxy?</title>

  <p>A <em>webproxy</em> szűri az Ön által megtekintett weboldalakat, megkapja a webböngésző által küldött, a weboldalak és azok elemeinek letöltésére vonatkozó kéréseket, és eldöntik hogy Ön elérheti-e azokat. A proxykat gyakran használják cégeknél és nyilvános vezeték nélküli hotspotokon a megtekinthető weboldalak felügyeletéhez. Céljuk, hogy megakadályozzák Önt az internet bejelentkezés nélküli használatában, vagy a weboldalakon biztonsági ellenőrzések végzése.</p>

</section>

<section id="change">
  <title>Proxy módszer megváltoztatása</title>

  <steps>
    <item>
      <p>Nyissa meg a <gui xref="shell-introduction#activities">Tevékenységek</gui> áttekintést, és kezdje el begépelni a <gui>Hálózat</gui> szót.</p>
    </item>
    <item>
      <p>Kattintson a <gui>Hálózat</gui> elemre a panel megnyitásához.</p>
    </item>
    <item>
      <p>Válassza a bal oldali lista <gui>Hálózati proxy</gui> pontját.</p>
    </item>
    <item>
      <p>Válassza ki a használni kívánt proxy módszert:</p>
      <terms>
        <item>
          <title><gui itst:context="proxy">Nincs</gui></title>
          <p>Az alkalmazások közvetlen kapcsolatot használnak a webes tartalmak elérésére.</p>
        </item>
        <item>
          <title><gui>Kézi</gui></title>
          <p>Minden proxyzott protokoll esetén adja meg egy proxy címét és portját. A protokollok: <gui>HTTP</gui>, <gui>HTTPS</gui>, <gui>FTP</gui> és <gui>SOCKS</gui>.</p>
        </item>
        <item>
          <title><gui>Automatikus</gui></title>
          <p>Egy olyan erőforrásra mutató URL, amely tartalmazza a rendszere számára megfelelő beállításokat.</p>
        </item>
      </terms>
    </item>
  </steps>

  <p>A hálózati kapcsolatot használó alkalmazások a megadott proxybeállításokat fogják használni.</p>

</section>

</page>
