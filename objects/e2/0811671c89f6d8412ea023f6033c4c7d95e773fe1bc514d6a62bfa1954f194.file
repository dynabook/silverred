<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-batterywindows" xml:lang="es">

  <info>
    <link type="guide" xref="power#faq"/>
    <link type="seealso" xref="power-batteryestimate"/>
    <link type="seealso" xref="power-batterylife"/>
    <link type="seealso" xref="power-batteryslow"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>

    <desc>Los ajustes de los fabricantes y diferentes estimaciones de duración de la batería pueden ser la causa de este problema.</desc>
    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>¿Por qué tengo menor tiempo de batería que el que tenía en Windows/Mac OS?</title>

<p>Algunos equipos parecen tener una duración menor de la batería funcionando bajo GNU/Linux que la que tienen cuando funcionan con Windows o Mac OS. Una razón es que los fabricantes instalan algún software especial para Windows/Mac OS que optimiza varias configuraciones de hardware/software para un modelo de equipo en especial. Éstos ajustes son generalmente muy específicos y pueden no estar documentados, por eso incluirlos en GNU/Linux es muy difícil.</p>

<p>Por desgracia, no existe una forma fácil de aplicar estos ajustes uno mismo sin saber exactamente cuáles son. No obstante, es posible que usar algunos <link xref="power-batterylife">métodos de ahorro de energía</link> ayude. Si su equipo dispone de un <link xref="power-batteryslow">procesador de velocidad variable</link>, también puede resultarle útil cambiar su configuración.</p>

<p>Otra razón posible es que el método de estimar el tiempo de batería es diferente en Windows/Mac OS que en Linux. La duración real de la batería puede ser la misma, pero los métodos distintos ofrecen estimaciones distintas.</p>
	
</page>
