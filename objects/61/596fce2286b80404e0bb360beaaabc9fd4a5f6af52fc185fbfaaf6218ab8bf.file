<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-touchpad-click" xml:lang="nl">

  <info>
    <link type="guide" xref="mouse"/>

    <revision pkgversion="3.7" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-29" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013, 2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Klik, sleep, of scroll met behulp van tikken en gebaren op uw touchpad.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Klik, sleep, of scroll met het touchpad</title>

  <p>U kunt klikken, dubbelklikken, slepen en scrollen met alleen uw touchpad, zonder fysieke knoppen.</p>

  <note>
    <p><link xref="touchscreen-gestures">Touchscreen gestures</link> are
    covered separately.</p>
  </note>

<section id="tap">
  <title>Tikken om te klikken</title>

  <p>U kunt op uw touchpad tikken om te klikken in plaats van via een knop.</p>

  <steps>
    <item>
      <p>Open het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht en typ <gui>Muis &amp; Touchpad</gui>.</p>
    </item>
    <item>
      <p>Klik op <gui>Muis en touchpad</gui> om het paneel te openen.</p>
    </item>
    <item>
      <p>In the <gui>Touchpad</gui> section, make sure the <gui>Touchpad</gui>
      switch is set to on.</p>
      <note>
        <p>De <gui>Touchpad</gui>-sectie verschijnt alleen als uw systeem een touchpad heeft.</p>
      </note>
    </item>
   <item>
      <p>Switch the <gui>Tap to click</gui> switch to on.</p>
    </item>
  </steps>

  <list>
    <item>
      <p>Tik op het touchpad om te klikken.</p>
    </item>
    <item>
      <p>Tik twee maal om te dubbelklikken.</p>
    </item>
    <item>
      <p>Om een item te slepen, tik twee maal, maar laat uw vinger niet los na de tweede tik. Sleep het item naar de gewenste plek en laat uw vinger los om het neer te zetten.</p>
    </item>
    <item>
      <p>Als uw touchpad tikken met meerdere vingers ondersteunt, dan kunt u rechtsklikken door met twee vingers tegelijk te tikken. Zo niet, dan moet u nog steeds fysieke knoppen gebruiken om met rechts te klikken. Zie <link xref="a11y-right-click"/> voor een manier om met rechts te klikken zonder een tweede muisknop.</p>
    </item>
    <item>
      <p>Als uw touchpad tikken met meerdere vingers ondersteunt, dan kunt u <link xref="mouse-middleclick">middelklikken</link> door met drie vingers tegelijk te tikken.</p>
    </item>
  </list>

  <note>
    <p>Wanneer u tikt of sleept met meerdere vingers, zorg er dan voor dat uw vingers ver genoeg uit elkaar gespreid zijn. Als uw vingers te dicht tegen elkaar aan zitten, dan kan het zijn dat de computer dit ziet als één vinger.</p>
  </note>

</section>

<section id="twofingerscroll">
  <title>Scrollen met twee vingers</title>

  <p>U kunt met uw touchpad scrollen met twee vingers.</p>

  <steps>
    <item>
      <p>Open het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht en typ <gui>Muis &amp; Touchpad</gui>.</p>
    </item>
    <item>
      <p>Klik op <gui>Muis en touchpad</gui> om het paneel te openen.</p>
    </item>
    <item>
      <p>In the <gui>Touchpad</gui> section, make sure the <gui>Touchpad</gui>
      switch is set to on.</p>
    </item>
    <item>
      <p>Switch the <gui>Two-finger Scrolling</gui> switch to on.</p>
    </item>
  </steps>

  <p>Wanneer dit geselecteerd is, zal tikken en slepen met één vinger normaal werken, maar door met twee vingers over een deel van het touchpad te slepen kunt u scrollen. Wanneer u ook <gui>Horizontaal scrollen inschakelen</gui> selecteert, dan kunt u met uw vingers naar links en rechts gaan om horizontaal te scrollen. Zorg er wel voor dat uw vingers licht gespreid zijn. Wanneer uw vingers te dicht bij elkaar zijn ziet uw touchpad dat als één vinger.</p>

  <note>
    <p>Het scrollen met twee vingers werkt mogelijk niet op alle touchpads.</p>
  </note>

</section>

<section id="contentsticks">
  <title>Natuurlijk scrollen</title>

  <p>Via het touchpad kunt u inhoud slepen alsof er fysiek een stuk papier wordt versleept.</p>

  <steps>
    <item>
      <p>Open het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht en typ <gui>Muis &amp; Touchpad</gui>.</p>
    </item>
    <item>
      <p>Klik op <gui>Muis en touchpad</gui> om het paneel te openen.</p>
    </item>
    <item>
      <p>In the <gui>Touchpad</gui> section, make sure that the
     <gui>Touchpad</gui> switch is set to on.</p>
    </item>
    <item>
      <p>Switch the <gui>Natural Scrolling</gui> switch to on.</p>
    </item>
  </steps>

  <note>
    <p>Deze functie staat ook bekend als <em>Achteruit Scrollen</em>.</p>
  </note>

</section>

</page>
