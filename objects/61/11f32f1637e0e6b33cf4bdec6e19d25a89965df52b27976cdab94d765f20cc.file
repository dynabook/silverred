<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-changepicture" xml:lang="lv">

  <info>
    <link type="guide" xref="user-accounts#manage"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.14.0" date="2014-10-08" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.32.0" date="2019-03-16" status="final"/>

    <credit type="author">
      <name>GNOME dokumentācijas projekts</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Pievienot savu fotogrāfiju ierakstīšanās un lietotāju ekrāniem.</desc>
  </info>

  <title>Nomainīt ierakstīšanās ekrāna attēlu</title>

  <p>Ierakstoties vai pārslēdzot lietotājus, redzēsiet lietotāju sarakstu ar attēliem. Varat nomainīt savējo ar kādu no standarta vai personīgajiem attēliem, vai arī uzņemt jaunu attēlu ar tīmekļa kameru.</p>

  <p>Jums ir jābūt <link xref="user-admin-explain">administratora tiesībām</link>, lai rediģētu ne savējos kontus.</p>

  <steps>
    <item>
      <p>Atveriet <gui xref="shell-introduction#activities">Aktivitāšu</gui> pārskatu un sāciet rakstīt <gui>Lietotāji</gui>.</p>
    </item>
    <item>
      <p>Spiediet <gui>Lietotāji</gui>, lai atvērtu paneli.</p>
    </item>
    <item>
      <p>Ja vēlaties rediģēt kādu citu lietotāju, spiediet pogu <gui style="button">Atslēgt</gui> un ievadiet savu  paroli.</p>
    </item>
    <item>
      <p>Nospiediet attēlu līdzās savam vārdam. Atvērsies galerija ar standarta attēliem. Ja jums kāds no tiem patīk, veiciet klikšķi, lai to izmantotu.</p>
      <list>
        <item>
          <p>If you would rather use a picture you already have on your
          computer, click <gui>Select a file…</gui>.</p>
        </item>
        <item>
          <p>If you have a webcam, you can take a new login photo right now by
          clicking <gui>Take a picture…</gui>. Take your
          picture, then move and resize the square outline to crop out the
          parts you do not want. If you do not like the picture you took, click
          <gui style="button">Take Another Picture</gui> to try again, or
          <gui>Cancel</gui> to give up.</p>
        </item>
      </list>
    </item>
  </steps>

</page>
