<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-launch-applications" xml:lang="pl">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>

    <link type="guide" xref="getting-started" group="videos"/>
    <title role="trail" type="link">Uruchamianie programów</title>
    <link type="seealso" xref="shell-apps-open"/>
    <title role="seealso" type="link">Samouczek uruchamiania programów</title>
    <link type="next" xref="gs-switch-tasks"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2013-2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2013-2020</mal:years>
    </mal:credit>
  </info>

  <title>Uruchamianie programów</title>

  <ui:overlay width="812" height="452">
   <media type="video" its:translate="no" src="figures/gnome-launching-applications.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-launching-apps.svg"/>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>Uruchamianie programów</tt:p>
         </tt:div>
         <tt:div begin="5s" end="7.5s">
           <tt:p>Przesuń kursor myszy na <gui>Podgląd</gui> w lewym górnym rogu ekranu.</tt:p>
           </tt:div>
         <tt:div begin="7.5s" end="9.5s">
           <tt:p>Kliknij ikonę <gui>Wyświetl programy</gui>.</tt:p>
         </tt:div>
         <tt:div begin="9.5s" end="11s">
           <tt:p>Kliknij program, który chcesz uruchomić, na przykład Pomoc.</tt:p>
         </tt:div>
         <tt:div begin="12s" end="21s">
           <tt:p>Można też użyć klawiatury, aby otworzyć <gui>ekran podglądu</gui> naciskając klawisz <key href="help:gnome-help/keyboard-key-super">Super</key>.</tt:p>
         </tt:div>
         <tt:div begin="22s" end="29s">
           <tt:p>Zacznij wpisywać nazwę programu, który chcesz uruchomić.</tt:p>
         </tt:div>
         <tt:div begin="30s" end="33s">
           <tt:p>Naciśnij klawisz <key>Enter</key>, aby uruchomić program.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
   </media>
  </ui:overlay>

  <section id="launch-apps-mouse">
    <title>Uruchamianie programów za pomocą myszy</title>
 
  <steps>
    <item><p>Przesuń kursor myszy na <gui>Podgląd</gui> w lewym górnym rogu ekranu, aby wyświetlić <gui>ekran podglądu</gui>.</p></item>
    <item><p>Kliknij ikonę <gui>Wyświetl programy</gui>, wyświetlaną na dole paska po lewej stronie ekranu.</p></item>
    <item><p>Wyświetlona zostanie lista programów. Kliknij program, który chcesz uruchomić, na przykład Pomoc.</p></item>
  </steps>

  </section>

  <section id="launch-app-keyboard">
    <title>Uruchamianie programów za pomocą klawiatury</title>

  <steps>
    <item><p>Otwórz <gui>ekran podglądu</gui> naciskając klawisz <key href="help:gnome-help/keyboard-key-super">Super</key>.</p></item>
    <item><p>Zacznij wpisywać nazwę programu, który chcesz uruchomić. Wyszukiwanie programu rozpocznie się natychmiast.</p></item>
    <item><p>Po wyświetleniu i wybraniu ikony programu, naciśnij klawisz <key>Enter</key>, aby go uruchomić.</p></item>
  </steps>

  </section>

</page>
