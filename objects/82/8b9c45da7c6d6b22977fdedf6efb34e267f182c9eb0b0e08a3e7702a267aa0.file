<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-get-online" xml:lang="ca">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>
    <link type="guide" xref="getting-started" group="videos"/>
    <title role="trail" type="link">Connecteu-vos</title>
    <link type="seealso" xref="net"/>
    <title role="seealso" type="link">Un programa d'aprenentatge per connectar-se a Internet</title>
    <link type="next" xref="gs-browse-web"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Manel Vidal</mal:name>
      <mal:email>verduler@gmail.com</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  </info>

  <title>Connecteu-vos</title>
  
  <note style="important">
      <p>Podeu veure l'estat de la connexió a la xarxa a la part dreta de la barra superior.</p>
  </note>  

  <section id="going-online-wired">

    <title>Connecteu-vos a una xarxa amb fil</title>

    <media its:translate="no" type="image" mime="image/svg" src="gs-go-online1.svg" width="100%"/>

    <steps>
      <item><p>La icona de l'estat de la connexió a la xarxa situada a la part dreta de la barra superior indica que esteu fora de línia.</p>
      <p>L'estat de fora de línia pot ser degut a diverses causes, per exemple: que el cable de xarxa estigui desconnectat, que l'ordinador estigui en <em>mode d'avió</em> o que no hi hagi cap xarxa sense fil disponible al vostre voltant.</p>
      <p>Si voleu utilitzar una connexió amb fil, només cal que connecteu un cable de xarxa. L'ordinador intentarà configurar la connexió de xarxa automàticament.</p>
      <p>Mentre s'està configurant la connexió de xarxa, la icona de l'estat de la connexió a la xarxa mostra tres punts.</p></item>
      <item><p>Un cop s'ha configurat correctament la connexió de xarxa, la icona de l'estat de la connexió a la xarxa canvia per mostrar el símbol d'un ordinador connectat en xarxa.</p></item>
    </steps>
    
  </section>

  <section id="going-online-wifi">

    <title>Connecteu-vos a una xarxa sense fil</title>

    <media its:translate="no" type="image" mime="image/svg" src="gs-go-online2.svg" width="100%"/>
    
    <steps>
      <title>Per connectar-se a una xarxa Wi-Fi sense fil:</title>
      <item>
        <p>Feu clic al <gui xref="shell-introduction#yourname">menú del sistema</gui> situat a la part dreta de la barra superior.</p>
      </item>
      <item>
        <p>Seleccioneu <gui>Wi-Fi apagat</gui>. La secció de connexió Wi-Fi del menú s'expandirà.</p>
      </item>
      <item>
        <p>Cliqueu <gui>Seleccioneu una xarxa</gui>.</p>
      </item>
    </steps>

     <note style="important">
       <p>Només us podeu connectar a una xarxa sense fil si el vostre maquinari ho suporta i si us trobeu dins d'una zona amb cobertura d'una xarxa sense fil.</p>
     </note>

    <media its:translate="no" type="image" mime="image/svg" src="gs-go-online3.svg" width="100%"/>

    <steps style="continues">
    <item><p>De la llista de xarxes sense fil disponibles, seleccioneu la xarxa a la qual us voleu connectar i per confirmar, feu clic a <gui>Connecta</gui>.</p>
    <p>En funció de quina sigui la configuració de la xarxa, se us demanaran les credencials per connectar-vos-hi.</p></item>
    </steps>

  </section>

</page>
