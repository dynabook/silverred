<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="display-dual-monitors" xml:lang="pl">

  <info>
    <link type="guide" xref="prefs-display"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-09" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-23" status="review"/>
    <revision pkgversion="3.28" date="2018-07-28" status="review"/>
    <revision pkgversion="3.34" date="2019-11-12" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Konfiguracja dodatkowego monitora.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  </info>

<title>Podłączanie drugiego monitora do komputera</title>

<!-- TODO: update video
<section id="video-demo">
  <title>Video Demo</title>
  <media its:translate="no" type="video" width="500" mime="video/webm" src="figures/display-dual-monitors.webm">
    <p its:translate="yes">Demo</p>
    <tt:tt its:translate="yes" xmlns:tt="http://www.w3.org/ns/ttml">
      <tt:body>
        <tt:div begin="1s" end="3s">
          <tt:p>Type <input>displays</input> in the <gui>Activities</gui>
          overview to open the <gui>Displays</gui> settings.</tt:p>
        </tt:div>
        <tt:div begin="3s" end="9s">
          <tt:p>Click on the image of the monitor you would like to activate or
          deactivate, then switch it <gui>ON/OFF</gui>.</tt:p>
        </tt:div>
        <tt:div begin="9s" end="16s">
          <tt:p>The monitor with the top bar is the main monitor. To change
          which monitor is “main”, click on the top bar and drag it over to
          the monitor you want to set as the “main” monitor.</tt:p>
        </tt:div>
        <tt:div begin="16s" end="25s">
          <tt:p>To change the “position” of a monitor, click on it and drag it
          to the desired position.</tt:p>
        </tt:div>
        <tt:div begin="25s" end="29s">
          <tt:p>If you would like both monitors to display the same content,
          check the <gui>Mirror displays</gui> box.</tt:p>
        </tt:div>
        <tt:div begin="29s" end="33s">
          <tt:p>When you are happy with your settings, click <gui>Apply</gui>
          and then click <gui>Keep This Configuration</gui>.</tt:p>
        </tt:div>
        <tt:div begin="33s" end="37s">
          <tt:p>To close the <gui>Displays Settings</gui> click on the
          <gui>×</gui> in the top corner.</tt:p>
        </tt:div>
      </tt:body>
    </tt:tt>
  </media>
</section>
-->

<section id="steps">
  <title>Konfiguracja dodatkowego monitora</title>
  <p>Aby skonfigurować dodatkowy monitor, podłącz go do komputera. Jeśli system nie wykryje go od razu lub potrzeba dostosować ustawienia:</p>

  <steps>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Ekrany</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Ekrany</gui>, aby otworzyć panel.</p>
    </item>
    <item>
      <p>W polu ułożenia ekranów przeciągnij ekrany do wybranych położeń względem siebie.</p>
      <note style="tip">
        <p>Cyfry z tego pola są wyświetlane w górnym lewym rogu każdego ekranu, kiedy panel <gui>Ekrany</gui> jest włączony.</p>
      </note>
    </item>
    <item>
      <p>Kliknij <gui>Główny ekran</gui>, aby wybrać główny ekran.</p>

      <note>
        <p>Główny ekran to ten z <link xref="shell-introduction">górnym paskiem</link>, i na którym wyświetlany jest <gui>ekran podglądu</gui>.</p>
      </note>
    </item>
    <item>
      <p>Wybierz orientację, rozdzielczość lub skalowanie oraz częstotliwość odświeżania.</p>
    </item>
    <item>
      <p>Kliknij przycisk <gui>Zastosuj</gui>. Nowe ustawienia zostaną będą używane przez 20 sekund, po czym zostaną przywrócone. W ten sposób, jeśli nowe ustawienia powodują brak obrazu, to poprzednie zostaną automatycznie przywrócone. Jeśli nowe ustawienia pasują, to kliknij przycisk <gui>Zachowaj zmiany</gui>.</p>
    </item>
  </steps>

</section>

<section id="modes">

  <title>Tryby wyświetlania</title>
    <p>Podczas używania dwóch ekranów dostępne są te tryby:</p>
    <list>
      <item><p><gui>Połączone ekrany:</gui> krawędzie ekranów są połączone, więc można przenosić okna z jednego ekranu na drugi.</p></item>
      <item><p><gui>Ten sam obraz:</gui> ta sama treść jest wyświetlana na dwóch ekranach, w tej samej rozdzielczości i orientacji.</p></item>
      <item><p><gui>Jeden ekran:</gui> tylko jeden ekran jest skonfigurowany, co powoduje wyłączenie drugiego. Na przykład, zewnętrzny monitor podłączony do stacji dokującej laptopa z zamkniętą pokrywą byłby tym jednym skonfigurowanym ekranem.</p></item>
    </list>

</section>

<section id="multiple">

  <title>Dodawanie więcej niż jednego monitora</title>
    <p>W przypadku więcej niż dwóch ekranów, <gui>Połączone ekrany</gui> jest jedynym dostępnym trybem.</p>
    <list>
      <item>
        <p>Kliknij przycisk ekranu, który ma zostać skonfigurowany.</p>
      </item>
      <item>
        <p>Przeciągnij ekrany do wybranych położeń względem siebie.</p>
      </item>
    </list>

</section>
</page>
