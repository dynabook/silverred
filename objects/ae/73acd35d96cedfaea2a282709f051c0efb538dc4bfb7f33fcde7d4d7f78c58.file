<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="keyboard-nav" xml:lang="pt">
  <info>
    <link type="guide" xref="keyboard" group="a11y"/>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>

    <revision pkgversion="3.7.5" version="0.2" date="2013-02-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.20" date="2016-08-13" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-27" status="review"/>

    <credit type="author">
      <name>Michael Hilh</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
       <name>Julita Inca</name>
       <email>yrazes@gmail.com</email>
    </credit>
    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
       <name>Ekaterina Gerasimova</name>
       <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Utilizar aplicações e o escritório sem rato.</desc>
  </info>

  <title>Navegação com o teclado</title>

  <p>Nesta página detalha-se a navegação com o teclado para pessoas que não podem usar um rato ou outro dispositivo apuntador, ou que quer usar o teclado o mais possível. Para ver os atalhos de teclado, úteis para todos os utilizadores, consulte a <link xref="shell-keyboard-shortcuts"/> em seu lugar.</p>

  <note style="tip">
    <p>Se não pode usar um dispositivo apontado como um rato, pode controlar o ponteiro do rato utilizando o teclado numérico de seu teclado. Consulte a <link xref="mouse-mousekeys"/> para obter mais detalhes.</p>
  </note>

<table frame="top bottom" rules="rows">
  <title>Navegar pelas interfaces de utilizador</title>
  <tr>
    <td><p><key>Tab</key> and</p>
    <p><keyseq><key>Ctrl</key><key>Tab</key></keyseq></p>
    </td>
    <td>
      <p>Mover o foco do teclado entre diferentes controles. <keyseq><key>Ctrl</key> <key>Tab</key></keyseq> move-o entre grupos de controles, por exemplo duma barra lateral ao conteúdo principal. <keyseq><key>Ctrl</key><key>Tab</key></keyseq> também pode sair dum controle que use <key>Tab</key> por si mesmo, como um área de texto.</p>
      <p>Mantenha premida a tecla <key>Shift</key> para mover o foco em ordem inversa.</p>
    </td>
  </tr>
  <tr>
    <td><p>Teclas de setas</p></td>
    <td>
      <p>Mover a seleção entre elementos dum único controle, ou entre um conjunto de controles relacionados. Use as teclas de setas para dar o foco aos botões, selecionar elementos numa vista de lista ou de ícones ou para selecionar um botão de rádio num grupo.</p>
    </td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key>Teclas de setas</keyseq></p></td>
    <td><p>Na vista de lista ou de ícones, mover o foco do teclado a outro elemento sem mudar o elemento que está selecionado.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Mayús</key>Teclas de setas</keyseq></p></td>
    <td><p>Na vista de lista ou de ícones, selecionar todos os elementos desde o elemento selecionado neste momento até o novo elemento com o foco.</p>
    <p>In a tree view, items that have children can be expanded or collapsed,
    to show or hide their children: expand by pressing
    <keyseq><key>Shift</key><key>→</key></keyseq>, and collapse by
    pressing <keyseq><key>Shift</key><key>←</key></keyseq>.</p></td>
  </tr>
  <tr>
    <td><p><key>Espaço</key></p></td>
    <td><p>Ativar um elemento que tenha o foco, como um botão, uma lacuna ou um elemento duma lista.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>Espaço</key></keyseq></p></td>
    <td><p>Na vista de lista ou de ícones, selecionar ou desmarcar o elemento com o foco sem desmarcar o resto de elementos.</p></td>
  </tr>
  <tr>
    <td><p><key>Alt</key></p></td>
    <td><p>Mantenha premida a tecla <key>Alt</key> para mostrar os <em>aceleradores</em>: teclas sublinhadas em elementos de menu, botões e outros controles. Carregue <key>Alt</key>+ a tecla sublinhada para ativar o controle, como se tivesse carregado sobre ele.</p></td>
  </tr>
  <tr>
    <td><p><key>Esc</key></p></td>
    <td><p>Sair dum menu, menu emergente, seletor ou janela de diálogo.</p></td>
  </tr>
  <tr>
    <td><p><key>F10</key></p></td>
    <td><p>Abrir o primeiro elemento dum menu na barra de menu duma janela. Use as teclas de setas para navegar pelos menus.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key xref="keyboard-key-super">Super</key> <key>F10</key></keyseq></p></td>
    <td><p>Abrir o menu do aplicação na barra superior.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Shift</key><key>F10</key></keyseq> or</p>
    <p><key xref="keyboard-key-menu">Menu</key></p></td>
    <td>
      <p>Mostrar o menu contextual para a seleção atual, como se tivesse carregado com o botão direito.</p>
    </td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>F10</key></keyseq></p></td>
    <td><p>No gestor de ficheiros, mostrar o menu contextual para a diretório atual, como se tivesse carregado com o botão direito sobre o fundo e não sobre um elemento.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>PageUp</key></keyseq></p>
    <p>and</p>
    <p><keyseq><key>Ctrl</key><key>PageDown</key></keyseq></p></td>
    <td><p>Na interface com flanges, alterar para a flange da esquerda ou à da direita.</p></td>
  </tr>
</table>

<table frame="top bottom" rules="rows">
  <title>Navegar pelo escritório</title>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="alt-f1"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-tab"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-tick"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="ctrl-alt-tab"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-updown"/>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F6</key></keyseq></p></td>
    <td><p>Mover entre as janelas do mesmo aplicação. Mantenha premida a tecla <key>Alt</key> e carregue <key>F6</key> até que se realce a janela que quer, e então solte a tecla <key>Alt</key>. Isto é similar à caraterística <keyseq><key>Alt</key><key>`</key></keyseq>.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>Esc</key></keyseq></p></td>
    <td><p>Rotacionar através de todas as janelas abertas num espaço de trabalho.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>V</key></keyseq></p></td>
    <td><p><link xref="shell-notifications#notificationlist">Open the
    notification list.</link> Press <key>Esc</key> to close.</p></td>
  </tr>
</table>

<table frame="top bottom" rules="rows">
  <title>Navegar pelas janelas</title>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F4</key></keyseq></p></td>
    <td><p>Fechar a janela atual.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F5</key></keyseq> ou <keyseq><key>Super</key><key>↓</key></keyseq></p></td>
    <td><p>Restaurar uma janela maximizada a seu tamanho original. Use <keyseq><key>Alt</key> <key>F10</key></keyseq> para maximizar. <keyseq><key>Alt</key><key>F10</key></keyseq> maximiza e restaura.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F7</key></keyseq></p></td>
    <td><p>Mover a janela atual. Carregue <keyseq><key>Alt</key><key>F7</key></keyseq> e use as teclas de setas para mover a janela. Carregue <key>Enter</key> para deixar de mover a janela, ou <key>Esc</key> para devolver a sua localização original.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F8</key></keyseq></p></td>
    <td><p>Redimensionar a janela atual. Carregue <keyseq><key>Alt</key><key>F8</key></keyseq> e use as teclas de setas para redimensionar a janela. Carregue <key>Enter</key> para deixar de redimensionar a janela, ou <key>Esc</key> para devolver a seu tamanho original.</p></td>
  </tr>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="shift-super-updown"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="shift-super-left"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="shift-super-right"/>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F10</key></keyseq> ou <keyseq><key>Super</key><key>↑</key></keyseq></p>
    </td>
    <td><p><link xref="shell-windows-maximize">Maximizar</link> uma janela. Carregue <keyseq><key>Alt</key><key>F10</key></keyseq> ou <keyseq><key>Super</key><key>↓</key></keyseq> para restaurar uma janela maximizada a seu tamanho original.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>H</key></keyseq></p></td>
    <td><p>Minimizar uma janela</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>←</key></keyseq></p></td>
    <td><p>Maximizar uma janela ao longo da parte esquerda do ecrã. Carregue outra vez para restaurar a janela a seu tamanho original. Carregue <keyseq><key>Super</key><key>→</key></keyseq> para mudar ambos lados.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>→</key></keyseq></p></td>
    <td><p>Maximizar uma janela ao longo de parte-a direita do ecrã. Carregue outra vez para restaurar a janela a seu tamanho original. Carregue <keyseq><key>Super</key><key>←</key></keyseq> para mudar ambos lados</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>Espaço</key></keyseq></p></td>
    <td><p>Mostrar o menu da janela, como se tivesse carregado com o botão direito sobre a barra de título.</p></td>
  </tr>
</table>

</page>
