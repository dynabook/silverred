Current Bug List:
================

- Processing Framemaker input (request from: delorme@panda.ceng.cea.fr)

- When a input PS file contains NULL characters, mpage will go nuts...
  but you'll only see it when you try to print/gs its output.

- Scaling should be done differently: 18/4 gives odd integers, so border
  lines and clipareas maybe off by one point...
  Try mpage -8 -H <file>

- When resizing with -W and -L, the header may become a mesh.
  Probably should not scale header with this...

- CTRL-D characters at the beginning of a file are removed.


Current Wish List:
=================

-  I18N support

- Option to print line numbers before each line of text

- It would be nice if the Makefile used sed on the man page to insert
  the user's PREFIX/share directory into the man page, rather than having
  /usr/local/share/mpage hard coded.

- Build Configure script...


- Build RPM package for mpage


- Check out th -j option. Does not seem to work properly.
  maybe it is  fixed with the multiple -j patches...


- Add support to select paper trays.


- Tidy up the manual page. Option description could be much better...


- Allow printing of header for postscript-input also. 
  Sort of request by Tobias Buchal <buch@ifh.bau-verm.uni-karlsruhe.de>


- Better control of how pages are positioned on the page 
  (David B. Peterson (dave@Spacestar.COM,
   Ted Stern <stern@amath.washington.edu>
   Dave Lopez <lopez@chdasic.sps.mot.com>)
  found a need for again a different layout. Guess we need something like:

  -<someoption><#c><#r>[[<portrait/landscape><normal/upsidedown>]<page#>]+

  and pages are printed viewing as portrait from left to right from
  bottom to top, eg (proposing option -e):

   -e2c3rpu14l23n56 

  would print logical pages in 2 (short side) by 3 long side) rows: 
      1 and 4 in portrait, upside down
      2 and 3 in landscape, upside down
      5 and 6 in landscape, normal
  with logical page 1 in the lower left corner, page 4 in the lower right
  corner, page 2 in the middle row left, page 3 in the middle row right,
  page 5 in the top row left, page 6 in the top row right.


- Definition of page location, landscape/portrait etc becomes spagetti.
  Need to redefine the way this is setup...


- Define an option to leave some space between logical pages. 
  suggested by: Jeremie PETIT petit@aurora.unice.fr


- More flexible header definitions (size, fontname, which items where
  greying ...):,e.g.:

  mpage ... -H="grey=20% user=left filename=center pagenumber=right" ...

  suggested by: Jeremie PETIT petit@aurora.unice.fr


- Define a (optional) physical page header with optionally a
  physical pagenumber. Also dates etc.
  Currently partly implemented: -X option. Could use some work
  on proper font (bold/not bold...)


- Also might want a general pageframe around the logical pages. On top
  of this frame we can put the -X option output.


- The postscript character encoding support is sort of a hack. I guess
  things could be a bit cleaner.


- Support for reducing mpage output.  I.E. if I do:

		mpage -8P file.txt | mpage -8

  I should get 64-up output.  Problems are that the PostScript code
  produced is not "recursive".  The end-of-page macro in particular
  suffers under re-definition, you get blank pages.  Another problems is
  resolution of the printer.  See the next Wish item.

 
- Better handeling of PostScript input.  The current stuff works, but
  it is not general enough.  I am currently learning more here, thanks
  to the "Green Book" and will work on a cleaner parser for Version 3.
  (Oh-my-goodness, a pre-announcement ;-)

  Perhaps I should convert the whole PostScript processing routing to be
  a "Finite Automata".  Each line will represent a "token" parsed by its
  comment, output will be based upon transitions from state to state.
  The automata should allow states and transitions to be added dynamicly
  so that we can adapt to differening structure conventions, and
  differing PostScript code generators (psdit, dvi2ps, etc ...)


- From: zachary brown <zbrown@lynx.dac.neu.edu>

  When I use mpage on
  postscript files, the document's preexisting margins cause mpage to leave a
  lot of blank space in between each mini-page, thus causing it to shrink the
  pages more than necessary. My suggestion is this: allow the user to specify
  how much a mini-page can break out of the boundary mpage sets for it and
  overwrite what is on a neighboring mini-page. Also allow the user to specify
  how far along either end of the imaginary line connecting the upper left
  corner and the lower right corner of each minipage, the mini-page will be
  magnified. This way, since there would only be those extra margins poking
  into other space, nothing would in fact be overwritten, and mpage would
  magnify the tiny-pages to their optimal size, and (since the user would
  specify the edges to be magnified), there would be no problem with part of
  the mini-page being off the physical page. I came to be aware of this
  problem by trying to print out the gnu libc manual, and finding no way to
  avoid huge gaps between the mini-pages.


- Add better timing information, possible adding command line
  arguments to control the amount of timing information added to the
  file.  (Eyuk, more arguments:-(


- Recognise things like c^Hc^Hc to be displayed in bold font...
  (underline works fine currently)


- Lots more, I'm sure.

