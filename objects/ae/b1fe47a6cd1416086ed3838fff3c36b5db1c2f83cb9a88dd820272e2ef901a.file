<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-lost" xml:lang="nl">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>

    <revision pkgversion="3.8.0" date="2013-04-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Gnome-documentatieproject</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Kijk in het <gui>Activiteiten</gui>-overzicht of andere werkbladen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Zoeken naar een verloren venster</title>

  <p>U vindt een scherm dat zich bevindt in een andere werkruimte of achter een ander scherm eenvoudig via het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht:</p>

  <list>
    <item>
      <p>Open het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht. Als het venster dat u zoekt zich bevindt in de huidige <link xref="shell-windows#working-with-workspaces">werkruimte</link>, dan wordt het hier getoond in miniatuur. Klik eenvoudigweg op het miniatuur om het venster weer te tonen, of</p>
    </item>
    <item>
      <p>Klik op verschillende werkruimtes in de <link xref="shell-workspaces">werkruimtekiezer</link> aan de rechterkant van het scherm om naar het venster te zoeken, of</p>
    </item>
    <item>
      <p>Klik met rechts op de toepassing in de starter, waardoor u een lijst met geopende vensters krijgt. Klik op het venster in de lijst om er naartoe te gaan.</p>
    </item>
  </list>

  <p>De vensterwisselaar gebruiken:</p>

  <list>
    <item>
      <p>Druk op <keyseq><key xref="keyboard-key-super">Super</key><key>Tab</key></keyseq> om de <link xref="shell-windows-switching">vensterwisselaar</link> weer te geven. Blijf de <key>Super</key>-toets ingedrukt houden en druk op de <key>Tab</key>-toets om door de geopende vensters te  navigeren, of <keyseq><key>Shift</key><key>Tab</key> </keyseq> om achteruit te navigeren.</p>
    </item>
    <item if:test="!platform:gnome-classic">
      <p>Als een toepassing meerdere geopende vensters heeft,  dan houdt u <key>Super</key> ingedrukt en drukt u op <key>`</key> (of de toets boven <key>Tab</key>) om er doorheen te bladeren.</p>
    </item>
  </list>

</page>
