<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-launch-applications" xml:lang="kn">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>

    <link type="guide" xref="getting-started" group="videos"/>
    <title role="trail" type="link">ಅನ್ವಯಗಳನ್ನು ಆರಂಭಿಸಿ</title>
    <link type="seealso" xref="shell-apps-open"/>
    <title role="seealso" type="link">ಅನ್ವಯಗಳನ್ನು ಆರಂಭಿಸುವಿಕೆಯ ಕುರಿತು ಮಾಹಿತಿ</title>
    <link type="next" xref="gs-switch-tasks"/>
  </info>

  <title>ಅನ್ವಯಗಳನ್ನು ಆರಂಭಿಸಿ</title>

  <ui:overlay width="812" height="452">
   <media type="video" its:translate="no" src="figures/gnome-launching-applications.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-launching-apps.svg"/>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>ಅನ್ವಯಗಳನ್ನು ಆರಂಭಿಸುವಿಕೆ</tt:p>
         </tt:div>
         <tt:div begin="5s" end="7.5s">
           <tt:p>ನಿಮ್ಮ ಮೌಸ್‌ನ ಸೂಚಕವನ್ನು ತೆರೆಯ ಮೇಲ್ಭಾಗದ ಎಡಗಡೆ ಇರುವ <gui>ಚಟುವಟಿಕೆಗಳು</gui> ಮೂಲೆಗೆ ತೆಗೆದುಕೊಂಡುಹೋಗಿ.</tt:p>
           </tt:div>
         <tt:div begin="7.5s" end="9.5s">
           <tt:p><gui>ಅನ್ವಯಗಳನ್ನು ತೋರಿಸು</gui> ಚಿಹ್ನೆಯ ಮೇಲೆ ಕ್ಲಿಕ್ ಮಾಡಿ.</tt:p>
         </tt:div>
         <tt:div begin="9.5s" end="11s">
           <tt:p>ನೀವು ಚಲಾಯಿಸಲು ಬಯಸುವ ಅನ್ವಯದ ಮೇಲೆ ಕ್ಲಿಕ್ ಮಾಡಿ, ಉದಾಹರಣೆಗೆ, ಸಹಾಯ.</tt:p>
         </tt:div>
         <tt:div begin="12s" end="21s">
           <tt:p>ಪರ್ಯಾಯವಾಗಿ, <gui>ಚಟುವಟಿಕೆಗಳ ಅವಲೋಕನ</gui>ವನ್ನು ತೆರೆಯಲು ಕೀಲಿಮಣೆಯಲ್ಲಿನ <key href="help:gnome-help/keyboard-key-super">Super</key> ಕೀಲಿಯನ್ನು ಒತ್ತಿ.</tt:p>
         </tt:div>
         <tt:div begin="22s" end="29s">
           <tt:p>ನೀವು ಚಲಾಯಿಸಲು ಬಯಸುವ ಅನ್ವಯದ ಹೆಸರನ್ನು ನಮೂದಿಸಲು ಆರಂಭಿಸಿ.</tt:p>
         </tt:div>
         <tt:div begin="30s" end="33s">
           <tt:p>ಅನ್ವಯವನ್ನು ಚಲಾಯಿಸಲು <key>Enter</key> ಅನ್ನು ಒತ್ತಿ.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
   </media>
  </ui:overlay>

  <section id="launch-apps-mouse">
    <title>ಮೌಸ್‌ ಮುಖಾಂತರ ಅನ್ವಯಗಳನ್ನು ಆರಂಭಿಸಿ</title>
 
  <steps>
    <item><p><gui>ಚಟುವಟಿಕೆಗಳ ಅವಲೋಕನವನ್ನು</gui> ತೋರಿಸಲು ನಿಮ್ಮ ಮೌಸ್‌ನ ಸೂಚಕವನ್ನು ತೆರೆಯ ಮೇಲ್ಭಾಗದ ಎಡಗಡೆ ಇರುವ <gui>ಚಟುವಟಿಕೆಗಳು</gui> ಮೂಲೆಗೆ ತೆಗೆದುಕೊಂಡುಹೋಗಿ&gt;.</p></item>
    <item><p>ತೆರೆಯ ಎಡಗಡೆಯಲ್ಲಿ ಕೆಳಭಾಗದಲ್ಲಿರುವ ಪಟ್ಟಿಯಲ್ಲಿ ತೋರಿಸಲಾಗುವ <gui>ಅನ್ವಯಗಳನ್ನು ತೋರಿಸು</gui> ಚಿಹ್ನೆಯ ಮೇಲೆ ಕ್ಲಿಕ್ ಮಾಡಿ.</p></item>
    <item><p>ಅನ್ವಯಗಳ ಒಂದು ಪಟ್ಟಿಯನ್ನು ತೋರಿಸಲಾಗುತ್ತದೆ. ನೀವು ಚಲಾಯಿಸಲು ಬಯಸುವ ಅನ್ವಯದ ಮೇಲೆ ಕ್ಲಿಕ್ ಮಾಡಿ, ಉದಾಹರಣೆಗೆ, ಸಹಾಯ.</p></item>
  </steps>

  </section>

  <section id="launch-app-keyboard">
    <title>ಕೀಲಿಮಣೆಯ ಮುಖಾಂತರ ಅನ್ವಯಗಳನ್ನು ಆರಂಭಿಸಿ</title>

  <steps>
    <item><p><gui>ಚಟುವಟಿಕೆಗಳ ಅವಲೋಕನ</gui>ವನ್ನು ತೆರೆಯಲು <key href="help:gnome-help/keyboard-key-super">Super</key> ಕೀಲಿಯನ್ನು ಒತ್ತಿ.</p></item>
    <item><p>ನೀವು ಚಲಾಯಿಸಲು ಬಯಸುವ ಅನ್ವಯದ ಹೆಸರನ್ನು ನಮೂದಿಸಲು ಆರಂಭಿಸಿ. ತಕ್ಷಣ ಅನ್ವಯಕ್ಕಾಗಿ ಹುಡುಕಲು ಆರಂಭಿಸಲಾಗುತ್ತದೆ.</p></item>
    <item><p>ಅನ್ವಯದ ಚಿಹ್ನೆಯು ಕಾಣಿಸಿಕೊಂಡು ನಂತರ ಅದನ್ನು ಆರಿಸಿದಾಗ, ಅನ್ವಯವನ್ನು ಚಲಾಯಿಸಲು <key>Enter</key> ಅನ್ನು ಒತ್ತಿ.</p></item>
  </steps>

  </section>

</page>
