<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-add" xml:lang="es">

  <info>
    <link type="guide" xref="user-accounts#manage" group="#first"/>

    <revision pkgversion="3.8" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2013-11-01" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Añadir usuarios nuevos de manera que otras personas puedan iniciar sesión en el equipo.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Añadir una cuenta de usuario nueva</title>

  <p>Puede añadir varias cuentas de usuario a su equipo. Dar una cuenta a cada persona en su hogar o empresa. Cada usuario tiene su propia carpeta de inicio, documentos y configuración.</p>

  <p>Necesita <link xref="user-admin-explain">privilegios de administrador</link> para añadir cuentas de usuario.</p>

  <steps>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Usuarios</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Usuarios</gui> para abrir el panel.</p>
    </item>
    <item>
      <p>Pulse <gui style="button">Desbloquear</gui> en la esquina superior derecha e introduzca su contraseña cuando se le pida.</p>
    </item>
    <item>
      <p>Debajo de la lista de cuentas en la izquierda, pulse el botón <gui style="button">+</gui> para añadir una cuenta de usuario nueva.</p>
    </item>
    <item>
      <p>Si quiere que el nuevo usuario tenga <link xref="user-admin-explain">acceso de administrador</link> al equipo, seleccione <gui>Administrador</gui> para el tipo de cuenta.</p>
      <p>Los administradores pueden hacer cosas como añadir y eliminar usuarios, instalar software y controladores y cambiar la fecha y la hora.</p>
    </item>
    <item>
      <p>Escriba el nombre completo del nuevo usuario. El nombre de usuario se rellenará automáticamente en función del nombre completo. Si no le gusta el nombre de usuario propuesto, puede cambiarlo.</p>
    </item>
    <item>
      <p>Puede elegir entre establecer una contraseña para el nuevo usuario o dejar que la establezca la primera vez que inicie sesión.</p>
      <p>Si elige establecer ahora la contraseña, puede pulsar el icono <gui style="button"><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16">
      <span its:translate="yes">generar contraseña</span></media></gui> para generar automáticamente una contraseña aleatoria.</p>
    </item>
    <item>
      <p>Pulse <gui>Añadir</gui>.</p>
    </item>
  </steps>

  <p>Si quiere cambiar la contraseña después de crear la cuenta, seleccione la cuenta, <gui style="button">desbloquee</gui> el panel y pulse sobre el estado actual de la contraseña.</p>

  <note>
    <p>En en panel <gui>Usuarios</gui> puede pulsar la imagen junto al nombre del usuario, a la derecha, para establecer una imagen para la cuenta. Esta imagen se mostrará en la ventana de inicio de sesión. GNOME proporciona algunos conjuntos de imágenes que puede usar, o puede seleccionar su propia imagen o hacer una foto con su cámara web.</p>
  </note>

</page>
