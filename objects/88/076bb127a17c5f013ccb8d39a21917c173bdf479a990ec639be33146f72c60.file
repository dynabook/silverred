<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-bouncekeys" xml:lang="te">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>షాన్ మెక్‌కేన్స్</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>ఫిల్ బుల్</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>మైకేల్ హిల్</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>ఒకే కీను త్వరితంగా-మరిన్ని సార్లు నొక్కితే విస్మరించు.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Praveen Illa</mal:name>
      <mal:email>mail2ipn@gmail.com</mal:email>
      <mal:years>2011, 2014. </mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>కృష్ణబాబు క్రొత్తపల్లి</mal:name>
      <mal:email>kkrothap@redhat.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

  <title>బౌన్స్ మీటలను ప్రారంరంభించు</title>

  <p>వేగంగా పునరావృతమయ్యే కీ వత్తులను విస్మరించుటకు <em>బౌన్స్ కీలు</em> ఆన్ చేయి. ఉదాహరణకు, మీ చేయి వణకడం మూలాన వొకసారి నొక్కుదామని మరిన్ని సార్లు నొక్కుతూంటే, మీరు బౌన్స్ కీలను ఆన్ చేయాలి.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Universal Access</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Press <gui>Typing Assist (AccessX)</gui> in the <gui>Typing</gui>
      section.</p>
    </item>
    <item>
      <p>Switch the <gui>Bounce Keys</gui> switch to on.</p>
    </item>
  </steps>

  <note style="tip">
    <title>బౌన్స్ కీలను త్వరితంగా ఆన్ మరియు ఆఫ్ చేయి</title>
    <p>పై పట్టీ నందలి <link xref="a11y-icon">ఏక్సెసబిలిటి ప్రతిమ</link> పైన నొక్కి <gui>బౌన్స్ కీలు</gui> ఎంపికచేయుట ద్వారా మీరు బౌన్స్ కీలు ఆన్ లేదా ఆఫ్ చేయవచ్చు. <gui>సార్వత్రిక ఏక్సెస్</gui> పానల్ నుండి వొకటి లేదా అంతకన్నా ఎక్కువ అమరికలు చేతనమైనప్పుడు ఏక్సెసబిలిటీ ప్రతిమ కనిపించును.</p>
  </note>

  <p>మీరు మొదటిసారి కీ వత్తిన తరువాత ఏంతసేపటకి తరువాతి కీ వత్తును  బౌన్సు కీలు నమోదుచేయాలో ఆ సమయాన్ని మార్చుటకు <gui>ఆమోదమగు ఆలస్యం</gui> స్లైడర్  వుపయోగించుము. ఇంతకు మునుపు నొక్కిన కీ తరువాత అతి త్వరగా అదే కీను నొక్కుట వలన కీ వత్తును విస్మరించిన ప్రతిసారి కంప్యూటర్ శబ్దం చేయవలెనంటే <gui>కీ తిరస్కరించబడినప్పుడు శబ్ధంచేయి</gui> ఎంపికచేయి.</p>

</page>
