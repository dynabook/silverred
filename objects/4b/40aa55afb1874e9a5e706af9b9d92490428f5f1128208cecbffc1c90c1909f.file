
COPYRIGHT:

Mpage and all the files distributed with mpage are covered by copyright:

 Copyright (c) 1994-2004 Marcel J.E. Mol, The Netherlands
 Copyright (c) 1988 Mark P. Hahn, Herndon, Virginia
  
     This program is free software; you can redistribute it and/or
     modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2
     of the License, or (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program; if not, write to the Free Software
     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

   marcel@mesa.nl
   MESA Consulting  B.V.
   Nootdorp
   The Netherlands
   Phone: +31-15-3105252
   Mobile:+31-6-54724868
   Fax:   +31-15-3105253
   email: info@mesa.nl		http://www.mesa.nl   ftp://ftp.mesa.nl
   

=================================================================
DESCRIPTION:

Mpage is a program to reduce and print multiple pages of text per
sheet on a PostScript compatible printer.
It also has limited functionality to do the same directly with postscript
files.

The following are the files you should have for mpage.

     README                 Notes and descriptions, this file
     README.OS2             OS/2 port description
     Copyright              Copyright notice
     CHANGES                Change history
     Makefile               The Makefile
     TODO                   Wish List for changes
     FAQ                    Useful tips and hints
     NEWS                   Global changes, User visible changes
     Mpage.lsm              LSM file for mpage
     args.c                 Command line and options processing
     encoding.h             Definition of internal default character encoding
     encoding.h.CP850       Popular for renaming to encoding.h
     file.c                 Generic file handling
     glob.c                 Global variable setup
     mpage.h                Definitions
     mpage.c                Main Control
     page.c                 Page layout routines
     post.c                 PostScript file processing
     sample.c               Prints sample page layout pages 
     text.c                 Text file processing
     util.c                 Misc utility functions
     mpage.1                Manual page

     All.chars              Test file containing all ASCII characters
     Encodings              Directory with character encoding library files
     Characters             List of Postscript character encoding names
     Encoding.format        Description on how to create character encoding
                            library files
     gencodes.c             Util program to build All.chars file
     Test                   Directory with test pages

     OS2                    Directory with simulated lpr for OS/2
     Contrib                Directory with contributions by others
     Contrib/mfix           Fix to mpage that makes it work with ArborText


INSTRUCTIONS:

All you should need to do is run make. Actually it is probably better
to have a look at the Makefile and to check settings like for example PAGESIZE
to set the default page size (e.g A4 or Letter ...).
This will create the programs mpage and msample.  Mpage is
the program to print n-up pages.  Msample prints a sample outline.  I
used it for debugging the placement of the layout.  It is also handy
for other layout purposes.  It accepts all the arguments that mpage
does, but does not print files.

As a quick sample try:

	"mpage  mp_args.c"
or
	"mpage -8 mp_post.c"
or
	"groff  -man mpage.1 | mpage -2"


The manual page, mpage.1, formats with UCB or ATT manual macros.



USING MPAGE:

You will find that it takes a while on an Apple LW or Apple LW+ to
print 4 pages per sheet, It takes "a good long time" for 8 pages.
Reduced PostScript documents take a while too.  (A rule of thumb might
be 4 times as long for 4-pages-per-sheet reduction :-) On a QMS 8ppm
printer 4 pages per sheet is about the same speed and other forms of
"1-up" output.  mpage prints some timing information back from the
printer so you can measure the time required for full jobs.



ADMINISTRATORS:

As a printer administrator you should see the wish list for caveats
and information about future versions.  I still have work to do to
make mpage more "administrator friendly", but figured that I'd better
get what's been done out the door.



BUG REPORTS:

Mpage is supported but this is done in spare time. So please send in
bug reports, requests or other things related to mpage and once in
a while I'll release a version...
If you have a problem please send me a complete description of the
symptoms.  Please include the brand name of your printer, a copy of
the input file (especially if it is with PostScript input), and a
description of what to look for on the output.  If your printer has an
error log, please include as much of it as is revalant.  In general,
the more clearly organized information I have, the easier it will be
for me. And please do not forget to mention the mpage version you're
using. Also the output generated by your mpage might come in handy...



MAKING CHANGES:

Please, if you make any improvements, send them back to me so that I
can include them in future versions.  Also I would solicit comments as
to the usefulness, and effectiveness of mpage.  Let me know what you
like or dislike and feel free to ask any questions you have concerning
this mpage distribution.  Thank you!

-Marcel Mol
------------------------------------------------------------------------------
Marcel J.E. Mol               phone: (+31) 015-3105252
------------------------------------------------------------------------------
                                   | MESA Consulting BV
                                   | Networking and Unix Consultancy
                                   | marcel@mesa.nl
------------------------------------------------------------------------------
 They couldn't think of a number, so they gave me a name!
                                -- Rupert Hine
