<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="hardware-cardreader" xml:lang="nl">

  <info>
    <link type="guide" xref="media#photos"/>
    <link type="guide" xref="hardware#problems"/>

    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Gnome-documentatieproject</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Troubleshoot media card readers.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

<title>Problemen met geheugenkaartlezers</title>

<p>Veel computers hebben lezers voor SD (Secure Digital), MMC (MultiMediaCard), SmartMedia, geheugenstick, CompactFlash en andere opslagmediakaarten. Deze zouden automatisch gedetecteerd en <link xref="disk-partitions">aangekoppeld</link> moeten worden. Hier volgen een paar stappen voor het oplossen van het probleem als dat niet het geval is:</p>

<steps>
<item>
<p>Zorg ervoor dat de kaart op de juiste manier is ingebracht. Veel kaarten lijken "ondersteboven" te zitten wanneer ze op de juiste manier zijn ingebracht. Zorg er ook voor dat de kaart goed in de sleuf zit; vooral CompactFlash moet met enige kracht ingebracht worden. (Pas op dat u niet te hard drukt! Als u ergens tegenaan komt, forceer dan niet.)</p>
</item>

<item>
  <p>Open <app>Bestanden</app> via het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht. Verschijnt de ingebrachte kaart in de lijst met <gui>Apparaten</gui> in de linker zijbalk? Soms staat de kaart wel in de lijst, maar is hij niet aangekoppeld; klik er één keer op om hem aan te koppelen. (Als de zijbalk niet zichtbaar is, druk dan op <key>F9</key> of klik op <gui style="menu">Bestanden</gui> in de bovenbalk en kies de <gui> Zijbalk</gui>.)</p>
</item>

<item>
  <p>Als uw kaart niet getoond wordt in de zijbalk, druk dan op <keyseq><key>Ctrl</key><key>L</key></keyseq>, typ vervolgens <input>computer:///</input> en druk op <key>Enter</key>. Als uw kaart op de juiste wijze is geconfigureerd, dan zou de lezer als een station te zien moeten zijn wanneer er geen kaart in zit, en de kaart zelf wanneer de kaart gekoppeld is.</p>
</item>

<item>
<p>Als u wel de kaartlezer ziet, maar niet de kaart, dan ligt het probleem mogelijk bij de kaart zelf. Probeer het met een andere kaart of controleer de kaart in een andere lezer, indien mogelijk.</p>
</item>
</steps>

<p>Als er geen kaarten of schijven beschikbaar zijn in de map <gui>Computer</gui>, dan is het mogelijk dat uw kaartlezer niet werkt met Linux omdat er geen stuurprogramma voor is. Dit is het meer waarschijnlijk wanneer u een ingebouwde kaartlezer heeft (in de computer in plaats van daar buiten). De beste oplossing is om uw apparaat (camera, mobiele telefoon, enz.) direct op een usb-poort van de computer aan te sluiten. Externe usb-kaartlezers zijn ook te krijgen en worden veel beter ondersteund door Linux.</p>

</page>
