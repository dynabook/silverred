<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" version="1.0 if/1.0" id="shell-introduction" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="shell-overview" group="#first"/>
    <link type="guide" xref="index" group="intro"/>

    <revision pkgversion="3.6.0" date="2012-10-13" status="review"/>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-28" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="review"/>
    <revision pkgversion="3.35.91" date="2020-07-19" status="review"/>

    <credit type="author">
      <name>Šon Mek Kens</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klaper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>A visual overview of your desktop, the top bar, and the
    <gui>Activities</gui> overview.</desc>
  </info>

  <title>Visual overview of GNOME</title>

  <p>Gnom 3 karakteriše potpuno nov izgled korisničkog sučelja osmišljenog da vam ne stoji na putu, umanji ometanje, i da vam pomogne da obavite posao. Kada se prvi put prijavite, videćete praznu radnu površ i gornju traku.</p>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <media type="image" src="figures/shell-top-bar.png" width="600" if:test="!target:mobile">
      <p>Gornja traka Gnomove školjke</p>
    </media>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-top-bar-classic.png" width="500" if:test="!target:mobile">
      <p>Gornja traka Gnomove školjke</p>
    </media>
  </if:when>
</if:choose>

  <p>The top bar provides access to your windows and applications, your
  calendar and appointments, and
  <link xref="status-icons">system properties</link> like sound, networking,
  and power. In the system menu in the top bar, you can change the volume or
  screen brightness, edit your <gui>Wi-Fi</gui> connection details, check your
  battery status, log out or switch users, and turn off your computer.</p>

<links type="section"/>

<!-- TODO: Replace "Activities overview" title for classic mode with something
like "Application windows" by using if:when and if:else ? -->
<section id="activities">
  <title>Pregled <gui>Aktivnosti</gui></title>

<media type="image" src="figures/shell-activities-dash.png" height="530" style="floatstart floatleft" if:test="!target:mobile, !platform:gnome-classic">
  <p>Activities button and Dash</p>
</media>

  <p if:test="!platform:gnome-classic">Da pristupite vašim prozorima i programima, kliknite na dugme <gui>Aktivnosti</gui>, ili jednostavno pomerite pokazivač miša u gornji levi ugao. Takođe možete da pritisnete taster <key xref="keyboard-key-super">Super</key> na vašoj tastaturi. Videćete vaše prozore i programe u pregledu. Možete takođe samo da započnete da kucate da biste pretražili vaše programe, datoteke, fascikle i veb.</p>

  <p if:test="platform:gnome-classic">To access your windows and applications,
  click the button at the bottom left of the screen in the window list. You can
  also press the <key xref="keyboard-key-super">Super</key> key to see an
  overview with live thumbnails of all the windows on the current workspace.</p>

  <p if:test="!platform:gnome-classic">On the left of the overview, you will find the <em>dash</em>. The dash
  shows you your favorite and running applications. Click any icon in the
  dash to open that application; if the application is already running, it will
  have a small dot below its icon. Clicking its icon will bring up the most
  recently used window. You can also drag the icon to the overview, or onto any
  workspace on the right.</p>

  <p if:test="!platform:gnome-classic">Klik desnim tasterom miša na ikonicu prikazuje izbornik koji vam omogućava da izaberete bilo koji prozor u pokrenutom programu, ili da otvorite novi prozor. Možete takođe da kliknete na ikonicu dok držite pritisnutim taster <key>Ktrl</key> da otvorite novi prozor.</p>

  <p if:test="!platform:gnome-classic">Kada ste u pregledu, na početku će vam uvek biti prikazan pregled prozora. U njemu su vam prikazane žive minijature svih prozora na tekućem radnom prostoru.</p>

  <p if:test="!platform:gnome-classic">Kliknite na dugme mreže pri dnu poletnika da prikažete pregled programa. Ovo će vam prikazati sve programe instalirane na vašem računaru. Kliknite na neki od programa da ga pokrenete, ili prevucite program u pregled ili na minijaturu radnog prostora. Možete takođe da prevučete program u poletniku da biste ga učinili omiljenim. Vaši omiljeni programi ostaju u poletniku čak i kada nisu pokrenuti, tako da im možete pristupiti brzo.</p>

  <list style="compact">
    <item>
      <p><link xref="shell-apps-open">Saznajte više o početnim programima.</link></p>
    </item>
    <item>
      <p><link xref="shell-windows">Saznajte više o prozorima i radnim prostorima.</link></p>
    </item>
  </list>

</section>

<section id="appmenu">
  <title>Izbornik programa</title>
  <if:choose>
    <if:when test="!platform:gnome-classic">
      <media type="image" src="figures/shell-appmenu-shell.png" width="250" style="floatend floatright" if:test="!target:mobile">
        <p>Izbornik programa <app>Terminal</app></p>
      </media>
      <p>Application menu, located beside the <gui>Activities</gui> button,
      shows the name of the active application alongside with its icon and
      provides quick access to windows and details of the application, as well
      as a quit item.</p>
    </if:when>
    <!-- TODO: check how the app menu removal affects classic mode -->
    <if:when test="platform:gnome-classic">
      <media type="image" src="figures/shell-appmenu-classic.png" width="250" style="floatend floatright" if:test="!target:mobile">
        <p>Izbornik programa <app>Terminal</app></p>
      </media>
      <p>Izbornik programa, koje se nalazi pored izbornika <gui>Programi</gui> i <gui>Prečice</gui>, prikazuje naziv radnog programa zajedno sa njegovom ikonicom i obezbeđuje brz pristup postavkama ili pomoći programa. Stavke koje su dostupne u izborniku programa zavise od programa.</p>
    </if:when>
  </if:choose>

</section>

<section id="clock">
  <title>Sat, kalendar i sastanci</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <media type="image" src="figures/shell-appts.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Sat, kalendar, sastanci i obaveštenja</p>
    </media>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-appts-classic.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Sat, kalendar, i sastanci</p>
    </media>
  </if:when>
</if:choose>

  <p>Click the clock on the top bar to see the current date, a month-by-month
  calendar, a list of your upcoming appointments and new notifications. You can
  also open the calendar by pressing
  <keyseq><key>Super</key><key>M</key></keyseq>. You can access the date and
  time settings and open your full calendar application directly from
  the menu.</p>

  <list style="compact">
    <item>
      <p><link xref="clock-calendar">Saznajte više o kalendaru i sastancima.</link></p>
    </item>
    <item>
      <p><link xref="shell-notifications">Learn more about notifications and
      the notification list.</link></p>
    </item>
  </list>

</section>


<section id="systemmenu">
  <title>System menu</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <media type="image" src="figures/shell-exit.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Korisnički izbornik</p>
    </media>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-exit-classic.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Korisnički izbornik</p>
    </media>
  </if:when>
</if:choose>

  <p>Kliknite na sistemski izbornik u gornjem desnom uglu da upravljate sistemskim podešavanjima i vašim računarom.</p>

<!-- TODO: Update for 3.36 UI option "Do Not Disturb" in calendar dropdown:

<p>If you set yourself to Unavailable, you won’t be bothered by message popups
at the bottom of your screen. Messages will still be available in the message
tray when you move your mouse to the bottom-right corner. But only urgent
messages will be presented, such as when your battery is critically low.</p>
-->

  <p>Kada napustite vaš računar, možete zaključati ekran da neko drugi ne bi mogao da ga koristi. Takođe možete vrlo lako da promenite korisnike bez potpunog odjavljivanja da biste nekome dali pristup vašem računaru, ili možete da obustavite ili da ugasite računar iz izbornika. Ako imate ekran koji podržava uspravno ili vodoravno okretanje, možete vrlo brzo da ga okrenete u sistemskom izbornuku. Ako vaš ekran ne podržava okretanje, nećete videti dugme.</p>

  <list style="compact">
    <item>
      <p><link xref="shell-exit">Saznajte više o promeni korisnika, odjavljivanju, i gašenju računara.</link></p>
    </item>
  </list>

</section>

<section id="lockscreen">
  <title>Zaključavanje ekrana</title>

  <p>When you lock your screen, or it locks automatically, the lock screen is
  displayed. In addition to protecting your desktop while you’re away from your
  computer, the lock screen displays the date and time. It also shows
  information about your battery and network status.</p>

  <list style="compact">
    <item>
      <p><link xref="shell-lockscreen">Saznajte više o zaključavanju ekrana.</link></p>
    </item>
  </list>

</section>

<section id="window-list">
  <title>Spisak prozora</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <p>Gnoma odlikuje drugačiji pristup prebacivanja prozora nego što je to stalno vidljiv spisak prozora koji nalazimo u drugim okruženjima radne površi. Ovo vam dopušta da se usredsredite na zadatak na kome radite bez ometanja.</p>
    <list style="compact">
      <item>
        <p><link xref="shell-windows-switching">Saznajte više o prebacivanju prozora.</link></p>
      </item>
    </list>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-window-list-classic.png" width="800" if:test="!target:mobile">
      <p>Spisak prozora</p>
    </media>
    <p>Spisak prozora na dnu ekrana obezbeđuje pristup svim vašim otvorenim prozorima i programima i dopušta vam da ih brzo umanjite i povratite.</p>
    <p>At the right-hand side of the window list, GNOME displays the four
    workspaces. To switch to a different workspace, select the workspace you
    want to use.</p>
  </if:when>
</if:choose>

</section>

</page>
