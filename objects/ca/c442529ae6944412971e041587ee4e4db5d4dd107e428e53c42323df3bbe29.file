<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-bouncekeys" xml:lang="vi">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Bỏ qua phím lặp nhanh.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nguyễn Thái Ngọc Duy</mal:name>
      <mal:email>pclouds@gmail.com</mal:email>
      <mal:years>2011-2012.</mal:years>
    </mal:credit>
  </info>

  <title>Bật phím dội</title>

  <p>Bật <em>phím dội</em> để bỏ qua phím nhấn được lặp lại nhanh. Ví dụ, nếu bạn run tay và nhấn một phím nhiều lần khi bạn chỉ muốn nhấn một lần, bạn nên bật phím dội.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Universal Access</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Press <gui>Typing Assist (AccessX)</gui> in the <gui>Typing</gui>
      section.</p>
    </item>
    <item>
      <p>Switch the <gui>Bounce Keys</gui> switch to on.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Bật hoặc tắt phím dội nhanh</title>
    <p>You can turn bounce keys on and off by clicking the
    <link xref="a11y-icon">accessibility icon</link> on the top bar and
    selecting <gui>Bounce Keys</gui>. The accessibility icon is visible when
    one or more settings have been enabled from the <gui>Universal Access</gui>
    panel.</p>
  </note>

  <p>Dùng con trượt <gui>Độ trễ chấp nhận</gui> để thay đổi cần đợi bao lâu từ khi nhấn phím lần đầu trước khi công nhận một phím bấm. Chọn <em>Kêu bíp khi một phím bị từ chối</em> nếu bạn muốn máy tính cảnh báo mỗi lần bỏ qua phím nhấn vì xảy ra quá nhanh.</p>

</page>
