<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="disk-benchmark" xml:lang="nl">

  <info>
    <link type="guide" xref="disk"/>

    <revision pkgversion="3.6.2" version="0.2" date="2012-11-16" status="review"/>
    <revision pkgversion="3.10" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Gnome-documentatieproject</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
   <credit type="editor">
     <name>Michael Hill</name>
     <email>mdhillca@gmail.com</email>
   </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Prestatiemetingen (benchmarks) uitvoeren op uw harde schijf om te controleren hoe snel die is.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

<title>De prestaties van u harde schijf testen</title>

  <p>Om de snelheid van uw harde schijf te testen:</p>

  <steps>
    <item>
      <p>Open <app>Schijven</app> vanuit het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht.</p>
    </item>
    <item>
      <p>Kies de harde schijf uit de lijst in het linker paneel.</p>
    </item>
    <item>
      <p>Klik op de menuknop en selecteer <gui>Schijf benchmarken…</gui> uit het menu.</p>
    </item>
    <item>
      <p>Klik op <gui>Benchmark uitvoeren…</gui> en pas de parameters <gui>Overdrachtsnelheid</gui> en <gui>Toegangstijd</gui> naar wens aan.</p>
    </item>
    <item>
      <p>Klik op <gui>Benchmark uitvoeren</gui> om te testen hoe snel gegevens van de schijf gelezen kunnen worden. Er zijn <link xref="user-admin-explain">Beheerdersrechten</link> voor nodig. Voer uw wachtwoord in, of het wachtwoord voor de beheerdersaccount.</p>
      <note>
        <p>Als <gui>Schrijfbenchmark uitvoeren</gui> is aangevinkt, dan zal er getest worden hoe snel gegevens gelezen van en geschreven naar de schijf kunnen worden. Dit zal langer duren.</p>
      </note>
    </item>
  </steps>

  <p>Wanneer de test klaar is zullen de resultaten verschijnen in de grafiek. De groene stippen en lijnen daartussen geven aan welke steekproeven er genomen zijn; ze komen overeen met de rechteras met de toegangstijd uitgezet tegen de onderste lijn, en stelt de tijd in procenten voor die verstreken is tijdens de meting. De blauwe lijn stelt de leessnelheid voor, en de rode lijn de schrijfsnelheid; deze tonen de gegevenstoegangsnelheid op de linkeras, uitgezet tegen de onderste lijn als percentage van de schijfbeweging van de buitenkant naar de as.</p>

  <p>Onderaan de grafiek worden waarden weergegeven voor de minimale, maximale en gemiddelde lees- en schrijfsnelheid en de tijd die verstreken is sinds de laatste meting.</p>

</page>
