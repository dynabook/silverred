<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="a11y task" id="a11y" xml:lang="ca">

  <info>
    <link type="guide" xref="index" group="a11y"/>

    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc><link xref="a11y#vision">Visió</link>, <link xref="a11y#sound">audició</link>, <link xref="a11y#mobility">mobilitat</link>, <link xref="a11y-braille">braille</link>, <link xref="a11y-mag">ampliador de pantalla</link>…</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

  <title>Accés universal</title>

  <p>L'escriptori del GNOME inclou tecnologies d'assistència per donar suport a usuaris amb limitacions i necessitats especials, i per interactuar amb els dispositius assistents més comuns. Es pot afegir un menú d'accessibilitat a la barra superior per facilitar l'accés a moltes de les funcions d'accessibilitat.</p>

  <section id="vision">
    <title>Discapacitats visuals</title>

    <links type="topic" groups="blind" style="linklist">
      <title>Ceguesa</title>
    </links>
    <links type="topic" groups="lowvision" style="linklist">
      <title>Poca visió</title>
    </links>
    <links type="topic" groups="colorblind" style="linklist">
      <title>Daltonisme</title>
    </links>
    <links type="topic" style="linklist">
      <title>Altres temes</title>
    </links>
  </section>

  <section id="sound">
    <title>Discapacitats auditives</title>
    <links type="topic" style="linklist"/>
  </section>

  <section id="mobility">
    <title>Discapacitats de mobilitat</title>

    <links type="topic" groups="pointing" style="linklist">
      <title>Moviment del ratolí</title>
    </links>
    <links type="topic" groups="clicking" style="linklist">
      <title>Com fer clic i arrossegar</title>
    </links>
    <links type="topic" groups="keyboard" style="linklist">
      <title>Utilització del teclat</title>
    </links>
    <links type="topic" style="linklist">
      <title>Altres temes</title>
    </links>
  </section>
</page>
