<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="tips-specialchars" xml:lang="da">

  <info>
    <link type="guide" xref="tips"/>
    <link type="seealso" xref="keyboard-layouts"/>

    <revision pkgversion="3.8.2" version="0.3" date="2013-05-18" status="review"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.26" date="2017-11-27" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Skriv tegn som ikke findes på dit tastatur. inklusive fremmede alfabeter, matematiske symboler og dingbats.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years/>
    </mal:credit>
  </info>

  <title>Indtast specialtegn</title>

  <p>Du kan indtaste og vise tusindvis af tegn fra de fleste af verdens skrivesystemer — selv dem der ikke findes på dit tastatur. Siden viser nogle forskellige måder til at indtaste specialtegn.</p>

  <links type="section">
    <title>Metoder til at indtaste tegn</title>
  </links>

  <section id="characters">
    <title>Tegn</title>
    <p>GNOME kommer med et tegnkort-program som giver dig mulighed for at finde og indsætte ualmindelige tegn, inklusive emoji, ved at gennemse tegnkategorier eller søge efter nøgleord.</p>

    <p>Du kan starte <app>Tegn</app> fra Aktivitetsoversigten.</p>

  </section>

  <section id="compose">
    <title>Compose-tasten</title>
    <p>En compose-tast er en speciel tast som giver dig mulighed for at trykke på flere taster efter hinanden for at få et specialtegn. For f.eks. at skrive <em>é</em> med accenttegn, kan du trykke på <key>compose</key> og så <key>'</key> og <key>e</key>.</p>
    <p>Tastaturer har ikke en bestemt compose-tast. Du kan i stedet definere en af de eksisterende taster på dit tastatur som en compose-tast.</p>

    <note style="important">
      <p>You need to have <app>Tweaks</app> installed on your computer to
      change this setting.</p>
      <if:if xmlns:if="http://projectmallard.org/if/1.0/" test="action:install">
        <p><link style="button" action="install:gnome-tweaks">Installér <app>Tilpasninger</app></link></p>
      </if:if>
    </note>

    <steps>
      <title>Vælg en compose-tast</title>
      <item>
        <p>Åbn <gui xref="shell-introduction#activities">Aktivitetsoversigten</gui> og begynd at skrive <gui>Tilpasninger</gui>.</p>
      </item>
      <item>
        <p>Klik på <gui>Tilpasninger</gui> for at åbne programmet.</p>
      </item>
      <item>
        <p>Klik på fanebladet <gui>Tastatur &amp; mus</gui>.</p>
      </item>
      <item>
        <p>Klik på <gui>Deaktiveret</gui> ved siden af <gui>Compose-tast</gui>-indstillingen.</p>
      </item>
      <item>
        <p>Tænd for kontakten i dialogen og vælg den tastaturgenvej, du vil bruge.</p>
      </item>
      <item>
        <p>Tilvælg afkrydsningsboksen til den tast du vil indstille som Compose-tast.</p>
      </item>
      <item>
        <p>Luk dialogen.</p>
      </item>
      <item>
        <p>Luk <gui>Tilpasninger</gui>-vinduet.</p>
      </item>
    </steps>

    <p>Du kan skrive mange almindelige tegn med compose-tasten, f.eks.:</p>

    <list>
      <item><p>Tryk på <key>compose</key> og så <key>'</key> og så et bogstav for at sætte et accent aigu over, såsom <em>é</em>.</p></item>
      <item><p>Tryk på <key>compose</key> og så <key>`</key> (omvendt ´) og så et bogstav for at sætte et accent grave over, såsom <em>è</em>.</p></item>
      <item><p>Tryk på <key>compose</key> og så <key>"</key> og så et bogstav for at sætte et umlaut over bogstavet, såsom <em>ë</em>.</p></item>
      <item><p>Tryk på <key>compose</key> og så <key>-</key> og så et bogstav for at sætte en macron over bogstavet, såsom <em>ē</em>.</p></item>
    </list>
    <p>Se <link href="http://en.wikipedia.org/wiki/Compose_key#Common_compose_combinations">Wikipedias side om Compose-tasten</link> for flere sekvenser til Compose-tasten.</p>
  </section>

<section id="ctrlshiftu">
  <title>Kodepunkter</title>

  <p>Du kan indtaste alle Unicode-tegn med dit tastatur ved at bruge tegnets numeriske kodepunkt. Hvert tegn identificeres af et kodepunkt på 4 tegn. Du kan finde et tegns kodepunkt ved at slå det op i <app>Tegn</app>-programmet. Kodepunktet er de fire tegn efter <gui>U+</gui>.</p>

  <p>For at indtaste et tegns kodepunkt skal du trykke på <keyseq><key>Ctrl</key><key>Skift</key><key>U</key></keyseq> og så skrive koden på 4 tegn og trykke på <key>Mellemrum</key> eller <key>Enter</key>. Hvis du ofte bruger tegn som du ikke har let adgang til på andre måder, så kan det være, du vil huske kodepunkterne for de tegn udenad så du hurtigt kan indtaste dem.</p>

</section>

  <section id="layout">
    <title>Tastaturlayouts</title>
    <p>Du kan få et tastatur til at opføre sig som tastaturet på et andet sprog, uanset hvilke bogstaver der er trykt på tasterne. Du kan endda let skifte mellem forskellige tastaturlayouts ved at bruge et ikon på toplinjen. Se <link xref="keyboard-layouts"/> for at læse mere.</p>
  </section>

<section id="im">
  <title>Indtastningsmetoder</title>

  <p>En indtastningsmetode udvider de forrige metoder ved at give mulighed for at indtaste tegn, ikke kun med tastaturet, men alle inputenheder. Du kan f.eks. indtaste tegn med en mus ved at bruge en gestusmetode eller indtaste japanske tegn med et latin-tastatur.</p>

  <p>Højreklik på tekst-widgeten for at vælge en indtastningsmetode, og vælg den indtastningsmetode du vil bruge i <gui>Indtastningsmetode</gui>-menuen. Der leveres ikke nogen standardindtastningsmetode, så referer til dokumentationen for indtastningsmetode for at se hvordan de bruges.</p>

</section>

</page>
