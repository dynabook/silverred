<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="sound-nosound" xml:lang="gl">

  <info>
    <link type="guide" xref="sound-broken"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.18" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Proxecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Check that the sound is not muted, that cables are plugged in properly,
    and that the sound card is detected.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2020</mal:years>
    </mal:credit>
  </info>

<title>Non podo escoitar ningún son do computador</title>

  <p>If you cannot hear any sounds on your computer, for example when you try
  to play music, go through the following troubleshooting tips.</p>

<section id="mute">
  <title>Asegúrese que o son non está desactivado</title>

  <p>Open the <gui xref="shell-introduction#systemmenu">system menu</gui> and make sure that
  the sound is not muted or turned down.</p>

  <p>Some laptops have mute switches or keys on their keyboards — try pressing
  that key to see if it unmutes the sound.</p>

  <p>Tamén pode comprobar que non silenciou a aplicación que está usando para reproducir o son (por exemplo, o seu reprodutor de música ou reprodutor de filmes). A aplicación pode ter un botón de silencio ou de volume na súa xanela principal, polo que debe comprobalo. Tamén pode premer na icona no panel superior e seleccionar <gui>Preferencias de son</gui>. Cando apareza a xanela de <gui>Son</gui>, vaia á lapela <gui>Aplicacións</gui> e comprobe que a súa aplicación non está silenciado.</p>

  <p>Tamén, pode comprobar a lapela de <gui>Aplicacións</gui> na interface de <gui>Son</gui>:</p>
  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
      overview and start typing <gui>Sound</gui>.</p>
    </item>
    <item>
      <p>Prema en <gui>Son</gui> para abrir o panel.</p>
    </item>
    <item>
      <p>Baixo <gui>Niveis de volume</gui>, comprobe que a súa aplicación non está en silencio.</p>
    </item>
  </steps>

</section>

<section id="speakers">
  <title>Comprobe que os altoparlantes están acendidos e están conectados correctamente</title>
  <p>If your computer has external speakers, make sure that they are turned on
  and that the volume is turned up. Make sure that the speaker cable is securely
  plugged into the “output” audio socket on your computer. This socket
  is usually light green in color.</p>

  <p>Some sound cards can switch between the socket they use for output
  (to the speakers) and the socket for input (from a microphone, for instance).
  The output socket may be different when running Linux, Windows or Mac OS.
  Try connecting the speaker cable to a different audio socket on your
  computer.</p>

 <p>A final thing to check is that the audio cable is securely plugged into the
 back of the speakers. Some speakers have more than one input, too.</p>
</section>

<section id="device">
  <title>Comprobe que o dispositivo de son correcto está seleccionado</title>

  <p>Algúns equipos teñen varios «dispositivos de son» instalados. Algúns destes son capaces de facer saír o son e outros non, así que vostede debe comprobar que ten seleccionado o correcto. Isto podería implicar algún ensaio e erro para elixir o correcto.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
      overview and start typing <gui>Sound</gui>.</p>
    </item>
    <item>
      <p>Prema en <gui>Son</gui> para abrir o panel.</p>
    </item>
    <item>
      <p>Under <gui>Output</gui>, change the <gui>Profile</gui> settings for the
      selected device and play a sound to see if it works. You might need to go
      through the list and try each profile.</p>

      <p>Se iso non funciona, pode tentar facer o mesmo con calquera dos dispositivos que se mostran.</p>
    </item>
  </steps>

</section>

<section id="hardware-detected">

 <title>Comprobe que a tarxeta de son foi detectada correctamente</title>

  <p>Se non se detecta a súa tarxeta de son, pode que os controladores da tarxeta non estean instalados. Pode que teña que instalalos manualmente. A forma de facelo dependerá da tarxeta que teña.</p>

  <p>Execute a orde <cmd>lspci</cmd> no Terminal para obter máis información sobre a súa tarxeta de son:</p>
  <steps>
    <item>
      <p>Vaia á vista previa de <gui>Actividades</gui> e abra un terminal.</p>
    </item>
    <item>
      <p>Run <cmd>lspci</cmd> as <link xref="user-admin-explain">superuser</link>;
      either type <cmd>sudo lspci</cmd> and type your password, or type
      <cmd>su</cmd>, enter the <em>root</em> (administrative) password,
      then type <cmd>lspci</cmd>.</p>
    </item>
    <item>
      <p>Check if an <em>audio controller</em> or <em>audio device</em> is listed:
      in such case you should see the make and model number of the sound card. 
      Also, <cmd>lspci -v</cmd> shows a list with more detailed information.</p>
    </item>
  </steps>

  <p>Pode que encontre e saiba instalar os controladores para a súa tarxeta. O mellor é pedir instrucións nos foros de soporte (ou similares) para a súa distribución de Linux.</p>

  <p>Se non pode conseguir os controladores para a tarxeta de son, é posíbel que prefira comprar unha tarxeta de son nova. Pode obter tarxetas de son que se poden instalar no equipo e tarxetas de son externas USB.</p>

</section>

</page>
