<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-browse" xml:lang="pl">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>
    <link type="seealso" xref="files-copy"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-16" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Zarządzanie i organizowanie plików za pomocą menedżera plików.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  </info>

<title>Przeglądanie plików i katalogów</title>

<p>Używaj menedżera plików <app>Pliki</app> do przeglądania i organizowania plików na komputerze. Można go także używać do zarządzania plikami na urządzeniach do przechowywania danych (takich jak zewnętrzne dyski twarde), na <link xref="nautilus-connect">serwerach plików</link> i na zasobach sieciowych.</p>

<p>Aby uruchomić menedżera plików, otwórz <app>Pliki</app> na <gui xref="shell-introduction#activities">ekranie podglądu</gui>. Można także wyszukiwać pliki i katalogi na ekranie podglądu w ten sam sposób, co <link xref="shell-apps-open">programy</link>.</p>

<section id="files-view-folder-contents">
  <title>Przeglądanie zawartości katalogów</title>

<p>W menedżerze plików podwójnie kliknij dowolny katalog, aby wyświetlić jego zawartość, i podwójnie kliknij (lub kliknij <link xref="mouse-middleclick">środkowym przyciskiem myszy</link>) dowolny plik, aby otworzyć go w domyślnym programie dla danego typu pliku. Kliknij katalog środkowym przyciskiem myszy, aby otworzyć go w nowej karcie. Można także kliknąć katalog prawym przyciskiem myszy, aby otworzyć go w nowej karcie lub oknie.</p>

<p>Podczas przeglądania plików w katalogu można szybko <link xref="files-preview">podejrzeć każdy plik</link> naciskając spację, aby upewnić się, że to właściwy plik przed jego otwarciem, skopiowaniem lub usunięciem.</p>

<p><em>Pasek ścieżki</em> nad listą plików i katalogów wyświetla przeglądany katalog razem z jego katalogami nadrzędnymi. Kliknij jeden z nich, aby do niego przejść. Kliknij dowolny katalog na pasku ścieżki prawym przyciskiem myszy, aby otworzyć go w nowej karcie lub oknie, albo wyświetlić jego właściwości.</p>

<p>Aby szybko <link xref="files-search">wyszukać plik</link> w przeglądanym katalogu lub jego katalogach podrzędnych, zacznij pisać jego nazwę. Na górze okna pojawi się <em>pasek wyszukiwania</em>, a wyświetlane będą tylko pliki pasujące do wyszukiwanej frazy. Naciśnij klawisz <key>Esc</key>, aby anulować wyszukiwanie.</p>

<p>Za pomocą <em>panelu bocznego</em> można szybko przechodzić do często używanych miejsc. Jeśli nie jest on widoczny, to kliknij przycisk menu w górnym prawym rogu okna, a następnie zaznacz <gui>Panel boczny</gui>. Można dodać zakładki do często używanych katalogów, które będą wyświetlane na panelu bocznym. Przeciągnij na niego katalog i upuść go na pole <gui>Nowa zakładka</gui>, które pojawi się automatycznie, lub kliknij nazwę obecnego katalogu na pasku ścieżki i wybierz <gui style="menuitem">Dodaj zakładkę</gui>.</p>

</section>

</page>
