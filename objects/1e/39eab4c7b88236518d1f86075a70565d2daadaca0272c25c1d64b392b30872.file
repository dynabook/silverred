<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wired-connect" xml:lang="ca">

  <info>
    <link type="guide" xref="net-wired" group="#first"/>

    <revision pkgversion="3.4" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.28" date="2018-03-28" status="review"/>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Per configurar la majoria de les connexions de xarxa per cable, tot el que cal fer és connectar un cable de xarxa.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

  <title>Connectar a una xarxa de cable (Ethernet)</title>

  <!-- TODO: create icon manually because it is one overlayed on top of another
       in real life. -->
  <p>Per configurar la majoria de les connexions de xarxa per cable, tot el que cal fer és connectar un cable de xarxa. La icona de la xarxa cablejada (<media its:translate="no" type="image" src="figures/network-wired-symbolic.svg"><span its:translate="yes">configuració</span></media>) es mostra a la barra superior amb tres punts mentre s'estableix la connexió. Els punts desapareixen quan està connectat.</p>

  <p>Si això no passa, primer haureu d'assegurar-vos que el cable de xarxa estigui connectat. Un extrem del cable ha de connectar-se al port Ethernet rectangular (xarxa) de l'ordinador i l'altre extrem hauria de connectar-se a un interruptor, encaminador, endoll de xarxa o similar (depenent de la configuració de xarxa que tingueu). De vegades, una llum al costat del port Ethernet indica que està connectat i actiu.</p>

  <note>
    <p>No podeu connectar un ordinador directament a una altra amb un cable de xarxa (si més no sense una configuració addicional). Per connectar dos ordinadors, haureu de connectar-les a un concentrador de xarxa, a un encaminador o a un interruptor.</p>
  </note>

  <p>Si encara no esteu connectat, pot ser que la vostra xarxa no admeti la configuració automàtica (DHCP). En aquest cas haureu de <link xref="net-manual">configurar-ho manualment</link>.</p>

</page>
