<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-name-location" xml:lang="cs">

  <info>
    <link type="guide" xref="printing#setup"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author copyright">
      <name>Jana Švárová</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak změnit název nebo umístění tiskárny v nastaveních tiskárny.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>
  
  <title>Změna názvu nebo umístění tiskárny</title>

  <p>Můžete změnit název nebo umístění tiskárny v nastaveních tiskárny.</p>

  <note>
    <p>Abyste mohli změnit název a umístění tiskárny, potřebujete v systému <link xref="user-admin-explain">oprávnění správce</link>.</p>
  </note>

  <section id="printer-name-change">
    <title>Změna názvu tiskárny</title>

  <p>Když chcete změnit název tiskárny, postupujte v následujících krocích:</p>

  <steps>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Tiskárny</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Tiskárny</gui> otevřete příslušný panel.</p>
    </item>
    <item>
      <p>Zmáčkněte <gui style="button">Odemknout</gui> v pravém horním rohu a po vyzvání zadejte heslo.</p>
    </item>
    <item>
      <p>Klikněte na název tiskárny a napište její nový název.</p>
    </item>
    <item>
      <p>Zmáčknutím <key>Enter</key> své změny uložte.</p>
    </item>
  </steps>

  </section>

  <section id="printer-location-change">
    <title>Změna umístění tiskárny</title>

  <p>Když chcete změnit umístění tiskárny:</p>

  <steps>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Tiskárny</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Tiskárny</gui> otevřete příslušný panel.</p>
    </item>
    <item>
      <p>Zmáčkněte <gui style="button">Odemknout</gui> v pravém horním rohu a po vyzvání zadejte heslo.</p>
    </item>
    <item>
      <p>Klikněte na umístění a upravte jej.</p>
    </item>
    <item>
      <p>Zmáčknutím <key>Enter</key> změny uložte.</p>
    </item>
  </steps>

  </section>

</page>
