<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-use-windows-workspaces" xml:lang="lv">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>
    <link type="guide" xref="getting-started" group="tasks"/>
    <title role="trail" type="link">Izmantot logus un darbvietas</title>
    <link type="seealso" xref="shell-windows-switching"/>
    <title role="seealso" type="link">Ceļvedis logu un darbvietu izmantošanā</title>
    <link type="next" xref="gs-use-system-search"/>
  </info>

  <title>Izmantot logus un darbvietas</title>

  <ui:overlay width="812" height="452">
  <media type="video" its:translate="no" src="figures/gnome-windows-and-workspaces.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-windows-and-workspaces.svg"/>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>Logi un darbvietas</tt:p>
         </tt:div>
         <tt:div begin="6s" end="10s">
           <tt:p>Lai maksimizētu logu, satveriet loga virsraksta joslu un velciet to uz ekrāna augšpusi.</tt:p>
           </tt:div>
         <tt:div begin="10s" end="13s">
           <tt:p>Kad ekrāns ir izgaismots, atlaidiet logu.</tt:p>
         </tt:div>
         <tt:div begin="14s" end="20s">
           <tt:p>Lai atjaunotu logu tā iepriekšējā izmērā, satveriet loga virsraksta joslu un velciet to prom no ekrāna malām.</tt:p>
         </tt:div>
         <tt:div begin="25s" end="29s">
           <tt:p>Lai atjaunotu logu, to varat arī satvert augšējo joslu un vilkt logu no tā projām.</tt:p>
         </tt:div>
         <tt:div begin="34s" end="38s">
           <tt:p>Lai maksimizētu logu gar kreiso pusi, satveriet loga virsraksta joslu un velciet to uz ekrāna kreiso pusi.</tt:p>
         </tt:div>
         <tt:div begin="38s" end="40s">
           <tt:p>Kad puse ekrāna ir izgaismota, atlaidiet logu.</tt:p>
         </tt:div>
         <tt:div begin="41s" end="44s">
           <tt:p>Lai maksimizētu logu gar labo pusi, satveriet loga virsraksta joslu un velciet to uz ekrāna labo pusi.</tt:p>
         </tt:div>
         <tt:div begin="44s" end="48s">
           <tt:p>Kad puse ekrāna ir izgaismota, atlaidiet logu.</tt:p>
         </tt:div>
         <tt:div begin="54s" end="60s">
           <tt:p>Lai maksimizētu logu ar tastatūru, turiet piespiestu taustiņu <key href="help:gnome-help/keyboard-key-super">Super</key> un spiediet <key>↑</key>.</tt:p>
         </tt:div>
         <tt:div begin="61s" end="66s">
           <tt:p>Lai atjaunotu logu uz tā iepriekšējo izmēr ar tastatūru, turiet piespiestu taustiņu <key href="help:gnome-help/keyboard-key-super">Super</key> un spiediet <key>↓</key>.</tt:p>
         </tt:div>
         <tt:div begin="66s" end="73s">
           <tt:p>Lai maksimizētu logu ar gar ekrāna labo malu, turiet piespiestu taustiņu <key href="help:gnome-help/keyboard-key-super">Super</key> un spiediet <key>→</key>.</tt:p>
         </tt:div>
         <tt:div begin="76s" end="82s">
           <tt:p>Lai maksimizētu logu ar gar kreiso labo malu, turiet piespiestu taustiņu <key href="help:gnome-help/keyboard-key-super">Super</key> un spiediet <key>←</key>.</tt:p>
         </tt:div>
         <tt:div begin="83s" end="89s">
           <tt:p>Lai pārvietotu darbvietu zem pašreizējās darbvietas, spiediet <keyseq><key href="help:gnome-help/keyboard-key-super">Super</key> <key>Page Down</key></keyseq>.</tt:p>
         </tt:div>
         <tt:div begin="90s" end="97s">
           <tt:p>Lai pārvietotu darbvietu virs pašreizējās darbvietas, spiediet <keyseq><key href="help:gnome-help/keyboard-key-super">Super</key> <key>Page Up</key></keyseq>.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>
  
  <section id="use-workspaces-and-windows-maximize">
    <title>Maksimizēt logu un atjaunot tā izmēru</title>
    <p/>
    
    <steps>
      <item><p>Lai maksimizētu logu, lai tas aizņemtu visu darbvietu, satveriet loga virsraksta joslu un velciet to uz ekrāna augšpusi.</p></item>
      <item><p>Kad ekrāns ir izgaismots, atlaidiet logu, lai to maksimizētu.</p></item>
      <item><p>Lai atjaunotu logu tā iepriekšējā izmērā, satveriet loga virsraksta joslu un velciet to prom no ekrāna malām.</p></item>
    </steps>
    
  </section>

  <section id="use-workspaces-and-windows-tile">
    <title>Izklāt logus</title>
    <p/>
    
    <steps>
      <item><p>Lai maksimizētu logu gar ekrāna malu, satveriet loga virsraksta joslu un velciet to uz ekrāna kreiso vai labo pusi.</p></item>
      <item><p>Kad puse ekrāna ir izgaismota, atlaidiet logu, lai maksimizētu to gar izvēlēto ekrāna malu.</p></item>
      <item><p>Lai maksimizētu logus vienu pie otra, satveriet otra loga virsraksta joslu un velciet to uz ekrāna otru pusi.</p></item>
       <item><p>Kad puse ekrāna ir izgaismota, atlaidiet logu, lai maksimizētu to gar otru ekrāna malu.</p></item>
    </steps>
    
  </section>
  
  <section id="use-workspaces-and-windows-maximize-keyboard">
    <title>Maksimizēt logu un atjaunot tā izmēru, izmantojot tastatūru</title>
    
    <steps>
      <item><p>Lai maksimizētu logu ar tastatūru, turiet piespiestu taustiņu <key href="help:gnome-help/keyboard-key-super">Super</key> un spiediet <key>↑</key>.</p></item>
      <item><p>Lai atjaunotu logu tā iepriekšējā izmērā ar tastatūru, turiet piespiestu taustiņu <key href="help:gnome-help/keyboard-key-super">Super</key> un spiediet <key>↓</key>.</p></item>
    </steps>
    
  </section>
  
  <section id="use-workspaces-and-windows-tile-keyboard">
    <title>Izklāt logus, izmantojot tastatūru</title>
    
    <steps>
      <item><p>Lai maksimizētu logu ar gar ekrāna labo malu, turiet piespiestu taustiņu <key href="help:gnome-help/keyboard-key-super">Super</key> un spiediet <key>→</key>.</p></item>
      <item><p>Lai maksimizētu logu ar gar kreiso labo malu, turiet piespiestu taustiņu <key href="help:gnome-help/keyboard-key-super">Super</key> un spiediet <key>←</key>.</p></item>
    </steps>
    
  </section>
  
  <section id="use-workspaces-and-windows-workspaces-keyboard">
    <title>Pārslēgties starp darbvietām ar tastatūru</title>
    
    <steps>
    
    <item><p>Lai pārvietotu darbvietu zem pašreizējās darbvietas, spiediet <keyseq><key href="help:gnome-help/keyboard-key-super">Super</key> <key>Page Down</key></keyseq>.</p></item>
    <item><p>Lai pārvietotu darbvietu virs pašreizējās darbvietas, spiediet <keyseq><key href="help:gnome-help/keyboard-key-super">Super</key> <key>Page Up</key></keyseq>.</p></item>

    </steps>
    
  </section>

</page>
