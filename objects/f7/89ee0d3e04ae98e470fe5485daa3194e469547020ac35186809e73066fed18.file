<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="sound-volume" xml:lang="el">

  <info>
    <link type="guide" xref="media#sound"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ορίστε την ένταση του ήχου για τον υπολογιστή και ελέγξτε την ακουστότητα κάθε εφαρμογής.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

<title>Αλλαγή έντασης του ήχου</title>

  <p>To change the sound volume, open the
  <gui xref="shell-introduction#systemmenu">system menu</gui> from the right
  side of the top bar and move the volume slider left or right. You can
  completely turn off sound by dragging the slider to the left.</p>

  <p>Some keyboards have keys that let you control the volume. They normally
  look like stylized speakers with waves coming out of them. They are often
  near the “F” keys at the top. On laptop keyboards, they are usually on the
  “F” keys.  Hold down the <key>Fn</key> key on your keyboard to use them.</p>

  <p>If you have external speakers, you can also change the volume
  using the speakers’ volume control. Some headphones have a
  volume control too.</p>

<section id="apps">
 <title>Αλλαγή της έντασης ήχου για μεμονωμένες εφαρμογές</title>

  <p>Μπορείτε να αλλάξετε την ένταση μιας εφαρμογής και να την αφήσετε αμετάβλητη για τις άλλες. Αυτό είναι χρήσιμο αν ακούτε μουσική και περιηγείστε στον ιστό. Μπορεί να θέλετε να κλείσετε την ένταση στον περιηγητή ιστού, έτσι ώστε οι ήχοι από τους ιστοτόπους να μην διακόπτουν τη μουσική.</p>

  <p>Μερικές εφαρμογές διαθέτουν ολισθητές έντασης στο κύριο παράθυρο τους. Αν η εφαρμογή σας διαθέτει έναν, χρησιμοποιήστε τον. Αν όχι:</p>

    <steps>
    <item>
      <p>Ανοίξτε την επισκόπηση <gui xref="shell-introduction#activities">Δραστηριότητες</gui> και αρχίστε να πληκτρολογείτε <gui>Ήχος</gui>.</p>
    </item>
    <item>
      <p>Κάντε κλικ στο <gui>Ήχος</gui> για να ανοίξετε τον πίνακα.</p>
    </item>
    <item>
      <p>Under <gui>Volume Levels</gui>, change the volume of the application
      listed there.</p>

  <note style="tip">
    <p>Μόνο εφαρμογές που αναπαράγουν ήχους θα εμφανιστούν. Αν μια εφαρμογή αναπαράγει ήχους αλλά δεν είναι καταχωρισμένη, μπορεί να μην υποστηρίξει το γνώρισμα που σας επιτρέπει να ελέγξετε την ένταση Σε αυτήν την περίπτωση, δεν μπορείτε να αλλάξετε την έντασή του.</p>
  </note>
    </item>

  </steps>

</section>

</page>
