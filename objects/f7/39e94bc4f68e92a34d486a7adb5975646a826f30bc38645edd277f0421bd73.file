<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-install-flash" xml:lang="ta">

  <info>
    <link type="guide" xref="net-browser"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>ஃபில் புல்</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>வீடியோக்கள் மற்றும் ஊடாடும் வலைப் பக்கங்களை காண்பிக்கும் YouTube போன்ற வலைத்தளங்களைக் காண Flash ஐ நிறுவ வேண்டும்.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>Flash செருகுநிரலை நிறுவுதல்</title>

  <p><app>Flash</app> is a <em>plug-in</em> for your web browser that allows
  you to watch videos and use interactive web pages on some websites. Some
  websites won’t work without Flash.</p>

  <p>உங்கள் கணினியில் Flash நிறுவியிருக்காவிட்டால், அதைக் கூறும் ஒரு செய்தி காண்பிக்கப்படலாம். பெரும்பாலான வலை உலாவிகளுக்கு Flash இலவசமாக பதிவிறக்கக் கிடைக்கிறது (ஆனால் திறமூலமல்ல). பெரும்பாலான Linux விநியோகங்களில் Flash இன் ஒரு பதிப்பு இருக்கும், அதை நீங்கள் அவற்றின் மென்பொருள் நிறுவி (தொகுப்பு நிர்வாகி) மூலமும் நிறுவலாம்.</p>

  <steps>
    <title>மென்பொருள் நிறுவியில் Flash இருந்தால்:</title>
    <item>
      <p>மென்பொருள் நிறுவி பயன்பாட்டைத் திறந்து <input>Flash</input> ஐத் தேடவும்.</p>
    </item>
    <item>
      <p><gui>Adobe Flash செருகு நிரல்</gui>, <gui>Adobe Flash Player</gui> அல்லது அது போன்ற உருப்படியைக் கண்டறிந்து அதை நிறுவ அதை சொடுக்கவும்.</p>
    </item>
    <item>
      <p>வலை உலாவி சாளரங்கள் ஏதேனும் திறந்திருந்தால், அவற்றை மூடிவிட்டு மீண்டும் திறக்கவும். அதன் பின் வலை உலாவியை நீங்கள் திறக்கும் போது Flash நிறுவப்பட்டுள்ளது என்று வலை உலாவி அடையாளம் காண முடியும், நீங்கள் Flash ஐப் பயன்படுத்தி வலைப்பக்கங்களைக் காண முடியும்.</p>
    </item>
  </steps>

  <steps>
    <title>மென்பொருள் நிறுவியில் Flash <em>கிடைக்காவிட்டால்</em>:</title>
    <item>
      <p><link href="http://get.adobe.com/flashplayer">Flash Player பதிவிறக்க வலைத்தளத்திற்கு</link> செல்லவும். உங்கள் உலாவியும் இயக்க முறைமையும் தானாக கண்டறியப்படும்.</p>
    </item>
    <item>
      <p>Click where it says <gui>Select version to download</gui> and choose
      the type of software installer that works for your Linux distribution. If
      you don’t know which to use, choose the <file>.tar.gz</file> option.</p>
    </item>
    <item>
      <p>உங்கள் வலை உலாவிக்கு Flash ஐ எப்படி நிறுவ வேண்டும் என அறிய <link href="http://kb2.adobe.com/cps/153/tn_15380.html">Flash க்கான நிறுவல் வழிமுறைகள்</link> க்குச் செல்லவும்.</p>
    </item>
  </steps>

<section id="alternatives">
  <title>Flash க்கான மாற்று திறமூல மென்பொருள்</title>

  <p>Flash க்கான இலவச திற மூல மாற்று மென்பொருள் பல உள்ளன. அவை சில விதத்தில் Flash செருகு நிரலை விட சிறப்பாகவே வேலை செய்யக்கூடும் (உதாரணமாக ஒலி இயக்கத்தை இன்னும் சிறப்பாகக் கையாளலாம்). அதே சமயம் அவை சில அம்சங்களில் மோசமாக இருக்கலாம் (உதாரணமாக, இணையத்தில் உள்ள மிகவும் சிக்கலான Flash பக்கங்கள் சிலவற்றை ஒழுங்காகக் காட்ட முடியாது போகலாம்).</p>

  <p>உங்களுக்கு Flash பிளேயர் திருப்தியாக இல்லாவிட்டால் அல்லது நீங்கள் உங்கள் கணினியில் முடிந்தவரை திற மூல மென்பொருளையே பயன்படுத்த விரும்பினால் நீங்கள் இவற்றில் ஒன்றை முயற்சி செய்து பார்க்கலாம். அவற்றுள் சில இங்கே கொடுக்கப்பட்டுள்ளன:</p>

  <list style="compact">
    <item>
      <p>LightSpark</p>
    </item>
    <item>
      <p>Gnash</p>
    </item>
  </list>

</section>

</page>
