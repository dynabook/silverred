<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-touchpad-click" xml:lang="gu">

  <info>
    <link type="guide" xref="mouse"/>

    <revision pkgversion="3.7" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-29" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>માઇકલ હીલ</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>ઍકાટેરીના ગેરાસીમોવા</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013, 2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Click, drag, or scroll using taps and gestures on your
    touchpad.</desc>
  </info>

  <title>ટચપેડ સાથે ક્લિક, ખેંચો, અથવા સ્ક્રોલ  કરો</title>

  <p>અલગ હાર્ડવેર બટનો વગર  ફક્ત તમારાં ટચપેડની મદદથી તમે ક્લિક, બે વાર ક્લિક, ખેંચી, અને સ્ક્રોલ કરી શકે છે.</p>

  <note>
    <p><link xref="touchscreen-gestures">Touchscreen gestures</link> are
    covered separately.</p>
  </note>

<section id="tap">
  <title>Tap to click</title>

  <p>You can tap your touchpad to click instead of using a button.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Mouse &amp; Touchpad</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Mouse &amp; Touchpad</gui> to open the panel.</p>
    </item>
    <item>
      <p>In the <gui>Touchpad</gui> section, make sure the <gui>Touchpad</gui>
      switch is set to on.</p>
      <note>
        <p><gui>ટચપેડ</gui> વિભાગ ફક્ત દેખાય છે જો તમારી સિસ્ટમ પાસે ટચપેડ હોય.</p>
      </note>
    </item>
   <item>
      <p>Switch the <gui>Tap to click</gui> switch to on.</p>
    </item>
  </steps>

  <list>
    <item>
      <p>ક્લિક કરવા માટે, ટચપેડ પર ટેપ કરો.</p>
    </item>
    <item>
      <p>બે વાર ક્લિક કરવા માટે, બે વાર ટેપ કરો.</p>
    </item>
    <item>
      <p>To drag an item, double-tap but don’t lift your finger after the
      second tap. Drag the item where you want it, then lift your finger to
      drop.</p>
    </item>
    <item>
      <p>If your touchpad supports multi-finger taps, right-click by tapping
      with two fingers at once. Otherwise, you still need to use hardware
      buttons to right-click. See <link xref="a11y-right-click"/> for a method
      of right-clicking without a second mouse button.</p>
    </item>
    <item>
      <p>If your touchpad supports multi-finger taps,
      <link xref="mouse-middleclick">middle-click</link> by tapping with three
      fingers at once.</p>
    </item>
  </list>

  <note>
    <p>When tapping or dragging with multiple fingers, make sure your fingers
    are spread far enough apart. If your fingers are too close, your computer
    may think they’re a single finger.</p>
  </note>

</section>

<section id="twofingerscroll">
  <title>બે આંગળીથી સ્ક્રોલ</title>

  <p>તમે બે આંગળીની મદદથી તમારાં ટચપેડને સ્ક્રોલ કરી શકો છો.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Mouse &amp; Touchpad</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Mouse &amp; Touchpad</gui> to open the panel.</p>
    </item>
    <item>
      <p>In the <gui>Touchpad</gui> section, make sure the <gui>Touchpad</gui>
      switch is set to on.</p>
    </item>
    <item>
      <p>Switch the <gui>Two-finger Scrolling</gui> switch to on.</p>
    </item>
  </steps>

  <p>When this is selected, tapping and dragging with one finger will work as
  normal, but if you drag two fingers across any part of the touchpad, it will
  scroll instead. Move your fingers between the top and bottom of your touchpad
  to scroll up and down, or move your fingers across the touchpad to scroll
  sideways. Be careful to space your fingers a bit apart. If your fingers are
  too close together, they just look like one big finger to your touchpad.</p>

  <note>
    <p>બે આંગળીથી સ્ક્રોલીંગ એ બધા ટચપેડ પર કામ કરતુ નથી.</p>
  </note>

</section>

<section id="contentsticks">
  <title>Natural scrolling</title>

  <p>તમે સમાવિષ્ટને ખેંચી શકો છો જો ટચપેડની મદદથી પેપરનાં ભૌતિક ભાગને સ્લાઇડ કરી રહ્યા હોય.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Mouse &amp; Touchpad</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Mouse &amp; Touchpad</gui> to open the panel.</p>
    </item>
    <item>
      <p>In the <gui>Touchpad</gui> section, make sure that the
     <gui>Touchpad</gui> switch is set to on.</p>
    </item>
    <item>
      <p>Switch the <gui>Natural Scrolling</gui> switch to on.</p>
    </item>
  </steps>

  <note>
    <p>This feature is also known as <em>Reverse Scrolling</em>.</p>
  </note>

</section>

</page>
