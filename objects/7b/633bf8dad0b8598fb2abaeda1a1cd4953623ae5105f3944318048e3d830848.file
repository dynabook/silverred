<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="keyboard-nav" xml:lang="hu">
  <info>
    <link type="guide" xref="keyboard" group="a11y"/>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>

    <revision pkgversion="3.7.5" version="0.2" date="2013-02-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.20" date="2016-08-13" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-27" status="review"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
       <name>Julita Inca</name>
       <email>yrazes@gmail.com</email>
    </credit>
    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
       <name>Ekaterina Gerasimova</name>
       <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Alkalmazások és az asztal használata egér nélkül.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Billentyűzetnavigáció</title>

  <p>Ez az oldal részletezi a billentyűnavigációt azok számára, akik nem tudnak egeret vagy más mutatóeszközt használni, vagy csak a lehető legtöbbet szeretnék használni a billentyűzetet. A minden felhasználó számára hasznos gyorsbillentyűkkel kapcsolatban nézze meg a <link xref="shell-keyboard-shortcuts"/> oldalt.</p>

  <note style="tip">
    <p>Ha nem tudja használni az egérhez hasonló mutatóeszközöket, akkor az egérmutatót irányíthatja a numerikus billentyűzetről is. A részletekért lásd az <link xref="mouse-mousekeys"/> oldalt.</p>
  </note>

<table frame="top bottom" rules="rows">
  <title>Navigáció felhasználói felületeken</title>
  <tr>
    <td><p><key>Tab</key> és</p>
    <p><keyseq><key>Ctrl</key><key>Tab</key></keyseq></p>
    </td>
    <td>
      <p>A billentyűfókusz mozgatása különböző vezérlőelemek között. A <keyseq><key>Ctrl</key> <key>Tab</key></keyseq> vezérlőelemek csoportjai között mozog, például egy oldalsávról a fő tartalomra. A <keyseq><key>Ctrl</key><key>Tab</key></keyseq> képes kitörni az olyan vezérlőelemekből, amelyek maguk is használják a <key>Tab</key> billentyűt, mint például a szövegbeviteli mező.</p>
      <p>Tartsa lenyomva a <key>Shift</key> billentyűt a fókusz visszafelé mozgatásához.</p>
    </td>
  </tr>
  <tr>
    <td><p>Nyílbillentyűk</p></td>
    <td>
      <p>Kijelölés mozgatása egyetlen vezérlőelem részei között, vagy kapcsolódó vezérlőelemek halmazában. A nyílbillentyűk segítségével mozgathatja a fókuszt az eszköztáron belül, kiválaszthat elemeket egy listából vagy ikonnézetből, vagy egy választógombot egy csoportból.</p>
    </td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key>Nyílbillentyűk</keyseq></p></td>
    <td><p>Listában vagy ikonnézetben a billentyűzetfókusz másik elemre mozgatása a kijelölt elem megváltoztatása nélkül.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Shift</key>Nyílbillentyűk</keyseq></p></td>
    <td><p>Listában vagy ikonnézetben az összes elem kiválasztása a jelenleg kijelölt elemtől az újonnan fókuszba került elemig.</p>
    <p>Fanézetben a gyermekekkel rendelkező elemek kibonthatók vagy összecsukhatók a gyermekek megjelenítéséhez vagy elrejtéséhez: a kibontáshoz nyomja meg a <keyseq><key>Shift</key><key>→</key></keyseq>, az összecsukáshoz a <keyseq><key>Shift</key><key>←</key></keyseq> billentyűkombinációt.</p></td>
  </tr>
  <tr>
    <td><p><key>Szóköz</key></p></td>
    <td><p>Fókuszban lévő elem, például gomb, jelölőnégyzet vagy listaelem aktiválása.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>Szóköz</key></keyseq></p></td>
    <td><p>Lista- vagy ikonnézetben a fókuszban lévő elem kijelölése vagy kijelölésének megszüntetése a többi elem kijelölésének megszüntetése nélkül.</p></td>
  </tr>
  <tr>
    <td><p><key>Alt</key></p></td>
    <td><p>Tartsa lenyomva az <key>Alt</key> billentyűt a <em>hívóbetűk</em> megjelenítéséhez: ezek a menüelemek, gombok és más vezérlőelemek aláhúzott betűi. Nyomja meg az <key>Alt</key> billentyűt és aláhúzott betűt a vezérlőelem aktiválásához, mintha csak rákattintott volna.</p></td>
  </tr>
  <tr>
    <td><p><key>Esc</key></p></td>
    <td><p>Kilépés menüből, felugró, váltó vagy párbeszédablakból.</p></td>
  </tr>
  <tr>
    <td><p><key>F10</key></p></td>
    <td><p>Megnyitja egy ablak menüsorának első menüjét. A menük közti navigációhoz használja a nyílbillentyűket.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key xref="keyboard-key-super">Super</key> <key>F10</key></keyseq></p></td>
    <td><p>Nyissa meg az alkalmazásmenüt a felső sávon.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Shift</key><key>F10</key></keyseq> vagy</p>
    <p><key xref="keyboard-key-menu">Menü</key></p></td>
    <td>
      <p>Az aktuális kijelölés helyi menüjének megjelenítése, mintha a jobb egérgombbal kattintott volna.</p>
    </td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>F10</key></keyseq></p></td>
    <td><p>A fájlkezelőben az aktuális mappa helyi menüjének megjelenítése, mintha a jobb egérgombbal kattintott volna a háttéren.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>PageUp</key></keyseq></p>
    <p>és</p>
    <p><keyseq><key>Ctrl</key><key>PageDown</key></keyseq></p></td>
    <td><p>Lapokat használó felületen a balra vagy jobbra lévő lapra váltás.</p></td>
  </tr>
</table>

<table frame="top bottom" rules="rows">
  <title>Navigáció az asztalon</title>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="alt-f1"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-tab"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-tick"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="ctrl-alt-tab"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-updown"/>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F6</key></keyseq></p></td>
    <td><p>Váltás ugyanazon alkalmazás ablakai közt. Tartsa lenyomva az <key>Alt</key> billentyűt, majd nyomja meg az <key>F6</key> billentyűt, amíg a kívánt ablakra nem kerül a kiemelés, majd engedje el az <key>Alt</key> billentyűt. Ez hasonló az <keyseq><key>Alt</key><key>`</key></keyseq> (magyar billentyűzeteken: a <key>Tab</key> fölötti <key>0</key>) szolgáltatáshoz.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>Esc</key></keyseq></p></td>
    <td><p>Váltás egy munkaterület összes nyitott ablaka között.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>V</key></keyseq></p></td>
    <td><p><link xref="shell-notifications#notificationlist">Az értesítéslista megnyitása.</link> A bezárásához nyomja meg az <key>Esc</key> billentyűt.</p></td>
  </tr>
</table>

<table frame="top bottom" rules="rows">
  <title>Navigáció az ablakok közt</title>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F4</key></keyseq></p></td>
    <td><p>A jelenlegi ablak bezárása.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F5</key></keyseq> vagy <keyseq><key>Super</key><key>↓</key></keyseq></p></td>
    <td><p>Teljes méretű ablak visszaállítása az eredeti méretre. Az <keyseq><key>Alt</key><key>F10</key></keyseq> teljes méretűvé teszi az ablakot. Az <keyseq><key>Alt</key><key>F10</key></keyseq> teljes méretűvé teszi és vissza is állítja az ablakot.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F7</key></keyseq></p></td>
    <td><p>Az aktuális ablak áthelyezése. Az <keyseq><key>Alt</key><key>F7</key></keyseq> lenyomása után használja a nyílbillentyűket az ablak áthelyezésére. Az <key>Enter</key> lenyomása befejezi az ablak áthelyezését, vagy az <key>Esc</key> visszaállítja az eredeti helyére.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F8</key></keyseq></p></td>
    <td><p>Az aktuális ablak átméretezése. Az <keyseq><key>Alt</key><key>F8</key></keyseq> lenyomása után használja a nyílbillentyűket az ablak átméretezésére. Az <key>Enter</key> lenyomása befejezi az ablak átméretezését, vagy az <key>Esc</key> visszaállítja az eredeti méretére.</p></td>
  </tr>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="shift-super-updown"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="shift-super-left"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="shift-super-right"/>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F5</key></keyseq> vagy <keyseq><key>Super</key><key>↓</key></keyseq></p>
    </td>
    <td><p>Ablak <link xref="shell-windows-maximize">teljes méretűvé</link> tétele. Nyomja meg az <keyseq><key>Alt</key><key>F10</key></keyseq> vagy a <keyseq><key>Super</key><key>↓</key></keyseq> kombinációt a teljes méretű ablak visszaállításához az eredeti méretre.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>H</key></keyseq></p></td>
    <td><p>Ablak minimalizálása.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>←</key></keyseq></p></td>
    <td><p>Ablak függőleges maximalizálása a képernyő bal éle mentén. Nyomja meg újra az ablak visszaállításához az eredeti méretére. Az oldalak közti váltáshoz nyomja meg a <keyseq><key>Super</key><key>→</key></keyseq> billentyűkombinációt.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>→</key></keyseq></p></td>
    <td><p>Ablak függőleges maximalizálása a képernyő jobb éle mentén. Nyomja meg újra az ablak visszaállításához az eredeti méretére. Az oldalak közti váltáshoz nyomja meg a <keyseq><key>Super</key><key>←</key></keyseq> billentyűkombinációt.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>Szóköz</key></keyseq></p></td>
    <td><p>Az ablak menüjének megjelenítése, mintha a jobb egérgombbal kattintott volna a címsorra.</p></td>
  </tr>
</table>

</page>
