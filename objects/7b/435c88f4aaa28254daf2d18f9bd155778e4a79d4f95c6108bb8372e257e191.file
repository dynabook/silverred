<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="fs-device" xml:lang="sv">

  <info>
    <revision version="0.1" date="2014-01-26" status="review"/>
    <link type="guide" xref="index#filesystems" group="filesystems"/>
    <link type="seealso" xref="fs-info"/>
    
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    
    <credit type="author copyright">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
      <years>2014</years>
    </credit>

    <desc>Varje enhet motsvarar en <em>partition</em> på en hårddisk.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isak Östlund</mal:name>
      <mal:email>translate@catnip.nu</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Vad är de olika enheterna i fliken Filsystem?</title>

  <p>Varje enhet under fliken <gui>Filsystem</gui> är en lagringsenhet (som en hårddisk eller USB-minne), eller en diskpartition. Du kan se varje enhets totala kapacitet, hur mycket som används, och teknisk information om vilken <link xref="fs-info">typ av filsystem</link> den har och <link xref="fs-info">var den är ”monterad”</link>.</p>
  
  <p>Diskutrymmet på en fysisk hårddisk kan delas upp i flera delar, så kallade <em>partitioner</em>, som var och en kan användas som om de vore separata diskar. Ifall din hårddisk har partitionerats (kanske av dig eller datortillverkaren), visas var partition för sig i listan för filsystem.</p>
  
  <note>
    <p>Du kan hantera diskar och partitioner samt se mer detaljerad information med programmet <app>Diskar</app>.</p>
  </note>

</page>
