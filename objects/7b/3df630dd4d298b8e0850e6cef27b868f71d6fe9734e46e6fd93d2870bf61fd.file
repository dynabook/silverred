<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="sound-volume" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="media#sound"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>

    <credit type="author">
      <name>Fil Bul</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Šon Mek Kens</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Podesite jačinu zvuka na računaru i odredite glasnost svakog programa.</desc>
  </info>

<title>Izmenite jačinu zvuka</title>

  <p>To change the sound volume, open the
  <gui xref="shell-introduction#systemmenu">system menu</gui> from the right
  side of the top bar and move the volume slider left or right. You can
  completely turn off sound by dragging the slider to the left.</p>

  <p>Neke tastature imaju tastere koji vam omogućavaju da upravljate jačinom zvuka. Ovi tasteri obično izgledaju kao zvučnici iz kojih izlaze zvučni talasi. Često se nalaze blizu tastera „F“ na vrhu. Na tastaturama prenosnih računara, obično se nalaze na „F“ tasterima. Držite pritisnutim taster <key>Fn</key> na tastaturi da ih koristite.</p>

  <p>Ako imate spoljne zvučnike, možete da promenite jačinu zvučnika koristeći dugme za pojačavanje koje se nalazi zvučnicima. Neke slušalice takođe imaju dugme za podešavanje jačine zvuka.</p>

<section id="apps">
 <title>Izmenite jačinu zvuka za pojedinačne programe</title>

  <p>Možete da izmenite jačinu zvuka nekog programa i da ostavite neizmenjenom jačinu zvuka za druge programe. Ovo je korisno ako slušate muziku i razgledate veb, na primer. Možete da isključite zvuk u veb pregledniku tako da zvuci sa veb sajtova ne ometaju muziku.</p>

  <p>Neki programi imaju dugmad za upravljanje zvukom u svojim glavnim prozorima. Ako ih vaš program ima, koristite ih da promenite jačinu zvuka. Ako ne:</p>

    <steps>
    <item>
      <p>Otvorite pregled <gui xref="shell-introduction#activities">Aktivnosti</gui> i počnite da kucate <gui>Zvuk</gui>.</p>
    </item>
    <item>
      <p>Kliknite na <gui>Zvuk</gui> da otvorite panel.</p>
    </item>
    <item>
      <p>Under <gui>Volume Levels</gui>, change the volume of the application
      listed there.</p>

  <note style="tip">
    <p>Samo programi koji trenutno puštaju zvuk biće na spisku. Ako neki program koji trenutno pušta zvuk nije na spisku, možda ne podržava funkciju koja vam dopušta da upravljate jačinom zvuka na ovaj način. U tom slučaju, nećete moći da promenite njegovu jačinu zvuka.</p>
  </note>
    </item>

  </steps>

</section>

</page>
