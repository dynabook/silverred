<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="net-email-virus" xml:lang="sv">

  <info>
    <link type="guide" xref="net-email"/>
    <link type="guide" xref="net-security"/>
    <link type="seealso" xref="net-antivirus"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Det är osannolikt att virus infekterar din dator, men kan infektera datorerna hos de personer du e-postar.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Behöver jag söka igenom min e-post efter virus?</title>

  <p>Virus är program som orsakar problem om de lyckas leta sig in på din dator. Ett vanligt sätt för dem att komma in på din dator är genom e-postmeddelanden.</p>

  <p>Virus som kan påverka datorer som kör Linux är väldigt ovanliga, så det är <link xref="net-antivirus">osannolikt att du får ett virus via e-post eller på annat sätt</link>. Om du får e-post med ett virus gömt i det kommer det troligtvis inte har någon effekt på din dator. Av den anledningen behöver du inte söka av din e-post efter virus.</p>

  <p>Du kan dock önska att leta genom din e-post efter virus om du skulle skicka vidare ett virus från en person till en annan. Om till exempel en av dina vänner har en Windows-dator med ett virus och skickar dig virusinfekterad e-post som du sedan skickar vidare till en annan vän med en Windows-dator, då kan den andra vännen få viruset också. Du kan installera antivirusprogramvara för att leta genom din e-post för att förhindra detta, men det är osannolikt att det händer och de flesta personer som kör Windows och Mac OS har själva antivirusprogramvara ändå.</p>

</page>
