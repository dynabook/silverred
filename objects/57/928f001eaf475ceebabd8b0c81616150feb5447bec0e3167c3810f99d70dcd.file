<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-recover" xml:lang="gl">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>
    <link type="seealso" xref="files-lost"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="review"/>

    <credit type="author">
      <name>Proxecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Os ficheiros eliminados normalmente envíanse ao lixo, polo que pode recuperalos.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2020</mal:years>
    </mal:credit>
  </info>

  <title>Recuperar un ficheiro eliminado</title>

  <p>Se elimina un ficheiro co xestor de ficheiros, normalmente enviase ao <gui>Lixo</gui>, e debería poder recuperalo.</p>

  <steps>
    <title>Para restaurar un ficheiro do lixo:</title>
    <item>
      <p>Comprobe a vista xeral de <gui>Actividades</gui> e comece a escribir <gui>Ficheiros</gui>.</p>
    </item>
    <item>
      <p>Prema en <app>Ficheiros</app> para abrir o xestor de ficheiros.</p>
    </item>
    <item>
      <p>Na barra lateral prema <gui>Lixo</gui> Se non o ve na barra lateral, prema o botón do menú superior esquerda da xanela e seleccione <gui>Barra lateral</gui>.</p>
    </item>
    <item>
      <p>If your deleted file is there, click on it and select
      <gui>Restore</gui>. It will be restored to the folder from where it was
      deleted.</p>
    </item>
  </steps>

  <p>If you deleted the file by pressing <keyseq><key>Shift</key><key>Delete
  </key></keyseq>, or by using the command line, the file has been permanently
  deleted. Files that have been permanently deleted can’t be recovered from the
  <gui>Trash</gui>.</p>

  <p>There are a number of recovery tools available that are sometimes able to
  recover files that were permanently deleted. These tools are generally not
  very easy to use, however. If you accidentally permanently deleted a file,
  it’s probably best to ask for advice on a support forum to see if you can
  recover it.</p>

</page>
