<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="solaris-mode" xml:lang="el">
  <info>
    <revision version="0.2" pkgversion="3.11" date="2014-01-26" status="review"/>
    <link type="guide" xref="index#other" group="other"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author copyright">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
      <years>2011</years>
    </credit>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2011, 2014</years>
    </credit>

    <desc>Use Solaris mode to reflect the number of CPUs.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name> Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Τι είναι η κατάσταση Solaris;</title>

  <p>Σε ένα σύστημα που έχει πολλές CPUs ή <link xref="cpu-multicore">πυρήνες</link>, οι διεργασίες μπορεί να χρησιμοποιούν περισσότερες από μία ταυτόχρονα. Είναι δυνατό η στήλη <gui>% CPU</gui> να εμφανίζει τιμές που το άθροισμα είναι μεγαλύτερο από 100%. (δηλαδή 400% σε ένα σύστημα με 4 CPU). Η <gui>κατάσταση Solaris</gui> διαιρεί την <gui>% CPU</gui> για κάθε διεργασία με τον αριθμό των CPUs στο σύστημα, έτσι ώστε το άθροισμα να είναι 100%.</p>

  <p>Για να εμφανίσετε τη <gui>% CPU</gui> σε <gui>κατάσταση Solaris</gui>:</p>

  <steps>
    <item><p>Πατήστε στο μενού εφαρμογών <gui>Προτιμήσεις</gui>.</p></item>
    <item><p>Πατήστε την καρτέλα <gui>Διεργασίες</gui>.</p></item>
    <item><p>Επιλέξτε <gui>Διαίρεση της χρήσης CPU με τον αριθμό CPU</gui>.</p></item>
  </steps>

    <note><p>Ο όρος <gui>κατάσταση Solaris</gui> προέρχεται από το UNIX της Sun, συγκρινόμενο με την προεπιλογή Linux της κατάστασης IRIX, που ονομάζεται για το UNIX της SGI.</p></note>

</page>
