<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-suspend" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspendfail"/>

    <desc>Obustavljanje šalje vaš računar na spavanje tako da koristi manje napajanja.</desc>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Gnomov projekat dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Šta se dešava kada obustavim računar?</title>

<p>Kada <em>obustavite rad</em> računara, vi ga uspavljujete. Svi vaši programi i dokumenta ostaju otvoreni, ali ekran i drugi delovi računara se isključuju radi uštede energije. Računar je ipak i dalje upaljen, i dalje će koristiti malu količinu energije. Možete da ga probudite pritiskom tastera ili klikom na dugme miša. Ako to ne pomogne, pokušajte da pritisnete dugme napajanja.</p>

<p>Neki računari imaju problema sa podrškom komponenti što znači da oni <link xref="power-suspendfail">možda neće moći da obustave rad ispravno</link>. Dobra zamisao je da isprobate obustavljanje na vašem računaru da biste videli da li radi.</p>

<note style="important">
  <title>Uvek sačuvajte vaš rad pre obustavljanja</title>
  <p>Trebali biste da sačuvate sav vaš rad pre nego što obustavite računar, u slučaju da nešto krene naopako i da vaši otvoreni programi i dokumenta ne mogu biti povraćeni kada ponovo povratite računar.</p>
</note>

</page>
