<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="net-email-virus" xml:lang="mr">

  <info>
    <link type="guide" xref="net-email"/>
    <link type="guide" xref="net-security"/>
    <link type="seealso" xref="net-antivirus"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>व्हायरस आपला संगणक संभाव्यतया संक्रमित करू शकतो, परंतु ईमेल पाठणाऱ्या वापरकर्त्यांचे संगणक देखील संक्रमित होऊ शकतात.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

  <title>ईमेल्सला वायरसेसकरिता स्कॅन करायचे?</title>

  <p>वायरसेस प्रोग्राम आहेत ज्यामुळे संगणकाकरिता मार्ग सापडल्यास अडचणी निर्माण होऊ शकतील. संगणकात ईमेल संदेशतर्फे वायरस येणे सर्वात मोठे कारण आहे.</p>

  <p>Viruses that can affect computers running Linux are quite rare, so you are
  <link xref="net-antivirus">unlikely to get a virus through email or
  otherwise</link>. If you receive an email with a virus hidden in it, it will
  probably have no effect on your computer. As such, you probably don’t need to
  scan your email for viruses.</p>

  <p>You may, however, wish to scan your email for viruses in case you happen
  to forward a virus from one person to another. For example, if one of your
  friends has a Windows computer with a virus and sends you a virus-infected
  email, and you then forward that email to another friend with a Windows
  computer, then the second friend might get the virus too. You could install
  an anti-virus application to scan your emails to prevent this, but it’s
  unlikely to happen and most people using Windows and Mac OS have anti-virus
  software of their own anyway.</p>

</page>
