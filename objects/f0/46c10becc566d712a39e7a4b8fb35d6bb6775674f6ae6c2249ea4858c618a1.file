<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-which-application" xml:lang="nl">

  <info>
    <link type="guide" xref="accounts"/>
    <link type="seealso" xref="accounts-disable-service"/>

    <revision pkgversion="3.8.2" date="2013-05-22" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="incomplete"/>

    <credit type="author copyright">
      <name>Baptiste Mille-Mathias</name>
      <email>baptistem@gnome.org</email>
      <years>2012, 2013</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Toepassingen kunnen gebruik maken van de in <app>Online-accounts</app> aangemaakte accounts en de diensten die zij benutten.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Onlinediensten en toepassingen</title>

  <p>Als u een online-account heeft toegevoegd, kan elke toepassing dat account gebruiken voor de beschikbare diensten die u niet heeft <link xref="accounts-disable-service">uitgeschakeld</link>. Verschillende providers leveren verschillende diensten. Deze pagina geeft een overzicht van de verschillende diensten en enkele van de toepassingen waarvan bekend is dat ze ze gebruiken.</p>

  <terms>
    <item>
      <title>Agenda</title>
      <p>Met de agendadienst kunt u afspraken weergeven, toevoegen en bewerken in een online-agenda. Het wordt gebruikt door toepassingen als <app>Calendar</app>, <app>Evolution</app>, en <app>California</app>.</p>
    </item>

    <item>
      <title>Chatten</title>
      <p>Met de Chatdienst kunt u chatten met uw contacten op populaire ‘instant messaging’-platforms. Het wordt gebruikt door de toepassing <app>Empathy</app>.</p>
    </item>

    <item>
      <title>Contacten</title>
      <p>Met de Contactendienst kunt u de gepubliceerde gegevens zien van uw contacten op verschillende diensten. Het wordt gebruikt door toepassingen zoals <app>Contacten</app> en <app>Evolution</app>.</p>
    </item>

    <item>
      <title>Documenten</title>
      <p>Met de Documentendienst kunt u uw onlinedocumenten bekijken zoals die in Google docs. U kunt uw documenten bekijken met de toepassing <app>Documenten</app>.</p>
    </item>

    <item>
      <title>Bestanden</title>
      <p>De Bestandendienst voegt een bestandslocatie op afstand toe, op dezelfde manier zoals u zou doen met de <link xref="nautilus-connect">Met server verbinden</link>-functionaliteit in bestandsbeheer. U kunt zowel bestandsbeheer als de Openen- en Opslaan-dialoogvensters in elke toepassing gebruiken voor toegang tot bestanden op afstand.</p>
    </item>

    <item>
      <title>E-mail</title>
      <p>Met de E-maildienst kunt u e-mail versturen en ontvangen via een e-mail-provider zoals Google. Het wordt gebruikt door <app>Evolution</app>.</p>
    </item>

<!-- TODO: Not sure what this does. Doesn't seem to do anything in Maps app.
    <item>
      <title>Maps</title>
    </item>
-->

    <item>
      <title>Foto's</title>
      <p>Met de Fotodienst kunt u uw onlinefoto's bekijken, zoals de foto's die u heeft geplaatst op Facebook. U kunt uw foto's bekijken met de toepassing <app>Foto's</app>.</p>
    </item>

    <item>
      <title>Printers</title>
      <p>Met de Printersdienst kunt u een PDF-kopie versturen naar een provider vanuit het afdrukvenster van elke toepassing. De provider levert misschien afdrukdiensten, of dient alleen maar als opslaglocatie voor het PDF-bestand, dat u later kunt downloaden en afdrukken.</p>
    </item>

    <item>
      <title>Later lezen</title>
      <p>Met de dienst ‘Later lezen’ kunt u een webpagina opslaan naar een externe dienst zodat u hem later kunt lezen op een ander apparaat. Momenteel zijn er geen toepassingen die deze dienst gebruiken.</p>
    </item>

  </terms>

</page>
