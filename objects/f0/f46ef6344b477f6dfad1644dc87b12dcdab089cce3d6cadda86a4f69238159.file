<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-history-recent-off" xml:lang="cs">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.8" date="2013-03-11" status="final"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak zastavit nebo omezit svůj počítač ve sledování nedávno použitých souborů.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Vypněte nebo omezte sledování historie souborů</title>
  
  <p>Sledování nedávno použitých souborů a složek může usnadnit vyhledání položek, se kterými jste pracovali ve správci souborů a dialogových oknech souborů v aplikacích. Na druhou stranu si můžete přát, aby se tyto položky uchovaly v soukromí, nebo aby se sledovala jen úplně nejnovější historie.</p>

  <steps>
    <title>Vypnutí sledování historie souborů</title>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Soukromí</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Soukromí</gui> otevřete příslušný panel.</p>
    </item>
    <item>
      <p>Vyberte <gui>Použití a historie</gui>.</p>
    </item>
    <item>
     <p>Přepněte vypínač <gui>Nedávno použité</gui> do polohy vypnuto.</p>
     <p>Pro opětovné povolení této funkce přepněte vypínač <gui>Nedávno použité</gui> do polohy zapnuto.</p>
    </item>
    <item>
      <p>Použijte tlačítko <gui>Vyčistit nedávnou historii</gui> k okamžitému zahození historie.</p>
    </item>
  </steps>
  
  <note><p>Toto nastavení neovlivní, jak uchovává webový prohlížeč informace o webových stránkách, které jste navštívili.</p></note>

  <steps>
    <title>Omezení doby, po kterou je soubor s historií schraňován</title>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Soukromí</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Soukromí</gui> otevřete příslušný panel.</p>
    </item>
    <item>
      <p>Vyberte <gui>Použití a historie</gui>.</p>
    </item>
    <item>
     <p>Ujistěte se, že je vypínač <gui>Nedávno použité</gui> přepnutý do polohy zapnuto.</p>
    </item>
    <item>
     <p>U položky <gui>Zachovat historii</gui> vyberte dobu uchování. Na výběr je <gui>1 den</gui>, <gui>7 dní</gui>, <gui>30 dní</gui> nebo <gui>Napořád</gui>.</p>
    </item>
    <item>
      <p>Použijte tlačítko <gui>Vyčistit nedávnou historii</gui> k okamžitému zahození historie.</p>
    </item>
  </steps>

</page>
