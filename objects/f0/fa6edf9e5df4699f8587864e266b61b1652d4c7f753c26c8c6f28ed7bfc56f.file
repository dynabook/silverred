<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-changepassword" xml:lang="sv">

  <info>
    <link type="guide" xref="user-accounts#passwords"/>
    <link type="seealso" xref="user-goodpassword"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Dokumentationsprojekt för GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Håll ditt konto säkert genom att ändra ditt lösenord ofta i dina kontoinställningar.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Välj ditt lösenord</title>

  <p>Det är en god idé att ibland ändra ditt lösenord, speciellt om du tror att någon annan känner till ditt lösenord.</p>

  <p>Du behöver <link xref="user-admin-explain">administratörsbehörighet</link> för att redigera användarkonton andra än ditt eget.</p>

  <steps>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Användare</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Användare</gui> för att öppna panelen.</p>
    </item>
    <item>
      <p>Klicka på etiketten <gui>·····</gui> till <gui>Lösenord</gui>. Om du ändrar lösenordet för en annan användare, måste du först <gui>Låsa upp</gui> panelen.</p>
    </item>
    <item>
      <p>Mata in ditt aktuella lösenord, sedan ett nytt lösenord. Mata in ditt nya lösenord igen i fältet <gui>Verifiera nytt lösenord</gui>.</p>
      <p>Du kan trycka på <gui style="button"><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16">
      <span its:translate="yes">generera lösenord</span></media></gui>-ikonen för att automatiskt generera ett slumpmässigt lösenord.</p>
    </item>
    <item>
      <p>Klicka på <gui>Ändra</gui>.</p>
    </item>
  </steps>

  <p>Säkerställ att du <link xref="user-goodpassword">väljer ett bra lösenord</link>. Detta hjälper till att hålla ditt konto säkert.</p>

  <note>
    <p>När du uppdaterar ditt inloggningslösenord kommer lösenordet för din inloggningsnyckelring automatiskt att uppdateras till att vara detsamma som ditt nya inloggningslösenord.</p>
  </note>

  <p>Om du glömmer ditt lösenord kan vilken användare som helst med administratörsbehörighet ändra det åt dig.</p>

</page>
