<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="keyboard-osk" xml:lang="pt">

  <info>
    <link type="guide" xref="keyboard" group="a11y"/>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>

    <credit type="author">
      <name>Jeremy Bicha</name>
      <email>jbicha@ubuntu.com</email>
    </credit>
    <credit type="author">
      <name>Julita Inca</name>
      <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hilh</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Utilizar um teclado em ecrã para introduzir texto clicando botões com o rato ou num ecrã tátil.</desc>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Utilizar um teclado em ecrã</title>

  <p>Se não tem um teclado conetado o seu computador ou se prefere não o usar, pode ativar o <em>teclado em ecrã</em> para introduzir texto.</p>

  <note>
    <p>O teclado em ecrã ativa-se automaticamente se usa um ecrã tátil</p>
  </note>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Universal Access</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Ative o <gui>Teclado em ecrã</gui> na seção <gui>Escritura</gui>.</p>
    </item>
  </steps>

  <p>Quando tenha que voltar a escrever, o teclado em ecrã aparecerá na parte inferior do ecrã.</p>

  <p>Press the <gui style="button">?123</gui> button to enter numbers and
  symbols. More symbols are available if you then press the
  <gui style="button">=/&lt;</gui> button. To return to the alphabet keyboard,
  press the <gui style="button">ABC</gui> button.</p>

  <p>You can press the
  <gui style="button"><media its:translate="no" type="image" src="figures/go-down-symbolic.svg" width="16" height="16"><span its:translate="yes">down</span></media></gui>
  button to hide the keyboard temporarily. The keyboard will show again
  automatically when you next press on something where you can use it.</p>
  <p>Press the
  <gui style="button"><media its:translate="no" type="image" src="figures/emoji-flags-symbolic.svg" width="16" height="16"><span its:translate="yes">flag</span></media></gui>
  button to change your settings for
  <link xref="session-language">Language</link> or
  <link xref="keyboard-layouts">Input Sources</link>.</p>
  <!-- obsolete <p>To make the keyboard show again, open the
  <link xref="shell-notifications">messagetray</link> (by moving your mouse to
  the bottom of the screen), and press the keyboard icon.</p> -->

</page>
