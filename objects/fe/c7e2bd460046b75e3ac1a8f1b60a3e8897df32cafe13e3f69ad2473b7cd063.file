<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="lockdown-online-accounts" xml:lang="ko">

  <info>
    <link type="guide" xref="user-settings#lockdown"/>
    <link type="seealso" xref="dconf-lockdown"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2015</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

   <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>일부 또는 모든 온라인 계정 사용을 허용하거나 거부합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조재은</mal:name>
      <mal:email>ckr971028@gmail.com</mal:email>
      <mal:years>2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>
  <title>온라인 계정 허용 또는 어부</title>

  <p><app>그놈 온라인 계정</app>(GOA)는 그놈 데스크톱과 프로그램에 사용자 네트워크 계정을 통합하는 용도로 사용합니다. 사용자는 구글, 페이스북, 플리커, 오운클라우드 등 <app>온라인 계정</app> 프로그램으로 개인 온라인 계정을 추가할 수 있습니다.</p>

  <p>시스템 관리자는 다음을 할 수 있습니다:</p>
  <list>
    <item><p>모든 온라인 계정 활성.</p></item>
    <item><p>일부 온라인 계정 활성.</p></item>
    <item><p>모든 온라인 계정 비활성.</p></item>
  </list>

<steps>
  <title>온라인 계정 설정</title>
  <item><p>Make sure that you have the <sys>gnome-online-accounts</sys> package
  installed on your system.</p>
  </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
  <item>
  <p>다음 설정을 넣은 <sys>local</sys> 데이터베이스에 정보를 제공할 <file>/etc/dconf/db/local.d/00-goa</file> 키 파일을 만드십시오.</p>
   <list>
   <item><p>특정 제공자를 활성화하려면:</p>
<code>
[org/gnome/online-accounts]
whitelisted-providers= ['google', 'facebook']
</code>
  </item>
   <item><p>모든 제공자 비활성:</p>
<code>
[org/gnome/online-accounts]
whitelisted-providers= ['']
</code>
  </item>
  <item><p>모든 제공자 활성:</p>
<code>
[org/gnome/online-accounts]
whitelisted-providers= ['all']
</code>
  <p>기본 설정입니다.</p></item>
   </list>
  </item>
  <item>
    <p>이 설정을 사용자가 우선 적용하지 못하게 하려면, <file>/etc/dconf/db/local.d/locks/goa</file> 파일을 만들어 다음 내용을 넣으십시오:</p>
    <listing>
    <title><file>/etc/dconf/db/local.db/locks/goa</file></title>
<code>
# Lock the list of providers that are allowed to be loaded
/org/gnome/online-accounts/whitelisted-providers
</code>
    </listing>
  </item>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
</steps>

</page>
