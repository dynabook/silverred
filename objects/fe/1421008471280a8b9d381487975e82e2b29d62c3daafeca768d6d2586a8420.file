<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="bluetooth-device-specific-pairing" xml:lang="sv">

  <info>
    <link type="guide" xref="bluetooth" group="#last"/>

    <credit type="author">
      <name>Bastien Nocera</name>
      <email>hadess@hadess.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Hur du parar ihop specifika enheter med din dator.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Instruktioner för att para ihop specifika enheter</title>

  <p>Även om du lyckas hitta manualen för enheten är det inte säkert att den innehåller tillräckligt med information för att genomföra en framgångsrik ihopparning. Här kommer detaljer för ett antal vanliga enheter.</p>

  <terms>
    <item>
      <title>PlayStation 3-handkontroller</title>
      <p>De enheterna använder ”kabel-ihopparning”. Anslut handkontrollerna via USB med <gui>Bluetoothinställningar</gui> öppnat och Bluetooth påslaget. Efter att ha tryckt på ”PS”-knappen kommer du att tillfrågas huruvida du vill installera handkontrollerna. Koppla från dem och tryck på ”PS”-knappen för att använda dem över Bluetooth.</p>
    </item>
    <item>
      <title>PlayStation 4-handkontroller</title>
      <p>De enheterna använder också ”kabel-ihopparning”. Anslut handkontrollerna via USB med <gui>Bluetoothinställningar</gui> öppnat och Bluetooth påslaget. Du kommer att tillfrågas huruvida du vill installera handkontrollerna utan att behöva trycka på PS-knappen. Koppla från dem och tryck på ”PS”-knappen för att använda dem över Bluetooth.</p>
      <p>Kombinationen av ”PS”- och ”Share”-knapparna för att para ihop handkontrollen kan också användas för att göra handkontrollen synlig och para ihop den som vilken annan Bluetooth-enhet som helst, om du inte har en USB-kabel till hands.</p>
    </item>
    <item>
      <title>PlayStation 3 BD-fjärrkontroll</title>
      <p>Håll ner ”Start”- och ”Enter”-knapparna samtidigt under cirka fem sekunder. Du kan sen välja fjärrkontrollen i enhetslistan som vanligt.</p>
    </item>
    <item>
      <title>Nintendo Wii- och Wii U-fjärrkontroller</title>
      <p>Använd den röda ”Sync”-knappen bakom batteriluckan för att starta ihopparningsprocessen. Andra knappkombinationer kommer inte att behålla ihopparningsinformationen så du skulle komma att behöva att göra om det igen inom kort. Notera också att viss programvara vill ha direktåtkomst till fjärrkontrollerna och i de fallen bör du inte installera dem direkt i Bluetooth-panelen. Referera till programmets handbok för instruktioner.</p>
    </item>
  </terms>

</page>
