<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-default-browser" xml:lang="cs">

  <info>
    <link type="guide" xref="net-browser"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Výchozí webový prohlížeč můžete změnit, když přejdete do <gui>Podrobností</gui> v <gui>Nastavení</gui>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Změna prohlížeče, ve kterém se jako výchozím otevírají webové stránky</title>

  <p>Když kliknete v nějaké aplikaci na odkaz vedoucí na www, webový prohlížeč automaticky otevře danou webovou stránku. Pokud ale máte nainstalováno více prohlížečů, může se otevřít v jiném, než chcete. Napravit to můžete změnou výchozího webového prohlížeče:</p>

  <steps>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Podrobnosti</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Podrobnosti</gui> otevřete příslušný panel.</p>
    </item>
    <item>
      <p>V seznamu na levé straně okna vyberte <gui>Výchozí aplikace</gui>.</p>
    </item>
    <item>
      <p>Změnou položky <gui>WWW</gui> si zvolte, který webový prohlížeč byste rádi používali jak výchozí.</p>
    </item>
  </steps>

  <p>Když otevřete jiný webový prohlížeč, může vám oznámit, že není výchozím prohlížečem a že by se jím chtěl stát. V takovém případě klikněte na tlačítko <gui>Zrušit</gui> (nebo obdobné), aby se o to nepokoušel a nestal se výchozím místo prohlížeče, který jste si nastavili.</p>

</page>
