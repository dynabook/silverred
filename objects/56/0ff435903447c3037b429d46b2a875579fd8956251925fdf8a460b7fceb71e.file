<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="display-dual-monitors" xml:lang="hu">

  <info>
    <link type="guide" xref="prefs-display"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-09" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-23" status="review"/>
    <revision pkgversion="3.28" date="2018-07-28" status="review"/>
    <revision pkgversion="3.34" date="2019-11-12" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>További monitorok beállítása.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

<title>További monitorok csatlakoztatása a számítógépéhez</title>

<!-- TODO: update video
<section id="video-demo">
  <title>Video Demo</title>
  <media its:translate="no" type="video" width="500" mime="video/webm" src="figures/display-dual-monitors.webm">
    <p its:translate="yes">Demo</p>
    <tt:tt its:translate="yes" xmlns:tt="http://www.w3.org/ns/ttml">
      <tt:body>
        <tt:div begin="1s" end="3s">
          <tt:p>Type <input>displays</input> in the <gui>Activities</gui>
          overview to open the <gui>Displays</gui> settings.</tt:p>
        </tt:div>
        <tt:div begin="3s" end="9s">
          <tt:p>Click on the image of the monitor you would like to activate or
          deactivate, then switch it <gui>ON/OFF</gui>.</tt:p>
        </tt:div>
        <tt:div begin="9s" end="16s">
          <tt:p>The monitor with the top bar is the main monitor. To change
          which monitor is “main”, click on the top bar and drag it over to
          the monitor you want to set as the “main” monitor.</tt:p>
        </tt:div>
        <tt:div begin="16s" end="25s">
          <tt:p>To change the “position” of a monitor, click on it and drag it
          to the desired position.</tt:p>
        </tt:div>
        <tt:div begin="25s" end="29s">
          <tt:p>If you would like both monitors to display the same content,
          check the <gui>Mirror displays</gui> box.</tt:p>
        </tt:div>
        <tt:div begin="29s" end="33s">
          <tt:p>When you are happy with your settings, click <gui>Apply</gui>
          and then click <gui>Keep This Configuration</gui>.</tt:p>
        </tt:div>
        <tt:div begin="33s" end="37s">
          <tt:p>To close the <gui>Displays Settings</gui> click on the
          <gui>×</gui> in the top corner.</tt:p>
        </tt:div>
      </tt:body>
    </tt:tt>
  </media>
</section>
-->

<section id="steps">
  <title>További monitorok beállítása.</title>
  <p>További monitorok beállításához először csatlakoztassa azokat a számítógépéhez. Ha a rendszer nem ismeri fel azonnal, vagy módosítani szeretné a beállításait:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Displays</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Displays</gui> to open the panel.</p>
    </item>
    <item>
      <p>In the display arrangement diagram, drag your displays to the relative
      positions you want.</p>
      <note style="tip">
        <p>The numbers on the diagram are shown at the top-left of each
        display when the <gui>Displays</gui> panel is active.</p>
      </note>
    </item>
    <item>
      <p>Kattintson az <gui>Elsődleges kijelző</gui> elemre az elsődleges kijelző kiválasztásához.</p>

      <note>
        <p>Az elsődleges kijelzőn látható a <link xref="shell-introduction">felső sáv</link>, és ezen jelenik meg a <gui>Tevékenységek</gui> áttekintés.</p>
      </note>
    </item>
    <item>
      <p>Select the orientation, resolution or scale, and refresh rate.</p>
    </item>
    <item>
      <p>Nyomja meg az <gui>Alkalmaz</gui> gombot. Az új beállítások 20 másodpercre alkalmazásra, majd visszavonásra kerülnek. Így ha nem lát semmit az új beállítások mellett, akkor a régi beállítások automatikusan visszaállításra kerülnek. Ha elégedett az új beállításokkal, akkor nyomja meg a <gui>Változtatások megtartása</gui> gombot.</p>
    </item>
  </steps>

</section>

<section id="modes">

  <title>Megjelenítési módok</title>
    <p>Két képernyővel ezek a megjelenítési módok érhetők el:</p>
    <list>
      <item><p><gui>Kijelzők egyesítése:</gui> a képernyőszélek egyesítve lesznek, így a dolgok átmehetnek az egyik kijelzőről a másikra.</p></item>
      <item><p><gui>Tükrözés:</gui> ugyanaz a tartalom jelenik meg mindkét kijelzőn ugyanolyan felbontással és tájolással.</p></item>
      <item><p><gui>Másodlagos kijelző:</gui> csak egy kijelző van beállítva, amely kikapcsolja a másikat. Például egy lehajtott tetejű, dokkolóban lévő laptophoz csatlakoztatott külső monitor lehet a másodlagos beállított kijelző.</p></item>
    </list>

</section>

<section id="multiple">

  <title>Több monitor hozzáadása</title>
    <p>Kettőnél több képernyővel a <gui>Kijelzők egyesítése</gui> az egyetlen elérhető mód.</p>
    <list>
      <item>
        <p>Press the button for the display that you would like to
        configure.</p>
      </item>
      <item>
        <p>Húzza a képernyőket a kívánt relatív helyekre.</p>
      </item>
    </list>

</section>
</page>
