<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-user" xml:lang="ko">

  <info>
    <link type="guide" xref="sundry#session"/>
    <link type="guide" xref="login#management"/>
    <link type="seealso" xref="session-custom"/>
    <revision pkgversion="3.4.2" date="2012-12-01" status="draft"/>
    <revision pkgversion="3.8" date="2013-08-06" status="review"/>
    <revision pkgversion="3.12" date="2014-06-17" status="review"/>

    <credit type="author copyright">
      <name>minnie_eg</name>
      <email>amany.elguindy@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>사용자에 대한 기본 세션을 지정합니다.</desc>
   
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조재은</mal:name>
      <mal:email>ckr971028@gmail.com</mal:email>
      <mal:years>2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

  <title>사용자 기본 세션 설정</title>

  <p>기본 세션은 <app>AccountsService</app> 프로그램에서 가져옵니다. <app>AccountsService</app> 프로그램에는 <file>/var/lib/AccountsService/users/</file> 디렉터리의 정보를 저장합니다.</p>

<note style="note">
  <p>그놈 2 에서는 사용자 기본 디렉터리에 기본 세션을 만들 때 <file>.dmrc</file> 파일을 사용했었습니다. <file>.dmrc</file> 파일은 더이상 사용하지 않습니다.</p>
</note>

  <steps>
    <title>사용자 기본 세션 설정</title>
    <item>
      <p>시스템에 <sys>gnome-session-xsession</sys> 패키지를 설치했는지 확인하십시오.</p>
    </item>
    <item>
      <p>각 가용 세션에 대한 <file>.desktop</file> 파일을 찾을 수 있는 <file>/usr/share/xsessions</file> 디렉터리를 찾아보십시오. 활용하고자 하는 세션을 결정할 <file>.desktop</file> 파일의 내용을 살펴보십시오.</p>
    </item>
    <item>
      <p>사용자 기본 세션을 지정하려면 <file>/var/lib/AccountsService/users/<var>username</var></file>파일의 사용자 <sys>계정 서비스</sys>를 업데이트하십시오:</p>
<code>[User]
Language=
XSession=gnome-classic</code>
       <p>이 예제에서는 <file>/usr/share/xsessions/gnome-classic.desktop</file> 파일을 활용하여 <link href="help:gnome-help/gnome-classic">그놈 클래식</link>을 기본 세션으로 설정했습니다.</p>
     </item>
  </steps>

  <p>사용자용 기본 세션을 지정하고 나면, 사용자가 다른 세션을 선택하지 않는 한 나중에 로그인했을 때 활용할 수 있습니다.</p>

</page>
