<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-slowkeys" xml:lang="ru">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <credit type="author">
      <name>Шон МакКенс (Shaun McCance)</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Фил Булл (Phil Bull)</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Задержка между нажатием клавиши и появлением символа на экране.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Включение «медленных клавиш»</title>

  <p>Включите <em>«медленные клавиши»</em>, если хотите, чтобы между нажатием клавиши и появлением символа на экране была задержка. Это означает, что для набора каждого символа нужно будет некоторое время удерживать соответствующую клавишу нажатой. Используйте «медленные клавиши», если при наборе вы часто случайно нажимаете несколько клавиш одновременно, или если вы недавно начали пользоваться компьютером, и вам трудно найти на клавиатуре нужные клавиши.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Universal Access</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Нажмите <gui>Помощник ввода (AccessX)</gui> в разделе <gui>Ввод</gui>.</p>
    </item>
    <item>
      <p>Switch the <gui>Slow Keys</gui> switch to on.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Быстрое включение и отключение «медленных клавиш»</title>
    <p>В разделе <gui>Управляется с клавиатуры</gui> выберите <gui>Включать и выключать специальные возможности с помощью клавиатуры</gui>, чтобы иметь возможность включать и отключать «медленные» клавиши с клавиатуры. Если этот параметр включён, то, нажав и удерживая клавишу <key>Shift</key> в течение восьми секунд, можно включать и отключать «медленные» клавиши.</p>
    <p>Для включения или отключения «медленных клавиш» можно также нажать на <link xref="a11y-icon">значок специальных возможностей</link> в верхней панели и выбрать <gui>«Медленные» клавиши</gui>. Значок специальных возможностей становится виден на панели, если один или более параметров были включены в разделе <gui>Универсальный доступ</gui>.</p>
  </note>

  <p>Используйте ползунок <gui>Задержка принятия</gui> для настройки времени, в течение которого нужно удерживать клавишу для регистрации её нажатия.</p>

  <p>You can have your computer make a sound when you press a key, when a key
  press is accepted, or when a key press is rejected because you didn’t hold
  the key down long enough.</p>

</page>
