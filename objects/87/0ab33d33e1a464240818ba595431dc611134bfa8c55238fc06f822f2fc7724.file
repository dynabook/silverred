<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="dconf-profiles" xml:lang="sv">

  <info>
    <link type="guide" xref="setup"/>
    <link type="seealso" xref="dconf-custom-defaults"/>
    <link type="seealso" xref="dconf"/>
    <revision pkgversion="3.30" date="2019-02-22" status="incomplete"/>

    <credit type="author copyright">
      <name>Ryan Lortie</name>
      <email>desrt@desrt.ca</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Švárová</name>
      <email>jana.svarova@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Detaljerad information om profiler och profilval.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Profiler</title>

  <p>En <em>profil</em> är en lista över konfigurationsdatabaser. Den första databasen i en profil är databasen att skriva till och de återstående databaserna är skrivskyddade. Var och en av databaserna genereras från en nyckelfilskatalog. Varje nyckelfilskatalog innehåller en eller flera nyckelfiler. Varje nyckelfil innehåller minst en dconf-sökväg och en eller flera nycklar samt motsvarande värden.</p>

  <p>Nyckelpar vilka ställs in i en <sys>dconf</sys>-<em>profil</em> kommer att åsidosätta standardinställningarna om det inte finns problem med värdet som du har ställt in.</p>

  <p>Du kommer oftast vilja att din <sys>dconf</sys>-profil består av en <em>användardatabas</em> och minst en systemdatabas. Profilen måste lista en databas per rad.</p>

  <p>Den första raden i en profil är databasen som ändringar skrivs till. Detta är vanligen <code>user-db:<input>user</input></code>. <input>user</input> är namnet på användardatabasen som vanligen kan hittas i <file>~/.config/dconf</file>.</p>

  <p>En <code>system-db</code>-rad anger en systemdatabas. Dessa databaser hittas i <file>/etc/dconf/db/</file>.</p>

  <example>
    <listing>
      <title>Exempelprofil</title>
<code its:translate="no">
user-db:user
system-db:<var>local</var>
system-db:<var>site</var>
</code>
    </listing>
  </example>

  <!-- TODO: explain the profile syntax (maybe new page) -->
  <!--TODO: explain local and site -->

  <p>Att konfigurera en ensam användardatabas och flera systemdatabaser tillåter att ha lager av inställningar. Inställningar från databasfilen <code>user</code> övertrumfar inställningarna i databasfilen <code>local</code>, och databasfilen <code>local</code> övertrumfar i sin tur databasfilen <code>site</code>.</p>

  <p>Prioritetsordningen för <link xref="dconf-lockdown">lås</link> är dock omvänd. Lås som introducerats i databasfilerna <code>site</code> eller <code>local</code> har prioritet över de som finns i <code>user</code>.</p>

  <note style="important">
  <p>En sessions <sys>dconf</sys>-profil avgörs vid inloggning, så användare kommer behöva logga ut och logga in för att tillämpa en ny <sys>dconf</sys>-användarprofil för sin session.</p>
  </note>

  <p>För mer information, se manualsidan för <link its:translate="no" href="man:dconf(7)">
  <cmd>dconf</cmd>(7)</link>.</p>

  <section id="dconf-profiles">

  <title>Välja en profil</title>

  <p>Vid uppstart tittar <sys>dconf</sys> på miljövariabeln <sys>DCONF_PROFILE</sys>. Variabeln kan ange en relativ sökväg till en fil i <file>/etc/dconf/profile/</file>, eller en absolut sökväg, exempelvis till användarens hemkatalog.</p>

  <p>Om miljövariabeln är inställd försöker <sys>dconf</sys> att öppna den namngivna profilen och avbryter om detta misslyckas. Om variabeln inte är inställd försöker <sys>dconf</sys> öppna profilen med namnet ”user”. Om detta misslyckas kommer det att falla tillbaka till en intern hårdkodad konfiguration.</p>

  <p>För mer information, se manualsidan för <link its:translate="no" href="man:dconf(7)">
  <cmd>dconf</cmd>(7)</link>.</p>

  </section>

</page>
