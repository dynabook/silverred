<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="keyboard-nav" xml:lang="mr">
  <info>
    <link type="guide" xref="keyboard" group="a11y"/>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>

    <revision pkgversion="3.7.5" version="0.2" date="2013-02-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.20" date="2016-08-13" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-27" status="review"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
       <name>Julita Inca</name>
       <email>yrazes@gmail.com</email>
    </credit>
    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>२०१२</years>
    </credit>
    <credit type="editor">
       <name>Ekaterina Gerasimova</name>
       <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>माऊस विना ॲप्लिकेशन्स आणि डेस्कटॉपचा वापर करा.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

  <title>कीबोर्ड मार्गदर्शन</title>

  <p>हे पृष्ठ, जे वापरकर्ते माउस किंवा इतर पॉइंटिंग साधनाचा वापर करू शकत नाही, किंवा ज्यांना कळफलकाचा शक्य तेवढा वापर करायचा आहे त्यांना कळफलक संचारनचे तपशील पुरवतो. सर्व वापरकर्त्यांना उपयोगी पडणाऱ्या कळफलक शार्टक्ट्सकरिता, त्याऐवजी <link xref="shell-keyboard-shortcuts"/> पहा.</p>

  <note style="tip">
    <p>माउसप्रमाणे पॉइंटिंग साधनाचा वापर करणे शक्य नसल्यास, कळफलकवरील न्युमेरिक किपॅडचा वापर करून माऊस नियंत्रीत करणे शक्य आहे. तपशीलकरिता <link xref="mouse-mousekeys"/> पहा.</p>
  </note>

<table frame="top bottom" rules="rows">
  <title>वापरकर्ता इंटरफेसचे मार्गदर्शन करा</title>
  <tr>
    <td><p><key>Tab</key> and</p>
    <p><keyseq><key>Ctrl</key><key>Tab</key></keyseq></p>
    </td>
    <td>
      <p>विविध रंग अंतर्गत कळफलक फोकस स्थानांतरित करा. <keyseq><key>Ctrl</key> <key>Tab</key></keyseq> कंट्रोल्सच्या गट अंतर्गत स्थानांतरित करते, जसे कि बाजूच्यापट्टीपासून मुख्य अंतर्भुत माहितीकरिता. <keyseq><key>Ctrl</key><key>Tab</key></keyseq> कंट्रोल देखील खंडीत करू शकते जे <key>Tab</key>चा स्वतःहून वापर करते, जसे कि मजूकर क्षेत्र.</p>
      <p>फोकसला उलट क्रमवारित हलवण्याकरिता <key>Shift</key> दाबून ठेवा.</p>
    </td>
  </tr>
  <tr>
    <td><p>बाण कीझ</p></td>
    <td>
      <p>सिंगल कंट्रोलमध्ये घटकांच्या अंतर्गत, किंवा संबंधित कंट्रोल्स अंतर्गत निवडला स्थानांतरित करा. साधनपट्टी अंतर्गत बटनांकरिता फोकस करण्यासाठी, सूची किंवा चिन्ह अवलोकनमध्ये घटकांची निवड करण्यासाठी, किंवा गटपासून रेडिओ बटनाची निवड करण्यासाठी बाणांचा वापर करा.</p>
    </td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key>बाण कीझ</keyseq></p></td>
    <td><p>सूची किंवा चिन्ह अवलोकनमध्ये, घटक निवड न बदलता कळफल फोकसला दुसऱ्या घटककरिता स्थानांतरित करा.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Shift</key>बाण कीझ</keyseq></p></td>
    <td><p>सूची किंवा चिन्ह अवलोकनमध्ये, सध्या निवडलेल्या घटकापासून ते नविन फोकस केलेल्या घटकपर्यंत सर्व घटकांची निवड करा.</p>
    <p>In a tree view, items that have children can be expanded or collapsed,
    to show or hide their children: expand by pressing
    <keyseq><key>Shift</key><key>→</key></keyseq>, and collapse by
    pressing <keyseq><key>Shift</key><key>←</key></keyseq>.</p></td>
  </tr>
  <tr>
    <td><p><key>स्पेस</key></p></td>
    <td><p>फोकस्ड घटक जसे कि बटन, चेकबॉक्स, किंवा सूचीतील घटक सक्रीय करा.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>स्पेस</key></keyseq></p></td>
    <td><p>सूची किंवा चिन्ह अवलोकनमध्ये, इतर घटकांना न निवडता फोकस केलेले घटक निवड अशक्य किंवा अशक्य करा.</p></td>
  </tr>
  <tr>
    <td><p><key>Alt</key></p></td>
    <td><p><em>ॲक्सिलरेटर्स</em> दाखवण्याकरिता <key>Alt</key> कि दाबून ठेवा: मेन्यु घटकांवरील, बटन, आणि इतर कंट्रोल्सवरील अधोरेखीत अक्षरे. <key>Alt</key> आणि अधोरेखीत अक्षर दाबून नियंत्रण सक्रीय करा, तुम्ही क्लिक केले तशाच प्रकारे.</p></td>
  </tr>
  <tr>
    <td><p><key>Esc</key></p></td>
    <td><p>मेन्यु, पॉपअप, स्विचर, किंवा संवाद पटलातून बाहेर पडा.</p></td>
  </tr>
  <tr>
    <td><p><key>F10</key></p></td>
    <td><p>पटलाच्या मेन्युपट्टीतील पहिले मेन्यु निवडा. मेन्युजकरिता संचारनकरिता बाण किज्चा वापर करा.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key xref="keyboard-key-super">Super</key> <key>F10</key></keyseq></p></td>
    <td><p>शीर्षपट्टीवर ॲप्लिकेशन मेन्यु उघडा.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Shift</key><key>F10</key></keyseq> or</p>
    <p><key xref="keyboard-key-menu">Menu</key></p></td>
    <td>
      <p>सध्याच्या निवडकरिता संदर्भ मेन्यु पॉपअप होईल, उजवी-क्लिक दिल्याप्रमाणे.</p>
    </td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>F10</key></keyseq></p></td>
    <td><p>फाइल व्यवस्थापकात, सध्याच्या फोल्डरकरिता संदर्भ मेन्यु पॉपअप करा, पार्श्वभूमीत उजवी-क्लिक दिल्याप्रमाणे आणि कुठल्याही घटकावर नाही.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>PageUp</key></keyseq></p>
    <p>and</p>
    <p><keyseq><key>Ctrl</key><key>PageDown</key></keyseq></p></td>
    <td><p>टॅब्ड संवादात, डावी किंवा उजवीकडील टॅब्चा वापर करा.</p></td>
  </tr>
</table>

<table frame="top bottom" rules="rows">
  <title>डेस्कटॉपचे मार्गदर्शन करा</title>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="alt-f1"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-tab"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-tick"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="ctrl-alt-tab"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-updown"/>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F6</key></keyseq></p></td>
    <td><p>त्याच ॲप्लिकेशनमध्ये पटलांतर्गत चक्राकार करा. <key>Alt</key> कि दाबून ठेवा आणि ठळक करण्याजोगी पटल दिसेपर्यंत <key>F6</key> दाबा, त्यानंतर <key>Alt</key> सोडून द्या. हे <keyseq><key>Alt</key><key>`</key></keyseq> गुणविशेष सारखे आहे.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>Esc</key></keyseq></p></td>
    <td><p>कार्यक्षेत्रात सर्व खुल्या पटलांतर्गत चक्राकार करा.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>V</key></keyseq></p></td>
    <td><p><link xref="shell-notifications#notificationlist">Open the
    notification list.</link> Press <key>Esc</key> to close.</p></td>
  </tr>
</table>

<table frame="top bottom" rules="rows">
  <title>पटलाचे मार्गदर्शन करा</title>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F4</key></keyseq></p></td>
    <td><p>सध्याचे पटल बंद करा.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F5</key></keyseq> किंवा <keyseq><key>Super</key><key>↓</key></keyseq></p></td>
    <td><p>Restore a maximized window to its original size. Use
    <keyseq><key>Alt</key><key>F10</key></keyseq> to maximize.
    <keyseq><key>Alt</key><key>F10</key></keyseq> both maximizes and
    restores.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F7</key></keyseq></p></td>
    <td><p>सध्याचे पटल हलवा. <keyseq><key>Alt</key><key>F7</key></keyseq> दाबा, त्यानंतर पटल हलवण्याकरिता बाण किजचा वापर करा. पटल हलवणे थांबवण्याकरिता <key>Enter</key> दाबा, किंवा मूळ ठिकाणी नेण्याकरिता <key>Esc</key> दाबा.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F8</key></keyseq></p></td>
    <td><p>सध्याच्या पटलाला पुन्हा आकार द्या. <keyseq><key>Alt</key><key>F8</key></keyseq> दाबा, त्यानंतर पटलाचे आकार सुधारित करण्यासाठी बाण किजचा वापर करा. पटालाचे आकार बदलणे संपन्न करण्यासाठी <key>Enter</key> दाबा, किंवा मूळ आकारात नेण्याकरिता <key>Esc</key> दाबा.</p></td>
  </tr>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="shift-super-updown"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="shift-super-left"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="shift-super-right"/>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F10</key></keyseq> किंवा <keyseq><key>Super</key><key>↑</key></keyseq></p>
    </td>
    <td><p>पटल <link xref="shell-windows-maximize">मोठे करा</link>. मोठ्या आकाराच्या पटलला मूळ आकारात नेण्याकरिता <keyseq><key>Alt</key><key>F10</key></keyseq> किंवा <keyseq><key>Super</key><key>↓</key></keyseq> दाबा.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>H</key></keyseq></p></td>
    <td><p>पटल छोटे करा.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>←</key></keyseq></p></td>
    <td><p>पड्याच्या डाव्या बाजूस पटल उभेरित्या मोठे करा. पटलला मागील आकार नेण्याकरिता पुन्हा दाबा. बाजू बदलण्याकरिता <keyseq><key>Super</key><key>→</key></keyseq> दाबा.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>→</key></keyseq></p></td>
    <td><p>पड्याच्या उजव्या बाजूस पटल उभेरित्या मोठे करा. पटलला मागील आकार नेण्याकरिता पुन्हा दाबा. बाजू बदलण्याकरिता <keyseq><key>Super</key><key>→</key></keyseq> दाबा.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>स्पेस</key></keyseq></p></td>
    <td><p>पटल मेन्यु पॉपअप होईल, शीर्षपट्टीवरील उजवी-क्लिक दिल्याप्रमाणे.</p></td>
  </tr>
</table>

</page>
