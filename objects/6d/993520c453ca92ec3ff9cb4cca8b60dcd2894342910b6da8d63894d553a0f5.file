<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="contacts-link-unlink" xml:lang="hu">

  <info>
    <link type="guide" xref="contacts"/>
    <revision pkgversion="3.5.5" date="2012-02-19" status="review"/>
    <revision pkgversion="3.8" date="2013-04-27" status="review"/>
    <revision pkgversion="3.12" date="2014-02-27" status="final"/>
    <revision pkgversion="3.15" date="2015-01-28" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Egy névjegy információinak egyesítése több forrásból.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

<title>Névjegyek összekapcsolása/szétválasztása</title>

<section id="link-contacts">
  <title>Névjegyek összekapcsolása</title>

  <p>A többször előforduló névjegyek összekapcsolhatók a helyi címjegyzékéből és online fiókjaiból egyetlen <app>Névjegyek</app> bejegyzéssé. Ez a szolgáltatás segít a címjegyzékét rendszerezetten tartani: egy partner összes adata egy helyről lesz elérhető.</p>

  <steps>
    <item>
      <p>Kapcsolja be a <em>kijelölési módot</em> a névjegylista feletti pipa gomb megnyomásával.</p>
    </item>
    <item>
      <p>Minden névjegy mellett megjelenik egy jelölőnégyzet. Jelölje be az egyesíteni kívánt névjegyek melletti négyzeteket.</p>
    </item>
    <item>
      <p>Kattintson az <gui style="button">Összekapcsolás</gui> gombra a kijelölt névjegyek összekapcsolásához.</p>
    </item>
  </steps>

</section>

<section id="unlink-contacts">
  <title>Partnerek szétválasztása</title>

  <p>Ha véletlenül össze nem tartozó névjegyeket kapcsolt össze, akkor szétválaszthatja azokat.</p>

  <steps>
    <item>
      <p>Válassza ki a választani kívánt névjegyet a névjegyek listájából.</p>
    </item>
    <item>
      <p>Kattintson a <gui style="button">Szerkesztés</gui> gombra a <app>Névjegyek</app> jobb felső sarkában.</p>
    </item>
    <item>
      <p>Nyomja meg az <gui style="button">Összekapcsolt fiókok</gui> gombot.</p>
    </item>
    <item>
      <p>Nyomja meg az <gui style="button">Eltávolítás</gui> gombot a bejegyzés leválasztásához a névjegyről.</p>
    </item>
    <item>
      <p>Zárja be az ablakot a bejegyzések szétválasztásának befejezésekor.</p>
    </item>
    <item>
      <p>Kattintson a <gui style="button">Kész</gui> gombra a névjegy szerkesztésének befejezéséhez.</p>
    </item>
  </steps>

</section>

</page>
