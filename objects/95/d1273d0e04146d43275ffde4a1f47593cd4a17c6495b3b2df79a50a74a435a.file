<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-wireless-disconnecting" xml:lang="el">

  <info>
    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-problem"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <desc>Μπορεί να έχετε χαμηλό σήμα, ή το δίκτυο μπορεί να μην σας επιτρέπει να συνδεθείτε κατάλληλα.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

<title>Γιατί το ασύρματο δίκτυό μου εξακολουθεί να είναι αποσυνδεμένο;</title>

<p>Μπορεί να βρείτε ότι έχετε αποσυνδεθεί από ένα ασύρματο δίκτυο αν και θα θέλατε να παραμείνετε συνδεμένοι. Ο υπολογιστής σας θα προσπαθήσει κανονικά να επανασυνδεθεί στο δίκτυο μόλις αυτό συμβεί (το εικονίδιο διαδικτύου στην πάνω γραμμή θα εμφανίσει τρεις κουκκίδες αν προσπαθεί να επανασυνδεθεί), αλλά μπορεί να είναι ενοχλητικό, ιδιαίτερα εάν χρησιμοποιούσατε το διαδίκτυο τότε.</p>

<section id="signal">
 <title>Ασθενές ασύρματο σήμα</title>

 <p>Μια συνηθισμένη αιτία αποσύνδεσης από ένα ασύρματο δίκτυο είναι ότι έχετε χαμηλό σήμα. Τα ασύρματα δίκτυα έχουν περιορισμένο εύρος, έτσι εάν είσαστε υπερβολικά μακριά από τον ασύρματο σταθμό βάσης, ενδέχεται να μην μπορείτε να πάρετε ένα αρκετά ισχυρό σήμα για να διατηρήσετε μια σύνδεση. Τοίχοι και άλλα αντικείμενα μεταξύ του σταθμού βάσης και εσάς μπορεί επίσης να εξασθενούν το σήμα.</p>

 <p>Το εικονίδιο δικτύου στην πάνω γραμμή εμφανίζει πόσο ισχυρό είναι το ασύρματο σήμα. Εάν το σήμα φαίνεται χαμηλό, δοκιμάστε να μετακινηθείτε πιο κοντά στον ασύρματο σταθμό βάσης.</p>

</section>

<section id="network">
 <title>Η σύνδεση δικτύου δεν έγινε σωστά</title>

 <p>Sometimes, when you connect to a wireless network, it may appear that you
 have successfully connected at first, but then you will be disconnected soon
 after. This normally happens because your computer was only partially
 successful in connecting to the network — it managed to establish a connection,
 but was unable to finalize the connection for some reason and so was
 disconnected.</p>

 <p>Μια πιθανή αιτία για αυτό είναι ότι εισάγατε λάθος το ασύρματο συνθηματικό, ή ότι ο υπολογιστής σας δεν επιτρεπόταν στο δίκτυο (επειδή το δίκτυο απαιτεί ένα όνομα χρήστη για σύνδεση, για παράδειγμα).</p>

</section>

<section id="hardware">
 <title>Αναξιόπιστοι ασύρματοι οδηγοί/υλικά</title>

 <p>Μερικά υλικά ασύρματου δικτύου μπορούν να είναι λίγο αναξιόπιστα. Τα ασύρματα δίκτυα είναι περίπλοκα, έτσι οι ασύρματες κάρτες και σταθμοί βάσης ενίοτε δημιουργούν δευτερεύοντα προβλήματα και μπορεί να απορρίπτουν συνδέσεις. Αυτό είναι ενοχλητικό, αλλά συμβαίνει αρκετά συχνά με πολλές συσκευές. Εάν αποσυνδέεστε από ασύρματες συνδέσεις κάπου-κάπου, αυτό μπορεί να είναι απλά ο λόγος. Εάν συμβαίνει πολύ συχνά, ίσως να πρέπει να σκεφτείτε να πάρετε κάποιο διαφορετικό υλικό.</p>

</section>

<section id="busy">
 <title>Απασχολημένα ασύρματα δίκτυα</title>

 <p>Τα ασύρματα δίκτυα σε πολυάσχολα σημεία (σε πανεπιστήμια και καφετέριες, για παράδειγμα) έχουν συχνά πολλούς υπολογιστές που προσπαθούν να συνδεθούν σε αυτά. Μερικές φορές αυτά τα δίκτυα απασχολούνται υπερβολικά και ενδέχεται να μην μπορούν να χειριστούν όλους τους υπολογιστές που προσπαθούν να συνδεθούν, έτσι κάποιοι από αυτούς αποσυνδέονται.</p>

</section>

</page>
