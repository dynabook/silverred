<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="getting-started" version="1.0 if/1.0" xml:lang="mr">

<info>
    <link type="guide" xref="index" group="gs"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <desc>GNOMEसाठी नवीन? कसं वावरायचं ते शिका.</desc>
    <title type="link">Getting started with GNOME</title>
    <title type="text">सुरु करतांना</title>

    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande</mal:name>
      <mal:email>djaniketster@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

<title>सुरु करतांना</title>

<if:choose>
<if:when test="!platform:gnome-classic">

  <ui:overlay width="235" height="145">
  <media type="video" its:translate="no" src="figures/gnome-launching-applications.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-launching-apps.svg">
      <ui:caption its:translate="yes"><desc style="center">ॲप्लिकेशन्स लॉँच करा</desc></ui:caption>
    </ui:thumb>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>ॲप्लिकेशन्स लॉँच करत आहे</tt:p>
         </tt:div>
         <tt:div begin="5s" end="7.5s">
           <tt:p>तुमचा माऊस प्वाइन्टर <gui>कृती</gui> येथे हलवा जे स्क्रीनच्या सगळ्यात वर डाव्या हाताच्या कोपऱ्यावर आहे.</tt:p>
           </tt:div>
         <tt:div begin="7.5s" end="9.5s">
           <tt:p><gui>ॲप्लिकेशन्स दाखवा</gui> चिन्हावर क्लिक करा.</tt:p>
         </tt:div>
         <tt:div begin="9.5s" end="11s">
           <tt:p>जे ॲपलिकेशन सुरु करायचं असेल त्यावर क्लिक करा, उदाहरण, मदत.</tt:p>
         </tt:div>
         <tt:div begin="12s" end="21s">
           <tt:p>पर्यायी, <gui>कार्य सारांश</gui> बघायला कीबोर्डवर <key href="help:gnome-help/keyboard-key-super">Super</key> दाबा.</tt:p>
         </tt:div>
         <tt:div begin="22s" end="29s">
           <tt:p>त्या ॲपलिकेशनचं नाव टाइप करायला सुरुवात करा जे लॉँच तुम्हाला करायचं आहे.</tt:p>
         </tt:div>
         <tt:div begin="30s" end="33s">
           <tt:p>ॲपलिकेशन लॉँच करण्यासाठी <key>Enter</key> दाबा.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

</if:when>
</if:choose>

  <ui:overlay width="235" height="145">
  <media type="video" its:translate="no" src="figures/gnome-task-switching.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-task-switching.svg">
        <ui:caption its:translate="yes"><desc style="center">कामे स्थलांतरित करा</desc></ui:caption>
    </ui:thumb>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>कामे स्थलांतरित करा</tt:p>
         </tt:div>
         <tt:div begin="5s" end="8s">
           <tt:p if:test="!platform:gnome-classic">तुमचा माऊस प्वाइन्टर <gui>कृती</gui> येथे हलवा जे स्क्रीनच्या सगळ्यात वर डाव्या हाताच्या कोपऱ्यावर आहे.</tt:p>
         </tt:div>
         <tt:div begin="9s" end="12s">
           <tt:p>त्या कामावर स्थलांतरित करण्यासाठी पटलावर क्लिक करा.</tt:p>
         </tt:div>
         <tt:div begin="12s" end="14s">
           <tt:p>To maximize a window along the left side of the screen, grab
            the window’s titlebar and drag it to the left.</tt:p>
         </tt:div>
         <tt:div begin="14s" end="16s">
           <tt:p>जेव्हा अर्धी स्क्रीन दृष्टीवेधली असेल, पटल सोडा.</tt:p>
         </tt:div>
         <tt:div begin="16s" end="18">
           <tt:p>To maximize a window along the right side, grab the window’s
            titlebar and drag it to the right.</tt:p>
         </tt:div>
         <tt:div begin="18s" end="20s">
           <tt:p>जेव्हा अर्धी स्क्रीन दृष्टीवेधली असेल, पटल सोडा.</tt:p>
         </tt:div>
         <tt:div begin="23s" end="27s">
           <tt:p><gui>पटल परिवर्तक</gui> दाखवण्यासाठी <keyseq> <key href="help:gnome-help/keyboard-key-super">Super</key><key> Tab</key></keyseq> दाबा.</tt:p>
         </tt:div>
         <tt:div begin="27s" end="29s">
           <tt:p>पुढचं दृष्टीवेधित पटल निवडण्यासाठी <key href="help:gnome-help/keyboard-key-super">Super </key> सोडा.</tt:p>
         </tt:div>
         <tt:div begin="29s" end="32s">
           <tt:p>उघड्या पटलांमधून निवडण्यासाठी, <key href="help:gnome-help/keyboard-key-super">Super</key> सोडू नका पण धरून ठेवा, आणि <key>Tab</key> दाबा.</tt:p>
         </tt:div>
         <tt:div begin="35s" end="37s">
           <tt:p><gui>कार्य सारांश</gui> दाखवण्यासाठी <key href="help:gnome-help/keyboard-key-super">Super </key> दाबा.</tt:p>
         </tt:div>
         <tt:div begin="37s" end="40s">
           <tt:p>त्या ॲपलिकेशनचं नाव टाइप करायला सुरुवात करा ज्यावर तुम्हाला स्थलांतर करायचं आहे.</tt:p>
         </tt:div>
         <tt:div begin="40s" end="43s">
           <tt:p>जेव्हा ॲपलिकेशन पाहिलं निष्कर्ष म्हणून दिसेल, त्यावर स्थलांतर करण्यासाठी <key>Enter</key> दाबा.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

  <ui:overlay width="235" height="145">
  <media type="video" its:translate="no" src="figures/gnome-windows-and-workspaces.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-windows-and-workspaces.svg">
          <ui:caption its:translate="yes"><desc style="center">पटल आणि वर्कस्पेसेस वापरा</desc></ui:caption>
    </ui:thumb>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>पटल आणि वर्कस्पेसेस</tt:p>
         </tt:div>
         <tt:div begin="6s" end="10s">
           <tt:p>To maximize a window, grab the window’s titlebar and drag it to
            the top of the screen.</tt:p>
           </tt:div>
         <tt:div begin="10s" end="13s">
           <tt:p>जेव्हा स्क्रीन दृष्टीवेधली असेल, पटल सोडा.</tt:p>
         </tt:div>
         <tt:div begin="14s" end="20s">
           <tt:p>To unmaximize a window, grab the window’s titlebar and drag it
            away from the edges of the screen.</tt:p>
         </tt:div>
         <tt:div begin="25s" end="29s">
           <tt:p>तुम्ही उच्च पट्टीवर क्लिक करून पटलाला दूर नेऊ शकता आणि छोटे करू शकता.</tt:p>
         </tt:div>
         <tt:div begin="34s" end="38s">
           <tt:p>To maximize a window along the left side of the screen, grab
            the window’s titlebar and drag it to the left.</tt:p>
         </tt:div>
         <tt:div begin="38s" end="40s">
           <tt:p>जेव्हा अर्धी स्क्रीन दृष्टीवेधली असेल, पटल सोडा.</tt:p>
         </tt:div>
         <tt:div begin="41s" end="44s">
           <tt:p>To maximize a window along the right side of the screen, grab
            the window’s titlebar and drag it to the right.</tt:p>
         </tt:div>
         <tt:div begin="44s" end="48s">
           <tt:p>जेव्हा अर्धी स्क्रीन दृष्टीवेधली असेल, पटल सोडा.</tt:p>
         </tt:div>
         <tt:div begin="54s" end="60s">
           <tt:p>पटल कीबोर्डने मोठे करण्यासाठी, <key href="help:gnome-help/keyboard-key-super">Super</key> दाबून ठेवा आणि <key>↑</key> दाबा.</tt:p>
         </tt:div>
         <tt:div begin="61s" end="66s">
           <tt:p>पटल कीबोर्डने पूर्वस्थितीत छोटे करण्यासाठी, <key href="help:gnome-help/keyboard-key-super">Super</key> दाबून ठेवा आणि <key>↓</key> दाबा.</tt:p>
         </tt:div>
         <tt:div begin="66s" end="73s">
           <tt:p>पटल कीबोर्डने उजव्या बाजूला मोठे करण्यासाठी, <key href="help:gnome-help/keyboard-key-super">Super</key> दाबून ठेवा आणि <key>→</key> दाबा.</tt:p>
         </tt:div>
         <tt:div begin="76s" end="82s">
           <tt:p>पटल कीबोर्डने डाव्या बाजूला मोठे करण्यासाठी, <key href="help:gnome-help/keyboard-key-super">Super</key> दाबून ठेवा आणि <key>←</key> दाबा.</tt:p>
         </tt:div>
         <tt:div begin="83s" end="89s">
           <tt:p>आत्ताच्या वर्कस्पेसपेक्षा खालच्या वर्कस्पेसवर जाण्यासाठी, <keyseq><key href="help:gnome-help/keyboard-key-super">Super </key><key>Page Down</key></keyseq> दाबा.</tt:p>
         </tt:div>
         <tt:div begin="90s" end="97s">
           <tt:p>आत्ताच्या वर्कस्पेसपेक्षा वरच्या वर्कस्पेसवर जाण्यासाठी, <keyseq><key href="help:gnome-help/keyboard-key-super">Super </key><key>Page Up</key></keyseq> दाबा.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

<links type="topic" style="grid" groups="tasks">
<title style="heading">Common tasks</title>
</links>

<links type="guide" style="heading nodesc">
<title/>
</links>

</page>
