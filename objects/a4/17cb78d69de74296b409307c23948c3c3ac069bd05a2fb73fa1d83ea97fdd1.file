<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="dconf-custom-defaults" xml:lang="gl">

  <info>
    <link type="guide" xref="setup"/>
    <link type="seealso" xref="dconf-profiles"/>
    <link type="seealso" xref="dconf-lockdown"/>
    <link type="seealso" xref="dconf"/>
    <revision pkgversion="3.30" date="2019-02-08" status="draft"/>

    <credit type="author copyright">
      <name>Ryan Lortie</name>
      <email>desrt@desrt.ca</email>
      <years>2012</years>
    </credit>
    <credit type="author copyright">
      <name>Jeremy Bicha</name>
      <email>jbicha@ubuntu.com</email>
      <years>2012</years>
    </credit>
    <credit type="author copyright">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="copyright editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Estabeleza as preferencias por omisión a nivel de sistema usando perfiles <sys its:translate="no">dconf</sys>.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2010-2018</mal:years>
    </mal:credit>
  </info>

  <title>Valores por omisións personalizados para as preferencias a nivel de sistema</title>

  <p>System-wide default settings can be set by providing a default for a key
  in a <sys its:translate="no">dconf</sys> profile. These defaults can be
  overridden by the user.</p>

<section id="example">
  <title>Estabelecer un valor por omisión</title>

  <p>To set a default for a key, the <sys>user</sys> profile must exist and the
  value for the key must be added to a <sys its:translate="no">dconf</sys>
  database.</p>

  <steps>
    <title>An example setting the default background</title>
    <item>
      <p>Crear o perfíl <file its:translate="no">user</file>:</p>
      <listing its:translate="no">
        <title><file>/etc/dconf/profile/user</file></title>
<code>
user-db:user
system-db:local
</code>
      </listing>
      <p><input its:translate="no">local</input> é o nome dunha base de datos <sys its:translate="no">dconf</sys>.</p>
    </item>
    <item>
      <p>Cree un <em>ficheiro de chaves</em> para a base de datos <input its:translate="no">local</input> que contén as preferencias por omisión:</p>
      <listing its:translate="no">
        <title><file>/etc/dconf/db/local.d/01-background</file></title>
<code>
# <span its:translate="yes">ruta de dconf</span>
[org/gnome/desktop/background]

# <span its:translate="yes">nomes de chaves de dconf e os seus valores correspondentes</span>
picture-uri='file:///usr/local/share/backgrounds/wallpaper.jpg'
picture-options='scaled'
primary-color='000000'
secondary-color='FFFFFF'
</code>
      </listing>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  </steps>

  <note>
    <p>When the <sys its:translate="no">user</sys> profile is created or
    changed, the user will need to log out and log in again before the changes
    will be applied.</p>
  </note>

  <p>If you want to avoid creating the <sys its:translate="no">user</sys>
  profile, you can use the <cmd>dconf</cmd> command-line utility to read and
  write individual values or entire directories from and to a
  <sys its:translate="no">dconf</sys> database. For more information, see the
  <link its:translate="no" href="man:dconf"><cmd>dconf</cmd>(1)</link> man
  page.</p>

</section>

</page>
