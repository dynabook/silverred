<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="process-identify-file" xml:lang="sv">
  <info>
    <revision version="0.2" pkgversion="3.11" date="2014-01-26" status="review"/>
    <link type="guide" xref="index#processes-tasks" group="processes-tasks"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author copyright">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
      <years>2011</years>
    </credit>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2011, 2014</years>
    </credit>

    <desc>Sök efter en öppen fil för att visa vilken process som använder den.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isak Östlund</mal:name>
      <mal:email>translate@catnip.nu</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Hitta vilket program som använder en viss fil</title>

  <p>Ibland kommer ett felmeddelande informera dig om att en enhet (som ljudenheten eller DVD-ROM) är upptagen, eller att filen du vill redigera redan används. För att hitta den ansvariga processen eller processerna:</p>

    <steps>
      <item><p>Tryck på <guiseq><gui>Systemövervakare</gui><gui>Sök efter öppna filer</gui></guiseq>.</p>
      </item>
      <item><p>Skriv in ett filnamn eller del av filnamn. Det kan vara <file>/dev/snd</file> för ljudenheten eller <file>/media/cdrom</file> för DVD-ROM.</p>
      </item>
      <item><p>Klicka på <gui>Hitta</gui>.</p>
      </item>
    </steps>

  <p>Det här visar en lista med program som körs och för tillfället har tillgång till filen eller filerna som matchar sökningen. Att avsluta programmet bör ge dig åtkomst till enheten eller låta dig redigera filen.</p>

</page>
