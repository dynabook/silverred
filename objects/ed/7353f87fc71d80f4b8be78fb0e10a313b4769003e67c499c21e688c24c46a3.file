<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-brightness" xml:lang="nl">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="guide" xref="hardware-problems-graphics"/>
    <link type="seealso" xref="power-whydim"/>
    <link type="seealso" xref="a11y-contrast"/>

    <revision pkgversion="3.8" version="0.4" date="2013-03-28" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.28" date="2018-07-19" status="review"/>
    <revision pkgversion="3.33" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Gnome-documentatieproject</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>De helderheid van het scherm verhogen om het beter leesbaar te maken bij fel licht.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>De helderheid van het scherm instellen</title>

  <p>Afhankelijk van uw hardware kunt u het scherm dimmen om energie te sparen of de helderheid verhogen om het beter leesbaar te maken bij fel licht.</p>

  <p>To change the brightness of your screen, click the
  <gui xref="shell-introduction#systemmenu">system menu</gui> on the right side
  of the top bar and adjust the screen brightness slider to the value you want
  to use. The change should take effect immediately.</p>

  <note style="tip">
    <p>Veel laptoptoetsenborden hebben speciale toetsen om de helderheid aan te passen. Deze hebben vaak een afbeelding die eruit ziet als een zonnetje. Om deze toetsen te gebruiken houdt u de <key>Fn</key>-toets ingedrukt.</p>
  </note>

  <p>U kunt de helderheid van het scherm ook aanpassen via het paneel <gui>Energie</gui>.</p>

  <steps>
    <title>Om de helderheid van het scherm in te stellen via het paneel <gui>Energie</gui>:</title>
    <item>
      <p>Open het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht en typ <gui>Energie</gui>.</p>
    </item>
    <item>
      <p>Klik op <gui>Energie</gui> om het paneel te openen.</p>
    </item>
    <item>
      <p>Gebruik de schuifbalk <gui>Helderheid van het scherm</gui> om de helderheid naar uw wens aan te passen. De wijziging zou direct van toepassing moeten zijn.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Als uw computer beschikt over een geïntegreerde lichtsensor, dan zal de helderheid van het scherm automatisch voor u worden aangepast. U kunt automatische helderheid van het scherm uitschakelen in het paneel <gui>Energie</gui>.</p>
  </note>

  <p>Als het mogelijk is de helderheid van uw scherm in te stellen, dan kunt u ook het scherm automatisch dimmen om energie te sparen. Zie voor meer informatie <link xref="power-whydim"/>.</p>

</page>
