<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-notifications" xml:lang="sv">

  <info>
    <link type="guide" xref="color#problems"/>
    <link type="seealso" xref="color-why-calibrate"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Du kan bli aviserad om din färgprofil är gammal och otillförlitlig.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Kan jag bli aviserad när min färgprofil är felaktig?</title>

  <p>Du kan bli påmind att omkalibrera dina enheter efter en viss tid. Tyvärr är det inte möjligt att avgöra utan omkalibrering huruvida en enhetsprofil är otillförlitligt så det är bäst att omkalibrera enheter regelbundet.</p>

  <p>Vissa företag har väldigt specifika strategier för när kalibreringsprofiler går ut, eftersom en otillförlitlig färgprofil kan göra en stor skillnad för en slutprodukt.</p>

  <p>Om du ställer utlösningsstrategin och en profil är äldre än strategin tillåter kommer en röd varningstriangel att visas i panelen <gui>Färg</gui> intill profilen. En varningsavisering kommer också att visas varje gång du loggar in på datorn.</p>

  <p>För att sätta strategin för skärm- och skrivarenheter anger du maximal ålder för profilen i dagar:</p>

<screen>
<output style="prompt">$ </output><input>gsettings set org.gnome.settings-daemon.plugins.color recalibrate-printer-threshold 180</input>
<output style="prompt">$ </output><input>gsettings set org.gnome.settings-daemon.plugins.color recalibrate-display-threshold 90</input>
</screen>

</page>
