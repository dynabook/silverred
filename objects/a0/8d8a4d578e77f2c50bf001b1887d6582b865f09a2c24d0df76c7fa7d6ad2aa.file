<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-findip" xml:lang="sl">

  <info>
    <link type="guide" xref="net-general"/>
    <link type="seealso" xref="net-what-is-ip-address"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Poznavanje vašega naslova IP vam lahko pomaga pri odpravljanju težav z omrežjem.</desc>
  </info>

  <title>Najdite svoj naslov IP</title>

  <p>Poznavanje vašega naslova IP vam lahko pomaga pri odpravljanju težav z vašo internetno povezavo. Vaš računalnik ima <em>dva</em> naslova IP: naslov IP vašega računalnika na notranjem omrežju in naslov IP vašega računalnika na internetu.</p>

  <steps>
    <title>Najdite svoj notranji (omrežni) naslov IP</title>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Network</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Network</gui> to open the panel.</p>
    </item>
    <item>
      <p>Choose which connection, <gui>Wi-Fi</gui> or <gui>Wired</gui>, from
      the left pane.</p>
      <p>The IP address for a wired connection will be displayed on the
      right.</p>
      
      <p>Click the
      <media its:translate="no" type="image" src="figures/emblem-system.png"><span its:translate="yes">settings</span></media>
      button to see the IP address for the wireless network in the
      <gui>Details</gui> panel.</p>
    </item>
  </steps>

  <steps>
  	<title>Najdite svoj zunanji, internetni naslov IP:</title>
    <item>
      <p>Obiščite <link href="http://whatismyipaddress.com/">whatismyipaddress.com</link>.</p>
    </item>
    <item>
      <p>Spletišče vam bo za vas prikazalo vaš zunanji naslov IP.</p>
    </item>
  </steps>

  <p>Depending on how your computer connects to the internet, both of these
  addresses may be the same.</p>

</page>
