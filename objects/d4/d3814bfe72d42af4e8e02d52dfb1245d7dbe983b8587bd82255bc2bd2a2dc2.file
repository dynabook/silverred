<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-slowkeys" xml:lang="el">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Για να έχετε καθυστέρηση μεταξύ πατήματος ενός πλήκτρου και εμφάνισης αυτού του γράμματος στην οθόνη.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Ενεργοποίηση αργών πλήκτρων</title>

  <p>Ενεργοποιήστε τα <em>Αργά πλήκτρα</em> εάν θα θέλατε να υπάρχει μια καθυστέρηση μεταξύ πατήματος ενός πλήκτρου και εμφάνισης αυτού του γράμματος στην οθόνη. Αυτό σημαίνει ότι πρέπει να κρατάτε πατημένο  για λίγο, κάθε επιθυμητό πλήκτρο που θέλετε να πληκτρολογήσετε πριν εμφανιστεί. Χρησιμοποιήστε τα αργά πλήκτρα εάν πατάτε τυχαία πολλά πλήκτρα τη φορά όταν πληκτρολογείτε, ή εάν δυσκολεύεστε να πατήσετε το σωστό πλήκτρο στο πληκτρολόγιο με τη μία.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Universal Access</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Πατήστε <gui>Βοήθεια πληκτρολόγησης (AccessX)</gui> στην ενότητα <gui>Πληκτρολόγηση</gui>.</p>
    </item>
    <item>
      <p>Switch the <gui>Slow Keys</gui> switch to on.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Ενεργοποιήστε και απενεργοποιήστε γρήγορα τα αργά πλήκτρα</title>
    <p>Στο <gui>Ενεργοποίηση με το πληκτρολόγιο</gui>, επιλέξτε <gui>Ενεργοποίηση λειτουργιών προσιτότητας από το πληκτρολόγιο</gui> για ενεργοποίηση ή απενεργοποίηση των αργών πλήκτρων από το πληκτρολόγιο. Όταν επιλεχθεί αυτή η επιλογή, μπορείτε να πατήσετε και να κρατήσετε πατημένο το <key>Shift</key> για οκτώ δευτερόλεπτα για να ενεργοποιήσετε ή να απενεργοποιήσετε τα αργά πλήκτρα.</p>
    <p>Μπορείτε επίσης να ενεργοποιήσετε ή να απενεργοποιήσετε τα αργά πλήκτρα κάνοντας κλικ στο <link xref="a11y-icon">εικονίδιο προσιτότητας</link> στην πάνω γραμμή και να επιλέξετε <gui>αργά πλήκτρα</gui>. Το εικονίδιο πρόσβασης είναι ορατό όταν μία ή περισσότερες ρυθμίσεις έχουν ενεργοποιηθεί από τον πίνακα <gui>Γενική πρόσβαση</gui>.</p>
  </note>

  <p>Χρησιμοποιήστε τον ολισθητή <gui>αποδοχή καθυστέρησης</gui> για να ελέγξετε πόσο πρέπει να πατάτε ένα πλήκτρο για να καταχωριστεί.</p>

  <p>You can have your computer make a sound when you press a key, when a key
  press is accepted, or when a key press is rejected because you didn’t hold
  the key down long enough.</p>

</page>
