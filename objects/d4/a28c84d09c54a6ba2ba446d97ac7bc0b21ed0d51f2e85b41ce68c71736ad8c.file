<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="color-whatisspace" xml:lang="pt">

  <info>
    <link type="guide" xref="color#profiles"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <desc>Um espaço de cor é uma faixa definida de cores.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Que é um espaço de cor?</title>

  <p>Um espaço de cor é uma faixa de cores definido. Os espaços de cores bem documentadas incluem sRGB, AdobeRGB e ProPhotoRGB.</p>

  <p>O sistema visual humano não é um sensor RGB simples mas se pode aproximar como responde o olho humano ao diagrama de cromaticidad CIE 1931 que mostra a resposta visual humana com uma forma de ferradura. Pode ver que na visão humana se detetam muitos mais tons de verde que de azul ou vermelho. Com um espaço de cor tricromático como o RGB, se representam estas cores na computador utilizando três valores, o que se restringe a codificar um <em>triângulo</em> de cores.</p>

  <note>
    <p>Utilizar modelos tais como o diagrama de cromaticidad CIE 1931 é uma enorme simplificação do sistema visual humano e as faixas reais se expressam em 3D, em vez de em projeções 2D. Uma projeção 2D duma forma 3D pode levar a confusões, de tal forma que se quer ver a representação 3D use o aplicação <code>gcm-viewer</code>.</p>
  </note>

  <figure>
    <desc>sRGB, AdobeRGB and ProPhotoRGB representados por triângulos brancos</desc>
    <media its:translate="no" type="image" mime="image/png" src="figures/color-space.png"/>
  </figure>

  <p>Primeiro, olhe ao sRGB, que é o menor espaço e que pode codificar o menor número de cores. É uma aproximação dum ecrã CRT de 10 anos de antiguidade e a maioria dos monitores modernos podem mostrar muitos mais cores que estes. sRGB é um regular do <em>menor-denominador-comum</em> e usa-se num grande número de aplicações (incluindo Internet).</p>
  <p>AdobeRGB usa-se frequentemente como um <em>espaço de edição</em>. Pode codificar mais cores que sRGB, o que significa que pode ajustar as cores numa fotografia sem se preocupar muito de que o brilho das cores se corte ou os negros se calquem.</p>
  <p>PhoPhoto é o maior lugar disponível e usa-se frequentemente para archivar documentos. Pode codificar quase toda a faixa de cores detetadas pelo olho humano e inclusive codificar cores que o olho humano não pode detetar.</p>

  <p>
    Now, if ProPhoto is clearly better, why don’t we use it for everything?
    The answer is to do with <em>quantization</em>.
    If you only have 8 bits (256 levels) to encode each channel, then a
    larger range is going to have bigger steps between each value.
  </p>
  <p>Passos maiores significam um maior erro entre a cor capturada e a cor alojada e para algumas cores é um grande problema. Resulta que as cores fique, como as cores da pele, são importantíssimas e inclusive pequenos erros farão que uma pessoa não acostumada note que algo está mau na fotografia.</p>
  <p>Por suposto, usar 16 bits para a imagem fará que tenha mais níveis e um erro de cuantização bem mais pequeno, mas isto multiplica por dois o tamanho da cada ficheiro de imagem. A maioria do conteúdo existente hoje em dia tem 8bpp, isto é, 8 bits por pixel.</p>
  <p>O processo de gestão de cor para converter dum espaço de cor a outro, onde o espaço de cor se pode definir bem num espaço de cor sRGB ou um espaço de cor personalizada tal como o do perfil de seu monitor ou a sua impressora.</p>

</page>
