<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-autorun" xml:lang="ca">

  <info>
    <link type="guide" xref="media#photos"/>
    <link type="guide" xref="media#videos"/>
    <link type="guide" xref="media#music"/>
    <link type="guide" xref="files#removable"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-10-04" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Executa automàticament aplicacions per a CD i DVD, càmeres, reproductors d'àudio i altres dispositius i suports.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

  <!-- TODO: fix bad UI strings, then update help -->
  <title>Obrir aplicacions per a dispositius o discos</title>

  <p>Podeu iniciar una aplicació automàticament quan connecteu un dispositiu o inseriu un disc o una targeta multimèdia. Per exemple, és possible que vulgueu que el vostre organitzador de fotos s'iniciï quan connecteu una càmera digital. També podeu desactivar-ho, de manera que no passi res quan el connecteu.</p>

  <p>Per decidir quines aplicacions s'han d'iniciar quan es connectin diversos dispositius:</p>

<steps>
  <item>
    <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a escriure <gui>Detalls</gui>.</p>
  </item>
  <item>
    <p>Feu clic a <gui>Detalls</gui> per obrir el quadre.</p>
  </item>
  <item>
    <p>Feu clic a <gui>Suports extraïbles</gui>.</p>
  </item>
  <item>
    <p>Cerqueu el tipus de dispositiu o suport desitjat i, a continuació, seleccioneu una aplicació o acció per a aquest tipus de suport. Vegeu a continuació per obtenir una descripció dels diferents tipus de dispositius i suports.</p>
    <p>En lloc d'iniciar una aplicació, també podeu configurar-la perquè el dispositiu es mostri al gestor de fitxers, amb l'opció. <gui>Obre la carpeta</gui>. Quan això succeeix, se us preguntarà què fer, o automàticament no passarà res.</p>
  </item>
  <item>
    <p>Si no veieu el tipus de dispositiu o tipus de suport que voleu canviar a la llista (com ara discs Blu-ray o lectors de llibres electrònics), feu clic a <gui>Altres suports...</gui> per veure una llista més detallada de dispositius. Seleccioneu el tipus de dispositiu o suport del menú desplegable <gui>Tipus</gui> i l'aplicació o acció del menú desplegable <gui>Acció</gui>.</p>
  </item>
</steps>

  <note style="tip">
    <p>Si no voleu que s'obri cap aplicació automàticament, sigui quina sigui la vostra connexió, seleccioneu <gui>No sol·liciteu ni inicieu programes quan s'insereixi un suport extern</gui> a la part inferior de la finestra <gui>Detalls</gui>.</p>
  </note>

<section id="files-types-of-devices">
  <title>Tipus de dispositius i suports</title>
<terms>
  <item>
    <title>Discs d'àudio</title>
    <p>Trieu la vostra aplicació de música preferida o un extractor de CD per gestionar els CD d'àudio. Si utilitzeu DVD d'àudio (DVD-A), seleccioneu com obrir-los en <gui>Altres suports...</gui>. Si obriu un disc d'àudio amb el gestor de fitxers, les pistes es mostraran com a fitxers WAV que podeu reproduir en qualsevol aplicació de reproducció d'àudio.</p>
  </item>
  <item>
    <title>Discs de vídeo</title>
    <p>Trieu la vostra aplicació de vídeo preferida per gestionar DVD de vídeo. Utilitzeu el botó <gui>Altres mitjans...</gui> per configurar una aplicació per a reproductors Blu-ray, HD DVD, CD de vídeo (VCD) i super CD de vídeo (SVCD). Si els DVD o altres discs de vídeo no funcionen correctament quan els inseriu, consulteu <link xref="video-dvd"/>.</p>
  </item>
  <item>
    <title>Discs en blanc</title>
    <p>Utilitzeu el botó <gui>Altres mitjans...</gui> per seleccionar una aplicació de gravació de disc per CDs en blanc, DVDs en blanc, discs Blu-ray en blanc i HD DVD en blanc.</p>
  </item>
  <item>
    <title>Càmeres i fotos</title>
    <p>Utilitzeu el menú desplegable <gui>Fotos</gui> per triar l'aplicació de gestió fotogràfica que s'executarà quan connecti la seva càmera digital o quan insereixi una targeta multimèdia des d'una càmera, com ara una targeta CF, SD, MMC o MS. També podeu navegar les vostres fotos mitjançant el gestor de fitxers.</p>
    <p>Des d'<gui>Altres suports...</gui>, podeu seleccionar una aplicació per obrir CDs de fotografies Kodak, com ara les que podeu haver fet en una botiga. Aquests són CD normals de dades amb imatges JPEG en una carpeta anomenada <file>Imatges</file>.</p>
  </item>
  <item>
    <title>Reproductors de música</title>
    <p>Trieu una aplicació per gestionar la biblioteca de música del vostre reproductor de música portàtil, o gestioneu els fitxers des del gestor de fitxers.</p>
    </item>
    <item>
      <title>Lectors de llibres digitals</title>
      <p>Utilitzeu el botó <gui>Altres suports...</gui> per triar una aplicació per administrar els llibres al vostre lector de llibres electrònics o administrar els fitxers usant el gestor de fitxers.</p>
    </item>
    <item>
      <title>Programari</title>
      <p>Alguns discs i suports extraïbles contenen programari que se suposa s'executa automàticament quan s'introdueix el suport. Utilitzeu l'opció <gui>Programari</gui> per controlar què fer quan s'inclouen els suports amb el programari autorun. Sempre se us demanarà una confirmació abans d'executar el programari.</p>
      <note style="warning">
        <p>No utilitzeu mai programari dels suports en què no confieu.</p>
      </note>
   </item>
</terms>

</section>

</page>
