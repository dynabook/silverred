<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-calibrationdevices" xml:lang="sv">

  <info>

    <link type="guide" xref="color#calibration"/>

    <desc>Vi har stöd för ett stort antal kalibreringsenheter.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Vilka färgmätningsinstrument finns det stöd för?</title>

  <p>GNOME förlitar sig på Argyll-färghanteringssystemet för stöd för färginstrument. Därmed finns det stöd för följande mätinstrument för skärmar:</p>

  <list>
    <item><p>Gretag-Macbeth i1 Pro (spektrometer)</p></item>
    <item><p>Gretag-Macbeth i1 Monitor (spektrometer)</p></item>
    <item><p>Gretag-Macbeth i1 Display 1, 2 eller LT (färgkalibrator)</p></item>
    <item><p>X-Rite i1 Display Pro (färgkalibrator)</p></item>
    <item><p>X-Rite ColorMunki Design eller Photo (spektrometer)</p></item>
    <item><p>X-Rite ColorMunki Create (färgkalibrator)</p></item>
    <item><p>X-Rite ColorMunki Display (färgkalibrator)</p></item>
    <item><p>Pantone Huey (färgkalibrator)</p></item>
    <item><p>MonacoOPTIX (färgkalibrator)</p></item>
    <item><p>ColorVision Spyder 2 och 3 (färgkalibrator)</p></item>
    <item><p>Colorimètre HCFR (färgkalibrator)</p></item>
  </list>

  <note style="tip">
   <p>Pantone Huey är för närvarande den billigaste hårdvaran och också den med bäst stöd i Linux.</p>
  </note>

  <p>Tack vare Argyll finns det också stöd för ett antal fläck- och bandläsande reflektiva spektrometrar som kan hjälpa dig att kalibrera och karakterisera dina skrivare:</p>

  <list>
    <item><p>X-Rite DTP20 ”Pulse” (reflektiv spektrometer av ”svep”-typ)</p></item>
    <item><p>X-Rite DTP22 Digital Swatchbook (reflektiv spektrometer av fläcktyp)</p></item>
    <item><p>X-Rite DTP41 (reflektiv spektrometer av fläck- och bandläsande typ)</p></item>
    <item><p>X-Rite DTP41T (reflektiv spektrometer av fläck- och bandläsande typ)</p></item>
    <item><p>X-Rite DTP51 (reflektiv spektrometer av fläcktyp)</p></item>
  </list>

</page>
