<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="look-resolution" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="seealso" xref="look-display-fuzzy"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.28" date="2018-07-19" status="review"/>
    <revision pkgversion="3.33" date="2019-07-19" status="candidate"/>
    <revision pkgversion="3.34" date="2019-11-12" status="review"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Altere a resolução da tela e a sua orientação (rotação).</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

  <title>Alterando a resolução ou orientação da tela</title>

  <p>Você pode alterar quão grande (ou quão detalhadas) as coisas aparecem na tela alterando a <em>resolução da tela</em>. Você pode alterar em que direção elas aparecem (por exemplo, se você tiver girado o monitor) alterando a <em>rotação</em>.</p>

  <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Telas</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Telas</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Se você tem múltiplos monitores e eles não estão espelhados, você pode ter configurações diferentes em cada monitor. Selecione um monitor na área de visualização.</p>
    </item>
    <item>
      <p>Selecione a ordentação, resolução ou escala, e taxa de atualização.</p>
    </item>
    <item>
      <p>Clique em <gui>Aplicar</gui>. As novas configurações serão aplicadas por 20 segundos antes de reverteram aos valores anteriores. Dessa forma, se você não conseguir ver nada com as novas configurações, as antigas serão automaticamente restauradas. Se estiver satisfeito com os seus novos ajustes, clique em <gui>Manter esta configuração</gui>.</p>
    </item>
  </steps>

<section id="resolution">
  <title>Resolução</title>

  <p>A resolução é a quantidade de pixels (pontos na tela) em cada direção que se pode exibir. Cada resolução tem uma <em>taxa de proporção</em>, a razão entre a largura e a altura. Monitores de tela larga usam uma taxa de proporção de 16:9, enquanto os tradicionais usam 4:3. Se você escolher uma resolução que não tenha a taxa de proporção do seu monitor, a tela terá faixas pretas para evitar distorção, adicionando barras pretas no topo e baixo ou ambas laterais da tela.</p>

  <p>Você pode escolher a resolução que preferir na lista suspensa <gui>Resolução</gui>. Se escolher uma que não é certa para a sua tela, ela pode <link xref="look-display-fuzzy">exibir borrada ou quadriculada</link>.</p>

</section>

<section id="native">
  <title>Resolução nativa</title>

  <p>A <em>resolução nativa</em> de uma tela de notebook ou monitor LCD é a que funciona melhor: os pixels no sinal de vídeo se alinharão precisamente com os pixels na tela. Quando a tela é necessária para mostrar outras resoluções, a interpolação é necessária para representar os pixels, causando uma perda de qualidade de imagem.</p>

</section>

<section id="refresh">
  <title>Taxa de atualização</title>

  <p>A taxa de atualização é o número de vezes por segundo em que a imagem da tela é desenhada ou atualizada.</p>
</section>

<section id="scale">
  <title>Escala</title>

  <p>A configuração da escala aumenta o tamanho dos objetos mostrados na tela para corresponder à densidade da tela, facilitando a leitura. Escolha <gui>100%</gui> ou <gui>200%</gui>.</p>

</section>

<section id="orientation">
  <title>Orientação</title>

  <p>Em alguns dispositivos, você pode girar fisicamente a tela em várias direções. Clique em <gui>Orientação</gui> no painel e escolha entre <gui>Paisagem</gui>, <gui>Retrato direito</gui>, <gui>Retrato esquerdo</gui> ou <gui>Paisagem (virado)</gui>.</p>

  <note style="tip">
    <p>Se o dispositivo girar a tela automaticamente, você poderá bloquear a rotação atual usando o botão <media its:translate="no" type="image" src="figures/rotation-locked-symbolic.svg"><span its:translate="yes">bloqueio de rotação</span></media> na parte inferior do <gui xref="shell-introduction#systemmenu">menu do sistema</gui>. Para desbloquear, pressione o botão <media its:translate="no" type="image" src="figures/rotation-allowed-symbolic.svg"><span its:translate="yes">desbloqueio de rotação</span></media></p>
  </note>

</section>

</page>
