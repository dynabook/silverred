<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="backup-restore" xml:lang="el">

  <info>
    <link type="guide" xref="files#backup"/>
    <desc>Ανακτήστε τα αρχεία σας από ένα αντίγραφο ασφαλείας.</desc>
    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit>
      <name>Έργο Τεκμηρίωσης GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

<title>Επαναφορά ενός αντιγράφου ασφαλείας</title>

  <p>Εάν χάσατε ή διαγράψατε κάποια από τα αρχεία σας, αλλά έχετε ένα αντίγραφο τους, μπορείτε να τα ανακτήσετε από το αντίγραφο:</p>

<list>
 <item><p>Εάν θέλετε να ανακτήσετε το αντίγραφο ασφαλείας σας από μια συσκευή όπως έναν εξωτερικό σκληρό δίσκο, μια συσκευή USB ή άλλο υπολογιστή στο δίκτυο, μπορείτε να <link xref="files-copy">τα αντιγράψετε</link> πίσω στον υπολογιστή σας.</p></item>

 <item><p>Εάν δημιουργήσατε το αντίγραφο ασφαλείας σας χρησιμοποιώντας μια εφαρμογή εφεδρικού όπως το <app>Déjà Dup</app>, συνιστάται να χρησιμοποιήσετε την ίδια εφαρμογή για επαναφορά του αντίγραφου σας. Επιθεωρήστε την βοήθεια εφαρμογής για το πρόγραμμα εφεδρικού: θα σας δώσει ειδικές οδηγίες πώς να επαναφέρετε τα αρχεία σας.</p></item>
</list>

</page>
