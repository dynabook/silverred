<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-templates" xml:lang="es">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Anita Reitere</name>
      <email>nitalynx@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Crear rápidamente documentos desde archivos de plantillas personalizados.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Plantillas para tipos de documentos usados comúnmente</title>

  <p>Si crea a menudo documentos basados en el mismo contenido, puede beneficiarse del uso de archivos de plantilla. Un archivo de plantilla puede ser un documento de cualquier tipo con el formato o el contenido que quiera reutilizar. Por ejemplo, puede crear una plantilla de documento con su membrete.</p>

  <steps>
    <title>Crear una plantilla nueva</title>
    <item>
      <p>Cree un documento que vaya a usar como una plantilla. Por ejemplo, puede hacer su membrete en una aplicación de procesado de textos.</p>
    </item>
    <item>
      <p>Guarde el archivo con el contenido de la plantilla en la carpeta <file>Plantillas</file> en su carpeta <file>Personal</file>. Si la carpeta <file>Plantillas</file> no existe deberá crearla primero.</p>
    </item>
  </steps>

  <steps>
    <title>Usar una plantilla para crear un documento</title>
    <item>
      <p>Abra la carpeta donde quiere ubicar el documento nuevo.</p>
    </item>
    <item>
      <p>Pulse con el botón derecho en cualquier lugar del espacio vacío en la carpeta, luego elija <gui style="menuitem">Nuevo documento</gui>. Los nombres de las plantillas disponibles se listarán en el submenú.</p>
    </item>
    <item>
      <p>Elija la plantilla que quiere de la lista.</p>
    </item>
    <item>
      <p>Pulse dos veces sobre el archivo para abrirlo y empezar la edición. Puede querer <link xref="files-rename">renombrar el archivo</link> cuando termine.</p>
    </item>
  </steps>

</page>
