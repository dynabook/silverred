<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-import-windows" xml:lang="pl">

  <info>
    <link type="guide" xref="color#problems"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <link type="seealso" xref="color-gettingprofiles"/>
    <desc>Jak zaimportować istniejący profil ICC za pomocą systemu Windows.</desc>
    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Instalowanie profilu ICC w systemie Microsoft Windows</title>
  <p>Metoda przydzielania profilu do urządzenia oraz używania osadzonych krzywych kalibracji różni się w zależności od wersji systemu Microsoft Windows.</p>
  <section id="xp">
    <title>Windows XP</title>
    <p>Kliknij profil w Eksploratorze Windows prawym przyciskiem myszy i wybierz <gui>Zainstaluj profil</gui>. Automatycznie skopiuje to profil do właściwego katalogu.</p>
    <p>Następnie otwórz <guiseq><gui>Panel sterowania</gui><gui>Kolor</gui></guiseq> i dodaj profil do urządzenia.</p>
    <note style="warning">
      <p>Powyższy skrót nie działa, jeśli zastępowany jest istniejący profil w systemie Windows XP. Profile muszą zostać ręcznie skopiowane do katalogu <file>C:\Windows\system32\spool\drivers\color</file>, aby oryginalny profil został zastąpiony.</p>
    </note>
    <p>Windows XP wymaga uruchamiania programu do kopiowania krzywych kalibracji profilu do karty graficznej podczas włączania systemu. Można do tego używać programów <app>Adobe Gamma</app>, <app>LUT Loader</app> lub bezpłatnego <app href="https://www.microsoft.com/download/en/details.aspx?displaylang=en&amp;id=12714">Microsoft Color Control Panel Applet</app>. Ten ostatni dodaje nowy element <gui>Kolor</gui> do panelu sterowania i umożliwia łatwe ustawianie krzywych kalibracji z domyślnego profilu podczas każdego uruchamiania systemu.</p>
  </section>

  <section id="vista">
    <title>Windows Vista</title>
    <p>Microsoft Windows Vista błędnie usuwa krzywe kalibracji z LUT obrazu po zalogowaniu, uśpieniu i wyświetleniu ekranu UAC. Oznacza to, że krzywe kalibracji profilu ICC muszą być ręcznie wczytywane ponownie. Jeśli używane są profile z osadzonymi krzywymi kalibracji, to należy uważać, czy stan kalibracji nie został wyczyszczony.</p>
  </section>

  <section id="7">
    <title>Windows 7</title>
    <p>Windows 7, podobnie jak system Linux, umożliwia instalowanie profili dla całego systemu lub dla konkretnego użytkownika. Są one jednak przechowywane w tym samym miejscu. Kliknij profil prawym przyciskiem myszy w Eksploratorze Windows i wybierz <gui>Zainstaluj profil</gui> lub skopiuj plik .icc do katalogu <file>C:\Windows\system32\spool\drivers\color</file>.</p>
    <p>Otwórz <guiseq><gui>Panel sterowania</gui><gui>Zarządzanie kolorami</gui></guiseq>, a następnie dodać profil do systemu klikając przycisk <gui>Dodaj</gui>. Kliknij kartę <gui>Zaawansowane</gui> i poszukaj <gui>Kalibracji ekranu</gui>. Wczytywanie krzywych kalibracji można włączyć za pomocą pola wyboru <gui>Używaj kalibracji ekranu Windows</gui>, ale jest ono nieaktywne. Można to zmienić klikając <gui>Zmień domyślne ustawienia systemu</gui>, a następnie wracając do karty <gui>Zaawansowane</gui> i klikając to pole.</p>
    <p>Zamknij okno dialogowe i kliknij <gui>Ponownie wczytaj obecne kalibracje</gui>, aby ustawić progi gammy. Krzywe kalibracji profilu powinny być teraz ustawiane podczas każdego uruchomienia systemu.</p>
  </section>

</page>
