<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-what" xml:lang="ca">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Feu una còpia de seguretat de qualsevol cosa que no pugueu perdre si alguna cosa va malament.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

  <title>Què s'hauria d'incloure en una còpia de seguretat</title>

  <p>La vostra prioritat hauria de ser fer còpies de seguretat dels <link xref="backup-thinkabout">fitxers més importants</link>, així com d'aquells que són costosos de refer. Un exemple de més a menys important podria ser:</p>

<terms>
 <item>
  <title>Els fitxers personals</title>
   <p>Això inclou els documents, els llibres de càlcul, els correus electrònics, les cites del calendari, les dades financeres, les fotografies de la família i qualsevol cosa que considereu important. Aquests són, sens dubte, els més importants, ja que són irreemplaçables.</p>
 </item>

 <item>
  <title>Els paràmetres de configuració personals</title>
   <p>Això inclou els canvis que hàgiu fet als colors, fons de pantalla, resolució de pantalla i paràmetres de configuració del ratolí de l'escriptori. També inclou les preferències de les aplicacions, com per exemple els paràmetres de configuració del <app>LibreOffice</app>, el reproductor de música i el gestor de correu electrònic. Aquests són reemplaçables, però pot dur una mica de temps refer-los.</p>
 </item>

 <item>
  <title>Paràmetres del sistema</title>
   <p>La majoria de persones no canvia mai els paràmetres de configuració de la instal·lació per defecte. Si, en canvi, heu personalitzat el sistema, o utilitzeu l'ordinador com a servidor, considereu també fer còpies de seguretat d'aquests paràmetres de configuració.</p>
 </item>

 <item>
  <title>Programari instal·lat</title>
   <p>Normalment, el programari es pot recuperar de forma ràpida després d'haver tingut algun problema amb l'ordinador si, simplement, es torna a instal·lar.</p>
 </item>
</terms>

  <p>En general, voldreu tenir còpies de seguretat d'aquells fitxers que són irreemplaçables o que tardaríeu massa en refer. En canvi, tot allò que sigui fàcil de recuperar o tornar a fer pot interessar-vos no copiar-ho per estalviar-vos l'espai que ocuparien al suport on feu les còpies de seguretat.</p>

</page>
