<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="getting-started" version="1.0 if/1.0" xml:lang="as">

<info>
    <link type="guide" xref="index" group="gs"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <desc>GNOME লৈ নতুন? কেনেকৈ পৰিচিত হ'ব শিকক।</desc>
    <title type="link">Getting started with GNOME</title>
    <title type="text">আৰম্ভণি</title>
</info>

<title>আৰম্ভণি</title>

<if:choose>
<if:when test="!platform:gnome-classic">

  <ui:overlay width="235" height="145">
  <media type="video" its:translate="no" src="figures/gnome-launching-applications.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-launching-apps.svg">
      <ui:caption its:translate="yes"><desc style="center">এপ্লিকেচনসমূহ আৰম্ভ কৰক</desc></ui:caption>
    </ui:thumb>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>এপ্লিকেচনসমূহ আৰম্ভ কৰা হৈছে</tt:p>
         </tt:div>
         <tt:div begin="5s" end="7.5s">
           <tt:p>আপোনাৰ মাউছ পোইন্টাৰক পৰ্দাৰ ওপৰ বাঁওফালৰ চুকত <gui>কাৰ্য্যসমূহ</gui> লৈ নিয়ক।</tt:p>
           </tt:div>
         <tt:div begin="7.5s" end="9.5s">
           <tt:p><gui>এপ্লিকেচনসমূহ দেখুৱাওক</gui> আইকন ক্লিক কৰক।</tt:p>
         </tt:div>
         <tt:div begin="9.5s" end="11s">
           <tt:p>আপুনি চলাব বিচৰা এপ্লিকেচন ক্লিক কৰক, উদাহৰণস্বৰূপ, সহায়।</tt:p>
         </tt:div>
         <tt:div begin="12s" end="21s">
           <tt:p>বিকল্পভাৱে, <gui>কাৰ্য্যসমূহ অভাৰভিউ</gui> খোলিবলে কিবৰ্ড ব্যৱহাৰ কৰক আৰু <key href="help:gnome-help/keyboard-key-super">Super</key> কি' টিপক।</tt:p>
         </tt:div>
         <tt:div begin="22s" end="29s">
           <tt:p>আপুনি আৰম্ভ কৰিব বিচৰা এপ্লিকেচনৰ নাম টাইপ কৰা আৰম্ভ কৰক।</tt:p>
         </tt:div>
         <tt:div begin="30s" end="33s">
           <tt:p>এপ্লিকেচন আৰম্ভ কৰিবলে <key>Enter</key> টিপক।</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

</if:when>
</if:choose>

  <ui:overlay width="235" height="145">
  <media type="video" its:translate="no" src="figures/gnome-task-switching.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-task-switching.svg">
        <ui:caption its:translate="yes"><desc style="center">কাৰ্য্য পৰিবৰ্তন কৰক</desc></ui:caption>
    </ui:thumb>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>কাৰ্য্য পৰিবৰ্তন কৰা হৈছে</tt:p>
         </tt:div>
         <tt:div begin="5s" end="8s">
           <tt:p if:test="!platform:gnome-classic">আপোনাৰ মাউছ পোইন্টাৰক পৰ্দাৰ ওপৰ বাঁওফালৰ চুকত <gui>কাৰ্য্যসমূহ</gui> লৈ নিয়ক।</tt:p>
         </tt:div>
         <tt:div begin="9s" end="12s">
           <tt:p>এটা কাৰ্য্যলৈ যাবলে এটা উইন্ডোত ক্লিক কৰক।</tt:p>
         </tt:div>
         <tt:div begin="12s" end="14s">
           <tt:p>To maximize a window along the left side of the screen, grab
            the window’s titlebar and drag it to the left.</tt:p>
         </tt:div>
         <tt:div begin="14s" end="16s">
           <tt:p>যেতিয়া পৰ্দাৰ আধা উজ্জ্বল হয়, উইন্ডো এৰিব।</tt:p>
         </tt:div>
         <tt:div begin="16s" end="18">
           <tt:p>To maximize a window along the right side, grab the window’s
            titlebar and drag it to the right.</tt:p>
         </tt:div>
         <tt:div begin="18s" end="20s">
           <tt:p>যেতিয়া পৰ্দাৰ আধা উজ্জ্বল হয়, উইন্ডো এৰিব।</tt:p>
         </tt:div>
         <tt:div begin="23s" end="27s">
           <tt:p><gui>উইন্ডো পৰিবৰ্তক</gui> দেখুৱাবলে <keyseq> <key href="help:gnome-help/keyboard-key-super">Super</key><key> Tab</key></keyseq> টিপক।</tt:p>
         </tt:div>
         <tt:div begin="27s" end="29s">
           <tt:p>পৰৱৰ্তী উজ্জ্বল উইন্ডো বাছিবলে <key href="help:gnome-help/keyboard-key-super">Super </key> এৰক।</tt:p>
         </tt:div>
         <tt:div begin="29s" end="32s">
           <tt:p>খোলা উইন্ডোসমূহৰে গমন কৰিবলে, <key href="help:gnome-help/keyboard-key-super">Super</key> ধৰি ৰাখক, আৰু <key>Tab</key> টিপক।</tt:p>
         </tt:div>
         <tt:div begin="35s" end="37s">
           <tt:p><gui>কাৰ্য্যসমূহ অভাৰভিউ</gui> দেখুৱাবলে <key href="help:gnome-help/keyboard-key-super">Super </key> কি' টিপক।</tt:p>
         </tt:div>
         <tt:div begin="37s" end="40s">
           <tt:p>আপুনি যি এপ্লিকেচনলৈ যাব বিচাৰে তাৰ নাম টাইপ কৰা আৰম্ভ কৰক।</tt:p>
         </tt:div>
         <tt:div begin="40s" end="43s">
           <tt:p>যেতিয়া এপ্লিকেচন আপোনাৰ সন্ধানৰ প্ৰথম ফলাফল হিচাপে উপস্থিত হয়, তালৈ যাবলে <key> Enter</key> টিপক।</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

  <ui:overlay width="235" height="145">
  <media type="video" its:translate="no" src="figures/gnome-windows-and-workspaces.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-windows-and-workspaces.svg">
          <ui:caption its:translate="yes"><desc style="center">উইন্ডো আৰু কাৰ্য্যস্থানসমূহ ব্যৱহাৰ কৰক</desc></ui:caption>
    </ui:thumb>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>উইন্ডো আৰু কাৰ্য্যস্থানসমূহ</tt:p>
         </tt:div>
         <tt:div begin="6s" end="10s">
           <tt:p>To maximize a window, grab the window’s titlebar and drag it to
            the top of the screen.</tt:p>
           </tt:div>
         <tt:div begin="10s" end="13s">
           <tt:p>যেতিয়া পৰ্দা উজ্জ্বল হয়, উইন্ডো এৰিব।</tt:p>
         </tt:div>
         <tt:div begin="14s" end="20s">
           <tt:p>To unmaximize a window, grab the window’s titlebar and drag it
            away from the edges of the screen.</tt:p>
         </tt:div>
         <tt:div begin="25s" end="29s">
           <tt:p>উইন্ডোক টানি আনিবলে আৰু ইয়াক সৰু কৰিবলে আপুনি ওপৰ বাৰত ক্লিক কৰিলেও হ'ব।</tt:p>
         </tt:div>
         <tt:div begin="34s" end="38s">
           <tt:p>To maximize a window along the left side of the screen, grab
            the window’s titlebar and drag it to the left.</tt:p>
         </tt:div>
         <tt:div begin="38s" end="40s">
           <tt:p>যেতিয়া পৰ্দাৰ আধা উজ্জ্বল হয়, উইন্ডো এৰিব।</tt:p>
         </tt:div>
         <tt:div begin="41s" end="44s">
           <tt:p>To maximize a window along the right side of the screen, grab
            the window’s titlebar and drag it to the right.</tt:p>
         </tt:div>
         <tt:div begin="44s" end="48s">
           <tt:p>যেতিয়া পৰ্দাৰ আধা উজ্জ্বল হয়, উইন্ডো এৰিব।</tt:p>
         </tt:div>
         <tt:div begin="54s" end="60s">
           <tt:p>কি'বৰ্ড ব্যৱহাৰ কৰি এটা উইন্ডোক ডাঙৰ কৰিবলে, <key href="help:gnome-help/keyboard-key-super">Super</key> কি'ক টিপি ধৰি <key>↑</key> টিপক।</tt:p>
         </tt:div>
         <tt:div begin="61s" end="66s">
           <tt:p>উইন্ডোক তাৰ প্ৰকৃত অৱস্থালৈ পুনৰ লৈ যাবলে, <key href="help:gnome-help/keyboard-key-super">Super</key> কি' টিপি ধৰি <key>↓</key> টিপক।</tt:p>
         </tt:div>
         <tt:div begin="66s" end="73s">
           <tt:p>এটা উইন্ডোক পৰ্দাৰ সোঁফালে ডাঙৰ কৰিবলে, <key href="help:gnome-help/keyboard-key-super">Super</key> কি'ক টিপি ধৰি <key>→</key> টিপক।</tt:p>
         </tt:div>
         <tt:div begin="76s" end="82s">
           <tt:p>এটা উইন্ডোক পৰ্দাৰ বাঁওফালে ডাঙৰ কৰিবলে, <key href="help:gnome-help/keyboard-key-super">Super</key> কি'ক টিপি ধৰি <key>←</key> টিপক।</tt:p>
         </tt:div>
         <tt:div begin="83s" end="89s">
           <tt:p>বৰ্তমান কাৰ্য্যস্থানৰ তলত এটা কাৰ্য্যস্থানক স্থানান্তৰ কৰিবলে, <keyseq><key href="help:gnome-help/keyboard-key-super">Super </key><key>Page Down</key></keyseq> টিপক।</tt:p>
         </tt:div>
         <tt:div begin="90s" end="97s">
           <tt:p>বৰ্তমান কাৰ্য্যস্থানৰ ওপৰত এটা কাৰ্য্যস্থানক স্থানান্তৰ কৰিবলে, <keyseq><key href="help:gnome-help/keyboard-key-super">Super </key><key>Page Up</key></keyseq> টিপক।</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

<links type="topic" style="grid" groups="tasks">
<title style="heading">Common tasks</title>
</links>

<links type="guide" style="heading nodesc">
<title/>
</links>

</page>
