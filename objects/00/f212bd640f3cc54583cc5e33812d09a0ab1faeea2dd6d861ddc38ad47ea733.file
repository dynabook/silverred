<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-use-windows-workspaces" xml:lang="te">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>జాకబ్ స్టైనర్</name>
    </credit>
    <credit type="author">
      <name>పెత్ర్ కోవర్</name>
    </credit>
    <link type="guide" xref="getting-started" group="tasks"/>
    <title role="trail" type="link">విండోలు మరియు పనిస్థలాలు వుపయోగించుము</title>
    <link type="seealso" xref="shell-windows-switching"/>
    <title role="seealso" type="link">విండోలు మరియు పనిస్థలాలు వుపయోగించుటపై ట్యుటోరియల్</title>
    <link type="next" xref="gs-use-system-search"/>
  </info>

  <title>విండోలు మరియు పనిస్థలాలు వుపయోగించు</title>

  <ui:overlay width="812" height="452">
  <media type="video" its:translate="no" src="figures/gnome-windows-and-workspaces.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-windows-and-workspaces.svg"/>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>విండోలు మరియు పనిస్థలాలు</tt:p>
         </tt:div>
         <tt:div begin="6s" end="10s">
           <tt:p>To maximize a window, grab the window’s titlebar and drag it to
            the top of the screen.</tt:p>
           </tt:div>
         <tt:div begin="10s" end="13s">
           <tt:p>తెర ఉద్దీపనం కాగానే, విండోను వదులుము.</tt:p>
         </tt:div>
         <tt:div begin="14s" end="20s">
           <tt:p>To unmaximize a window, grab the window’s titlebar and drag it
            away from the edges of the screen.</tt:p>
         </tt:div>
         <tt:div begin="25s" end="29s">
           <tt:p>విండో పరిమాణం తగ్గించుటకు మీరు పై పట్టీను నొక్కి విండోను లాగివేయవచ్చు.</tt:p>
         </tt:div>
         <tt:div begin="34s" end="38s">
           <tt:p>To maximize a window along the left side of the screen, grab
            the window’s titlebar and drag it to the left.</tt:p>
         </tt:div>
         <tt:div begin="38s" end="40s">
           <tt:p>తెర అర్ధభాగం ఉద్దీపనం కాగానే, విండోను వదులుము.</tt:p>
         </tt:div>
         <tt:div begin="41s" end="44s">
           <tt:p>To maximize a window along the right side of the screen, grab
            the window’s titlebar and drag it to the right.</tt:p>
         </tt:div>
         <tt:div begin="44s" end="48s">
           <tt:p>తెర అర్ధభాగం ఉద్దీపనం కాగానే, విండోను వదులుము.</tt:p>
         </tt:div>
         <tt:div begin="54s" end="60s">
           <tt:p>కీబోర్డు వుపయోగించి విండోను పెద్దది చేయుటకు, <key href="help:gnome-help/keyboard-key-super">Super</key> కీ నొక్కిపట్టి <key>↑</key> వత్తుము.</tt:p>
         </tt:div>
         <tt:div begin="61s" end="66s">
           <tt:p>విండోను దాని క్రితం పరిమాణంకు తిప్పివుంచుటకు, <key href="help:gnome-help/keyboard-key-super">Super</key> కీ నొక్కిపట్టి <key>↓</key> వత్తండి.</tt:p>
         </tt:div>
         <tt:div begin="66s" end="73s">
           <tt:p>విండోను తెర కుడివైపునకు పెద్దది చేయుటకు, <key href="help:gnome-help/keyboard-key-super">Super</key> కీ నొక్కిపట్టి <key>→</key> వత్తండి.</tt:p>
         </tt:div>
         <tt:div begin="76s" end="82s">
           <tt:p>విండోను తెర ఎడమవైపునకు పెద్దది చేయుటకు, <key href="help:gnome-help/keyboard-key-super">Super</key> కీ నొక్కిపట్టి <key>←</key> వత్తండి.</tt:p>
         </tt:div>
         <tt:div begin="83s" end="89s">
           <tt:p>ప్రస్తుత పనిస్థలమునకు కిందన వున్న పనిస్థలమునకు కదుల్చుటకు, <keyseq><key href="help:gnome-help/keyboard-key-super">Super </key><key>Page Down</key></keyseq> వత్తండి.</tt:p>
         </tt:div>
         <tt:div begin="90s" end="97s">
           <tt:p>ప్రస్తుత పనిస్థలమునకు పైని పనిస్థలమునకు కదుల్చుటకు,  <keyseq><key href="help:gnome-help/keyboard-key-super">Super </key><key>Page Up</key></keyseq> వత్తండి.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>
  
  <section id="use-workspaces-and-windows-maximize">
    <title>విండోలను పెద్దవిచేయి మరియు పరిమాణంతగ్గించు</title>
    <p/>
    
    <steps>
      <item><p>To maximize a window so that it takes up all of the space on
       your desktop, grab the window’s titlebar and drag it to the top of the
       screen.</p></item>
      <item><p>తెర ఉద్దీపనం చెందగానే, దానిని పెద్దదిచేయుటకు విండోను వదులుము.</p></item>
      <item><p>To restore a window to its unmaximized size, grab the window’s
       titlebar and drag it away from the edges of the screen.</p></item>
    </steps>
    
  </section>

  <section id="use-workspaces-and-windows-tile">
    <title>టైల్ విండోలు</title>
    <p/>
    
    <steps>
      <item><p>To maximize a window along a side of the screen, grab the window’s
       titlebar and drag it to the left or right side of the screen.</p></item>
      <item><p>తెర అర్ధభాగం ఉద్దీపనం కాగానే, విండోను ఎంపికచేసిన తెర వైపుకు పెద్దది చేయుటకు దానిని వదులుము.</p></item>
      <item><p>వెండు విండోలను పక్క-పక్కనే పెద్దవి చేయుటకు, రెండో విండో యొక్క శీర్షిక పట్టీను పట్టీ దానిని తెరకు వ్యతిరేక దిశలో లాగుము.</p></item>
       <item><p>తెర అర్ధభాగం ఉద్దీపనం కాగానే, విండోను ఎంపికచేసిన తెర వ్యతిరేక దిశకు పెద్దది చేయుటకు దానిని వదులుము.</p></item>
    </steps>
    
  </section>
  
  <section id="use-workspaces-and-windows-maximize-keyboard">
    <title>కీబోర్డు వుపయోగించి విండోలను పెద్దవిచేయి మరియు పరిమాణంతగ్గించు</title>
    
    <steps>
      <item><p>కీబోర్డు వుపయోగించి విండోను పెద్దది చేయుటకు, <key href="help:gnome-help/keyboard-key-super">Super</key> కీ నొక్కిపట్టి <key>↑</key> వత్తుము.</p></item>
      <item><p>కీబోర్డు వుపయోగించి విండోను గతపరిమాణంకు వుంచుటకు, <key href="help:gnome-help/keyboard-key-super">Super</key> పట్టివుంచి <key>↓</key> నొక్కుము.</p></item>
    </steps>
    
  </section>
  
  <section id="use-workspaces-and-windows-tile-keyboard">
    <title>కీబోర్డు వుపయోగించి టైల్ విండోలు</title>
    
    <steps>
      <item><p>విండోను తెర కుడివైపునకు పెద్దది చేయుటకు, <key href="help:gnome-help/keyboard-key-super">Super</key> కీ నొక్కిపట్టి <key>→</key> వత్తండి.</p></item>
      <item><p>విండోను తెర ఎడమవైపునకు పెద్దది చేయుటకు, <key href="help:gnome-help/keyboard-key-super">Super</key> కీ నొక్కిపట్టి <key>←</key> వత్తండి.</p></item>
    </steps>
    
  </section>
  
  <section id="use-workspaces-and-windows-workspaces-keyboard">
    <title>కీబోర్డు వుపయోగించి పనిస్థలాలు మార్చు</title>
    
    <steps>
    
    <item><p>ప్రస్తుత పనిస్థలం కిందవున్న పనిస్థలంకు మారుటకు, <keyseq><key href="help:gnome-help/keyboard-key-super">Super</key><key>Page Down</key></keyseq> వత్తుము.</p></item>
    <item><p>ప్రస్తుత పనిస్థలమునకు పైని పనిస్థలమునకు కదుల్చుటకు,  <keyseq><key href="help:gnome-help/keyboard-key-super">Super</key><key>Page Up</key></keyseq> వత్తండి.</p></item>

    </steps>
    
  </section>

</page>
