<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="keyboard-layout" xml:lang="es">

  <info>
    <link type="guide" xref="login#management"/>
    <revision pkgversion="3.11" date="2014-01-29" status="draft"/>

    <credit type="author copyright">
      <name>minnie_eg</name>
      <email>amany.elguindy@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Rellenar el selector de distribuciones de teclado en la pantalla de inicio de sesión.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Oliver Gutiérrez</mal:name>
      <mal:email>ogutsua@gmail.com</mal:email>
      <mal:years>2018 - 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2017 - 2019</mal:years>
    </mal:credit>
  </info>

  <title>Mostrar múltiples distribuciones de teclado en la pantalla de inicio de sesión</title>

  <p>Puede cambiar los ajustes de la distribución de teclado del sistema para añadir distribuciones de teclado alternativas que los usuarios puedan seleccionar en la pantalla de inicio de sesión. Esto puede ser util para usuarios que normalmente utilizan una distribución de teclado distinta a la predeterminada y que quieran tener esas distribuciones de teclado disponibles en la pantalla de inicio de sesión.</p>

  <steps>
    <title>Cambiar la configuración de la distribución del teclado del sistema</title>
    <item>
      <p>Busque los códigos de las distribuciones del lenguaje que desee en el archivo <file>/usr/share/X11/xkb/rules/base.lst</file> bajo la sección llamada <sys>! layout</sys>.</p>
    </item>
    <item>
      <p>Utilice la herramienta <cmd>localectl</cmd> para cambiar los ajustes de la distribución de teclado del sistema de la siguiente manera:</p>
      <screen><cmd>localectl set-x11-keymap <var>layout</var></cmd></screen>
      <p>Puede especificar múltiples distribuciones en una lista separada por comas. Por ejemplo, para configurar <sys>es</sys> como distribución predeterminada, y <sys>us</sys> como secundaria, ejecute el siguiente comando:</p>
      <screen><output>$ </output><input>localectl set-x11-keymap es,us</input></screen>
    </item>
    <item>
      <p>Cierre la sesión para comprobar que las distribuciones definidas están disponibles en la barra superior de la pantalla de inicio de sesión.</p>
    </item>
  </steps>
  <p>Tenga en cuenta que también puede usar la herramienta <cmd>localectl</cmd> para especificar el modelo de teclado, variante y opciones globales predeterminadas. Para más información consulte la página del manual <cmd>localectl</cmd>(1).</p>

  <section id="keyboard-layout-no-localectl">
  <title>Mostrar múltiples distribuciones de teclado sin usar localectl</title>

  <p>En sistemas que no facilitan la herramienta <cmd>localectl</cmd>, puede cambiar los ajustes de la distribución de teclado global editando un archivo de configuración en <file>/usr/share/X11/xorg.conf.d/</file>.</p>

  <steps>
    <title>Cambiar la configuración de la distribución del teclado del sistema</title>
    <item>
      <p>Busque los códigos de las distribuciones del lenguaje que desee en el archivo <file>/usr/share/X11/xkb/rules/base.lst</file> bajo la sección llamada <sys>! layout</sys>.</p>
    </item>
    <item>
      <p>Añada los códigos de distribución a <file>/usr/share/X11/xorg.conf.d/10-evdev.conf</file> de la siguiente manera:</p>
<screen>
Section "InputClass"
  Identifier "evdev keyboard catchall"
  MatchIsKeyboard "on"
  MatchDevicePath "/dev/input/event*"
  Driver "evdev"
  <input>Option "XkbLayout" "en,fr"</input>
EndSection
</screen>
      <p>Pueden añadirse múltiples distribuciones en una lista separada por comas, como se muestra en el ejemplo para las distribuciones Inglés (<sys>en</sys>) y Francés (<sys>fr</sys>).</p>
    </item>
    <item>
      <p>Cierre la sesión para comprobar que las distribuciones definidas están disponibles en la barra superior de la pantalla de inicio de sesión.</p>
    </item>
  </steps>

  </section>

</page>
