<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-right-click" xml:lang="gu">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="clicking"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-21" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>૨૦૧૨</years>
    </credit>
    <credit type="author">
      <name>ફીલ બુલ</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>માઇકલ હીલ</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>ઍકાટેરીના ગેરાસીમોવા</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>જમણી ક્લિક કરીને ડાબે માઉસ બટનને દબાવો અને પકડો.</desc>
  </info>

  <title>જમણું માઉસ ક્લિકનું અનુકરણ કરો</title>

  <p>તમે ડાબે માઉસ બટન પર પકડી રાખીને જમણી ક્લિક કરી શકો છો. આ ઉપયોગી છે જો તમે એકજ હાથ પર વ્યક્તિગત રીતે તમારી આંગળીને ખસેડવા માટે તેને મુશ્કેલી શોધો તો, અથવા જો ફક્ત તમારી નિર્દેશન ઉપકરણ પાસે એકજ બટન હોય તો.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Universal Access</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Universal Access</gui> to open the panel.</p>
    </item>
    <item>
      <p>Press <gui>Click Assist</gui> in the <gui>Pointing &amp; Clicking</gui>
      section.</p>
    </item>
    <item>
      <p>In the <gui>Click Assist</gui> window, switch the <gui>Simulated
      Secondary Click</gui> switch to on.</p>
    </item>
  </steps>

  <p>You can change how long you must hold down the left mouse button before it
  is registered as a right click by changing the <gui>Acceptance
  delay</gui>.</p>

  <p>To right-click with simulated secondary click, hold down the left mouse
  button where you would normally right-click, then release. The pointer fills
  with a different color as you hold down the left mouse button. Once it will
  change this color entirely, release the mouse button to right-click.</p>

  <p>Some special pointers, such as the resize pointers, do not change colors.
  You can still use simulated secondary click as normal, even if you do not get
  visual feedback from the pointer.</p>

  <p>જો તમે <link xref="mouse-mousekeys">માઉસ કી</link> ને વાપરો તો, આ તમારાં કીપેડ પર <key>5</key> કીને પકડી રાખીને જમણી ક્લિક વડે તમને પરવાનગી આપે છે.</p>

  <note>
    <p>In the <gui>Activities</gui> overview, you are always able to long-press
    to right-click, even with this feature disabled. Long-press works slightly
    differently in the overview: you do not have to release the button to
    right-click.</p>
  </note>

</page>
