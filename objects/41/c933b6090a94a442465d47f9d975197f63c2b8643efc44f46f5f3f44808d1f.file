<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-file-properties-basic" xml:lang="as">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>টিফানি এন্টপলস্কি</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>শ্বণ মেকেন্স</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>মৌলিক ফাইল তথ্য দৰ্শন কৰক, অনুমতিসমূহ সংহতি কৰক, আৰু অবিকল্পিত এপ্লিকেচনসমূহ বাছক।</desc>

  </info>

  <title>ফাইলৰ বৈশিষ্ট্যসমূহ</title>

  <p>এটা ফাইল অথবা ফোল্ডাৰৰ বিষয়ে তথ্য দৰ্শন কৰিবলৈ, ইয়াত ৰাইট-ক্লিক কৰক আৰু <gui>বৈশিষ্ট্যসমূহ</gui> বাছক। আপুনি ফাইল নিৰ্বাচন কৰি <keyseq><key>Alt</key><key>Enter</key></keyseq> টিপিব পাৰে।</p>

  <p>The file properties window shows you information like the type of file,
  the size of the file, and when you last modified it. If you need this
  information often, you
  can have it displayed in <link xref="nautilus-list">list view columns</link>
  or <link xref="nautilus-display#icon-captions">icon captions</link>.</p>

  <p>The information given on the <gui>Basic</gui> tab is explained below.
  There are also
  <gui><link xref="nautilus-file-properties-permissions">Permissions</link></gui>
  and <gui><link xref="files-open#default">Open With</link></gui> tabs. For
  certain types of files, such as images and videos, there will be an extra tab
  that provides information like the dimensions, duration, and codec.</p>

<section id="basic">
 <title>মৌলিক বৈশিষ্ট্যসমূহ</title>
 <terms>
  <item>
    <title><gui>Name</gui></title>
    <p>আপুনি এই ফিল্ড পৰিবৰ্তন কৰি ফাইলক পুনৰ নামকৰণ কৰিব পাৰিব। আপুনি এটা ফাইলক বৈশিষ্ট্যসমূহ উইন্ডোৰ বাহিৰতো পুনৰ নামকৰণ কৰিব পাৰিব। <link xref="files-rename"/> চাওক।</p>
  </item>
  <item>
    <title><gui>Type</gui></title>
    <p>This helps you identify the type of the file, such as PDF document,
    OpenDocument Text, or JPEG image. The file type determines which
    applications can open the file, among other things. For example, you
    can’t open a picture with a music player. See <link xref="files-open"/>
    for more information on this.</p>
    <p>ফাইলৰ <em>MIME ধৰণ</em> ব্ৰেকেটত দেখুৱা হয়; MIME ধৰণ এটা প্ৰামাণিক পদ্ধতি যি কমপিউটাৰসমূহে এই ধৰণৰ ফাইল প্ৰসংগ কৰিবলৈ ব্যৱহাৰ কৰে।</p>
  </item>

  <item>
    <title>সমলসমূহ</title>
    <p>This field is displayed if you are looking at the properties of a folder rather than a file. It helps you see the number of items in the folder.  If the folder includes other folders, each inner folder is counted as one item, even if it contains further items. Each file is also counted as one item. If the folder is empty, the contents will display <gui>nothing</gui>.</p>
  </item>

  <item>
    <title>আকাৰ</title>
    <p>This field is displayed if you are looking at a file (not a folder). The size of a file tells you how much disk space it takes up. This is also an indicator of how long it will take to download a file or send it in an email (big files take longer to send/receive).</p>
    <p>Sizes may be given in bytes, KB, MB, or GB; in the case of the last three, the size in bytes will also be given in parentheses. Technically, 1 KB is 1024 bytes, 1 MB is 1024 KB and so on.</p>
  </item>

  <item>
    <title>Parent Folder</title>
    <p>The location of each file on your computer is given by its <em>absolute
    path</em>. This is a unique “address” of the file on your computer, made up
    of a list of the folders that you would need to go into to find the file.
    For example, if Jim had a file called <file>Resume.pdf</file> in his Home
    folder, its parent folder would be <file>/home/jim</file> and its location
    would be <file>/home/jim/Resume.pdf</file>.</p>
  </item>

  <item>
    <title>মুক্ত স্থান</title>
    <p>This is only displayed for folders. It gives the amount of disk space
    which is available on the disk that the folder is on. This is useful for
    checking if the hard disk is full.</p>
  </item>

  <item>
    <title>অভিগম কৰা হৈছে</title>
    <p>ফাইল সৰ্বশেষ খোলাৰ তাৰিখ আৰু সময়।</p>
  </item>

  <item>
    <title>পৰিবৰ্তিত</title>
    <p>ফাইল সৰ্বশেষ পৰিবৰ্তন কৰি সংৰক্ষণ কৰাৰ তাৰিখ আৰু সময়।</p>
  </item>
 </terms>
</section>

</page>
