<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-name-location" xml:lang="de">

  <info>
    <link type="guide" xref="printing#setup"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>So ändern Sie den Namen oder den Ort eines Druckers in den Druckereinstellungen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2019.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2019.</mal:years>
    </mal:credit>
  </info>
  
  <title>Den Namen oder den Ort eines Druckers ändern</title>

  <p>Sie können den Namen oder den Ort eines Druckers in den Druckereinstellungen ändern.</p>

  <note>
    <p>Sie benötigen <link xref="user-admin-explain">Systemverwalterrechte</link>, um den Namen oder den Ort eines Druckers zu ändern.</p>
  </note>

  <section id="printer-name-change">
    <title>Den Druckernamen ändern</title>

  <p>So ändern Sie den Namen eines Druckers:</p>

  <steps>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Drucker</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Drucker</gui>, um das Panel zu öffnen.</p>
    </item>
    <item>
      <p>Klicken Sie auf den Knopf <gui style="button">Entsperren</gui> in der rechten oberen Ecke und geben Sie Ihr Passwort ein.</p>
    </item>
    <item>
      <p>Sie können den Namen des Druckers ändern, indem Sie darauf klicken und den neuen Namen des Druckers eingeben.</p>
    </item>
    <item>
      <p>Drücken Sie die <key>Eingabetaste</key>, um die Änderungen zu speichern.</p>
    </item>
  </steps>

  </section>

  <section id="printer-location-change">
    <title>Den Druckerort ändern</title>

  <p>So ändern Sie den Ort Ihres Druckers:</p>

  <steps>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Drucker</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Drucker</gui>, um das Panel zu öffnen.</p>
    </item>
    <item>
      <p>Klicken Sie auf den Knopf <gui style="button">Entsperren</gui> in der rechten oberen Ecke und geben Sie Ihr Passwort ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf den Ort des Druckers und bearbeiten Sie diesen.</p>
    </item>
    <item>
      <p>Drücken Sie die <key>Eingabetaste</key>, um die Änderungen zu speichern.</p>
    </item>
  </steps>

  </section>

</page>
