<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-airplane" xml:lang="pl">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.24" date="2017-03-26" status="final"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>

    <credit type="author">
      <name>Projekt dokumentacji GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2015</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2015</years>
    </credit>

    <desc>Otwórz ustawienia sieci i włącz tryb samolotowy.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  </info>

<title>Wyłączanie urządzeń bezprzewodowych (tryb samolotowy)</title>

<p>Jeśli komputer jest używany w samolocie (lub w innym miejscu, gdzie połączenia bezprzewodowe są zabronione), to należy wyłączyć urządzenia bezprzewodowe. Można to zrobić także z innych powodów (np. aby oszczędzać akumulator).</p>

  <note>
    <p>Użycie <em>trybu samolotowego</em> całkowicie wyłączy wszystkie połączenia bezprzewodowe, w tym połączenia Wi-Fi, 3G i Bluetooth.</p>
  </note>

  <p>Aby włączyć tryb samolotowy:</p>

  <steps>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Wi-Fi</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Wi-Fi</gui>, aby otworzyć panel.</p>
    </item>
    <item>
      <p>Przełącz <gui>Tryb samolotowy</gui> na <gui>|</gui> (włączone). Wyłączy to połączenia bezprzewodowe do chwili wyłączenia trybu samolotowego.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Można wyłączyć połączenie Wi-Fi z <gui xref="shell-introduction#systemmenu">menu systemowego</gui> klikając nazwę połączenia i wybierając <gui>Wyłącz</gui>.</p>
  </note>

</page>
