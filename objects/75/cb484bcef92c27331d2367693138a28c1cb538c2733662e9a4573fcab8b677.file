<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-batterywindows" xml:lang="ru">

  <info>
    <link type="guide" xref="power#faq"/>
    <link type="seealso" xref="power-batteryestimate"/>
    <link type="seealso" xref="power-batterylife"/>
    <link type="seealso" xref="power-batteryslow"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>

    <desc>Причиной этой проблемы могут быть тонкие настройки от производителя и отличающийся метод оценки времени работы аккумулятора.</desc>
    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Фил Булл (Phil Bull)</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Почему у меня время работы от аккумулятора ниже, чем было в Windows/Mac OS?</title>

<p>Под управлением Linux время работы некоторых компьютеров от аккумулятора оказывается меньшим, чем при работе в Windows или Mac OS. Одна из причин состоит в том, что производители этих компьютеров устанавливают специальные программы для Windows или Mac OS, которые оптимизируют различные аппаратные и программные настройки для данной модели компьютера. Эти настройки весьма специфичны и могут быть не документированными, поэтому добавить их в Linux трудно.</p>

<p>К сожалению, не существует простого способа применить эти тонкие настройки без точного знания, какой именно эффект они дают. Тем не менее, возможно, некоторые <link xref="power-batterylife">способы экономии энергии</link> смогут помочь. Также полезным может оказаться изменение <link xref="power-batteryslow">частоты процессора</link>.</p>

<p>Ещё одной возможной причиной несоответствия может быть различие в методах оценки оставшегося времени работы от аккумулятора в Windows/Mac OS и в Linux. Реальное время работы может быть одним и тем же, но разные методы дают разные оценочные значения.</p>
	
</page>
