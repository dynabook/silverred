<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-what" xml:lang="pl">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Projekt dokumentacji GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Zachowaj wszystko, czego nie chcesz stracić w przypadku awarii.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  </info>

  <title>Co zachować w kopii zapasowej</title>

  <p>Priorytetem powinny być <link xref="backup-thinkabout">najważniejsze pliki</link>, a także te trudne do odtworzenia. Na przykład, w kolejności od najważniejszych do najmniej ważnych:</p>

<terms>
 <item>
  <title>Pliki użytkownika</title>
   <p>Dokumenty, arkusze kalkulacyjne, wiadomości e-mail, spotkania w kalendarzu, dane finansowe, rodzinne zdjęcia i wszelkie inne pliki użytkownika, których nie da się zastąpić.</p>
 </item>

 <item>
  <title>Ustawienia użytkownika</title>
   <p>Zmiany kolorów, tła, rozdzielczość ekranu i ustawienia myszy. Także preferencje programów, w tym pakietu <app>LibreOffice</app>, odtwarzacza muzyki i klienta poczty. Można je zastąpić, ale może to zająć trochę czasu.</p>
 </item>

 <item>
  <title>Ustawienia systemu</title>
   <p>Większość osób nigdy nie zmienia ustawień systemu utworzonych podczas instalacji. Jeśli jednak dostosowano ustawienia systemu, albo jeśli komputer jest używany jako serwer, to można je zachować.</p>
 </item>

 <item>
  <title>Zainstalowane oprogramowanie</title>
   <p>Używane programy można zwykle całkiem szybko przywrócić po poważnej awarii, instalując je ponownie.</p>
 </item>
</terms>

  <p>Generalnie warto zachować pliki, których nie da się zastąpić oraz te, których zastąpienie bez kopii zapasowej wymaga bardzo dużo czasu. Jeśli coś można łatwo zastąpić, to nie trzeba poświęcać na to miejsca na dysku.</p>

</page>
