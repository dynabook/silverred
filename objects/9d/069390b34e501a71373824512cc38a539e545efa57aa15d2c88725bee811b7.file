<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-file-properties-basic" xml:lang="sr">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Тифани Антополоски</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Шон Мек Кенс</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Дејвид Кинг</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Погледајте основне податке о датотеци, подесите овлашћења, и изаберите основне програме.</desc>

  </info>

  <title>Својства датотеке</title>

  <p>Да погледате податке о датотеци или фасцикли, кликните десним тастером миша на њу и изаберите <gui>Особине</gui>. Можете такође да изаберете датотеку и да притиснете <keyseq><key>Алт</key><key>Унеси</key></keyseq>.</p>

  <p>Прозор особина датотеке ће вам показати податке као што је врста датотеке, величина, и када сте је последњи пут изменили. Ако су вам често потребни ови подаци, можете да их прикажете у <link xref="nautilus-list">колонама прегледа списком</link> или у <link xref="nautilus-display#icon-captions">натпису иконице</link>.</p>

  <p>Подаци дати у језичку <gui>Основно</gui> су објашњени испод. Поред њега имате такође и језичке <gui><link xref="nautilus-file-properties-permissions">Овлашћења</link></gui> и <gui><link xref="files-open#default">Отвори помоћу</link></gui>. За одређене врсте датотека, као што су слике и снимци, биће приказан додатни језичак који ће обезбеђивати податке о величини, трајању, и кодеку.</p>

<section id="basic">
 <title>Основна својства</title>
 <terms>
  <item>
    <title><gui>Назив</gui></title>
    <p>Можете да преименујете датотеку мењајући садржај овог поља. Можете такође да преименујете датотеку изван прозора особина. Погледајте <link xref="files-rename"/>.</p>
  </item>
  <item>
    <title><gui>Врста</gui></title>
    <p>Ово ће вам помоћи да одредите врсту датотеке, као што је ПДФ документ, текст Отвореног документа, или ЈПЕГ слика. Врста датотеке одређује који програми могу да отворе датотеку, уз друге ствари. На пример, не можете да отворите слику програмом за пуштање музике. Погледајте <link xref="files-open"/> за више података о овоме.</p>
    <p><em>МИМЕ врста</em> датотеке је приказана у загради; МИМЕ врста је уобичајени начин који рачунари користе да би обавестили о врсти датотеке.</p>
  </item>

  <item>
    <title>Садржај</title>
    <p>Ово поље се приказује ако разгледате особине фасцикле уместо датотеке. Помаже вам да видите број ставки у фасцикли. Ако фасцикла садржи друге фасцикле, свака унутрашња фасцикла се броји као једна ставка, чак и ако садржи додатне ставке. Свака датотека се такође броји као једна ставка. Ако је фасцикла празна, садржај ће приказати <gui>ништа</gui>.</p>
  </item>

  <item>
    <title>Величина</title>
    <p>Ово поље се приказује ако посматрате датотеку (а не фасциклу). Величина датотеке вам говори о томе колико заузима простора на диску. Ово вам такође показује колико времена ће бити потребно за преузимање датотеке или слање путем е-поште (великим датотекама је потребно више времена за слање/пријем).</p>
    <p>Величине могу бити дате у бајтовима, KB, MB, или GB; у случају последња три, величина у бајтовима ће такође бити дата у загради. Технички, 1 KB је 1024 бајта, 1 MB је 1024 KB и тако редом.</p>
  </item>

  <item>
    <title>Родитељска фасцикла</title>
    <p>Путања сваке датотеке на вашем рачунару је дата њеном <em>апсолутном путањом</em>. То је јединствена „адреса“ датотеке на вашем рачунару, коју чини списак фасцикли кроз које ћете морати да прођете да бисте пронашли датотеку. На пример, ако Пера има датотеку под називом <file>Плате.pdf</file> у својој личној фасцикли, њена родитељска фасцикла биће <file>/home/pera</file> а њена путања биће <file>/home/pera/Плате.pdf</file>.</p>
  </item>

  <item>
    <title>Слободан простор</title>
    <p>Ово се приказује само за фасцикле. Приказује износ простора диска доступног на диску на коме се налази фасцикла. Ово је корисно приликом провере заузећа чврстог диска.</p>
  </item>

  <item>
    <title>Приступљен</title>
    <p>Датум и време када је датотека била отворена последњи пут.</p>
  </item>

  <item>
    <title>Измењено</title>
    <p>Датум и време када је датотека била последњи пут измењена и сачувана.</p>
  </item>
 </terms>
</section>

</page>
