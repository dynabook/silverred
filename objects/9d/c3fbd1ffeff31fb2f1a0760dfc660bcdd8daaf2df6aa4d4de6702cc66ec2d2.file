<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-states" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>

    <revision pkgversion="3.4.0" date="2012-03-24" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>管理好工作区中的各个窗口，可以让您的工作更加高效。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>移动和缩放窗口</title>

  <p>You can move and resize windows to help you work more efficiently. In
  addition to the dragging behavior you might expect, GNOME features shortcuts
  and modifiers to help you arrange windows quickly.</p>

  <list>
    <item>
      <p>Move a window by dragging the titlebar, or hold down
      <key xref="keyboard-key-super">Super</key> and drag anywhere in the
      window. Hold down <key>Shift</key> while moving to snap the window to the
      edges of the screen and other windows.</p>
    </item>
    <item>
      <p>改变窗口大小可以拖动窗口的四个边或角，按住 <key>Shift</key> 键可以让窗口贴紧屏幕或其他窗口的边缘。</p>
      <p if:test="platform:gnome-classic">You can also resize a maximized
      window by clicking the maximize button in the titlebar.</p>
    </item>
    <item>
      <p>使用键盘来移动或改变窗口大小。按 <keyseq><key>Alt</key><key>F7</key></keyseq> 来移动窗口，或者按 <keyseq><key>Alt</key><key>F8</key></keyseq> 改变窗口大小。使用方向键移动或改变窗口大小，然后按<key>回车键</key>确定，也可以按 <key>Esc</key> 取消，返回到原来的状态。</p>
    </item>
    <item>
      <p>把窗口拖动到屏幕顶部可以<link xref="shell-windows-maximize">最大化窗口</link>，把窗口拖到屏幕的一侧，将会在屏幕这一半最大化，这样您可以将<link xref="shell-windows-tiled">两个窗口并排最大化</link>。</p>
    </item>
  </list>

</page>
