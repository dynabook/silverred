<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-mousekeys" xml:lang="ru">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="pointing"/>

    <revision pkgversion="3.8" date="2013-03-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <credit type="author">
      <name>Фил Булл (Phil Bull)</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Шон МакКенс (Shaun McCance)</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013, 2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    
    <desc>Включите «Кнопки мыши», чтобы управлять курсором с клавиатуры.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Click and move the mouse pointer using the keypad</title>

  <p>Если вам трудно управлять мышью или другим координатным устройством, можно управлять указателем мыши с помощью клавиш цифрового блока клавиатуры. Эта функция называется <em>«кнопки мыши»</em>.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Universal Access</gui>.</p>
      <p>You can access the <gui>Activities</gui> overview by pressing on it,
      by moving your mouse pointer against the top-left corner of the screen,
      by using <keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq>
      followed by <key>Enter</key>, or by using the
      <key xref="keyboard-key-super">Super</key> key.</p>
    </item>
    <item>
      <p>Click <gui>Universal Access</gui> to open the panel.</p>
    </item>
    <item>
      <!-- TODO investigate keyboard navigation, because the arrow keys show no
           visual feedback, and do not seem to work. -->
      <p>Use the up and down arrow keys to select <gui>Mouse Keys</gui> in the
      <gui>Pointing &amp; Clicking</gui> section, then press <key>Enter</key>
      to switch the <gui>Mouse Keys</gui> switch to on.</p>
    </item>
    <item>
      <p>Убедитесь, что индикатор <key>Num Lock</key> отключен. Теперь можно управлять указателем мыши с помощью цифрового блока клавиатуры.</p>
    </item>
  </steps>

  <p>The keypad is a set of numerical buttons on your keyboard, usually
  arranged into a square grid. If you have a keyboard without a keypad (such as
  a laptop keyboard), you may need to hold down the function (<key>Fn</key>)
  key and use certain other keys on your keyboard as a keypad. If you use this
  feature often on a laptop, you can purchase external USB or Bluetooth numeric
  keypads.</p>

  <p>Каждая клавиша с номером в цифровом блоке соответствует направлению. Например, нажатие клавиши <key>8</key> переместит указатель вверх, а нажатие клавиши <key>2</key> — вниз. Чтобы выполнить нажатие мышью, нажмите <key>5</key>, а для двойного нажатия быстро нажмите эту же клавишу дважды.</p>

  <p>На многих клавиатурах имеется специальная клавиша, соответствующая нажатию правой кнопкой мыши. Иногда её называют «кнопкой <key xref="keyboard-key-menu">Меню</key>». Обратите внимание, что нажатие этой клавиши соответствует нажатию правой кнопкой там, где находится фокус клавиатуры, а не там, где находится указатель мыши. См. <link xref="a11y-right-click"/>, чтобы узнать, как можно имитировать нажатие правой кнопкой при удержании нажатой клавиши <key>5</key> или левой кнопки мыши.</p>

  <p>Если при включённых «кнопках мыши» нужно воспользоваться цифровым блоком для ввода цифр, включите <key>Num Lock</key>. Помните, что при включённом <key>Num Lock</key> управлять указателем мыши с помощью цифрового блока клавиатуры невозможно.</p>

  <note>
    <p>Обычные клавиши с цифрами, находящиеся в верхнем ряду клавиатуры, не могут управлять указателем мыши. Для этого можно использовать лишь клавиши цифрового блока.</p>
  </note>

</page>
