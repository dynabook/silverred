<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="a11y task" id="a11y-screen-reader" xml:lang="gl">

  <info>
    <link type="guide" xref="a11y#vision" group="blind"/>

    <revision pkgversion="3.13.92" date="2014-09-20" status="incomplete"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Jana Heves</name>
      <email>jsvarova@gnome.org</email>
    </credit>

    <desc>Use o lector de pantalla <app>Orca</app> para ler a interface de usuario.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2020</mal:years>
    </mal:credit>
  </info>

  <title>Ler a pantalla</title>

  <p>GNOME fornece o lector de pantalla <app>Orca</app> para ler a interface de usuario. Dependendo de como instalara GNOME, pode non ter Orca instalado. Se non é así, instale Orca primeiro.</p>

  <p if:test="action:install"><link style="button" action="install:orca">Instalar Orca</link></p>
  
  <p>Para iniciar <app>Orca</app> use o teclado:</p>
  
  <steps>
    <item>
    <p>Prema <key>Super</key>+<key>Alt</key>+<key>S</key>.</p>
    </item>
  </steps>
  
  <p>Ou inicie <app>Orca</app> usando un rato e teclado:</p>

  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Actividades</gui> e comece a escribir <gui>Acceso universal</gui>.</p>
    </item>
    <item>
      <p>Prema <gui>Acceso universal</gui> para abrir o panel.</p>
    </item>
    <item>
      <p>Prema <gui>Lector de pantalla</gui> na sección <gui>Visión</gui> e logo active o <gui>Lector de pantalla</gui> no diálogo.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Activar e desactivar rapidamente o Lector de pantalla</title>
    <p>Pode activar e desactivar de forma rápida o Lector de Pantalla desde a <link xref="a11y-icon">icona de accesibilidade</link> na barra superior e seleccoine <gui>Lector de pantalla</gui>.</p>
  </note>

  <p>Visite a <link href="help:orca">axuda de Orca</link> para obter máis información.</p>
</page>
