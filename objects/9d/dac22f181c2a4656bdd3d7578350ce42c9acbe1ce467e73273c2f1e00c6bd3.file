<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-default-email" xml:lang="el">

  <info>
    <link type="guide" xref="net-email"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Αλλάξτε τον προεπιλεγμένο πελάτη ηλεκτρονικής αλληλογραφίας πηγαίνοντας στο <gui>Λεπτομέρειες</gui> στις <gui>Ρυθμίσεις</gui>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Αλλάξτε ποια εφαρμογή αλληλογραφίας χρησιμοποιείται για συγγραφή ηλεκτρονικής αλληλογραφίας</title>

  <p>Όταν κάνετε κλικ σε ένα κουμπί ή σύνδεσμο για να στείλετε μια νέα ηλεκτρονική αλληλογγραφία (για παράδειγμα, στην εφαρμογή επεξεργασίας κειμένου), η προεπιλεγμένη εφαρμογή αλληλογραφίας θα ανοίξει με ένα κενό μήνυμα, έτοιμο για να το γράψετε. Όμως, αν έχετε περισσότερες από μια εφαρμογές αλληλογραφίας εγκατεστημένες, η εσφαλμένη εφαρμογή αλληλογραφίας μπορεί να ανοίξει. Μπορείτε να το διορθώσετε αλλάζοντας την προεπιλεγμένη εφαρμογή ηλεκτρονικής αλληλογραφίας:</p>

  <steps>
    <item>
      <p>Ανοίξτε την επισκόπηση <gui xref="shell-introduction#activities">Δραστηριότητες</gui> και αρχίστε να πληκτρολογείτε <gui>Λεπτομέρειες</gui>.</p>
    </item>
    <item>
      <p>Κάντε κλικ στο <gui>Λεπτομέρειες</gui> για να ανοίξετε τον πίνακα.</p>
    </item>
    <item>
      <p>Επιλέξτε <gui>Προεπιλεγμένες εφαρμογές</gui> από τον κατάλογο στα αριστερά του παραθύρου.</p>
    </item>
    <item>
      <p>Επιλέξτε ποιος πελάτης ηλεκτρονικής αλληλογραφίας θα θέλατε να χρησιμοποιηθεί από προεπιλογή αλλάζοντας την επιλογή <gui>Αλληλογραφία</gui>.</p>
    </item>
  </steps>

</page>
