<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-name-location" xml:lang="gl">

  <info>
    <link type="guide" xref="printing#setup"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Cambie o nome e localización dunha impresora nas preferencias de impresión.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2020</mal:years>
    </mal:credit>
  </info>
  
  <title>Cambiar o tamaño e localización dunha impresora</title>

  <p>Pode cambiar o nome e localización dunha impresora nas preferencias de impresión.</p>

  <note>
    <p>You need <link xref="user-admin-explain">administrative privileges</link>
    on the system to change the name or location of a printer.</p>
  </note>

  <section id="printer-name-change">
    <title>Cambiar o nome da impresora</title>

  <p>Se quere cambiar o nome dunha impresora, siga os seguintes pasos:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Printers</gui>.</p>
    </item>
    <item>
      <p>Prema <gui>Impresoras</gui> para abrir o panel.</p>
    </item>
    <item>
      <p>Prema <gui style="button">Desbloquear</gui> na esquina superior dereita e escriba o seu contrasinal cando se lle pregunte.</p>
    </item>
    <item>
      <p>Ao premer no nome da impresora, comece a escribir un novo nome para a impresora.</p>
    </item>
    <item>
      <p>Prema <key>Intro</key> para gardar os cambios.</p>
    </item>
  </steps>

  </section>

  <section id="printer-location-change">
    <title>Cambie a localización da impresora</title>

  <p>Para cambiar a localización da súa impresora:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Printers</gui>.</p>
    </item>
    <item>
      <p>Prema <gui>Impresoras</gui> para abrir o panel.</p>
    </item>
    <item>
      <p>Prema <gui style="button">Desbloquear</gui> na esquina superior dereita e escriba o seu contrasinal cando se lle pregunte.</p>
    </item>
    <item>
      <p>Ao premer na localización comece a edición da mesma.</p>
    </item>
    <item>
      <p>Prema <key>Intro</key> para gardar os cambios.</p>
    </item>
  </steps>

  </section>

</page>
