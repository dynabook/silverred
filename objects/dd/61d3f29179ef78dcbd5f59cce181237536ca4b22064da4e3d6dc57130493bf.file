<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sharing-bluetooth" xml:lang="nl">

  <info>
    <link type="guide" xref="bluetooth"/>
    <link type="guide" xref="sharing"/>
    <link type="guide" xref="prefs-sharing"/>

    <revision pkgversion="3.8.2" date="2013-05-13" status="draft"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-08" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-13" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="author">
      <name>David King</name>
      <email>davidk@gnome.org</email>
      <years>2014-2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Het uploaden van bestanden naar uw computer via Bluetooth toestaan.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Delen van besturing via Bluetooth</title>

  <p>U kunt delen via <gui>Bluetooth</gui> inschakelen om bestanden via Bluetooth te ontvangen in de map <file>Downloads</file></p>

  <steps>
    <title>Toestaan dat bestanden worden gedeeld in uw map <file>Downloads</file></title>
    <item>
      <p>Open het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht en typ <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Klik op het <gui>Bluetooth</gui> om het paneel te openen.</p>
    </item>
    <item>
      <p>Zorg ervoor dat <link xref="bluetooth-turn-on-off"><gui>Bluetooth</gui> aan staat</link>.</p>
    </item>
    <item>
      <p>Apparaten waarop Bluetooth is ingeschakeld kunnen alleen bestanden naar de map <file>Downloads</file> sturen wanneer het <gui>Bluetooth</gui>-paneel geopend is.</p>
    </item>
  </steps>

  <note style="tip">
    <p>U kunt de naam die uw computer aan andere apparaten laat zien <link xref="sharing-displayname">wijzigen</link>.</p>
  </note>

</page>
