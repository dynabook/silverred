<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-add" xml:lang="lv">

  <info>
    <link type="guide" xref="user-accounts#manage" group="#first"/>

    <revision pkgversion="3.8" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2013-11-01" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>GNOME dokumentācijas projekts</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Pievienot jaunus lietotājus, lai cilvēki varētu ierakstīties sistēmā un lietot datoru.</desc>
  </info>

  <title>Pievienot jaunu lietotāja kontu</title>

  <p>Jūs varat pievienot datoram vairākus lietotāja kontus. Izveidojiet vienu kontu katram cilvēkam jūsu mājās vai uzņēmumā. Katram lietotājam ir sava mājas mape, dokumenti un iestatījumi.</p>

  <p>Jums ir jābūt <link xref="user-admin-explain">administratora tiesībām</link>, lai pievienotu lietotāju kontus.</p>

  <steps>
    <item>
      <p>Atveriet <gui xref="shell-introduction#activities">Aktivitāšu</gui> pārskatu un sāciet rakstīt <gui>Lietotāji</gui>.</p>
    </item>
    <item>
      <p>Spiediet <gui>Lietotāji</gui>, lai atvērtu paneli.</p>
    </item>
    <item>
      <p>Spiediet pogu <gui style="button">Atslēgt</gui> un ievadiet savu  paroli.</p>
    </item>
    <item>
      <p>Spiediet <gui style="button">+</gui> pogu zem kontu saraksta pa kreisi, lai pievienotu jaunu lietotāja kontu.</p>
    </item>
    <item>
      <p>Ja gribat, lai jaunajam lietotājam būtu <link xref="user-admin-explain">administratīva pieeja</link> datoram, izvēlēties <gui>Administratora</gui> konta tipu.</p>
      <p>Administratori var pievienot un dzēst lietotājus, instalēt programmatūru un draiverus, nomainīt datumu un laiku u.tml.</p>
    </item>
    <item>
      <p>Ievadiet jaunā lietotāja pilno vārdu. Automātiski tiks ieteikts atbilstošs lietotājvārds. Ja nepatīk piedāvātais lietotājvārds, varat to nomainīt.</p>
    </item>
    <item>
      <p>Jūs varat izvēlēties jaunā lietotāja paroli, vai ļaut viņiem pašiem to iestatīt pirmajā ierakstīšanās reizē.</p>
      <p>Ja izvēlaties iestatīt paroli tagad, varat spiest uz <gui style="button"><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16">
      <span its:translate="yes">ģenerēt paroli</span></media></gui> ikonas, lai automātiski ģenerētu nejaušu paroli.</p>
    </item>
    <item>
      <p>Spiediet <gui>Pievienot</gui>.</p>
    </item>
  </steps>

  <p>Ja vēlaties mainīt paroli pēc konta izveidošanas, izvēlieties kontu, <gui style="button">Atslēdziet</gui> paneli un spiediet uz pašreizējās paroles statusa.</p>

  <note>
    <p>Panelī <gui>Lietotāji</gui> var veikt klikšķi uz attēla līdzās lietotāja vārdam labajā pusē, lai iestatītu konta attēlu. Šis attēls parādīsies ierakstīšanās logā. GNOME piedāvā standarta attēlus, bet jūs varat arī izvēlēties kādu no savējiem vai uzņemt jaunu attēlu ar tīmekļa kameru.</p>
  </note>

</page>
