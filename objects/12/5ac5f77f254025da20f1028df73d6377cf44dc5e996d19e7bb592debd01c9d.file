<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-behavior" xml:lang="lv">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-behavior"/>

    <desc>Atvērt datnes ar vienu klikšķi, norādīt, ko darīt ar izpildāmām teksta datnēm un iestatīt miskastes uzvedību.</desc>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Sindhu S</name>
      <email>sindhus@live.in</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Datņu pārvaldnieka uzvedības iestatījumi</title>
<p>You can control whether you single-click or double-click files, how
executable text files are handled, and the trash behavior. Click the menu
button in the top-right corner of the window and select <gui>Preferences</gui>,
and select the <gui>Behavior</gui> tab.</p>

<section id="behavior">
<title>Uzvedība</title>
<terms>
 <item>
  <title><gui>Viens klikšķis atver vienumus</gui></title>
  <title><gui>Dubultklikšķis atver vienumus</gui></title>
  <p>Pēc noklusējuma viens klikšķis izvēlas datni, bet dubultklikšķis to atver. Tā vietā jūs varat izvēlēties, lai datnes un mapes atvērtos ar vienu klikšķi. Šajā režīmā jūs varat pieturēt nospiestu <key>Ctrl</key> taustiņu, lai izmantotu klikšķus, lai izvēlētos datnes.</p>
 </item>
</terms>

</section>
<section id="executable">
<title>Izpildāmās teksta datnes</title>
 <p>Izpildāma teksta datne ir datne, kas satur palaižamu (izpildāmu) programmu. Arī <link xref="nautilus-file-properties-permissions">datnes atļaujām</link> jābūt iestatītām, lai atļautu palaist datni kā programmu. Izplatītākās izpildāmās teksta datnes ir <sys>čaulas</sys>, <sys>Python</sys> un <sys>Perl</sys> skripti. To paplašinājumi ir attiecīgi <file>.sh</file>, <file>.py</file> un <file>.pl</file>.</p>
 
 <p>Atverot izpildāmu datni, jūs varat izvēlēties no:</p>
 
 <list>
  <item>
    <p><gui>Palaist izpildāmās teksta datnes, kad tās tiek atvērtas</gui></p>
  </item>
  <item>
    <p><gui>Skatīt izpildāmās teksta datnes, kad tās tiek atvērtas</gui></p>
  </item>
  <item>
    <p><gui>Vaicāt katru reizi</gui></p>
  </item>
 </list>

 <p>Ja izvēlēsities <gui>Vaicāt katru reizi</gui>, parādīsies dialoglodziņš, kas vaicās, vai vēlaties izpildīt vai skatīt izvēlēto teksta datni.</p>

 <p>Izpildāmas teksta datnes mēdz saukt arī par <em>skriptiem</em>. Visi skripti mapē <file>~/.local/share/nautilus/scripts</file> parādīsies konteksta izvēlnē, zem apakšizvēlnes <gui style="menuitem">Skripti</gui>. Kas skripts tiek izpildīts, no lokālās mapes, visas datnes tiks ielīmētas skriptos kā parametri. Lai izpildītu skriptu uz datnes:</p>

<steps>
  <item>
    <p>Dodaties uz vēlamo mapi.</p>
  </item>
  <item>
    <p>Izvēlieties vēlamo datni.</p>
  </item>
  <item>
    <p>Spiediet labo klikšķi uz datnes, lai atvērtu konteksta izvēlni un izvēlnē <gui style="menuitem">Skripti</gui> izvēlieties skriptu, ko izpildīt.</p>
  </item>
</steps>

 <note style="important">
  <p>Skriptam netiks padoti nekādi parametri, ja tas tiks izpildīts no attālinātas mapes, piemēram, mapes, kas rāda tīmekļa vai <sys>ftp</sys> saturu.</p>
 </note>

</section>

<section id="trash">
<info>
<link type="seealso" xref="files-delete"/>
<title type="link">Datņu pārvaldnieka miskastes iestatījumi</title>
</info>
<title>Miskaste</title>

<terms>
 <item>
  <title><gui>Jautāt, pirms iztukšot miskasti</gui></title>
  <p>Šī iespēja pēc noklusējuma ir ieslēgta. Iztukšojot miskasti, sistēmas ziņojums prasīs apstiprinājumu, ka tiešām vēlaties iztukšot miskasti vai dzēst datnes.</p>
 </item>
</terms>
</section>

</page>
