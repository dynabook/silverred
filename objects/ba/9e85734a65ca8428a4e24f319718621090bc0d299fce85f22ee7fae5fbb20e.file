<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wired-connect" xml:lang="it">

  <info>
    <link type="guide" xref="net-wired" group="#first"/>

    <revision pkgversion="3.4" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.28" date="2018-03-28" status="review"/>

    <credit type="author">
      <name>Progetto documentazione di GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Per configurare la maggior parte delle connessioni di rete via cavo, è necessario unicamente collegare un cavo di rete.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Connettersi a una rete via cavo (Ethernet)</title>

  <!-- TODO: create icon manually because it is one overlayed on top of another
       in real life. -->
  <p>To set up most wired network connections, all you need to do is plug in a
  network cable. The wired network icon
  (<media its:translate="no" type="image" src="figures/network-wired-symbolic.svg"><span its:translate="yes">settings</span></media>)
  is displayed on the top bar with three dots while the connection is being
  established. The dots disappear when you are connected.</p>

  <p>Se ciò non accade, per prima cosa assicurarsi di aver collegato il cavo di rete. Una estremità del cavo deve essere collegata alla porta rettangolare di rete (Ethernet) sul computer e l'altra deve essere collegata in uno switch, un router, una presa di rete a muro o simili (a seconda del proprio apparato di rete). Talvolta una luce accanto alla porta Ethernet indica che questa è collegata e attiva.</p>

  <note>
    <p>You cannot plug one computer directly into another one with a network
    cable (at least, not without some extra setting-up). To connect two
    computers, you should plug them both into a network hub, router or
    switch.</p>
  </note>

  <p>If you are still not connected, your network may not support automatic
  setup (DHCP). In this case you will have to <link xref="net-manual">configure
  it manually</link>.</p>

</page>
