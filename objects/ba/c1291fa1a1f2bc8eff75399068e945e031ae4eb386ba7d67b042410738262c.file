<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-othercountry" xml:lang="cs">

  <info>
    <link type="guide" xref="power#problems"/>
    <desc>Váš počítač fungovat bude, ale možná budete potřebovat jiný napájecí kabel nebo cestovní adaptér.</desc>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

<title>Bude můj počítač fungovat s napájením v jiné zemi?</title>

<p>Různé země používají napájecí rozvody s různým napětím (obvykle 220 až 240 V nebo 110 V) a střídavé frekvence (obvykle 50 Hz nebo 60 Hz). Váš počítač může fungovat s napájením v různých zemích, pokud máte příslušný adaptér. Někdy to bývá řešeno přepínačem.</p>

<p>Pokud máte notebook, je vše co potřebujete udělat, získat napájecí šňůru se správnou koncovkou. Některé notebooky jsou dodávané v balení s více typy šňůr pro váš napájecí adaptér, takže možná už tu správnou máte. Pokud ne, stačí vaši současnou šňůru zasunout do běžného cestovního adaptéru.</p>

<p>Jestliže máte stolní počítač, možné jste s ním dostali i kabel s jinou koncovku, nebo máte cestovní adaptér. V takovém případě může být ale nutné přepnout přepínač vstupního napětí na napájecím zdroji (pokud tam tedy je). Řada počítačů tento přepínač nemá, a fungují jen s jedním pevně daným napětím. Podívejte se zezadu počítače a najděte zdířku pro připojení napájecího kabelu. Někde poblíž by měl být malý přepínač označený popisky „110 V“ a „230 V“ (například). V případě potřeby jej přepněte.</p>

<note style="warning">
  <p>Při výměně napájecích kabelů nebo používání cestovních adaptérů buďte opatrní. Pokud můžete, nejdříve vše vypněte.</p>
</note>

</page>
