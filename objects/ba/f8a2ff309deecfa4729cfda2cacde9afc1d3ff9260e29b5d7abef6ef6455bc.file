<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-font-size" xml:lang="pt">

  <info>
    <link type="guide" xref="a11y#vision" group="lowvision"/>

    <revision pkgversion="3.7.1" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.26" date="2017-08-04" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hilh</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Utilizar tipografías maiores para fazer que o texto seja mais fácil de ler.</desc>
  </info>

  <title>Mudar o tamanho do texto no ecrã</title>

  <p>Se tem dificuldades para ler o texto em seu ecrã, pode mudar o tamanho da tipografía.</p>

  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Atividades</gui> e comece a escrever <gui>Acesso universal</gui>.</p>
    </item>
    <item>
      <p>Carregue em <gui>Acesso universal</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>In the <gui>Seeing</gui> section, switch the <gui>Large Text</gui>
      switch to on.</p>
    </item>
  </steps>

  <p>Alternativamente, pode ajustar rapidamente o tamanho do texto clicando em o <link xref="a11y-icon">ícone de acessibilidade</link> na barra superior e selecionando <gui>Texto grande</gui>.</p>

  <note style="tip">
    <p>Em muitos aplicações, pode aumentar o tamanho do texto em qualquer momento clicando <keyseq><key>Ctrl</key><key>+</key></keyseq>. Para reduzir o tamanho, carregue <keyseq><key>Ctrl</key><key>-</key></keyseq>.</p>
  </note>

  <p><gui>Large Text</gui> will scale the text by 1.2 times. You can use
  <app>Tweaks</app> to make text size bigger or smaller.</p>

</page>
