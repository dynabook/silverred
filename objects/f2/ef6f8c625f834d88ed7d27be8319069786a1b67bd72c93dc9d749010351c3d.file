<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-which-application" xml:lang="es">

  <info>
    <link type="guide" xref="accounts"/>
    <link type="seealso" xref="accounts-disable-service"/>

    <revision pkgversion="3.8.2" date="2013-05-22" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="incomplete"/>

    <credit type="author copyright">
      <name>Baptiste Mille-Mathias</name>
      <email>baptistem@gnome.org</email>
      <years>2012, 2013</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Aplicaciones que pueden usar las cuentas creadas en <app>Cuentas en línea</app> y los servicios que pueden usar.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Servicios y aplicaciones en línea</title>

  <p>Una vez que haya añadido una cuenta en línea, cualquier aplicación puede usar esa cuenta para cualquiera de los servicios disponibles que no se hayan <link xref="accounts-disable-service">desactivado</link>. Diferentes proveedores ofrecen distintos servicios. Esta págia lista los diferentes servicios y algunas de aplicaciones conocidas que los usan.</p>

  <terms>
    <item>
      <title>Calendario</title>
      <p>El servicio de calendario le permite ver, añadir y editar eventos en un calendario en línea. Esto lo usan aplicaciones tales como <app>Calendar</app>, <app>Evolution</app>, y <app>California</app>.</p>
    </item>

    <item>
      <title>Chat</title>
      <p>El servicio de chat le permite chatear con sus contactos en populares plataformas de mensajería instantánea. Esto lo usa la aplicación <app>Empathy</app>.</p>
    </item>

    <item>
      <title>Contactos</title>
      <p>El servicio de contactos le permite ver los detalles publicados de sus contactos en varios dispositivos. Esto lo usan aplicaciones tales como <app>Contacts</app> y <app>Evolution</app>.</p>
    </item>

    <item>
      <title>Documentos</title>
      <p>El servicio de documentos le permite ver sus documentos en línea, como los que están en Google docs. Puede ver los documentos usando la aplicación <app>Documentos</app>.</p>
    </item>

    <item>
      <title>Archivos</title>
      <p>El servicio Archivos añade una ubicación de archivo remota, como si hubiese añadido una usando la funcionalidad de <link xref="nautilus-connect">Conectar al servidor</link> en el gestor de archivos. Puede acceder a los archivos remotos usando el gestor de archivos así como los diálogos de abrir y guardar archivos de cualquier aplicación.</p>
    </item>

    <item>
      <title>Correo-e</title>
      <p>El servicio de correo-e le permite enviar y recibir correos electrónicos mediante un proveedor de correo-e como Google. Esto lo usa <app>Evolution</app>.</p>
    </item>

<!-- TODO: Not sure what this does. Doesn't seem to do anything in Maps app.
    <item>
      <title>Maps</title>
    </item>
-->

    <item>
      <title>Fotos</title>
      <p>El servicio de fotos le permite ver sus fotos en línea, como las que tiene publicadas en Facebook. Puede ver sus fotos usando la aplicación <app>Fotos</app>.</p>
    </item>

    <item>
      <title>Impresoras</title>
      <p>El servicio Impresoras le permite enviar un archivo PDF a un proveedor desde el diálogo de impresión de cualquier aplicación. Este proveedor puede ofrecer servicios de impresión o simplemente servir como almacenamiento del PDF, que puede descargar e imprimir más tarde.</p>
    </item>

    <item>
      <title>Leer más tarde</title>
      <p>El servicio de leer más tarde le permite guardar una página web en dispositivos externos para poder leerla más tarde en otro dispositivo. Actualmente, ninguna aplicación usa este servicio.</p>
    </item>

  </terms>

</page>
