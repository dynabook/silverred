<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-slowkeys" xml:lang="pt">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bulh</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hilh</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ter um atraso ao carregar uma tecla e o tempo que demora em aparecer no ecrã.</desc>
  </info>

  <title>Ativar as teclas lentas</title>

  <p>Ative as <em>teclas lentas</em> se quer que tenha um atraso entre a clique duma tecla e a visualização dessa letra no ecrã. Isto significa que tendrá que manter premida um pouco a cada tecla que quer digitar antes de que apareça. Use as teclas lentas se clica acidentalmente várias teclas ao mesmo tempo quando digita, ou se lhe resulta dificil carregar a tecla adequada no teclado a primeira vez.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Universal Access</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Carregue <gui>Assistente de editável</gui> na seção <gui>Digitar</gui>.</p>
    </item>
    <item>
      <p>Switch the <gui>Slow Keys</gui> switch to on.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Ativar e desativar rapidamente as teclas lentas</title>
    <p>Em <gui>Ativar por teclado</gui>, selecione <gui>Ativar as caraterísticas de acessibilidade desde o teclado</gui> para ativar e desativar as teclas lentas desde o teclado. Quando esta opção está selecionada pode carregar e manter premida a tecla <key>Shift</key> durante oito segundos para ativar ou desativar as teclas lentas.</p>
    <p>Também pode ativar as teclas lentas clicando o <link xref="a11y-icon">ícone de acesso universal</link> na barra superior e mude <gui>Teclas lentas</gui>. O ícone de acessibilidade está visível quando se ativaram uma ou mas opções no painel de <gui>Acesso universal</gui>.</p>
  </note>

  <p>Use o deslizador <gui>Atraso de aceitação</gui> para controlar quanto tempo deve manter premida a tecla para que se registe.</p>

  <p>You can have your computer make a sound when you press a key, when a key
  press is accepted, or when a key press is rejected because you didn’t hold
  the key down long enough.</p>

</page>
