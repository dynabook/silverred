<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="a11y task" id="a11y-visualalert" xml:lang="it">

  <info>
    <link type="guide" xref="a11y#sound"/>
    <link type="seealso" xref="sound-alert"/>

    <revision pkgversion="3.7.1" date="2012-11-10" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Attivare gli avvisi visibili quando viene riprodotto un avviso sonoro.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Far illuminare lo schermo per gli avvisi</title>

  <p>Il computer riprodurrà un semplice avviso sonoro per alcune tipologie di messaggi ed eventi. Nel caso non si potessero udire facilmente questi avvisi, è possibile fare in modo che lo schermo intero o solo la finestra attiva vengano illuminati quando viene riprodotto un avviso sonoro.</p>

  <p>This can also be useful if you’re in an environment where you need your
  computer to be silent, such as in a library. See <link xref="sound-alert"/>
  to learn how to mute the alert sound, then enable visual alerts.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Universal Access</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Universal Access</gui> to open the panel.</p>
    </item>
    <item>
      <p>Press <gui>Visual Alerts</gui> in the <gui>Hearing</gui> section.</p>
    </item>
    <item>
      <p>Switch the <gui>Visual Alerts</gui> switch to on.</p>
    </item>
    <item>
      <p> Select whether you want the entire screen or just your current window
      title to flash.</p>
    </item>
  </steps>

  <note style="tip">
    <p>È possibile attivare e disattivare rapidamente gli avvisi visibili facendo clic sulla <link xref="a11y-icon">icona accesso universale</link> nella barra superiore e selezionando <gui>Avvisi visibili</gui>.</p>
  </note>

</page>
