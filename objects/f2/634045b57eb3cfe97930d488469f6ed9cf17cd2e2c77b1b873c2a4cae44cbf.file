<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="solaris-mode" xml:lang="pl">
  <info>
    <revision version="0.2" pkgversion="3.11" date="2014-01-26" status="review"/>
    <link type="guide" xref="index#other" group="other"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author copyright">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
      <years>2011</years>
    </credit>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2011, 2014</years>
    </credit>

    <desc>Ustawienie trybu Solaris, aby odzwierciedlać liczbę procesorów.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Czym jest tryb systemu Solaris?</title>

  <p>W przypadku komputerów z wieloma procesorami lub <link xref="cpu-multicore">rdzeniami</link> procesy mogą używać więcej niż jednego w tym samym czasie. To możliwe, że kolumna <gui>% procesora</gui> wyświetla wartości łącznie wyższe niż 100% (np. 400% na komputerze z czterema procesorami). <gui>Tryb Solaris</gui> dzieli <gui>% procesora</gui> dla każdego procesu przez liczbę procesorów w komputerze tak, aby suma wynosiła 100%.</p>

  <p>Aby wyświetlać <gui>% procesora</gui> w <gui>Trybie Solaris</gui>:</p>

  <steps>
    <item><p>Kliknij <gui>Preferencje</gui> w menu programu.</p></item>
    <item><p>Kliknij kartę <gui>Procesy</gui>.</p></item>
    <item><p>Zaznacz opcję <gui>Dzielenie użycia procesora przez liczbę procesorów</gui>.</p></item>
  </steps>

    <note><p>Termin <gui>Tryb Solaris</gui> pochodzi od systemu UNIX firmy Sun, w odróżnieniu od trybu IRIX, domyślnego w systemie Linux, nazwanego tak od systemu UNIX firmy SGI.</p></note>

</page>
