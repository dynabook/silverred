<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="task" id="printing-to-file" xml:lang="gu">

  <info>
    <link type="guide" xref="printing" group="#last"/>

    <revision pkgversion="3.8" date="2013-03-29" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author copyright">
      <name>ઍકાટેરીના ગેરાસીમોવા</name>
      <email>kittykat3756@gmail.com</email>
      <years>૨૦૧૩</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Save a document as a PDF, PostScript or SVG file instead of sending
    it to a printer.</desc>
  </info>

  <title>ફાઇલમાં છાપો</title>

  <p>You can choose to print a document to a file instead of sending it to
  print from a printer. Printing to file will create a <sys>PDF</sys>, a
  <sys>PostScript</sys> or a <sys>SVG</sys> file that contains the document.
  This can be useful if you want to transfer the document to another machine
  or to share it with someone.</p>

  <steps>
    <title>To print to file:</title>
    <item>
      <p>Open the print dialog by pressing
      <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p><gui style="tab">સામાન્ય</gui> ટેબમાં <gui>પ્રિન્ટર</gui> હેઠળ <gui>ફાઇલમાં છાપો</gui> ને પસંદ કરો.</p>
    </item>
    <item>
      <p>મૂળભૂત ફાઇલનામને બદલવા માટે અને જ્યાં ફાઇલ સંગ્રહેલ છે, પ્રિન્ટર પસંદગી નીચે ફાઇલનામ પર ક્લિક કરો. <gui style="button">પસંદ કરો</gui> પર ક્લિક કરો એકવાર તમે પસંદ કરવાનુ સમાપ્ત કરો.</p>
    </item>
    <item>
      <p><sys>PDF</sys> is the default file type for the document. If you want
      to use a different <gui>Output format</gui>, select either
      <sys>PostScript</sys> or <sys>SVG</sys>.</p>
    </item>
    <item>
      <p>તમારાં બીજા પાનાંની પસંદગીઓને પસંદ કરો.</p>
    </item>
    <item>
      <p>ફાઇલને સંગ્રહવા માટે <gui style="button">છાપો</gui> બટનને દબાવો.</p>
    </item>
  </steps>

</page>
