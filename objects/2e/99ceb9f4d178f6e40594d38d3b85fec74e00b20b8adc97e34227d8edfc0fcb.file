<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-brightness" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="guide" xref="hardware-problems-graphics"/>
    <link type="seealso" xref="power-whydim"/>
    <link type="seealso" xref="a11y-contrast"/>

    <revision pkgversion="3.8" version="0.4" date="2013-03-28" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.28" date="2018-07-19" status="review"/>
    <revision pkgversion="3.33" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Gnomov projekat dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalija Ruz Lejva</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="author">
      <name>Šon Mek Kens</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Izmenite osvetljenost ekrana da poboljšate čitljivost pri jakom svetlu.</desc>
  </info>

  <title>Podesite osvetljenost ekrana</title>

  <p>U zavisnosti od vašeg hardvera, možete da izmenite osvetljenost ekrana zbog uštede energije ili da učinite ekran čitljivijim pri jakom svetlu.</p>

  <p>To change the brightness of your screen, click the
  <gui xref="shell-introduction#systemmenu">system menu</gui> on the right side
  of the top bar and adjust the screen brightness slider to the value you want
  to use. The change should take effect immediately.</p>

  <note style="tip">
    <p>Mnoge tastature prenosnih računara imaju posebne tastere za podešavanje osvetljenja. Na njima se često nalazi sličica nalik suncu. Držite pritisnutim taster <key>Fn</key> da koristite taj taster.</p>
  </note>

  <p>Možete takođe da podesite osvetljenje ekrana koristeći panel <gui>napajanja</gui>.</p>

  <steps>
    <title>Da podesite osvetljenost ekrana koristeći panel napajanja:</title>
    <item>
      <p>Otvorite pregled <gui xref="shell-introduction#activities">Aktivnosti</gui> i počnite da kucate <gui>Napajanje</gui>.</p>
    </item>
    <item>
      <p>Kliknite na <gui>Napajanje</gui> da otvorite panel.</p>
    </item>
    <item>
      <p>Podesite klizač <gui>osvetljenja ekrana</gui> na vrednost koja vam odgovara. Promena će odmah stupiti u dejstvo.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Ako vaš računar ima ugrađeni senzor svetla, osvetljenost ekrana će sama po sebi biti podešena. Ovo možete da isključite u panelu <gui>napajanja</gui>.</p>
  </note>

  <p>Ako je moguće podešavanje osvetljenosti vašeg ekrana, možete takođe da podesite da se ekran sam zatamni kada računar miruje kako biste uštedeli napajanje. Za više o tome, vidite <link xref="power-whydim"/>.</p>

</page>
