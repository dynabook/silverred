<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="hardware-cardreader" xml:lang="ca">

  <info>
    <link type="guide" xref="media#photos"/>
    <link type="guide" xref="hardware#problems"/>

    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Solucionar problemes amb els lectors de targetes multimèdia.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

<title>Problemes amb el lector de targetes multimèdia</title>

<p>Molts ordinadors disposen de lectors per a targetes SD, MMC, SM, MS, CF i altres mitjans d'emmagatzematge. Aquests s'han de detectar i <link xref="disk-partitions">muntar</link> automàticament. Aquí hi ha alguns passos per a la resolució de problemes si no succeeix així:</p>

<steps>
<item>
<p>Assegureu-vos que la targeta es col·loca correctament. Moltes targetes semblen estar al revés quan s'insereixen correctament. Assegureu-vos també que la targeta està ben col·locada a la ranura; Algunes targetes, especialment CF, requereixen una mica de força per inserir-se correctament. (Aneu amb compte de no pressionar massa! Si us topeu amb alguna cosa resistent, no la forceu).</p>
</item>

<item>
  <p>Obriu <app>Fitxers</app> des de la vista general d'<gui xref="shell-introduction#activities">Activitats</gui>. Apareix la targeta inserida a la llista de <gui>Dispositius</gui> a la barra esquerra? De vegades, la targeta apareix a la llista però no està muntada; feu-hi clic una vegada per muntar. (Si la barra lateral no està visible, premeu <key>F9</key> o feu clic a <gui style="menu">Fitxers</gui> a la barra superior i seleccioneu <gui style="menuitem">Barra lateral</gui>.)</p>
</item>

<item>
  <p>Si la vostra targeta no apareix a la barra lateral, premeu <keyseq><key>Ctrl</key><key>L</key></keyseq>, aleshores escriviu <input>computer:///</input> i premeu <key>Retorn</key>. Si el vostre lector de targetes està configurat correctament, el lector hauria d'aparèixer com a unitat de disc quan no hi hagi cap targeta, i la pròpia targeta quan s'hagi muntat.</p>
</item>

<item>
<p>Si veieu el lector de targetes però no la targeta, el problema pot estar en la pròpia targeta. Proveu una targeta diferent o valideu la targeta en un altre lector si és possible.</p>
</item>
</steps>

<p>Si no es mostren targetes ni discs quan navegueu per l'<gui>Ordinador</gui>, és possible que el lector de targetes no funcioni amb Linux a causa de problemes amb el controlador. Si el vostre lector de targetes és intern (a l'interior de l'ordinador en lloc d'estar-se fora) és més probable. La millor solució és connectar directament el dispositiu (càmera, telèfon mòbil, etc.) a un port USB de l'ordinador. També estan disponibles els lectors de targetes externs USB i són molt més compatibles amb Linux.</p>

</page>
