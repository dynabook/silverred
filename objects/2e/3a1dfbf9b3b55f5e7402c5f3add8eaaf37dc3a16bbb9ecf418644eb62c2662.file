<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="a11y task" id="a11y-visualalert" xml:lang="lv">

  <info>
    <link type="guide" xref="a11y#sound"/>
    <link type="seealso" xref="sound-alert"/>

    <revision pkgversion="3.7.1" date="2012-11-10" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Aktivēt vizuālos brīdinājumus, lai mirgotu ekrāns vai logs tad, kad tiek atskaņota brīdinājuma skaņa.</desc>
  </info>

  <title>Ekrāna zibsnīšana pie brīdinājuma skaņas saņemšanas</title>

  <p>Jūsu dators atskaņos vienkāršu brīdinājuma skaņu noteikta veida ziņojumiem un notikumiem. Ja jums ir grūti sadzirdēt šīs skaņas, jūs varat likt vai nu visam ekrānam, vai arī aktīvajam logam vizuāli nozibsnīt vienmēr, kad šāda brīdinājuma skaņa tiek atskaņota.</p>

  <p>Tas var būt arī lietderīgi, ja esat vidē, kur nepieciešams, lai jūsu dators uzvestos klusi, piemēram, bibliotēkā. Skatiet <link xref="sound-alert"/>, lai uzzinātu, kā apklusināt brīdinājuma skaņas, un pēc tam ieslēgt vizuālo brīdināšanu.</p>

  <steps>
    <item>
      <p>Atveriet <gui xref="shell-introduction#activities">Aktivitāšu</gui> pārskatu un sāciet rakstīt <gui>Universālā piekļuve</gui>.</p>
    </item>
    <item>
      <p>Spiediet <gui>Universālā piekļuve</gui>, lai atvērtu paneli.</p>
    </item>
    <item>
      <p>Spiediet uz <gui>Vizuālie brīdinājumi</gui> sadaļā <gui>Dzirdēšana</gui>.</p>
    </item>
    <item>
      <p>Switch the <gui>Visual Alerts</gui> switch to on.</p>
    </item>
    <item>
      <p>Izvēlieties, vai vēlaties visa ekrāna, vai tikai aktīvā loga virsraksta zibsnīšanu.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Jūs varat ātri ieslēgt un izslēgt vizuālos brīdinājumus, spiežot uz <link xref="a11y-icon">pieejamības ikonas</link> augšējā joslā un izvēloties <gui>Vizuālie brīdinājumi</gui>.</p>
  </note>

</page>
