<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-wireless" xml:lang="sv">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-batterylife"/>

    <revision pkgversion="3.20" date="2016-06-15" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2016</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Bluetooth, wi-fi och mobilt bredband kan slås av för att minska batterianvändning.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Slå av trådlösa teknologier som inte används</title>

  <p>Du kan minska batterianvändning genom att slå av bluetooth, wi-fi eller mobilt bredband då de inte används.</p>

  <steps>

    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Ström</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Ström</gui> för att öppna panelen.</p>
    </item>
    <item>
      <p>Avsnittet <gui>Strömspar</gui> innehåller växelknappar för <gui>Trådlöst nätverk</gui>, <gui>Mobilt bredband</gui> och <gui>Bluetooth</gui>. Slå av de oanvända tjänsterna. Återaktivera dem då de behövs genom att slå på igen.</p>
    </item>

  </steps>

</page>
