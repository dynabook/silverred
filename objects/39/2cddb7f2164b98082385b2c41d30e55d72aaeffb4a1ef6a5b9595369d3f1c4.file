<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-mag" xml:lang="pl">

  <info>
    <link type="guide" xref="a11y#vision" group="lowvision"/>

    <revision pkgversion="3.7.1" date="2012-11-10" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Przybliżenie ekranu, aby lepiej widzieć.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  </info>

  <title>Powiększenie obszaru na ekranie</title>

  <p>Powiększanie ekranu to inna funkcja niż po prostu zwiększenie <link xref="a11y-font-size">rozmiaru tekstu</link>. To bardziej jak lupa powiększająca, umożliwiająca poruszanie się powiększając części ekranu.</p>

  <steps>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Ułatwienia dostępu</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Ułatwienia dostępu</gui>, aby otworzyć panel.</p>
    </item>
    <item>
      <p>Naciśnij <gui>Powiększanie</gui> w sekcji <gui>Wzrok</gui>.</p>
    </item>
    <item>
      <p>Przełącz <gui>Powiększanie</gui> na <gui>|</gui> (włączone) w górnym prawym rogu okna <gui>Opcje powiększania</gui>.</p>
      <!--<note>
        <p>The <gui>Zoom</gui> section lists the current settings for the
        shortcut keys, which can be set in the <gui>Universal Access</gui>
        section of the <link xref="keyboard-shortcuts-set">Shortcuts</link> tab
        on the <gui>Keyboard</gui> panel.</p>
      </note>-->
    </item>
  </steps>

  <p>Można teraz poruszać się po ekranie. Przesunięcie myszy do krawędzi ekranu spowoduje przesunięcie powiększanego obszaru w różnych kierunkach.</p>

  <note style="tip">
    <p>Można szybko przełączać powiększanie klikając <link xref="a11y-icon">ikonę ułatwień dostępu</link> na górnym pasku i wybierając <gui>Powiększenie</gui>.</p>
  </note>

  <p>Można zmienić poziom powiększenia, śledzenie myszy i położenie powiększonego obszaru na ekranie w karcie <gui>Lupa</gui> okna <gui>Opcje powiększania</gui>.</p>

  <p>Można włączyć celownik pomagający odnajdować kursor myszy lub panelu dotykowego. Karta <gui>Celownik</gui> okna <gui>Powiększanie</gui> umożliwia jego włączenie i dostosowanie długości, koloru i grubości.</p>

  <p>Można włączyć <gui>Białe na czarnym</gui> i dostosować opcje jasności, kontrastu i odcieni szarości lupy. Połączenie tych opcji jest przydatne dla osób niedowidzących, z każdym stopniem światłowstrętu lub tylko używających komputera w niekorzystnym świetle. Wybierz kartę <gui>Zmiana kolorów</gui> w oknie <gui>Powiększanie</gui>, aby włączyć i zmieniać te opcje.</p>

</page>
