<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="bluetooth-remove-connection" xml:lang="nl">

  <info>
    <link type="guide" xref="bluetooth"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.12" date="2014-03-04" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Een apparaat uit de lijst met Bluetooth-apparaten verwijderen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Verbinding met een Bluetooth-apparaat verbreken</title>

  <p>Als u niet langer verbonden wilt zijn met een Bluetooth-apparaat, dan kunt u de verbinding verwijderen. Dit is handig wanneer u een apparaat zoals een muis of koptelefoon niet langer wilt gebruiken, of wanneer u geen bestanden meer over wilt zetten van het ene apparaat naar het andere.</p>

  <steps>
    <item>
      <p>Open het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht en typ <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Klik op het <gui>Bluetooth</gui> om het paneel te openen.</p>
    </item>
    <item>
      <p>Selecteer het apparaat dat u wilt uitschakelen in de lijst.</p>
    </item>
    <item>
      <p>In the device dialog box, switch the <gui>Connection</gui> switch to
      off, or to remove the device from the <gui>Devices</gui> list,
      click <gui>Remove Device</gui>.</p>
    </item>
  </steps>

  <p>U kunt desgewenst later <link xref="bluetooth-connect-device"> opnieuw met een  Bluetooth-apparaat verbinden</link>.</p>

</page>
