<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="power-nowireless" xml:lang="de">

  <info>
    <link type="guide" xref="power#problems"/>
    <link type="seealso" xref="power-suspendfail"/>
    <link type="seealso" xref="hardware-driver"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Einige Funknetzwerkgeräte haben Probleme, wenn der Rechner in den Ruhezustand versetzt wurde und nicht richtig aufgeweckt wird.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2019.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2019.</mal:years>
    </mal:credit>
  </info>

  <title>Ich habe keine Funknetzwerkverbindung, wenn ich meinen Rechner aus einem Energiesparmodus aufwecke</title>

  <p>Wenn Sie Ihren Rechner in Bereitschaft versetzt haben kann es vorkommen, dass keine Funknetzwerkverbindung mehr besteht, wenn Sie ihn wieder aufwecken. Dies tritt ein, wenn die <link xref="hardware-driver">Treiber</link> des Funknetzwerkgerätes bestimmte Energiesparfunktionen nicht vollständig unterstützen. Dies hat oft zur Folge, dass die Funknetzwerkverbindung nach dem Aufwachen nicht hergestellt werden kann.</p>

  <p>Versuchen Sie in diesem Fall, das Funknetzwerk aus- und wieder einzuschalten:</p>

  <steps>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>WLAN</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>WLAN</gui>, um das Panel zu öffnen.</p>
    </item>
    <item>
      <p>Schalten Sie den Schalter <gui>WLAN</gui> oben rechts im Fenster aus, und anschließend wieder ein.</p>
    </item>
    <item>
      <p>Falls das Funknetzwerk noch immer nicht funktioniert, schalten Sie den <gui>Flugzeugmodus</gui> ein und dann wieder aus.</p>
    </item>
  </steps>

  <p>Falls dies nichts hilft, sollte ein Neustart Ihres Rechners das Funknetzwerk wieder funktionieren lassen. Wenn Sie danach noch immer Probleme haben, verbinden Sie sich über ein kabelgebundenes Netzwerk mit dem Internet und aktualisieren Sie Ihren Rechner.</p>

</page>
