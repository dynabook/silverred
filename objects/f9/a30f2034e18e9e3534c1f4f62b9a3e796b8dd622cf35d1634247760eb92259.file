<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="task" id="printing-to-file" xml:lang="mr">

  <info>
    <link type="guide" xref="printing" group="#last"/>

    <revision pkgversion="3.8" date="2013-03-29" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>२०१३</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Save a document as a PDF, PostScript or SVG file instead of sending
    it to a printer.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

  <title>फाइलला प्रिंट करा</title>

  <p>You can choose to print a document to a file instead of sending it to
  print from a printer. Printing to file will create a <sys>PDF</sys>, a
  <sys>PostScript</sys> or a <sys>SVG</sys> file that contains the document.
  This can be useful if you want to transfer the document to another machine
  or to share it with someone.</p>

  <steps>
    <title>To print to file:</title>
    <item>
      <p>Open the print dialog by pressing
      <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p><gui>सर्वसाधारण</gui> टॅबमध्ये  <gui>छपाईयंत्र</gui> अंतर्गत <gui>फाइलकरिता छपाई</gui> निवडा</p>
    </item>
    <item>
      <p>पूर्वनिर्धारित फाइलनाव आणि फाइल कुठे साठवायचे बदलण्याकरिता, छपाईयंत्र निवड खालील फाइलनाव क्लिक करा. एकदाचे पूर्णपणे बंद केल्यावर <gui style="button">निवडा</gui> क्लिक करा.</p>
    </item>
    <item>
      <p><sys>PDF</sys> is the default file type for the document. If you want
      to use a different <gui>Output format</gui>, select either
      <sys>PostScript</sys> or <sys>SVG</sys>.</p>
    </item>
    <item>
      <p>Choose your other page preferences.</p>
    </item>
    <item>
      <p>फाइलला साठवण्यासाठी <gui style="button">छापा</gui> दाबा.</p>
    </item>
  </steps>

</page>
