<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="color-whyimportant" xml:lang="sv">

  <info>
    <link type="guide" xref="color"/>
    <desc>Färghantering är viktigt för designers, fotografer och konstnärer.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Varför är färghantering viktigt?</title>
  <p>Färghantering är processen där en färg uppfattas av en indataenhet, visas på skärmen, och skrivs ut medan varje mediums exakta färger och färgintervall kontrolleras.</p>

  <p>Behovet av färghantering förklaras troligtvis bäst med ett fotografi av en fågel under en frostig vinterdag.</p>

  <figure>
    <desc>En fågel på en frostbiten mur, sett genom kamerans objektiv</desc>
    <media its:translate="no" type="image" mime="image/png" src="figures/color-camera.png"/>
  </figure>

  <p>Skärmar övermättar vanligtvis den blå kanalen, vilket får bilderna att se kalla ut.</p>

  <figure>
    <desc>Detta är vad användaren ser på skärmen på en vanlig bärbar dator</desc>
    <media its:translate="no" type="image" mime="image/png" src="figures/color-display.png"/>
  </figure>

  <p>Observera hur det vita inte är ”pappersvitt” och att det svarta i fågelns öga nu har en mörkbrun ton.</p>

  <figure>
    <desc>Detta är vad användaren ser vid utskrift på en vanlig bläckstråleskrivare</desc>
    <media its:translate="no" type="image" mime="image/png" src="figures/color-printer.png"/>
  </figure>

  <p>Grundproblemet här är att varje enhet kan hantera olika färgintervall. Så även om du kan ta ett foto av elektriskt blått kommer de flesta skrivare inte kunna återge den färgen.</p>
  <p>De flesta bildenheter använder RGB (röd, grön, blå) och måste konvertera till CMYK (cyan/ljusblå, magenta, yellow/gul, black/svart) vid utskrift. Ett annat problem är att du inte kan använda <em>vitt</em> bläck, så vitheten blir bara så bra som pappret är färgat från början.</p>

  <p>Ett annat problem är enheter. Utan att ange skalan en färg mäts i kan vi inte veta om 100% röd är nära infrarött eller bara den rödaste färgen i skrivaren. Vad som är 50% rött på en skärm kan vara något i stil med 62% rött på en annan skärm. Det är lite som att berätta för någon att du just körde 7 distansenheter; utan enhet vet man inte om det rör sig om 7 kilometer eller 7 meter.</p>

  <p>För färg så refererar vi till enheter som färgomfång. Färgomfång är helt enkelt intervallet av färger som kan återges. En enhet som en DSLR-kamera kan ha ett väldigt stort färgomfång, kapabel att återge alla färgerna i en solnedgång, men en projektor har ett väldigt litet färgomfång så alla färgerna kommer att se ”urtvättade” ut.</p>

  <p>I vissa fall kan vi <em>korrigera</em> enhetens utdata genom att ändra data vi skickar till den, men i andra fall där detta inte är möjligt (du kan inte skriva ut elektriskt blått) måste vi visa användaren hur resultatet kommer att se ut.</p>

  <p>För fotografier är det vettigt att använda hela nyansintervallet hos en färgenhet för att kunna göra mjuka övergångar mellan färger. För annan grafik kanske du vill matcha färgen exakt, vilket är viktigt om du försöker skriva ut en anpassad mugg med en Red Hat-logotyp som <em>måste</em> vara exakt Red Hat-röd.</p>

</page>
