<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="help-irc" xml:lang="ja">

  <info>
    <link type="guide" xref="more-help"/>
    <desc>IRC でリアルタイムに助言をもらえます。</desc>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>

    <credit type="author">
      <name>Baptiste Mille-Mathias</name>
      <email>baptistem@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>松澤 二郎</mal:name>
      <mal:email>jmatsuzawa@gnome.org</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>赤星 柔充</mal:name>
      <mal:email>yasumichi@vinelinux.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kentaro KAZUHAMA</mal:name>
      <mal:email>kazken3@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shushi Kurose</mal:name>
      <mal:email>md81bird@hitaki.net</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Noriko Mizumoto</mal:name>
      <mal:email>noriko@fedoraproject.org</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>坂本 貴史</mal:name>
      <mal:email>o-takashi@sakamocchi.jp</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>日本GNOMEユーザー会</mal:name>
      <mal:email>http://www.gnome.gr.jp/</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>IRC</title>
   <p>IRC とは、インターネットリレーチャット (Internet Relay Chat) の略称です。IRC は、リアルタイムかつマルチユーザーのメッセージングシステムです。GNOME IRC サーバー上では、他の GNOME ユーザーや開発者とコミュニケーションを取り、助言をもらうことができます。</p>
   <p>GNOME IRC サーバーに接続するには、<app>empathy</app> や <app>xchat</app> を使用するか、あるいは <link href="http://chat.mibbit.com/">mibbit</link> などの Web インターフェースを使用します。</p>
   <p>empathy で IRC アカウントを作成する方法については、<link href="help:empathy/irc-manage">Empathy のドキュメント</link>を参照してください。</p>
   <p>GNOME IRC サーバーは、<sys>irc.gnome.org</sys> です。これは "GIMP network" としても参照できます。お使いのコンピューターが適切に設定されていれば、<link href="irc://irc.gnome.org/gnome"/> のリンクから、gnome チャンネルに接続できます。</p>
   <p>
      While IRC is a real-time discussion medium, people tend to not reply immediately, so be patient.
   </p>

  <note>
    <p>Please note the
    <link href="https://wiki.gnome.org/Foundation/CodeOfConduct">GNOME code of
    conduct</link> applies when you chat on IRC.</p>
  </note>

</page>
