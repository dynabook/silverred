<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:if="http://projectmallard.org/if/1.0/" type="guide" style="task" version="1.0 if/1.0" id="bluetooth-connect-device" xml:lang="ru">

  <info>
    <link type="guide" xref="bluetooth"/>
    <link type="guide" xref="hardware-phone#setup"/>
    <link type="seealso" xref="sharing-bluetooth"/>
    <link type="seealso" xref="bluetooth-remove-connection"/>

    <revision pkgversion="3.8" date="2013-05-16" status="review"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-22" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Джим Кэмпбелл (Jim Campbell)</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Шон МакКенс (Shaun McCance)</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Дэвид Кинг (David King)</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Сопряжение устройств Bluetooth.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Соединение компьютера с устройством Bluetooth</title>

  <p>Before you can use a Bluetooth device like a mouse or a headset, you first
  need to connect your computer to the device. This is also called
  <em>pairing</em> the Bluetooth devices.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Bluetooth</gui> to open the panel.</p>
    </item>
    <item>
      <p>Make sure Bluetooth is enabled: the switch at the top should be set to
      on. With the panel open and the switch on, your computer will begin
      searching for devices.</p>
    </item>
    <item>
      <p>Make the other Bluetooth device
      <link xref="bluetooth-visibility">discoverable or visible</link> and
      place it within 5-10 meters (about 16-33 feet) of your computer.</p>
    </item>
    <item>
      <p>Click the device in the <gui>Devices</gui> list. The panel for the
      device will open.</p>
    </item>
    <item>
      <p>If required, confirm the PIN on your other device. The device should
      show you the PIN you see on your computer screen. Confirm the PIN on the
      device (you may need to click <gui>Pair</gui> or <gui>Confirm</gui>),
      then click <gui>Confirm</gui> on the computer.</p>
      <p>На большинстве устройств необходимо закончить ввод в течение приблизительно 20 секунд, иначе соединение не будет завершено. В таком случае вернитесь в список устройств и начните снова.</p>
    </item>
    <item>
      <p>The entry for the device in the <gui>Devices</gui> list will show a
      <gui>Connected</gui> status.</p>
    </item>
    <item>
      <p>To edit the device, click on it in the <gui>Device</gui> list. You
      will see a panel specific to the device. It may display additional
      options applicable to the type of device to which you are connecting.</p>
    </item>
    <item>
      <p>Close the panel once you have changed the settings.</p>
    </item>
  </steps>

  <media its:translate="no" type="image" src="figures/bluetooth-menu.png" style="floatend">
    <p its:translate="yes">Значок Bluetooth в верхней панели</p>
  </media>
  <p>When one or more Bluetooth devices are connected, the Bluetooth icon
  appears in the system status area.</p>

</page>
