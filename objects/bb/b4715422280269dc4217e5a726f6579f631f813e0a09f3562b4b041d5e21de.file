<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-templates" xml:lang="de">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Anita Reitere</name>
      <email>nitalynx@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Erstellen Sie neue Dokumente aus benutzerdefinierten Dateivorlagen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2019.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2019.</mal:years>
    </mal:credit>
  </info>

  <title>Vorlagen für häufig genutzte Dokumenttypen</title>

  <p>Falls Sie oft Dokumente erstellen, die auf den gleichen Inhalten basieren, könnte es nützlich sein, dafür Vorlagen zu verwenden. Eine Dateivorlage kann ein Dokument beliebigen Typs sein, mit der Formatierung oder dem Inhalt, den Sie wiederverwenden wollen. Beispielsweise könnten Sie eine Vorlage mit Ihrem Briefkopf erstellen.</p>

  <steps>
    <title>Eine neue Vorlage erstellen</title>
    <item>
      <p>Erstellen Sie ein Dokument, das Sie als Vorlage verwenden wollen. Beispielsweise könnten Sie Ihren Briefkopf in einer Anwendung zur Textverarbeitung.</p>
    </item>
    <item>
      <p>Speichern Sie die Datei mit dem Inhalt der Vorlage im Ordner <file>Vorlagen</file> in Ihrem <file>persönlichen Ordner</file>. Falls der Ordner <file>Vorlagen</file> nicht existiert, müssen Sie ihn zuerst anlegen.</p>
    </item>
  </steps>

  <steps>
    <title>Eine Vorlage zur Erstellung eines Dokuments verwenden</title>
    <item>
      <p>Öffnen Sie den Ordner, in welchem Sie das neue Dokument erstellen wollen.</p>
    </item>
    <item>
      <p>Klicken Sie mit der rechten Maustaste auf eine freie Stelle im Ordner. Wählen Sie anschließend <gui style="menuitem">Neues Dokument</gui>. Die Namen der verfügbaren Vorlagen werden im Untermenü aufgelistet.</p>
    </item>
    <item>
      <p>Wählen Sie die gewünschte Vorlage in der Liste aus.</p>
    </item>
    <item>
      <p>Klicken Sie doppelt auf die Datei, um Sie zu öffnen, und beginnen Sie mit der Bearbeitung. Sie können die Datei <link xref="files-rename">umbenennen</link>, wenn Sie fertig sind.</p>
    </item>
  </steps>

</page>
