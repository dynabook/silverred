<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-touchpad-click" xml:lang="lv">

  <info>
    <link type="guide" xref="mouse"/>

    <revision pkgversion="3.7" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-29" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013, 2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Spiediet, velciet vai ritiniet, izmantojot sitienus un žestus uz skārienpaliktņa.</desc>
  </info>

  <title>Spiediet, velciet vai ritiniet ar skārienpaliktni</title>

  <p>Jūs varat spiest, veikt dubultklikšķi, vilkt un ritināt, izmantojot skārienpaliktni, neizmantojot atsevišķas pogas.</p>

  <note>
    <p><link xref="touchscreen-gestures">Touchscreen gestures</link> are
    covered separately.</p>
  </note>

<section id="tap">
  <title>Uzsitiens nozīmē klikšķi</title>

  <p>Lai klikšķinātu, jūs varat uzsist uz skārienpaliktņa, nevis izmantot pogu.</p>

  <steps>
    <item>
      <p>Atveriet <gui xref="shell-introduction#activities">Aktivitāšu</gui> pārskatu un sāciet rakstīt <gui>Pele un skārienpaliktnis</gui>.</p>
    </item>
    <item>
      <p>Spiediet <gui>Pele un skārienpaliktnis</gui>, lai atvērtu paneli.</p>
    </item>
    <item>
      <p>In the <gui>Touchpad</gui> section, make sure the <gui>Touchpad</gui>
      switch is set to on.</p>
      <note>
        <p><gui>Skārienpaliktņa</gui> sadaļa parādās tikai tad, ja sistēmai ir skārienpaliktnis.</p>
      </note>
    </item>
   <item>
      <p>Switch the <gui>Tap to click</gui> switch to on.</p>
    </item>
  </steps>

  <list>
    <item>
      <p>Lai klikšķinātu, uzsitiet uz skārienpaliktņa.</p>
    </item>
    <item>
      <p>Lai veiktu dubultklikšķi, uzsitiet divreiz.</p>
    </item>
    <item>
      <p>Lai pavilktu kādu priekšmetu, uzsit uz tā divreiz, bet pirkstu pēc otrā uzsitiena nepalaid vāļā. Aizvelc priekšmetu, kur vēlies, un atlaid pirkstu.</p>
    </item>
    <item>
      <p>Ja skārienpaliktnis atbalsta vairāku skārienu sitienus, labais peles klikšķis var tikt atveidots, uzsitot ar 2 pirkstiem reizē. Citādi tev vajadzēs izmantot pogas, lai veiktu labo klikšķi. Lasi <link xref="a11y-right-click"/>, lai uzzinātu par klikšķināšanas metodi bez otrās peles pogas.</p>
    </item>
    <item>
      <p>Ja skārienpaliktnis atbalsta vairāku skārienu sitienus, <link xref="mouse-middleclick">veic vidējo klikšķi</link>, uzsitot ar 3 pirkstiem reizē.</p>
    </item>
  </list>

  <note>
    <p>Sitot vai velkot ar vairākiem pirkstiem, pārliecinies vai pirksti ir pietiekami tālu viens no otra. Ja pirksti būs pārāk tuvu, dators var padomāt, ka tas ir viens pirksts.</p>
  </note>

</section>

<section id="twofingerscroll">
  <title>Divu pirkstu ritināšana</title>

  <p>Ritināt var ar skārienpaliktni, izmantojot divus pirkstus.</p>

  <steps>
    <item>
      <p>Atveriet <gui xref="shell-introduction#activities">Aktivitāšu</gui> pārskatu un sāciet rakstīt <gui>Pele un skārienpaliktnis</gui>.</p>
    </item>
    <item>
      <p>Spiediet <gui>Pele un skārienpaliktnis</gui>, lai atvērtu paneli.</p>
    </item>
    <item>
      <p>In the <gui>Touchpad</gui> section, make sure the <gui>Touchpad</gui>
      switch is set to on.</p>
    </item>
    <item>
      <p>Switch the <gui>Two-finger Scrolling</gui> switch to on.</p>
    </item>
  </steps>

  <p>Kad tas ir izvēlēts, uzsišana un vilkšana ar vienu pirkstu strādās kā ierasts, bet ja pa skārienpaliktni pavilksi 2 pirkstus, sāksies ritināšana. Pārvietojiet pirkstus starp skārienpaliktņa augšpusi un apakšpusi, lai ritinātu augšup un lejup, vai pārvietojiet pirkstus pa skārienpaliktni, lai ritinātu sāniski. Starp pirkstiem jāietur atstarpe. Ja pirksti būs pārāk tuvu, dators reaģēs kā uz vienu lielu pirkstu.</p>

  <note>
    <p>Divu pirkstu ritināšana var nestrādāt uz visiem skārienpaliktņiem.</p>
  </note>

</section>

<section id="contentsticks">
  <title>Dabiskā ritināšana</title>

  <p>Jūs varat vilkt saturu, it kā slidinātu fizisku papīru, izmantojot skārienpaliktni.</p>

  <steps>
    <item>
      <p>Atveriet <gui xref="shell-introduction#activities">Aktivitāšu</gui> pārskatu un sāciet rakstīt <gui>Pele un skārienpaliktnis</gui>.</p>
    </item>
    <item>
      <p>Spiediet <gui>Pele un skārienpaliktnis</gui>, lai atvērtu paneli.</p>
    </item>
    <item>
      <p>In the <gui>Touchpad</gui> section, make sure that the
     <gui>Touchpad</gui> switch is set to on.</p>
    </item>
    <item>
      <p>Switch the <gui>Natural Scrolling</gui> switch to on.</p>
    </item>
  </steps>

  <note>
    <p>Šī iespēja ir zināma kā <em>apgrieztā ritināšana</em>.</p>
  </note>

</section>

</page>
