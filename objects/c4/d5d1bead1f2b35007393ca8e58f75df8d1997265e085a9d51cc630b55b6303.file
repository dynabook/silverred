<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting" xml:lang="lv">

  <info>
    <link type="guide" xref="net-wireless" group="first"/>
    <link type="guide" xref="hardware#problems" group="first"/>
    <link type="next" xref="net-wireless-troubleshooting-initial-check"/>

    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Ubuntu dokumentācijas viki veidotāji</name>
    </credit>
    <credit type="author">
      <name>GNOME dokumentācijas projekts</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Identificēt un novērst problēmas ar bezvadu tīkla savienojumiem.</desc>
  </info>

  <title>Bezvadu tīkla problēmu risināšana</title>

  <p>Šī ir problēmu novēršanas pamācība, kā soli pa solim identificēt un novērst bezvadu tīkla problēmas. Ja jūs nevarat savienoties ar bezvadu tīklu kādu iemeslu dēļ, mēģiniet sekot šīm instrukcijām.</p>

  <p>Mēs dosimies caur sekojošiem pasākumiem, lai savienotu datoru ar internetu:</p>

  <list style="numbered compact">
    <item>
      <p>Veiciet sākotnējo pārbaudi</p>
    </item>
    <item>
      <p>Iegūstiet informāciju par savu ierīci</p>
    </item>
    <item>
      <p>Pārbaudiet savu ierīci</p>
    </item>
    <item>
      <p>Mēģiniet izveidot savienojumu ar bezvadu maršrutētāju</p>
    </item>
    <item>
      <p>Pārbaudiet savu modemu un maršrutētāju</p>
    </item>
  </list>

  <p>Lai sāktu darbu, spiediet uz <em>Nākamā</em> saites lapas augšā. Šī saite, un citas sekojošās lapas izvedīs jūs cauri pamācības soļiem.</p>

  <note>
    <title>Komandrindas lietošana</title>
    <p>Dažas no šīs rokasgrāmatas instrukcijām prasa ievadīt komandas <em>komandrindā</em> (Terminālī). Jūs varat atrast <app>Termināļa</app> lietotni <gui>Aktivitāšu</gui> pārskatā.</p>
    <p>Ja neesat pazīstams ar komandrindām, neuztraucaties — šī pamācība jūs izvedīs cauri katram solim. Viss ko jums vajag atcerēties, komandrindas ir reģistr jutīgas (tādēļ jums tie ir jāieraksta <em>tieši</em> tā kā ir parādīti šeit), un jānospiež <key>Enter</key> pēc katras komandas, lai to palaistu.</p>
  </note>

</page>
