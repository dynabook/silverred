<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-delete" xml:lang="es">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>
    <link type="seealso" xref="files-recover"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-16" status="review"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="candidate"/>
    <revision pkgversion="3.16" date="2015-02-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Quitar archivos o carpetas que ya no necesita.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>Eliminar archivos y carpetas</title>

  <p>Si ya no quiere un archivo o una carpeta, puede eliminarla. Cuando elimina un elemento, se mueve a la carpeta <gui>Papelera</gui>, donde se guarda hasta que la vacía. Puede <link xref="files-recover">restaurar elementos</link> de la <gui>Papelera</gui> a su ubicación original si decide que los necesita, o si los eliminó accidentalmente.</p>

  <steps>
    <title>Para enviar un archivo a la papelera:</title>
    <item><p>Seleccione el elemento que quiere eliminar pulsándolo una sola vez.</p></item>
    <item><p>Pulse <key>Supr</key> en su teclado. Alternativamente, arrastre el elemento hasta la <gui>Papelera</gui> en la barra lateral.</p></item>
  </steps>

  <p>El archivo se moverá a la papelera y aparecerá una opción para <gui>Deshacer</gui> la acción. El botón <gui>Deshacer</gui> aparecerá durante unos segundos. Si selecciona <gui>Deshacer</gui>, el archivo se restaurará a su ubicación original.</p>

  <p>Para eliminar archivos permanentemente y liberar espacio en el equipo, debe vaciar la papelera. Para vaciar la papelera, pulse con el botón derecho en la <gui>Papelera</gui> en la barra lateral y elija <gui>Vaciar papelera</gui>.</p>

  <section id="permanent">
    <title>Eliminar permanentemente un archivo</title>
    <p>Puede eliminar un archivo permanentemente de forma inmediata, sin tenerlo que enviar primero a la papelera.</p>

  <steps>
    <title>Para eliminar permanentemente un archivo:</title>
    <item><p>Seleccione el elemento que quiere eliminar.</p></item>
    <item><p>Pulse y mantenga pulsada la tecla <key>Mayús</key>, y luego pulse la tecla <key>Supr</key> de su teclado.</p></item>
    <item><p>Como no puede deshacer esta operación, se le preguntará que confirme si quiere eliminar el archivo o carpeta.</p></item>
  </steps>

  <note><p>Los archivos eliminados en un <link xref="files#removable">dispositivo extraíble</link> puede no ser visibles en otros sistemas operativos, tales como Windows o Mac OS. Los archivos siguen ahí, y estarán disponibles cuando conecte el dispositivo de nuevo en su equipo.</p></note>

  </section>

</page>
