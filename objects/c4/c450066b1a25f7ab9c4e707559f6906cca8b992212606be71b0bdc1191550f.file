<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-delete" xml:lang="es">

  <info>
    <link type="guide" xref="user-accounts#manage"/>
    <link type="seealso" xref="user-add"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Eliminar usuarios que ya no usan su equipo</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Eliminar una cuenta de usuario</title>

  <p>Puede <link xref="user-add">añadir varias cuentas de usuario a su equipo</link>. Si alguien no va a volver a usar su equipo, puede eliminar esa cuenta de usuario.</p>

  <p>Necesita <link xref="user-admin-explain">privilegios de administrador</link> para eliminar cuentas de usuario.</p>

  <steps>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Usuarios</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Usuarios</gui> para abrir el panel.</p>
    </item>
    <item>
      <p>Pulse <gui style="button">Desbloquear</gui> en la esquina superior derecha e introduzca su contraseña cuando se le pida.</p>
    </item>
    <item>
      <p>Seleccione el usuario que quiere eliminar y pulse el botón <gui style="button">-</gui>, debajo de la lista de cuentas de la izquierda, para eliminar esa cuenta de usuario.</p>
    </item>
    <item>
      <p>Cada usuario tiene su propia carpeta de inicio para sus archivos y configuraciones. Puede optar por mantener o eliminar la carpeta personal del usuario. Pulse <gui>Eliminar archivos</gui> si está seguro de que no se volverá a utilizar y necesita liberar espacio en disco. Estos archivos se eliminan permanentemente. No se pueden recuperar. Es posible que quiera hacer una copia de respaldo de los archivos en un dispositivo de almacenamiento externo o en un CD antes de eliminarlos.</p>
    </item>
  </steps>

</page>
