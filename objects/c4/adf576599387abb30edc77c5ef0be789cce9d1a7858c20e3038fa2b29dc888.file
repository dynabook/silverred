<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="net-security-tips" xml:lang="pt">

  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.4.0" date="2012-02-21" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Steven Richards</name>
      <email>steven.richardspc@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Conselhos gerais que recordar ao usar Internet.</desc>
  </info>

  <title>Manter-se seguro em Internet</title>

  <p>Uma razão pela que pode estar a usar Linux é por a sua robustez e a sua segurança. Uma razão pela que Linux é relativamente seguro em frente a vírus e «malware» é devido ao reduzida número de pessoas que o usam. Os vírus focam-se  a sistemas operativos populares como Windows, que têm uma grande quantidade de utilizadores. Linux é muito seguro devido a sua natureza de codigo aberto que permite aos experientes modificar e melhorar as caraterísticas de segurança incluídas na cada distribuição.</p>

  <p>Independentemente das medidas tomadas para assegurar-se de que a sua instalação de Linux é segura, sempre existem vulnerabilidades. Como utilizador médio em Internet é susceptível de:</p>

  <list>
    <item>
      <p>Fraudes de «phishing» (lugares site que tentam obter informação delicada através de enganos)</p>
    </item>
    <item>
      <p><link xref="net-email-virus">Reenvio de correios maliciosos</link></p>
    </item>
    <item>
      <p><link xref="net-antivirus">Aplicações com intenções maliciosas (vírus)</link></p>
    </item>
    <item>
      <p><link xref="net-wireless-wepwpa">Acesso não autorizado à rede local/remota</link></p>
    </item>
  </list>

  <p>Para ligar-se se maneira segura, recorde as seguintes sugestões:</p>

  <list>
    <item>
      <p>Não abra correios eletrónicos, adjuntos ou enlaces enviados por pessoas que não conhece.</p>
    </item>
    <item>
      <p>If a website’s offer is too good to be true, or asks for sensitive
      information that seems unnecessary, then think twice about what
      information you are submitting and the potential consequences if that
      information is compromised by identity thieves or other criminals.</p>
    </item>
    <item>
      <p>Tenha cuidado ao outorgar <link xref="user-admin-explain">permissões de administrador</link> a um aplicação, especialmente àquelas que não tem usado anteriormente ou aplicações que não conhece bem. Outorgar permissões de administrador a qualquer ou a qualquer coisa põe o seu computador em risco.</p>
    </item>
    <item>
      <p>Assegure-se de que só executa os serviços de acesso remoto necessários. Ter SSH ou VNC em execução pode ser útil, mas deixa o seu computador aberta ante os intrusos se não está securizado adequadamente. Considere a possibilidade de usar um <link xref="net-firewall-on-off">firewalh</link> para ajudar-lhe a proteger o seu computador em frente aos intrusos.</p>
    </item>
  </list>

</page>
