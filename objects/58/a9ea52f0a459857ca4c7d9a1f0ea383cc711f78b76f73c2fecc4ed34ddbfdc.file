<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="sound-crackle" xml:lang="ca">

  <info>
    <link type="guide" xref="sound-broken"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Comproveu els cables d'àudio i els controladors de la targeta de so.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

<title>Sento el so entretallat o brunzits quan es reprodueixen sons</title>

  <p>Si sentiu espetecs o brunzits quan es reprodueixen sons a l'ordinador, és possible que tingueu un problema amb els cables d'àudio o els connectors, o un problema amb els controladors de la targeta de so.</p>

<list>
 <item>
  <p>Comproveu que els altaveus estiguin connectats correctament.</p>
  <p>Si els altaveus no estan totalment connectats o si estan connectats al sòcol incorrecte, és possible que sentiu un brunzit.</p>
 </item>

 <item>
  <p>Assegureu-vos que el cable de l'altaveu / auricular no està malmès.</p>
  <p>Els cables d'àudio i els connectors es poden desgastar gradualment amb l'ús. Intenteu connectar el cable o els auriculars a un altre dispositiu d'àudio (com un reproductor de MP3 o un reproductor de CD) per comprovar si encara sentiu espetecs. Si és així, és possible que hàgiu de reemplaçar el cable o els auriculars.</p>
 </item>

 <item>
  <p>Comproveu si els controladors de so no són massa bons.</p>
  <p>Algunes targetes de so no funcionen bé a Linux perquè no tenen controladors massa bons. Aquest problema és més difícil d'identificar. Intenteu cercar la marca i el model de la targeta de so a Internet, a més del terme de cerca "Linux", per veure si hi ha altres persones que tenen el mateix problema.</p>
  <p>Podeu utilitzar l'ordre <cmd>lspci</cmd> per obtenir més informació sobre la vostra targeta de so.</p>
 </item>
</list>

</page>
