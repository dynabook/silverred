<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-notifications" xml:lang="cs">

  <info>
    <link type="guide" xref="color#problems"/>
    <link type="seealso" xref="color-why-calibrate"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak můžete být upozorněni, když je váš profil barev zastaralý nebo nesprávný.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Mohu získat upozornění, když je můj profil barev nesprávný?</title>

  <p>Může vám být připomenuto, že máte provést rekalibraci zařízení po uplynutí nějaké doby. Bohužel ale není možné bez rekalibrace říci, jestli je profil zařízení ještě správný. Proto je lepší provádět rekalibraci v pravidelných intervalech.</p>

  <p>Některé firmy mají velmi specifické zásady vypršení platnosti kalibrace pro profily, protože nesprávný profil může mít velký vliv na koncový výrobek.</p>

  <p>Když si nastavíte zásadu s časovým omezením a profil bude starší, než bude určovat ona zásada, zobrazí se vedle postiženého profilu v panelu <gui>Barvy</gui> červený trojúhelník. Varovné upozornění se vám zobrazí pokaždé, když se k počítači přihlásíte.</p>

  <p>Když chcete nadefinovat zásadu pro displeje a tiskárny, zadejte nejvyšší možné stáří profilu ve dnech:</p>

<screen>
<output style="prompt">$ </output><input>gsettings set org.gnome.settings-daemon.plugins.color recalibrate-printer-threshold 180</input>
<output style="prompt">$ </output><input>gsettings set org.gnome.settings-daemon.plugins.color recalibrate-display-threshold 90</input>
</screen>

</page>
