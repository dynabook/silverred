<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="tips-specialchars" xml:lang="es">

  <info>
    <link type="guide" xref="tips"/>
    <link type="seealso" xref="keyboard-layouts"/>

    <revision pkgversion="3.8.2" version="0.3" date="2013-05-18" status="review"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.26" date="2017-11-27" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Escriba los caracteres que no se encuentran en el teclado, incluidos los alfabetos extranjeros, símbolos matemáticos y «dingbats».</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Insertar caracteres especiales</title>

  <p>Puede introducir y ver miles de caracteres de la mayoría de los sistemas de escritura del mundo, incluso los que no se encuentran en el teclado. En esta página se enumeran algunas de las diferentes maneras en las que puede introducir caracteres especiales.</p>

  <links type="section">
    <title>Métodos para introducir caracteres</title>
  </links>

  <section id="characters">
    <title>Caracteres</title>
    <p>GNOME incluye una aplicación de mapa de caracteres que le permite buscar e insertar caracteres poco habituales buscando en categorías de caracteres o buscando por palabras clave.</p>

    <p>Puede lanzar <app>Caracteres</app> desde la vista de actividades.</p>

  </section>

  <section id="compose">
    <title>Tecla de composición</title>
    <p>Una clave compuesta es una clave especial que le permite pulsar varias teclas seguidas para obtener un carácter especial. Por ejemplo, para usar la letras con tilde <em>é</em> puede pulsar <key>componer</key> despúes <key>'</key> y después <key>e</key>.</p>
    <p>Los teclados no tienen teclas de composición específicas. En su lugar, puede definir una de las teclas existentes en su teclado, como tecla para componer.</p>

    <note style="important">
      <p>Necesita tener instalado <app>Retoques</app> en su equipo para modificar esta opción.</p>
      <if:if xmlns:if="http://projectmallard.org/if/1.0/" test="action:install">
        <p><link style="button" action="install:gnome-tweak-tool">Instalar <app>Retoques</app></link></p>
      </if:if>
    </note>

    <steps>
      <title>Definir una tecla de composición</title>
      <item>
        <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Retoques</gui>.</p>
      </item>
      <item>
        <p>Pulse en <gui>Retoques</gui> para abrir la aplicación.</p>
      </item>
      <item>
        <p>Pulse en la pestaña <gui>Teclado y ratón</gui>.</p>
      </item>
      <item>
        <p>Pulse en <gui>Desactivado</gui> junto a la opción de <gui>Tecla de composición</gui>.</p>
      </item>
      <item>
        <p>Active el interruptor en el diálogo y elija el atajo del teclado que quiere usar.</p>
      </item>
      <item>
        <p>Marque la casilla de la tecla que quiere usar como Tecla de composición.</p>
      </item>
      <item>
        <p>Cierre el diálogo</p>
      </item>
      <item>
        <p>Cierre la ventana de la <gui>Retoques</gui>.</p>
      </item>
    </steps>

    <p>Puede teclear muchos caracteres comunes usando la tecla compuesta, por ejemplo:</p>

    <list>
      <item><p>Pulse <key>componer</key> después <key>'</key> después una letra para establecer la tilde en tal letra, tal como la <em>é</em>.</p></item>
      <item><p>Pulse <key>componer</key> después <key>`</key> después una letra para establecer un acento grave sobre tal letra, tal como <em>è</em>.</p></item>
      <item><p>Pulse <key>componer</key> después <key>"</key> después una letra para establecer una diéresis en tal letra, tal como la <em>ë</em>.</p></item>
      <item><p>Pulse <key>componer</key> después <key>-</key> después una letra para establecer un macrón sobre la letra, tal como la <em>ë</em>.</p></item>
    </list>
    <p>Para saber más sobre teclas de composición, consulte <link href="http://en.wikipedia.org/wiki/Compose_key#Common_compose_combinations">la página de teclas de composición en Wikipedia</link>.</p>
  </section>

<section id="ctrlshiftu">
  <title>Puntos de código</title>

  <p>Puede introducir cualquier carácter Unicode usando solo el teclado con el punto de código numérico del carácter. Cada carácter se identifica con un punto de código de cuatro caracteres. Para encontrar el punto de código de un carácter, busque el carácter en la aplicación <app>Caracteres</app>. El punto de código son los cuatro caracteres que hay después de <gui>U+</gui>.</p>

  <p>Para introducir un carácter por su punto de código, <keyseq><key>Ctrl</key><key>Mayús</key><key>U</key></keyseq> seguido de los cuatros caracteres del código y pulse <key>Espacio</key> o <key>Intro</key>. Si suele usar caracteres a los que no puede acceder fácilmente con otros métodos, puede que le resulte útil memorizar el código para esos caracteres de manera que pueda introducirlos rápidamente.</p>

</section>

  <section id="layout">
    <title>Distribuciones de teclado</title>
    <p>Puede hacer que el teclado se comporte como el teclado de otro idioma, independientemente de las letras impresas en las teclas. Incluso puede cambiar fácilmente entre diferentes distribuciones de teclado con un icono en la barra superior. Para saber cómo hacerlo, consulte la sección <link xref="keyboard-layouts"/>.</p>
  </section>

<section id="im">
  <title>Métodos de entrada</title>

  <p>Un método de entrada expande los métodos anteriores permitiendo introducir caracteres no sólo con el teclado sino también con otros dispositivos de entrada. Por ejemplo, puede introducir caracteres con un ratón usando un gesto, o introducir caracteres en japonés usando un teclado latino.</p>

  <p>Para elegir un método de entrada, pulse con el botón derecho sobre un widget de texto y, en el menú <gui>Método de entrada</gui>, elija el método de entrada que quiere usar. No hay ninguno predeterminado, por lo que puede consultar la documentación de los métodos de entrada para saber cómo usarlos.</p>

</section>

</page>
