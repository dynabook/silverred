<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-othersconnect" xml:lang="mr">

  <info>
    <link type="guide" xref="net-problem"/>
    <link type="seealso" xref="net-othersedit"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>नेटवर्क जोडणीकरिता सेटिंग्ज साठवणे शक्य आहे (जसे कि पासवर्ड) जेणेकरून कोणिही संगणक वापर असल्यास संगणकाशी जोडणी करू शकेल.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

  <title>Other users can’t connect to the internet</title>

  <p>नेटवर्क जोडणी सेटअप करतेवेळी, संगणकावरील इतर सर्व वापरकर्ते सर्वसाधरणपे त्याचा वापर शकतील. जोडणी माहिती शेअर केली नसल्यास, जोडणी सेटिंग्ज तपासा.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Network</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Network</gui> to open the panel.</p>
    </item>
    <item>
      <p>Select <gui>Wi-Fi</gui> from the list on the left.</p>
    </item>
    <item>
      <p>Click the 
      <media its:translate="no" type="image" src="figures/emblem-system.png"><span its:translate="yes">settings</span></media> button to open the connection
      details.</p>
    </item>
    <item>
      <p>Select <gui>Identity</gui> from the pane on the left.</p>
    </item>
    <item>
      <p>At the bottom of the <gui>Identity</gui> panel, check the <gui>Make
      available to other users</gui> option to allow other users to use the
      network connection.</p>
    </item>
    <item>
      <p>Press <gui style="button">Apply</gui> to save the changes.</p>
    </item>
  </steps>

  <p>संगणकाचे इतर वापरकर्ते आत्ता पुढील तपशील न देता ह्या जोडणीचा वापर करू शकतील.</p>

  <note>
    <p>कोणताही वापरकर्ता ही सेटिंग बदलवू शकतो.</p>
  </note>

</page>
