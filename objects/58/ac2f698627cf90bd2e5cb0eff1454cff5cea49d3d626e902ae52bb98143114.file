<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="keyboard-key-menu" xml:lang="gu">

  <info>
    <link type="guide" xref="keyboard" group="a11y"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>
    <link type="seealso" xref="a11y#mobility" group="keyboard"/>

    <revision pkgversion="3.7.91" version="0.2" date="2013-03-16" status="new"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>જુઆન્જો મારીન</name>
      <email>juanj.marin@juntadeandalucia.es</email>
    </credit>
    <credit type="editor">
      <name>ઍકાટેરીના ગેરાસીમોવા</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>જમણી ક્લિક સાથે કરવા કરતા કિબોર્ડ સાથે <key>Menu</key> કી એ સંદર્ભ મેનું શરૂ કરે છે.</desc>
  </info>

  <title><key>મેનુ</key> કી શુ છે?</title>

  <p>The <key>Menu</key> key, also known as the <em>Application</em> key, is a
  key found on some Windows-oriented keyboards. This key is usually on the
  bottom-right of the keyboard, next to the <key>Ctrl</key> key, but it can be
  placed in a different location by keyboard manufacturers. Its is usually
  depicted as a cursor hovering above a menu:
  <media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-menu.svg">
  <key>Menu</key> key icon</media>.</p>

  <p>જમણાં માઉસ બટનને ક્લિક કરવાને બદલે કિબોર્ડ સાથે સંદર્ભ મેનુ ને શરૂ કરવા આ કીનું પ્રાથમિક વિધેય છે:  આ ઉપયોગી છે જો માઉસ અથવા સરખા ઉપકરણ એ ઉપલબ્ધ ન હોય તો, અથવા જ્યારે જમણું માઉસ બટન હાજર ન હોય તો.</p>

  <p><key>Menu</key>  કી અમુકવાર જગ્યાની પસંદગીમાં કાઢેલ છે, ખાસ કરીને પોર્ટેબલ અને લેપટોપ કિબોર્ડ પર. આ સ્થિતિમાં, અમુક કિબોર્ડ <key>Menu</key> વિધેય કીને સમાવે છે કે જે વિધેય (<key>Fn</key>) કી સાથે સંયોજનમાં સક્રિય કરી શકાય છે.</p>

  <p><em>સંદર્ભ મેનુ</em> એ મેનુ છે કે જે પોપ કરે છે જ્યારે તમે જમણી ક્લિક કરો. મેનુ કે જે તમે જુઓ છો, જો કોઇપણ હોય તો, વિસ્તારનાં સંદર્ભ અને વિધેય પર આધારિત હોય છે કે જે તમે જમણી ક્લિક કરેલ હોય. જ્યારે તમે <key>મેનુ</key> કીને વાપરે ચો, સંદર્ભ મેનુ એ સ્ક્રીનનાં વિસ્તાર માટે બતાવેલ છે કે જે તમારું કર્સર એ બિંદુ પર હોય જ્યારે કી દબાવેલ હોય.</p>

</page>
