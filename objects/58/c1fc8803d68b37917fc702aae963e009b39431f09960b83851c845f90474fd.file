<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-launch-applications" xml:lang="mr">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>

    <link type="guide" xref="getting-started" group="videos"/>
    <title role="trail" type="link">ॲप्लिकेशन्स लॉँच करा</title>
    <link type="seealso" xref="shell-apps-open"/>
    <title role="seealso" type="link">ॲप्लिकेशन्स लॉँच करण्याबाबत वर्गपाठ</title>
    <link type="next" xref="gs-switch-tasks"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande</mal:name>
      <mal:email>djaniketster@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>ॲप्लिकेशन्स लॉँच करा</title>

  <ui:overlay width="812" height="452">
   <media type="video" its:translate="no" src="figures/gnome-launching-applications.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-launching-apps.svg"/>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>ॲप्लिकेशन्स लॉँच करत आहे</tt:p>
         </tt:div>
         <tt:div begin="5s" end="7.5s">
           <tt:p>तुमचा माऊस प्वाइन्टर <gui>कृती</gui> येथे हलवा जे स्क्रीनच्या सगळ्यात वर डाव्या हाताच्या कोपऱ्यावर आहे.</tt:p>
           </tt:div>
         <tt:div begin="7.5s" end="9.5s">
           <tt:p><gui>ॲप्लिकेशन्स दाखवा</gui> चिन्हावर क्लिक करा.</tt:p>
         </tt:div>
         <tt:div begin="9.5s" end="11s">
           <tt:p>जे ॲपलिकेशन सुरु करायचं असेल त्यावर क्लिक करा, उदाहरण, मदत.</tt:p>
         </tt:div>
         <tt:div begin="12s" end="21s">
           <tt:p>पर्यायी, <gui>कार्य सारांश</gui> बघायला कीबोर्डवर <key href="help:gnome-help/keyboard-key-super">Super</key> दाबा.</tt:p>
         </tt:div>
         <tt:div begin="22s" end="29s">
           <tt:p>त्या ॲपलिकेशनचं नाव टाइप करायला सुरुवात करा जे लॉँच तुम्हाला करायचं आहे.</tt:p>
         </tt:div>
         <tt:div begin="30s" end="33s">
           <tt:p>ॲपलिकेशन लॉँच करण्यासाठी <key>Enter</key> दाबा.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
   </media>
  </ui:overlay>

  <section id="launch-apps-mouse">
    <title>माउसने ॲप्लिकेशन्स लॉँच करा</title>
 
  <steps>
    <item><p><gui>कार्य सारांश</gui> दाखवण्यासाठी तुमचा माऊस प्वाइन्टर <gui>कृती</gui> येथे हलवा जे स्क्रीनच्या सगळ्यात वर डाव्या हाताच्या कोपऱ्यावर आहे.</p></item>
    <item><p><gui>ॲप्लिकेशन्स दाखवा</gui> चिन्हावर क्लिक करा.</p></item>
    <item><p>ॲपलिकेशन्सची यादी दाखवलेली आहे. जे ॲपलिकेशन सुरु करायचं असेल त्यावर क्लिक करा, उदाहरण, मदत.</p></item>
  </steps>

  </section>

  <section id="launch-app-keyboard">
    <title>कीबोर्डने ॲप्लिकेशन्स लॉँच करा</title>

  <steps>
    <item><p><gui>कार्य सारांश</gui> <key href="help:gnome-help/keyboard-key-super">Super</key> दाबुन उघडा.</p></item>
    <item><p>त्या ॲपलिकेशनचं नाव टाइप करायला सुरुवात करा जे लॉँच तुम्हाला करायचं आहे. ॲपलिकेशन शोध लगेच सुरु होतं.</p></item>
    <item><p>जेव्हा ॲपलिकेशन दाखवलं आणि निवडलं जाईल, <key>Enter</key> दाबुन ॲपलिकेशन लॉँच करा.</p></item>
  </steps>

  </section>

</page>
