<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="fs-device" xml:lang="fr">

  <info>
    <revision version="0.1" date="2014-01-26" status="review"/>
    <link type="guide" xref="index#filesystems" group="filesystems"/>
    <link type="seealso" xref="fs-info"/>
    
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    
    <credit type="author copyright">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
      <years>2014</years>
    </credit>

    <desc>Chaque périphérique correspond à une <em>partition</em> sur le disque dur.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>naybnet</mal:name>
      <mal:email>naybnet@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leonor Palazzo</mal:name>
      <mal:email>leonor.palazzo@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Les différents périphériques de l'onglet Systèmes de fichiers</title>

  <p>Chaque périphérique listé dans l'onglet <gui>Systèmes de fichiers</gui> est un périphérique de stockage (comme un disque dur ou une clé USB) ou une partition de disque. Pour chacun d'eux, vous pouvez voir la capacité totale, la quantité d'espace utilisée et des informations techniques concernant le <link xref="fs-info">type de système de fichiers</link> et l'<link xref="fs-info">endroit où il est « monté »</link>.</p>
  
  <p>L'espace disque d'un disque dur physique peut être divisé en plusieurs parties appelées <em>partitions</em>. Chacune d'entre elles peut être utilisée comme un disque séparé. Si votre disque dur a été partitionné (peut-être par vous ou par le constructeur de l'ordinateur), chaque partition est listée séparément dans la liste du système de fichiers.</p>
  
  <note>
    <p>Vous pouvez gérer les disques et les partitions et accéder à des informations plus détaillées avec l'application <app>Disques</app>.</p>
  </note>

</page>
