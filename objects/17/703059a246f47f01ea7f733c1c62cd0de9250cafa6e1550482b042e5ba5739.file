<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-dwellclick" xml:lang="ro">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="clicking"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-21" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Funcționalitatea <gui>Clic plutitor</gui> (clic Dwell) vă permite să efectuați clicuri ținând mausul nemișcat.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Șerbănescu</mal:name>
      <mal:email>daniel [at] serbanescu [dot] dk</mal:email>
      <mal:years>2016, 2019</mal:years>
    </mal:credit>
  </info>

  <title>Simularea clicului prin plutire</title>

  <p>Puteți efectua clic sau să trageți mausul prin pasarea cursorului deasupra unui control sau obiect de pe ecran. Aceasta este utilă dacă vă este dificil să mișcați mausul și să apăsați clic în același timp. Această funcționalitate se numește <gui>Clic plutitor</gui> sau clic Dwell.</p>

  <p>Când <gui>Clic plutitor</gui> este activat, puteți muta cursorul mausului deasuprea unui control, apoi țineți mausul nemișcat, și așteptați un moment înainte ca butonul să fie apăsat automat.</p>

  <steps>
    <item>
      <p>Deschideți vederea de ansamblu <gui xref="shell-introduction#activities">Activități</gui> și tastați <gui>Acces universal</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Universal Access</gui> to open the panel.</p>
    </item>
    <item>
      <p>Apăsați <gui>Asistență la clic</gui> în secțiunea <gui>Cursor și maus</gui>.</p>
    </item>
    <item>
      <p>Switch <gui>Hover Click</gui> to on.</p>
    </item>
  </steps>

  <p>Fereastra <gui>Clic plutitor</gui> se va deschide și va rămâne deasupra tuturor celorlalte ferestre. Puteți utiliza această fereastră pentru a alege ce tip de clic ar trebui să aibă efect atunci când plasați cursorul. De exeplu, dacă selectați <gui>Clic secundar</gui> veți apăsa clic dreapta atunci când plasați cursorul. După ce se efectuează dublu-clic, clic dreapta sau o tragere se va reveni automat la clic obișnuit.</p>

  <p>Când plasați cursorul mausului deasupra unui buton și nu îl mișcați, își va schimba gradat culoarea. Când culoarea s-a schimbat complet, butonul va fi apăsat.</p>

  <p>Ajustați configurarea de <gui>Întârziere</gui> pentru a modifica cât de mult timp trebuie să țineți cursorul mausului nemișcat înainte de a efectua un clic.</p>

  <p>Nu trebuie să țineți mausul complet nemișcat când folosiți clicul plutitor. Cursorului îi este permis să se miște un pic înainte de a efectua un clic după o perioadă de timp. Dacă se mișcă prea mult clicul nu va avea loc.</p>

  <p>Ajustați configurarea <gui>Prag de mișcare</gui> pentru a modifica cât de mult poate cursorul să se miște pentru a fi considerat plutitor.</p>

</page>
