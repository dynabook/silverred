<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-switching" xml:lang="da">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>
    <link type="guide" xref="shell-overview#apps"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.12" date="2014-03-07" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>GNOMEs dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>


    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Tryk på <keyseq><key>Super</key><key>Tab</key></keyseq>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years/>
    </mal:credit>
  </info>

<title>Skift mellem vinduer</title>

  <p>I <em>vinduesskifteren</em> kan du se alle de kørende programmer, der har en grafisk brugerflade. Så skal der kun udføres ét trin for at skifte mellem opgaver, og det giver et fuldt billede af, hvilke programmer som kører.</p>

  <p>Fra et arbejdsområde:</p>

  <steps>
    <item>
      <p>Tryk på <keyseq><key xref="keyboard-key-super">Super</key><key>Tab</key></keyseq> for at få <gui>vinduesskifteren</gui> frem.</p>
    </item>
    <item>
      <p>Slip <key xref="keyboard-key-super">Super</key> for at vælge det næste (fremhævede) vindue i skifteren.</p>
    </item>
    <item>
      <p>Ellers bliv ved med at holde <key xref="keyboard-key-super">Super</key>-tasten nede, tryk på <key>Tab</key> for at gennemløbe listen over åbne vinduer, eller <keyseq><key>Skift</key><key>Tab</key></keyseq> for at gennemløbe baglæns.</p>
    </item>
  </steps>

  <p if:test="platform:gnome-classic">Du kan også bruge vindueslisten på bundlinjen for at få adgang til alle dine åbne vinduer og skifte mellem dem.</p>

  <note style="tip" if:test="!platform:gnome-classic">
    <p>Vinduerne i vinduesskifteren grupperes efter programmer. Forhåndsvisninger af programmer med flere vinduer popper ned efterhånden som du klikker. Hold <key xref="keyboard-key-super">Super</key> nede og tryk på <key>½</key> (eller tasten over <key>Tab</key>) for at gennemløbe listen.</p>
  </note>

  <p>Du kan også flytte mellem programikonerne i vinduesskifteren med <key>→</key>- eller <key>←</key>-tasterne eller vælge et ved at klikke på det med musen.</p>

  <p>Forhåndsvisninger af programmer med et enkelt vindue kan vises med <key>↓</key>-tasten.</p>

  <p>Fra <gui>Aktivitetsoversigten</gui> kan du klikke på et <link xref="shell-windows">vindue</link> for at skifte til det og forlade oversigten. Hvis du har flere åbne <link xref="shell-windows#working-with-workspaces">arbejdsområder</link>, så kan du klikke på et arbejdsområde for at vise de åbne vinduer på hvert arbejdsområde.</p>

</page>
