<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="process-identify-file" xml:lang="es">
  <info>
    <revision version="0.2" pkgversion="3.11" date="2014-01-26" status="review"/>
    <link type="guide" xref="index#processes-tasks" group="processes-tasks"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author copyright">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
      <years>2011</years>
    </credit>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2011, 2014</years>
    </credit>

    <desc>Buscar un archivo abierto para mostrar qué proceso lo está usando.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2014 - 2015</mal:years>
    </mal:credit>
  </info>

  <title>Saber qué programa está usando un archivo en concreto</title>

  <p>A veces un mensaje de error dice que un dispositivo (como la tarjeta de sonido o el DVD) está ocupado, o que el archivo que quiere editar se está usando. Para buscar el proceso o los procesos responsables.</p>

    <steps>
      <item><p>Pulse en <guiseq><gui>Monitor del sistema</gui><gui>Buscar archivos abiertos</gui></guiseq>.</p>
      </item>
      <item><p>Introduzca el nombre del archivo o una parte del nombre. Esto puede ser <file>/dev/snd</file> para el dispositivo de sonido o <file>/media/cdrom</file> para el DVD.</p>
      </item>
      <item><p>Pulse <gui>Buscar</gui>.</p>
      </item>
    </steps>

  <p>Esto mostrará una lista de los procesos en ejecución que actualmente están accediendo a los archivos que coincidan con la búsqueda. Salir del programa le debería permitir acceder al dispositivo o editar el archivo.</p>

</page>
