<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-cancel-job" xml:lang="gu">

  <info>
    <link type="guide" xref="printing#problems"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>ફીલ બુલ</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>જીમ કેમ્પબેલ</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>જાના સ્વરોવા</name>
      <email>jana.svarova@gmail.com</email>
      <years>૨૦૧૩</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>સ્થગિત પ્રિન્ટ કાર્યને રદ કરો અને કતારમાંથી તેને દૂર કરો.</desc>
  </info>

  <title>પ્રિન્ટ કાર્યને રદ કરો, અટકાવો અથવા પ્રકાશિત કરો</title>

  <p>સ્થગિત પ્રિન્ટ કાર્યને તમે રદ કરી શકો છો અને પ્રિન્ટર સુયોજનમાંંથી તેને દૂર કરો.</p>

  <section id="cancel-print-job">
    <title>પ્રિન્ટ કાર્યને રદ કરો</title>

  <p>જો તમે આકસ્મિક રીતે દસ્તાવેજને છાપવાનું શરૂ કરેલ હોય તો, તમે છાપવાનું રદ કરી શકો છો તેથી તમારે કોઇપણ સહી અથવા પેપરનો કચરો કરવાની જરૂર નથી.</p>

  <steps>
    <title>કેવી રીકે છાપન કાર્યને રદ કરવુ:</title>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Printers</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Printers</gui> to open the panel.</p>
    </item>
    <item>
      <p><gui>પ્રિન્ટર</gui> સંવાદની જમણી બાજુ પર <gui>કાર્ય બતાવો</gui> બટન પર ક્લિક કરો.</p>
    </item>
    <item>
      <p>Cancel the print job by clicking the stop button.</p>
    </item>
  </steps>

  <p>If this does not cancel the print job like you expected, try holding down
  the <em>cancel</em> button on your printer.</p>

  <p>As a last resort, especially if you have a big print job with a lot of
  pages that will not cancel, remove the paper from the printer’s paper input
  tray. The printer should realize that there is no paper and will stop
  printing. You can then try canceling the print job again, or try turning the
  printer off and then on again.</p>

  <note style="warning">
    <p>Be careful that you don’t damage the printer when removing the paper,
    though. If you would have to pull hard on the paper to remove it, you
    should probably just leave it where it is.</p>
  </note>

  </section>

  <section id="pause-release-print-job">
    <title>પ્રિન્ટ કાર્યને અટકાવો અને પ્રકાશિત કરો</title>

  <p>જો તમે પ્રિન્ટ કાર્યને અટકાવવા અથવા પ્રકાશિત કરવા માંગો તો, તમે પ્રિન્ટર સુયોજનોમાં કાર્યોમાં જઇને કરી શકો છો.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Printers</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Printers</gui> to open the panel.</p>
    </item>
    <item>
      <p><gui>પ્રિન્ટર</gui> સંવાદની જમણી બાજુ પર <gui>કાર્ય બતાવો</gui> બટન પર ક્લિક કરો અને ક્યાંતો તમારી જરૂરિયાતો પર આધારિત પ્રિન્ટ કાર્યને અટકાવો અથવા પ્રકાશિત કરો.</p>
    </item>
  </steps>

  </section>

</page>
