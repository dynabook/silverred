<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-open" xml:lang="as">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-30" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="author">
      <name>ক্ৰিস্টফাৰ থমাচ</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>শ্বণ মেকেন্স</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Open files using an application that isn’t the default one for that
    type of file. You can change the default too.</desc>
  </info>

<title>অন্য এপ্লিকেচনসমূহৰ সৈতে ফাইলসমূহ খোলক</title>

  <p>When you double-click (or middle-click) a file in the file manager, it
  will be opened with the default application for that file type. You can open
  it in a different application, search online for applications, or set the
  default application for all files of the same type.</p>

  <p>To open a file with an application other than the default, right-click
  the file and select the application you want from the top of the menu. If
  you do not see the application you want, select <gui>Open With Other
  Application</gui>. By default, the file manager only shows applications that
  are known to handle the file. To look through all the applications on your
  computer, click <gui>View All Applications</gui>.</p>

<p>If you still cannot find the application you want, you can search for
more applications by clicking <gui>Find New Applications</gui>. The
file manager will search online for packages containing applications
that are known to handle files of that type.</p>

<section id="default">
  <title>অবিকল্পিত এপ্লিকেচন পৰিবৰ্তন কৰক</title>
  <p>আপুনি এটা ধৰণৰ ফাইলসমূহ খোলিবলৈ ব্যৱহৃত অবিকল্পিত এপ্লিকেচন পৰিবৰ্তন কৰিব পাৰিব। ইয়াৰ বাবে আপুনি আপোনাৰ পছন্দৰ এপ্লিকেচন খোলিব পাৰিব যেতিয়া আপুনি এটা ফাইল খোলিবলৈ দুবাৰ-ক্লিক কৰে। উদাহৰণস্বৰূপ, আপুনি এটা MP3 ফাইল দুবাৰ-ক্লিক কৰোতে আপোনাৰ পছন্দৰ সংগিত প্লেয়াৰ খোলাটো বিচাৰে।</p>

  <steps>
    <item><p>এটা ফাইল বাছক যাৰ বাবে আপুনি অবিকল্পিত এপ্লিকেচন সলনি কৰিব খোজে। উদাহৰণস্বৰূপ, MP3 ফাইলসমূহ কোনো এপ্লিকেচন দ্বাৰা খোলা হব পৰিবৰ্তন কৰিবলৈ, এটা <file>.mp3</file> ফাইল বাছক।</p></item>
    <item><p>ফাইল ৰাইট-ক্লিক কৰক আৰু <gui>বৈশিষ্ট্যসমূহ</gui> বাছক।</p></item>
    <item><p><gui>চিহ্নিত বস্তুৰ সৈতে খোলক</gui> টেব বাছক।</p></item>
    <item><p>Select the application you want and click
    <gui>Set as default</gui>.</p>
    <p>If <gui>Other Applications</gui> contains an application you sometimes
    want to use, but do not want to make the default, select that application
    and click <gui>Add</gui>. This will add it to <gui>Recommended
    Applications</gui>. You will then be able to use this application by
    right-clicking the file and selecting it from the list.</p></item>
  </steps>

  <p>ই কেৱল নিৰ্বাচিত ফাইলৰ বাবেই নহয়, একে ধৰণৰ সকলো ফাইলৰ বাবে অবিকল্পিত এপ্লিকেচন পৰিবৰ্তন কৰে।</p>

<!-- TODO: mention resetting the open with list with the "Reset" button -->

</section>

</page>
