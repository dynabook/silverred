<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-othercountry" xml:lang="de">

  <info>
    <link type="guide" xref="power#problems"/>
    <desc>Ihr Rechner wird funktionieren, aber Sie werden möglicherweise ein anderes Stromversorgungskabel oder einen Reiseadapter benötigen.</desc>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2019.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2019.</mal:years>
    </mal:credit>
  </info>

<title>Wird mein Rechner an einem Netzanschluss eines anderen Landes funktionieren?</title>

<p>Verschiedene Länder nutzen verschiedene Netzspannungen (meist 110V oder 220–240V) und Frequenzen (50 oder 60 Hz). Ihr Rechner sollte auch am Stromnetz eines anderen Landes funktionieren, sofern Sie einen entsprechenden Netzadapter haben. Vielleicht genügt es auch, einen Schalter umzulegen.</p>

<p>Wenn Sie einen Laptop verwenden, benötigen Sie nur den richtigen Stecker für Ihr Netzteil. Viele Laptops werden mit mehr als einem Stecker für Ihr Netzteil ausgeliefert, vielleicht haben Sie also bereits den richtigen. Wenn nicht, können Sie den mitgelieferten Stecker über einen Reiseadapter mit der Steckdose verbinden.</p>

<p>Bei einem Arbeitsplatzrechner können Sie ebenfalls ein Kabel mit einem anderen Stecker verwenden oder einen Reiseadapter verwenden. In diesem Fall müssen Sie jedoch ggf. den Spannungsschalter des Netzteils umstellen, falls vorhanden. Viele Rechner verfügen nicht über einen solchen Schalter und funktionieren einwandfrei mit beiden Spannungen. Sehen Sie auf der Rückseite dort nach, wo das Stromkabel angeschlossen wird. Daneben könnte sich ein kleiner Schalter (zum Beispiel) mit den Beschriftungen »110V« oder »230V« befinden. Stellen Sie diesen falls nötig um.</p>

<note style="warning">
  <p>Seien Sie vorsichtig, wenn Sie Netzkabel wechseln oder Reiseadapter verwenden. Schalten Sie alles aus, wenn möglich.</p>
</note>

</page>
