<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="a11y task" id="a11y-visualalert" xml:lang="ta">

  <info>
    <link type="guide" xref="a11y#sound"/>
    <link type="seealso" xref="sound-alert"/>

    <revision pkgversion="3.7.1" date="2012-11-10" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>ஷான் மெக்கேன்ஸ்</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>மைக்கேல் ஹில்</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>எக்காட்டெரினா ஜெராசிமோவா</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>விழிப்பூட்டல் இயக்கப்படும் போது திரை அல்லது சாளரத்தில் பளிச் ஒளி எழுப்புவதற்கு, காட்சி விழிப்பூட்டல்களை இயக்கவும்.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>விழிப்பூட்டல் ஒலிகளுக்கு திரையில் பளிச் ஒளியெழுப்பு</title>

  <p>சில வகை செய்திகள் மற்றும் நிகழ்வுகளுக்கு உங்கள் கணினி ஒரு எளிய விழிப்பூட்டலை இயக்கும். இந்த ஒலிகளைக் கேட்க உங்களுக்கு கடினமாக இருந்தால், விழிப்பூட்டல் ஒலி இயக்கப்படும் போது உங்கள் முழு திரை அல்லது உங்கள் நடப்பு சாளரம் காட்சிரீதியாக பளிச் ஒளி எழுப்பும் படி நீங்கள் அமைத்துக்கொள்ள முடியும்.</p>

  <p>This can also be useful if you’re in an environment where you need your
  computer to be silent, such as in a library. See <link xref="sound-alert"/>
  to learn how to mute the alert sound, then enable visual alerts.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Universal Access</gui>.</p>
    </item>
    <item>
      <p><gui>அனைவருக்குமான அணுகல்</gui> ஐத் திறந்து <gui>தட்டச்சு</gui> கீற்றைத் தேர்ந்தெடுக்கவும்.</p>
    </item>
    <item>
      <p><gui>அனைவருக்குமான அணுகல்</gui> ஐத் திறந்து <gui>கேட்டல்</gui> கீற்றைத் தேர்ந்தெடுக்கவும்.</p>
    </item>
    <item>
      <p>Switch the <gui>Visual Alerts</gui> switch to on.</p>
    </item>
    <item>
      <p><gui>காட்சி விழிப்பூட்டல்கள்</gui> ஐ இயக்கவும். உங்கள் முழு திரையும் பளிச்சிட வேண்டுமா அலல்து நடப்பு சாளரம் மட்டுமா எனத் தேர்ந்தெடுக்கவும்.</p>
    </item>
  </steps>

  <note style="tip">
    <p>நீங்கள் மேல் பட்டியில் உள்ள <link xref="a11y-icon">அணுகல் வசதி சின்னத்தை</link> சொடுக்கி <gui>காட்சி விழிப்பூட்டல்கள்</gui> என்பதைத் தேர்ந்தெடுப்பதன் மூலம் <gui>காட்சி விழிப்பூடல்கள்</gui> வசதியை விரைவாக இயக்கலாம், அணைக்கலாம்.</p>
  </note>

</page>
