<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="extensions-enable" xml:lang="cs">

  <info>
    <link type="guide" xref="software#extension"/>
    <link type="seealso" xref="extensions-lockdown"/>
    <link type="seealso" xref="extensions"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>
    <credit type="author">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
   </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak zapnout rozšíření GNOME Shell pro všechny uživatele.</desc>
  </info>

  <title>Zapnutí celosystémových rozšíření</title>
  
  <p>Abyste rozšíření zpřístupnili všem uživatelům systému, nainstalujte je do složky <file>/usr/share/gnome-shell/extensions</file>. Pamatujte, že nově nainstalovaná rozšíření pro celý systém jsou ve výchozím stavu zakázaná.</p>

  <p>Když chcete nastavit výchozí povolená rozšíření, musíte nastavit klíč <code>org.gnome.shell.enabled-extensions</code>. V současnosti ale neexistuje způsob, jak povolit dodatečná rozšíření uživatelům, kteří jsou již přihlášení. To neplatí pro stávající uživatele, kteří mají nainstalovaná a povolená svá vlastní rozšíření GNOME.</p>

  <steps>
    <title>Nastavení klíče org.gnome.shell.enabled-extensions</title>
    <item>
      <p>Vytvořte profil <em>user</em> v <file>/etc/dconf/profile/user</file>:</p>
      <listing>
        <code>
user-db:user
system-db:local
</code>
      </listing>
    </item>
    <item>
      <p>Vytvořte databázi <em>local</em> pro celosystémové nastavení v <file>/etc/dconf/db/local.d/00-extensions</file>:</p>
      <listing>
        <code>
[org/gnome/shell]
# Seznam rozšíření, která chcete mít povolená pro všechny uživatele
enabled-extensions=['<input>myextension1@myname.example.com</input>', '<input>myextension2@myname.example.com</input>']
</code>
      </listing>
      <p>Klíč <code>enabled-extensions</code> povoluje rozšíření pomocí jejich uuid (<code>mojerozsireni1@mojejmeno.priklad.cz</code> a <code>mojerozsireni2@mojejmeno.priklad.cz</code>).</p>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  </steps>

</page>
