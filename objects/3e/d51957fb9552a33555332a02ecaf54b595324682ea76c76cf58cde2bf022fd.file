<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="video-dvd" xml:lang="pl">

  <info>
    <link type="guide" xref="media#videos"/>
    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.12.1" date="2014-03-30" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>

    <credit type="author">
      <name>Projekt dokumentacji GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Właściwe kodeki nie są zainstalowane lub płyta DVD może mieć błędny region.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  </info>

  <title>Dlaczego nie mogę odtwarzać płyt DVD?</title>

  <p>Jeśli włożono płytę DVD do komputera i nie chce się odtworzyć, to właściwe <em>kodeki</em> DVD mogą nie być zainstalowane lub płyta DVD może być z innego <em>regionu</em>.</p>

<section id="codecs">
  <title>Instalowanie właściwych kodeków do odtwarzania płyt DVD</title>

  <p>Aby odtwarzać płyty DVD, muszą być zainstalowane właściwe <em>kodeki</em>. Kodek to oprogramowanie umożliwiające programom odczytywanie formatów wideo i audio. Jeśli odtwarzacz filmów nie odnalazł właściwych kodeków, to może umożliwić ich zainstalowanie. Jeśli nie, to należy zainstalować je ręcznie — zapytaj o pomoc, jak to zrobić, na przykład na forum wsparcia używanej dystrybucji systemu Linux.</p>

  <p>Płyty DVD są także <em>chronione przed kopiowaniem</em> za pomocą systemu o nazwie CSS. Uniemożliwia to kopiowanie płyt DVD, ale uniemożliwia także ich odtwarzanie, jeśli nie zainstalowano dodatkowego oprogramowania do obsługi ochrony przed kopiowaniem. To oprogramowanie jest dostępne w wielu dystrybucjach systemu Linux, ale nie może być legalnie używane we wszystkich krajach. Można kupić komercyjny dekoder płyt DVD obsługujący ochronę przed kopiowaniem od firmy <link href="https://fluendo.com/en/products/multimedia/oneplay-dvd-player/">Fluendo</link>. Działa on w systemie Linux i powinien być legalny do użycia we wszystkich krajach.</p>

  </section>

<section id="region">
  <title>Sprawdzanie regionu płyty DVD</title>

  <p>Płyty DVD mają <em>kod regionu</em>, który mówi, w jakim regionie świata można je odtwarzać. Jeśli region napędu DVD komputera nie pasuje do regionu płyty DVD, to nie będzie można jej odtworzyć. Na przykład, w napędzie DVD 2. regionu można odtwarzać tylko płyty DVD z Europy, Bliskiego Wschodu, Japonii i RPA, ale już nie ze Stanów Zjednoczonych.</p>

  <p>Często możliwa jest zmiana regionu używanego przed napęd DVD, ale można to zrobić tylko kilka razy, zanim zostanie on trwale zablokowany na jeden region. Aby zmienić region DVD napędu w komputerze, użyj programu <link href="http://linvdr.org/projects/regionset/">regionset</link>.</p>

  <p><link href="https://pl.wikipedia.org/wiki/Region_DVD">Artykuł o regionach DVD w Wikipedii</link> zawiera więcej informacji.</p>

</section>

</page>
