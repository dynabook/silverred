<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="keyboard-key-super" xml:lang="hu">

  <info>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.7.91" version="0.2" date="2013-03-16" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-27" status="review"/>

    <credit type="author">
      <name>GNOME dokumentációs projekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>A <key>Super</key> billentyű megnyitja a <gui>Tevékenységek</gui> áttekintést. Ez általában az <key>Alt</key> billentyű mellett van.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Mi az a <key>Super</key> billentyű?</title>

  <p>A <key>Super</key> billentyű megnyomásakor megjelenik a <gui>Tevékenységek</gui> áttekintés. Ez a billentyű általában a billentyűzet bal alsó sarkában található, az <key>Alt</key> billentyű mellett, és egy Windows-logó látható rajta. Néha <em>Windows</em> billentyűnek vagy rendszer billentyűnek is hívják.</p>

  <note>
    <p>Ha Apple billentyűzete van, akkor a Windows billentyű helyett a <key>⌘</key> (Command) billentyűt használhatja, míg a Chromebook készülékeken egy nagyító billentyű található.</p>
  </note>

  <p>A <gui>Tevékenységek</gui> áttekintés megjelenítéséhez használt billentyű megváltoztatása:</p>

  <steps>
    <item>
      <p>Nyissa meg a <gui xref="shell-introduction#activities">Tevékenységek</gui> áttekintést, és kezdje el begépelni a <gui>Beállítások</gui> szót</p>
    </item>
    <item>
      <p>Válassza a <gui>Beállítások</gui> lehetőséget.</p>
    </item>
    <item>
      <p>Kattintson az <gui>Eszközök</gui> lehetőségre az oldalsávon.</p>
    </item>
    <item>
      <p>Kattintson a <gui>Billentyűzet</gui> elemre az oldalsávon a panel megnyitásához.</p>
    </item>
    <item>
      <p>A <gui>Rendszer</gui> kategórián belül kattintson a <gui>Tevékenységek áttekintés megjelenítése</gui> sorra.</p>
    </item>
    <item>
      <p>Tartsa lenyomva a kívánt billentyűkombinációt.</p>
    </item>
  </steps>

</page>
