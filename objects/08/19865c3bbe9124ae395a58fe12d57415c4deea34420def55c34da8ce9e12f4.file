<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="net-email-virus" xml:lang="vi">

  <info>
    <link type="guide" xref="net-email"/>
    <link type="guide" xref="net-security"/>
    <link type="seealso" xref="net-antivirus"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Viruses are unlikely to infect your computer, but could infect the
    computers of people you email.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nguyễn Thái Ngọc Duy</mal:name>
      <mal:email>pclouds@gmail.com</mal:email>
      <mal:years>2011-2012.</mal:years>
    </mal:credit>
  </info>

  <title>Tôi có cần quét virút trong email của tôi không?</title>

  <p>Virút là chương trình phá hoại nếu có thể thâm nhập vào máy bạn. Cách thông thường để vào máy bạn là thông qua email.</p>

  <p>Viruses that can affect computers running Linux are quite rare, so you are
  <link xref="net-antivirus">unlikely to get a virus through email or
  otherwise</link>. If you receive an email with a virus hidden in it, it will
  probably have no effect on your computer. As such, you probably don’t need to
  scan your email for viruses.</p>

  <p>You may, however, wish to scan your email for viruses in case you happen
  to forward a virus from one person to another. For example, if one of your
  friends has a Windows computer with a virus and sends you a virus-infected
  email, and you then forward that email to another friend with a Windows
  computer, then the second friend might get the virus too. You could install
  an anti-virus application to scan your emails to prevent this, but it’s
  unlikely to happen and most people using Windows and Mac OS have anti-virus
  software of their own anyway.</p>

</page>
