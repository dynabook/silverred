<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-multi-monitor" xml:lang="cs">

  <info>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.28" date="2018-07-22" status="review"/>
    <revision pkgversion="3.33" date="2019-07-21" status="candidate"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak namapovat grafický tablet na konkrétní obrazovku.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Výběr obrazovky</title>

<steps>
  <item>
    <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Grafický tablet</gui>.</p>
  </item>
  <item>
    <p>Kliknutím na položku <gui>Grafický tablet</gui> otevřete příslušný panel.</p>
  </item>
  <item>
    <p>Klikněte na tlačítko <gui>Tablet</gui> v záhlavní liště.</p>
    <note style="tip"><p>Pokud není zjištěn žádný grafický tablet, jste vyzváni <gui>Připojte a/nebo zapněte svůj grafický tablet</gui>. Jestli chcete připojit bezdrátový tablet, klikněte na odkaz <gui>Nastavení Bluetooth</gui>.</p></note>
  </item>
  <item><p>Klikněte na <gui>Mapování na obrazovku…</gui></p></item>
  <item><p>Zaškrtněte <gui>Mapovat na jedinou obrazovku</gui>.</p></item>
  <item><p>Vedle popisku <gui>Výstup</gui> vyberte monitor, který chcete, aby přijímal vstup z grafického tabletu.</p>
     <note style="tip"><p>Vybrat lze jen monitory, které jsou správně nastavené.</p></note>
  </item>
  <item>
    <p>Přepnutím vypínač <gui>Zachovat poměr (omezit oblast)</gui> do polohy zapnuto přizpůsobíte kreslicí oblast tabletu proporcím monitoru. Toto nastavení, nazývané také <em>vynucené proporce</em>, kreslicí oblast <em>omezí</em> (ořízne), takže například tablet 4∶3 bude více odpovídat širokoúhlé obrazovce 16∶9.</p>
  </item>
  <item><p>Klikněte na <gui>Zavřít</gui>.</p></item>
</steps>

</page>
