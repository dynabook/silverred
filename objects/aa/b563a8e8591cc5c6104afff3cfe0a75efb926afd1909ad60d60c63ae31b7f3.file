<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting" xml:lang="fr">

  <info>
    <link type="guide" xref="net-wireless" group="first"/>
    <link type="guide" xref="hardware#problems" group="first"/>
    <link type="next" xref="net-wireless-troubleshooting-initial-check"/>

    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Contributeurs au wiki de la documentation Ubuntu</name>
    </credit>
    <credit type="author">
      <name>Le projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Identify and fix problems with wireless connections.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Dépannage du réseau sans fil</title>

  <p>This is a step-by-step troubleshooting guide to help you identify and fix
  wireless problems. If you cannot connect to a wireless network for some
  reason, try following the instructions here.</p>

  <p>Nous allons passer par les étapes suivantes pour connecter votre ordinateur à internet :</p>

  <list style="numbered compact">
    <item>
      <p>faire un premier contrôle,</p>
    </item>
    <item>
      <p>collecter les informations sur votre matériel,</p>
    </item>
    <item>
      <p>vérifier votre matériel,</p>
    </item>
    <item>
      <p>tenter de créer une connexion vers votre routeur sans fil,</p>
    </item>
    <item>
      <p>vérifier votre modem et votre routeur.</p>
    </item>
  </list>

  <p>Pour commencer, cliquez sur le lien <em>Suivant</em> en haut à droite de la page. Ce lien, et les autres comme lui sur les pages suivantes vont vous conduire à travers chaque étape de ce guide.</p>

  <note>
    <title>Utilisation de la ligne de commande</title>
    <p>Some of the instructions in this guide ask you to type commands into the
    <em>command line</em> (Terminal). You can find the <app>Terminal</app> application in
    the <gui>Activities</gui> overview.</p>
    <p>If you are not familiar with using a command line, don’t worry — this
    guide will direct you at each step. All you need to remember is that
    commands are case-sensitive (so you must type them <em>exactly</em> as they
    appear here), and to press <key>Enter</key> after typing each command to
    run it.</p>
  </note>

</page>
