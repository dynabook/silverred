<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-slowkeys" xml:lang="hu">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Késleltetés a billentyűlenyomás és a billentyű képernyőn történő megjelenése között.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Lassú billentyűk bekapcsolása</title>

  <p>A <em>lassú billentyűk</em> bekapcsolásával késleltetés iktatható a billentyűk lenyomása és a betű képernyőn való megjelenése közé. Ez azt jelenti, hogy minden billentyűt hosszabb ideig kell lenyomva tartania, mielőtt az megjelenne. A lassú billentyűket akkor használja, ha gépeléskor véletlenül egyszerre több billentyűt is le szokott nyomni, vagy problémát okoz a megfelelő billentyű megnyomása elsőre a billentyűzeten.</p>

  <steps>
    <item>
      <p>Nyissa meg a <gui xref="shell-introduction#activities">Tevékenységek</gui> áttekintést, és kezdje el begépelni a <gui>Beállítások</gui> szót</p>
    </item>
    <item>
      <p>Válassza a <gui>Beállítások</gui> lehetőséget.</p>
    </item>
    <item>
      <p>Kattintson az <gui>Akadálymentesítés</gui> elemre az oldalsávon a panel megnyitásához.</p>
    </item>
    <item>
      <p>Nyomja meg a <gui>Gépelési segéd</gui> gombot a <gui>Gépelés</gui> szakaszban.</p>
    </item>
    <item>
      <p>Switch the <gui>Slow Keys</gui> switch to on.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Lassú billentyűk gyors be- és kikapcsolása</title>
    <p>Az <gui>Engedélyezés billentyűzettel</gui> alatt jelölje be az <gui>Akadálymentesítési szolgáltatások bekapcsolása a billentyűzetről</gui> négyzetet a lassú billentyűk be- és kikapcsolásához a billentyűzetről. Ha ez a négyzet ki van választva, akkor a <key>Shift</key> billentyűt nyolc másodpercig lenyomva tartva engedélyezheti vagy letilthatja a lassú billentyűket.</p>
    <p>A lassú billentyűket be- és kikapcsolhatja a felső sávon az <link xref="a11y-icon">akadálymentesítés ikonra</link> kattintással, majd a <gui>Lassú billentyűk</gui> menüpont kiválasztásával is. Az akadálymentesítési ikon akkor látható, ha legalább egy funkciót bekapcsolt az <gui>Akadálymentesítés</gui> panelen.</p>
  </note>

  <p>Az <gui>Elfogadás késleltetése</gui> csúszka segítségével beállíthatja, hogy meddig kell lenyomva tartani egy billentyűt a leütés érzékeléséhez.</p>

  <p>Beállíthatja, hogy a számítógép hangjelzést adjon egy billentyű lenyomásakor, billentyűleütés elfogadásakor, vagy egy túl rövid billentyűleütés elutasításakor.</p>

</page>
