<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-autobrightness" xml:lang="fi">

  <info>
    <link type="guide" xref="power#saving"/>
    <link type="seealso" xref="display-brightness"/>

    <revision pkgversion="3.20" date="2016-06-15" status="candidate"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2016</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Hallitse näytön kirkkautta automaattisesti säästääksesi akun käyttöä.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

  <title>Ota käyttöön automaattinen kirkkaus</title>

  <p>If your computer has an integrated light sensor, it can be used to
  automatically control screen brightness. This ensures that the screen is
  always easy to see in different ambient light conditions, and helps to reduce
  battery consumption.</p>

  <steps>

    <item>
      <p>Avaa <gui xref="shell-introduction#activities">Toiminnot</gui>-yleisnäkymä ja ala kirjoittamaan <gui>Virransäästö</gui>.</p>
    </item>
    <item>
      <p>Napsauta <gui>Virranhallinta</gui> avataksesi paneelin.</p>
    </item>
    <item>
      <p>In the <gui>Power Saving</gui> section, ensure that the
      <gui>Automatic brightness</gui> switch is set to on.</p>
    </item>

  </steps>

  <p>To disable automatic screen brightness, switch it to off.</p>

</page>
