<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-templates" xml:lang="ja">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Anita Reitere</name>
      <email>nitalynx@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>カスタマイズしたファイルテンプレートから新しいドキュメントをすばやく作成します。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>松澤 二郎</mal:name>
      <mal:email>jmatsuzawa@gnome.org</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>赤星 柔充</mal:name>
      <mal:email>yasumichi@vinelinux.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kentaro KAZUHAMA</mal:name>
      <mal:email>kazken3@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shushi Kurose</mal:name>
      <mal:email>md81bird@hitaki.net</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Noriko Mizumoto</mal:name>
      <mal:email>noriko@fedoraproject.org</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>坂本 貴史</mal:name>
      <mal:email>o-takashi@sakamocchi.jp</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>日本GNOMEユーザー会</mal:name>
      <mal:email>http://www.gnome.gr.jp/</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>よく使うドキュメントのテンプレート</title>

  <p>同じ内容をベースとするドキュメントをよく作成する場合、ファイルテンプレートを利用すると良いでしょう。再利用したい書式や内容をもつどんな種類のドキュメントもファイルテンプレートにすることができます。たとえば、レターヘッドを記述したテンプレートを作成することもできます。</p>

  <steps>
    <title>新しいテンプレートを作成する</title>
    <item>
      <p>テンプレートとして使用したいドキュメントを作成します。たとえば、ワープロソフトでレターヘッドを作成します。</p>
    </item>
    <item>
      <p>Save the file with the template content in the <file>Templates</file>
      folder in your <file>Home</file> folder. If the <file>Templates</file>
      folder does not exist, you will need to create it first.</p>
    </item>
  </steps>

  <steps>
    <title>テンプレートを使ってドキュメントを作成する</title>
    <item>
      <p>新しいドキュメントを配置するフォルダーを開きます。</p>
    </item>
    <item>
      <p>Right-click anywhere in the empty space in the folder, then choose
      <gui style="menuitem">New Document</gui>. The names of available
      templates will be listed in the submenu.</p>
    </item>
    <item>
      <p>使用するテンプレートを一覧から選択します。</p>
    </item>
    <item>
      <p>Double-click the file to open it and start editing. You may wish to
      <link xref="files-rename">rename the file</link> when you are
      finished.</p>
    </item>
  </steps>

</page>
