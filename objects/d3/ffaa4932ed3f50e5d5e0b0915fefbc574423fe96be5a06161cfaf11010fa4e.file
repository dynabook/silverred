<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="net-what-is-ip-address" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Džim Kembel</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>IP adresa je kao telefonski broj za vaš računar.</desc>
  </info>

  <title>Šta je to IP adresa?</title>

  <p>„IP adresa“ znači <em>Adresa internet protokola</em>, i svaki uređaj koji je povezan na mrežu (kao što je internet) ima jednu.</p>

  <p>IP adresa je slična vašem telefonskom broju. Vaš telefonski broj je jedinstveni skup brojeva koji određuje vaš telefon tako da vas drugi ljudi mogu zvati. Slično ovome, IP adresa je jedinstveni skup brojeva koji određuju vaš računar tako da može da šalje i da prima podatke sa drugih računara.</p>

  <p>Trenutno, većina IP adresa se sastoji od četiri skupa brojeva, razdvojenih tačkama. <code>192.168.1.42</code> je jedan primer IP adrese.</p>

  <note style="tip">
    <p>IP adresa može biti ili <em>dinamička</em> ili <em>statička</em>. Dinamičke IP adrese se privremeno dodeljuju svaki put kada se vaš računar poveže na mrežu. Statičke IP adrese su stalne, i ne menjaju se. Dinamičke IP adrese su opštije od statičkih adresa — statičke adrese se uglavnom koriste samo tamo gde postoji naročita potreba za njima, kao što je administracija servera.</p>
  </note>

</page>
