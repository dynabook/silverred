<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-slowkeys" xml:lang="cs">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak ponechat prodlevu mezi zmáčknutím klávesy a napsáním písmena na obrazovku.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Zapnutí pomalých kláves</title>

  <p><em>Pomalé klávesy</em> zapněte, když chcete aby byla prodleva mezi tím, co zmáčknete klávesu a tím, co se písmeno objeví na obrazovce. Znamená to, že musíte každou klávesu chvíli podržet zmáčknutou, než se objeví to, co chcete napsat. Tuto možnost používejte, když při psaní nechtěně mačkáte několik kláves naráz, nebo když je pro vás obtížné zmáčknout na klávesnici správnou klávesu napoprvé.</p>

  <steps>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Nastavení</gui>.</p>
    </item>
    <item>
      <p>Klikněte na <gui>Nastavení</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Univerzální přístup</gui> v postranním panelu otevřete příslušný panel.</p>
    </item>
    <item>
      <p>V části <gui>Psaní</gui> zmáčkněte <gui>Pomáhat s psaním</gui>.</p>
    </item>
    <item>
      <p>Přepněte vypínač <gui>Pomalé klávesy</gui> do polohy zapnuto.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Rychlé zapnutí nebo vypnutí pomalých kláves</title>
    <p>Abyste mohli funkci pomalých kláves zapínat a vypínat pomocí klávesnice, v části <gui>Zapnout z klávesnice</gui> vyberte <gui>Zapínat funkce zpřístupnění pomocí klávesnice</gui>. Když je tato volba zapnutá, můžete zmáčknout na osm vteřin klávesu <gui>Shift</gui> a tím pomalé klávesy zapnout nebo vypnout.</p>
    <p>Pomalé klávesy můžete zapnout a vypnout také kliknutím na <link xref="a11y-icon">ikonu zpřístupnění</link> na horní liště a následujícím výběrem položky <gui>Pomalé klávesy</gui>. Ikona zpřístupnění je viditelná, když je zapnuté aspoň jedno nastavení v panelu <gui>Univerzální přístup</gui>.</p>
  </note>

  <p>Pomocí táhla <gui>Zpoždění přijetí</gui> můžete řídit, jak dlouho je třeba držet klávesu zmáčknutou, aby byla zaregistrována.</p>

  <p>Můžete nechat počítač vydat zvuk pokaždé, když zmáčknete klávesu, když je klávesa přijata, nebo když je klávesa odmítnuta, protože jste ji nedrželi dostatečně dlouho.</p>

</page>
