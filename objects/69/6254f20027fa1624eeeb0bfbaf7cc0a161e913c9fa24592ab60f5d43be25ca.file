<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-import-windows" xml:lang="it">

  <info>
    <link type="guide" xref="color#problems"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <link type="seealso" xref="color-gettingprofiles"/>
    <desc>Come importare un profilo ICC esistente usando un sistema Windows.</desc>
    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Milo Casagrande</mal:name>
      <mal:email>milo@milo.name</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

  <title>Installare un profilo ICC su Microsoft Windows</title>
  <p>Il metodo per assegnare un profilo a un dispositivo e per utilizzare le curve di calibrazione integrate è diverso per ogni versione di Microsoft Windows.</p>
  <section id="xp">
    <title>Windows XP</title>
    <p>Fare clic destro sul profilo in Windows Explorer e quindi su <gui>Installa profilo</gui>: ciò copierà automaticamente il profilo nella directory corretta.</p>
    <p>Quindi aprire <guiseq><gui>Centro di controllo</gui><gui>Colore</gui></guiseq> e aggiungere il profilo al dispositivo.</p>
    <note style="warning">
      <p>Se viene sostituito un profilo esistente in Windows XP, la procedura indicata sopra non funziona. I profili devono essere copiati manualmente in <file>C:\Windows\system32\spool\drivers\color</file> per rimpiazzare il profilo originale.</p>
    </note>
    <p>Per Windows XP è necessario eseguire un programma all'avvio per copiare le curve di calibrazione nella scheda video. Per fare ciò è possibile utilizzare <app>Adobe Gamma</app>, <app>LUT Loader</app> o l'applet gratuita <app href="https://www.microsoft.com/download/en/details.aspx?displaylang=en&amp;id=12714">Microsoft Color Control Panel Applet</app>. Utilizzando quest'ultima, viene aggiunto un nuovo elemento <gui>Colore</gui> nel centro di controllo che consente di impostare facilmente le curve di calibrazione dal profilo predefinito a ogni avvio.</p>
  </section>

  <section id="vista">
    <title>Windows Vista</title>
    <p>Microsoft Windows Vista purtroppo rimuove erroneamente le curve di calibrazione dallo LUT una volta effettuato l'accesso, dopo la sospensione e quando appare la schermata UAC. Ciò implica che è necessario ricaricare manualmente il profilo delle curve di calibrazione ICC. Se vengono usati profili con curve di calibrazione integrate è necessario controllare accuratamente che lo stato di calibrazione non sia stato azzerato.</p>
  </section>

  <section id="7">
    <title>Windows 7</title>
    <p>Windows 7 supporta uno schema simile a quello di Linux in cui i profili possono essere installati a livello di sistema o per utente. Tuttavia, i profili sono archiviati nello stesso posto. Fare clic destro sul profilo in Windows Explorer e quindi fare clic su <gui>Installa profilo</gui> o copiare il profilo .icc in <file>C:\Windows\system32\spool\drivers\color</file>.</p>
    <p>Aprire <guiseq><gui>Centro di controllo</gui><gui>Gestione colore</gui></guiseq> e aggiungere il profilo al sistema premendo il pulsante <gui>Aggiungi</gui>. Selezionare la scheda <gui>Avanzate</gui> e cercare <gui>Calibrazione display</gui>: il caricamento delle curve di calibrazione è controllato tramite la casella di controllo <gui>Usa calibrazione display Windows</gui>, ma non è abilitata. Per abilitarla, fare clic su <gui>Modifica predefiniti di sistema</gui>, tornare alla scheda <gui>Avanzate</gui> e fare clic sulla casella.</p>
    <p>Chiudere la finestra e fare clic su <gui>Ricaricare calibrazioni correnti</gui> per impostare la gamma. Le curve di calibrazione sono ora impostate per ogni avvio.</p>
  </section>

</page>
