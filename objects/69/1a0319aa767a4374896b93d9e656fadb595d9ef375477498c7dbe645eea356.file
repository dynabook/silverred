<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-lost" xml:lang="ca">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Seguiu aquests consells si no podeu trobar un fitxer que hàgiu creat o baixat.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

<title>Trobar un fitxer perdut</title>

<p>Si heu creat o baixat un fitxer, però ara no el podeu trobar, seguiu aquests consells.</p>

<list>
  <item><p>Si no recordeu on vau desar el fitxer, però teniu una idea de com el vau nomenar, podeu fer-ho <link xref="files-search">cercar el fitxer pel nom</link>.</p></item>

  <item><p>Si acaba de descarregar el fitxer, el vostre navegador web podria haver-lo guardat automàticament en una carpeta comuna. Mireu a les carpetes <file>Escriptori</file> i <file>Baixades</file> al vostre directori 'Inici'.</p></item>

  <item><p>Podeu haver esborrat el fitxer accidentalment. Quan suprimiu un fitxer, es mou a la paperera, on es manté fins que buideu la paperera manualment. Consultar <link xref="files-recover"/> per saber-ne més sobre com recuperar un fitxer eliminat.</p></item>

  <item><p>És possible que hàgiu canviat el nom d'un fitxer de manera que hàgiu ocultat el fitxer. Fitxers que comencen per a <file>.</file> o acabin amb <file>~</file> estan ocults al gestor de fitxers. Feu clic al botó Opcions de visualització a la barra d'eines <app>Fitxers</app> i activeu <gui>Mostra els fitxers ocults</gui> per mostrar-los. Consultar <link xref="files-hidden"/> per saber-ne més.</p></item>
</list>

</page>
