<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-othercountry" xml:lang="id">

  <info>
    <link type="guide" xref="power#problems"/>
    <desc>Komputer Anda akan bekerja, tapi Anda mungkin perlu kabel listrik lain atau adaptor perjalanan.</desc>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Proyek Dokumentasi GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

<title>Akankan komputer saya bekerja dengan listrik di negara lain?</title>

<p>Negara lain memakai listrik dengan tegangan (biasanya 110V atau 220-240V) dan frekuensi berbeda (biasanya 50 Hz atau 60 Hz). Komputer Anda mestinya bekerja dengan listrik di negara lain selama Anda memiliki adaptor daya yang sesuai. Anda mungkin juga perlu mengubah setelan saklar.</p>

<p>If you have a laptop, all you should need to do is get the right plug for
your power adapter. Some laptops come packaged with more than one plug for
their adapter, so you may already have the right one. If not, plugging your
existing one into a standard travel adapter will suffice.</p>

<p>If you have a desktop computer, you can also get a cable with a different
plug, or use a travel adapter. In this case, however, you may need to change
the voltage switch on the computer’s power supply, if there is one. Many
computers do not have a switch like this, and will happily work with either
voltage. Look at the back of the computer and find the socket that the power
cable plugs into. Somewhere nearby, there may be a small switch marked “110V”
or “230V” (for example). Switch it if you need to.</p>

<note style="warning">
  <p>Be careful when changing power cables or using travel adapters. Switch
  everything off first if you can.</p>
</note>

</page>
