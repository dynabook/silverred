<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-removedrive" xml:lang="sl">

  <info>
    <link type="guide" xref="files#removable"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <revision pkgversion="3.6.0" version="0.2" date="2012-10-08" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Izvrzite ali odklopite USB ključek, CD, DVD ali drugo napravo.</desc>
  </info>

  <title>Varna odstranitev zunanjega pogona</title>

  <p>Ko uporabljate naprave zunanje shrambe kot so USB ključki, jih pred odklopom varno odstranite. V primeru da napravo izklopite brez varne odstranitve, tvegate odklop naprave medtem ko jo program še vedno uporablja. To lahko povzroči izgubo ali poškodbo nekaterih datotek. Iste korake lahko uporabite za izvrzitev optičnega diska kot je CD ali DVD.</p>

  <steps>
    <title>Za izvrzitev odstranljive naprave:</title>
    <item>
      <p>From the <gui xref="shell-introduction#activities">Activities</gui> overview,
      open <app>Files</app>.</p>
    </item>
    <item>
      <p>Najdite napravo v stranski vrstici. Poleg imena naj bi imela majhno ikono imenovano izvrzi. Za varno odstranjevanje naprave kliknite nanjo.</p>
      <p>Namesto tega lahko desno kliknite na napravo v stranski vrstici in izberite <gui>Izvrzi</gui>.</p>
    </item>
  </steps>

  <section id="remove-busy-device">
    <title>Varna odstranitev naprave, ki je v uporabi</title>

  <p>If any of the files on the device are open and in use by an application,
  you will not be able to safely remove the device. You will be prompted with a
  window telling you <gui>Volume is busy</gui>. To safely remove the device:</p>

  <steps>
    <item><p>Kliknite <gui>Prekliči</gui>.</p></item>
    <item><p>Zaprite vse datoteke na napravi.</p></item>
    <item><p>Kliknite ikono za odklop, da varno odstranite oz. odklopite napravo.</p></item>
    <item><p>Namesto tega lahko desno kliknite na napravo v stranski vrstici in izberite <gui>Izvrzi</gui>.</p></item>
  </steps>

  <note style="warning"><p>Izberete lahko tudi <gui>Vseeno odklopi</gui> za odstranitev naprave brez zapiranja datotek. To lahko povzroči napake v programih, v katerih so te datoteke odprte.</p></note>

  </section>

</page>
