<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="keyboard-layouts" xml:lang="fr">

  <info>
    <link type="guide" xref="prefs-language"/>
    <link type="guide" xref="keyboard" group="i18n"/>
    <link type="guide" xref="keyboard-shortcuts-set"/>

    <revision pkgversion="3.8" version="0.3" date="2013-04-30" status="review"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="author">
       <name>Julita Inca</name>
       <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Juanjo Marín</name>
      <email>juanj.marin@juntadeandalucia.es</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ajouter des agencements au clavier et basculer entre eux.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Utilisation d'agencements de clavier alternatifs</title>

  <p>Il existe des centaines d'agencements de claviers différents pour les différentes langues. Même pour une seule langue, il existe souvent de nombreux agencements de clavier, comme l'agencement Dvorak pour l'anglais. Vous pouvez modifier votre clavier afin qu'il se comporte comme un autre clavier, sans tenir compte des lettres et des symboles imprimés sur les touches. C'est utile si vous jonglez souvent entre plusieurs langues.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Region &amp; Language</gui> in the sidebar to open the
      panel.</p>
    </item>
    <item>
      <p>Cliquez sur le bouton<gui>+</gui> dans la partie <gui>Sources d'entrées</gui>, sélectionnez la langue associée à l'agencement, puis sélectionnez un agencement et cliquez sur <gui>Ajouter</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <p>If there are multiple user accounts on your system, there is a separate
    instance of the <gui>Region &amp; Language</gui> panel for the login screen.
    Click the <gui>Login Screen</gui> button at the top right to toggle between
    the two instances.</p>

    <p>Some rarely used keyboard layout variants are not available by default
    when you click the <gui>+</gui> button. To make also those input sources
    available you can open a terminal window by pressing
    <keyseq><key>Ctrl</key><key>Alt</key><key>T</key></keyseq>
    and run this command:</p>
    <p><cmd its:translate="no">gsettings set org.gnome.desktop.input-sources
    show-all-sources true</cmd></p>
  </note>

  <note style="sidebar">
    <p>Vous pouvez afficher l'aperçu d'un agencement en sélectionnant celui-ci dans la liste des <gui>Sources d'entrées</gui> et en cliquant sur <gui><media its:translate="no" type="image" src="figures/input-keyboard-symbolic.png" width="16" height="16"><span its:translate="yes">aperçu</span></media></gui></p>
  </note>

  <p>Certaines langues offrent des options de configurations supplémentaires. Vous pouvez les identifier grâce à l'icône <gui><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16"><span its:translate="yes">aperçu</span></media></gui> qui les accompagne. Pour accéder à ces paramètres supplémentaires, sélectionnez la langue dans la liste des <gui>Source d'entrées</gui> et appuyez sur le nouveau bouton <gui style="button"><media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg" width="16" height="16"><span its:translate="yes">préférences</span></media></gui>.</p>

  <p>When you use multiple layouts, you can choose to have all windows use the
  same layout or to set a different layout for each window. Using a different
  layout for each window is useful, for example, if you’re writing an article
  in another language in a word processor window. Your keyboard selection will
  be remembered for each window as you switch between windows. Press the
  <gui style="button">Options</gui> button to select how you want to manage
  multiple layouts.</p>

  <p>La barre du haut affiche un court indicateur de l'agencement actuel, par exemple <gui>fr</gui> pour un clavier français standard. Cliquez sur cet indicateur et choisissez l'agencement que vous voulez utiliser dans le menu déroulant. Si la langue choisie possède des paramètres supplémentaires, ils s'affichent sous la liste des agencements disponibles. Cela vous donne une vue générale rapide de vos paramètres. Vous pouvez aussi ouvrir l'image de l'agencement clavier actuel et vous y référer en cas de besoin.</p>

  <p>La façon la plus rapide de passer à un autre agencement du clavier est d'utiliser les <gui>raccourcis clavier</gui> de la <gui>Source d'entrées</gui>. Ces raccourcis ouvrent le sélecteur <gui>Source d'entrées</gui> où vous pouvez vous déplacer en avant et en arrière. Par défaut, il est possible de passer à la source d'entrées suivante avec <keyseq><key xref="keyboard-key-super">Logo</key><key>Espace</key></keyseq> et à la précédente par <keyseq><key>Maj</key><key>Logo</key><key>Espace</key></keyseq>. Vous pouvez modifier ces raccourcis dans les paramètres du <gui>Clavier</gui>.</p>

  <p><media type="image" src="figures/input-methods-switcher.png"/></p>

</page>
