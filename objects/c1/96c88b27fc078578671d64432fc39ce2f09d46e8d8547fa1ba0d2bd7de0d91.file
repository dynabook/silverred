<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-mode" xml:lang="as">

  <info>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.28" date="2018-07-22" status="review"/>
    <revision pkgversion="3.33" date="2019-07-21" status="candidate"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>মাইকেল হিল</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>টেবলেটক টেবলেট অৱস্থা আৰু মাউছ অৱস্থাৰ মাজত চুইচ কৰক।</desc>
  </info>

  <title>Set the Wacom tablet’s tracking mode</title>

<p><gui>অনুকৰণ অৱস্থা</gui> এ পইন্টাৰ কেনেকৈ পৰ্দালৈ মেপ কৰা হয় নিৰ্ধাৰণ কৰে।</p>

<steps>
  <item>
    <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
    overview and start typing <gui>Wacom Tablet</gui>.</p>
  </item>
  <item>
    <p>Click on <gui>Wacom Tablet</gui> to open the panel.</p>
  </item>
  <item>
    <p>Click the <gui>Tablet</gui> button in the header bar.</p>
    <note style="tip"><p>If no tablet is detected, you’ll be asked to
    <gui>Please plug in or turn on your Wacom tablet</gui>. Click the
    <gui>Bluetooth Settings</gui> link to connect a wireless tablet.</p></note>
  </item>
  <item><p><gui>অনুকৰণ অৱস্থা</gui> ৰ কাষত, <gui>টেবলেট (প্ৰকৃত)</gui> অথবা <gui>টাচপেড (প্ৰাসংগিক)</gui> বাছক।</p></item>
</steps>

<note style="info"><p><em>প্ৰকৃত</em> অৱস্থাত, টেবলেটৰ প্ৰতিটো বিন্দু পৰ্দাৰ এটা বিন্দুলৈ চিহ্ন কৰে। পৰ্দাৰ ওপৰ বাঁও চুক, উদাহৰণস্বৰূপ, সদায় টেবলেটৰ একেটা বিন্দুলৈ প্ৰসংগ কৰে।</p>
 <p>In <em>relative</em> mode, if you lift the pointer off the tablet and put it
 down in a different position, the cursor on the screen doesn’t move. This is
    the way a mouse operates.</p>
  </note>

</page>
