<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-howtoimport" xml:lang="pl">

  <info>
    <link type="guide" xref="color#profiles"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <link type="seealso" xref="color-assignprofiles"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="review"/>
    <revision pkgversion="3.28" date="2018-04-05" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Profile kolorów można importować otwierając je.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  </info>

  <title>Jak importować profile kolorów?</title>

  <p>Można zaimportować profil kolorów podwójnie klikając plik <file>.ICC</file> lub <file>.ICM</file> w menedżerze plików.</p>

  <p>Można także zarządzać profilami kolorów w panelu <gui>Kolor</gui>.</p>

  <steps>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Ustawienia</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Ustawienia</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Urządzenia</gui> na panelu bocznym.</p>
    </item>
    <item>
      <p>Kliknij <gui>Kolor</gui> na panelu bocznym, aby otworzyć panel.</p>
    </item>
    <item>
      <p>Wybierz urządzenie.</p>
    </item>
    <item>
      <p>Kliknij przycisk <gui>Dodaj profil</gui>, aby wybrać istniejący profil lub zaimportować nowy profil.</p>
    </item>
    <item>
      <p>Kliknij przycisk <gui>Dodaj</gui>, aby potwierdzić wybór.</p>
    </item>
  </steps>

  <p>Producent ekranu może dostarczać profil, którego można użyć. Takie profile są zwykle tworzone dla typowego ekranu, więc mogą nie być idealne dla używanego. Aby uzyskać najlepsze wyniki kalibracji, należy <link xref="color-calibrate-screen">utworzyć własny profil</link> za pomocą kolorymetra lub spektrometra.</p>

</page>
