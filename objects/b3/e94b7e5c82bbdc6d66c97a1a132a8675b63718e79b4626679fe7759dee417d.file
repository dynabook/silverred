<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-calibrate-screen" xml:lang="sv">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-calibrate-printer"/>
    <link type="seealso" xref="color-calibrate-scanner"/>
    <link type="seealso" xref="color-calibrate-camera"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-05" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Att kalibrera din skärm är viktigt för att visa korrekta färger.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Hur kalibrerar jag min skärm?</title>

  <p>Du kan kalibrera din skärm så att den visar mer korrekta färger. Detta är speciellt användbart om du håller på med digitalfoto, design eller konst.</p>

  <p>Du kommer att behöva antingen en färgkalibrator eller spektrofotometer för att göra detta. Båda enheterna används för att profilera skärmar men de fungerar på något olika sätt.</p>

  <steps>
    <item>
      <p>Säkerställa att din kalibreringsenhet är ansluten till din dator.</p>
    </item>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Inställningar</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Inställningar</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Enheter</gui> i sidopanelen.</p>
    </item>
    <item>
      <p>Klicka på <gui>Färg</gui> i sidopanelen för att öppna panelen.</p>
    </item>
    <item>
      <p>Välj din skärm.</p>
    </item>
    <item>
      <p>Tryck på <gui style="button">Kalibrera…</gui> för att påbörja kalibreringen.</p>
    </item>
  </steps>

  <p>Skärmar ändrar sig hela tiden: bakgrundsbelysningen i en TFT-skärm kommer att halvera sin ljusstyrka ungefär var 18:e månad och bli gulare när den blir äldre. Detta innebär att du bör omkalibrera din skärm när ikonen [!] visas i <gui>Färg</gui>-panelen.</p>

  <p>LED-skärmar ändras också över tiden, men mycket långsammare än TFT:er.</p>

</page>
