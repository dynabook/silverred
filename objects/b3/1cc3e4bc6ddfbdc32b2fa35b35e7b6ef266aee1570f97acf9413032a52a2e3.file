<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-get-online" xml:lang="gl">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>
    <link type="guide" xref="getting-started" group="videos"/>
    <title role="trail" type="link">Conectarse a Internet</title>
    <link type="seealso" xref="net"/>
    <title role="seealso" type="link">Un titorial sobre como conectarse a Internet</title>
    <link type="next" xref="gs-browse-web"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2013-2020.</mal:years>
    </mal:credit>
  </info>

  <title>Conectarse a Internet</title>
  
  <note style="important">
      <p>O estado da súa conexión de rede aparece na parte dereita da barra superior.</p>
  </note>  

  <section id="going-online-wired">

    <title>Conectarse a unha rede con fíos</title>

    <media its:translate="no" type="image" mime="image/svg" src="gs-go-online1.svg" width="100%"/>

    <steps>
      <item><p>A icona de conexión de rede da parte dereita da barra superior mostra que está desconectado.</p>
      <p>Existen varias razóns polas que o estado é desconectado: por exemplo, o cable de rede está desenchufado, configurouse o computador en <em>modo avión</em> ou non existen redes Wifi dispoñíbeis na súa área.</p>
      <p>Se quere usar unha conexión con fíos, simplemente conecte o cable de rede para conectarse a Internet. O computador tentará configurar a conexión á rede automaticamente.</p>
      <p>Mentres o computador configura unha conexión de rede por vostede a icona de conexión de rede mostrará tres puntos.</p></item>
      <item><p>En canto se leve a cabo a configuración, a icona de conexión de rede cambiará ao símbolo de equipo en rede.</p></item>
    </steps>
    
  </section>

  <section id="going-online-wifi">

    <title>Conectarse a unha rede Wifi</title>

    <media its:translate="no" type="image" mime="image/svg" src="gs-go-online2.svg" width="100%"/>
    
    <steps>
      <title>Conectarse a unha rede Wifi (sen fíos):</title>
      <item>
        <p>Prema o <gui xref="shell-introduction#yourname">menú do sistema</gui> na parte dereita da barra superior.</p>
      </item>
      <item>
        <p>Seleccione <gui>Wifi non conectada</gui>. A sección de Wifi do menú expandirase.</p>
      </item>
      <item>
        <p>Prema <gui>Seleccionar rede</gui>.</p>
      </item>
    </steps>

     <note style="important">
       <p>Só pode conectarse a redes Wifi se o seu hardware do seu computador permiten facelo e está no área de cobertura da Wifi.</p>
     </note>

    <media its:translate="no" type="image" mime="image/svg" src="gs-go-online3.svg" width="100%"/>

    <steps style="continues">
    <item><p>Seleccione a rede á que quere conectarse, desde a lista de redes dispoñíbeis, e prema <gui>Conectar</gui> para confirmar.</p>
    <p>Dependendo da configuración da rede podería preguntárselle polas credenciais da rede.</p></item>
    </steps>

  </section>

</page>
