<?xml version="1.0" encoding="UTF-8"?>
<libosinfo version="0.0.1">

  <os id="http://endlessos.com/eos/3.6">
    <short-id>eos3.6</short-id>
    <name>Endless OS 3.6</name>
    <name xml:lang="fr">Endless OS 3.6</name>
    <name xml:lang="pl">Endless OS 3.6</name>
    <name xml:lang="uk">Endless OS 3.6</name>
    <version>3.6</version>
    <vendor>Endless Mobile, Inc</vendor>
    <vendor xml:lang="ca">Endless Mobile, Inc</vendor>
    <vendor xml:lang="fr">Endless Mobile, Inc</vendor>
    <vendor xml:lang="it">Endless Mobile, Inc</vendor>
    <vendor xml:lang="pl">Endless Mobile, Inc</vendor>
    <vendor xml:lang="uk">Endless Mobile, Inc</vendor>
    <family>linux</family>
    <distro>eos</distro>

    <upgrades id="http://endlessos.com/eos/3.5"/>
    <derives-from id="http://endlessos.com/eos/3.5"/>

    <release-date>2019-06-11</release-date>

    <eol-date>2019-10-24</eol-date>


    
    <devices>
      <device id="http://pcisig.com/pci/1b36/0100"/> 
      <device id="http://pcisig.com/pci/8086/2415"/> 
      <device id="http://usb.org/usb/80ee/0021"/> 
      <device id="http://pcisig.com/pci/8086/2668"/> 
      <device id="http://pcisig.com/pci/1af4/1001"/> 
      <device id="http://pcisig.com/pci/1af4/1000"/> 
      <device id="http://pcisig.com/pci/1af4/1003"/> 
      <device id="http://pcisig.com/pci/1af4/1005"/> 
      <device id="http://pcisig.com/pci/1033/0194"/> 
      <device id="http://pcisig.com/pci/1b36/0004"/> 
      <device id="http://pcisig.com/pci/1af4/1041"/> 
      <device id="http://pcisig.com/pci/1af4/1042"/> 
      <device id="http://pcisig.com/pci/1af4/1043"/> 
      <device id="http://pcisig.com/pci/1af4/1044"/> 
      <device id="http://pcisig.com/pci/1af4/1045"/> 
      <device id="http://pcisig.com/pci/1af4/1048"/> 
      <device id="http://pcisig.com/pci/1af4/1049"/> 
      <device id="http://pcisig.com/pci/1af4/1052"/> 
      <device id="http://qemu.org/chipset/x86/q35"/> 
      <device id="http://pcisig.com/pci/8086/10d3"/> 
      <device id="http://pcisig.com/pci/8086/293e"/> 
    </devices>


    <variant id="base">
      <name>Endless OS Basic</name>
      <name xml:lang="fr">Endless OS Basique</name>
      <name xml:lang="it">Endless OS Basic</name>
      <name xml:lang="pl">Endless OS Basic</name>
      <name xml:lang="uk">Endless OS Basic</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="base"/>
      <url>https://images-dl.endlessm.com/release/3.6.4/eos-amd64-amd64/base/eos-eos3.6-amd64-amd64.190925-180324.base.iso</url>
      <iso>
        <volume-id>Endless-OS-3-6-\d+-base$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <variant id="en">
      <name>Endless OS English</name>
      <name xml:lang="fr">Endless OS Anglais</name>
      <name xml:lang="it">Endless OS (inglese)</name>
      <name xml:lang="pl">Endless OS (angielski)</name>
      <name xml:lang="uk">Endless OS English</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="en"/>
      <url>https://images-dl.endlessm.com/release/3.6.4/eos-amd64-amd64/en/eos-eos3.6-amd64-amd64.190925-180325.en.iso</url>
      <iso>
        <volume-id>Endless-OS-3-6-\d+-en$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <variant id="es">
      <name>Endless OS Spanish</name>
      <name xml:lang="fr">Endless OS Espagnol</name>
      <name xml:lang="it">Endless OS (spagnolo)</name>
      <name xml:lang="pl">Endless OS (hiszpański)</name>
      <name xml:lang="uk">Endless OS Spanish</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="es"/>
      <url>https://images-dl.endlessm.com/release/3.6.4/eos-amd64-amd64/es/eos-eos3.6-amd64-amd64.190925-191507.es.iso</url>
      <iso>
        <volume-id>Endless-OS-3-6-\d+-es$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <variant id="fr">
      <name>Endless OS French</name>
      <name xml:lang="fr">Endless OS Français</name>
      <name xml:lang="it">Endless OS (francese)</name>
      <name xml:lang="pl">Endless OS (francuski)</name>
      <name xml:lang="uk">Endless OS French</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="fr"/>
      <url>https://images-dl.endlessm.com/release/3.6.4/eos-amd64-amd64/fr/eos-eos3.6-amd64-amd64.190925-194217.fr.iso</url>
      <iso>
        <volume-id>Endless-OS-3-6-\d+-fr$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <variant id="id">
      <name>Endless OS Indonesian</name>
      <name xml:lang="fr">Endless OS Indonésien</name>
      <name xml:lang="it">Endless OS (indonesiano)</name>
      <name xml:lang="pl">Endless OS (indonezyjski)</name>
      <name xml:lang="uk">Endless OS Indonesian</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="id"/>
      <url>https://images-dl.endlessm.com/release/3.6.4/eos-amd64-amd64/id/eos-eos3.6-amd64-amd64.190925-203428.id.iso</url>
      <iso>
        <volume-id>Endless-OS-3-6-\d+-id$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <variant id="pt_BR">
      <name>Endless OS Portuguese (Brazil)</name>
      <name xml:lang="fr">Endless OS Portugais (Brésil)</name>
      <name xml:lang="it">Endless OS (portoghese, Brasile)</name>
      <name xml:lang="pl">Endless OS (brazylijski portugalski)</name>
      <name xml:lang="uk">Endless OS Portuguese (Бразилія)</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="pt_BR"/>
      <url>https://images-dl.endlessm.com/release/3.6.4/eos-amd64-amd64/pt_BR/eos-eos3.6-amd64-amd64.190925-205818.pt_BR.iso</url>
      <iso>
        <volume-id>Endless-OS-3-6-\d+-pt_BR$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <variant id="th">
      <name>Endless OS Thai</name>
      <name xml:lang="fr">Endless OS Thaï</name>
      <name xml:lang="it">Endless OS (tailandese)</name>
      <name xml:lang="pl">Endless OS (tajski)</name>
      <name xml:lang="uk">Endless OS Thai</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="th"/>
      <url>https://images-dl.endlessm.com/release/3.6.4/eos-amd64-amd64/th/eos-eos3.6-amd64-amd64.190925-210851.th.iso</url>
      <iso>
        <volume-id>Endless-OS-3-6-\d+-th$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <variant id="vi">
      <name>Endless OS Vietnamese</name>
      <name xml:lang="fr">Endless OS Vietnamien</name>
      <name xml:lang="it">Endless OS (vietnamita)</name>
      <name xml:lang="pl">Endless OS (wietnamski)</name>
      <name xml:lang="uk">Endless OS Vietnamese</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="vi"/>
      <url>https://images-dl.endlessm.com/release/3.6.4/eos-amd64-amd64/vi/eos-eos3.6-amd64-amd64.190925-210852.vi.iso</url>
      <iso>
        <volume-id>Endless-OS-3-6-\d+-vi$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>


    <variant id="other">
      <name>Endless OS (other)</name>
      <name xml:lang="fr">Endless OS (autres)</name>
      <name xml:lang="pl">Endless OS (inne)</name>
      <name xml:lang="uk">Endless OS (інша)</name>
    </variant>

    <media live="true" arch="all">
      <variant id="other"/>
      <iso>
        <volume-id>\D+-3-6-\d+</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <resources arch="all">
      <minimum>
        <n-cpus>1</n-cpus>
        <cpu>1000000000</cpu>
        <ram>2147483648</ram>
        <storage>21474836480</storage>
      </minimum>

      <recommended>
        <ram>2147483648</ram>
        <storage>42949672960</storage>
      </recommended>
    </resources>

  </os>
</libosinfo>