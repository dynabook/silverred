<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-right-click" xml:lang="pt">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="clicking"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-21" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="author">
      <name>Phil Bulh</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hilh</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Para realizar um clique direito, carregue e mantenha carregado o botão esquerdo do rato.</desc>
  </info>

  <title>Simular um clique direito do rato</title>

  <p>Pode fazer que, em vez de carregar o botão direito do rato, baste com manter carregado o botão esquerdo do rato durante um tempo para realizar a mesma ação. É útil se tem dificuldades para mover os dedos duma mão de forma individual ou se seu dispositivo apuntador tem um sozinho botão.</p>

  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Atividades</gui> e comece a escrever <gui>Acesso universal</gui>.</p>
    </item>
    <item>
      <p>Carregue em <gui>Acesso universal</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Carregue em <gui>Assistente de clique</gui> na seção <gui>Apontar e carregar</gui>.</p>
    </item>
    <item>
      <p>In the <gui>Click Assist</gui> window, switch the <gui>Simulated
      Secondary Click</gui> switch to on.</p>
    </item>
  </steps>

  <p>Pode mudar a duração da clique do botão esquerdo do rato antes de que se considere uma clique com o botão direito mudando o <gui>Atraso de aceitação</gui>.</p>

  <p>Para carregar com o botão direito do rato utilizando uma clique secundária simulada, mantenha carregado o botão esquerdo do rato onde normalmente carregaria com o botão direito, e depois o solte. O ponteiro rechear-se-á com uma cor azul enquanto mantém carregado o botão esquerdo. Quando esteja azul do tudo, solte o botão para fazer a clique direita.</p>

  <p>Alguns ponteiros especiais, como os ponteiros de redimensionar, com mudam de cor. Segue podendo usar a clique secundária simulada de maneira normal, ainda que não veja o efeito visual do ponteiro.</p>

  <p>Se usa as <link xref="mouse-mousekeys">Teclas do rato</link>, isto também lhe permite simular um clique direito mantendo premida a tecla <key>5</key> de seu teclado numérico.</p>

  <note>
    <p>Na vista de <gui>Atividades</gui> sempre pode realizar uma clique longa para executar uma clique com o botão direito, inclusive se esta caraterística está desativada. A clique longa funciona de forma ligeiramente diferente na vista geral: não tem que libertar o botão para realizar a clique com o botão direito.</p>
  </note>

</page>
