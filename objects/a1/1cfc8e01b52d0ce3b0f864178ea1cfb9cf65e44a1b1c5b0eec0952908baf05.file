<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-willnotturnon" xml:lang="ru">

  <info>
    <link type="guide" xref="power#problems"/>
    <link type="guide" xref="hardware-problems-graphics" group="#last"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Возможные причины — плохой контакт в разъёмах кабелей или аппаратные неполадки.</desc>
    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Компьютер не включается</title>

<p>Возможны различные причины, из-за которых компьютер не включается. В этом разделе приведён краткий обзор некоторых из возможных причин.</p>
	
<section id="nopower">
  <title>Компьютер не подключён к источнику питания, разряжена батарея или плохой контакт</title>
  <p>Проверьте, надёжно ли вставлены в разъёмы кабели питания компьютера и есть ли напряжение в розетках. Убедитесь, что монитор подключен к розетке и включён. Если у вас ноутбук, подключите зарядное устройство (в случае, если аккумулятор разряжен). Можно также проверить, надёжно ли закреплён аккумулятор (проверьте снизу ноутбука), если он съёмный.</p>
</section>

<section id="hardwareproblem">
  <title>Неполадки аппаратного обеспечения</title>
  <p>A component of your computer may be broken or malfunctioning. If this is
  the case, you will need to get your computer repaired. Common faults include
  a broken power supply unit, incorrectly-fitted components (such as the
  memory or RAM) and a faulty motherboard.</p>
</section>

<section id="beeps">
  <title>Компьютер подаёт звуковые сигналы и отключается</title>
  <p>If the computer beeps several times when you turn it on and then turns off
  (or fails to start), it may be indicating that it has detected a problem.
  These beeps are sometimes referred to as <em>beep codes</em>, and the pattern
  of beeps is intended to tell you what the problem with the computer is.
  Different manufacturers use different beep codes, so you will have to consult
  the manual for your computer’s motherboard, or take your computer in for
  repairs.</p>
</section>

<section id="fans">
  <title>Вентиляторы компьютера крутятся, но изображение на экране отсутствует</title>
  <p>В первую очередь проверьте, подсоединён ли и включён ли монитор.</p>
  <p>Проблема может быть в аппаратной неисправности. Например, при нажатии кнопки питания вентиляторы включаются, а другие важные части компьютера — нет. В таком случае нужно отнести компьютер в ремонт.</p>
</section>

</page>
