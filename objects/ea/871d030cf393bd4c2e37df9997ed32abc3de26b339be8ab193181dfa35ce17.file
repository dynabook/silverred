<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-cancel-job" xml:lang="cs">

  <info>
    <link type="guide" xref="printing#problems"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Jana Švárová</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak zrušit čekající tiskovou úlohu a jak ji odstranit z fronty.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Zrušení, pozastavení nebo uvolnění tiskové úlohy</title>

  <p>Čekající tiskovou úlohu můžete zrušit a odstranit ji z fronty v nastavení tiskárny.</p>

  <section id="cancel-print-job">
    <title>Zrušení tiskové úlohy</title>

  <p>Když tisk dokumentu spustíte nechtěně, můžete jej zrušit, abyste neplýtvali papírem a inkoustem nebo tonerem.</p>

  <steps>
    <title>Jak zrušit tiskovou úlohu:</title>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Tiskárny</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Tiskárny</gui> otevřete příslušný panel.</p>
    </item>
    <item>
      <p>V dialogovém okně <gui>Tiskárny</gui> klikněte na pravé straně na tlačítko <gui>Zobrazit úlohy</gui>.</p>
    </item>
    <item>
      <p>Zrušte tiskovou úlohu kliknutím na tlačítko stop.</p>
    </item>
  </steps>

  <p>V případě, že se tím tisková úloha nezruší, jak očekáváte, zkuste podrže zmáčknuté tlačítko pro <em>rušení</em> na své tiskárně.</p>

  <p>Jako poslední záchrana, hlavně když máte velkou tiskovou úlohu se spoustou stránek a nejde zrušit, je sebrat tiskárně papíry ze vstupního zásobníku. Tiskárna zjistí, že nemá papír a tisk zastaví. Pak můžete zkusit tiskovou úlohu znovu zrušit nebo tiskárnu prostě vypnout a znovu zapnout.</p>

  <note style="warning">
    <p>Při odebírání papírů buďte opatrní, abyste tiskárnu nepoškodili. Pokud některý papír nejde volně vytáhnout a museli byste to udělat násilím, tiskárna jej už pravděpodobně uchopila – ponechte jej jak je.</p>
  </note>

  </section>

  <section id="pause-release-print-job">
    <title>Pozastavení a uvolnění tiskové úlohy</title>

  <p>Když chcete tiskovou úlohu pozastavit nebo v ní pokračovat, přejděte do dialogového okna tiskových úloh v nastavení tiskárny a klikněte na příslušné tlačítko.</p>

  <steps>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Tiskárny</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Tiskárny</gui> otevřete příslušný panel.</p>
    </item>
    <item>
      <p>Klikněte na tlačítko <gui>Zobrazit úlohy</gui> na pravé straně v dialogovém okně <gui>Tiskárny</gui> a pak podle potřeby tiskovou úlohu pozastavte nebo nechte pokračovat.</p>
    </item>
  </steps>

  </section>

</page>
