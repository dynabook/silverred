<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-closelid" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspendfail"/>
    <link type="seealso" xref="power-suspend"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.10" date="2013-11-08" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.26" date="2017-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <credit type="author">
      <name>Gnomov projekat dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Prenosni računar se uspavljuje kada spustite poklopac, da bi sačuvao energiju.</desc>
  </info>

  <title>Zašto se moj računar gasi kada spustim poklopac?</title>

  <p>Kada spustite poklopac vašeg prenosnog računara, vaš računar će <link xref="power-suspend"><em>obustaviti</em></link> rad kako bi sačuvao energiju. To znači da računar zapravo nije ugašen — samo se uspavao. Možete da ga povratite podizanjem poklopca. Ako se ne povrati, probajte da kliknete mišem ili da pritisnete neki taster. Ako i ovako ne proradi, pritisnite dugme za napajanje.</p>

  <p>Neki računari ne mogu ispravno da obustave rad, obično zato što njihove komponente nisu u potpunosti podržane operativnim sistemom (na primer, upravljački programi Linuksa nisu potpuni). U tom slučaju, možete uvideti da niste u mogućnosti da povratite vaš računar nakon što ste spustili poklopac. Možete da probate da <link xref="power-suspendfail">ispravite problem sa obustavom</link>, ili možete da sprečite računar da pokuša da obustavi rad kada spustite poklopac.</p>

<section id="nosuspend">
  <title>Ne dozvolite računaru da obustavi rad kada spustite poklopac</title>

  <note style="important">
    <p>Ova uputstva će raditi samo ako koristite <app>systemd</app>. Obratite se vašoj distribuciji za više podataka.</p>
  </note>

  <note style="important">
    <p>Trebalo bi da ste instalirali program <app>Alat za lickanje</app> na vašem računaru da biste mogli da izmenite ovo podešavanje.</p>
    <if:if xmlns:if="http://projectmallard.org/if/1.0/" test="action:install">
      <p><link style="button" action="install:gnome-tweak-tool">Instalirajte <app>Alat za lickanje</app></link></p>
    </if:if>
  </note>

  <p>Ako ne želite da računar obustavi rad kada zatvorite poklopac, možete da izmenite podešavanje tog ponašanja.</p>

  <note style="warning">
    <p>Budite veoma pažljivi ako menjate ovo podešavanje. Neki prenosni računari mog da se pregreju ako su ostavljeni da rade sa spuštenim poklopcem, pogotovo ako su u zatvorenom prostoru kao što je ranac.</p>
  </note>

  <steps>
    <item>
      <p>Otvorite pregled <gui xref="shell-introduction#activities">Aktivnosti</gui> i počnite da kucate <gui>Alat za lickanje</gui>.</p>
    </item>
    <item>
      <p>Kliknite na <gui>Alat za lickanje</gui> da ga otvorite.</p>
    </item>
    <item>
      <p>Select the <gui>General</gui> tab.</p>
    </item>
    <item>
      <p>Switch the <gui>Suspend when laptop lid is closed</gui> switch to
      off.</p>
    </item>
    <item>
      <p>Zatvorite prozor <gui>Alata za lickanje</gui>.</p>
    </item>
  </steps>

</section>

</page>
