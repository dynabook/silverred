<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-canshareprofiles" xml:lang="ja">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <desc>Sharing color profiles is never a good idea as hardware changes over time.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>松澤 二郎</mal:name>
      <mal:email>jmatsuzawa@gnome.org</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>赤星 柔充</mal:name>
      <mal:email>yasumichi@vinelinux.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kentaro KAZUHAMA</mal:name>
      <mal:email>kazken3@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shushi Kurose</mal:name>
      <mal:email>md81bird@hitaki.net</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Noriko Mizumoto</mal:name>
      <mal:email>noriko@fedoraproject.org</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>坂本 貴史</mal:name>
      <mal:email>o-takashi@sakamocchi.jp</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>日本GNOMEユーザー会</mal:name>
      <mal:email>http://www.gnome.gr.jp/</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Can I share my color profile?</title>

  <p>
    Color profiles that you have created yourself are specific to the
    hardware and lighting conditions that you calibrated for.
    A display that has been powered for a few hundred hours
    is going to have a very different color profile to a similar display
    with the next serial number that has been lit for a thousand hours.
  </p>
  <p>
    This means if you share your color profile with somebody, you might
    be getting them <em>closer</em> to calibration, but it’s misleading
    at best to say that their display is calibrated.
  </p>
  <p>
    Similarly, unless everyone has recommended controlled lighting
    (no sunlight from windows, black walls, daylight bulbs etc.) in a
    room where viewing and editing images takes place, sharing a profile
    that you created in your own specific lighting conditions doesn’t make
    a lot of sense.
  </p>

  <note style="warning">
    <p>
      You should carefully check the redistribution conditions for profiles
      downloaded from vendor websites or that were created on your behalf.
    </p>
  </note>

</page>
