<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="sound-volume" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="media#sound"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Configure o volume sonoro para o computador e controle a sonoridade de cada aplicativo.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

<title>Alterando o volume do som</title>

  <p>Para alterar o volume do som, abra o <gui xref="shell-introduction#systemmenu">menu do sistema</gui> no lado direito na barra superior e mova o controle deslizante de volume para esquerda ou direita. Você pode desligar completamente arrastando-o completamente para a esquerda.</p>

  <p>Alguns teclados têm teclas que lhe permitem controlar o volume. Normalmente elas se parecem com alto-falantes estilizados com ondas saindo deles. Elas costumam ficar próximas às teclas “F” (teclas de função) na parte superior. Em teclados de notebooks, ficam frequentemente nas teclas “F”. Mantenha pressionada a tecla <key>Fn</key> do teclado para usá-las.</p>

  <p>Se você tem alto-falantes externos, você também pode alterar o volume usando o controle de volume dos alto-falantes. Alguns fones de ouvido têm um controle de volume próprio também.</p>

<section id="apps">
 <title>Alterando o volume sonoro para aplicativos individuais</title>

  <p>Você pode alterar o volume de um aplicativo e deixar o volume inalterado para outros. Isso é útil se você estiver ouvindo música e navegando pela web, por exemplo. Você pode desligar o áudio no navegador de forma que os sons de sites não interrompam a música.</p>

  <p>Alguns aplicativos possuem controles de volume nas janelas principais. Se seu aplicativo possui seu controle de volume, use aquele para alterar o volume. Se não:</p>

    <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Som</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Som</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Sob <gui>Níveis de volume</gui>, altere o volume do aplicativo listado lá.</p>

  <note style="tip">
    <p>Somente aplicativos que estejam reproduzindo sons serão listados. Se um aplicativo está reproduzindo sons e não está listado, pode não haver suporte para ele que lhe permita controlar seu volume desta maneira. Nesse caso, você não pode alterar seu volume.</p>
  </note>
    </item>

  </steps>

</section>

</page>
