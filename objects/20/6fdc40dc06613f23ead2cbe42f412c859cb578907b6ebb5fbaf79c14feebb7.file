<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="look-display-fuzzy" xml:lang="cs">

  <info>
    <link type="guide" xref="hardware-problems-graphics"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-10-11" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.28" date="2018-07-28" status="review"/>

    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Možná je nastavené nesprávné rozlišení obrazovky.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Proč vše na mojí obrazovce vypadá rozmazané/kostičkované?</title>

  <p>To se může stát, že rozlišení displeje, které máte nastavené, neodpovídá skutečnému rozlišení vaší obrazovky. Abyste to napravili:</p>

  <steps>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Nastavení</gui>.</p>
    </item>
    <item>
      <p>Klikněte na <gui>Nastavení</gui>.</p>
    </item>
    <item>
      <p>V postranním panelu klikněte na <gui>Zařízení</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Displeje</gui> v postranním panelu otevřete příslušný panel.</p>
    </item>
    <item>
      <p>Vyzkoušejte volby u položky <gui>Rozlišení</gui> a vyberte tu, se kterou vypadá obrazovka nejlépe.</p>
    </item>
  </steps>

<section id="multihead">
  <title>Když je připojeno více displejů</title>

  <p>Pokud máte k počítači připojeny dva displeje (například normální monitor a projektor), mohou mít navzájem různé optimální nebo <link xref="look-resolution#native">přirozené</link> rozlišení.</p>

  <p>Použitím režimu <link xref="display-dual-monitors#modes">Duplikovat</link> můžete zobrazit tu samou věc na dvou obrazovkách. Obě obrazovky budou používat stejné rozlišení, které ale nemusí odpovídat přirozenému rozlišení obou obrazovek, takže tím může utrpět ostrost obrazu.</p>

  <p>Při použití režimu <link xref="display-dual-monitors#modes">Sloučit displeje</link> se dá nastavit rozlišení u každé obrazovky nezávisle, takže u obou můžete mít jejich přirozené rozlišení.</p>

</section>

</page>
