<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="sharing-media" xml:lang="ru">

  <info>
    <link type="guide" xref="sharing"/>
    <link type="guide" xref="prefs-sharing"/>

    <revision pkgversion="3.10" version="0.2" date="2013-11-02" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-13" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <credit type="author">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Share media on your local network using UPnP.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Сделать общими музыку, фото и видео.</title>

  <p>Просматривать, производить поиск в файлах мультимедиа и проигрывать мультимедия со своего компьютера можно с помощью устройств, поддерживающих <sys>UPnP</sys> или <sys>DLNA</sys>, такими, как смартфон, телевизор или игровая консоль. Настройте <gui>Общий доступ к мультимедиа</gui>, чтобы эти устройства имели доступ к вашим папкам, содержащим музыку, фото и видео.</p>

  <note style="info package">
    <p>Чтобы параметр <gui>Общий доступ к мультимедиа</gui> стал видимым, в системе должен быть установлен пакет <app>Rygel</app>.</p>

    <if:choose xmlns:if="http://projectmallard.org/if/1.0/">
      <if:when test="action:install">
        <p><link action="install:rygel" style="button">Установите Rygel</link></p>
      </if:when>
    </if:choose>
  </note>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Sharing</gui>.</p>
    </item>
    <item>
      <p>Нажмите <gui>Общий доступ</gui> чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
      <p>If the <gui>Sharing</gui> switch in the top-right of the window is
      set to off, switch it to on.</p>

      <note style="info"><p>If the text below <gui>Computer Name</gui> allows
      you to edit it, you can <link xref="sharing-displayname">change</link>
      the name your computer displays on the network.</p></note>
    </item>
    <item>
      <p>Выберите <gui>Общий доступ к мультимедиа</gui>.</p>
    </item>
    <item>
      <p>Switch the <gui>Media Sharing</gui> switch to on.</p>
    </item>
    <item>
      <p>By default, <file>Music</file>, <file>Pictures</file> and 
      <file>Videos</file> are shared. To remove one of these, click the
      <gui>×</gui> next to the folder name.</p>
    </item>
    <item>
      <p>To add another folder, click <gui style="button">+</gui> to open the
      <gui>Choose a folder</gui> window. Navigate <em>into</em> the desired
      folder and click <gui style="button">Open</gui>.</p>
    </item>
    <item>
      <p>Click <gui style="button">×</gui>. You will now be able to browse
      or play media in the folders you selected using the external device.</p>
    </item>
  </steps>

  <section id="networks">
  <title>Networks</title>

  <p>The <gui>Networks</gui> section lists the networks to which you are
  currently connected. Use the switch next to each to choose where your media
  can be shared.</p>

  </section>

</page>
