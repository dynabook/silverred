<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="printing-differentsize" xml:lang="nl">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <its:rules xmlns:its="http://www.w3.org/2005/11/its" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <desc>Een document afdrukken op een ander papierformaat of -oriëntatie.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Het papierformaat wijzigen bij afdrukken</title>

  <p>Als u het papierformaat van uw document wilt wijzigen (bijvoorbeeld een PDF in US Letter-formaat afdrukken op een A4tje, dan kunt u het afdrukformaat voor het document wijzigen.</p>

  <steps>
    <item>
      <p>Druk op <keyseq><key>Ctrl</key><key>P</key></keyseq> om het afdrukdialoogvenster te openen.</p>
    </item>
    <item>
      <p>Selecteer het tabblad <gui>Paginainstellingen</gui>.</p>
    </item>
    <item>
      <p>Kies in de kolom <em>Papier</em> het gewenste <em>Papierformaat</em> uit de keuzelijst.</p>
    </item>
    <item>
      <p>Klik op <gui>Afdrukken</gui> om uw document af te drukken.</p>
    </item>
  </steps>

  <p>U kunt ook de keuzelijst <gui>Oriëntatie</gui> gebruiken om een andere oriëntatie te kiezen:</p>

  <list>
    <item><p><gui>Staand</gui></p></item>
    <item><p><gui>Liggend</gui></p></item>
    <item><p><gui>Staand omgekeerd</gui></p></item>
    <item><p><gui>Liggend omgekeerd</gui></p></item>
  </list>

</page>
