<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="dconf-keyfiles" xml:lang="ca">

  <info>
    <link type="guide" xref="setup"/>
    <link type="seealso" xref="dconf"/>
    <link type="seealso" xref="dconf-profiles"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Ryan Lortie</name>
      <email>desrt@desrt.ca</email>
      <years>2012</years>
    </credit>
    <credit type="author">
      <name>Aruna Sankaranarayanan</name>
      <email>aruna.evam@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Fes servir <sys its:translate="no">dconf</sys> <em>fitxers de claus</em> per configurar una configuració específica amb un editor de text.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

  <title>Control de la configuració del sistema amb fitxers de claus</title>

  <p>Els fitxers de dades del sistema, ubicats a <file its:translate="no">/etc/dconf/db</file>, no es poden editar perquè estan escrits amb format GVDB. Per canviar la configuració del sistema amb un editor de text, pots modificar els <em> fitxers de claus</em> que es troben als <em>directoris de fitxers de claus</em>. Cada directori de fitxers de claus correspon a un fitxer de base de dades concret del sistema, i té el mateix nom que el fitxer de base de dades amb una extensió ".d" afegida (per exemple: <file>/etc/dconf/db/local.d</file>). Tots els directoris de fitxers de claus es troben a <file its:translate="no">/etc/dconf/db</file>, i cadascun conté fitxers de claus en un format especial que es pot compilar a dins de la <sys its:translate="no">dconf</sys>base de dades.</p>

  <listing>
    <title>Un arxiu de claus en aquest directori es veuria de la següent manera:</title>
      <code>
# Algunes configuracions per defecte útils per al nostre lloc

[system/proxy/http]
host='172.16.0.1'
enabled=true

[org/gnome/desktop/background]
picture-uri='file:///usr/local/rupert-corp/company-wallpaper.jpeg'
      </code>
  </listing>

  <note style="important">
    <p><cmd>dconf update</cmd> must be run whenever you modify a keyfile.
    When you do this, <sys its:translate="no">dconf</sys> compares the
    timestamp on a system database file with the timestamp on the corresponding
    keyfile directory. If the timestamp on the keyfile directory is more
    recent than the one on the database file,
    <sys its:translate="no">dconf</sys> regenerates the
    <code>system-db</code> file and sends a notification to the system
    <sys>D-Bus</sys>, which in turn notifies all running applications to
    reread their settings.</p>
  </note>

  <p>The group name in the keyfile references a
  <link href="https://developer.gnome.org/GSettings/">GSettings schema ID</link>.
  For example, <code>org/gnome/desktop/background</code> references the
  schema <code>org.gnome.desktop.background</code>, which contains the key
  <code>picture-uri</code>.</p>

  <p>The values under a group are expected in
  <link href="https://developer.gnome.org/glib/stable/gvariant-text.html">serialized
  GVariant form</link>.</p>

</page>
