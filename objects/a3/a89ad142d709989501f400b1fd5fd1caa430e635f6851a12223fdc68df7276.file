<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="tips-specialchars" xml:lang="sv">

  <info>
    <link type="guide" xref="tips"/>
    <link type="seealso" xref="keyboard-layouts"/>

    <revision pkgversion="3.8.2" version="0.3" date="2013-05-18" status="review"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.26" date="2017-11-27" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Skriv tecken som inte finns på ditt tangentbord, inklusive främmande alfabeten, matematiska symboler och dingbats.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Mata in speciella tecken</title>

  <p>Du kan mata in och titta på tusentals tecken från de flesta av världens skriftsystem, till och med de som inte finns på ditt tangentbord. Denna sida listar några olika sätt på vilka du kan mata in specialtecken.</p>

  <links type="section">
    <title>Metoder att mata in tecken</title>
  </links>

  <section id="characters">
    <title>Tecken</title>
    <p>GNOME kommer med ett teckentabellprogram som låter dig söka efter och infoga ovanliga tecken, inklusive emojitecken, genom att bläddra genom teckenkategorier eller leta efter nyckelord.</p>

    <p>Du kan köra <app>Tecken</app> från översiktsvyn Aktiviteter.</p>

  </section>

  <section id="compose">
    <title>Compose-tangent</title>
    <p>En Compose-tangent är en speciell tangent som låter dig trycka ner flera tangenter i rad för att få ett specialtecken. För att till exempel skriva det apostroferade tecknet <em>é</em> kan du trycka på <key>compose</key> och sedan <key>'</key> och sedan <key>e</key>.</p>
    <p>Tangentbord har inte specifika compose-tangenter. Istället kan du definiera en av de existerande tangenterna på ditt tangentbord som en compose-tangent.</p>

    <note style="important">
      <p>Du måste ha <app>Justering</app> installerat på din dator för att ändra denna inställning.</p>
      <if:if xmlns:if="http://projectmallard.org/if/1.0/" test="action:install">
        <p><link style="button" action="install:gnome-tweaks">Installera <app>Justering</app></link></p>
      </if:if>
    </note>

    <steps>
      <title>Definiera en compose-tangent</title>
      <item>
        <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Justering</gui>.</p>
      </item>
      <item>
        <p>Klicka på <gui>Justering</gui> för att öppna programmet.</p>
      </item>
      <item>
        <p>Klicka på <gui>Tangentbord &amp; mus</gui>-fliken.</p>
      </item>
      <item>
        <p>Tryck på <gui>Inaktiverad</gui> intill inställningen <gui>Compose-tangent</gui>.</p>
      </item>
      <item>
        <p>Slå på växeln i dialogen och välj tangentbordsgenvägen du vill använda.</p>
      </item>
      <item>
        <p>Kryssa i kryssrutan för den knapp du vill använda som Compose-tangent.</p>
      </item>
      <item>
        <p>Stäng dialogrutan.</p>
      </item>
      <item>
        <p>Stäng fönstret <gui>Justering</gui>.</p>
      </item>
    </steps>

    <p>Du kan mata in många vanliga tecken genom att använda compose-tangenten, till exempel:</p>

    <list>
      <item><p>Tryck <key>compose</key> sedan <key>'</key> och sedan ett tecken för att placera en akut accent ovanför det tecknet, som till exempel <em>é</em>.</p></item>
      <item><p>Tryck <key>compose</key> sedan <key>`</key> och sedan ett tecken för att placera en grav accent ovanför det tecknet, som till exempel <em>è</em>.</p></item>
      <item><p>Tryck <key>compose</key> sedan <key>"</key> och sedan ett tecken för att placera ett trema ovanför det tecknet, som till exempel <em>ë</em>.</p></item>
      <item><p>Tryck <key>compose</key> sedan <key>-</key> och sedan ett tecken för att placera ett makron ovanför det tecknet, som till exempel <em>ē</em>.</p></item>
    </list>
    <p>För ytterligare compose-tangentsekvenser se <link href="http://en.wikipedia.org/wiki/Compose_key#Common_compose_combinations">compose-tangentsidan på Wikipedia</link>.</p>
  </section>

<section id="ctrlshiftu">
  <title>Kodpunkter</title>

  <p>Du kan mata in vilket Unicode-tecken som helst genom att använda bara ditt tangentbord med tecknets numeriska kodpunkt. Varje tecken identifieras med en fyra-teckens kodpunkt. För att hitta kodpunkten för ett tecken, slå upp det i programmet <app>Tecken</app>. Kodpunkten är de fyra tecknen efter <gui>U+</gui>.</p>

  <p>För att mata in ett tecken via dess kodpunkt, tryck <keyseq><key>Ctrl</key><key>Skift</key><key>U</key></keyseq>, skriv sedan den fyra tecken långa kodpunkten, och tryck <key>Blanksteg</key> eller <key>Retur</key>. Om du ofta använder tecken som du inte kan nå enkelt med andra metoder kan det vara värdefullt att memorera kodpunkterna för de tecknen så att du kan mata in dem snabbt.</p>

</section>

  <section id="layout">
    <title>Tangentbordslayouter</title>
    <p>Du kan få ditt tangentbord att bete sig som ett tangentbord för ett annat språk, oavsett vilka tecken som finns tryckta på tangenterna. Du kan till och med enkelt byta mellan olika tangentbordslayouter via en ikon på systemraden. För att lära dig hur, se <link xref="keyboard-layouts"/>.</p>
  </section>

<section id="im">
  <title>Inmatningsmetoder</title>

  <p>En inmatningsmetod expanderar de föregående metoderna genom att tillåta inmatning av tecken inte enbart med tangentbordet utan också andra inmatningsenheter. Du kan till exempel mata in tecken med en mus via en gestmetod eller mata in japanska tecken via ett latinskt tangentbord.</p>

  <p>För att välja en inmatningsmetod, högerklicka på textkomponenten och i menyn <gui>Inmatningsmetod</gui> välj inmatningsmetoden du vill använda. Det finns ingen standardinmatningsmetod så läs i dokumentationen för inmatningsmetoder om hur de används.</p>

</section>

</page>
