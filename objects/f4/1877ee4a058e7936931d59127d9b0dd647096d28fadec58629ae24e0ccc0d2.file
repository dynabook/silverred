<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="desktop-shield" xml:lang="de">

  <info>
    <link type="guide" xref="appearance"/>
    <revision pkgversion="3.22" date="2017-01-09" status="draft"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2017</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Den Hintergrund des Sperrbildschirms ändern.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2017, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

  <title>Sperrbildschirm ändern</title>

  <p>Der <em>Sperrbildschirm</em> fährt vor die Arbeitsumgebung, sobald diese gesperrt wird. Der Hintergrund des Sperrbildschirms wird durch den GSettings-Schlüssel <sys>org.gnome.desktop.screensaver.picture-uri</sys> gesteuert. Weil <sys>GDM</sys> ein eigenes <sys>dconf</sys>-Profil verwendet, können Sie den Vorgabe-Hintergrund festlegen, indem Sie die Einstellungen dieses Profils anpassen.</p>

<steps>
  <title>Den Schlüssel org.gnome.desktop.screensaver.picture-uri festlegen</title>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-gdm'])"/>
  <item><p>Erstellen Sie die Datenbank <sys>gdm</sys> für systemweite Einstellungen unter <file>/etc/dconf/db/gdm.d/<var>01-screensaver</var></file>:</p>
  <code>[org/gnome/desktop/screensaver]
picture-uri='file://<var>/opt/corp/background.jpg</var>'</code>
  <p>Ersetzen Sie <var>/opt/corp/background.jpg</var> mit dem Pfad zur Bilddatei, die Sie als festgelegten Sperrbildschirmhintergrund verwenden wollen.</p>
  <p>Hierbei werden die Formate PNG, JPG, JPEG und TGA unterstützt. Falls nötig, wird das Bild zur Anpassung an den Bildschirm skaliert.</p>
  </item>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  <item><p>Sie müssen sich abmelden, damit die systemweiten Einstellungen wirksam werden.</p>
  </item>
</steps>

<p>Beim nächsten Sperren des Bildschirms wird der neue Sperrbildschirm im Hintergrund angezeigt. Im Vordergrund werden Zeit, Datum und der aktuelle Wochentag angezeigt.</p>

<section id="troubleshooting-background">
  <title>Was tun, wenn der Hintergrund nicht aktualisiert wird?</title>

  <p>Stellen Sie sicher, dass Sie den Befehl <cmd its:translate="no">dconf update</cmd> ausgeführt haben, um die Systemdatenbanken zu aktualisieren.</p>

  <p>Falls der Hintergrund nicht aktualisiert wird, versuchen Sie, <sys>GDM</sys> neu zu starten.</p>
</section>

</page>
