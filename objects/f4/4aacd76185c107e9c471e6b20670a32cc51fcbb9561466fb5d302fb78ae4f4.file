;; bn-inscript2.mim -- Bengali input method for inscript layout
;; Copyright (c) 2011-2016 Red Hat, Inc. All Rights Reserved.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation; either version 2.1 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public
;; License along with the m17n library; if not, write to the Free
;; Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.
;;
;; Author: Parag Nemade <pnemade@redhat.com>

(input-method bn inscript2)

(description "Bengali input method for enhanced inscript layout.

Reference URL - http://pune.cdac.in/html/gist/down/inscript_d.asp

Key summary:
Use AltGr (Alt_R key) to type the following characters:

Character    Key
----------------------------
ZWJ            AltGr + 1
ZWNJ         AltGr + 2
₹              AltGr + 4

To write \"juktakhor\" i.e. conjunct characters of consonants please use the \"halant\" character on the key 'd' between the two consonant akshar. 

E.g. ক্ষ = k+d+<

Key summary: Runa Bhattacharjee <runab@redhat.com>
")

(title "ক")

(map
 (trans
  ((KP_1) "১")
  ((KP_2) "২")
  ((KP_3) "৩")
  ((KP_4) "৪")
  ((KP_5) "৫")
  ((KP_6) "৬")
  ((KP_7) "৭")
  ((KP_8) "৮")
  ((KP_9) "৯")
  ((KP_0) "০")
  ((KP_Decimal) ".")
  ((KP_Divide) "/")
  ((KP_Multiply) "*")
  ((KP_Add) "+")
  ((KP_Subtract) "-") 
 
  ((G-1) "‍")
  ("!" "অ্যা")
  ((G-!) "৴")
  ("1" "১")
  ((G-@) "৵")
  ("2" "২")
  ((G-2) "‌")
  ("#" "্র")
  ((G-#) "৶")
  ("3" "৩")
  ("$" "র্")
  ((G-$) "৷")
  ("4" "৪")
  ((G-4) "₹")
  ("%" "জ্ঞ")
  ((G-%) "৸")
  ("5" "৫")
  ("^" "ত্র")
  ((G-^) "৹")
  ("6" "৬")
  ("&" "ক্ষ")
  ("7" "৭")
  ("*" "শ্র")
  ("8" "৮")
  ("9" "৯")
  ("(" "(")
  (")" ")")
  ("0" "০")
  ("\"" "ঠ")
  ("'" "ট")
  ("," ",")
  ((G-,) "৳")
  ("-" "-")
  ("." ".")
  ((G-.) "॥")
  ("/" "য়")
  ((G-/) "্য")
  (":" "ছ")
  (";" "চ")
  ("<" "ষ")
  ((G-<) "৲")
  ("=" "ৃ")
  ((G-=) "ৄ")
  ("+" "ঋ")
  ((G-+) "ৠ")
  (">" "।")
  ((G->) "ঽ")
  ("?" "য")
  ((G-?) "৻")
  ("A" "ও")
  ("C" "ণ")
  ("D" "অ")
  ("E" "আ")
  ("F" "ই")
  ((G-F) "ঌ")
  ("G" "উ")
  ("H" "ফ")
  ("I" "ঘ")
  ("K" "খ")
  ("L" "থ")
  ("M" "শ")
  ("O" "ধ")
  ("P" "ঝ")
  ("Q" "ঔ")
  ("R" "ঈ")
  ((G-R) "ৡ")
  ("S" "এ")
  ("T" "ঊ")
  ("U" "ঙ")
  ("W" "ঐ")
  ("X" "ঁ")
  ("Y" "ভ")
  ("{" "ঢ")
  ((G-{) "ঢ়")
  ("[" "ড")
  ((G-[) "ড়")
  ("}" "ঞ")
  ("]" "়")
  ("_" "ঃ")
  ("a" "ো")
  ("c" "ম")
  ("d" "্")
  ("e" "া")
  ("f" "ি")
  ((G-f) "ৢ")
  ("g" "ু")
  ("h" "প")
  ("i" "গ")
  ("j" "র")
  ("k" "ক")
  ("l" "ত")
  ((G-l) "ৎ")
  ("m" "স")
  ("n" "ল")
  ("o" "দ")
  ("p" "জ")
  ("q" "ৌ")
  ("r" "ী")
  ((G-r) "ৣ")
  ("s" "ে")
  ("t" "ূ")
  ("u" "হ")
  ("v" "ন")
  ("w" "ৈ")
  ("x" "ং")
  ((G-x) "৺")
  ("y" "ব")
  ("z" "ʼ")
  ))

(state
 (init
    (trans)))
