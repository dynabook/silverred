<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="bluetooth-device-specific-pairing" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="bluetooth" group="#last"/>

    <credit type="author">
      <name>Bastien Nocera</name>
      <email>hadess@hadess.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Como parear dispositivos específicos com seu computador.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

  <title>Instruções de pareamento para dispositivos específicos</title>

  <p>Mesmo se você conseguir obter o manual de um dispositivo, ele pode não conter informações suficientes para possibilitar o pareamento. Aqui estão os detalhes de alguns dispositivos comuns.</p>

  <terms>
    <item>
      <title>Controles de PlayStation 3</title>
      <p>Esses dispositivos usam “pareamento de cabos”. Conecte os controles via USB com as <gui>Configurações do Bluetooth</gui> abertas e o Bluetooth ativado. Depois de pressionar o botão “PS”, você será perguntado se deseja configurar esses controles. Desconecte-os e pressione o botão “PS” para usá-los através do Bluetooth.</p>
    </item>
    <item>
      <title>Controles de PlayStation 4</title>
      <p>Esses dispositivos usam “pareamento de cabos” também. Conecte os controles via USB com as <gui>Configurações do Bluetooth</gui> abertas e o Bluetooth ativado. Depois de pressionar o botão “PS”, você será perguntado se deseja configurar esses controles. Desconecte-os e pressione o botão “PS” para usá-los através do Bluetooth.</p>
      <p>O uso da combinação de botões “PS” e “Compartilhar” para parear o controle também pode ser usado para torná-lo visível e pareá-lo como qualquer outro dispositivo Bluetooth, se você não tiver um cabo USB na mão.</p>
    </item>
    <item>
      <title>Controle remoto de MD do PlayStation 3</title>
      <p>Mantenha pressionados os botões “Start” e “Enter” ao mesmo tempo por cerca de 5 segundos. Você pode selecionar o controle remoto na lista de dispositivos, como de costume.</p>
    </item>
    <item>
      <title>Controles remotos de Nintendo Wii e Wii U</title>
      <p>Use o botão vermelho “Sync” dentro do compartimento da bateria para iniciar o processo de pareamento. Outras combinações de botões não manterão as informações de pareamento, de forma que você precisará fazer tudo novamente em pouco tempo. Observe também que alguns softwares desejam acesso direto aos controles remotos e, nesses casos, você não deve configurá-los no painel Bluetooth. Consulte o manual do aplicativo para obter instruções.</p>
    </item>
  </terms>

</page>
