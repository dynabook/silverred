<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="getting-started" version="1.0 if/1.0" xml:lang="ru">

<info>
    <link type="guide" xref="index" group="gs"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <desc>Новичок в GNOME? Узнай, с чего начать.</desc>
    <title type="link">Getting started with GNOME</title>
    <title type="text">Начало работы</title>

    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2015.</mal:years>
    </mal:credit>
  </info>

<title>Начало работы</title>

<if:choose>
<if:when test="!platform:gnome-classic">

  <ui:overlay width="235" height="145">
  <media type="video" its:translate="no" src="figures/gnome-launching-applications.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-launching-apps.svg">
      <ui:caption its:translate="yes"><desc style="center">Запуск приложений</desc></ui:caption>
    </ui:thumb>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>Запуск приложений</tt:p>
         </tt:div>
         <tt:div begin="5s" end="7.5s">
           <tt:p>Переместите курсор мыши на <gui>Обзор</gui>, в левый верхний угол экрана.</tt:p>
           </tt:div>
         <tt:div begin="7.5s" end="9.5s">
           <tt:p>Нажмите значок <gui>Показать приложения</gui>.</tt:p>
         </tt:div>
         <tt:div begin="9.5s" end="11s">
           <tt:p>Нажмите на приложение для его запуска, например «Справка».</tt:p>
         </tt:div>
         <tt:div begin="12s" end="21s">
           <tt:p>Также можно открыть <gui>Обзор</gui> нажав клавишу <key href="help:gnome-help/keyboard-key-super">Super</key>.</tt:p>
         </tt:div>
         <tt:div begin="22s" end="29s">
           <tt:p>Начните вводить название приложения, которое вы хотите запустить.</tt:p>
         </tt:div>
         <tt:div begin="30s" end="33s">
           <tt:p>Нажмите <key>Enter</key> для запуска приложения.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

</if:when>
</if:choose>

  <ui:overlay width="235" height="145">
  <media type="video" its:translate="no" src="figures/gnome-task-switching.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-task-switching.svg">
        <ui:caption its:translate="yes"><desc style="center">Переключение между задачами</desc></ui:caption>
    </ui:thumb>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>Переключение между задачами</tt:p>
         </tt:div>
         <tt:div begin="5s" end="8s">
           <tt:p if:test="!platform:gnome-classic">Переместите курсор мыши на <gui>Обзор</gui>, в левый верхний угол экрана.</tt:p>
         </tt:div>
         <tt:div begin="9s" end="12s">
           <tt:p>Нажмите на окно, чтобы переключиться на эту задачу.</tt:p>
         </tt:div>
         <tt:div begin="12s" end="14s">
           <tt:p>To maximize a window along the left side of the screen, grab
            the window’s titlebar and drag it to the left.</tt:p>
         </tt:div>
         <tt:div begin="14s" end="16s">
           <tt:p>Когда в левой половине экрана появится подсветка, отпустите окно.</tt:p>
         </tt:div>
         <tt:div begin="16s" end="18">
           <tt:p>To maximize a window along the right side, grab the window’s
            titlebar and drag it to the right.</tt:p>
         </tt:div>
         <tt:div begin="18s" end="20s">
           <tt:p>Когда в левой половине экрана появится подсветка, отпустите окно.</tt:p>
         </tt:div>
         <tt:div begin="23s" end="27s">
           <tt:p>Чтобы открыть <gui>переключатель окон</gui>, нажмите <keyseq> <key href="help:gnome-help/keyboard-key-super">Super</key><key> Tab</key></keyseq>.</tt:p>
         </tt:div>
         <tt:div begin="27s" end="29s">
           <tt:p>Чтобы выбрать следующее выделенное окно, отпустите клавишу <key href="help:gnome-help/keyboard-key-super">Super </key>.</tt:p>
         </tt:div>
         <tt:div begin="29s" end="32s">
           <tt:p>Для перемещения по списку открытых окон нажмите клавишу <key>Tab</key>, одновременно удерживая клавишу <key href="help:gnome-help/keyboard-key-super">Super</key>.</tt:p>
         </tt:div>
         <tt:div begin="35s" end="37s">
           <tt:p>Откройте <gui>Обзор</gui>, нажав клавишу <key href="help:gnome-help/keyboard-key-super">Super </key>.</tt:p>
         </tt:div>
         <tt:div begin="37s" end="40s">
           <tt:p>Начните вводить название приложения, на которое вы хотите переключиться.</tt:p>
         </tt:div>
         <tt:div begin="40s" end="43s">
           <tt:p>Когда приложение появится в качестве первого результата, нажмите <key> Enter</key>, чтобы на него переключиться.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

  <ui:overlay width="235" height="145">
  <media type="video" its:translate="no" src="figures/gnome-windows-and-workspaces.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-windows-and-workspaces.svg">
          <ui:caption its:translate="yes"><desc style="center">Использование окон и рабочих столов</desc></ui:caption>
    </ui:thumb>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>Окна и рабочие столы</tt:p>
         </tt:div>
         <tt:div begin="6s" end="10s">
           <tt:p>To maximize a window, grab the window’s titlebar and drag it to
            the top of the screen.</tt:p>
           </tt:div>
         <tt:div begin="10s" end="13s">
           <tt:p>Когда на экране появится подсветка, отпустите окно.</tt:p>
         </tt:div>
         <tt:div begin="14s" end="20s">
           <tt:p>To unmaximize a window, grab the window’s titlebar and drag it
            away from the edges of the screen.</tt:p>
         </tt:div>
         <tt:div begin="25s" end="29s">
           <tt:p>Также можно нажать на верхнюю панель, чтобы перетащить окно и вернуть его прежний размер.</tt:p>
         </tt:div>
         <tt:div begin="34s" end="38s">
           <tt:p>To maximize a window along the left side of the screen, grab
            the window’s titlebar and drag it to the left.</tt:p>
         </tt:div>
         <tt:div begin="38s" end="40s">
           <tt:p>Когда в левой половине экрана появится подсветка, отпустите окно.</tt:p>
         </tt:div>
         <tt:div begin="41s" end="44s">
           <tt:p>To maximize a window along the right side of the screen, grab
            the window’s titlebar and drag it to the right.</tt:p>
         </tt:div>
         <tt:div begin="44s" end="48s">
           <tt:p>Когда в левой половине экрана появится подсветка, отпустите окно.</tt:p>
         </tt:div>
         <tt:div begin="54s" end="60s">
           <tt:p>Чтобы развернуть окно на весь экран с помощью клавиатуры, нажмите клавишу <key href="help:gnome-help/keyboard-key-super">Super</key> и затем <key>↑</key>.</tt:p>
         </tt:div>
         <tt:div begin="61s" end="66s">
           <tt:p>Чтобы восстановить прежний размер окна используя клавиатуру, нажмите клавишу <key href="help:gnome-help/keyboard-key-super">Super</key> и затем <key>↓</key>.</tt:p>
         </tt:div>
         <tt:div begin="66s" end="73s">
           <tt:p>Чтобы растянуть окно вдоль правой стороны экрана, нажмите клавишу <key href="help:gnome-help/keyboard-key-super">Super</key> и затем <key>→</key>.</tt:p>
         </tt:div>
         <tt:div begin="76s" end="82s">
           <tt:p>Чтобы растянуть окно вдоль левой стороны экрана, нажмите клавишу <key href="help:gnome-help/keyboard-key-super">Super</key> и затем <key>←</key>.</tt:p>
         </tt:div>
         <tt:div begin="83s" end="89s">
           <tt:p>Чтобы переключиться на нижнее рабочее место, нажмите <keyseq><key href="help:gnome-help/keyboard-key-super">Super </key><key>Page Down</key></keyseq>.</tt:p>
         </tt:div>
         <tt:div begin="90s" end="97s">
           <tt:p>Чтобы переключиться на верхнее рабочее место, нажмите <keyseq><key href="help:gnome-help/keyboard-key-super">Super </key><key>Page Up</key></keyseq>.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

<links type="topic" style="grid" groups="tasks">
<title style="heading">Common tasks</title>
</links>

<links type="guide" style="heading nodesc">
<title/>
</links>

</page>
