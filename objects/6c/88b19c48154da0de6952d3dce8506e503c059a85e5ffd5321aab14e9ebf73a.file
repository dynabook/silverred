<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-disc-write" xml:lang="sl">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Zapis datotek in dokumentov na prazen CD ali DVD z uporabo zapisovalca CD-jev/DVD-jev.</desc>
  </info>

  <title>Zapis datotek na CD ali DVD</title>

  <p>Datoteke lahko zapišete na prazen disk z <gui>Ustvarjalnikom CD-jev/DVD-jev</gui>. Možnost ustvarjanja CD-ja ali DVD-ja se bo v upravljalniku datotek pojavila takoj, ko vstavite CD v zapisovalnik CD/DVD. Upravljalnik datotek vam omogoča enostaven prenos datotek na druge računalnike ali ustvarjanje <link xref="backup-why">varnostnih kopij</link> s kopiranjem datotek na prazen disk. Za zapisovanje datotek na CD ali DVD:</p>

  <steps>
    <item>
      <p>Prazen disk vstavite v zapisljivi pogon CD/DVD.</p></item>
    <item>
      <p>V oknu <gui>Prazen disk CD/DVD-R</gui>, ki se pojavi, izberite <gui>Odpri z ustvarjalnikom CD-jev/DVD-jev</gui>. Odprlo se bo okno <gui>Ustvarjalnik CD-jev/DVD-jev</gui>.</p>
      <p>(Lahko tudi kliknete na <gui>Prazen disk CD/DVD-R</gui> pod <gui>Naprave</gui> v stranski vrstici upravljalnika datotek.)</p>
    </item>
    <item>
      <p>V polje <gui>Ime diska</gui> vpišite ime diska.</p>
    </item>
    <item>
      <p>Povlecite ali kopirajte želene datoteke v okno.</p>
    </item>
    <item>
      <p>Kliknite <gui>Zapiši na disk</gui>.</p>
    </item>
    <item>
      <p>V <gui>Izbor diska za zapisovanje </gui> izberite prazen disk.</p>
      <p>(Namesto tega izberite <gui>Datoteka odtisa</gui>. To bo iz datotek ustvarilo <em>odtis diska</em>, ki bo shranjen na vaš računalnik. Odtis diska lahko kasneje zapišete na prazen disk.)</p>
    </item>
    <item>
      <p>V primeru da želite prilagoditi hitrost zapisovanja, mesto začasnih datotek in druge možnosti, kliknite <gui>Lasnosti</gui>. Privzete možnosti bi morale biti v redu.</p>
    </item>
    <item>
      <p>Za začetek zapisovanja kliknite na gumb <gui>Zapiši</gui>.</p>
      <p>V primeru da ste izbrali <gui>Zapiši več kopij</gui>, boste pozvani k vstavitvi dodatnih diskov.</p>
    </item>
    <item>
      <p>Ko je zapisovanje diska končano, bo samodejno izvržen. Za končanje izberite <gui>Naredi več kopij</gui> ali <gui>Zapri</gui>.</p>
    </item>
  </steps>

<section id="problem">
  <title>If the disc wasn’t burned properly</title>

  <p>Sometimes the computer doesn’t record the data correctly, and you won’t be
  able to see the files you put onto the disc when you insert it into a
  computer.</p>

  <p>In this case, try burning the disc again but use a lower burning speed,
  for example, 12x rather than 48x. Burning at slower speeds is more reliable.
  You can choose the speed by clicking the <gui>Properties</gui> button in the
  <gui>CD/DVD Creator</gui> window.</p>

</section>

</page>
