<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="bluetooth-visibility" xml:lang="de">

  <info>
    <link type="guide" xref="bluetooth" group="#last"/>
    <link type="seealso" xref="bluetooth-device-specific-pairing"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-04" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Legt fest, ob andere Geräte Ihren Rechner finden können.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2019.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2019.</mal:years>
    </mal:credit>
  </info>

  <title>Was ist die Bluetooth-Sichtbarkeit?</title>

  <p>Die Bluetooth-Sichtbarkeit bezieht sich darauf, ob andere Geräte bei der Suche nach Bluetooth-Geräten Ihren Rechner finden können. Wenn Bluetooth eingeschaltet ist und das Panel <gui>Bluetooth</gui> geöffnet ist, dann kündigt sich Ihr Rechner allen anderen Geräten innerhalb des Empfangsbereichs an und erlaubt den Aufbau von Verbindungen mit Ihrem Rechner.</p>

  <note style="tip">
    <p>Sie können den Namen <link xref="sharing-displayname">ändern</link>, unter dem Ihr Rechner für andere erscheint.</p>
  </note>

  <p>Nachdem Sie sich <link xref="bluetooth-connect-device">mit einem Gerät verbunden haben</link>, muss weder Ihr Rechner noch das andere Gerät sichtbar bleiben, um kommunizieren zu können.</p>

  <p>Geräte ohne Anzeigefeld bieten üblicherweise eine Möglichkeit, den Kopplungsmodus über das Gedrückthalten eines Knopfes oder einer Knopfkombination zu aktivieren, während das Gerät bereits läuft oder gerade eingeschaltet wird.</p>

  <p>Um herauszufinden, wie Sie den Kopplungsmodus eines Gerätes aktivieren können, sollten Sie am besten das Gerätehandbuch zu Rate ziehen. Bei einigen Geräten könnte die Prozedur <link xref="bluetooth-device-specific-pairing">vom Standard abweichen</link>.</p>

</page>
