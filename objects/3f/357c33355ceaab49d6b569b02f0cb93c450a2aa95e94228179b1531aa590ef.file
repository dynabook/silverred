<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting-hardware-check" xml:lang="ca">

  <info>
    <link type="next" xref="net-wireless-troubleshooting-device-drivers"/>
    <link type="guide" xref="net-wireless-troubleshooting"/>

    <revision pkgversion="3.4.0" date="2012-03-05" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Col·laboradors a la wiki de documentació d'Ubuntu</name>
    </credit>
    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Encara que el vostre adaptador sense fil estigui connectat, pot ser que l'ordinador no l'hagi reconegut correctament.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

  <title>Solució de problemes de connexió sense fil</title>
  <subtitle>Comproveu que l'adaptador sense fil s'hagi reconegut</subtitle>

  <p>Fins i tot si l'adaptador sense fil està connectat a l'ordinador, pot ser que no s'hagi reconegut com a dispositiu de xarxa. En aquest pas, comproveu si el dispositiu s'ha reconegut correctament.</p>

  <steps>
    <item>
      <p>Obriu una finestra del terminal, escriviu <cmd>lshw -C network</cmd> i premeu <key>Retorn</key>. Si ús dóna un missatge d'error, és possible que hàgiu d'instal·lar el programa <app>lshw</app> al vostre ordinador.</p>
    </item>
    <item>
      <p>Mireu a través de la informació que apareix i troba la secció <em>Interface del dispositiu sense fil</em>. Si el vostre adaptador s'ha detectat correctament, hauríeu de veure alguna cosa similar (però no idèntic) a això:</p>
      <code>*-network
       description: Wireless interface
       product: PRO/Wireless 3945ABG [Golan] Network Connection
       vendor: Intel Corporation</code>
    </item>
    <item>
      <p>Si es llista un dispositiu sense, continueu al <link xref="net-wireless-troubleshooting-device-drivers">pas Controladors del Dispositiu</link>.</p>
      <p>Si el dispositiu sense fil <em>no</em> es llista, els següents passos que feu dependran del tipus de dispositiu que utilitzeu. Consulteu a sota la secció que sigui rellevant per al tipus d'adaptador que teniu instal·lat a l'ordinador (<link xref="#pci">PCI intern</link>, <link xref="#usb">USB</link>, o <link xref="#pcmcia">PCMCIA</link>).</p>
    </item>
  </steps>

<section id="pci">
  <title>Adaptador PCI (intern) de la xarxa sense fil</title>

  <p>Els adaptadors interns PCI són els més comuns i es troben en la majoria de portàtils dels darrers anys. Per comprovar si s'ha reconegut el vostre adaptador PCI:</p>

  <steps>
    <item>
      <p>Obriu un Terminal, escriviu <cmd>lspci</cmd> i premeu <key>Retorn</key>.</p>
    </item>
    <item>
      <p>Mireu la llista de dispositius que es mostren i trobeu tots els marcats com a <code>controlador de xarxa</code> o <code>controlador Ethernet</code>. Diversos dispositius es poden marcar d'aquesta manera; el que correspongui al vostre adaptador pot incloure paraules com <code>wireless</code>, <code>WLAN</code>, <code>wifi</code> o <code>802.11</code>. Aquí teniu un exemple del que podria visualitzar-se:</p>
      <code>Network controller: Intel Corporation PRO/Wireless 3945ABG [Golan] Network Connection</code>
    </item>
    <item>
      <p>Si heu trobat l'adaptador sense fil a la llista, aneu al pas <link xref="net-wireless-troubleshooting-device-drivers">Controladors del Dispositiu</link>. Si no heu trobat res relacionat amb l'adaptador sense fil, consulteu <link xref="#not-recognized"> les instruccions a continuació</link>.</p>
    </item>
  </steps>

</section>

<section id="usb">
  <title>Adaptador USB</title>

  <p>Els adaptadors de les xarxes sense fil que es connecten a un port USB de l'ordinador són menys comuns. Poden connectar-se directament a un port USB o connectar-se mitjançant un cable USB. Els adaptadors de banda ampla 3G/mòbil són molt similars als adaptadors sense fil (Wi-Fi), de manera que si creieu que teniu un adaptador USB, comproveu que en realitat no és un adaptador 3G. Per comprovar si s'ha reconegut l'adaptador USB:</p>

  <steps>
    <item>
      <p>Obriu un terminal, escriviu <cmd>lsusb</cmd> i premeu <key>Retorn</key>.</p>
    </item>
    <item>
      <p>Mireu la llista de dispositius que es mostren i cerqueu-ne qualsevol que sembli referir-se a un dispositiu sense fil o de xarxa. El que correspongui al vostre adaptador pot incloure paraules com <code>wireless</code>, <code>WLAN</code>, <code>wifi</code> o <code>802.11</code>. Aquí teniu un exemple de com podria ser l'entrada:</p>
      <code>Bus 005 Device 009: ID 12d1:140b Huawei Technologies Co., Ltd. EC1260 Wireless Data Modem HSD USB Card</code>
    </item>
    <item>
      <p>Si heu trobat l'adaptador sense fil a la llista, aneu al pas <link xref="net-wireless-troubleshooting-device-drivers">Controladors del Dispositiu</link>. Si no heu trobat res relacionat amb l'adaptador sense fil, consulteu <link xref="#not-recognized"> les instruccions a continuació</link>.</p>
    </item>
  </steps>

</section>

<section id="pcmcia">
  <title>Comprovar un dispositiu PCMCIA</title>

  <p>Els adaptadors sense fil PCMCIA solen ser targetes rectangulars que es col·loquen al costat del portàtil. Es troben més comunament en ordinadors antics. Per comprovar si s'ha reconegut l'adaptador PCMCIA:</p>

  <steps>
    <item>
      <p>Arrenqueu l'ordinador <em>sense</em> la targeta de xarxa connectada.</p>
    </item>
    <item>
      <p>Obriu un terminal i escriviu el següent i premeu <key>Retorn</key>:</p>
      <code>tail -f /var/log/messages</code>
      <p>Això mostrarà una llista de missatges relacionats amb el maquinari de l'ordinador, i s'actualitzarà automàticament si hi ha alguna cosa que hagi canviat al maquinari.</p>
    </item>
    <item>
      <p>Introduïu l'adaptador a la ranura PCMCIA i mireu els canvis a la finestra del Terminal. Els canvis haurien d'incloure certa informació sobre l'adaptador sense fil. Mireu si podeu identificar-los.</p>
    </item>
    <item>
      <p>Per acabar l'ordre que s'executa al terminal, premeu <keyseq><key>Ctrl</key><key>C</key></keyseq>. Després de fer-ho, podeu tancar el terminal si voleu.</p>
    </item>
    <item>
      <p>Si heu trobat informació sobre el vostre adaptador, aneu al <link xref="net-wireless-troubleshooting-device-drivers">pas controladors del Dispositiu</link>. Si no heu trobat res relacionat amb l'adaptador sense fil, consulteu <link xref="#not-recognized">les instruccions a continuació</link>.</p>
    </item>
  </steps>
</section>

<section id="not-recognized">
  <title>No s'ha reconegut l'adaptador de la xarxa sense fil</title>

  <p>Si no es reconeix l'adaptador, és possible que no funcioni correctament o que no s'hi instal·lin els controladors correctes. Com es comprova si hi ha algun controlador que es pugui instal·lar dependrà de la distribució de Linux que s'utilitzi (com ara Ubuntu, Arch, Fedora o openSUSE).</p>

  <p>Per obtenir ajuda específica, consulteu les opcions de suport al lloc web de la vostra distribució. Aquests poden incloure llistes de correu i xats web on podeu preguntar sobre l'adaptador, per exemple.</p>

</section>

</page>
