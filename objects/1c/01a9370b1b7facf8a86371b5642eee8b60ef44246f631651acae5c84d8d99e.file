<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-bouncekeys" xml:lang="nl">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Snel herhaalde toetsaanslagen negeren.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Springende toetsen inschakelen</title>

  <p><em>Springende toetsen</em> inschakelen om toetsaanslagen die snel herhaald worden te negeren. Als u bijvoorbeeld door bevende handen onbedoeld meerdere malen op een toets drukt kunt u springende toetsen inschakelen.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Universal Access</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Druk op <gui>Typ-assistent (AccessX)</gui> onder het kopje <gui>Typen</gui>.</p>
    </item>
    <item>
      <p>Switch the <gui>Bounce Keys</gui> switch to on.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Springende toetsen snel aan- en uitzetten</title>
    <p>U kunt springende toetsen aan- en uitzetten door te klikken op het <link xref="a11y-icon">toegankelijkheidspictogram</link> in de bovenbalk en <gui>Springende toetsen</gui> te selecteren. Het toegankelijkheidspictogram is zichtbaar als één of meer instellingen in <gui>Universele toegang</gui> ingeschakeld zijn.</p>
  </note>

  <p>Gebruik de <gui>Vertraging</gui>-schuifregelaar om te wijzigen hoe lang de vertraging moet duren voordat de computer een tweede toetsaanslag herkent, nadat er een toets is ingedrukt. Selecteer <gui>Piepen wanneer een toets is geweigerd</gui> als u wilt dat de computer een geluid maakt wanneer een toetsaanslag wordt genegeerd omdat deze te snel volgt op de vorige toetsaanslag.</p>

</page>
