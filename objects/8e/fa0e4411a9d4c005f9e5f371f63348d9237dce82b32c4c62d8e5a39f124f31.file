<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-mag" xml:lang="sr">

  <info>
    <link type="guide" xref="a11y#vision" group="lowvision"/>

    <revision pkgversion="3.7.1" date="2012-11-10" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Шон Мек Кенс</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Мајкл Хил</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Увећајте ваш екран тако да вам буде лакше да видите ствари.</desc>
  </info>

  <title>Повећајте област екрана</title>

  <p>Повећавање екрана је другачије од самог увећавања <link xref="a11y-font-size">величине текста</link>. Ова функција је као да имате увеличавајуће стакло, које вам омогућава да се крећете унаоколо и да приближавате делове екрана.</p>

  <steps>
    <item>
      <p>Отворите преглед <gui xref="shell-introduction#activities">Активности</gui> и почните да куцате <gui>Универзални приступ</gui>.</p>
    </item>
    <item>
      <p>Кликните на <gui>Универзални приступ</gui> да отворите панел.</p>
    </item>
    <item>
      <p>Притисните <gui>Увеличање</gui> у одељку <gui>Видети</gui>.</p>
    </item>
    <item>
      <p>Switch the <gui>Zoom</gui> switch in the top-right corner of the
      <gui>Zoom Options</gui> window to on.</p>
      <!--<note>
        <p>The <gui>Zoom</gui> section lists the current settings for the
        shortcut keys, which can be set in the <gui>Universal Access</gui>
        section of the <link xref="keyboard-shortcuts-set">Shortcuts</link> tab
        on the <gui>Keyboard</gui> panel.</p>
      </note>-->
    </item>
  </steps>

  <p>Сада можете да се крећете унаоколо по области екрана. Померајући вашег миша до ивица екрана, помераћете увеличану област у разним правцима, што ће вам омогућити да видите област вашег избора.</p>

  <note style="tip">
    <p>Можете да укључите или да искључите зумирање тако што ћете кликнути на <link xref="a11y-icon">иконицу приступачности</link> на горњој траци и изабрати <gui>Зумирај</gui>.</p>
  </note>

  <p>Можете да измените чинилац увеличања, праћење миша, и положај увеличаног прегледа на екрану. Дотерајте ово у језичку <gui>Лупа</gui> у прозору <gui>Опција увећања</gui>.</p>

  <p>Можете да активирате нишан да вам помогне да пронађете миша или показивач додирне табле. Укључите их и дотерајте њихову дужину, боју и дебљину, у језичку <gui>Нишана</gui> у прозору подешавања <gui>Увећања</gui>.</p>

  <p>Можете да се пребаците на обрнути видео или <gui>Бело на црном</gui>, и да подесите опције осветљености, контраста и сивила за лупу. Комбинација ових опција је корисна за људе са слабијим видом, сваки степен фотофобије, или само за коришћење рачунара под неповољним условима осветљености. Изаберите језичак <gui>Ефекти боје</gui> у прозору подешавања <gui>Увећања</gui> да укључите и измените ове опције.</p>

</page>
