<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="clock-world" xml:lang="sv">

  <info>
    <link type="guide" xref="clock" group="#last"/>
    <link type="seealso" href="help:gnome-clocks/index"/>

    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.28" date="2018-07-30" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhill@gnome.org</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Visa tider i olika städer under kalendern.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Lägg till en världsklocka</title>

  <p>Använd <app>Klockor</app> för att lägga till tider i olika städer.</p>

  <note>
    <p>Detta kräver att programmet <app>Klockor</app> är installerat.</p>
    <p>De flesta distributionerna har <app>Klockor</app> förinstallerat som standard. Om din inte har det så kan du behöva installera det via din distributions pakethanterare.</p>
  </note>

  <p>För att lägga till en världsklocka:</p>

  <steps>
    <item>
      <p>Klicka på klockan i systemraden.</p>
    </item>
    <item>
      <p>Klicka på länken <gui>Lägg till världsklocka</gui> under kalendern för att starta <app>Klockor</app>.</p>

    <note>
       <p>Om du redan har en eller flera världsklockor, klicka på en så kommer <app>Klockor</app> att starta.</p>
    </note>

    </item>
    <item>
      <p>I fönstret <app>Klockor</app>, klicka på knappen <gui style="button">Ny</gui> eller tryck <keyseq><key>Ctrl</key><key>N</key></keyseq> för att lägga till en ny stad.</p>
    </item>
    <item>
      <p>Börja skriva in namnet på staden i sökrutan.</p>
    </item>
    <item>
      <p>Välj rätt stad eller den platsen närmast dig från listan.</p>
    </item>
    <item>
      <p>Tryck på <gui style="button">Lägg till</gui> för att lägga till staden.</p>
    </item>
  </steps>

  <p>Se vidare i <link href="help:gnome-clocks">Hjälp för klockan</link> för ytterligare funktioner i <app>Klockor</app>.</p>

</page>
