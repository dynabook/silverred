<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mime-types-custom-user" xml:lang="es">

  <info>
    <link type="guide" xref="software#management"/>
    <link type="seealso" xref="mime-types"/>
    <revision pkgversion="3.12" date="2014-06-17" status="review"/>

    <credit type="author copyright">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Crear una especificación de tipo MIME para un usuario y registrar una aplicación predeterminada.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Oliver Gutiérrez</mal:name>
      <mal:email>ogutsua@gmail.com</mal:email>
      <mal:years>2018 - 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2017 - 2019</mal:years>
    </mal:credit>
  </info>

    <title>Añadir un tipo MIME personalizado para usuarios individuales</title>
    <p>Para añadir un tipo MIME personalizado para usuarios individuales y registrar una aplicación predeterminada para ese tipo MIME, debe crear un nuevo archivo de especificación de tipo MIME en la carpeta <file>~/.local/share/mime/packages/</file> y un archivo <file>.desktop</file> en la carpeta <file>~/.local/share/applications/</file>.</p>
    <steps>
      <title>Añadir un tipo MIME <code>application/x-newtype</code> para usuarios individuales</title>
      <item>
        <p>Cree el archivo <file>~/.local/share/mime/packages/application-x-newtype.xml</file>:</p>
        <code mime="application/xml">&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;mime-info xmlns="http://www.freedesktop.org/standards/shared-mime-info"&gt;
  &lt;mime-type type="application/x-newtype"&gt;
    &lt;comment&gt;new mime type&lt;/comment&gt;
    &lt;glob pattern="*.xyz"/&gt;
  &lt;/mime-type&gt;
&lt;/mime-info&gt;</code>
      <p>El archivo de ejemplo <file>application-x-newtype.xml</file> más arriba define un nuevo tipo MIME <sys>application/x-newtype</sys> y asigna nombres de archivo con la extensión <file>.xyz</file> a ese tipo MIME.</p>
      </item>
      <item>
        <p>Cree un archivo <file>.desktop</file> nuevo llamado, por ejemplo, <file>myapplication1.desktop</file>, y cópielo a la carpeta <file>~/.local/share/applications/</file>:</p>
        <code>[Desktop Entry]
Type=Application
MimeType=application/x-newtype
Name=<var>My Application 1</var>
Exec=<var>myapplication1</var></code>
      <p>El archivo de ejemplo <file>myapplication1.desktop</file> más arriba, asocia el tipo MIME <code>application/x-newtype</code> con una aplicación llamada <app>My Application 1</app>, la cual se ejecuta con el comando <cmd>myapplication1</cmd>.</p>
      </item>
      <item>
        <p>Actualice la base de datos MIME para aplicar los cambios:</p>
        <screen><output>$ </output><input>update-mime-database ~/.local/share/mime</input>
        </screen>
      </item>
      <item>
        <p>Actualice la base de datos:</p>
        <screen><output>$ </output><input>update-desktop-database ~/.local/share/applications</input>
        </screen>
      </item>
      <item>
        <p>Para verificar que ha asociado correctamente los archivos <file>*.xyz</file> con el tipo MIME <sys>application/x-newtype</sys>, primero cree un archivo vacío, por ejemplo <file>test.xyz</file>:</p>
        <screen><output>$ </output><input>touch test.xyz</input></screen>
        <p>Entonces ejecute el comando <cmd>gio info</cmd>:</p>
        <screen><output>$ </output><input>gio info test.xyz | grep "standard::content-type"</input>
  standard::content-type: application/x-newtype</screen>
        </item>
        <item>
          <p>Para verificar que el archivo <file>myapplication1.desktop</file> se ha configurado como la aplicación registrada predeterminada para el tipo MIME <sys>application/x-newtype</sys>, ejecute el comando <cmd>gio mime</cmd>:</p>
        <screen><output>$ </output><input>gio mime application/x-newtype</input>
Default application for “application/x-newtype”: myapplication1.desktop
Registered applications:
	myapplication1.desktop
Recommended applications:
	myapplication1.desktop</screen>
      </item>
    </steps>
</page>
