<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="bluetooth-remove-connection" xml:lang="fi">

  <info>
    <link type="guide" xref="bluetooth"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.12" date="2014-03-04" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Laitteen poistaminen Bluetooth-laitteiden luettelosta.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

  <title>Katkaise yhteys Bluetooth-laitteeseen</title>

  <p>If you do not want to be connected to a Bluetooth device anymore, you can
  remove the connection. This is useful if you no longer want to use a device
  like a mouse or headset, or if you no longer wish to transfer files to or
  from a device.</p>

  <steps>
    <item>
      <p>Avaa <gui xref="shell-introduction#activities">Toiminnot</gui>-yleisnäkymä ja ala kirjoittamaan <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Napsauta <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Select the device which you want to disconnect from the list.</p>
    </item>
    <item>
      <p>In the device dialog box, switch the <gui>Connection</gui> switch to
      off, or to remove the device from the <gui>Devices</gui> list,
      click <gui>Remove Device</gui>.</p>
    </item>
  </steps>

  <p>Voit <link xref="bluetooth-connect-device">yhdistää Bluetooth-laitteeseen uudelleen</link> myöhemmin niin halutessasi.</p>

</page>
