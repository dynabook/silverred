<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-wireless-noconnection" xml:lang="ca">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" version="0.2" date="2013-11-11" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <desc>Comproveu la contrasenya i altres coses per provar.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

<title>He introduït la contrasenya correcta, però encara no puc connectar-me</title>

<p>Si esteu segur que heu introduït correctament la <link xref="net-wireless-wepwpa">contrasenya de la xarxa</link> però encara no podeu connectar-vos correctament a la xarxa sense fil, proveu algunes de les següents accions:</p>

<list>
 <item>
  <p>Comproveu que teniu la contrasenya correcta</p>
  <p>Les contrasenyes distingeixen entre majúscules i minúscules (importa si tenen caràcters amb majúscules o minúscules), així que comproveu que no tingueu una de les lletres incorrectes.</p>
 </item>

 <item>
  <p>Proveu la clau de pas hexadecimal o ASCII</p>
  <p>La contrasenya que introduïu també es pot representar d'una manera diferent - com una cadena de caràcters en hexadecimal (números 0-9 i lletres a-f - anomenada clau de pas. Cada contrasenya té una clau de pas equivalent. Si teniu accés a la clau de pas, així com a la contrasenya / clau, intenteu escriure la clau de pas. Assegureu-vos d'haver triat correctament l'opció de <gui>seguretat de la xarxa</gui> quan se us demani la contrasenya (per exemple, seleccioneu <gui>la clau WEP 40/128-bit</gui> si esteu escrivint la clau de pas de 40 caràcters per a una connexió WEP encriptada).</p>
 </item>

 <item>
  <p>Proveu de desactivar la targeta sense fil i tornar-la a encendre</p>
  <p>De vegades les targetes sense fil s'aturen o experimenten problemes menors que vol dir que no es connecten. Proveu de desactivar la targeta i torneu-la a iniciar per restablir-la, consulteu <link xref="net-wireless-troubleshooting"/> per a més informació.</p>
 </item>

 <item>
  <p>Comproveu que feu servir el tipus correcte de seguretat per a la xarxa</p>
  <p>Quan se us demani la contrasenya de seguretat, podeu triar el tipus de seguretat que voleu utilitzar. Assegureu-vos que trieu la que utilitza l'encaminador o l'estació sense fil base. Això s'ha de seleccionar per defecte, però de vegades no es fa per alguna raó. Si no sabeu quina és, feu servir la prova i error per anar passant per les diferents opcions.</p>
 </item>

 <item>
  <p>Comproveu que la vostra targeta de xarxa sense fil està suportada pel sistema</p>
  <p>Algunes targetes no es suporten bé. Es presenten com una connexió sense fil, però no es poden connectar a una xarxa perquè els seus controladors no tenen la capacitat de fer-ho. Consulteu si podeu obtenir un controlador alternatiu o si necessiteu una configuració addicional (com ara instal·lar un altre <em>firmware</em>). Consulteu <link xref="net-wireless-troubleshooting"/> per a més informació.</p>
 </item>

</list>

</page>
