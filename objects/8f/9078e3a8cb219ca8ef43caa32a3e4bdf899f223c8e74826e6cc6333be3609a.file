<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-browse" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>
    <link type="seealso" xref="files-copy"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-16" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Gerencie e organize arquivos com o gerenciador de arquivos.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

<title>Navegando por arquivos e pastas</title>

<p>Use o gerenciador de arquivos <app>Arquivos</app> para navegar e organizar os arquivos no seu computador. Você também pode usá-lo para gerenciar os arquivos em dispositivos de armazenamento (como discos rígidos externos), em <link xref="nautilus-connect">servidores de arquivos</link> e em compartilhamentos em rede.</p>

<p>Para abrir o gerenciador de arquivos, abra <app>Arquivos</app> no panorama de <gui xref="shell-introduction#activities">Atividades</gui>. Você também pode pesquisar por arquivos e pastas através do panorama da mesma forma que faria uma <link xref="shell-apps-open">pesquisa por aplicativos</link>.</p>

<section id="files-view-folder-contents">
  <title>Explorando o conteúdo dos diretórios</title>

<p>No gerenciador de arquivos, dê um clique duplo em qualquer pasta para ver seu conteúdo, e dê um duplo clique ou <link xref="mouse-middleclick">clique com o botão do meio</link> em qualquer arquivo para abri-lo com o seu aplicativo padrão. Clique com o botão meio para abri-lo em uma nova aba. Você também pode efetuar um clique com o botão direito em uma pasta para abri-la em uma nova aba ou janela.</p>

<p>Ao buscar por arquivos em uma pasta, você pode <link xref="files-preview">fazer uma pré-visualização de um arquivo</link> rapidamente, pressionando a barra de espaço: isso assegura que você está com o arquivo certo antes de abri-lo, copiá-lo ou excluí-lo.</p>

<p>A <em>barra de caminho</em> acima da lista de arquivos e pastas lhe mostra qual pasta você está vendo, inclusive as pastas pai da pasta atual. Clique em uma pasta pai nessa barra de caminho para ir até ela. Clique com o direito em qualquer pasta na barra de caminho para abri-la em uma nova aba ou janela, ou acessar suas propriedades.</p>

<p>Se quiser <link xref="files-search">pesquisar por um arquivo</link> rapidamente, dentro ou abaixo da pasta que você está vendo, comece a digitar o nome dele. Uma <em>barra de pesquisa</em> aparecerá no parte superior da janela e apenas arquivos que coincidir com sua pesquisa será mostrado. Pressione <key>Esc</key> para cancelar a pesquisa.</p>

<p>Você pode acessar rapidamente lugares comuns na <em>barra lateral</em>. Se não vê a barra lateral, pressione o botão de menu no canto superior direito na janela e, então selecione <gui>Barra lateral</gui>. Você pode adicionar marcadores a pastas que você use com frequência e eles aparecerão na barra lateral. Arraste uma pasta para a barra lateral e solte-a sobre <gui>Novo marcador</gui>, o qual dinamicamente aparece, ou clique na pasta atual na barra de caminho e, então, selecione <gui style="menuitem">Adicione marcador</gui>.</p>

</section>

</page>
