<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-states" xml:lang="id">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>

    <revision pkgversion="3.4.0" date="2012-03-24" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Arrange windows in a workspace to help you work more
    efficiently.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

  <title>Memindah dan mengubah ukuran jendela</title>

  <p>You can move and resize windows to help you work more efficiently. In
  addition to the dragging behavior you might expect, GNOME features shortcuts
  and modifiers to help you arrange windows quickly.</p>

  <list>
    <item>
      <p>Move a window by dragging the titlebar, or hold down
      <key xref="keyboard-key-super">Super</key> and drag anywhere in the
      window. Hold down <key>Shift</key> while moving to snap the window to the
      edges of the screen and other windows.</p>
    </item>
    <item>
      <p>Resize a window by dragging the edges or corner of the window. Hold
      down <key>Shift</key> while resizing to snap the window to the edges
      of the screen and other windows.</p>
      <p if:test="platform:gnome-classic">You can also resize a maximized
      window by clicking the maximize button in the titlebar.</p>
    </item>
    <item>
      <p>Move or resize a window using only the keyboard. Press
      <keyseq><key>Alt</key><key>F7</key></keyseq> to move a window or
      <keyseq><key>Alt</key><key>F8</key></keyseq> to resize. Use the arrow
      keys to move or resize, then press <key>Enter</key> to finish, or press
      <key>Esc</key> to return to the original position and size.</p>
    </item>
    <item>
      <p><link xref="shell-windows-maximize">Maksimalkan suatu jendela</link> dengan menyeretnya ke puncak layar. Seret jendela ke salah satu sisi layar untuk memaksimalkannya sepanjang sisi, memungkinkan Anda <link xref="shell-windows-tiled">memasang jendela sejajar bersisian</link>.</p>
    </item>
  </list>

</page>
