<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="sound-nosound" xml:lang="lv">

  <info>
    <link type="guide" xref="sound-broken"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.18" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>GNOME dokumentācijas projekts</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Pārbaudiet, vai skaņa nav apklusināta, vai kabeļi ir iesprausti kārtīgi un vai skaņu karte ir atpazīta.</desc>
  </info>

<title>Es nedzirdu nekādas skaņas no datora</title>

  <p>Ja nedzirdat nekādas skaņas savā datorā, piemēram, atskaņojot mūziku, ejiet cauri sekojošajiem atkļūdošanas padomiem.</p>

<section id="mute">
  <title>Pārliecinieties, ka skaņa nav apklusināta</title>

  <p>Open the <gui xref="shell-introduction#systemmenu">system menu</gui> and make sure that
  the sound is not muted or turned down.</p>

  <p>Dažiem klēpjdatoriem ir skaņas atslēgšanas slēdži vai pogas uz tastatūrām — pamēģiniet nospiest šo taustiņu, lai pārbaudītu, vai tas ieslēdz skaņu.</p>

  <p>Vajadzētu arī pārliecināties, ka lietotne, kuru izmantojat, lai atskaņotu, piemēram, mūziku, nav apklusināta. Lietotnei vajadzētu būt skaņas regulācijas pogai galvenajā logā — pārbaudiet to.</p>

  <p>Arī pārbaudiet <gui>Lietotņu</gui> cilni <gui>Skaņas</gui> GUI:</p>
  <steps>
    <item>
      <p>Atveriet <gui xref="shell-introduction#activities">Aktivitāšu</gui> pārskatu un sāciet rakstīt <gui>Skaņa</gui>.</p>
    </item>
    <item>
      <p>Spiediet <gui>Skaņa</gui>, lai atvērtu paneli.</p>
    </item>
    <item>
      <p>Under <gui>Volume Levels</gui>, check that your application is not
      muted.</p>
    </item>
  </steps>

</section>

<section id="speakers">
  <title>Pārliecinieties, ka skaļruņi ir ieslēgti un pareizi savienoti</title>
  <p>Ja datoram ir ārējie skaļruņi, pārliecinieties, ka tie ir ieslēgti un ir uzgriezts skaļums. Pārliecinieties, ka skaļruņa kabelis ir droši iesprausts “izejas” audio kontaktā jūsu datorā. Parasti kontakts ir gaiši zaļā krāsā.</p>

  <p>Dažas skaņu kartes spēj pārslēgties starp kontaktiem, kuru izmanto izejai (skaļruņiem) un kontaktiem ieejai (piemēram, mikrofonam). Izejas kontakts Linux var atšķirties no tā, kurš ir, piemēram, Windows vai Mac OS. Pamēģiniet pieslēgt atskaņotāju citā kontaktā.</p>

 <p>Pēdējā lieta, ko pārbaudīt ir tas, vai audio kabelis ir droši iesprausts skaļruņu aizmugurē. Dažiem skaļruņiem var būt vairāk nekā viena ieeja.</p>
</section>

<section id="device">
  <title>Pārbaudiet, vai pareizā skaņas ierīce ir izvēlēta</title>

  <p>Dažiem datoriem ir instalētas vairākas “skaņu ierīces”.  Dažas no tām var atskaņot skaņu, bet dažas nē, tāpēc vajadzētu pārbaudīt, vai ir izvēlēta pareizā skaņas ierīce. Šeit varētu būt nepieciešami vairāki izmēģinājumi, lai atrastu pareizo.</p>

  <steps>
    <item>
      <p>Atveriet <gui xref="shell-introduction#activities">Aktivitāšu</gui> pārskatu un sāciet rakstīt <gui>Skaņa</gui>.</p>
    </item>
    <item>
      <p>Spiediet <gui>Skaņa</gui>, lai atvērtu paneli.</p>
    </item>
    <item>
      <p>Under <gui>Output</gui>, change the <gui>Profile</gui> settings for the
      selected device and play a sound to see if it works. You might need to go
      through the list and try each profile.</p>

      <p>Ja tas nestrādā, vajadzētu to pašu izmēģināt visām pārējām uzskaitītajām ierīcēm.</p>
    </item>
  </steps>

</section>

<section id="hardware-detected">

 <title>Pārbaudiet, vai skaņu karte tika pareizi atpazīta</title>

  <p>Ja skaņu karte nav atpazīta, iespējams, iespējams, ka nav uzinstalēti draiveri. Iespējams, jums vajadzēs ieinstalēt šīs kartes draiverus. Kā tas ir izdarāms, ir atkarīgs no skaņas kartes.</p>

  <p>Palaidiet <cmd>lspci</cmd> komandu no termināļa, lai uzzinātu, kāda jums ir skaņas karte:</p>
  <steps>
    <item>
      <p>Dodieties uz <gui>Aktivitāte</gui> pārskatu un atveriet Termināli.</p>
    </item>
    <item>
      <p>Palaidiet <cmd>lspci</cmd> kā <link xref="user-admin-explain">superlietotājam</link>; cita iespēja ir ierakstīt <cmd>sudo lspci</cmd> un ierakstīt paroli, vai ierakstīt <cmd>su</cmd>, un ievadīt <em>galveno</em> (administratora) paroli, un ierakstīt <cmd>lspci</cmd>.</p>
    </item>
    <item>
      <p>Pārliecinieties, ka <em>audio kontrolieris</em> vai <em>audio ierīce</em> ir sarakstā: ja ir, jābūt redzamam tās markai un modeļa numuram. Kā arī, <cmd>lspci -v</cmd> parāda sarakstu ar detalizētāku informāciju.</p>
    </item>
  </steps>

  <p>Jūs varētu varēt atrast un instalēt savas skaņu kartes draiverus. Vislabāk attiecīgās Linux distribūcijas instalēšanas norādījumus taujāt ir palīdzības forumos (vai citādi).</p>

  <p>Ja nevarat dabūt savas skaņu kartes draiverus, jūs varētu dot priekšroku jaunas skaņu kartes iegādei. Ir pieejamas gan datora iekšējās skaņas kartes, gan ārējās USB skaņas kartes.</p>

</section>

</page>
