<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sound-usespeakers" xml:lang="el">

  <info>
    <link type="guide" xref="media#sound"/>
    <link type="seealso" xref="sound-usemic"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Συνδέστε ηχεία ή ακουστικά και επιλέξτε μια προεπιλεγμένη συσκευή εξόδου ήχου.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Χρησιμοποιήστε διαφορετικά ηχεία ή ακουστικά</title>

  <p>Μπορείτε να χρησιμοποιήσετε εξωτερικά ηχεία ή ακουστικά με τον υπολογιστή σας. Τα ηχεία συνήθως συνδέονται χρησιμοποιώντας έναν κυκλικό σύνδεσμο TRS (<em>άκρο, δακτύλιο</em>) ή ένα USB.</p>

  <p>Εάν τα ηχεία ή ακουστικά σας έχουν μια σύνδεση TRS (γνωστό και ως καρφί), συνδέστε τα στην κατάλληλη υποδοχή του υπολογιστή σας. Οι περισσότεροι υπολογιστές έχουν δύο υποδοχές: μία για μικρόφωνα και μία για ηχεία (συνήθως έχουν και μια φωτογραφία ακουστικών). Τα ηχεία ή ακουστικά που συνδέονται στην κατάλληλη υποδοχή TRS θα χρησιμοποιηθούν συνήθως από προεπιλογή. Εάν όχι, δείτε τις παρακάτω οδηγίες για να επιλέξετε την προεπιλεγμένη συσκευή.</p>

  <p>Μερικοί υπολογιστές υποστηρίζουν πολυκαναλική έξοδο για τον περιφερειακό ήχο. Αυτή συνήθως χρησιμοποιεί πολλαπλούς ρευματολήπτες TRS, που είναι συχνά κωδικοποιημένοι με χρώμα. Αν είσαστε αβέβαιοι ποιοι ρευματολήπτες πηγαίνουν σε ποιες υποδοχές, μπορείτε να δοκιμάσετε την έξοδο ήχου στις ρυθμίσεις ήχου.</p>

  <p>Εάν έχετε ηχεία ή ακουστικά USB, ή αναλογικά ακουστικά συνδεμένα σε μια κάρτα ήχου USB, συνδέστε τα σε οποιαδήποτε θύρα USB. Τα ηχεία USB δρουν ως ξεχωριστές συσκευές ήχου και μπορεί να πρέπει να ορίσετε ποια ηχεία να χρησιμοποιήσετε από προεπιλογή.</p>

  <steps>
    <title>Επιλογή μιας προεπιλεγμένης συσκευής εισόδου ήχου</title>
    <item>
      <p>Ανοίξτε την επισκόπηση <gui xref="shell-introduction#activities">Δραστηριότητες</gui> και αρχίστε να πληκτρολογείτε <gui>Ήχος</gui>.</p>
    </item>
    <item>
      <p>Κάντε κλικ στο <gui>Ήχος</gui> για να ανοίξετε τον πίνακα.</p>
    </item>
    <item>
      <p>Στην καρτέλα <gui>Έξοδος</gui>, επιλέξτε τη συσκευή που θέλετε να χρησιμοποιήσετε.</p>
    </item>
  </steps>

  <p>Χρησιμοποιήστε το κουμπί <gui style="button">Δοκιμή ηχείων</gui> για να ελέγξετε ότι όλα τα ηχεία δουλεύουν και συνδέονται με την σωστή υποδοχή.</p>

</page>
