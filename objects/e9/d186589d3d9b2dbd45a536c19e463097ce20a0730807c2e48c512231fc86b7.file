<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-launch-applications" xml:lang="ja">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>

    <link type="guide" xref="getting-started" group="videos"/>
    <title role="trail" type="link">アプリケーションを起動する</title>
    <link type="seealso" xref="shell-apps-open"/>
    <title role="seealso" type="link">アプリケーションの起動に関するチュートリアル</title>
    <link type="next" xref="gs-switch-tasks"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>松澤 二郎</mal:name>
      <mal:email>jmatsuzawa@gnome.org</mal:email>
      <mal:years>2013, 2015, 2016</mal:years>
    </mal:credit>
  </info>

  <title>アプリケーションを起動する</title>

  <ui:overlay width="812" height="452">
   <media type="video" its:translate="no" src="figures/gnome-launching-applications.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-launching-apps.svg"/>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>アプリケーションの起動</tt:p>
         </tt:div>
         <tt:div begin="5s" end="7.5s">
           <tt:p>マウスポインターを画面左上隅の<gui>アクティビティ</gui>まで移動させます。</tt:p>
           </tt:div>
         <tt:div begin="7.5s" end="9.5s">
           <tt:p><gui>アプリケーションを表示する</gui>アイコンをクリックします。</tt:p>
         </tt:div>
         <tt:div begin="9.5s" end="11s">
           <tt:p>起動するアプリケーション、例えばヘルプ、をクリックします。</tt:p>
         </tt:div>
         <tt:div begin="12s" end="21s">
           <tt:p>他の方法もあります。キーボードから <key href="help:gnome-help/keyboard-key-super">Super</key> キーを押して<gui>アクティビティ画面</gui>を開きます。</tt:p>
         </tt:div>
         <tt:div begin="22s" end="29s">
           <tt:p>起動するアプリケーション名をキーボードでタイプします。</tt:p>
         </tt:div>
         <tt:div begin="30s" end="33s">
           <tt:p><key>Enter</key> を押すと、アプリケーションが起動します。</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
   </media>
  </ui:overlay>

  <section id="launch-apps-mouse">
    <title>マウスを使ってアプリケーションを起動する</title>
 
  <steps>
    <item><p>マウスポインターを画面左上隅の<gui>アクティビティ</gui>まで移動させ、<gui>アクティビティ画面</gui>を開きます。</p></item>
    <item><p>画面左端のバーの末尾にある、<gui>アプリケーションを表示する</gui>アイコンをクリックします。</p></item>
    <item><p>アプリケーションの一覧が表示されます。起動するアプリケーション、例えばヘルプ、をクリックします。</p></item>
  </steps>

  </section>

  <section id="launch-app-keyboard">
    <title>キーボードを使ってアプリケーションを起動する</title>

  <steps>
    <item><p><key href="help:gnome-help/keyboard-key-super">Super</key> キーを押して<gui>アクティビティ画面</gui>を開きます。</p></item>
    <item><p>起動するアプリケーション名をキーボードでタイプします。すぐにアプリケーションの検索が始まります。</p></item>
    <item><p>アプリケーションのアイコンが表示されるので、起動するものを選択します。<key>Enter</key> を押してアプリケーションを起動します。</p></item>
  </steps>

  </section>

</page>
