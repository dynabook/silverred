<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-mode" xml:lang="hu">

  <info>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.28" date="2018-07-22" status="review"/>
    <revision pkgversion="3.33" date="2019-07-21" status="candidate"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>A tábla átváltása táblagép és egér mód közt.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Wacom rajztábla követési módjának beállítása</title>

<p>A <gui>követési mód</gui> meghatározza, hogy a mutató hogyan kerül leképezésre a képernyőre.</p>

<steps>
  <item>
    <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
    overview and start typing <gui>Wacom Tablet</gui>.</p>
  </item>
  <item>
    <p>Click on <gui>Wacom Tablet</gui> to open the panel.</p>
  </item>
  <item>
    <p>Kattintson a <gui>Rajztábla</gui> gombra a fejlécsávon.</p>
    <note style="tip"><p>Ha a rajztábla nem került felismerésre, akkor a program megkéri, hogy <gui>Csatlakoztassa vagy kapcsolja be Wacom rajztábláját</gui>. Kattintson a <gui>Bluetooth beállítások</gui> hivatkozásra a vezeték nélküli rajztábla csatlakoztatásához.</p></note>
  </item>
  <item><p>A <gui>Követési mód</gui> mellett válassza ki a <gui>Rajztábla (abszolút)</gui> vagy az <gui>Érintőtábla (relatív)</gui> lehetőségek egyikét.</p></item>
</steps>

<note style="info"><p><em>Abszolút</em> módban a rajztáblán minden pont egy képernyőpontnak felel meg. A képernyő bal felső sarka például mindig a rajztábla bal felső sarkának felel meg.</p>
 <p>Ha <em>relatív</em> módban felemeli a mutatót a rajztábláról, és máshol teszi le, akkor a kurzor nem mozog a képernyőn. Ez megegyezik az egér működésével.</p>
  </note>

</page>
