<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="look-background" xml:lang="pt">

  <info>
    <link type="guide" xref="prefs-display"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-09" status="review"/>
    <revision pkgversion="3.34" date="2019-11-12" status="review"/>

    <credit type="author">
      <name>Projeto de documentação de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>April Gonzales</name>
      <email>loonycookie@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="editor">
      <name>Michael Hilh</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Set an image as your desktop background or lock screen background.</desc>
  </info>

  <title>Mudar o fundo do escritório e do ecrã de bloqueio</title>

  <p>To change the image used for your backgrounds:</p>

  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Atividades</gui> e comece a escrever <gui>Fundo</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Background</gui> to open the panel. The current
      selections for Background and Lock Screen are shown at the top.</p>
    </item>
    <item>
      <p>There are two ways to change the image used for your backgrounds:</p>
      <list>
        <item>
          <p>Click one of the many professional background images that ship with
          GNOME. You can choose <gui>Set Background</gui>, <gui>Set Lock
          Screen</gui>, or <gui>Set Background and Lock Screen</gui>.</p>
        <note style="info">
          <p>Some wallpapers change throughout the day. These wallpapers have a
          small clock icon in the bottom-right corner.</p>
        </note>
        </item>
        <item>
          <p>Click <gui>Add Picture...</gui> to use one of your own photos from
          your <file>Pictures</file> folder. Most photo management applications
          store photos there.</p>
        </item>
      </list>
    </item>
    <item>
      <p>A configuração aplicar-se-á imediatamente.</p>
        <note style="tip">
          <p>If you would like to use an image that is not in your
          <file>Pictures</file> folder, right-click on the image file in
          <app>Files</app> and select <gui>Set as Wallpaper</gui>, or open the
          image file in <app>Image Viewer</app>, click the menu button in the
          titlebar and select <gui>Set as Wallpaper</gui>. This will affect only
          the desktop background.</p>
        </note>
    </item>
    <item>
      <p><link xref="shell-workspaces-switch">Altere para um área de trabalho esvazia</link> para ver o escritório inteiro.</p>
    </item>
  </steps>

</page>
