<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" id="shell-apps-auto-start" xml:lang="pl">

  <info>
    <link type="guide" xref="shell-overview#desktop"/>

    <revision pkgversion="3.33" date="2019-07-21" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Aruna Sankaranarayanan</name>
      <email>aruna.evam@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <desc>Używanie programu <app>Dostrajanie</app> do automatycznego uruchamiania programów po zalogowaniu.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  </info>

  <title>Włączanie automatycznego uruchamiania programów po zalogowaniu</title>

  <p>Po zalogowaniu komputer automatycznie uruchamia pewne programy, które działają w tle. Są to zwykle ważne programy pomagające w poprawnym działaniu środowiska.</p>

  <p>Można użyć programu <app>Dostrajanie</app>, aby dodać inne często używane programy, takie jak przeglądarka WWW lub edytor, do listy programów automatycznie uruchamianych po zalogowaniu.</p>

  <note style="important">
    <p>Aby zmienić to ustawienie, na komputerze musi być zainstalowany program <app>Dostrajanie</app>.</p>
    <if:if test="action:install">
      <p><link style="button" action="install:gnome-tweaks">Zainstaluj program <app>Dostrajanie</app></link></p>
    </if:if>
  </note>

  <steps>
    <title>Aby automatycznie uruchamiać program po zalogowaniu:</title>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Dostrajanie</gui>.</p>
    </item>
    <item>
      <p>Kliknij ikonę <gui>Dostrajanie</gui>, aby otworzyć program.</p>
    </item>
    <item>
      <p>Kliknij kartę <gui>Programy startowe</gui>.</p>
    </item>
    <item>
      <p>Kliknij przycisk <gui style="button">+</gui>, aby wyświetlić listę dostępnych programów.</p>
    </item>
    <item>
      <p>Kliknij przycisk <gui style="button">Dodaj</gui>, aby dodać wybrany program do listy.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Można usunąć program z listy klikając przycisk <gui style="button">Usuń</gui> obok niego.</p>
  </note>

</page>
