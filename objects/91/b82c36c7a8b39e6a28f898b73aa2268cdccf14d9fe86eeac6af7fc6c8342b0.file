<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="shell-lockscreen" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="shell-overview#apps"/>
    <link type="guide" xref="shell-notifications#lock-screen-notifications"/>

    <revision pkgversion="3.6.1" date="2012-11-11" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>锁屏的功能和画面显示的有用信息。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>The lock screen</title>

  <p>在您的计算机锁定后，锁定屏幕可以指示当前的状态，让您获得一些你离开时发生事情的摘要。锁定状态的屏幕画面显示了清晰的画面，并提供一些有用的信息：</p>

  <list>
    <item><p>当前登录的用户名</p></item>
    <item><p>日期和时间，当前通知。</p></item>
    <item><p>电池和网络状态</p></item>
<!-- No media control anymore on lock screen, see BZ #747787: 
    <item><p>the ability to control media playback — change the volume, skip a
    track or pause your music without having to enter a password</p></item> -->
  </list>

  <p>To unlock your computer, raise the lock screen curtain by dragging it
  upward with the cursor, or by pressing <key>Esc</key> or <key>Enter</key>.
  This will reveal the login screen, where you can enter your password to
  unlock. Alternatively, just start typing your password and the curtain will
  be automatically raised as you type. You can also switch users if your
  computer is configured for more than one.</p>

  <p>To hide notifications from the lock screen, see
  <link xref="shell-notifications#lock-screen-notifications"/>.</p>

</page>
