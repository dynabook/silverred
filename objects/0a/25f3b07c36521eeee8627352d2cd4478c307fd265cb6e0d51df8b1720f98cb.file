<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-inklevel" xml:lang="ta">

  <info>
    <link type="guide" xref="printing"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>அனிதா ரெயித்ரே</name>
      <email>nitalynx@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>அச்சுப்பொறி கேர்ட்ரிட்ஜ்களில் மீதமுள்ள இங்க் அல்லது டோனர் அளவைப் பார்க்கவும்.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>How can I check my printer’s ink or toner levels?</title>

  <p>உங்கள் அச்சுப்பொறியின் இங்க்/டோனர் அளவை எப்படி பார்ப்பது என்பது அதன் நிறுவனம், மாடல் மற்றும் உங்கள் கணினியில் நிறுவியுள்ள பயன்பாடுகள் மற்றும் இயக்கிகளைப் பொறுத்தது.</p>

  <p>சில அச்சுப்பொறிகளில் இங்க் அளவு மற்றும் பிற விவரங்கள் அதிலேயே அமைந்த திரையில் காட்டப்படும்.</p>

  <p>Some printers report toner or ink levels to the computer, which can be
  found in the <gui>Printers</gui> panel in <app>Settings</app>. The ink
  level will be shown with the printer details if it is available.</p>

  <p>பெரும்பாலான HP அச்சுப்பொறிகளுக்கான இயக்கிகள் மற்றும் நிலைக் கருவிகள் HP Linux படமெடுத்தல் மற்றும் அச்சிடல் (HPLIP) திட்டப்பணியால் வழங்கப்படுகின்றன. மற்ற நிறுவனங்கள் அதே போன்ற அம்சங்களைக் கொண்ட உரிமைத் தன்மை கொண்ட இயக்கிகளை அளிக்கலாம்.</p>

  <p>Alternatively, you can install an application to check or monitor
  ink levels. <app>Inkblot</app> shows ink status for many HP, Epson
  and Canon printers. See if your printer is on the <link href="http://libinklevel.sourceforge.net/#supported">list of
  supported models</link>. Another ink levels application for Epson and
  some other printers is <app>mtink</app>.</p>

  <p>சில அச்சுப்பொறிகள் Linux இல் நன்கு ஆதரிக்கப்படாமல் இருக்கலாம், இன்னும் சில இங்க் அளவைத் தெரிவிக்கும் படி வடிவமைக்கப்படாமல் இருக்கலாம்.</p>

</page>
