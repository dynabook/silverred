<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-maximize" xml:lang="hu">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>
    <link type="seealso" xref="shell-windows-tiled"/>

    <revision pkgversion="3.4.0" date="2012-03-14" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Kattintson duplán az ablak címsorára vagy húzza azt az ablak teljes méretűvé tételéhez vagy visszaállításához.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Ablak maximalizálása és visszaállítása</title>

  <p>A maximalizált ablak elfoglalja az asztal egész területét, a visszaállítás a maximalizálás előtti méretre állítja vissza. Az ablakokat függőlegesen is maximalizálhatja a képernyő bal és jobb éle mentén, így egyszerre két ablakot is megjeleníthet. A részletekért lásd a <link xref="shell-windows-tiled"/> részt.</p>

  <p>Ablak maximalizálásához fogja meg a címsorát, és húzza a képernyő tetejére, vagy csak kattintson duplán a címsorára. A billentyűzettel való maximalizálásához tartsa lenyomva a <key xref="keyboard-key-super">Super billentyűt</key>, és nyomja meg a <key>↑</key> billentyűt, vagy használja az <keyseq><key>Alt</key><key>F10</key></keyseq> billentyűkombinációt.</p>

  <p if:test="platform:gnome-classic">Egy ablakot a címsorában lévő maximalizálás gombra kattintva is teljes méretűvé tehet.</p>

  <p>Egy ablak maximalizálás előtti állapotba való visszaállításához húzza azt el a képernyő széleitől. Ha az ablak maximalizálva van, akkor a címsorra való dupla kattintással is visszaállíthatja. Az ablak maximalizálására használt gyorsbillentyűket is használhatja.</p>

  <note style="tip">
    <p>Az ablak mozgatásához tartsa lenyomva a <key>Super</key> billentyűt, és az ablakon belül bárhová kattintva kezdje el húzni azt.</p>
  </note>

</page>
