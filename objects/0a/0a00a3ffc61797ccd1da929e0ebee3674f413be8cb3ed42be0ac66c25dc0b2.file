<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="net-security-tips" xml:lang="ca">

  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.4.0" date="2012-02-21" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Steven Richards</name>
      <email>steven.richardspc@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Consells generals a tenir en compte quan feu servir Internet.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

  <title>Estar segur a Internet</title>

  <p>Una possible raó per la qual utilitza Linux és la seguretat robusta per a la qual es coneix. Una de les raons per les quals Linux és relativament segur de programari maliciós i virus es deu a la menor quantitat de persones que la utilitzen. Els virus estan dirigits a sistemes operatius populars, com Windows, que tenen una base d'usuaris extremadament gran. Linux també és molt segur a causa de la seva naturalesa de codi obert, que permet als experts modificar i millorar les característiques de seguretat incloses en cada distribució.</p>

  <p>Malgrat les mesures preses per garantir que la vostra instal·lació de Linux sigui segura, sempre hi ha vulnerabilitats. Com a usuari mitjà a Internet, encara en podeu ser susceptibles:</p>

  <list>
    <item>
      <p>Les estafes per Phishing (llocs web i correus electrònics que intenten obtenir informació confidencial mitjançant l'engany)</p>
    </item>
    <item>
      <p><link xref="net-email-virus">Reenviament de correus electrònics maliciosos</link></p>
    </item>
    <item>
      <p><link xref="net-antivirus">Aplicacions amb intenció maliciosa (virus)</link></p>
    </item>
    <item>
      <p><link xref="net-wireless-wepwpa">Accés remot no autoritzat a la xarxa local</link></p>
    </item>
  </list>

  <p>Per mantenir-vos segurs en línia, tingueu en compte els següents consells:</p>

  <list>
    <item>
      <p>Aneu amb compte amb els missatges de correu electrònic, els fitxers adjunts o els enllaços que s'envien des de persones que no coneixeu.</p>
    </item>
    <item>
      <p>Si l'oferta d'un lloc web és massa bona per ser veritat, o demana informació sensible que sembla innecessària, llavors penseu-vos dues vegades sobre la informació que envieu i les conseqüències potencials si aquesta informació es veu compromesa pels lladres d'identitat o altres delinqüents.</p>
    </item>
    <item>
      <p>Aneu amb compte a proporcionar <link xref="user-admin-explain">permisos d'administrador</link> a qualsevol aplicació, especialment aquelles que no heu utilitzat anteriorment o que no són conegudes. Proporcionar a qualsevol o a qualsevol altra cosa permisos a nivell d'administrador fa que l'equip tingui un alt risc d'explotació.</p>
    </item>
    <item>
      <p>Assegureu-vos que només executeu els serveis d'accés remot necessaris. Tenir SSH o VNC en funcionament pot ser útil, però també deixa l'ordinador obert a la intrusió si no s'assegura correctament. Penseu en l'ús d'un <link xref="net-firewall-on-off">tallafoc</link> per ajudar-vos a protegir el vostre ordinador contra la intrusió.</p>
    </item>
  </list>

</page>
