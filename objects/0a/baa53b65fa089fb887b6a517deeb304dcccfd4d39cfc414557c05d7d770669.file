<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-check" xml:lang="el">
  <info>
    <link type="guide" xref="disk"/>


    <credit type="author">
      <name>Έργο Τεκμηρίωσης GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.91" date="2014-09-05" status="review"/>

    <desc>Test your hard disk for problems to make sure that it’s healthy.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

<title>Έλεγχος του σκληρού σας δίσκου για προβλήματα</title>

<section id="disk-status">
 <title>Έλεγχος του σκληρού δίσκου</title>
  <p>Οι σκληροί δίσκοι έχουν ένα ενσωματωμένο εργαλείο ελέγχου υγείας που λέγεται <app>SMART</app> (τεχνολογία αυτοπαρακολούθησης, ανάλυσης και αναφοράς), που ελέγχει συνεχώς τον δίσκο για δυνητικά προβλήματα. Το SMART επίσης προειδοποιεί εάν ο δίσκος πρόκειται να αστοχήσει, βοηθώντας την αποφυγή απώλειας σημαντικών δεδομένων.</p>

  <p>Although SMART runs automatically, you can also check your disk’s
 health by running the <app>Disks</app> application:</p>

<steps>
 <title>Check your disk’s health using the Disks application</title>

  <item>
    <p>Ανοίξτε το <app>Δίσκοι</app> από την επισκόπηση <gui>Δραστηριότητες</gui>.</p>
  </item>
  <item>
    <p>Επιλέξτε τον δίσκο που θέλετε να ελέγξετε από την λίστα συσκευών αποθήκευσης στα αριστερά σας. Θα εμφανιστούν οι πληροφορίες και η κατάσταση του δίσκου.</p>
  </item>
  <item>
    <p>Click the menu button and select <gui>SMART Data &amp; Self-Tests…</gui>.
    The <gui>Overall Assessment</gui> should say “Disk is OK”.</p>
  </item>
  <item>
    <p>Δείτε περισσότερες πληροφορίες στο <gui>Χαρακτηριστικά SMART</gui>, ή κάντε κλικ στο πλήκτρο <gui style="button">Έναρξη αυτοδιαγνωστικού ελέγχου</gui> για να εκτελέσετε τον έλεγχο.</p>
  </item>

</steps>

</section>

<section id="disk-not-healthy">

 <title>What if the disk isn’t healthy?</title>

  <p>Even if the <gui>Overall Assessment</gui> indicates that the disk
  <em>isn’t</em> healthy, there may be no cause for alarm. However, it’s better
  to be prepared with a <link xref="backup-why">backup</link> to prevent data
  loss.</p>

  <p>If the status says “Pre-fail”, the disk is still reasonably healthy but
 signs of wear have been detected which mean it might fail in the near future.
 If your hard disk (or computer) is a few years old, you are likely to see
 this message on at least some of the health checks. You should
 <link xref="backup-how">backup your important files regularly</link> and check
 the disk status periodically to see if it gets worse.</p>

  <p>Εάν χειροτερεύσει, ίσως θελήσετε να πάτε τον υπολογιστή/σκληρό δίσκο σε έναν επαγγελματία για παραιτέρω διάγνωση ή επισκευή.</p>

</section>

</page>
