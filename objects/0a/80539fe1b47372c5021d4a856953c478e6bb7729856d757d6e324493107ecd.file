<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="cpu-check" xml:lang="pt-BR">
  <info>
    <revision version="0.1" date="2014-01-28" status="review"/>
    <link type="guide" xref="index#cpu" group="cpu"/>
    <link type="seealso" xref="process-identify-hog"/>
    <link type="seealso" xref="process-priority-change"/>
    <link type="seealso" xref="cpu-mem-normal"/>
    <link type="seealso" xref="cpu-multicore"/>
    
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    
    <credit type="author copyright">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
      <years>2014</years>
    </credit>

    <desc>A quantidade de CPU sendo usada lhe diz quanto trabalho o computador está tentando fazer.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Verificando quanto do processador (CPU) está sendo usada</title>
  
  <p>Para verificar quanto do processador do seu computador está sendo usado, vá na aba <gui>Recursos</gui> e veja o gráfico <gui>Histórico da CPU</gui>. Isso lhe diz quando trabalho seu computador está realizando naquele momento.</p>
  
  <p>Muitos computadores têm mais de um processador (algumas vezes chamados de sistemas <em>multi-core</em>, ou multinúcleos). Cada processador é mostrado separadamente no gráfico. Se você quiser, você pode alterar a cor usada para cada processador clicando em um dos blocos coloridos logo abaixo do gráfico.</p>
  
  <p>Você também pode alterar o quão rápido o gráfico é atualizado (o intervalo de atualização), e como as linhas para cada processador são exibidas (ex.: um gráfico de área empilhada). Altere as opções relevantes clicando em <gui>Monitor de sistema</gui>, selecionando <gui>Preferências</gui> e, então, a aba <gui>Recursos</gui>.</p>

</page>
