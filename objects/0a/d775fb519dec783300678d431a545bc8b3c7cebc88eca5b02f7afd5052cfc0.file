<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="sharing-media" xml:lang="pt">

  <info>
    <link type="guide" xref="sharing"/>
    <link type="guide" xref="prefs-sharing"/>

    <revision pkgversion="3.10" version="0.2" date="2013-11-02" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-13" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <credit type="author">
      <name>Michael Hilh</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Partilhar meios na sua rede local utilizando UPnP.</desc>
  </info>

  <title>Partilhar a sua música, fotos e vídeos</title>

  <p>Pode explorar, procurar e reproduzir os meios na sua computador utilizando um dispositivo que suporte <sys>UPnP</sys> ou <sys>DLNA</sys> como pode ser um telefone, uma televisão ou uma videoconsola. Configure a <gui>Partilha de meios</gui> para permitir a estes dispositivos aceder às diretórios que contêm a sua música, fotos e vídeos.</p>

  <note style="info package">
    <p>Deve ter instalado o pacote <app>Rygel</app> para que a <gui>Partilha de meios</gui> esteja visível.</p>

    <if:choose xmlns:if="http://projectmallard.org/if/1.0/">
      <if:when test="action:install">
        <p><link action="install:rygel" style="button">Instalar Rygel</link></p>
      </if:when>
    </if:choose>
  </note>

  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Atividades</gui> e comece a escrever <gui>Compartição</gui>.</p>
    </item>
    <item>
      <p>Carregue em <gui>Compartição</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>If the <gui>Sharing</gui> switch in the top-right of the window is
      set to off, switch it to on.</p>

      <note style="info"><p>Se pode editar o texto de abaixo de <gui>Nome da computador</gui> pode <link xref="sharing-displayname">mudar</link> o nome da sua computador na rede.</p></note>
    </item>
    <item>
      <p>Selecione <gui>Partilha de meios</gui>.</p>
    </item>
    <item>
      <p>Switch the <gui>Media Sharing</gui> switch to on.</p>
    </item>
    <item>
      <p>By default, <file>Music</file>, <file>Pictures</file> and 
      <file>Videos</file> are shared. To remove one of these, click the
      <gui>×</gui> next to the folder name.</p>
    </item>
    <item>
      <p>Para acrescentar outra diretório, carregue <gui style="button">+</gui> para abrir a janela <gui>Eleger uma diretório</gui>. Navegue <em>dentro</em> da diretório que queira e carregue <gui style="button">Abrir</gui>.</p>
    </item>
    <item>
      <p>Click <gui style="button">×</gui>. You will now be able to browse
      or play media in the folders you selected using the external device.</p>
    </item>
  </steps>

  <section id="networks">
  <title>Redes</title>

  <p>The <gui>Networks</gui> section lists the networks to which you are
  currently connected. Use the switch next to each to choose where your media
  can be shared.</p>

  </section>

</page>
