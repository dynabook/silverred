<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="problem" id="bluetooth-problem-connecting" xml:lang="as">

  <info>
    <link type="guide" xref="bluetooth#problems"/>
    <link type="seealso" xref="hardware-driver"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>ফিল বুল</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>মাইকেল হিল</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>এডাপ্টাৰ বন্ধ থাকিব পাৰে অথবা ড্ৰাইভাৰসমূহ নাথাকিব পাৰে, অথবা ব্লুটুথ অসামৰ্থাব অথবা প্ৰতিৰোধিত থাকিব পাৰে।</desc>
  </info>

  <title>I cannot connect my Bluetooth device</title>

  <p>আপুনি এটা ব্লটুথ ডিভাইচ, যেনে এটা ফোন অথবা হেডছেটলৈ সংযোগ কৰিব নোৱাৰাৰ কেইবাটাও কাৰণ থাকিব পাৰে।</p>

  <terms>
    <item>
      <title>সংযোগ প্ৰতিৰোধিত অথবা ভৰষাহিন</title>
      <p>কিছুমান ব্লুটুথ ডিভাইচে অবিকল্পিতভাৱে সংযোগ প্ৰতিৰোধ কৰে, অথবা আপোনাক সংযোগ স্থাপন কৰিবলৈ আপোনাক এটা সংহতি পৰিবৰ্তন কৰিবলৈ ক'ব পাৰে। আপোনাৰ ডিভাইচ সংযোগৰ অনুমতি দিবলৈ সংস্থাপিত থকাটো সুনিশ্চিত কৰক।</p>
    </item>
    <item>
      <title>ব্লুটুথ হাৰ্ডৱেৰ চিনাক্ত কৰিব নোৱাৰি</title>
      <p>Your Bluetooth adapter or dongle may not have been recognized by the
      computer. This could be because
      <link xref="hardware-driver">drivers</link> for the adapter are not
      installed. Some Bluetooth adapters are not supported on Linux, so you may
      not be able to get the right drivers for them. In this case, you will
      probably have to get a different Bluetooth adapter.</p>
    </item>
    <item>
      <title>Adapter is not switched on</title>
        <p>Make sure that your Bluetooth adapter is switched on. Open the
        Bluetooth panel and check that it is not
        <link xref="bluetooth-turn-on-off">disabled</link>.</p>
    </item>
    <item>
      <title>ডিভাইচ ব্লুটুথ সংযোগ বন্ধ</title>
      <p>Check that Bluetooth is turned on on the device you are trying to
      connect to, and that it is <link xref="bluetooth-visibility">discoverable
      or visible</link>. For example, if you are trying to connect to a phone,
      make sure that it is not in airplane mode.</p>
    </item>
    <item>
      <title>আপোনাৰ কমপিউটাৰত কোনো ব্লুটুথ এডাপ্টাৰ নাই</title>
      <p>বহুতো কমপিউটাৰত ব্লুটুথ এডাপ্টাৰ নাই। আপুনি এটা এডাপ্টাৰ কিনিব পাৰিব যদি আপুনি ব্লটুথ ব্যৱহাৰ কৰিব বিচাৰে।</p>
    </item>
  </terms>

</page>
