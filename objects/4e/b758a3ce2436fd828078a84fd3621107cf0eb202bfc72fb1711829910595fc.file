<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="desktop-lockscreen" xml:lang="hu">

  <info>
    <link type="guide" xref="appearance"/>
    <link type="seealso" xref="dconf-profiles"/>
    <link type="seealso" xref="dconf-lockdown"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

    <desc>A képernyő automatikus zároltatása, így a felhasználónak meg kell adnia egy jelszót, miután tétlen volt.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Úr Balázs</mal:name>
      <mal:email>ur.balazs@fsf.hu</mal:email>
      <mal:years>2018, 2019, 2020.</mal:years>
    </mal:credit>
  </info>

  <title>A képernyő zárolása, ha a felhasználó tétlen</title>

  <p>Automatikusan zároltathatja a képernyőt, amikor a felhasználó valamennyi ideig tétlen. Ez akkor hasznos, ha a felhasználók esetleg felügyelet nélkül hagyják a számítógépeiket nyilvános vagy nem biztonságos helyeken.</p>

  <steps>
    <title>Automatikus képernyőzár engedélyezése</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
    <item>
      <p>Hozza létre az <file>/etc/dconf/db/local.d/00-screensaver</file> kulcsfájlt, hogy információkat biztosítson a <sys>local</sys> adatbázisnak.</p>
      <listing>
      <title><file>/etc/dconf/db/local.d/00-screensaver</file></title>
<code>
# A dconf útvonal megadása
[org/gnome/desktop/session]

# Tétlenség másodperceinek száma, mielőtt a képernyő elsötétül
# Állítsa 0 másodpercre, ha ki szeretné kapcsolni a képernyővédőt.
idle-delay=uint32 180

# A dconf útvonal megadása
[org/gnome/desktop/screensaver]

# A képernyő zárolása, miután a képernyő elsötétült
lock-enabled=true

# A képernyő elsötétítés utáni másodpercek száma, mielőtt a képernyő zárva lesz
lock-delay=uint32 0
</code>
      </listing>
      <p>Fel kell vennie az <code>uint32</code> típust is az egész szám kulcsértéke mellé, amint látható.</p>
    </item>
    <item>
      <p>Hogy megakadályozza a felhasználót a beállítások felülbírálásában, hozza létre az <file>/etc/dconf/db/local.d/locks/screensaver</file> fájlt a következő tartalommal:</p>
      <listing>
      <title><file>/etc/dconf/db/local.db/locks/screensaver</file></title>
<code>
# Asztal képernyővédő beállításainak zárolása
/org/gnome/desktop/session/idle-delay
/org/gnome/desktop/screensaver/lock-delay
</code>
      </listing>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
  </steps>

</page>
