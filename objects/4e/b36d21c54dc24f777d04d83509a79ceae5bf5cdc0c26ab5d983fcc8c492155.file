<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-workspaces-movewindow" xml:lang="fr">

  <info>
    <link type="guide" xref="shell-windows#working-with-workspaces"/>
    <link type="seealso" xref="shell-workspaces"/>

    <revision pkgversion="3.8" version="0.3" date="2013-05-10" status="review"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.35.91" date="2020-02-27" status="candidate"/>

    <credit type="author">
      <name>Le projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Aller dans la vue d'ensemble des <gui>Activités</gui> et déplacer la fenêtre vers un autre espace de travail.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Déplacement d'une fenêtre vers un autre espace de travail</title>

 <if:choose>
 <if:when test="platform:gnome-classic">
  <steps>
    <title>Avec la souris :</title>
    <item>
      <p>Press the button at the bottom left of the screen in the window list.</p>
    </item>
    <item>
      <p>Click and drag the window towards the bottom right of the screen.</p>
    </item>
    <item>
      <p>Drop the window onto one of the workspaces in the <em>workspace
      selector</em> at the right-hand side of the window list. This workspace
      now contains the window you have dropped.</p>
    </item>
  </steps>
 </if:when>
 <if:when test="!platform:gnome-classic">
  <steps>
    <title>Avec la souris :</title>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
      overview.</p>
    </item>
    <item>
      <p>Cliquez et déplacez la fenêtre vers la droite de l'écran.</p>
    </item>
    <item>
      <p>The <em xref="shell-workspaces">workspace selector</em> will
      expand.</p>
    </item>
    <item>
      <p>Déposez la fenêtre sur un espace de travail vide. Cet espace de travail contient maintenant la fenêtre que vous avez déposée et un nouvel espace de travail vide apparaît en bas du <em>sélecteur d'espace de travail</em>.</p>
    </item>
  </steps>
 </if:when>
 </if:choose>

  <steps>
    <title>Avec le clavier :</title>
    <item>
      <p>Select the window that you want to move (for example, using the
      <keyseq><key xref="keyboard-key-super">Super</key><key>Tab</key></keyseq>
      <em xref="shell-windows-switching">window switcher</em>).</p>
    </item>
    <item>
      <p if:test="!platform:gnome-classic">Appuyez sur <keyseq><key>Logo</key><key>Maj</key><key>Page haut</key></keyseq> pour déplacer la fenêtre vers un espace de travail au dessus de l'espace de travail actuel dans le <em>sélecteur d'espace de travail</em>.</p>
      <p if:test="!platform:gnome-classic">Appuyez sur <keyseq><key>Logo</key><key>Maj</key><key>Page bas</key></keyseq> pour déplacer la fenêtre vers un espace de travail en dessous de l'espace de travail actuel dans le <em>sélecteur d'espace de travail</em>.</p>
      <p if:test="platform:gnome-classic">Press <keyseq><key>Shift</key><key>Ctrl</key>
      <key>Alt</key><key>→</key></keyseq> to move the window to a workspace which
      is left of the current workspace on the <em>workspace selector</em>.</p>
      <p if:test="platform:gnome-classic">Press <keyseq><key>Shift</key><key>Ctrl</key>
      <key>Alt</key><key>←</key></keyseq> to move the window to a workspace which
      is right of the current workspace on the <em>workspace selector</em>.</p>
    </item>
  </steps>

</page>
