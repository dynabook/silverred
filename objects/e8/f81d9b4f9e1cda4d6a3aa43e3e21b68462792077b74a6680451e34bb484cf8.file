<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-use-system-search" xml:lang="pt">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>
    <credit type="author">
      <name>Hannie Dumoleyn</name>
    </credit>
    <link type="guide" xref="getting-started" group="tasks"/>
    <title role="trail" type="link">Use a procura do sistema</title>
    <link type="seealso" xref="shell-apps-open"/>
    <title role="seealso" type="link">Um tutorial sobre o uso da procura do sistema</title>
    <link type="next" xref="gs-get-online"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>nunober</mal:name>
      <mal:email>nunober@gmail.com</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Pedro Albuquerque</mal:name>
      <mal:email>palbuquerque73@gmail.com</mal:email>
      <mal:years>2015.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2016.</mal:years>
    </mal:credit>
  </info>

  <title>Use a procura do sistema</title>

    <media its:translate="no" type="image" mime="image/svg" src="gs-search1.svg" width="100%"/>
    
    <steps>
    <item><p>Open the <gui>Activities</gui> overview by clicking <gui>Activities</gui> 
    at the top left of the screen, or by pressing the
    <key href="help:gnome-help/keyboard-key-super">Super</key> key. 
    Start typing to search.</p>
    <p>A medida que escreve, resultados que coincidem com o que digitou aparecerão. O primeiro resultado é sempre realçado e mostrado em cima.</p>
    <p>Prima <key>Enter</key> para alternar para o primeiro resultado realçado.</p></item>
    </steps>
    
    <media its:translate="no" type="image" mime="image/svg" src="gs-search2.svg" width="100%"/>
    <steps style="continues">
      <item><p>Itens que podem aparecer nos resultados de procura incluem:</p>
      <list>
        <item><p>aplicações correspondentes, mostradas no topo dos resultados de procura,</p></item>
        <item><p>definições correspondentes,</p></item>
        <item><p>matching contacts,</p></item>
        <item><p>matching documents,</p></item>
        <item><p>matching calendar,</p></item>
        <item><p>matching calculator,</p></item>
        <item><p>matching software,</p></item>
        <item><p>matching files,</p></item>
        <item><p>matching terminal,</p></item>
        <item><p>matching passwords and keys.</p></item>
      </list>
      </item>
      <item><p>Nos resultados de procura, clique no item para alternar para este.</p>
      <p>Alternativamente, realce um item usando as teclas de setas e prima <key>Enter</key>.</p></item>
    </steps>

    <section id="use-search-inside-applications">
    
      <title>Pesquisa de dentro de aplicações</title>
      
      <p>O sistema procura por resultados agregados a partir de várias aplicações. No lado esquerdo dos resultados de procuras, pode ver ícones de aplicações que forneceram os resultados de procura. Carregue num dos ícones para reiniciar a procura de dentro da aplicação associada com esse ícone. Por só as melhores correspondências serem mostradas no <gui>Panorama de atividades</gui>, procurar de dentro da aplicação pode alcançar melhores resultados de procura.</p>

    </section>

    <section id="use-search-customize">

      <title>Personalizar resultados de procura</title>

      <media its:translate="no" type="image" mime="image/svg" src="gs-search-settings.svg" width="100%"/>

      <note style="important">
      <p>Your computer lets you customize what you want to display in the search
       results in the <gui>Activities Overview</gui>. For example, you can
        choose whether you want to show results for websites, photos, or music.
        </p>
      </note>


      <steps>
        <title>Para personalizar o que é mostrado nos resultados de procura:</title>
        <item><p>Carregue no <gui xref="shell-introduction#yourname">menu de sistema</gui> no lado direito da barra superior.</p></item>
        <item><p>Click <gui>Settings</gui>.</p></item>
        <item><p>Click <gui>Search</gui> in the left panel.</p></item>
        <item><p>In the list of search locations, click the switch next to the
        search location you want to enable or disable.</p></item>
      </steps>

    </section>

</page>
