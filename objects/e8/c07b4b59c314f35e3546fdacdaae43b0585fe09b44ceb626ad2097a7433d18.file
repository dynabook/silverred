<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:ui="http://projectmallard.org/ui/1.0/" type="guide" style="task" id="files-search" xml:lang="mr">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-25" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.34" date="2019-07-20" status="draft"/>

    <credit type="author">
      <name>GNOME डॉक्युमेंटेशन प्रकल्प</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>
    <credit type="editor">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Locate files based on file name and type.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

  <title>फाइल्ससाठी शोधा</title>

  <p>You can search for files based on their name or file type directly
  within the file manager.</p>

  <links type="topic" style="linklist">
    <title>इतर शोध ॲप्लिकेशन्स</title>
    <!-- This is an extension point where search apps can add
    their own topics. It's empty by default. -->
  </links>

  <steps>
    <title>शोधा</title>
    <item>
      <p>Open the <app>Files</app> application from the
      <gui xref="shell-introduction#activities">Activities</gui> overview.</p>
    </item>
    <item>
      <p>ठराविक फोल्डर अंतर्गत आवश्यक फाइल्स आढळल्यास, त्या फोल्डरकडे जा.</p>
    </item>
    <item>
      <p>Type a word or words that you know appear in the file name, and they
      will be shown in the search bar. For example, if you name all your
      invoices with the word “Invoice”, type <input>invoice</input>. Words are
      matched regardless of case.</p>
      <note>
        <p>Instead of typing words directly to bring up the search bar, you
        can click the <media its:translate="no" type="image" mime="image/svg" src="figures/edit-find-symbolic.svg"> <key>Search</key> key symbol
        </media> in the toolbar, or press
        <keyseq><key>Ctrl</key><key>F</key></keyseq>.</p>
      </note>
    </item>
    <item>
      <p>You can narrow your results by date, by file type, and by whether to
      search a file's full text, or to only search for file names.</p>
      <p>To apply filters, select the drop-down menu button to the left of the
      file manager's <media its:translate="no" type="image" mime="image/svg" src="figures/edit-find-symbolic.svg"> <key>Search</key> key symbol</media>
      icon, and choose from the available filters:</p>
      <list>
        <item>
          <p><gui>When</gui>: How far back do you want to search?</p>
        </item>
        <item>
          <p><gui>What</gui>: What is the type of item?</p>
        </item>
        <item>
          <p>Should your search include a full-text search, or search only the
          file names?</p>
        </item>
      </list>
    </item>
    <item>
      <p>To remove a filter, select the <gui>X</gui> beside the filter tag
      that you want to remove.</p>
    </item>
    <item>
      <p>शोध परिणामपासून फाइल्स उघडा, प्रत बनवा, नष्ट करा, किंवा त्यावर कार्य करा, फाइल व्यवस्थापकामधील कोणत्याही फोल्डरपासून कार्य करणे नुरूप.</p>
    </item>
    <item>
      <p>Click the <media its:translate="no" type="image" mime="image/svg" src="figures/edit-find-symbolic.svg"> <key>Search</key> key symbol
      </media> in the toolbar again to exit the search and return to the
      folder.</p>
    </item>
  </steps>

<section id="customize-files-search">
  <title>Customize files search</title>

<p>You may want certain directories to be included or excluded from searches
in the <app>Files</app> application. To customize which directories are
searched:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
      overview and start typing <gui>Search</gui>.</p>
    </item>
    <item>
      <p>Select <guiseq><gui>Settings</gui><gui>Search</gui></guiseq> from the
      results. This will open the <gui>Search Settings</gui> panel.</p>
    </item>
    <item>
      <p>Select the <media its:translate="no" type="image" mime="image/svg" src="figures/emblem-system-symbolic.svg"> <key>Preferences</key> key
      symbol</media> icon from the bottom of the <gui>Search</gui> settings
      panel.</p>
    </item>
  </steps>

<p>This will open a separate settings panel which allows you toggle directory
searches on or off. You can toggle searches on each of the three tabs:</p>

  <list>
    <item>
      <p><gui>Places</gui>: Lists common home directory locations</p>
    </item>
    <item>
      <p><gui>Bookmarks</gui>: Lists directory locations that you have
      bookmarked in the <app>Files</app> application</p>
    </item>
    <item>
      <p><gui>Other</gui>: Lists directory locations that you include via
      the <gui>+</gui> button.</p>
    </item>
  </list>

</section>

</page>
