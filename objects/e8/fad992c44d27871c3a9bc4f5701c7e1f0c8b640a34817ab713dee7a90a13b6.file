<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>audioiirfilter: GStreamer Good Plugins 1.0 Plugins Reference Manual</title>
<meta name="generator" content="DocBook XSL Stylesheets Vsnapshot">
<link rel="home" href="index.html" title="GStreamer Good Plugins 1.0 Plugins Reference Manual">
<link rel="up" href="ch01.html" title="gst-plugins-good Elements">
<link rel="prev" href="gst-plugins-good-plugins-audiofirfilter.html" title="audiofirfilter">
<link rel="next" href="gst-plugins-good-plugins-audioinvert.html" title="audioinvert">
<meta name="generator" content="GTK-Doc V1.32 (XML mode)">
<link rel="stylesheet" href="style.css" type="text/css">
</head>
<body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF">
<table class="navigation" id="top" width="100%" summary="Navigation header" cellpadding="2" cellspacing="5"><tr valign="middle">
<td width="100%" align="left" class="shortcuts">
<a href="#" class="shortcut">Top</a><span id="nav_description">  <span class="dim">|</span> 
                  <a href="#gst-plugins-good-plugins-audioiirfilter.description" class="shortcut">Description</a></span><span id="nav_hierarchy">  <span class="dim">|</span> 
                  <a href="#gst-plugins-good-plugins-audioiirfilter.object-hierarchy" class="shortcut">Object Hierarchy</a></span><span id="nav_properties">  <span class="dim">|</span> 
                  <a href="#gst-plugins-good-plugins-audioiirfilter.properties" class="shortcut">Properties</a></span><span id="nav_signals">  <span class="dim">|</span> 
                  <a href="#gst-plugins-good-plugins-audioiirfilter.signals" class="shortcut">Signals</a></span>
</td>
<td><a accesskey="h" href="index.html"><img src="home.png" width="16" height="16" border="0" alt="Home"></a></td>
<td><a accesskey="u" href="ch01.html"><img src="up.png" width="16" height="16" border="0" alt="Up"></a></td>
<td><a accesskey="p" href="gst-plugins-good-plugins-audiofirfilter.html"><img src="left.png" width="16" height="16" border="0" alt="Prev"></a></td>
<td><a accesskey="n" href="gst-plugins-good-plugins-audioinvert.html"><img src="right.png" width="16" height="16" border="0" alt="Next"></a></td>
</tr></table>
<div class="refentry">
<a name="gst-plugins-good-plugins-audioiirfilter"></a><div class="titlepage"></div>
<div class="refnamediv"><table width="100%"><tr>
<td valign="top">
<h2><span class="refentrytitle"><a name="gst-plugins-good-plugins-audioiirfilter.top_of_page"></a>audioiirfilter</span></h2>
<p>audioiirfilter</p>
</td>
<td class="gallery_image" valign="top" align="right"></td>
</tr></table></div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-audioiirfilter.properties"></a><h2>Properties</h2>
<div class="informaltable"><table class="informaltable" border="0">
<colgroup>
<col width="150px" class="properties_type">
<col width="300px" class="properties_name">
<col width="200px" class="properties_flags">
</colgroup>
<tbody>
<tr>
<td class="property_type">
<span class="type">GValueArray</span> *</td>
<td class="property_name"><a class="link" href="gst-plugins-good-plugins-audioiirfilter.html#GstAudioIIRFilter--a" title="The “a” property">a</a></td>
<td class="property_flags">Read / Write</td>
</tr>
<tr>
<td class="property_type">
<span class="type">GValueArray</span> *</td>
<td class="property_name"><a class="link" href="gst-plugins-good-plugins-audioiirfilter.html#GstAudioIIRFilter--b" title="The “b” property">b</a></td>
<td class="property_flags">Read / Write</td>
</tr>
</tbody>
</table></div>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-audioiirfilter.signals"></a><h2>Signals</h2>
<div class="informaltable"><table class="informaltable" border="0">
<colgroup>
<col width="150px" class="signal_proto_type">
<col width="300px" class="signal_proto_name">
<col width="200px" class="signal_proto_flags">
</colgroup>
<tbody><tr>
<td class="signal_type"><span class="returnvalue">void</span></td>
<td class="signal_name"><a class="link" href="gst-plugins-good-plugins-audioiirfilter.html#GstAudioIIRFilter-rate-changed" title="The “rate-changed” signal">rate-changed</a></td>
<td class="signal_flags">Run Last</td>
</tr></tbody>
</table></div>
</div>
<a name="GstAudioIIRFilter"></a><div class="refsect1">
<a name="gst-plugins-good-plugins-audioiirfilter.other"></a><h2>Types and Values</h2>
<div class="informaltable"><table class="informaltable" width="100%" border="0">
<colgroup>
<col width="150px" class="other_proto_type">
<col class="other_proto_name">
</colgroup>
<tbody><tr>
<td class="datatype_keyword">struct</td>
<td class="function_name"><a class="link" href="gst-plugins-good-plugins-audioiirfilter.html#GstAudioIIRFilter-struct" title="struct GstAudioIIRFilter">GstAudioIIRFilter</a></td>
</tr></tbody>
</table></div>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-audioiirfilter.object-hierarchy"></a><h2>Object Hierarchy</h2>
<pre class="screen">    GObject
    <span class="lineart">╰──</span> GInitiallyUnowned
        <span class="lineart">╰──</span> GstObject
            <span class="lineart">╰──</span> GstElement
                <span class="lineart">╰──</span> GstBaseTransform
                    <span class="lineart">╰──</span> GstAudioFilter
                        <span class="lineart">╰──</span> GstAudioFXBaseIIRFilter
                            <span class="lineart">╰──</span> GstAudioIIRFilter
</pre>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-audioiirfilter.description"></a><h2>Description</h2>
<p>audioiirfilter implements a generic audio <a class="ulink" href="http://en.wikipedia.org/wiki/Infinite_impulse_response" target="_top">IIR filter</a>. Before usage the
"a" and "b" properties have to be set to the filter coefficients that
should be used.</p>
<p>The filter coefficients describe the numerator and denominator of the
transfer function.</p>
<p>To change the filter coefficients whenever the sampling rate changes the
"rate-changed" signal can be used. This should be done for most
IIR filters as they're depending on the sampling rate.</p>
<div class="refsect2">
<a name="id-1.2.22.8.5"></a><h3>Example application</h3>
<div class="informalexample">
  <table class="listing_frame" border="0" cellpadding="0" cellspacing="0">
    <tbody>
      <tr>
        <td class="listing_lines" align="right"><pre>1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26
27
28
29
30
31
32
33
34
35
36
37
38
39
40
41
42
43
44
45
46
47
48
49
50
51
52
53
54
55
56
57
58
59
60
61
62
63
64
65
66
67
68
69
70
71
72
73
74
75
76
77
78
79
80
81
82
83
84
85
86
87
88
89
90
91
92
93
94
95
96
97
98
99
100
101
102
103
104
105
106
107
108
109
110
111
112
113
114
115
116
117
118
119
120
121
122
123
124
125
126
127
128
129
130
131
132
133
134
135
136
137
138
139
140
141</pre></td>
        <td class="listing_code"><pre class="programlisting"><span class="cm">/* GStreamer</span>
<span class="cm"> * Copyright (C) 2009 Sebastian Droege &lt;sebastian.droege@collabora.co.uk&gt;</span>
<span class="cm"> *</span>
<span class="cm"> * This library is free software; you can redistribute it and/or</span>
<span class="cm"> * modify it under the terms of the GNU Library General Public</span>
<span class="cm"> * License as published by the Free Software Foundation; either</span>
<span class="cm"> * version 2 of the License, or (at your option) any later version.</span>
<span class="cm"> *</span>
<span class="cm"> * This library is distributed in the hope that it will be useful,</span>
<span class="cm"> * but WITHOUT ANY WARRANTY; without even the implied warranty of</span>
<span class="cm"> * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU</span>
<span class="cm"> * Library General Public License for more details.</span>
<span class="cm"> *</span>
<span class="cm"> * You should have received a copy of the GNU Library General Public</span>
<span class="cm"> * License along with this library; if not, write to the</span>
<span class="cm"> * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,</span>
<span class="cm"> * Boston, MA 02110-1301, USA.</span>
<span class="cm"> */</span>

<span class="cm">/* This small sample application creates a lowpass IIR filter</span>
<span class="cm"> * and applies it to white noise.</span>
<span class="cm"> * See http://www.dspguide.com/ch19/2.htm for a description</span>
<span class="cm"> * of the IIR filter that is used.</span>
<span class="cm"> */</span>

<span class="cm">/* FIXME 0.11: suppress warnings for deprecated API such as GValueArray</span>
<span class="cm"> * with newer GLib versions (&gt;= 2.31.0) */</span>
<span class="cp">#define GLIB_DISABLE_DEPRECATION_WARNINGS</span>

<span class="cp">#include</span> <span class="cpf">&lt;string.h&gt;</span><span class="cp"></span>
<span class="cp">#include</span> <span class="cpf">&lt;math.h&gt;</span><span class="cp"></span>

<span class="cp">#include</span> <span class="cpf">&lt;gst/gst.h&gt;</span><span class="cp"></span>

<span class="cm">/* Cutoff of 4000 Hz */</span>
<span class="cp">#define CUTOFF (4000.0)</span>

<span class="k">static</span> <span class="n">gboolean</span>
<span class="nf">on_message</span> <span class="p">(</span><span class="n">GstBus</span> <span class="o">*</span> <span class="n">bus</span><span class="p">,</span> <span class="n">GstMessage</span> <span class="o">*</span> <span class="n">message</span><span class="p">,</span> <span class="n">gpointer</span> <span class="n">user_data</span><span class="p">)</span>
<span class="p">{</span>
  <span class="n">GMainLoop</span> <span class="o">*</span><span class="n">loop</span> <span class="o">=</span> <span class="p">(</span><span class="n">GMainLoop</span> <span class="o">*</span><span class="p">)</span> <span class="n">user_data</span><span class="p">;</span>

  <span class="k">switch</span> <span class="p">(</span><span class="n">GST_MESSAGE_TYPE</span> <span class="p">(</span><span class="n">message</span><span class="p">))</span> <span class="p">{</span>
    <span class="k">case</span> <span class="nl">GST_MESSAGE_ERROR</span><span class="p">:</span>
      <span class="n">g_error</span> <span class="p">(</span><span class="s">&quot;Got ERROR&quot;</span><span class="p">);</span>
      <span class="n">g_main_loop_quit</span> <span class="p">(</span><span class="n">loop</span><span class="p">);</span>
      <span class="k">break</span><span class="p">;</span>
    <span class="k">case</span> <span class="nl">GST_MESSAGE_WARNING</span><span class="p">:</span>
      <span class="n">g_warning</span> <span class="p">(</span><span class="s">&quot;Got WARNING&quot;</span><span class="p">);</span>
      <span class="n">g_main_loop_quit</span> <span class="p">(</span><span class="n">loop</span><span class="p">);</span>
      <span class="k">break</span><span class="p">;</span>
    <span class="k">case</span> <span class="nl">GST_MESSAGE_EOS</span><span class="p">:</span>
      <span class="n">g_main_loop_quit</span> <span class="p">(</span><span class="n">loop</span><span class="p">);</span>
      <span class="k">break</span><span class="p">;</span>
    <span class="k">default</span><span class="o">:</span>
      <span class="k">break</span><span class="p">;</span>
  <span class="p">}</span>

  <span class="k">return</span> <span class="n">TRUE</span><span class="p">;</span>
<span class="p">}</span>

<span class="k">static</span> <span class="kt">void</span>
<span class="nf">on_rate_changed</span> <span class="p">(</span><span class="n">GstElement</span> <span class="o">*</span> <span class="n">element</span><span class="p">,</span> <span class="n">gint</span> <span class="n">rate</span><span class="p">,</span> <span class="n">gpointer</span> <span class="n">user_data</span><span class="p">)</span>
<span class="p">{</span>
  <span class="n">GValueArray</span> <span class="o">*</span><span class="n">va</span><span class="p">;</span>
  <span class="n">GValue</span> <span class="n">v</span> <span class="o">=</span> <span class="p">{</span> <span class="mi">0</span><span class="p">,</span> <span class="p">};</span>
  <span class="n">gdouble</span> <span class="n">x</span><span class="p">;</span>

  <span class="k">if</span> <span class="p">(</span><span class="n">rate</span> <span class="o">/</span> <span class="mf">2.0</span> <span class="o">&gt;</span> <span class="n">CUTOFF</span><span class="p">)</span>
    <span class="n">x</span> <span class="o">=</span> <span class="n">exp</span> <span class="p">(</span><span class="o">-</span><span class="mf">2.0</span> <span class="o">*</span> <span class="n">G_PI</span> <span class="o">*</span> <span class="p">(</span><span class="n">CUTOFF</span> <span class="o">/</span> <span class="n">rate</span><span class="p">));</span>
  <span class="k">else</span>
    <span class="n">x</span> <span class="o">=</span> <span class="mf">0.0</span><span class="p">;</span>

  <span class="n">va</span> <span class="o">=</span> <span class="n">g_value_array_new</span> <span class="p">(</span><span class="mi">1</span><span class="p">);</span>

  <span class="n">g_value_init</span> <span class="p">(</span><span class="o">&amp;</span><span class="n">v</span><span class="p">,</span> <span class="n">G_TYPE_DOUBLE</span><span class="p">);</span>
  <span class="n">g_value_set_double</span> <span class="p">(</span><span class="o">&amp;</span><span class="n">v</span><span class="p">,</span> <span class="mf">1.0</span> <span class="o">-</span> <span class="n">x</span><span class="p">);</span>
  <span class="n">g_value_array_append</span> <span class="p">(</span><span class="n">va</span><span class="p">,</span> <span class="o">&amp;</span><span class="n">v</span><span class="p">);</span>
  <span class="n">g_value_reset</span> <span class="p">(</span><span class="o">&amp;</span><span class="n">v</span><span class="p">);</span>
  <span class="n">g_object_set</span> <span class="p">(</span><span class="n">G_OBJECT</span> <span class="p">(</span><span class="n">element</span><span class="p">),</span> <span class="s">&quot;a&quot;</span><span class="p">,</span> <span class="n">va</span><span class="p">,</span> <span class="nb">NULL</span><span class="p">);</span>
  <span class="n">g_value_array_free</span> <span class="p">(</span><span class="n">va</span><span class="p">);</span>

  <span class="n">va</span> <span class="o">=</span> <span class="n">g_value_array_new</span> <span class="p">(</span><span class="mi">1</span><span class="p">);</span>
  <span class="n">g_value_set_double</span> <span class="p">(</span><span class="o">&amp;</span><span class="n">v</span><span class="p">,</span> <span class="n">x</span><span class="p">);</span>
  <span class="n">g_value_array_append</span> <span class="p">(</span><span class="n">va</span><span class="p">,</span> <span class="o">&amp;</span><span class="n">v</span><span class="p">);</span>
  <span class="n">g_value_reset</span> <span class="p">(</span><span class="o">&amp;</span><span class="n">v</span><span class="p">);</span>
  <span class="n">g_object_set</span> <span class="p">(</span><span class="n">G_OBJECT</span> <span class="p">(</span><span class="n">element</span><span class="p">),</span> <span class="s">&quot;b&quot;</span><span class="p">,</span> <span class="n">va</span><span class="p">,</span> <span class="nb">NULL</span><span class="p">);</span>
  <span class="n">g_value_array_free</span> <span class="p">(</span><span class="n">va</span><span class="p">);</span>
<span class="p">}</span>

<span class="n">gint</span>
<span class="nf">main</span> <span class="p">(</span><span class="n">gint</span> <span class="n">argc</span><span class="p">,</span> <span class="n">gchar</span> <span class="o">*</span> <span class="n">argv</span><span class="p">[])</span>
<span class="p">{</span>
  <span class="n">GstElement</span> <span class="o">*</span><span class="n">pipeline</span><span class="p">,</span> <span class="o">*</span><span class="n">src</span><span class="p">,</span> <span class="o">*</span><span class="n">filter</span><span class="p">,</span> <span class="o">*</span><span class="n">conv</span><span class="p">,</span> <span class="o">*</span><span class="n">sink</span><span class="p">;</span>
  <span class="n">GstBus</span> <span class="o">*</span><span class="n">bus</span><span class="p">;</span>
  <span class="n">GMainLoop</span> <span class="o">*</span><span class="n">loop</span><span class="p">;</span>

  <span class="n">gst_init</span> <span class="p">(</span><span class="nb">NULL</span><span class="p">,</span> <span class="nb">NULL</span><span class="p">);</span>

  <span class="n">pipeline</span> <span class="o">=</span> <span class="n">gst_element_factory_make</span> <span class="p">(</span><span class="s">&quot;pipeline&quot;</span><span class="p">,</span> <span class="nb">NULL</span><span class="p">);</span>

  <span class="n">src</span> <span class="o">=</span> <span class="n">gst_element_factory_make</span> <span class="p">(</span><span class="s">&quot;audiotestsrc&quot;</span><span class="p">,</span> <span class="nb">NULL</span><span class="p">);</span>
  <span class="n">g_object_set</span> <span class="p">(</span><span class="n">G_OBJECT</span> <span class="p">(</span><span class="n">src</span><span class="p">),</span> <span class="s">&quot;wave&quot;</span><span class="p">,</span> <span class="mi">5</span><span class="p">,</span> <span class="nb">NULL</span><span class="p">);</span>

  <span class="n">filter</span> <span class="o">=</span> <span class="n">gst_element_factory_make</span> <span class="p">(</span><span class="s">&quot;audioiirfilter&quot;</span><span class="p">,</span> <span class="nb">NULL</span><span class="p">);</span>
  <span class="n">g_signal_connect</span> <span class="p">(</span><span class="n">G_OBJECT</span> <span class="p">(</span><span class="n">filter</span><span class="p">),</span> <span class="s">&quot;rate-changed&quot;</span><span class="p">,</span>
      <span class="n">G_CALLBACK</span> <span class="p">(</span><span class="n">on_rate_changed</span><span class="p">),</span> <span class="nb">NULL</span><span class="p">);</span>

  <span class="n">conv</span> <span class="o">=</span> <span class="n">gst_element_factory_make</span> <span class="p">(</span><span class="s">&quot;audioconvert&quot;</span><span class="p">,</span> <span class="nb">NULL</span><span class="p">);</span>

  <span class="n">sink</span> <span class="o">=</span> <span class="n">gst_element_factory_make</span> <span class="p">(</span><span class="s">&quot;autoaudiosink&quot;</span><span class="p">,</span> <span class="nb">NULL</span><span class="p">);</span>
  <span class="n">g_return_val_if_fail</span> <span class="p">(</span><span class="n">sink</span> <span class="o">!=</span> <span class="nb">NULL</span><span class="p">,</span> <span class="o">-</span><span class="mi">1</span><span class="p">);</span>

  <span class="n">gst_bin_add_many</span> <span class="p">(</span><span class="n">GST_BIN</span> <span class="p">(</span><span class="n">pipeline</span><span class="p">),</span> <span class="n">src</span><span class="p">,</span> <span class="n">filter</span><span class="p">,</span> <span class="n">conv</span><span class="p">,</span> <span class="n">sink</span><span class="p">,</span> <span class="nb">NULL</span><span class="p">);</span>
  <span class="k">if</span> <span class="p">(</span><span class="o">!</span><span class="n">gst_element_link_many</span> <span class="p">(</span><span class="n">src</span><span class="p">,</span> <span class="n">filter</span><span class="p">,</span> <span class="n">conv</span><span class="p">,</span> <span class="n">sink</span><span class="p">,</span> <span class="nb">NULL</span><span class="p">))</span> <span class="p">{</span>
    <span class="n">g_error</span> <span class="p">(</span><span class="s">&quot;Failed to link elements&quot;</span><span class="p">);</span>
    <span class="k">return</span> <span class="o">-</span><span class="mi">2</span><span class="p">;</span>
  <span class="p">}</span>

  <span class="n">loop</span> <span class="o">=</span> <span class="n">g_main_loop_new</span> <span class="p">(</span><span class="nb">NULL</span><span class="p">,</span> <span class="n">FALSE</span><span class="p">);</span>

  <span class="n">bus</span> <span class="o">=</span> <span class="n">gst_pipeline_get_bus</span> <span class="p">(</span><span class="n">GST_PIPELINE</span> <span class="p">(</span><span class="n">pipeline</span><span class="p">));</span>
  <span class="n">gst_bus_add_signal_watch</span> <span class="p">(</span><span class="n">bus</span><span class="p">);</span>
  <span class="n">g_signal_connect</span> <span class="p">(</span><span class="n">G_OBJECT</span> <span class="p">(</span><span class="n">bus</span><span class="p">),</span> <span class="s">&quot;message&quot;</span><span class="p">,</span> <span class="n">G_CALLBACK</span> <span class="p">(</span><span class="n">on_message</span><span class="p">),</span> <span class="n">loop</span><span class="p">);</span>
  <span class="n">gst_object_unref</span> <span class="p">(</span><span class="n">GST_OBJECT</span> <span class="p">(</span><span class="n">bus</span><span class="p">));</span>

  <span class="k">if</span> <span class="p">(</span><span class="n">gst_element_set_state</span> <span class="p">(</span><span class="n">pipeline</span><span class="p">,</span>
          <span class="n">GST_STATE_PLAYING</span><span class="p">)</span> <span class="o">==</span> <span class="n">GST_STATE_CHANGE_FAILURE</span><span class="p">)</span> <span class="p">{</span>
    <span class="n">g_error</span> <span class="p">(</span><span class="s">&quot;Failed to go into PLAYING state&quot;</span><span class="p">);</span>
    <span class="k">return</span> <span class="o">-</span><span class="mi">3</span><span class="p">;</span>
  <span class="p">}</span>

  <span class="n">g_main_loop_run</span> <span class="p">(</span><span class="n">loop</span><span class="p">);</span>

  <span class="n">gst_element_set_state</span> <span class="p">(</span><span class="n">pipeline</span><span class="p">,</span> <span class="n">GST_STATE_NULL</span><span class="p">);</span>

  <span class="n">g_main_loop_unref</span> <span class="p">(</span><span class="n">loop</span><span class="p">);</span>
  <span class="n">gst_object_unref</span> <span class="p">(</span><span class="n">pipeline</span><span class="p">);</span>

  <span class="k">return</span> <span class="mi">0</span><span class="p">;</span>
<span class="p">}</span></pre></td>
      </tr>
    </tbody>
  </table>
</div>

</div>
<div class="refsynopsisdiv">
<h2>Synopsis</h2>
<div class="refsect2">
<a name="id-1.2.22.8.6.1"></a><h3>Element Information</h3>
<div class="variablelist"><table border="0" class="variablelist">
<colgroup>
<col align="left" valign="top">
<col>
</colgroup>
<tbody>
<tr>
<td><p><span class="term">plugin</span></p></td>
<td>
            <a class="link" href="gst-plugins-good-plugins-plugin-audiofx.html#plugin-audiofx">audiofx</a>
          </td>
</tr>
<tr>
<td><p><span class="term">author</span></p></td>
<td>Sebastian Dröge &lt;sebastian.droege@collabora.co.uk&gt;</td>
</tr>
<tr>
<td><p><span class="term">class</span></p></td>
<td>Filter/Effect/Audio</td>
</tr>
</tbody>
</table></div>
</div>
<hr>
<div class="refsect2">
<a name="id-1.2.22.8.6.2"></a><h3>Element Pads</h3>
<div class="variablelist"><table border="0" class="variablelist">
<colgroup>
<col align="left" valign="top">
<col>
</colgroup>
<tbody>
<tr>
<td><p><span class="term">name</span></p></td>
<td>sink</td>
</tr>
<tr>
<td><p><span class="term">direction</span></p></td>
<td>sink</td>
</tr>
<tr>
<td><p><span class="term">presence</span></p></td>
<td>always</td>
</tr>
<tr>
<td><p><span class="term">details</span></p></td>
<td>audio/x-raw, format=(string){ F32LE, F64LE }, rate=(int)[ 1, 2147483647 ], channels=(int)[ 1, 2147483647 ], layout=(string)interleaved</td>
</tr>
</tbody>
</table></div>
<div class="variablelist"><table border="0" class="variablelist">
<colgroup>
<col align="left" valign="top">
<col>
</colgroup>
<tbody>
<tr>
<td><p><span class="term">name</span></p></td>
<td>src</td>
</tr>
<tr>
<td><p><span class="term">direction</span></p></td>
<td>source</td>
</tr>
<tr>
<td><p><span class="term">presence</span></p></td>
<td>always</td>
</tr>
<tr>
<td><p><span class="term">details</span></p></td>
<td>audio/x-raw, format=(string){ F32LE, F64LE }, rate=(int)[ 1, 2147483647 ], channels=(int)[ 1, 2147483647 ], layout=(string)interleaved</td>
</tr>
</tbody>
</table></div>
</div>
</div>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-audioiirfilter.functions_details"></a><h2>Functions</h2>
<p></p>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-audioiirfilter.other_details"></a><h2>Types and Values</h2>
<div class="refsect2">
<a name="GstAudioIIRFilter-struct"></a><h3>struct GstAudioIIRFilter</h3>
<pre class="programlisting">struct GstAudioIIRFilter;</pre>
<p>Opaque data structure.</p>
</div>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-audioiirfilter.property-details"></a><h2>Property Details</h2>
<div class="refsect2">
<a name="GstAudioIIRFilter--a"></a><h3>The <code class="literal">“a”</code> property</h3>
<pre class="programlisting">  “a”                        <span class="type">GValueArray</span> *</pre>
<p>Filter coefficients (denominator of transfer function).</p>
<p>Owner: GstAudioIIRFilter</p>
<p>Flags: Read / Write</p>
</div>
<hr>
<div class="refsect2">
<a name="GstAudioIIRFilter--b"></a><h3>The <code class="literal">“b”</code> property</h3>
<pre class="programlisting">  “b”                        <span class="type">GValueArray</span> *</pre>
<p>Filter coefficients (numerator of transfer function).</p>
<p>Owner: GstAudioIIRFilter</p>
<p>Flags: Read / Write</p>
</div>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-audioiirfilter.signal-details"></a><h2>Signal Details</h2>
<div class="refsect2">
<a name="GstAudioIIRFilter-rate-changed"></a><h3>The <code class="literal">“rate-changed”</code> signal</h3>
<pre class="programlisting"><span class="returnvalue">void</span>
user_function (<a class="link" href="gst-plugins-good-plugins-audioiirfilter.html#GstAudioIIRFilter"><span class="type">GstAudioIIRFilter</span></a> *filter,
               <span class="type">gint</span>               rate,
               <span class="type">gpointer</span>           user_data)</pre>
<p>Will be emitted when the sampling rate changes. The callbacks
will be called from the streaming thread and processing will
stop until the event is handled.</p>
<div class="refsect3">
<a name="GstAudioIIRFilter-rate-changed.parameters"></a><h4>Parameters</h4>
<div class="informaltable"><table class="informaltable" width="100%" border="0">
<colgroup>
<col width="150px" class="parameters_name">
<col class="parameters_description">
<col width="200px" class="parameters_annotations">
</colgroup>
<tbody>
<tr>
<td class="parameter_name"><p>filter</p></td>
<td class="parameter_description"><p>the filter on which the signal is emitted</p></td>
<td class="parameter_annotations"> </td>
</tr>
<tr>
<td class="parameter_name"><p>rate</p></td>
<td class="parameter_description"><p>the new sampling rate</p></td>
<td class="parameter_annotations"> </td>
</tr>
<tr>
<td class="parameter_name"><p>user_data</p></td>
<td class="parameter_description"><p>user data set when the signal handler was connected.</p></td>
<td class="parameter_annotations"> </td>
</tr>
</tbody>
</table></div>
</div>
<p>Flags: Run Last</p>
</div>
</div>
</div>
<div class="footer">
<hr>Generated by GTK-Doc V1.32</div>
</body>
</html>