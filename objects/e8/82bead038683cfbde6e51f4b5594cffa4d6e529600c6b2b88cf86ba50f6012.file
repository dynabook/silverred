<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-apps-favorites" xml:lang="da">

  <info>
    <link type="seealso" xref="shell-apps-open"/>
    <link type="guide" xref="shell-overview#desktop"/>

    <revision pkgversion="3.6.0" date="2012-10-14" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>GNOMEs dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Tilføj (eller fjern) programikoner som ofte bruges i favoritområdet.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years/>
    </mal:credit>
  </info>

  <title>Fastgør dine favoritprogrammer til favoritområdet</title>

  <p>Tilføj et program til <link xref="shell-introduction#activities">favoritområdet</link> for let adgang:</p>

  <steps>
    <item>
      <p if:test="!platform:gnome-classic">Åbn <gui xref="shell-introduction#activities">Aktivitetsoversigten</gui> ved at klikke på <gui>Aktiviteter</gui> øverst til venstre på skærmen</p>
      <p if:test="platform:gnome-classic">Klik på <gui xref="shell-introduction#activities">Programmer</gui>-menuen øverst til venstre på skærmen og vælg <gui>Aktivitetsoversigt</gui> fra menuen.</p></item>
    <item>
      <p>Klik på gitterknappen i favoritområdet og find det program, du vil tilføje.</p>
    </item>
    <item>
      <p>Højreklik på programikonet og vælg <gui>Føj til favoritter</gui>.</p>
      <p>Alternativt kan du klikke-og-trække ikonet til favoritområdet.</p>
    </item>
  </steps>

  <p>For at fjerne et programikon fra favoritområdet skal du højreklikke på programikonet og vælge <gui>Fjern fra favoritter</gui>.</p>

  <note style="tip" if:test="platform:gnome-classic"><p>Favoritprogrammer vises også i afsnittet <gui>Favoritter</gui> i <gui xref="shell-introduction#activities">Programmer</gui>-menuen.</p>
  </note>

</page>
