<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-purge" xml:lang="pt">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.10" date="2013-09-29" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <credit type="author">
      <name>Jim Campbelh</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hilh</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shaun McCance</name>
      <email>mdhillca@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Estabelecer com que frequência se limpam a caixote do lixo e os ficheiros temporários da sua computador.</desc>
  </info>

  <title>Limpar a caixote do lixo e os ficheiros temporários</title>

  <p>Limpar a caixote do lixo e os ficheiros temporários elimina ficheiros desnecessários da computador, e também liberta espaço no disco duro. Pode esvaziar a caixote do lixo e limpar os ficheiros temporários manualmente, mas também pode configurar a computador para que o faça automaticamente.</p>

  <p>Os ficheiros temporários são ficheiros criados automaticamente pelos aplicações em segundo plano. Podem melhorar o rendimento, proporcionando uma copia de dados que se tenham descarregado ou processado.</p>

  <steps>
    <title>Esvaziar automaticamente a caixote do lixo e limpar os ficheiros temporários</title>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Atividades</gui> comece a escrever <gui>Privacidade</gui>.</p>
    </item>
    <item>
      <p>Carregue em <gui>Privacidade</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Selecione <gui>Limpar a caixote do lixo e os ficheiros temporários</gui>.</p>
    </item>
    <item>
      <p>Switch one or both of the <gui>Automatically empty Trash</gui> or
      <gui>Automatically purge Temporary Files</gui> switches to on.</p>
    </item>
    <item>
      <p>Estabeleça a frequência com a que quer que se limpem a <em>Papelera</em> e os <em>Ficheiros temporários</em> mudando o valor de <gui>Limpar depois</gui>.</p>
    </item>
    <item>
      <p>Use os <gui>Esvaziar caixote do lixo</gui> ou <gui>Limpar ficheiros temporários</gui> para realizar estas ações imediatamente.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Pode eliminar ficheiros imediata e permanentemente se usar a caixote do lixo. Consulte a <link xref="files-delete#permanent"/> para obter mais informação.</p>
  </note>

</page>
