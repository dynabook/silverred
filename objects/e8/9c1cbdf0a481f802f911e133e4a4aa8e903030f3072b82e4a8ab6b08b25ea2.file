<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="printing-envelopes" xml:lang="cs">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Zkontrolujte, že máte obálku správnou stranou nahoru a zvolte správný formát papíru.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Tisk obálek</title>

  <p>Většina tiskáren umí tisknout přímo na obálky. To se hlavně hodí, když máte na poslání hodně dopisů.</p>

  <section id="envelope">
    <title>Tisk na obálky</title>

  <p>Při tisku na obálky byste měli zkontrolovat dvě věci.</p>
  <p>Za prvé, jestli vaše tiskárna zná danou velikost obálky. Zmáčknutím <keyseq><key>Ctrl</key><key>P</key></keyseq> otevřete dialogové okno <gui>Tisk</gui>, přejděte na kartu <gui>Nastavení stránky</gui> a jestli můžete, zvolte jako <gui>Typ papíru</gui> „Obálka“. Pokud to udělat nejde, podívejte se, jestli můžete změnit <gui>Velikost papíru</gui> na příslušnou velikost obálky (např. <gui>C5</gui>). Velikost obálek by měla být uvedena na jejich balení. Obálky mívají standardizované velikosti, podobně jako papíry.</p>

  <p>Zadruhé je třeba se ujistit, že se obálka podá do tiskárny správně otočená. Jak ji správně do podavače vložit se podívejte do příručky k tiskárně, nebo s jednou obálku odzkoušejte, která strana se potiskne, abyste viděli, jak ji otočit.</p>

  <note style="warning">
    <p>Některé tiskárny nejsou zkonstruovány k tomu, aby tisky na obálky, zejména některé laserové tiskárny. Podívejte se do příručky k dané tiskárně, zda obálky přijímá. Pokud ne, můžete tiskárnu podáním obálky poškodit.</p>
  </note>

  </section>

<!--
TODO: Please write this section!

<section id="labels">
 <title>Printing labels</title>

</section>
-->

</page>
