<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="power-nowireless" xml:lang="cs">

  <info>
    <link type="guide" xref="power#problems"/>
    <link type="seealso" xref="power-suspendfail"/>
    <link type="seealso" xref="hardware-driver"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Některá bezdrátová zařízení mají problém s přechodem počítače do stavu spánku a nesprávně pak obnoví svou činnost.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Po probuzení počítače nemám žádnou bezdrátovou síť</title>

  <p>Když počítač uspíte do paměti, můžete shledat, že po jeho opětovném probuzení nefunguje bezdrátové připojení k Internetu. To se stává, když <link xref="hardware-driver">ovladač</link> bezdrátového zařízení plně nepodporuje některé funkce šetření energií. Typicky se to projeví právě selháním připojení po probuzení počítače.</p>

  <p>Když se to stane, zkuste bezdrátové funkce vypnout a znovu zapnout:</p>

  <steps>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Wi-Fi</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Wi-Fi</gui> otevřete příslušný panel.</p>
    </item>
    <item>
      <p>Přepněte vypínač <gui>Wi-Fi</gui> v pravé horní části okna do polohy vypnuto a pak znovu do polohy zapnuto.</p>
    </item>
    <item>
      <p>Jestliže bezdrátové funkce stále nefungují, přepněte vypínač <gui>Režim „letadlo“</gui> do polohy zapnuto a pak opět do polohy vypnuto.</p>
    </item>
  </steps>

  <p>Pokud to nepomůže, může bezdrátové funkce rozchodit restartování počítače. Pokud i po té máte nadále problémy, připojte se k Internetu pomocí kabelu a počítač aktualizujte.</p>

</page>
