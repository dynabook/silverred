<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-mag" xml:lang="fr">

  <info>
    <link type="guide" xref="a11y#vision" group="lowvision"/>

    <revision pkgversion="3.7.1" date="2012-11-10" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Zoom avant sur l'écran pour rendre les choses plus visibles.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Agrandissement d'une zone écran</title>

  <p>L'agrandissement de l'écran est différent de la simple augmentation de la <link xref="a11y-font-size">taille du texte</link>. Cette option agit ici comme une loupe en vous permettant de vous déplacez en effectuant un zoom sur différentes parties de l'écran.</p>

  <steps>
    <item>
      <p>Ouvrez la vue d'ensemble des <link xref="shell-introduction#activities">Activités</link> et commencez à saisir <gui>Accès universel</gui>.</p>
    </item>
    <item>
      <p>Cliquez sur l'icône <gui>Accès universel</gui> pour afficher le panneau.</p>
    </item>
    <item>
      <p>Cliquez sur <gui>Zoom</gui> dans la rubrique <gui>Vision</gui>.</p>
    </item>
    <item>
      <p>Switch the <gui>Zoom</gui> switch in the top-right corner of the
      <gui>Zoom Options</gui> window to on.</p>
      <!--<note>
        <p>The <gui>Zoom</gui> section lists the current settings for the
        shortcut keys, which can be set in the <gui>Universal Access</gui>
        section of the <link xref="keyboard-shortcuts-set">Shortcuts</link> tab
        on the <gui>Keyboard</gui> panel.</p>
      </note>-->
    </item>
  </steps>

  <p>Vous pouvez maintenant vous déplacer sur l'écran. En déplaçant la souris vers les bords de l'écran, vous déplacez la zone agrandie dans différentes directions, ce qui vous permet de visualiser l'emplacement de votre choix.</p>

  <note style="tip">
    <p>Pour activer rapidement le zoom, cliquez sur l'<link xref="a11y-icon">icône d'accessibilité</link> dans la barre supérieure et sélectionnez <gui>Zoom</gui>.</p>
  </note>

  <p>You can change the magnification factor, the mouse tracking, and the
  position of the magnified view on the screen. Adjust these in the
  <gui>Magnifier</gui> tab of the <gui>Zoom Options</gui> window.</p>

  <p>Il est possible d'activer les pointeurs en croix pour vous aider à trouver le pointeur de la souris ou du pavé tactile. Pour les activer et régler leur largeur, leur couleur et leur épaisseur, cliquez sur <gui>Pointeurs en croix</gui> dans l'onglet <gui>Pointeurs en croix</gui> du panneau des paramètres de <gui>Zoom</gui>.</p>

  <p>Vous pouvez passer en vidéo inverse ou <gui>Blanc sur noir</gui>, et régler la luminosité, le contraste et le niveau de couleur de la loupe. La combinaison de ces options est utile aux personnes ayant une mauvaise vue ou un certain degré de photophobie, mais aussi si vous avez affaire à de mauvaises conditions d'éclairage. Sélectionnez l'onglet <gui>Effets de couleurs</gui> dans le panneau des paramètres de <gui>Zoom</gui> pour modifier ces options.</p>

</page>
