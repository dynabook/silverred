<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-use-windows-workspaces" xml:lang="gu">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>જેકબ સ્ટેઇનર</name>
    </credit>
    <credit type="author">
      <name>પેટર કોવર</name>
    </credit>
    <link type="guide" xref="getting-started" group="tasks"/>
    <title role="trail" type="link">વિન્ડો અને કામ કરવાની જગ્યાને વાપરો</title>
    <link type="seealso" xref="shell-windows-switching"/>
    <title role="seealso" type="link">વિન્ડો અને કામ કરવાની જગ્યાને વાપરવા પર માર્ગદર્શિકા</title>
    <link type="next" xref="gs-use-system-search"/>
  </info>

  <title>વિન્ડો અને કામ કરવાની જગ્યાને વાપરો</title>

  <ui:overlay width="812" height="452">
  <media type="video" its:translate="no" src="figures/gnome-windows-and-workspaces.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-windows-and-workspaces.svg"/>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>વિન્ડો અને કામ કરવાની જગ્યા</tt:p>
         </tt:div>
         <tt:div begin="6s" end="10s">
           <tt:p>To maximize a window, grab the window’s titlebar and drag it to
            the top of the screen.</tt:p>
           </tt:div>
         <tt:div begin="10s" end="13s">
           <tt:p>જ્યારે સ્ક્રીન પ્રકાશિત થયેલ હોય, વિન્ડોને પ્રકાશિત કરો.</tt:p>
         </tt:div>
         <tt:div begin="14s" end="20s">
           <tt:p>To unmaximize a window, grab the window’s titlebar and drag it
            away from the edges of the screen.</tt:p>
         </tt:div>
         <tt:div begin="25s" end="29s">
           <tt:p>વિન્ડોને દૂર ખેંચવા માટે તમે પણ ટોચની પટ્ટી પર ક્લિક કરી શકો છો અને તેને મહત્તમ કરો નહિં.</tt:p>
         </tt:div>
         <tt:div begin="34s" end="38s">
           <tt:p>To maximize a window along the left side of the screen, grab
            the window’s titlebar and drag it to the left.</tt:p>
         </tt:div>
         <tt:div begin="38s" end="40s">
           <tt:p>જ્યારે અડધી સ્ક્રીન પ્રકાશિત થયેલ હોય, વિન્ડોને પ્રકાશિત કરો.</tt:p>
         </tt:div>
         <tt:div begin="41s" end="44s">
           <tt:p>To maximize a window along the right side of the screen, grab
            the window’s titlebar and drag it to the right.</tt:p>
         </tt:div>
         <tt:div begin="44s" end="48s">
           <tt:p>જ્યારે અડધી સ્ક્રીન પ્રકાશિત થયેલ હોય, વિન્ડોને પ્રકાશિત કરો.</tt:p>
         </tt:div>
         <tt:div begin="54s" end="60s">
           <tt:p>કિબોર્ડની મદદથી વિન્ડોને મહત્તમ કરવા માટે, <key href="help:gnome-help/keyboard-key-super">Super</key> કીને પકડો અને <key>↑</key> ને દબાવો.</tt:p>
         </tt:div>
         <tt:div begin="61s" end="66s">
           <tt:p>તેનાં મહત્તમ ન હોય તેવા માપ સાથે વિન્ડોને પુન:સંગ્રહવા માટે, <key href="help:gnome-help/keyboard-key-super">Super</key> કીને પકડો અને <key>↓</key> દબાવો.</tt:p>
         </tt:div>
         <tt:div begin="66s" end="73s">
           <tt:p>સ્ક્રીનની જમણી બાજુ સાથે વિન્ડોને મહત્તમ કરવા માટે, <key href="help:gnome-help/keyboard-key-super">Super</key> કીને પકડી રાખો અને <key>←</key> દબાવો.</tt:p>
         </tt:div>
         <tt:div begin="76s" end="82s">
           <tt:p>સ્ક્રીનની ડાબી બાજુ સાથે વિન્ડોને મહત્તમ કરવા માટે, <key href="help:gnome-help/keyboard-key-super">Super</key> કીને પકડી રાખો અને <key>←</key> દબાવો.</tt:p>
         </tt:div>
         <tt:div begin="83s" end="89s">
           <tt:p>કામ કરવાની જગ્યાને ખસેડવા કે જે હાલની કામ કરવાની જગ્યાની નીચે છે, <keyseq><key href="help:gnome-help/keyboard-key-super">Super</key><key>Page Down</key></keyseq> ને દબાવો.</tt:p>
         </tt:div>
         <tt:div begin="90s" end="97s">
           <tt:p>કામ કરવાની જગ્યાને ખસેડવા કે જે હાલની કામ કરવાની જગ્યાની ઉપર છે, <keyseq><key href="help:gnome-help/keyboard-key-super">Super</key><key>Page Up</key></keyseq> ને દબાવો.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>
  
  <section id="use-workspaces-and-windows-maximize">
    <title>વિન્ડોને મહત્તમ કરો અને મહત્તમ ન કરો</title>
    <p/>
    
    <steps>
      <item><p>To maximize a window so that it takes up all of the space on
       your desktop, grab the window’s titlebar and drag it to the top of the
       screen.</p></item>
      <item><p>જ્યારે સ્ક્રીન પ્રકાશિત થયેલ હોય, તેને મહત્તમ કરવા માટે વિન્ડોને પ્રકાશિત કરો.</p></item>
      <item><p>To restore a window to its unmaximized size, grab the window’s
       titlebar and drag it away from the edges of the screen.</p></item>
    </steps>
    
  </section>

  <section id="use-workspaces-and-windows-tile">
    <title>તકતી વિન્ડો</title>
    <p/>
    
    <steps>
      <item><p>To maximize a window along a side of the screen, grab the window’s
       titlebar and drag it to the left or right side of the screen.</p></item>
      <item><p>જ્યારે અડધી સ્ક્રીન પ્રકાશિત થયેલ હોય, સ્ક્રીનની પસંદ થયેલ બાજુ સાથે તેને મહત્તમ કરવા માટે વિન્ડોને પ્રકાશિત કરો.</p></item>
      <item><p>વિન્ડોની બંને બાજુને મહત્તમ કરવા માટે, બીજી વિન્ડોની શીર્ષકપટ્ટીને લાવો અને સ્ક્રીનની વિરુદ્દ દિશામાં તેને ખેંચો.</p></item>
       <item><p>જ્યારે અડધી સ્ક્રીન પ્રકાશિત થયેલ હોય તો, સ્ક્રીનની વિરુદ્દ દિશાની સાથે તેને મહત્તમ કરવા માટે વિન્ડોને પ્રકાશિત કરો.</p></item>
    </steps>
    
  </section>
  
  <section id="use-workspaces-and-windows-maximize-keyboard">
    <title>કિબોર્ડની મદદથી વિન્ડોને મહત્તમ કરો અને મહત્તમ ન કરો</title>
    
    <steps>
      <item><p>કિબોર્ડની મદદથી વિન્ડોને મહત્તમ કરવા માટે, <key href="help:gnome-help/keyboard-key-super">Super</key> કીને પકડો અને <key>↑</key> ને દબાવો.</p></item>
      <item><p>કિબોર્ડની મદદથી વિન્ડોને મહત્તમ ન કરવા માટે, <key href="help:gnome-help/keyboard-key-super">Super</key> કીને પકડો અને <key>↓</key> ને દબાવો.</p></item>
    </steps>
    
  </section>
  
  <section id="use-workspaces-and-windows-tile-keyboard">
    <title>કિબોર્ડની મદદથી વિન્ડોની તકતી</title>
    
    <steps>
      <item><p>સ્ક્રીનની જમણી બાજુ સાથે વિન્ડોને મહત્તમ કરવા માટે, <key href="help:gnome-help/keyboard-key-super">Super</key> કીને પકડી રાખો અને <key>←</key> દબાવો.</p></item>
      <item><p>સ્ક્રીનની ડાબી બાજુ સાથે વિન્ડોને મહત્તમ કરવા માટે, <key href="help:gnome-help/keyboard-key-super">Super</key> કીને પકડી રાખો અને <key>←</key> દબાવો.</p></item>
    </steps>
    
  </section>
  
  <section id="use-workspaces-and-windows-workspaces-keyboard">
    <title>કિબોર્ડની મદદથી કામ કરવાની જગ્યાને બદલો</title>
    
    <steps>
    
    <item><p>કામ કરવાની જગ્યાને ખસેડવા કે જે હાલની કામ કરવાની જગ્યાની નીચે છે, <keyseq><key href="help:gnome-help/keyboard-key-super">Super</key><key>Page Down</key></keyseq> ને દબાવો.</p></item>
    <item><p>કામ કરવાની જગ્યાને ખસેડવા કે જે હાલની કામ કરવાની જગ્યાની ઉપર છે, <keyseq><key href="help:gnome-help/keyboard-key-super">Super</key><key>Page Up</key></keyseq> ને દબાવો.</p></item>

    </steps>
    
  </section>

</page>
