<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-which-application" xml:lang="ca">

  <info>
    <link type="guide" xref="accounts"/>
    <link type="seealso" xref="accounts-disable-service"/>

    <revision pkgversion="3.8.2" date="2013-05-22" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="incomplete"/>

    <credit type="author copyright">
      <name>Baptiste Mille-Mathias</name>
      <email>baptistem@gnome.org</email>
      <years>2012, 2013</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Les aplicacions poden utilitzar els comptes creats en comptes en línia. <app>Comptes en línia</app> i els serveis que exploten.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

  <title>Serveis i aplicacions en línia</title>

  <p>Un cop hàgiu afegit un compte en línia, qualsevol aplicació pot utilitzar aquest compte per a qualsevol dels serveis disponibles que no s'hagin <link xref="accounts-disable-service">desactivat</link>. Els diferents proveïdors proporcionen serveis diferents. Aquesta pàgina enumera els diferents serveis i algunes de les aplicacions conegudes per utilitzar.</p>

  <terms>
    <item>
      <title>Calendari</title>
      <p>El servei de calendari us permet veure, afegir i editar esdeveniments en un calendari en línia. L'utilitzen aplicacions com <app>Calendari</app>, <app>Evolution</app>, i <app>California</app>.</p>
    </item>

    <item>
      <title>Xat</title>
      <p>El servei de xat us permet parlar amb els vostres contactes a les plataformes de missatgeria instantània més populars. L'utilitza l'aplicació <app>Empathy</app>.</p>
    </item>

    <item>
      <title>Contactes</title>
      <p>El servei Contactes us permet veure els detalls publicats dels vostres contactes en diversos serveis. S'utilitza per aplicacions com ara <app>Contactes</app> i <app>Evolution</app>.</p>
    </item>

    <item>
      <title>Documents</title>
      <p>El servei Documents permet visualitzar els vostres documents en línia, com ara els de Google docs. Podeu visualitzar els vostres documents utilitzant l'aplicació <app>Documents</app>.</p>
    </item>

    <item>
      <title>Fitxers</title>
      <p>El Servei de fitxers afegeix una ubicació remota dels fitxers, com si n'haguéssiu afegit un amb la funcionalitat <link xref="nautilus-connect">Connecta a un servidor</link> al gestor de fitxers. Podeu accedir als fitxers remots usant el gestor de fitxers, així com a través del diàleg obrir i desar en qualsevol aplicació.</p>
    </item>

    <item>
      <title>Correu</title>
      <p>El Servei de correu us permet enviar i rebre coreus mitjançant el vostre proveïdor de correu, com per exemple Google. L'utilitza l'aplicació <app>Evolution</app>.</p>
    </item>

<!-- TODO: Not sure what this does. Doesn't seem to do anything in Maps app.
    <item>
      <title>Maps</title>
    </item>
-->

    <item>
      <title>Fotos</title>
      <p>El servei de Fotos us permet veure les fotos en línia com ara les que publiqueu a Facebook. Podeu veure les vostres fotos mitjançant l'aplicació <app>Fotos</app>.</p>
    </item>

    <item>
      <title>Impressores</title>
      <p>El servei Impressores us permet enviar una còpia en PDF a un proveïdor des del diàleg d'impressió de qualsevol aplicació. El proveïdor pot proporcionar serveis d'impressió, o pot fer-se servir d'emmagatzematge per al PDF, que podreu descarregar o imprimir més endavant.</p>
    </item>

    <item>
      <title>Llegir més endavant</title>
      <p>El servei 'Llegeix més endavant' us permet desar una pàgina web a serveis externs perquè pugueu llegir-la més tard en un altre dispositiu. Actualment no hi ha aplicacions que utilitzin aquest servei.</p>
    </item>

  </terms>

</page>
