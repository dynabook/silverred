<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-multi-monitor" xml:lang="sr-Latn">

  <info>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.28" date="2018-07-22" status="review"/>
    <revision pkgversion="3.33" date="2019-07-21" status="candidate"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Mapirajte Vakom tablicu prema posebnom monitoru.</desc>
  </info>

  <title>Izaberite monitor</title>

<steps>
  <item>
    <p>Otvorite pregled <gui xref="shell-introduction#activities">Aktivnosti</gui> i počnite da kucate <gui>Vakom tablica</gui>.</p>
  </item>
  <item>
    <p>Kliknite na <gui>Vakom tablica</gui> da otvorite panel.</p>
  </item>
  <item>
    <p>Click the <gui>Tablet</gui> button in the header bar.</p>
    <note style="tip"><p>Ako tablica nije otkrivena, od vas će biti zatraženo da <gui>priključite ili upalite vašu Vakom tablicu</gui>. Kliknite na vezu <gui>Podešavanja blututa</gui> da povežete bežičnu tablicu.</p></note>
  </item>
  <item><p>Kliknite <gui>Mapiraj monitor…</gui></p></item>
  <item><p>Izaberite <gui>Mapiraj na jednom monitoru</gui>.</p></item>
  <item><p>Pored <gui>Izlaza</gui>, izaberite monitor za koji želite da prima ulaz sa vaše grafičke tablice.</p>
     <note style="tip"><p>Samo monitori koji su podešeni moći će da se biraju.</p></note>
  </item>
  <item>
    <p>Switch the <gui>Keep aspect ratio (letterbox)</gui> switch to on to
    match the drawing area of the tablet to the proportions of the monitor.
      This setting, also called <em>force proportions</em>,
      “letterboxes” the drawing area on a tablet to correspond more
      directly to a display. For example, a 4∶3 tablet would be mapped so that
      the drawing area would correspond to a widescreen display.</p>
  </item>
  <item><p>Kliknite <gui>Zatvori</gui>.</p></item>
</steps>

</page>
