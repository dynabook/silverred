<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="look-display-fuzzy" xml:lang="sv">

  <info>
    <link type="guide" xref="hardware-problems-graphics"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-10-11" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.28" date="2018-07-28" status="review"/>

    <credit type="author">
      <name>Dokumentationsprojekt för GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Skärmupplösningen kan vara felaktigt inställd.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Varför ser saker otydliga/pixellerade ut på min skärm?</title>

  <p>Skärmupplösningen som konfigeras är kanske inte den rätta för din skärm. För att lösa detta:</p>

  <steps>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Inställningar</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Inställningar</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Enheter</gui> i sidopanelen.</p>
    </item>
    <item>
      <p>Klicka på <gui>Skärmar</gui> i sidopanelen för att öppna panelen.</p>
    </item>
    <item>
      <p>Pröva några av alternativen för <gui>Upplösning</gui> och välj det som får skärmen att se bättre ut.</p>
    </item>
  </steps>

<section id="multihead">
  <title>När flera skärmar är anslutna</title>

  <p>Om du har två skärmar anslutna till datorn (till exempel en vanlig skärm och en projektor) kan skärmarna ha olika optimala, eller <link xref="look-resolution#native">naturliga</link> upplösningar.</p>

  <p>Med läget <link xref="display-dual-monitors#modes">Spegling</link> kan du visa samma sak på två skärmar. Båda skärmarna använder samma upplösning, vilket kanske inte matchar den naturliga upplösningen för endera skärm, så bildskärpan kan drabbas på båda skärmarna.</p>

  <p>Då läget <link xref="display-dual-monitors#modes">Sammanfoga skärm­ar</link> används kan upplösningen för varje skärm ställas in oberoende, så de kan båda ställas in till sin naturliga upplösning.</p>

</section>

</page>
