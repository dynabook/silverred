<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mouse-middleclick" xml:lang="nl">

  <info>
    <link type="guide" xref="tips"/>
    <link type="guide" xref="mouse#tips"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.4" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Gebruik de middelste muisknop om toepassingen, tabbladen en nog veel meer te openen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

<title>Middelklik</title>

<p>Veel muizen, en sommige touchpads, hebben een middelste muisknop. Bij een muis met een scrollwieltje kunt u meestal op het wieltje drukken voor een middelklik. Als u geen middelste muisknop heeft, dan kunt u de linker en rechter muisknop tegelijk indrukken voor een middelklik.</p>

<p>Op een touchpad die het tikken met meerdere vingers ondersteunt, kunt u middenklikken door met drie vingers tegelijk te tikken. U dient <link xref="mouse-touchpad-click">klikken door tikken inschakelen</link> geselecteerd te hebben bij de touchpadinstellingen om dit te laten werken.</p>

<p>Veel toepassingen gebruiken de middelste muisknop voor geavanceerde snelkoppelingen.</p>

<list>
  <item><p>In applications with scrollbars, left-clicking in the empty space of
  the bar moves the scroll position directly to that place. Middle-clicking
  moves up to a single page towards that location.</p></item>

  <item><p>In het <gui>Activiteiten</gui>-overzicht kunt u met een middelklik snel een nieuw venster voor een toepassing openen. Hiervoor hoeft u alleen met de middelste muisknop op het pictogram in de Starter aan de linkerkant, of in het toepassingenoverzicht te klikken. Het toepassingenoverzicht wordt weergegeven via de rastertoets in de Starter.</p></item>

  <item><p>In de meeste webbrowsers kunt u snel links in tabbladen openen met de middelste muisknop. Klik gewoon met de middelste muisknop op een link en deze zal geopend worden in een nieuw tabblad.</p></item>

  <item><p>In bestandsbeheer heeft de middelklik twee functies. Als u middelklikt op een map, dan wordt die geopend in een nieuw tabblad. Dit doet het gedrag na van populaire webbrowsers. Als u middelklikt op een bestand, dan wordt het bestand geopend alsof u erop gedubbelklikt had.</p></item>
</list>

<p>In sommige gespecialiseerde toepassingen kunt u de middelste muisknop gebruiken voor andere functies. Zoek in de hulp van de toepassing naar <em>middelklik</em> of <em>middelste muisknop</em>.</p>

</page>
