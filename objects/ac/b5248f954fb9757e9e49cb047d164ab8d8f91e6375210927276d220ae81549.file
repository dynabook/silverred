<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="dconf-profiles" xml:lang="hu">

  <info>
    <link type="guide" xref="setup"/>
    <link type="seealso" xref="dconf-custom-defaults"/>
    <link type="seealso" xref="dconf"/>
    <revision pkgversion="3.30" date="2019-02-22" status="incomplete"/>

    <credit type="author copyright">
      <name>Ryan Lortie</name>
      <email>desrt@desrt.ca</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Švárová</name>
      <email>jana.svarova@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Részletes információk a profillal és a profil kiválasztásával kapcsolatban.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Úr Balázs</mal:name>
      <mal:email>ur.balazs@fsf.hu</mal:email>
      <mal:years>2018, 2019, 2020.</mal:years>
    </mal:credit>
  </info>

  <title>Profilok</title>

  <p>Egy <em>profil</em> a konfigurációs adatbázisok listája. Egy profilban lévő első adatbázis az írandó adatbázis, és a többi adatbázis csak olvasható. A rendszeradatbázisok mindegyike egy kulcsfájl könyvtárból kerül előállításra. Minden kulcsfájl könyvtár egy vagy több fájlt tartalmaz. Az egyes kulcsfájlok legalább egy dconf útvonalat, valamint egy vagy több kulcsot és a hozzájuk tartozó értékeket tartalmaznak.</p>

  <p>Azok a kulcspárok, amelyek egy <sys>dconf</sys> <em>profilban</em> be vannak állítva, felülbírálják az alapértelmezett beállításokat, hacsak nincs valami probléma azzal az értékkel, amelyet beállított.</p>

  <p>Általában azt szeretnénk, hogy a <sys>dconf</sys> profil egy <em>felhasználói adatbázisból</em> és legalább egy rendszeradatbázisból álljon. A profilnak soronként egy adatbázist kell felsorolnia.</p>

  <p>Egy profilban az első sor az az adatbázis, amelybe a változtatások írva lesznek. Ez általában a <code>user-db:<input>user</input></code>, ahol a <input>user</input> annak a felhasználói adatbázisnak a neve, amely általában a <file>~/.config/dconf</file> fájlban található.</p>

  <p>Egy <code>system-db</code> sor határoz meg egy rendszeradatbázist. Ezek az adatbázisok az <file>/etc/dconf/db/</file> könyvtárban találhatók.</p>

  <example>
    <listing>
      <title>Minta profil</title>
<code its:translate="no">
user-db:user
system-db:<var>local</var>
system-db:<var>site</var>
</code>
    </listing>
  </example>

  <!-- TODO: explain the profile syntax (maybe new page) -->
  <!--TODO: explain local and site -->

  <p>Egyetlen felhasználó és több rendszeradatbázis beállítása lehetővé teszi a beállítások rétegezését. A <code>user</code> adatbázisfájljából származó beállítások elsőbbséget élveznek a <code>local</code> adatbázisfájlban lévő beállításokkal szemben, és a <code>local</code> adatbázisfájl pedig elsőbbséget élvez a <code>site</code> adatbázisfájllal szemben.</p>

  <p>Azonban a <link xref="dconf-lockdown">zárak</link> elsőbbségének sorrendje fordított. A <code>site</code> vagy a <code>local</code> adatbázisfájlokban bevezetett zárak elsőbbséget élveznek a <code>user</code> adatbázisfájlokban lévőkkel szemben.</p>

  <note style="important">
  <p>Egy munkamenet <sys>dconf</sys> profilját a bejelentkezéskor határozzák meg, így a felhasználóknak ki kell jelentkezniük és be kell lépniük a munkamenetükre történő új <sys>dconf</sys> felhasználói profil alkalmazásához.</p>
  </note>

  <p>További információkért nézze meg a <link its:translate="no" href="man:dconf(7)">
  <cmd>dconf</cmd>(7)</link> kézikönyvoldalát.</p>

  <section id="dconf-profiles">

  <title>Egy profil kiválasztása</title>

  <p>Indításkor a <sys>dconf</sys> megnézi a <sys>DCONF_PROFILE</sys> környezeti változót. A változó meghatározhat egy relatív útvonalat az <file>/etc/dconf/profile/</file> könyvtárban lévő fájlhoz, vagy egy abszolút útvonalat, például a felhasználó saját könyvtárához.</p>

  <p>Ha a környezeti változó be van állítva, akkor a <sys>dconf</sys> megpróbálja megnyitni az elnevezett profilt, és megszakítja, ha az sikertelen. Ha a változó nincs beállítva, akkor a <sys>dconf</sys> megpróbálja megnyitni a „user” nevű profilt. Ha az sikertelen, akkor visszavált egy belső, beprogramozott beállításra.</p>

  <p>További információkért nézze meg a <link its:translate="no" href="man:dconf(7)">
  <cmd>dconf</cmd>(7)</link> kézikönyvoldalát.</p>

  </section>

</page>
