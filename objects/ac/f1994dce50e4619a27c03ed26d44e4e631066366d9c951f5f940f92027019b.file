<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>imagefreeze: GStreamer Good Plugins 1.0 Plugins Reference Manual</title>
<meta name="generator" content="DocBook XSL Stylesheets Vsnapshot">
<link rel="home" href="index.html" title="GStreamer Good Plugins 1.0 Plugins Reference Manual">
<link rel="up" href="ch01.html" title="gst-plugins-good Elements">
<link rel="prev" href="gst-plugins-good-plugins-iirequalizer.html" title="iirequalizer">
<link rel="next" href="gst-plugins-good-plugins-interleave.html" title="interleave">
<meta name="generator" content="GTK-Doc V1.32 (XML mode)">
<link rel="stylesheet" href="style.css" type="text/css">
</head>
<body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF">
<table class="navigation" id="top" width="100%" summary="Navigation header" cellpadding="2" cellspacing="5"><tr valign="middle">
<td width="100%" align="left" class="shortcuts">
<a href="#" class="shortcut">Top</a><span id="nav_description">  <span class="dim">|</span> 
                  <a href="#gst-plugins-good-plugins-imagefreeze.description" class="shortcut">Description</a></span><span id="nav_hierarchy">  <span class="dim">|</span> 
                  <a href="#gst-plugins-good-plugins-imagefreeze.object-hierarchy" class="shortcut">Object Hierarchy</a></span><span id="nav_properties">  <span class="dim">|</span> 
                  <a href="#gst-plugins-good-plugins-imagefreeze.properties" class="shortcut">Properties</a></span>
</td>
<td><a accesskey="h" href="index.html"><img src="home.png" width="16" height="16" border="0" alt="Home"></a></td>
<td><a accesskey="u" href="ch01.html"><img src="up.png" width="16" height="16" border="0" alt="Up"></a></td>
<td><a accesskey="p" href="gst-plugins-good-plugins-iirequalizer.html"><img src="left.png" width="16" height="16" border="0" alt="Prev"></a></td>
<td><a accesskey="n" href="gst-plugins-good-plugins-interleave.html"><img src="right.png" width="16" height="16" border="0" alt="Next"></a></td>
</tr></table>
<div class="refentry">
<a name="gst-plugins-good-plugins-imagefreeze"></a><div class="titlepage"></div>
<div class="refnamediv"><table width="100%"><tr>
<td valign="top">
<h2><span class="refentrytitle"><a name="gst-plugins-good-plugins-imagefreeze.top_of_page"></a>imagefreeze</span></h2>
<p>imagefreeze</p>
</td>
<td class="gallery_image" valign="top" align="right"></td>
</tr></table></div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-imagefreeze.properties"></a><h2>Properties</h2>
<div class="informaltable"><table class="informaltable" border="0">
<colgroup>
<col width="150px" class="properties_type">
<col width="300px" class="properties_name">
<col width="200px" class="properties_flags">
</colgroup>
<tbody><tr>
<td class="property_type"><span class="type">gint</span></td>
<td class="property_name"><a class="link" href="gst-plugins-good-plugins-imagefreeze.html#GstImageFreeze--num-buffers" title="The “num-buffers” property">num-buffers</a></td>
<td class="property_flags">Read / Write</td>
</tr></tbody>
</table></div>
</div>
<a name="GstImageFreeze"></a><div class="refsect1">
<a name="gst-plugins-good-plugins-imagefreeze.other"></a><h2>Types and Values</h2>
<div class="informaltable"><table class="informaltable" width="100%" border="0">
<colgroup>
<col width="150px" class="other_proto_type">
<col class="other_proto_name">
</colgroup>
<tbody><tr>
<td class="datatype_keyword">struct</td>
<td class="function_name"><a class="link" href="gst-plugins-good-plugins-imagefreeze.html#GstImageFreeze-struct" title="struct GstImageFreeze">GstImageFreeze</a></td>
</tr></tbody>
</table></div>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-imagefreeze.object-hierarchy"></a><h2>Object Hierarchy</h2>
<pre class="screen">    GObject
    <span class="lineart">╰──</span> GInitiallyUnowned
        <span class="lineart">╰──</span> GstObject
            <span class="lineart">╰──</span> GstElement
                <span class="lineart">╰──</span> GstImageFreeze
</pre>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-imagefreeze.description"></a><h2>Description</h2>
<p>The imagefreeze element generates a still frame video stream from
the input. It duplicates the first frame with the framerate requested
by downstream, allows seeking and answers queries.</p>
<div class="refsect2">
<a name="id-1.2.78.7.3"></a><h3>Example launch line</h3>
<div class="informalexample">
  <table class="listing_frame" border="0" cellpadding="0" cellspacing="0">
    <tbody>
      <tr>
        <td class="listing_lines" align="right"><pre>1</pre></td>
        <td class="listing_code"><pre class="programlisting"><span class="n">gst</span><span class="o">-</span><span class="n">launch</span><span class="o">-</span><span class="mf">1.0</span> <span class="o">-</span><span class="n">v</span> <span class="n">filesrc</span> <span class="n">location</span><span class="o">=</span><span class="n">some</span><span class="p">.</span><span class="n">png</span> <span class="o">!</span> <span class="n">decodebin</span> <span class="o">!</span> <span class="n">imagefreeze</span> <span class="o">!</span> <span class="n">autovideosink</span></pre></td>
      </tr>
    </tbody>
  </table>
</div>
 This pipeline shows a still frame stream of a PNG file.
</div>
<div class="refsynopsisdiv">
<h2>Synopsis</h2>
<div class="refsect2">
<a name="id-1.2.78.7.4.1"></a><h3>Element Information</h3>
<div class="variablelist"><table border="0" class="variablelist">
<colgroup>
<col align="left" valign="top">
<col>
</colgroup>
<tbody>
<tr>
<td><p><span class="term">plugin</span></p></td>
<td>
            <a class="link" href="gst-plugins-good-plugins-plugin-imagefreeze.html#plugin-imagefreeze">imagefreeze</a>
          </td>
</tr>
<tr>
<td><p><span class="term">author</span></p></td>
<td>Sebastian Dröge &lt;sebastian.droege@collabora.co.uk&gt;</td>
</tr>
<tr>
<td><p><span class="term">class</span></p></td>
<td>Filter/Video</td>
</tr>
</tbody>
</table></div>
</div>
<hr>
<div class="refsect2">
<a name="id-1.2.78.7.4.2"></a><h3>Element Pads</h3>
<div class="variablelist"><table border="0" class="variablelist">
<colgroup>
<col align="left" valign="top">
<col>
</colgroup>
<tbody>
<tr>
<td><p><span class="term">name</span></p></td>
<td>sink</td>
</tr>
<tr>
<td><p><span class="term">direction</span></p></td>
<td>sink</td>
</tr>
<tr>
<td><p><span class="term">presence</span></p></td>
<td>always</td>
</tr>
<tr>
<td><p><span class="term">details</span></p></td>
<td>video/x-raw(ANY)</td>
</tr>
</tbody>
</table></div>
<div class="variablelist"><table border="0" class="variablelist">
<colgroup>
<col align="left" valign="top">
<col>
</colgroup>
<tbody>
<tr>
<td><p><span class="term">name</span></p></td>
<td>src</td>
</tr>
<tr>
<td><p><span class="term">direction</span></p></td>
<td>source</td>
</tr>
<tr>
<td><p><span class="term">presence</span></p></td>
<td>always</td>
</tr>
<tr>
<td><p><span class="term">details</span></p></td>
<td>video/x-raw(ANY)</td>
</tr>
</tbody>
</table></div>
</div>
</div>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-imagefreeze.functions_details"></a><h2>Functions</h2>
<p></p>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-imagefreeze.other_details"></a><h2>Types and Values</h2>
<div class="refsect2">
<a name="GstImageFreeze-struct"></a><h3>struct GstImageFreeze</h3>
<pre class="programlisting">struct GstImageFreeze;</pre>
</div>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-imagefreeze.property-details"></a><h2>Property Details</h2>
<div class="refsect2">
<a name="GstImageFreeze--num-buffers"></a><h3>The <code class="literal">“num-buffers”</code> property</h3>
<pre class="programlisting">  “num-buffers”              <span class="type">gint</span></pre>
<p>Number of buffers to output before sending EOS (-1 = unlimited).</p>
<p>Owner: GstImageFreeze</p>
<p>Flags: Read / Write</p>
<p>Allowed values: &gt;= G_MAXULONG</p>
<p>Default value: -1</p>
</div>
</div>
</div>
<div class="footer">
<hr>Generated by GTK-Doc V1.32</div>
</body>
</html>