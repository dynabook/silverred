<?xml version="1.0" encoding="UTF-8"?>
<libosinfo version="0.0.1">

  <os id="http://fedoraproject.org/fedora/31">
    <short-id>fedora31</short-id>
    <name>Fedora 31</name>
    <name xml:lang="fr">Fedora 31</name>
    <name xml:lang="pl">Fedora 31</name>
    <name xml:lang="uk">Fedora 31</name>
    <version>31</version>
    <vendor>Fedora Project</vendor>
    <vendor xml:lang="ca">Projecte Fedora</vendor>
    <vendor xml:lang="de">Fedora-Projekt</vendor>
    <vendor xml:lang="es">Fedora Project</vendor>
    <vendor xml:lang="fr">Projet Fedora</vendor>
    <vendor xml:lang="id">Proyek Fedora</vendor>
    <vendor xml:lang="it">Progetto Fedora</vendor>
    <vendor xml:lang="ja">Fedora Project</vendor>
    <vendor xml:lang="pl">Projekt Fedora</vendor>
    <vendor xml:lang="pt">Projecto Fedora </vendor>
    <vendor xml:lang="pt_BR">Projeto Fedora</vendor>
    <vendor xml:lang="uk">Проєкт Fedora</vendor>
    <family>linux</family>
    <distro>fedora</distro>
    <upgrades id="http://fedoraproject.org/fedora/30"/>
    <derives-from id="http://fedoraproject.org/fedora/30"/>

    <release-date>2019-10-29</release-date>

    <variant id="workstation">
      <name>Fedora 31 Workstation</name>
      <name xml:lang="fr">Fedora 31 Workstation</name>
      <name xml:lang="pl">Fedora 31 Workstation</name>
      <name xml:lang="uk">Fedora 31 Workstation</name>
    </variant>
    <variant id="server">
      <name>Fedora 31 Server</name>
      <name xml:lang="fr">Fedora 31 Server</name>
      <name xml:lang="pl">Fedora 31 Server</name>
      <name xml:lang="uk">Fedora 31 Server</name>
    </variant>
    <variant id="server-netinst">
      <name>Fedora 31 Server</name>
      <name xml:lang="fr">Fedora 31 Server</name>
      <name xml:lang="pl">Fedora 31 Server</name>
      <name xml:lang="uk">Fedora 31 Server</name>
    </variant>
    <variant id="everything-netinst">
      <name>Fedora 31 Everything</name>
      <name xml:lang="fr">Fedora 31 Everything</name>
      <name xml:lang="pl">Fedora 31 Everything</name>
      <name xml:lang="uk">Fedora 31 Everything</name>
    </variant>

    

    
    <media live="true" installer-script="false" arch="x86_64">
      <variant id="workstation"/>
      <url>https://download.fedoraproject.org/pub/fedora/linux/releases/31/Workstation/x86_64/iso/Fedora-Workstation-Live-x86_64-31-1.9.iso</url>
      <iso>
        <volume-id>Fedora-WS-Live-31.*</volume-id>
      </iso>
      <kernel>isolinux/vmlinuz</kernel>
      <initrd>isolinux/initrd.img</initrd>
    </media>

    

    
    <media arch="x86_64">
      <variant id="server"/>
      <url>https://download.fedoraproject.org/pub/fedora/linux/releases/31/Server/x86_64/iso/Fedora-Server-dvd-x86_64-31-1.9.iso</url>
      <iso>
        <volume-id>Fedora-S-dvd-x86_64-31</volume-id>
        <system-id>LINUX</system-id>
      </iso>
      <kernel>isolinux/vmlinuz</kernel>
      <initrd>isolinux/initrd.img</initrd>
      <installer>
        <script id='http://fedoraproject.org/fedora/kickstart/jeos'/>
      </installer>
    </media>

    
    <media arch="x86_64">
      <variant id="server-netinst"/>
      <url>https://download.fedoraproject.org/pub/fedora/linux/releases/31/Server/x86_64/iso/Fedora-Server-netinst-x86_64-31-1.9.iso</url>
      <iso>
        <volume-id>Fedora-S-dvd-x86_64-31</volume-id>
      </iso>
      <kernel>isolinux/vmlinuz</kernel>
      <initrd>isolinux/initrd.img</initrd>
      <installer>
        <script id='http://fedoraproject.org/fedora/kickstart/jeos'/>
      </installer>
    </media>

    
    <media installer-script="false" arch="aarch64">
      <variant id="server"/>
      <url>https://download.fedoraproject.org/pub/fedora/linux/releases/31/Server/aarch64/iso/Fedora-Server-dvd-aarch64-31-1.9.iso</url>
      <iso>
        <volume-id>Fedora-S-dvd-aarch64-31</volume-id>
        <system-id>LINUX</system-id>
      </iso>
      <kernel>isolinux/vmlinuz</kernel>
      <initrd>isolinux/initrd.img</initrd>
      <installer>
        <script id='http://fedoraproject.org/fedora/kickstart/jeos'/>
      </installer>
    </media>

    
    <media installer-script="false" arch="aarch64">
      <variant id="server-netinst"/>
      <url>https://download.fedoraproject.org/pub/fedora/linux/releases/31/Server/aarch64/iso/Fedora-Server-netinst-aarch64-31-1.9.iso</url>
      <iso>
        <volume-id>Fedora-S-dvd-aarch64-31</volume-id>
      </iso>
      <kernel>isolinux/vmlinuz</kernel>
      <initrd>isolinux/initrd.img</initrd>
    </media>

    
    <media installer-script="false" arch="armv7l">
      <variant id="server"/>
      <url>https://download.fedoraproject.org/pub/fedora/linux/releases/31/Server/armhfp/iso/Fedora-Server-dvd-armhfp-31-1.9.iso</url>
      <iso>
        <volume-id>Fedora-S-dvd-armhfp-31</volume-id>
        <system-id>LINUX</system-id>
      </iso>
      <kernel>isolinux/vmlinuz</kernel>
      <initrd>isolinux/initrd.img</initrd>
    </media>

    
    <media installer-script="false" arch="armv7l">
      <variant id="server-netinst"/>
      <url>https://download.fedoraproject.org/pub/fedora/linux/releases/31/Server/armhfp/iso/Fedora-Server-netinst-armhfp-31-1.9.iso</url>
      <iso>
        <volume-id>Fedora-S-dvd-armhfp-31</volume-id>
      </iso>
      <kernel>isolinux/vmlinuz</kernel>
      <initrd>isolinux/initrd.img</initrd>
      <installer>
        <script id='http://fedoraproject.org/fedora/kickstart/jeos'/>
      </installer>
    </media>

    
    <media arch="x86_64">
      <variant id="everything-netinst"/>
      <url>https://download.fedoraproject.org/pub/fedora/linux/releases/31/Server/x86_64/iso/Fedora-Server-netinst-x86_64-31-1.9.iso</url>
      <iso>
        <volume-id>Fedora-E-dvd-x86_64-31</volume-id>
      </iso>
      <kernel>isolinux/vmlinuz</kernel>
      <initrd>isolinux/initrd.img</initrd>
      <installer>
        <script id='http://fedoraproject.org/fedora/kickstart/jeos'/>
        <script id='http://fedoraproject.org/fedora/kickstart/desktop'/>
      </installer>
    </media>

    <media installer-script="false" arch="aarch64">
      <variant id="everything-netinst"/>
      <url>https://download.fedoraproject.org/pub/fedora/linux/releases/31/Server/aarch64/iso/Fedora-Server-netinst-aarch64-31-1.9.iso</url>
      <iso>
        <volume-id>Fedora-E-dvd-aarch64-31</volume-id>
      </iso>
      <kernel>isolinux/vmlinuz</kernel>
      <initrd>isolinux/initrd.img</initrd>
    </media>

    <media installer-script="false" arch="armv7l">
      <variant id="everything-netinst"/>
      <url>https://download.fedoraproject.org/pub/fedora/linux/releases/31/Server/armhfp/iso/Fedora-Server-netinst-armhfp-31-1.9.iso</url>
      <iso>
        <volume-id>Fedora-E-dvd-armhfp-31</volume-id>
      </iso>
      <kernel>isolinux/vmlinuz</kernel>
      <initrd>isolinux/initrd.img</initrd>
    </media>

    <tree arch="x86_64">
      <url>https://download.fedoraproject.org/pub/fedora/linux/releases/31/Server/x86_64/os</url>
      <variant id="server"/>
      <treeinfo>
        <family>Fedora</family>
        <version>31</version>
        <arch>x86_64</arch>
        <variant>Server</variant>
      </treeinfo>
    </tree>

    <tree arch="aarch64">
      <url>https://download.fedoraproject.org/pub/fedora/linux/releases/31/Server/aarch64/os/</url>
      <variant id="server"/>
      <treeinfo>
        <family>Fedora</family>
        <version>31</version>
        <arch>aarch64</arch>
        <variant>Server</variant>
      </treeinfo>
    </tree>

    <tree arch="armv7l">
      <url>https://download.fedoraproject.org/pub/fedora/linux/releases/31/Server/armhfp/os/</url>
      <variant id="server"/>
      <treeinfo>
        <family>Fedora</family>
        <version>31</version>
        <arch>armhfp</arch>
        <variant>Server</variant>
      </treeinfo>
    </tree>

    <tree arch="x86_64">
      <url>https://download.fedoraproject.org/pub/fedora/linux/releases/31/Everything/x86_64/os</url>
      <variant id="everything-netinst"/>
      <treeinfo>
        <family>Fedora</family>
        <version>31</version>
        <arch>x86_64</arch>
        <variant>Everything</variant>
      </treeinfo>
    </tree>

    <tree arch="aarch64">
      <url>https://download.fedoraproject.org/pub/fedora/linux/releases/31/Everything/aarch64/os/</url>
      <variant id="everything-netinst"/>
      <treeinfo>
        <family>Fedora</family>
        <version>31</version>
        <arch>aarch64</arch>
        <variant>Everything</variant>
      </treeinfo>
    </tree>

    <tree arch="armv7l">
      <url>https://download.fedoraproject.org/pub/fedora/linux/releases/31/Everything/armhfp/os/</url>
      <variant id="everything-netinst"/>
      <treeinfo>
        <family>Fedora</family>
        <version>31</version>
        <arch>armhfp</arch>
        <variant>Everything</variant>
      </treeinfo>
    </tree>

    <image format="qcow2" cloud-init="true" arch="x86_64">
      <url>https://dl.fedoraproject.org/pub/fedora/linux/releases/31/Cloud/x86_64/images/Fedora-Cloud-Base-31-1.9.x86_64.qcow2</url>
    </image>

    <image format="qcow2" cloud-init="true" arch="aarch64">
      <url>https://dl.fedoraproject.org/pub/fedora/linux/releases/31/Cloud/aarch64/images/Fedora-Cloud-Base-31-1.9.aarch64.qcow2</url>
    </image>

    

    <resources arch="all">
      <minimum>
        <n-cpus>1</n-cpus>
        <cpu>1000000000</cpu>
        <ram>1073741824</ram>
        <storage>10737418240</storage>
      </minimum>

      <recommended>
        <ram>2147483648</ram>
        <storage>21474836480</storage>
      </recommended>
    </resources>

    <installer>
      <script id='http://fedoraproject.org/fedora/kickstart/jeos'/>
      <script id='http://fedoraproject.org/fedora/kickstart/desktop'/>
    </installer>
  </os>
</libosinfo>