<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-delete" xml:lang="de">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>
    <link type="seealso" xref="files-recover"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-16" status="review"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="candidate"/>
    <revision pkgversion="3.16" date="2015-02-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Entfernen Sie Dateien oder Ordner, die Sie nicht mehr benötigen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2019.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2019.</mal:years>
    </mal:credit>
  </info>

<title>Dateien und Ordner löschen</title>

  <p>Falls Sie eine Datei oder einen Ordner nicht mehr benötigen, dann löschen Sie diesen. Wenn Sie ein Objekt löschen wird es in den <gui>Papierkorb</gui>-Ordner verschoben, wo es verbleibt, bis Sie den Papierkorb leeren. Im <gui>Papierkorb</gui>-Ordner befindliche Objekte können an ihrem ursprünglichen Ort <link xref="files-recover">wiederhergestellt werden</link>, falls Sie diese doch noch brauchen oder wenn sie versehentlich gelöscht wurden.</p>

  <steps>
    <title>So verschieben Sie eine Datei in den Papierkorb:</title>
    <item><p>Wählen Sie die in den Papierkorb zu verschiebende Datei aus, indem Sie einmalig darauf klicken.</p></item>
    <item><p>Drücken Sie <key>Entf</key> auf Ihrer Tastatur. Alternativ können Sie das Objekt in den <gui>Papierkorb</gui> in der Seitenleiste ziehen.</p></item>
  </steps>

  <p>Die Datei wird in den Papierkorb verschoben und die Option zum <gui>rückgängig</gui> machen wird angezeigt. Der <gui>Rückgängig</gui>-Knopf ist für einige Sekunden sichtbar. Wenn Sie <gui>Rückgängig</gui> anklicken, wird die Datei an ihrem ursprünglichen Ort wiederhergestellt.</p>

  <p>Um Dateien dauerhaft zu löschen und Platz auf Ihrem Rechner zu schaffen, können Sie den Papierkorb leeren. Um dies zu tun, klicken Sie mit der rechten Maustaste auf <gui>Papierkorb</gui> in der Seitenleiste und wählen Sie <gui>Papierkorb leeren</gui>.</p>

  <section id="permanent">
    <title>Eine Datei dauerhaft löschen</title>
    <p>Sie können ein Objekt unmittelbar dauerhaft löschen, ohne es zuerst in den Papierkorb zu werfen.</p>

  <steps>
    <title>So löschen Sie eine Datei dauerhaft:</title>
    <item><p>Wählen Sie das zu löschende Objekt aus.</p></item>
    <item><p>Halten Sie die <key>Umschalttaste</key> gedrückt und drücken Sie dann die Taste <key>Entf</key> auf Ihrer Tastatur.</p></item>
    <item><p>Da Sie dies nicht rückgängig machen können, werden Sie um Bestätigung gebeten, dass die Datei oder der Ordner gelöscht werden soll.</p></item>
  </steps>

  <note><p>Gelöschte Dateien auf einem <link xref="files#removable">Wechseldatenträger</link> sind unter anderen Betriebssystemen, wie etwa Windows oder Mac OS, möglicherweise nicht sichtbar. Die Dateien sind aber immer noch da und verfügbar, wenn Sie das Gerät wieder an Ihren Rechner anschließen.</p></note>

  </section>

</page>
