<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wrongnetwork" xml:lang="sv">
  <info>

    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-problem"/>
    <link type="seealso" xref="net-wireless-connect"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Redigera dina anslutningsinställningar och ta bort det oönskade anslutningsalternativet.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Min dator ansluter till fel nätverk</title>

  <p>När du slår på din dator kommer datorn automatiskt att försöka att ansluta till trådlösa nätverk som du har anslutit till tidigare. Om den försöker att ansluta till ett nätverk som du inte vill ansluta till, följ stegen nedan.</p>

  <steps>
    <title>För att glömma en trådlös anslutning:</title>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Trådlöst nätverk</gui>.</p>
    </item>
    <item>
      <p>Hitta nätverket som du <em>inte</em> vill fortsätta att ansluta till.</p>
    </item>
    <item>
      <p>Klicka på knappen <media its:translate="no" type="image" src="figures/emblem-system.png"><span its:translate="yes">inställningar</span></media> intill nätverket.</p></item>
    <item>
      <p>Klicka på knappen <gui>Glöm anslutning</gui>. Din dator kommer inte att försöka att ansluta till nätverket mer.</p>
    </item>
  </steps>

  <p>Om du senare vill ansluta till nätverket som din dator nyss glömt, följ stegen i <link xref="net-wireless-connect"/>.</p>

</page>
