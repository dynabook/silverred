<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-open" xml:lang="fr">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-30" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Open files using an application that isn’t the default one for that
    type of file. You can change the default too.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Ouverture de fichiers avec une autre application</title>

  <p>When you double-click (or middle-click) a file in the file manager, it
  will be opened with the default application for that file type. You can open
  it in a different application, search online for applications, or set the
  default application for all files of the same type.</p>

  <p>To open a file with an application other than the default, right-click
  the file and select the application you want from the top of the menu. If
  you do not see the application you want, select <gui>Open With Other
  Application</gui>. By default, the file manager only shows applications that
  are known to handle the file. To look through all the applications on your
  computer, click <gui>View All Applications</gui>.</p>

<p>Si néanmoins vous ne trouvez toujours pas l'application voulue, cliquez sur <gui>Rechercher de nouvelles applications</gui>. Le gestionnaire de fichiers va chercher en ligne des paquets contenant des applications connues pour prendre en charge les fichiers de ce type.</p>

<section id="default">
  <title>Changement d'application par défaut</title>
  <p>Vous pouvez modifier l'application par défaut qui est utilisée pour ouvrir un certain type de fichier. Cela vous permet d'ouvrir votre application préférée quand vous faites un double-clic sur un fichier. Par exemple, vous pouvez lancer votre lecteur de musique préféré quand vous faites un double-clic sur un fichier MP3.</p>

  <steps>
    <item><p>Sélectionnez un type de fichier dont vous souhaitez changer l'application par défaut. Par exemple, pour définir l'application à lancer lors de l'ouverture d'un fichier MP3, sélectionnez un fichier <file>.mp3</file>.</p></item>
    <item><p>Faites un clic droit sur le fichier et choisissez <gui>Propriétés</gui>.</p></item>
    <item><p>Cliquez sur l'onglet <gui>Ouvrir avec</gui>.</p></item>
    <item><p>Choisissez l'application que vous voulez et cliquez sur <gui>Définir par défaut</gui>.</p>
    <p>Si <gui>Autres applications</gui> contient une application que vous voulez utiliser parfois, mais sans qu'elle soit par défaut, choisissez l'application et cliquez sur <gui>Ajouter</gui>. Cela l'ajoute aux <gui>Applications recommandées</gui>. Il vous sera désormais possible d'utiliser l'application par un clic droit sur le fichier et en la choisissant dans la liste.</p></item>
  </steps>

  <p>Cela change l'application par défaut pas uniquement pour le fichier sélectionné, mais pour tous les fichiers du même type.</p>

<!-- TODO: mention resetting the open with list with the "Reset" button -->

</section>

</page>
