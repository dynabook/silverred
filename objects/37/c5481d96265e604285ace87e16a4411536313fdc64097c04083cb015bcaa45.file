<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wrongnetwork" xml:lang="es">
  <info>

    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-problem"/>
    <link type="seealso" xref="net-wireless-connect"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Editar la configuración de su conexión y eliminar opciones de conexión no deseadas.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Mi equipo se conecta a una red incorrecta</title>

  <p>Al encender el equipo intentará automáticamente conectarse a redes inalámbricas que se ha conectado en el pasado. Si intenta conectarse a una red a la que quiere que se conecte haga lo siguiente:</p>

  <steps>
    <title>Para olvidar una conexión inalámbrica:</title>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Inalámbrica</gui>.</p>
    </item>
    <item>
      <p>Vaya a la pestaña <gui>Inalámbrica</gui> y busque la red a la que <em>no</em> quiere permanecer conectado.</p>
    </item>
    <item>
      <p>Pulse el botón <media its:translate="no" type="image" src="figures/emblem-system.png"><span its:translate="yes">configuración</span></media> que está junto a la red.</p></item>
    <item>
      <p>Pulse el botón <gui>olvidar conexión</gui>. Su equipo no volverá a intentar conectarse a esa red.</p>
    </item>
  </steps>

  <p>Si más adelante quiere conectarse a la red que su equipo ha olvidado, siga los pasos de la sección <link xref="net-wireless-connect"/>.</p>

</page>
