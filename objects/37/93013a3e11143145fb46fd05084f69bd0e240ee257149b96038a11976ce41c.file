<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="photo-take" xml:lang="gl">

  <info>
    <link type="guide" xref="index#main" group="#first"/>
    <link type="seealso" xref="pref-flash"/>
    <link type="seealso" xref="pref-countdown"/>
    <link type="seealso" xref="photo-view"/>
    <link type="seealso" xref="photo-delete"/>

    <revision pkgversion="3.0" date="2011-08-25" status="candidate"/>
    <revision pkgversion="3.1" date="2011-09-05" status="review"/>
    <revision pkgversion="3.8" date="2013-04-15" status="review"/>
    <revision pkgversion="3.12" date="2014-02-21" status="review"/>

    <credit type="author copyright">
      <name>Julita Inca</name>
      <email its:translate="no">yrazes@gmail.com</email>
      <years>2011</years>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Usar a súa cámara web para tomar fotos no lugar de vídeos.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Dieguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marcos Lans</mal:name>
      <mal:email>marcoslansgarza@gmail.com</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

  <title>Sacar fotos</title>

  <p>As cámaras web poden usarse para tomar fotos, así como vídeos. Para tomar unha foto:</p>

  <steps>
    <item>
      <p>Asegúrese que está en modo <gui>Foto</gui>. Se é así, o botón <gui>Foto</gui> estará seleccionado na pantalla.</p>
    </item>
    <item>
      <p>Prema o botón <gui style="button">Sacar unha foto usando a cámara web</gui> no medio do panel inferior ou prema a tecla <key>Espazo</key>.</p>
    </item>
    <item>
      <p>Haberá unha pequena conta atrás, seguida dun flash e logo aparecerá a foto na tira de fotos.</p>
    </item>
  </steps>

  <p>As fotos na tira de fotos gárdanse automaticamente no cartafol <file>Pictures/Webcam</file> do seu cartafol persoal. Gardaranse no formato JPEG (<file>.jpg</file>).</p>

  <p>Para cancelar a toma dunha foto despois de que premeu <gui>Tomar unha foto</gui> prema <key>Esc</key> antes de que a conta atrás remate.</p>

  <note>
    <p>Para tomar varias fotos rapidamente, use o <link xref="burst-mode"><gui>Modo ráfaga</gui> </link>.</p>
  </note>

</page>
