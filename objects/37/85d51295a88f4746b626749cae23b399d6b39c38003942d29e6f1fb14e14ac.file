<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting-hardware-info" xml:lang="nl">

  <info>
    <link type="next" xref="net-wireless-troubleshooting-hardware-check"/>
    <link type="guide" xref="net-wireless-troubleshooting"/>

    <revision pkgversion="3.4.0" date="2012-03-05" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Mensen die bijdragen aan de Ubuntu-documentatie wiki</name>
    </credit>
    <credit type="author">
      <name>Gnome-documentatieproject</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Voor het oplossen van problemen heeft u wellicht details nodig zoals het modelnummer van uw draadloos netwerk-adapter.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Probleemoplosser draadloos netwerk</title>
  <subtitle>Informatie verzamelen over uw netwerkhardware</subtitle>

  <p>In deze stap gaat u informatie verzamelen over uw draadloze netwerkapparaat. De manier waarop vele draadloze problemen opgelost worden hangt af van het merk en modelnummer van de draadloze adapter, daarom dient u een notitie te maken van deze details. Het kan ook helpen om sommige dingen die bij uw computer zaten erbij te zoeken, zoals stuurprogramma-installatieschijven. Zoek naar de volgende items, indien u deze nog heeft:</p>

  <list>
    <item>
      <p>De verpakking en instructies van uw draadloos apparaat (vooral de gebruikershandleiding van uw router)</p>
    </item>
    <item>
      <p>De schijf met stuurprogramma's voor uw draadloze adapter (zelfs als deze alleen maar Windows-stuurprogramma's bevat)</p>
    </item>
    <item>
      <p>De fabrikant en de modelnummers van uw computer, draadloze adapter en router. Deze informatie kan in het algemeen gevonden worden onderop/achterop het apparaat.</p>
    </item>
    <item>
      <p>Versienummers van uw draadloze apparaten, die op uw apparaten of hun verpakking staan. Vooral deze helpen, dus zoek hier goed naar.</p>
    </item>
    <item>
      <p>Alles op de schijf met stuurprogramma's waardoor of het apparaat zelf, de ‘firmware’-versie of de door het apparaat gebruikte componenten (chipset) kunnen worden geïdentificeerd.</p>
    </item>
  </list>

  <p>Indien mogelijk, probeer om toegang te krijgen tot een werkende internetverbinding zodat u software en stuurprogramma's kunt downloaden als dat nodig is. (Uw computer direct met een kabel op de router aansluiten is één van de mogelijkheden, maar doe dit alleen wanneer dit nodig is.)</p>

  <p>Als u zoveel mogelijk informatie verzameld heeft, klikt u op <gui>Volgende</gui>.</p>

</page>
