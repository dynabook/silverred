<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-where" xml:lang="ja">

  <info>
    <link type="guide" xref="backup-why"/>
    <title type="sort">c</title>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>GNOME ドキュメンテーションプロジェクト</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>バックアップの保存場所、および使用するストレージデバイスの種類に関するアドバイスです。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>松澤 二郎</mal:name>
      <mal:email>jmatsuzawa@gnome.org</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>赤星 柔充</mal:name>
      <mal:email>yasumichi@vinelinux.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kentaro KAZUHAMA</mal:name>
      <mal:email>kazken3@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shushi Kurose</mal:name>
      <mal:email>md81bird@hitaki.net</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Noriko Mizumoto</mal:name>
      <mal:email>noriko@fedoraproject.org</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>坂本 貴史</mal:name>
      <mal:email>o-takashi@sakamocchi.jp</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>日本GNOMEユーザー会</mal:name>
      <mal:email>http://www.gnome.gr.jp/</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>バックアップの保存先</title>

  <p>You should store backup copies of your files somewhere separate from your
 computer — on an external hard disk, for example. That way, if the computer
 breaks, the backup will still be intact. For maximum security, you shouldn’t
 keep the backup in the same building as your computer. If there is a fire or
 theft, both copies of the data could be lost if they are kept together.</p>

  <p>適切な<em>バックアップメディア</em>を選択することも重要です。バックアップファイルをすべて保存するのに十分なディスク容量のあるデバイスに保存する必要があります。</p>

   <list style="compact">
    <title>ローカルストレージおよびリモートストレージの選択肢</title>
    <item>
      <p>USB メモリ (小容量)</p>
    </item>
    <item>
      <p>内蔵ディスクドライブ (大容量)</p>
    </item>
    <item>
      <p>外付けハードディスク (一般に大容量)</p>
    </item>
    <item>
      <p>ネットワーク接続ドライブ (大容量)</p>
    </item>
    <item>
      <p>ファイルサーバー/バックアップサーバー (大容量)</p>
    </item>
    <item>
     <p>書き込み可能な CD または DVD (小/中容量)</p>
    </item>
    <item>
     <p>Online backup service
     (<link href="http://aws.amazon.com/s3/">Amazon S3</link>, for example;
     capacity depends on price)</p>
    </item>
   </list>

  <p>この選択肢のなかには、<em>完全システムバックアップ</em>とも言われる、お使いのシステムの全ファイルをバックアップするのに十分な容量を持っているものもあります。</p>
</page>
