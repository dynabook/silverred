<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-capacity" xml:lang="pt-BR">
  <info>
    <link type="guide" xref="disk"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <revision pkgversion="3.4.3" date="2012-06-15" status="review"/>
    <revision pkgversion="3.13.91" date="2014-09-05" status="review"/>

    <desc>Use o <gui>Analisador de uso de disco</gui> ou o <gui>Monitor do sistema</gui> para verificar o espaço e a capacidade.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

<title>Verificando quanto resta de espaço em disco</title>

  <p>Você pode verificar quanto de espaço em disco está livre com <app>Analisador de uso do disco</app> ou o <app>Monitor do sistema</app>.</p>

<section id="disk-usage-analyzer">
<title>Verificar com o Analisador de uso de disco</title>

  <p>Para verificar o espaço livre e a capacidade do disco usando o <app>Analisador de uso de disco</app>:</p>

  <list>
    <item>
      <p>Abra o <app>Analisador de uso de disco</app> a partir do panorama de <gui>Atividades</gui>. A janela mostrará uma lista de localizações de arquivos junto ao uso e capacidade de cada um.</p>
    </item>
    <item>
      <p>Clique em um dos itens na lista para visualizar um resumo detalhado do use para aquele item. Clique no botão de menu e, então, <gui>Varrer pasta…</gui> ou <gui>Varrer pasta remota…</gui> para varrer uma localização diferente.</p>
    </item>
  </list>
  <p>As informações são mostradas de acordo com a <gui>Pasta</gui>, <gui>Tamanho</gui>, <gui>Conteúdo</gui> e, quando o arquivo foi <gui>Modificado</gui> pela última vez. Veja mais detalhes em <link href="help:baobab"><app>Analisador do uso do disco</app></link>.</p>

</section>

<section id="system-monitor">

<title>Verificar com o Monitor de sistema</title>

  <p>Para verificar o espaço livre e a capacidade do disco com o <app>Monitor do sistema</app>:</p>

<steps>
 <item>
  <p>Abra o aplicativo <app>Monitor do sistema</app> a partir do panorama <gui>Atividades</gui>.</p>
 </item>
 <item>
  <p>Selecione a aba <gui>Sistemas de arquivos</gui> para ver as partições do sistema e o uso do espaço em disco. As informações são mostradas de acordo com o <gui>Total</gui>, <gui>Livre</gui>, <gui>Disponível</gui> e <gui>Em uso</gui>.</p>
 </item>
</steps>
</section>

<section id="disk-full">

<title>E se o disco estiver cheio demais?</title>

  <p>Se o disco estiver cheio demais, você deveria:</p>

 <list>
  <item>
   <p>Excluir arquivos que não são importantes ou que não vá mais usar.</p>
  </item>
  <item>
   <p>Fazer <link xref="backup-why">cópias de segurança</link> dos arquivos importantes que não vai precisar por um tempo e excluí-los do disco rígido.</p>
  </item>
 </list>
</section>

</page>
