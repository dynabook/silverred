<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-add" xml:lang="cs">

  <info>
    <link type="guide" xref="user-accounts#manage" group="#first"/>

    <revision pkgversion="3.8" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2013-11-01" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak přidat nové uživatele, aby se další lidé mohli přihlásit k počítači.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Přidání nového uživatelského účtu</title>

  <p>Do svého počítače můžete přidat více uživatelských účtů. Každé osobě v domácnosti nebo v práci přidělte jeden účet. Každý uživatel tak bude mít svoji vlastní domovskou složku, dokumenty a nastavení.</p>

  <p>Abyste mohli přidat účet uživatele, potřebujete <link xref="user-admin-explain">oprávnění správce</link>.</p>

  <steps>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Uživatelé</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Uživatelé</gui> otevřete příslušný panel.</p>
    </item>
    <item>
      <p>Zmáčkněte <gui style="button">Odemknout</gui> v pravém horním rohu a po vyzvání zadejte heslo.</p>
    </item>
    <item>
      <p>Pro přidání nového účtu zmáčkněte tlačítko <gui style="button">+</gui> vlevo pod seznamem účtů.</p>
    </item>
    <item>
      <p>Pokud chcete, aby měl nový uživatel k počítači <link xref="user-admin-explain">přístup správce</link>, vyberte u typu účtu <gui>Správce</gui>.</p>
      <p>Správce může dělat věci, jako je přidávání a odebírání uživatelů, instalace softwaru a ovladačů a změna data a času.</p>
    </item>
    <item>
      <p>Zadejte celé jméno uživatele. Uživatelské jméno bude vyplněno automaticky na základě celého jména. Pokud vám navrhované nebude vyhovovat, můžete jej samozřejmě změnit.</p>
    </item>
    <item>
      <p>Heslo novému uživateli můžete nastavit, nebo ponechat na něm, ať si jej nastaví při prvním přihlášení.</p>
      <p>Když zvolíte, že chcete nastavit heslo teď, můžete zmáčknout ikonu <gui style="button"><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16">
      <span its:translate="yes">generovat heslo</span></media></gui>, aby se vám automaticky vygenerovalo náhodné heslo.</p>
    </item>
    <item>
      <p>Klikněte na <gui>Přidat</gui>.</p>
    </item>
  </steps>

  <p>Když chcete změnit heslo po té, co byl účet vytvořen, vyberte účet, <gui style="button">Odekmkněte</gui> panel a zmáčkněte aktuální heslo.</p>

  <note>
    <p>V panelu <gui>Uživatelé</gui> můžete kliknout na obrázek vpravo vedle jména uživatele a nastavit účtu jiný obrázek. Ten se pak bude zobrazovat v přihlašovacím okně. GNOME poskytuje základní výběr obrázků, které můžete přímo použít nebo si můžete vybrat nějaký vlastní nebo si pořídit svoji fotografii pomocí webové kamery.</p>
  </note>

</page>
