<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-admin-change" xml:lang="ca">

  <info>
    <link type="guide" xref="user-accounts#privileges"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Podeu permetre als usuaris fer canvis al sistema donant-los privilegis administratius.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

  <title>Canviar qui té privilegis administratius</title>

  <p>Els privilegis administratius són una manera de decidir qui pot fer canvis en parts importants del sistema. Podeu canviar quins usuaris tenen privilegis administratius i quins no. Són una bona manera de mantenir el sistema segur i evitar possibles danys de canvis no autoritzats.</p>

  <p>Heu de tenir <link xref="user-admin-explain">privilegis administratius</link> per canviar el tipus de compte.</p>

  <steps>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a teclejar <gui>Usuaris</gui>.</p>
    </item>
    <item>
      <p>Feu clic <gui>Usuaris</gui> per obrir el quadre.</p>
    </item>
    <item>
      <p>Premeu <gui style="button">Desbloqueja</gui> a l'extrem superior dret i escriviu la vostra contrasenya quan se us demani.</p>
    </item>
    <item>
      <p>Seleccioneu l'usuari els privilegis que voleu canviar.</p>
    </item>
    <item>
      <p>Feu clic a l'etiqueta <gui>Estàndard</gui> al costat del <gui>Tipus de compte</gui> i seleccioneu <gui>Administrador</gui>.</p>
    </item>
    <item>
      <p>Els privilegis de l'usuari es canviaran quan es torni a iniciar la sessió.</p>
    </item>
  </steps>

  <note>
    <p>El primer compte d'usuari del sistema sol ser el que té privilegis d'administrador. Aquest és el compte d'usuari que es va crear quan vàreu instal·lar el sistema per primera vegada.</p>
    <p>No és aconsellable tenir massa usuaris amb privilegis d'<gui>Administrador</gui> en un sistema.</p>
  </note>

</page>
