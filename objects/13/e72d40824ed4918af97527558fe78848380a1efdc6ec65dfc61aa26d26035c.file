<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="keyboard-osk" xml:lang="sv">

  <info>
    <link type="guide" xref="keyboard" group="a11y"/>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>

    <credit type="author">
      <name>Jeremy Bicha</name>
      <email>jbicha@ubuntu.com</email>
    </credit>
    <credit type="author">
      <name>Julita Inca</name>
      <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Använd ett skärmtangentbord för att mata in text genom att klicka på knappar med musen eller en pekskärm.</desc>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Använd ett skärmtangentbord</title>

  <p>Om du inte har ett tangentbord anslutet till din dator eller föredrar att inte använda det kan du aktivera <em>skärmtangentbordet</em> för att mata in text.</p>

  <note>
    <p>Skärmtangentbordet aktiveras automatiskt om du använder en pekskärm</p>
  </note>

  <steps>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Inställningar</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Inställningar</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Hjälpmedel</gui> i sidopanelen för att öppna panelen.</p>
    </item>
    <item>
      <p>Slå på <gui>Skärmtangentbord</gui> i avsnittet <gui>Skriva</gui>.</p>
    </item>
  </steps>

  <p>Vid nästa inmatningstillfälle, kommer skärmtangentbordet att öppnas i botten på skärmen.</p>

  <p>Tryck på <gui style="button">?123</gui>-knappen för att mata in nummer och symboler. Fler symboler är tillgängliga om du sedan trycker på <gui style="button">=/&lt;</gui>-knappen. För att återgå till det alfabetiska tangentbordet, tryck på <gui style="button">ABC</gui>-knappen.</p>

  <p>Du kan trycka på <gui style="button"><media its:translate="no" type="image" src="figures/go-down-symbolic.svg" width="16" height="16"><span its:translate="yes">ner</span></media></gui>-knappen för att gömma tangentbordet tillfälligt. Tangentbordet kommer att visas igen automatiskt nästa gång du trycker på något ställe där du kan använda det.</p>
  <p>Tryck på <gui style="button"><media its:translate="no" type="image" src="figures/emoji-flags-symbolic.svg" width="16" height="16"><span its:translate="yes">flagga</span></media></gui>-knappen för att ändra dina inställningar för <link xref="session-language">Språk</link> eller <link xref="keyboard-layouts">Inmatningskällor</link>.</p>
  <!-- obsolete <p>To make the keyboard show again, open the
  <link xref="shell-notifications">messagetray</link> (by moving your mouse to
  the bottom of the screen), and press the keyboard icon.</p> -->

</page>
