<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:itst="http://itstool.org/extensions/" type="topic" style="task" id="net-proxy" xml:lang="mr">

  <info>
    <its:rules xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Baptiste Mille-Mathias</name>
      <email>baptistem@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>A proxy is an intermediary for web traffic, it can be used for accessing web services anonymously, for control or security purposes.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

  <title>प्रॉक्सी सेटिंग निश्चित करा</title>

<section id="what">
  <title>प्रॉक्सी काय असते?</title>

  <p>A <em>web proxy</em> filters websites that you look at, it receives
  requests from your web browser to fetch web pages and their elements, and
  following a policy will decide to pass them you back. They are commonly used
  in businesses and at public wireless hotspots to control what websites you
  can look at, prevent you from accessing the internet without logging in, or
  to do security checks on websites.</p>

</section>

<section id="change">
  <title>प्रॉक्सी पद्धत बदला</title>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Network</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Network</gui> to open the panel.</p>
    </item>
    <item>
      <p>Select <gui>Network proxy</gui> from the list on the left.</p>
    </item>
    <item>
      <p>Choose which proxy method you want to use from:</p>
      <terms>
        <item>
          <title><gui itst:context="proxy">None</gui></title>
          <p>The applications will use a direct connection to fetch the content
          from the web.</p>
        </item>
        <item>
          <title><gui>मॅन्युअल</gui></title>
          <p>प्रत्येक प्रॉक्सीड प्रोटोकॉलकरिता, प्रॉक्सीचा पत्ता आणि प्रोटोकॉल्सकरिता पोर्ट ठरवा. प्रोटोकॉल्स <gui>HTTP</gui>, <gui>HTTPS</gui>, <gui>FTP</gui> आणि <gui>SOCKS</gui> आहे.</p>
        </item>
        <item>
          <title><gui>आपोआप</gui></title>
          <p>A URL pointing to a resource, which contains the appropriate
          configuration for your system.</p>
        </item>
      </terms>
    </item>
  </steps>

  <p>नेटवर्क जोडणीचा वापर करणारे ॲप्लिकेशन्स निर्देशीत प्रॉक्सी सेटिंग्जचा वापर करेल.</p>

</section>

</page>
