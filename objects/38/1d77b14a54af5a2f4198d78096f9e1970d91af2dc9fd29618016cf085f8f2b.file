<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-use-windows-workspaces" xml:lang="sk">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>
    <link type="guide" xref="getting-started" group="tasks"/>
    <title role="trail" type="link">Používanie okien a pracovných priestorov</title>
    <link type="seealso" xref="shell-windows-switching"/>
    <title role="seealso" type="link">Návod na používanie okien a pracovných priestorov</title>
    <link type="next" xref="gs-use-system-search"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Richard Stanislavský</mal:name>
      <mal:email>kenny.vv@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>Používanie okien a pracovných priestorov</title>

  <ui:overlay width="812" height="452">
  <media type="video" its:translate="no" src="figures/gnome-windows-and-workspaces.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-windows-and-workspaces.svg"/>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>Okná a pracovné priestory</tt:p>
         </tt:div>
         <tt:div begin="6s" end="10s">
           <tt:p>To maximize a window, grab the window’s titlebar and drag it to
            the top of the screen.</tt:p>
           </tt:div>
         <tt:div begin="10s" end="13s">
           <tt:p>Keď bude obrazovka zvýraznená, pustíme okno.</tt:p>
         </tt:div>
         <tt:div begin="14s" end="20s">
           <tt:p>To unmaximize a window, grab the window’s titlebar and drag it
            away from the edges of the screen.</tt:p>
         </tt:div>
         <tt:div begin="25s" end="29s">
           <tt:p>Tiež môžeme kliknutím na horný panel okno potiahnuť preč a obnoviť tak jeho veľkosť.</tt:p>
         </tt:div>
         <tt:div begin="34s" end="38s">
           <tt:p>To maximize a window along the left side of the screen, grab
            the window’s titlebar and drag it to the left.</tt:p>
         </tt:div>
         <tt:div begin="38s" end="40s">
           <tt:p>Keď je polovica obrazovky zvýraznená, pustíme okno.</tt:p>
         </tt:div>
         <tt:div begin="41s" end="44s">
           <tt:p>To maximize a window along the right side of the screen, grab
            the window’s titlebar and drag it to the right.</tt:p>
         </tt:div>
         <tt:div begin="44s" end="48s">
           <tt:p>Keď je polovica obrazovky zvýraznená, pustíme okno.</tt:p>
         </tt:div>
         <tt:div begin="54s" end="60s">
           <tt:p>Keď chceme okno maximalizovať pomocou klávesnice, podržíme stlačený kláves <key href="help:gnome-help/keyboard-key-super">Super</key> a zároveň stlačíme <key>↑</key>.</tt:p>
         </tt:div>
         <tt:div begin="61s" end="66s">
           <tt:p>Keď chceme obnoviť pôvodnú veľkosť okna, podržíme stlačený kláves <key href="help:gnome-help/keyboard-key-super">Super</key> a zároveň stlačíme <key>↓</key>.</tt:p>
         </tt:div>
         <tt:div begin="66s" end="73s">
           <tt:p>Keď chceme okno maximalizovať pozdĺž pravej strany obrazovky, podržíme stlačený kláves <key href="help:gnome-help/keyboard-key-super">Super</key> a zároveň stlačíme <key>→</key>.</tt:p>
         </tt:div>
         <tt:div begin="76s" end="82s">
           <tt:p>Keď chceme okno maximalizovať pozdĺž ľavej strany obrazovky, podržíme stlačený kláves <key href="help:gnome-help/keyboard-key-super">Super</key> a zároveň stlačíme <key>←</key>.</tt:p>
         </tt:div>
         <tt:div begin="83s" end="89s">
           <tt:p>Na pracovný priestor, ktorý je pod aktuálnym, sa presunieme stlačením kombinácie kláves <keyseq><key href="help:gnome-help/keyboard-key-super">Super </key><key>Page Down</key></keyseq>.</tt:p>
         </tt:div>
         <tt:div begin="90s" end="97s">
           <tt:p>Na pracovný priestor, ktorý je nad aktuálnym, sa presunieme stlačením kombinácie kláves <keyseq><key href="help:gnome-help/keyboard-key-super">Super </key><key>Page Up</key></keyseq>.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>
  
  <section id="use-workspaces-and-windows-maximize">
    <title>Maximalizovanie a obnovenie okna</title>
    <p/>
    
    <steps>
      <item><p>To maximize a window so that it takes up all of the space on
       your desktop, grab the window’s titlebar and drag it to the top of the
       screen.</p></item>
      <item><p>Ak je obrazovka zvýraznená, pustíme okno aby sa maximalizovalo.</p></item>
      <item><p>To restore a window to its unmaximized size, grab the window’s
       titlebar and drag it away from the edges of the screen.</p></item>
    </steps>
    
  </section>

  <section id="use-workspaces-and-windows-tile">
    <title>Usporiadanie okien do dlaždíc</title>
    <p/>
    
    <steps>
      <item><p>To maximize a window along a side of the screen, grab the window’s
       titlebar and drag it to the left or right side of the screen.</p></item>
      <item><p>Keď bude polovica obrazovky zvýraznená, pustíme okno aby sa maximalizovalo pozdĺž vybranej strany obrazovky.</p></item>
      <item><p>Keď chceme maximalizovať dve okná vedľa seba, chytíme titulok druhého okna a presunieme okno na opačný okraj obrazovky.</p></item>
       <item><p>Keď je polovica okna zvýraznená, pustíme okno aby sa maximalizovalo pozdĺž opačnej strany obrazovky.</p></item>
    </steps>
    
  </section>
  
  <section id="use-workspaces-and-windows-maximize-keyboard">
    <title>Maximalizácia a obnovenie okna použitím klávesnice</title>
    
    <steps>
      <item><p>Keď chceme okno maximalizovať pomocou klávesnice, podržíme stlačený kláves <key href="help:gnome-help/keyboard-key-super">Super</key> a zároveň stlačíme <key>↑</key>.</p></item>
      <item><p>Keď chceme pomocou klávesnice obnoviť pôvodnú veľkosť okna, podržíme stlačený kláves <key href="help:gnome-help/keyboard-key-super">Super</key> a zároveň stlačíme <key>↓</key>.</p></item>
    </steps>
    
  </section>
  
  <section id="use-workspaces-and-windows-tile-keyboard">
    <title>Usporiadanie okine do dlaždíc použitím klávesnice</title>
    
    <steps>
      <item><p>Keď chceme okno maximalizovať pozdĺž pravej strany obrazovky, podržíme stlačený kláves <key href="help:gnome-help/keyboard-key-super">Super</key> a zároveň stlačíme <key>→</key>.</p></item>
      <item><p>Keď chceme okno maximalizovať pozdĺž ľavej strany obrazovky, podržíme stlačený kláves <key href="help:gnome-help/keyboard-key-super">Super</key> a zároveň stlačíme <key>←</key>.</p></item>
    </steps>
    
  </section>
  
  <section id="use-workspaces-and-windows-workspaces-keyboard">
    <title>Prepínanie pracovných priestorov pomocou klávesnice</title>
    
    <steps>
    
    <item><p>Na pracovný priestor, ktorý je pod aktuálnym, sa presunieme stlačením kombinácie kláves <keyseq><key href="help:gnome-help/keyboard-key-super">Super</key><key>Page Down</key></keyseq>.</p></item>
    <item><p>Na pracovný priestor, ktorý je nad aktuálnym, sa presunieme stlačením kombinácie kláves <keyseq><key href="help:gnome-help/keyboard-key-super">Super</key><key>Page Up</key></keyseq>.</p></item>

    </steps>
    
  </section>

</page>
