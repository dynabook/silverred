<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-lost" xml:lang="sv">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>

    <revision pkgversion="3.8.0" date="2013-04-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Dokumentationsprojekt för GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Kontrollera översiktsvyn <gui>Aktiviteter</gui> eller andra arbetsytor.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Hitta ett förlorat fönster</title>

  <p>Ett fönster på en annan arbetsyta, eller gömt bakom ett annat fönster, hittas enkelt via översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui>:</p>

  <list>
    <item>
      <p>Öppna översiktsvyn <gui>Aktiviteter</gui>. Om det saknade fönstret finns på den aktuella <link xref="shell-windows#working-with-workspaces">arbetsytan</link> kommer det att visas här som en miniatyrbild. Klicka helt enkelt på miniatyrbilden för att åter visa fönstret, eller</p>
    </item>
    <item>
      <p>Klicka på olika arbetsytor i <link xref="shell-workspaces">arbetsyteväxlaren</link> till höger på skärmen för att försöka hitta ditt fönster, eller</p>
    </item>
    <item>
      <p>Högerklicka på programmet i snabbstartspanelen så kommer dess öppna fönster att listas. Klicka på fönstret i listan för att växla till det.</p>
    </item>
  </list>

  <p>Använda fönsterväxlaren:</p>

  <list>
    <item>
      <p>Tryck <keyseq><key xref="keyboard-key-super">Super</key><key>Tabb</key></keyseq> för att visa <link xref="shell-windows-switching">fönsterväxlaren</link>. Fortsätt att hålla ner <key>Super</key> och tryck på <key>Tabb</key> för att gå igenom de öppna fönstren, eller <keyseq><key>Skift</key><key>Tabb</key> </keyseq> för att gå baklänges.</p>
    </item>
    <item if:test="!platform:gnome-classic">
      <p>Om ett program att flera öppna fönster, håll ner <key>Super</key> och tryck <key>`</key> (eller tangenten ovanför <key>Tabb</key>) för att bläddra genom dem.</p>
    </item>
  </list>

</page>
