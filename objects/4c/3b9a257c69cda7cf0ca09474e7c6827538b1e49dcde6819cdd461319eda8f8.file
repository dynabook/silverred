<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-purge" xml:lang="cs">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.10" date="2013-09-29" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shaun McCance</name>
      <email>mdhillca@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak nastavit, jak často se má vysypávat koš a mají mazat dočasné soubory ve vašem počítači.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Vysypání koše a smazání dočasných souborů</title>

  <p>Vyčištěním koše a dočasných souborů se odstraní nechtěné a nepotřebné soubory z vašeho počítače a také se tím uvolní nějaké místo na disku. Vysypat koš a odstranit dočasné soubory můžete ručně, ale lze také nastavit, aby to počítač dělal automaticky za vás.</p>

  <p>Dočasné soubory jsou soubory, které si aplikace vytvářejí automaticky na pozadí. Mohou například zlepšit odezvu tím, že poskytnou okamžitě kopii dat, která se už dříve stahovala nebo vypočítávala.</p>

  <steps>
    <title>Automatické vyprázdnění koše a odstranění dočasných souborů</title>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Soukromí</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Soukromí</gui> otevřete příslušný panel.</p>
    </item>
    <item>
      <p>Vyberte <gui>Čištění koše a dočasných souborů</gui>.</p>
    </item>
    <item>
      <p>Přepněte jeden, nebo oba, z vypínačů <gui>Automaticky vyprázdnit koš</gui> a <gui>Automaticky odstranit dočasné soubory</gui> do polohy zapnuto.</p>
    </item>
    <item>
      <p>Pomocí hodnoty <gui>Vyčistit po</gui> nastavte, jak často by se měly <em>Koš</em> a <em>Dočasné soubory</em> čistit.</p>
    </item>
    <item>
      <p>K okamžitému vyčištění můžete použít tlačítka <gui>Vyprázdnit koš</gui> a <gui>Odstranit dočasné soubory</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Soubory můžete mazat ihned a trval bez použití Koše. Jak na to viz <link xref="files-delete#permanent"/>.</p>
  </note>

</page>
