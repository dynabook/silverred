<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="dconf-profiles" xml:lang="es">

  <info>
    <link type="guide" xref="setup"/>
    <link type="seealso" xref="dconf-custom-defaults"/>
    <link type="seealso" xref="dconf"/>
    <revision pkgversion="3.30" date="2019-02-22" status="incomplete"/>

    <credit type="author copyright">
      <name>Ryan Lortie</name>
      <email>desrt@desrt.ca</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Švárová</name>
      <email>jana.svarova@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Información detallada sobre la selección del perfil.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Oliver Gutiérrez</mal:name>
      <mal:email>ogutsua@gmail.com</mal:email>
      <mal:years>2018 - 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2017 - 2019</mal:years>
    </mal:credit>
  </info>

  <title>Perfiles</title>

  <p>Un <em>perfil</em> es una lista de bases de datos de configuración. La primera base de datos en un perfil es la base de datos de escritura, y el resto de bases de datos son de sólo lectura. Cada una de las bases de datos del sistema se genera a partir de una carpeta de archivos de claves. Cada carpeta de archivos de claves contiene uno o más archivos de claves. Cada archivo de claves contiene al menos una ruta de dconf y una o más claves con sus correspondientes valores.</p>

  <p>Las parejas de claves que se configuran en un <em>perfil</em> de <sys>dconf</sys> ignorarán los ajustes predeterminados excepto si hay un problema con el valor que usted haya configurado.</p>

  <p>Normalmente querrá que su perfil de <sys>dconf</sys> esté compuesto de una <em>base de datos de usuario</em> y al menos una base de datos de sistema. El perfil debe listar una base de datos por línea.</p>

  <p>La primera línea en un perfil es la base de datos en la que se escribirán los cambios. Normalmente es <code>user-db:<input>user</input></code>. <input>user</input> es el nombre de la base de datos del usuario que normalmente se encuentra en <file>~/.config/dconf</file>.</p>

  <p>Una línea <code>system-db</code> especifica una base de datos del sistema. Dichas bases de datos se encuentran en <file>/etc/dconf/db/</file>.</p>

  <example>
    <listing>
      <title>Perfil de ejemplo</title>
<code its:translate="no">
user-db:user
system-db:<var>local</var>
system-db:<var>site</var>
</code>
    </listing>
  </example>

  <!-- TODO: explain the profile syntax (maybe new page) -->
  <!--TODO: explain local and site -->

  <p>Configurar una sola base de datos de usuario y múltiples bases de datos de sistema permite hacer capas de preferencias. Los ajustes del archivo de base de datos <code>user</code> tendrá preferencia sobre los ajustes del archivo de base de datos <code>local</code>, y el archivo de base de datos <code>local</code> a su vez tendrá preferencia sobre el archivo de base de datos <code>site</code>.</p>

  <p>Sin embargo, el orden de precedencia para los <link xref="dconf-lockdown">bloqueos</link> está invertido. Los bloqueos introducidos en los archivos de base de datos <code>site</code> o <code>local</code> tienen preferencia sobre aquellas presentes en <code>user</code>.</p>

  <note style="important">
  <p>El perfil de <sys>dconf</sys> de una sesión se determina al iniciar sesión, por lo tanto los usuarios deberán cerrar la sesión actual y volver a iniciar sesión para aplicar un nuevo perfil de <sys>dconf</sys> a su sesión.</p>
  </note>

  <p>Para obtener más información, consulte la página del manual <link its:translate="no" href="man:dconf(7)">
  <cmd>dconf</cmd>(7)</link>.</p>

  <section id="dconf-profiles">

  <title>Seleccionar un perfil</title>

  <p>Al inicio, <sys>dconf</sys> consulta la variable de entorno <sys>DCONF_PROFILE</sys>. Dicha variable puede especificar una ruta relativa a un archivo en  <file>/etc/dconf/profile/</file> o una ruta absoluta, por ejemplo al directorio personal del usuario.</p>

  <p>Si la variable de entorno está presente, <sys>dconf</sys> intentará abrir el perfil especificado y acabará si la operación falla. Si la variable no está configurada, <sys>dconf</sys> intentará abrir el perfil llamado "user". Si esto falla, usará una configuración interna preconfigurada.</p>

  <p>Para obtener más información, consulte la página del manual <link its:translate="no" href="man:dconf(7)">
  <cmd>dconf</cmd>(7)</link>.</p>

  </section>

</page>
