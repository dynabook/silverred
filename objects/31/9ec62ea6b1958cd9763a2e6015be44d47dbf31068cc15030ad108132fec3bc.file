<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-frequency" xml:lang="ca">

  <info>
    <link type="guide" xref="files#backup"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Conegueu amb quina freqüència hauríeu de fer còpia de seguretat dels vostres fitxers importants per assegurar-vos que estiguin protegits.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

<title>Freqüència de les còpies de seguretat</title>

  <p>La freqüència en què feu còpies de seguretat depèn del tipus d'informació a copiar. Si, per exemple, esteu en un entorn en xarxa amb informació crítica emmagatzemada als vostres servidors, potser ni tan sols efectuant còpies de seguretat cada nit serà suficient.</p>

  <p>Per altra banda, si feu les còpies de seguretat de la informació que teniu a l'ordinador personal, és molt possible que fer còpies de seguretat cada hora sigui innecessari. Considereu els següents punts a l'hora de determinar la política de còpies de seguretat que voldreu aplicar:</p>

<list style="compact">
<item><p>La quantitat de temps que passeu a l'ordinador.</p></item>
<item><p>En quina freqüència i en quina mesura canvia la informació que teniu a l'ordinador.</p></item>
</list>

  <p>Si la informació que voleu assegurar és de baixa prioritat, o canvia poc, com per exemple música, correus electrònics i fotos de la família, una còpia de seguretat setmanal o, fins i tot, mensual, serà suficient. Si, en canvi, esteu preparant la declaració d'hisenda, potser cal fer les còpies amb més freqüència.</p>

  <p>Com a regla general, el temps que passa entre dues còpies de seguretat no hauria de ser superior al temps que voleu dedicar a refer la feina que hàgiu perdut. Així, si per exemple considereu que passar-vos una setmana tornant a escriure documents perduts és massa temps, hauríeu de plantejar-vos fer les còpies de seguretat una vegada a la setmana com a mínim.</p>

</page>
