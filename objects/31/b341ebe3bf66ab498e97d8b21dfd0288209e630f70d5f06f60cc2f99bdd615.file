<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="autostart-applications" xml:lang="cs">

  <info>
    <link type="guide" xref="software#management"/>
    <revision pkgversion="3.30" date="2019-02-08" status="draft"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="author copyright">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak přidat automaticky spouštěnou aplikaci pro všechny uživatele.</desc>
  </info>

  <title>Přidání automaticky spouštěné aplikace všem uživatelům</title>

  <p>Když chcete, aby se aplikace automaticky spustila, když se uživatel přihlásí, musíte pro ni vytvořit soubor <file>.desktop</file> ve složce <file>/etc/xdg/autostart/</file>.</p>

<steps>
  <title>Když chcete přidat pro všechny uživatele automaticky spouštěnou aplikaci:</title>
  <item><p>Vytvořte soubor <file>.desktop</file> ve složce <file>/etc/xdg/autostart</file>:</p>
<code>[Desktop Entry]
Type=Application
Name=<var>Files</var>
Exec=<var>nautilus -n</var>
OnlyShowIn=GNOME;
AutostartCondition=<var>GSettings org.gnome.desktop.background show-desktop-icons</var></code>
  </item>
  <item><p>Nahraďte <var>Files</var> názvem aplikace.</p></item>
  <item><p>Nahraďte <var>nautilus -n</var> příkazem, kterým si přejete aplikaci spouštět.</p></item>
  <item><p>Klíč <code>AutostartCondition</code> můžete použít ke kontrole hodnoty klíče GSettings.</p>
  <p>Správa sezení spustí aplikaci automaticky, pokud je hodnota klíče nastavená na pravda. Jestliže ke změně hodnoty dojde za běhu sezení, správa sezení aplikaci spustí, nebo zastaví, v závislosti na předchozí hodnotě klíče.</p>
</item>
</steps>

</page>
