<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-othersconnect" xml:lang="gu">

  <info>
    <link type="guide" xref="net-problem"/>
    <link type="seealso" xref="net-othersedit"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>ફીલ બુલ</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>માઇકલ હીલ</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>ઍકાટેરીના ગેરાસીમોવા</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>તમે નેટવર્ક જોડાણ માટે સુયોજનો (જેમ કે પાસવર્ડ) ને સંગ્રહી શકો છો તેથી તે દરેક કે જે કમ્પ્યૂટરને વાપરે છે કે તેની સાથે જોડાવા માટે સક્ષમ હશે.</desc>
  </info>

  <title>Other users can’t connect to the internet</title>

  <p>When you set up a network connection, all other users on your computer
  will normally be able to use it. If the connection information is not shared,
  you should check the connection settings.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Network</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Network</gui> to open the panel.</p>
    </item>
    <item>
      <p>ડાબી બાજુ પર યાદીમાંથી <gui>Wi-Fi</gui> ને પસંદ કરો.</p>
    </item>
    <item>
      <p>Click the 
      <media its:translate="no" type="image" src="figures/emblem-system.png"><span its:translate="yes">settings</span></media> button to open the connection
      details.</p>
    </item>
    <item>
      <p>Select <gui>Identity</gui> from the pane on the left.</p>
    </item>
    <item>
      <p>At the bottom of the <gui>Identity</gui> panel, check the <gui>Make
      available to other users</gui> option to allow other users to use the
      network connection.</p>
    </item>
    <item>
      <p>ફેરફારોને સંગ્રહવા માટે <gui style="button">લાગુ કરો</gui> ને દબાવો.</p>
    </item>
  </steps>

  <p>કોઇપણ ભવિષ્યનાં વિગતોને દાખલ કર્યા વગર આને વાપરવા માટે કમ્પ્યૂટરનાં બીજા વપરાશકર્તાઓ હવે સક્ષમ હશે.</p>

  <note>
    <p>કોઇપણ વપરાશકર્તા આ સુયોજનને બદલી શકે છે.</p>
  </note>

</page>
