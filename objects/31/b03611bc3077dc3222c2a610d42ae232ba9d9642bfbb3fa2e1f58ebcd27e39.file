<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-admin-change" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="user-accounts#privileges"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Você pode permitir que usuários façam alterações no sistema, dando-lhes privilégios administrativos.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

  <title>Alterando quem tem privilégios administrativos</title>

  <p>Os privilégios administrativos são uma maneira de decidir quem pode fazer alterações em partes importantes do sistema. Você pode alterar quais usuários têm privilégios administrativos e quais não têm. Eles são uma boa forma de manter seu sistema seguro e evitar alterações não autorizadas potencialmente danosas.</p>

  <p>Você pode <link xref="user-admin-explain">privilégios administrativos</link> para alterar os tipos de conta.</p>

  <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Usuários</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Usuários</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Pressione <gui style="button">Desbloquear</gui> no canto superior direito e insira sua senha quando solicitada.</p>
    </item>
    <item>
      <p>Selecione o usuário cujos privilégios queira alterar.</p>
    </item>
    <item>
      <p>Clique no rótulo <gui>Padrão</gui> próximo ao <gui>Tipo de conta</gui> e selecione <gui>Administrador</gui>.</p>
    </item>
    <item>
      <p>Os privilégios serão alterados na próxima sessão deles.</p>
    </item>
  </steps>

  <note>
    <p>A primeira conta de usuário no sistema costuma ser a que tem privilégios administrativos. É aquela criada quando você instalou seu sistema pela primeira vez.</p>
    <p>Não é muito sábio ter muitos usuários com privilégios de <gui>Administrador</gui> em um sistema.</p>
  </note>

</page>
