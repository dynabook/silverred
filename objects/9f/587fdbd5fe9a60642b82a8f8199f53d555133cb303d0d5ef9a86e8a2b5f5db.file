<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="power-batterylife" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspend"/>
    <link type="seealso" xref="shell-exit#suspend"/>
    <link type="seealso" xref="shell-exit#shutdown"/>
    <link type="seealso" xref="display-brightness"/>
    <link type="seealso" xref="power-whydim"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Dicas para reduzir o consumo de energia do seus computador.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

  <title>Usando menos energia e melhorando duração da bateria</title>

  <p>Computadores podem usar muita energia. Ao usar algumas estratégias simples de economia de energia, você pode reduzir sua fatura de energia e ajudar o ambiente.</p>

<section id="general">
  <title>Dicas gerais</title>

<list>
  <item>
    <p><link xref="shell-exit#suspend">Suspenda seu computador</link> quando você não estiver usando-o. Isso reduz significativamente a quantidade de energia que ele usa e ele pode ser acordado rapidamente.</p>
  </item>
  <item>
    <p><link xref="shell-exit#shutdown">Desligue</link> o computador quando você não for usá-lo por períodos longos. Algumas pessoas acham que desligar um computador com certa regularidade pode resultar em desgastá-lo mais rápido, mas isso não ocorre de verdade.</p>
  </item>
  <item>
    <p>Use o painel <gui>Energia</gui> nas <app>Configurações</app> para alterar suas configurações de energia. Há um punhado de opções que vai ajudá-lo a economizar energia: você pode <link xref="display-blank">apagar a tela automaticamente</link> após determinado período de tempo, reduzir o <link xref="display-brightness">brilho da tela</link> e ter um computador <link xref="power-autosuspend">suspenso automaticamente</link> se você o tiver usado por um determinado período de tempo.</p>
  </item>
  <item>
    <p>Desligue quaisquer dispositivos externos (como impressora e scanners) quando você não estiver usando-os.</p>
  </item>
</list>

</section>

<section id="laptop">
  <title>Notebooks, netbooks e outros dispositivos com baterias</title>

 <list>
   <item>
     <p>Reduza o <link xref="display-brightness">brilho da tela</link>. A energização da tela é responsável por uma fração significativa do consumo da energia do notebook.</p>
     <p>A maioria dos notebooks possuem botões no teclado (ou um atalho de teclado) que você pode usar para reduzir o brilho.</p>
   </item>
   <item>
     <p>Se você não precisa de conexão Internet por um tempo, desligue as placas de rede sem fio ou Bluetooth. Esses dispositivos funcionam por ondas de rádio em broadcast, o que utiliza uma boa parcela da energia.</p>
     <p>Alguns computadores possuem um botão que você pode usar para desligar, da mesma forma que outros possuem um atalho de teclado. Você pode ligá-lo novamente quando precisar.</p>
   </item>
 </list>

</section>

<section id="advanced">
  <title>Dicas mais avançadas</title>

 <list>
   <item>
     <p>Reduza o número de tarefas que estão executando em plano de fundo. Computadores usam muita energia quando eles têm mais trabalho para fazer.</p>
     <p>A maioria dos aplicativos em execução fazem pouco quando você não está usando-os ativamente. Porém, aplicativos que com frequência obtêm dados da internet ou reproduzem música e filmes podem impactar no seu consumo de energia.</p>
   </item>
 </list>

</section>

</page>
