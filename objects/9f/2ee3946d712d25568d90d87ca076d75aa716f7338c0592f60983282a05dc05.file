<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="process-identify-hog" xml:lang="ru">
  <info>
    <revision version="0.2" pkgversion="3.11" date="2014-01-26" status="review"/>
    <link type="guide" xref="index#processes-tasks" group="processes-tasks"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author copyright">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
      <years>2011</years>
    </credit>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2011, 2014</years>
    </credit>

    <desc>Отсортируйте список процессов по столбцу <gui>% ЦП</gui>, чтобы увидеть, какое приложение потребляет много ресурсов компьютера.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  </info>

  <title>Какая программа тормозит работу компьютера?</title>

  <p>Программы, загружающие процессор больше других, могут замедлять работу всего компьютера. Чтобы узнать, какой процесс это делает:</p>

  <steps>	
    <item>
      <p>Зайдите на вкладку <gui>Процессы</gui>.</p>
    </item>
    <item>
      <p>Щёлкните на заголовке столбца <gui>% ЦП</gui>, чтобы отсортировать процессы по потреблению ресурсов процессора.</p>
      <note>
	<p>Стрелка в заголовке столбца указывает направление сортировки; щёлкните ещё раз, чтобы изменить его на обратное. Стрелка должна быть направлена вниз.</p>
      </note>
   </item>
  </steps>

  <p>Процессы вверху списка потребляют нагружают процессор больше всего. Определив, какой из них использует больше ресурсов, чем следует, вы можете решить, стоит ли закрыть саму эту программу или же лучше закрыть другие программы, чтобы снизить нагрузку на процессор.</p>

  <note style="tip">
    <p>Зависший или сбойный процесс может использовать 100% времени процессора. В таком случае может понадобиться <link xref="process-kill">убить</link> процесс.</p>
  </note>

</page>
