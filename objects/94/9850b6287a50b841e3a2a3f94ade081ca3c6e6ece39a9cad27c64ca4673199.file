<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="user-admin-problems" xml:lang="el">

  <info>
    <link type="guide" xref="user-accounts#privileges"/>
    <link type="seealso" xref="user-admin-explain"/>
    <link type="seealso" xref="user-admin-change"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-03" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <!-- TODO: review that this is actually correct -->
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="authohar">
      <name>Έργο Τεκμηρίωσης GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Μπορείτε να κάνετε μόνο κάποια πράγματα, όπως εγκατάσταση εφαρμογών, μόνο εάν έχετε δικαιώματα διαχειριστή.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Προβλήματα που προκαλούνται από διαχειριστικούς περιορισμούς</title>

  <p>Μπορεί να αντιμετωπίσετε κάποια προβλήματα αν δεν έχετε <link xref="user-admin-explain">δικαιώματα διαχειριστή</link>. Μερικές εργασίες απαιτούν δικαιώματα διαχειριστή για να δουλέψουν, όπως:</p>

  <list>
    <item>
      <p>σύνδεση με ασύρματα και ενσύρματα δίκτυα,</p>
    </item>
    <item>
      <p>προβολή περιεχομένου διαφορετικών κατατμήσεων του δίσκου (για παράδειγμα, μια κατάτμηση Windows), ή</p>
    </item>
    <item>
      <p>εγκατάσταση νέων εφαρμογών.</p>
    </item>
  </list>

  <p>Μπορείτε <link xref="user-admin-change">να αλλάξετε ποιος έχει δικαιώματα διαχειριστή</link>.</p>

</page>
