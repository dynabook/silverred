<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-suspend" xml:lang="ja">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspendfail"/>

    <desc>サスペンドするとコンピューターはスリープ状態となるため、消費電力が少なくなります。</desc>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>GNOME ドキュメンテーションプロジェクト</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>松澤 二郎</mal:name>
      <mal:email>jmatsuzawa@gnome.org</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>赤星 柔充</mal:name>
      <mal:email>yasumichi@vinelinux.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kentaro KAZUHAMA</mal:name>
      <mal:email>kazken3@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shushi Kurose</mal:name>
      <mal:email>md81bird@hitaki.net</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Noriko Mizumoto</mal:name>
      <mal:email>noriko@fedoraproject.org</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>坂本 貴史</mal:name>
      <mal:email>o-takashi@sakamocchi.jp</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>日本GNOMEユーザー会</mal:name>
      <mal:email>http://www.gnome.gr.jp/</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>コンピューターをサスペンドしたらどうなりますか?</title>

<p>コンピューターを<em>サスペンド</em>すると、スリープ状態に入ります。アプリケーションやドキュメントはすべて開いたままの状態ですが、コンピューターの画面および他の部分は電源オフになり節電できます。コンピューター自体の電源はオンのままのため、若干の電力は消費し続けています。何らかのキーを押すかマウスをクリックするとコンピューターをスリープ状態から復帰させることができます。キーボードやマウスを操作してもコンピューターが立ち上がらない場合は電源ボタンを押してみてください。</p>

<p>Some computers have problems with hardware support which mean that they
<link xref="power-suspendfail">may not be able to suspend properly</link>. It is
a good idea to test suspend on your computer to see if it does work before
relying on it.</p>

<note style="important">
  <title>サスペンドする前に必ず作業を保存してください</title>
  <p>コンピューターを再開したときに何らかのエラーが発生してアプリケーションやドキュメントの復元が不可能にならないよう、サスペンドする前にすべての作業を必ず保存してください。</p>
</note>

</page>
