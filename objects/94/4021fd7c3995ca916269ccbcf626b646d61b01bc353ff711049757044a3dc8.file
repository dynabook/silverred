<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="display-dual-monitors" xml:lang="gl">

  <info>
    <link type="guide" xref="prefs-display"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-09" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-23" status="review"/>
    <revision pkgversion="3.28" date="2018-07-28" status="review"/>
    <revision pkgversion="3.34" date="2019-11-12" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Configurar un monitor adicional.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2020</mal:years>
    </mal:credit>
  </info>

<title>Conectar outro monitor ao seu computador</title>

<!-- TODO: update video
<section id="video-demo">
  <title>Video Demo</title>
  <media its:translate="no" type="video" width="500" mime="video/webm" src="figures/display-dual-monitors.webm">
    <p its:translate="yes">Demo</p>
    <tt:tt its:translate="yes" xmlns:tt="http://www.w3.org/ns/ttml">
      <tt:body>
        <tt:div begin="1s" end="3s">
          <tt:p>Type <input>displays</input> in the <gui>Activities</gui>
          overview to open the <gui>Displays</gui> settings.</tt:p>
        </tt:div>
        <tt:div begin="3s" end="9s">
          <tt:p>Click on the image of the monitor you would like to activate or
          deactivate, then switch it <gui>ON/OFF</gui>.</tt:p>
        </tt:div>
        <tt:div begin="9s" end="16s">
          <tt:p>The monitor with the top bar is the main monitor. To change
          which monitor is “main”, click on the top bar and drag it over to
          the monitor you want to set as the “main” monitor.</tt:p>
        </tt:div>
        <tt:div begin="16s" end="25s">
          <tt:p>To change the “position” of a monitor, click on it and drag it
          to the desired position.</tt:p>
        </tt:div>
        <tt:div begin="25s" end="29s">
          <tt:p>If you would like both monitors to display the same content,
          check the <gui>Mirror displays</gui> box.</tt:p>
        </tt:div>
        <tt:div begin="29s" end="33s">
          <tt:p>When you are happy with your settings, click <gui>Apply</gui>
          and then click <gui>Keep This Configuration</gui>.</tt:p>
        </tt:div>
        <tt:div begin="33s" end="37s">
          <tt:p>To close the <gui>Displays Settings</gui> click on the
          <gui>×</gui> in the top corner.</tt:p>
        </tt:div>
      </tt:body>
    </tt:tt>
  </media>
</section>
-->

<section id="steps">
  <title>Configurar un monitor adicional</title>
  <p>To set up an additional monitor, connect the monitor to your computer. If
  your system does not recognize it immediately, or you would like to adjust
  the settings:</p>

  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Actividades</gui> e comece a escribir <gui>Pantallas</gui>.</p>
    </item>
    <item>
      <p>Prema en <gui>Pantallas</gui> para abrir o panel.</p>
    </item>
    <item>
      <p>No diagrama de xestión de pantalla, arrastre as súas pantallas ás posicións relativas que quere.</p>
      <note style="tip">
        <p>The numbers on the diagram are shown at the top-left of each
        display when the <gui>Displays</gui> panel is active.</p>
      </note>
    </item>
    <item>
      <p>Prema <gui>Pantalla principal</gui> para seleccionar a súa pantalla primaria.</p>

      <note>
        <p>Na parte superior esquerda da pantalla, prema o <link xref="shell-terminology">menú de <gui>Aplicacións</gui></link> e seleccione <gui>Vista de Actividades</gui>.</p>
      </note>
    </item>
    <item>
      <p>Seleccione a orientación, resolucón ou escala e seleccione a taxa de refresco.</p>
    </item>
    <item>
      <p>Prema <gui>Aplicar</gui>. A nova configuración aplicarase durante 20 segundos antes de reverterse. Desta forma, se non pode ver nada coa nova configuración a súa antiga configuración restaurarase automaticamente. Se está satisfeito coa nova configuración, prema <gui>Manter esta configuración</gui>.</p>
    </item>
  </steps>

</section>

<section id="modes">

  <title>Modos de pantalla</title>
    <p>Con dúas pantallas, estes son os modos dispoñíbeis:</p>
    <list>
      <item><p><gui>Unir pantallas:</gui> os bordos das pantallas únense polo que as cousas poden pasarse dunha pantalla a outra.</p></item>
      <item><p><gui>Espello:</gui> móstrase o mesmo contido en dúas pantallas, coa mesma resolución e orientación nas dúas.</p></item>
      <item><p><gui>Single Display:</gui> only one display is configured,
      effectively turning off the other one. For instance, an external monitor
      connected to a docked laptop with the lid closed would be the single
      configured display.</p></item>
    </list>

</section>

<section id="multiple">

  <title>Engadir máis dun monitor</title>
    <p>Con máis de dous pantallas só estará dispoñíbel o modo <gui>Pantallas unidas</gui>.</p>
    <list>
      <item>
        <p>Prema o botón para a pantalla que quere configurar.</p>
      </item>
      <item>
        <p>Arrastre as pantallas ás posicións relativas desexadas.</p>
      </item>
    </list>

</section>
</page>
