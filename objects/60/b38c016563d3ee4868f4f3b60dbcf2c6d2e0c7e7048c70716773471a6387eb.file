<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-maximize" xml:lang="pt">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>
    <link type="seealso" xref="shell-windows-tiled"/>

    <revision pkgversion="3.4.0" date="2012-03-14" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Duplo clique na barra de titulos da janela para a maximizar ou a restaurar.</desc>
  </info>

  <title>Maximizar e desmaximizar (restaurar) uma janela</title>

  <p>Pode maximizar uma janela para que use todo o espaço livre em seu escritório e desmaximizar uma janela para restaurar a seu tamanho normal. Também pode maximizar janelas verticalmente ao longo dos lados direito e esquerdo do ecrã, de tal forma que pode ver duas janelas ao mesmo tempo de facilmente. Para obter mais detalhes consulte a <link xref="shell-windows-tiled"/>.</p>

  <p>Para maximizar uma janela, arraste a barra de título até a parte superior do ecrã ou simplesmente carregue duas vezes sobre a barra de título. Para maximizar uma janela utilizando o teclado, mantenha premida a tecla <key xref="keyboard-key-super">Super</key> e carregue <key>↑</key> ou <keyseq><key>Alt</key><key>F10</key></keyseq>.</p>

  <p if:test="platform:gnome-classic">Também pode maximizar uma janela clicando o botão de maximizar na barra de titulo.</p>

  <p>Para restaurar uma janela a seu tamanho desmaximizado, arraste-a fora das bordas do ecrã. Se a janela está completamente maximizada, pode carregar duas vezes sobre a barra de título para restaurá-la. Também pode usar os mesmos atalhos de teclado que usou para maximizar a janela.</p>

  <note style="tip">
    <p>Mantenha premida a tecla <key>Súper</key> e arraste a janela a qualquer parte para movê-la.</p>
  </note>

</page>
