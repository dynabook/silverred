<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-where" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="backup-why"/>
    <title type="sort">c</title>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Conselhos sobre onde armazenar suas cópias de segurança e que tipo de dispositivo de armazenamento usar.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

<title>Onde armazenar as cópias de segurança</title>

  <p>Você deveria armazenar suas cópias de segurança em algum lugar à parte do seu computador — em um disco rígido externo, por exemplo. Dessa maneira, se seu computador quebrar, a cópia ainda estará intacta. Para máxima segurança, você não deveria guardá-la na mesma edificação que a do seu computador. Se houver incêndio ou furto, as cópias de segurança poderiam ser perdidas.</p>

  <p>É importante escolher uma <em>mídia de segurança</em> adequada também. Você precisa armazenar suas cópias em um dispositivo que tenha capacidade em disco suficiente para conter todos os arquivos de cópia de segurança.</p>

   <list style="compact">
    <title>Opções de armazenamento local e remoto</title>
    <item>
      <p>Pendrive USB (pequena capacidade)</p>
    </item>
    <item>
      <p>Disco rígido interno (grande capacidade)</p>
    </item>
    <item>
      <p>Disco rígido externo (normalmente de grande capacidade)</p>
    </item>
    <item>
      <p>Unidade conectada por rede (grande capacidade)</p>
    </item>
    <item>
      <p>Servidor de arquivos/cópias de segurança (grande capacidade)</p>
    </item>
    <item>
     <p>CDs e DVDs graváveis (pequena/média capacidade)</p>
    </item>
    <item>
     <p>Serviço de cópia de segurança na Internet (<link href="http://aws.amazon.com/s3/">Amazon S3</link>, por exemplo; a capacidade depende do preço)</p>
    </item>
   </list>

  <p>Algumas dessas opções têm capacidade suficiente para permitir uma cópia de segurança para todos os arquivos do seu sistema, também conhecido como uma <em>cópia de segurança completa do sistema</em>.</p>
</page>
