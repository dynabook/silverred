<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-status" xml:lang="id">

  <info>

    <link type="guide" xref="power" group="#first"/>
    <link type="seealso" xref="power-batterylife"/>

    <revision pkgversion="3.20" date="2016-06-15" status="candidate"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2016</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Display the status of the battery and connected devices.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

  <title>Periksa status baterai</title>

  <steps>

    <title>Display the status of the battery and connected devices</title>

    <item>
      <p>Buka ringkasan <gui xref="shell-introduction#activities">Aktivitas</gui> dan mulai mengetik <gui>Daya</gui>.</p>
    </item>
    <item>
      <p>Klik pada <gui>Daya</gui> untuk membuka panel. Status dari perangkat yang dikenal ditampilkan.</p>
    </item>

  </steps>

    <p>If an internal battery is detected, the <gui>Battery</gui> section
    displays the status of one or more laptop batteries. The indicator bar
    shows the percent charged, as well as time until fully charged if plugged
    in, and time remaining when running on battery power.</p>

    <p>The <gui>Devices</gui> section displays the status of connected
    devices.</p>

</page>
