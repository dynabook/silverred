<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-lost" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="author">
      <name>Gnomov projekat dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Šon Mek Kens</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Dejvid King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Pratite ova uputstva ako ne možete da pronađete datoteke koje ste napravili ili preuzeli.</desc>
  </info>

<title>Pronađite izgubljenu datoteku</title>

<p>Ako ste napravili ili preuzeli datoteku, ali sada ne možete da je pronađete, pratite ova uputstva.</p>

<list>
  <item><p>Ako se ne sećate gde ste sačuvali datoteku, ali barem imate neku ideju o tome kako ste je nazvali, možete <link xref="files-search">da je potražite prema nazivu</link>.</p></item>

  <item><p>Ako ste samo preuzeli datoteku, vaš veb preglednik je možda sam sačuvao u uobičajenoj fascikli. Proverite fascikle <file>radne površi</file> i <file>preuzimanja</file> u vašoj ličnoj fascikli.</p></item>

  <item><p>Može biti da ste slučajno obrisali datoteku. Kada obrišete datoteku, ona biva premeštena u smeće, gde ostaje sve dok ručno ne izbacite smeće. Pogledajte <link xref="files-recover"/> da saznate kako da povratite obrisanu datoteku.</p></item>

  <item><p>Može biti da ste preimenovali datoteku na način koji je čini skrivenom. Datoteke koje počinju <file>.</file> (tačkom) ili se završavaju <file>~</file> (tildom) su skrivene u upravniku datoteka. Kliknite na dugme na traci alata u <app>datotekama</app> i izaberite <gui>Prikaži skrivene datoteke</gui>. Pogledajte <link xref="files-hidden"/> da saznate više.</p></item>
</list>

</page>
