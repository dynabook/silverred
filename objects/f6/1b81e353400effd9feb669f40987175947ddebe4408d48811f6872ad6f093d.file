<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="net-wireless" group="first"/>
    <link type="guide" xref="hardware#problems" group="first"/>
    <link type="next" xref="net-wireless-troubleshooting-initial-check"/>

    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Contribuidores para o wiki de documentação do Ubuntu</name>
    </credit>
    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Identifique a corrija problemas com conexões de rede sem fio.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

  <title>Solução de problemas de rede sem fio</title>

  <p>Esse é um guia de diagnóstico para ajudá-lo a identificar e corrigir problemas de rede sem fio. Se você não consegue se conectar em uma rede sem fio por algum motivo, tente seguir essas instruções.</p>

  <p>Você vai continuar com as etapas abaixo para fazer com que seu computador se conecte à Internet:</p>

  <list style="numbered compact">
    <item>
      <p>Realizando uma verificação inicial</p>
    </item>
    <item>
      <p>Coletando informações sobre seu hardware</p>
    </item>
    <item>
      <p>Verificando seu hardware</p>
    </item>
    <item>
      <p>Tentando criar uma conexão com seu roteador de rede sem fio</p>
    </item>
    <item>
      <p>Realizando uma verificação de seu modem e roteador</p>
    </item>
  </list>

  <p>Para iniciar, clique no link <em>Próximo</em> no canto superior direito da página. Esse link, e outros como esse nas páginas a seguir, vão levá-lo pelas etapas no guia.</p>

  <note>
    <title>Usando a linha de comando</title>
    <p>Algumas instruções neste guia solicitam que você digite comandos na <em>linha de comando</em> (Terminal). Você localizar o aplicativo <app>Terminal</app> no panorama de <gui>Atividades</gui>.</p>
    <p>Se você não está familiarizado com o uso da linha de comando, não se preocupe — este guia vai auxiliá-lo em cada etapa. Tudo que você precisa se lembrar é que comandos diferenciam caracteres maiúsculo de minúsculo (de forma que você deve digitar <em>exatamente</em> como eles aparecem aqui), e pressionar <key>Enter</key> após digitar cada comando para executá-lo.</p>
  </note>

</page>
