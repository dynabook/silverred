<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-name-location" xml:lang="id">

  <info>
    <link type="guide" xref="printing#setup"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Change the name or location of a printer in the printer
    settings.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>
  
  <title>Change the name or location of a printer</title>

  <p>You can change the name or location of a printer in the printer
  settings.</p>

  <note>
    <p>You need <link xref="user-admin-explain">administrative privileges</link>
    on the system to change the name or location of a printer.</p>
  </note>

  <section id="printer-name-change">
    <title>Mengubah nama pencetak</title>

  <p>If you want to change the name of a printer, take the following steps:</p>

  <steps>
    <item>
      <p>Buka ringkasan <gui xref="shell-introduction#activities">Aktivitas</gui> dan mulai mengetik <gui>Pencetak</gui>.</p>
    </item>
    <item>
      <p>Klik <gui>Pencetak</gui> untuk membuka panel.</p>
    </item>
    <item>
      <p>Press <gui style="button">Unlock</gui> in the top right corner and
      type in your password when prompted.</p>
    </item>
    <item>
      <p>Click the name of your printer, and start typing a new name for
      the printer.</p>
    </item>
    <item>
      <p>Press <key>Enter</key> to save your changes.</p>
    </item>
  </steps>

  </section>

  <section id="printer-location-change">
    <title>Ubah lokasi pencetak</title>

  <p>To change the location of your printer:</p>

  <steps>
    <item>
      <p>Buka ringkasan <gui xref="shell-introduction#activities">Aktivitas</gui> dan mulai mengetik <gui>Pencetak</gui>.</p>
    </item>
    <item>
      <p>Klik <gui>Pencetak</gui> untuk membuka panel.</p>
    </item>
    <item>
      <p>Press <gui style="button">Unlock</gui> in the top right corner and
      type in your password when prompted.</p>
    </item>
    <item>
      <p>Click the location, and start editing the location.</p>
    </item>
    <item>
      <p>Press <key>Enter</key> to save the changes.</p>
    </item>
  </steps>

  </section>

</page>
