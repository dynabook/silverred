<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-launch-applications" xml:lang="it">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>

    <link type="guide" xref="getting-started" group="videos"/>
    <title role="trail" type="link">Lanciare applicazioni</title>
    <link type="seealso" xref="shell-apps-open"/>
    <title role="seealso" type="link">Una guida su come lanciare applicazioni</title>
    <link type="next" xref="gs-switch-tasks"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Milo Casagrande</mal:name>
      <mal:email>milo@milo.name</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

  <title>Lanciare applicazioni</title>

  <ui:overlay width="812" height="452">
   <media type="video" its:translate="no" src="figures/gnome-launching-applications.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-launching-apps.svg"/>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>Lanciare applicazioni</tt:p>
         </tt:div>
         <tt:div begin="5s" end="7.5s">
           <tt:p>Spostare il puntatore del mouse nell'angolo <gui>Attività</gui>, in alto a sinistra dello schermo.</tt:p>
           </tt:div>
         <tt:div begin="7.5s" end="9.5s">
           <tt:p>Fare clic sull'icona <gui>Mostra applicazioni</gui>.</tt:p>
         </tt:div>
         <tt:div begin="9.5s" end="11s">
           <tt:p>Fare clic sull'applicazione che si vuole eseguire, per esempio Aiuto.</tt:p>
         </tt:div>
         <tt:div begin="12s" end="21s">
           <tt:p>In alternativa, usare la tastiera per aprire la <gui>Panoramica attività</gui> premendo il tasto <key href="help:gnome-help/keyboard-key-super">Super</key>.</tt:p>
         </tt:div>
         <tt:div begin="22s" end="29s">
           <tt:p>Iniziare a digitare il nome dell'applicazione che si vuole lanciare.</tt:p>
         </tt:div>
         <tt:div begin="30s" end="33s">
           <tt:p>Premere <key>Invio</key> per lanciare l'applicazione.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
   </media>
  </ui:overlay>

  <section id="launch-apps-mouse">
    <title>Lanciare applicazioni col mouse</title>
 
  <steps>
    <item><p>Spostare il puntatore del mouse sull'angolo <gui>Attività</gui> in alto a sinistra sullo schermo per mostrare la <gui>Panoramica attività</gui>.</p></item>
    <item><p>Fare clic sull'icona <gui>Mostra applicazioni</gui> collocata in fondo alla barra sul lato sinistro dello schermo.</p></item>
    <item><p>Viene mostrato un elenco di applicazioni. Fare clic sull'applicazione che si vuole avviare, per esempio Aiuto.</p></item>
  </steps>

  </section>

  <section id="launch-app-keyboard">
    <title>Lanciare applicazioni con la tastiera</title>

  <steps>
    <item><p>Aprire la <gui>Panoramica attività</gui> premendo il tasto <key href="help:gnome-help/keyboard-key-super">Super</key>.</p></item>
    <item><p>Iniziare a digitare il nome dell'applicazione che si vuole lanciare. La ricerca viene eseguita istantaneamente.</p></item>
    <item><p>Una volta che l'icona dell'applicazione è mostrata e selezionata, premere <key>Invio</key> per lanciare l'applicazione.</p></item>
  </steps>

  </section>

</page>
