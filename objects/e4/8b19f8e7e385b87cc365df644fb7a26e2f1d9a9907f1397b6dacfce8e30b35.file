<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="shell-lockscreen" xml:lang="fi">

  <info>
    <link type="guide" xref="shell-overview#apps"/>
    <link type="guide" xref="shell-notifications#lock-screen-notifications"/>

    <revision pkgversion="3.6.1" date="2012-11-11" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Koristeellinen ja toiminnollinen lukitusnäyttö sisältää hyödyllistä tietoa.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

  <title>Lukitusnäyttö</title>

  <p>Lukitusnäyttö näyttää mitä koneella tapahtuu sen ollessa lukittu, ja näyttää yhteenvedon mitä on tapahtunut ollessasi poissa. Lukitusnäyttö näyttää valitsemasi lukitusnäytön taustakuvan ja hyödyllisiä tietoja:</p>

  <list>
    <item><p>sisäänkirjautuneen käyttäjän nimen</p></item>
    <item><p>päivän ja ajan sekä tiettyjä ilmoituksia</p></item>
    <item><p>akun ja verkon tilan</p></item>
<!-- No media control anymore on lock screen, see BZ #747787: 
    <item><p>the ability to control media playback — change the volume, skip a
    track or pause your music without having to enter a password</p></item> -->
  </list>

  <p>Avaa tietokoneen lukitus nostamalla lukitusnäytön "verho" edestä hiirellä, tai painamalla <key>Esc</key> tai <key>Enter</key>. Eteesi ilmestyy sisäänkirjautumisnäkymä, johon voit kirjoittaa salasanan avataksesi tietokoneen lukituksen. Voit myös aloittaa suoraan lukitusnäytössä salasanasi kirjoittamisen, jolloin lukitusnäyttö poistuu näkyvistä automaattisesti. Voit myös vaihtaa käyttäjää sisäänkirjautumisnäkymässä, jos koneella on useampi kuin yksi käyttäjätili.</p>

  <p>To hide notifications from the lock screen, see
  <link xref="shell-notifications#lock-screen-notifications"/>.</p>

</page>
