<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="desktop-background" xml:lang="sv">

  <info>
    <link type="guide" xref="appearance"/>
    <link type="seealso" xref="backgrounds-extra"/>
    <revision pkgversion="3.11" date="2014-01-29" status="draft"/>
    <revision pkgversion="3.14" date="2014-06-17" status="incomplete">
      <desc>All text och instruktioner håller acceptabel nivå. Extra info krävs för bildalternativ, i enlighet med kommentar nedan. --shaunm</desc>
    </revision>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>davidk@gnome.org</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>

    <desc>Ändra standardbakgrund för skrivbordet för alla användare.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Ställa in en anpassad bakgrund</title>

  <p>Du kan ändra standardbakgrunden för skrivbordet till en egen som du vill använda. Du kan exempelvis vilja använda en bakgrund med ditt företags eller universitets logo istället för standardbakgrunden för GNOME.</p>

  <steps>
    <title>Ställ in standardbakgrunden</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
    <item>
      <p>Skapa nyckelfilen <file>/etc/dconf/db/local.d/00-background</file> för att tillhandahålla information åt databasen <sys>local</sys>.</p>
      <listing>
        <title><file>/etc/dconf/db/local.d/00-background</file></title>
<code>
# Ange dconf-sökvägen
[org/gnome/desktop/background]

# Ange sökvägen till bildfilen för skrivbordsbakgrunden
picture-uri='file:///usr/local/share/backgrounds/wallpaper.jpg'

# Ange en av renderingsalternativen för bakgrundsbilden:
picture-options='scaled'

# Ange den vänstra eller övre färgen då toningar ritas, eller den enda färgen
primary-color='000000'

# Ange den högra eller nedre färgen då toningar ritas
secondary-color='FFFFFF'
</code>
      </listing>
    </item>
    <item>
      <p>För att förhindra användaren från att åsidosätta dessa inställningar, skapa filen <file>/etc/dconf/db/local.d/locks/background</file> med följande innehåll:</p>
      <listing>
        <title><file>/etc/dconf/db/local.d/locks/background</file></title>
<code>
# Lås skrivbordets bakgrundsinställningar
/org/gnome/desktop/background/picture-uri
/org/gnome/desktop/background/picture-options
/org/gnome/desktop/background/primary-color
/org/gnome/desktop/background/secondary-color
</code>
      </listing>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
  </steps>

</page>
