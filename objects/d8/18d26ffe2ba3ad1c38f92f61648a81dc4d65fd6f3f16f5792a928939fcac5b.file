<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-changepicture" xml:lang="el">

  <info>
    <link type="guide" xref="user-accounts#manage"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.14.0" date="2014-10-08" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.32.0" date="2019-03-16" status="final"/>

    <credit type="author">
      <name>Έργο Τεκμηρίωσης GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Προσθέστε την φωτογραφία σας στη σύνδεση και τις οθόνες των χρηστών.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Αλλαγή της φωτογραφίας οθόνης εισόδου</title>

  <p>Όταν συνδέεστε ή αλλάζετε χρήστες, θα δείτε μια λίστα χρηστών με τις φωτογραφίες σύνδεσης τους. Μπορείτε να αλλάξετε την φωτογραφία σας με μια εικόνα παρακαταθήκης ή μια δικιά σας εικόνα. Μπορείτε ακόμα να πάρετε μια νέα φωτογραφία με την ιστοκάμερά σας.</p>

  <p>Χρειάζεστε <link xref="user-admin-explain">δικαιώματα διαχειριστή</link> για να επεξεργαστείτε λογαριασμούς χρήστη άλλους εκτός από τον δικό σας.</p>

  <steps>
    <item>
      <p>Ανοίξτε την επισκόπηση <gui xref="shell-introduction#activities">Δραστηριότητες</gui> και αρχίστε να πληκτρολογείτε <gui>Χρήστες</gui>.</p>
    </item>
    <item>
      <p>Κάντε κλικ στο <gui>Χρήστες</gui> για να ανοίξετε τον πίνακα.</p>
    </item>
    <item>
      <p>Αν θέλετε να επεξεργαστείτε έναν χρήστη εκτός από τον εαυτόν σας, πατήστε <gui style="button">Ξεκλείδωμα</gui> στην πάνω δεξιά γωνία και πληκτρολογήστε τον κωδικό πρόσβασής σας όταν σας ζητηθεί.</p>
    </item>
    <item>
      <p>Κάντε κλικ στην εικόνα δίπλα στο όνομά σας. Μια αναπτυσσόμενη συλλογή θα εμφανιστεί με μερικές φωτογραφίες σύνδεσης παρακαταθήκης. Εάν θέλετε μία από αυτές, κάντε κλικ για να την χρησιμοποιήσετε για τον εαυτόν σας.</p>
      <list>
        <item>
          <p>If you would rather use a picture you already have on your
          computer, click <gui>Select a file…</gui>.</p>
        </item>
        <item>
          <p>If you have a webcam, you can take a new login photo right now by
          clicking <gui>Take a picture…</gui>. Take your
          picture, then move and resize the square outline to crop out the
          parts you do not want. If you do not like the picture you took, click
          <gui style="button">Take Another Picture</gui> to try again, or
          <gui>Cancel</gui> to give up.</p>
        </item>
      </list>
    </item>
  </steps>

</page>
