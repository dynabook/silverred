<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-touchpad-click" xml:lang="de">

  <info>
    <link type="guide" xref="mouse"/>

    <revision pkgversion="3.7" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-29" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013, 2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Klicken, Ziehen oder Bildlauf mit dem Tastfeld.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2019.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2019.</mal:years>
    </mal:credit>
  </info>

  <title>Klicken, Ziehen oder Rollen mit dem Tastfeld</title>

  <p>Sie können klicken, doppelklicken, ziehen und rollen, indem Sie allein Ihr Tastfeld verwenden, ohne zusätzliche Hardwareknöpfe.</p>

  <note>
    <p><link xref="touchscreen-gestures">Gesten für Tastbildschirme</link> werden separat behandelt.</p>
  </note>

<section id="tap">
  <title>Tippen Sie, um zu klicken</title>

  <p>Sie können Ihr Tastfeld antippen, anstelle einen Knopf zu drücken.</p>

  <steps>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Maus und Tastfeld</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Maus und Tastfeld</gui>, um das Panel zu öffnen.</p>
    </item>
    <item>
      <p>Stellen Sie sicher, dass im Abschnitt <gui>Tastfeld</gui> der Schalter <gui>Tastfeld</gui> eingestellt ist.</p>
      <note>
        <p>Der Abschnitt <gui>Tastfeld</gui> ist nur dann vorhanden, wenn Ihr System tatsächlich über ein Tastfeld verfügt.</p>
      </note>
    </item>
   <item>
      <p>Schalten Sie <gui>Antippen zum Klicken</gui> ein.</p>
    </item>
  </steps>

  <list>
    <item>
      <p>Zum Klicken klopfen Sie auf das Tastfeld.</p>
    </item>
    <item>
      <p>Für einen Doppelklick klopfen Sie zweimal.</p>
    </item>
    <item>
      <p>Um ein Objekt zu ziehen, klopfen Sie doppelt, aber heben Sie Ihren Finger danach nicht an. Ziehen Sie dann das Objekt an den gewünschten Platz und heben Sie Ihren Finger an, um es abzulegen.</p>
    </item>
    <item>
      <p>Wenn Ihr Tastfeld das Klopfen mit mehreren Fingern unterstützt, klopfen Sie für einen Rechtsklick mit zwei Fingern gleichzeitig. Andernfalls müssen Sie für einen Rechtsklick die Tasten am Rechner benutzen. Siehe <link xref="a11y-right-click"/> für eine Methode, um einen Rechtsklick ohne zweite Maustaste auszuführen.</p>
    </item>
    <item>
      <p>Wenn Ihr Tastfeld das Klopfen mit mehreren Fingern unterstützt, führen Sie einen <link xref="mouse-middleclick">Mittelklick</link> aus, indem Sie mit drei Fingern gleichzeitig klopfen.</p>
    </item>
  </list>

  <note>
    <p>Wenn Sie mit mehreren Fingern klopfen oder ziehen, achten Sie darauf, dass Ihre Finger weit genug auseinander liegen. Wenn Ihre Finger zu nah beieinander sind, könnte der Rechner glauben, dass es sich um einen einzelnen Finger handelt.</p>
  </note>

</section>

<section id="twofingerscroll">
  <title>Bildlauf mit zwei Fingern</title>

  <p>Sie können mit Ihrem Tastfeld einen Bildlauf auslösen, indem Sie zwei Finger verwenden.</p>

  <steps>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Maus und Tastfeld</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Maus und Tastfeld</gui>, um das Panel zu öffnen.</p>
    </item>
    <item>
      <p>Stellen Sie sicher, dass im Abschnitt <gui>Tastfeld</gui> der Schalter <gui>Tastfeld</gui> eingestellt ist.</p>
    </item>
    <item>
      <p>Schalten Sie <gui>Bildlauf mit zwei Fingern</gui> ein.</p>
    </item>
  </steps>

  <p>Wenn diese Einstellung gewählt ist, funktioniert das Klopfen und Ziehen mit einem Finger wie üblich, aber wenn Sie zwei Finger an einer beliebigen Stelle des Tastfelds ziehen, wird ein Bildlauf ausgeführt. Bewegen Sie die Finger von oben nach unten und umgekehrt auf dem Tastfeld für Bildlauf nach unten und nach oben. Bewegen Sie Ihre Finger nach links oder rechts, um einen vertikalen Bildlauf auszuführen. Achten Sie darauf, die Finger ein kleines Stück zu spreizen. Wenn Ihre Finger zu nahe beieinander sind, erkennt das Tastfeld nur einen großen Finger.</p>

  <note>
    <p>Das Rollen mit zwei Fingern funktioniert unter Umständen nicht auf allen Tastfeldern.</p>
  </note>

</section>

<section id="contentsticks">
  <title>Natürlicher Bildlauf</title>

  <p>Sie können Inhalte ziehen, wie wenn Sie auf dem Tastfeld über ein echtes Stück Papier gleiten würden.</p>

  <steps>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Maus und Tastfeld</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Maus und Tastfeld</gui>, um das Panel zu öffnen.</p>
    </item>
    <item>
      <p>Stellen Sie sicher, dass im Abschnitt <gui>Tastfeld</gui> der Schalter <gui>Tastfeld</gui> eingestellt ist.</p>
    </item>
    <item>
      <p>Schalten Sie <gui>Natürlicher Bildlauf</gui> ein.</p>
    </item>
  </steps>

  <note>
    <p>Dieses Funktionsmerkmal ist auch als <em>Umgekehrter Bildlauf</em> bekannt.</p>
  </note>

</section>

</page>
