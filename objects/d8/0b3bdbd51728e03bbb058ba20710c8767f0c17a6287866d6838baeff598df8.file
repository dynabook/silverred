<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-disc-write" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>使用 CD/DVD 创建器把文件刻录到空白 CD 或 DVD 光盘上。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>把文件写入 CD 或 DVD</title>

  <p>通过使用 <gui>CD/DVD 创建器</gui> 可以将文件刻入空白磁盘，方便文件传输到其他计算机或进行<link xref="backup-why">备份</link>。要将文件写入 CD 或 DVD：</p>

  <steps>
    <item>
      <p>在您的 CD/DVD 刻录机中放一张空白光盘。</p></item>
    <item>
      <p>在屏幕下方弹出的<gui>空白 CD/DVD-R 光盘</gui>提醒框中，选择<gui>打开 CD/DVD 创建器</gui>，将会显示 <gui>CD/DVD 创建器</gui>窗口。</p>
      <p>(您也可以在文件管理器侧边栏的<gui>设备</gui>下面，点击<gui>空白 CD/DVD-R 盘片</gui>。)</p>
    </item>
    <item>
      <p>在<gui>盘片名</gui>文本框中，输入光盘的名称。</p>
    </item>
    <item>
      <p>将所需文件拖入或复制到该窗口中。</p>
    </item>
    <item>
      <p>单击<gui>写入到光盘</gui>。</p>
    </item>
    <item>
      <p>在<gui>请选择要写入的光盘</gui>对话框中，选择空白盘片。</p>
      <p>(您还可以选择<gui>映像文件</gui>，这将会把文件写入一个<em>映像文件</em>，并保存到您的计算机中。您可以稍后再刻录这个映像文件。)</p>
    </item>
    <item>
      <p>点击<gui>属性</gui>可以调整刻录速度、临时文件的位置等其他选项，默认选项应该就很好了。</p>
    </item>
    <item>
      <p>点<gui>刻录</gui>开始刻录光盘。</p>
      <p>如果选择<gui>刻录多份</gui>，您会提示放入其他光盘。</p>
    </item>
    <item>
      <p>当刻录完成后，光盘会自动弹出，选择<gui>制作其他副本</gui>或者<gui>关闭</gui>退出。</p>
    </item>
  </steps>

<section id="problem">
  <title>If the disc wasn’t burned properly</title>

  <p>Sometimes the computer doesn’t record the data correctly, and you won’t be
  able to see the files you put onto the disc when you insert it into a
  computer.</p>

  <p>In this case, try burning the disc again but use a lower burning speed,
  for example, 12x rather than 48x. Burning at slower speeds is more reliable.
  You can choose the speed by clicking the <gui>Properties</gui> button in the
  <gui>CD/DVD Creator</gui> window.</p>

</section>

</page>
