<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-gettingprofiles" xml:lang="vi">

  <info>
    <link type="guide" xref="color#profiles"/>
    <link type="seealso" xref="color-why-calibrate"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <link type="seealso" xref="color-missingvcgt"/>
    <desc>Color profiles are provided by vendors and can be generated yourself.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nguyễn Thái Ngọc Duy</mal:name>
      <mal:email>pclouds@gmail.com</mal:email>
      <mal:years>2011-2012.</mal:years>
    </mal:credit>
  </info>

  <title>Lấy hồ sơ màu ở đâu?</title>

  <p>
    The best way to get profiles is to generate them yourself, although
    this does require some initial outlay.
  </p>
  <p>
    Many manufacturers do try to provide color profiles for devices,
    although sometimes they are wrapped up in <em>driver bundles</em>
    which you may need to download, extract and then search for the
    color profiles.
  </p>
  <p>
    Some manufacturers do not provide accurate profiles for the hardware
    and the profiles are best avoided.
    A good clue is to download the profile, and if the creation date is
    more than a year before the date you bought the device then it’s
    likely dummy data generated that is useless.
  </p>

  <p>
    See <link xref="color-why-calibrate"/> for information on why vendor-supplied
    profiles are often worse than useless.
  </p>

</page>
