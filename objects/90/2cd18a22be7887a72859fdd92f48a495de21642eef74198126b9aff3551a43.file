<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-hotcomputer" xml:lang="fi">

  <info>
    <link type="guide" xref="power#problems"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Tietokoneet lämpeävät yleensä käytön aikana, mutta jos tietokone ylikuumenee, se voi olla vaarallista.</desc>

    <credit type="author">
      <name>Gnomen dokumentointiprojekti</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

<title>Tietokoneeni muuttuu hyvin lämpimäksi</title>

<p>Most computers get warm after a while, and some can get quite hot. This is
normal: it is simply part of the way that the computer cools itself. However,
if your computer gets very warm it could be a sign that it is overheating,
which can potentially cause damage.</p>

<p>Most laptops get reasonably warm once you have been using them for a while.
It is generally nothing to worry about — computers produce a lot of heat and
laptops are very compact, so they need to remove their heat rapidly and their
outer casing warms up as a result. Some laptops do get too hot, however, and
may be uncomfortable to use. This is normally the result of a poorly-designed
cooling system. You can sometimes get additional cooling accessories which fit
to the bottom of the laptop and provide more efficient cooling.</p>

<p>If you have a desktop computer which feels hot to the touch, it may have
insufficient cooling. If this concerns you, you can buy extra cooling fans or
check that the cooling fans and vents are free from dust and other blockages.
You might want to consider putting the computer in a better-ventilated area too
— if kept in confined spaces (for example, in a cupboard), the cooling system in the
computer may not be able to remove heat and circulate cool air fast enough.</p>

<p>Jotkut ihmiset ovat huolissaan kuumina käyvien kannettavien tietokoneiden riskeistä. On ehdotettu, että pitkäaikainen kuuman kannettavan sylissä pitäminen aiheuttaa miehillä ongelmia hedelmällisyydelle. Pahimmillaan sylissä pidetyt kannettavat tietokoneet ovat aiheuttaneet palovammoja. Jos olet huolissasi edellä mainituista asioista, keskustele lääketieteeseen perehtyneen asiantuntijan kanssa. Suositeltavampi vaihtoehto on kuitenkin olla pitämättä kuumana käyvää kannettavaa tietokonetta sylissä.</p>

<p>Useimmat nykyaikaiset tietokoneet sammuttavat itsensä, jos ne kuumenevat liikaa. Näin pyritään estämään kuumuudesta johtuvien vaurioiden syntyminen. Jos tietokoneesi sammuu jatkuvasti itsestään, se voi johtua ylikuumentumisesta. Mikäli tietokoneesi ylikuumenee, toimita tietokone huoltoon tai yritä korjata ongelma lisäämällä tietokoneeseen tuulettimia.</p>

</page>
