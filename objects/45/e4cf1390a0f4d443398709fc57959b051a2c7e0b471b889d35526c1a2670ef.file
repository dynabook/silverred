<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-admin-change" xml:lang="as">

  <info>
    <link type="guide" xref="user-accounts#privileges"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>GNOME তথ্যচিত্ৰ প্ৰকল্প</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>একাটেৰিনা গেৰাচিমভা</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>You can allow users to make changes to the system by giving them
    administrative privileges.</desc>
  </info>

  <title>প্ৰশাসনীয় সুবিধা থকাক পৰিবৰ্তন কৰক</title>

  <p>Administrative privileges are a way of deciding who can make changes to
  important parts of the system. You can change which users have administrative
  privileges and which ones do not. They are a good way of keeping your system
  secure and preventing potentially damaging unauthorized changes.</p>

  <p>You need <link xref="user-admin-explain">administrator privileges</link>
  to change account types.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Users</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Users</gui> to open the panel.</p>
    </item>
    <item>
      <p>Press <gui style="button">Unlock</gui> in the top right corner and
      type in your password when prompted.</p>
    </item>
    <item>
      <p>আপুনি সুবিধা পৰিবৰ্তন কৰিব খোজা ব্যৱহাৰকাৰীক বাছক।</p>
    </item>
    <item>
      <p>Click the label <gui>Standard</gui> next to <gui>Account Type</gui>
      and select <gui>Administrator</gui>.</p>
    </item>
    <item>
      <p>The user’s privileges will be changed when they next log in.</p>
    </item>
  </steps>

  <note>
    <p>The first user account on the system is usually the one that has
    administrator privileges. This is the user account that was created when
    you first installed the system.</p>
    <p>এটা চিস্টেমত <gui>প্ৰশাসক</gui> সুবিধা থকা অত্যাধিক ব্যৱহাৰকাৰী থকাটো সঠিক নহয়।</p>
  </note>

</page>
