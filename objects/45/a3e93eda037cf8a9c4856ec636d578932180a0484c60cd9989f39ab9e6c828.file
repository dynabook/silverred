<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="lockdown-logout" xml:lang="sv">

  <info>
    <link type="guide" xref="user-settings#lockdown"/>
    <link type="seealso" xref="dconf-lockdown"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2015</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Förhindra användaren från att logga ut och från att växla användare.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Inaktivera användarutloggning och användarväxling</title>

  <p>Att förhindra användaren från att logga ut är användbart för vissa sorter av GNOME-installationer (obemannade kiosker, öppna internetåtkomstterminaler och så vidare).</p>

  <note style="important">
  <p>Användare kan gå runt låsningen av utloggning genom att växla till en annan användare. Detta är orsaken till varför det rekommenderas att också inaktivera <em>användarväxling</em> när systemet konfigureras.</p>
  </note>

  <steps>
  <title>Inaktivera användarutloggning och användarväxling</title>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
  <item>
  <p>Skapa nyckelfilen <file>/etc/dconf/db/local.d/00-logout</file> för att tillhandahålla information åt databasen <sys>local</sys>:</p>
<screen>
[org/gnome/desktop/lockdown]
# Förhindra användaren från att logga ut
disable-log-out=true

# Förhindra användaren från att växla användare
disable-user-switching=true
</screen>
  </item>
  <item>
  <p>Åsidosätt användarens inställning och förhindra användaren från att ändra den i <file>/etc/dconf/db/local.d/locks/lockdown</file>:</p>
<screen>
# Lås användarutloggning
/org/gnome/desktop/lockdown/disable-log-out

# Lås växling av användare
/org/gnome/desktop/lockdown/disable-user-switching
</screen>
  </item>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  <item>
  <p>Starta om systemet för att de systemomfattande inställningarna ska börja gälla.</p>
  </item>
  </steps>

</page>
