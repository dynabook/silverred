<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-right-click" xml:lang="de">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="clicking"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-21" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Die linke Maustaste für einen Rechtsklick gedrückt halten.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2019.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2019.</mal:years>
    </mal:credit>
  </info>

  <title>Einen Klick der rechten Maustaste simulieren</title>

  <p>Sie können einstellen, dass Sie, statt die rechte Maustaste zu klicken, einfach die linke Maustaste für eine Weile gedrückt halten, um dasselbe zu erreichen. Dies ist nützlich, wenn es Ihnen schwer fällt, die Finger einer Hand gezielt zu bewegen, oder wenn Ihre Maus nur über eine einzige Taste verfügt.</p>

  <steps>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Barrierefreiheit</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Barrierefreiheit</gui>, um das Panel zu öffnen.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Klickassistent</gui> im Abschnitt <gui>Zeigen und Klicken</gui>.</p>
    </item>
    <item>
      <p>Schalten Sie im Fenster <gui>Klickassistent</gui> den Schalter <gui>Simulierter Kontextklick</gui> ein.</p>
    </item>
  </steps>

  <p>Sie können ändern, wie lange Sie die linke Maustaste gedrückt halten müssen, damit ein Rechtsklick registriert wird. Ändern Sie dazu die <gui>Akzeptanzverzögerung</gui>.</p>

  <p>Um einen Rechtsklick mit einem simulierten Kontextklick auszuführen, halten Sie die linke Maustaste an der Stelle am Bildschirm gedrückt, wo Sie sonst mit der rechten Maustaste klicken würden, und lassen Sie sie dann los. Der Zeiger wird mit einer anderen Farbe gefüllt, so lange Sie die linke Maustaste gedrückt halten. Sobald der Zeiger vollständig diese Farbe angenommen hat, lassen Sie die Maustaste los, um den Rechtsklick auszulösen.</p>

  <p>Einige spezielle Zeiger, wie jene zur Größenänderung, ändern die Farbe nicht. Sie können dennoch den simulierten Kontextklick verwenden, auch wenn Sie keine optische Rückmeldung erhalten.</p>

  <p>Falls Sie die <link xref="mouse-mousekeys">Tastaturmaus</link> verwenden, können Sie einen Rechtsklick durch gedrückt halten der Taste <key>5</key> auf Ihrer Tastatur erreichen.</p>

  <note>
    <p>In der <gui>Aktivitäten</gui>-Übersicht können Sie die Maustaste immer lang gedrückt halten, um einen Rechtsklick auszulösen, selbst wenn diese Funktion ausgeschaltet ist. Die Taste lange gedrückt zu halten funktioniert in der Übersicht etwas anders: Sie müssen die Taste für einen Rechtsklick nicht loslassen.</p>
  </note>

</page>
