<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="lockdown-file-saving" xml:lang="de">

  <info>
    <link type="guide" xref="user-settings#lockdown"/>
    <link type="seealso" xref="dconf-lockdown"/>
    <revision pkgversion="3.11" date="2014-10-14" status="candidate"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
        
    <desc>Den Benutzer daran hindern, Dateien auf der Festplatte zu speichern.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2017, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>
  
  <title>Speichern von Dateien unterbinden</title>

  <p>Sie können die Anzeige der Dialoge <gui>Speichern</gui> und <gui>Speichern unter</gui> verhindern. Das kann sinnvoll sein, wenn Sie einem Benutzer vorübergehenden Zugriff geben und Sie nicht wollen, dass dieser Dateien auf dem Rechner speichert.</p>

  <note style="warning">
    <p>Dies funktioniert nur in Anwendungen, die dieses Funktionsmerkmal unterstützen! Nicht alle GNOME- und Drittanbieter-Anwendungen können damit umgehen. Die Änderungen werden unwirksam sein, wenn Anwendungen dieses Funktionsmerkmal nicht unterstützen.</p>
  </note>

  <steps>
    <title>Speichern von Dateien unterbinden</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
    <item>
      <p>Erstellen Sie die Schlüsseldatei <file>/etc/dconf/db/local.d/00-filesaving</file>, um Informationen für die <sys>local</sys>-Datenbank bereitzustellen.</p>
      <listing>
        <title><file>/etc/dconf/db/local.d/00-filesaving</file></title>
<code>
# Den dconf-Pfad festlegen
[org/gnome/desktop/lockdown]

# Benutzer am Speichern von Daten auf der Platte hindern
disable-save-to-disk=true
</code>
     </listing>
    </item>
    <item>
      <p>Um den Benutzer daran zu hindern, diese Einstellungen zu überschreiben, erstellen Sie die Datei <file>/etc/dconf/db/local.d/locks/filesaving</file> mit dem folgenden Inhalt:</p>
      <listing>
        <title><file>/etc/dconf/db/local.db/locks/filesaving</file></title>
<code>
# Einstellungen zur Dateispeicherung sperren
/org/gnome/desktop/lockdown/disable-save-to-disk
</code>
      </listing>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
  </steps>

</page>
