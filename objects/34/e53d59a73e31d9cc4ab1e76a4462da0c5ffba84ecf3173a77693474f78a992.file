<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-purge" xml:lang="ta">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.10" date="2013-09-29" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <credit type="author">
      <name>ஜிம் காம்ப்பெல்</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>மைக்கேல் ஹில்</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>ஷான் மெக்கேன்ஸ்</name>
      <email>mdhillca@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>உங்கள் குப்பைத் தொட்டியும் தற்காலிகக் கோப்புகளும் உங்கள் கணினியில் இருந்து எப்படி அழிக்கப்படுகின்றன என அமைத்தல்.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>குப்பைத் தொட்டி &amp; தற்காலிகக் கோப்புகளை அழி</title>

  <p>உங்கள் குப்பைத் தொட்டியை அழித்தால், உங்கள் கணினியிலிருந்து தேவையற்ற கோப்புகள் அகற்றப்படும், வன் வட்டில் அதிக காலி இடம் உருவாகும். நீங்கள் கைமுறையாக உங்கள் குப்பைத் தொட்டி மற்றும் உங்கள் தற்காலிகக் கோப்புகளை அழிக்கலாம், ஆனால் இதை தானாக கணினியே செய்யும் படியும் அமைக்க முடியும்.</p>

  <p>Temporary files are files created automatically by applications in the
  background. They can increase performance by providing a copy of data that
  was downloaded or computed.</p>

  <steps>
    <title>Automatically empty your trash and clear temporary files</title>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Privacy</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Privacy</gui> to open the panel.</p>
    </item>
    <item>
      <p><gui>குப்பைத் தொட்டி &amp; தற்காலிகக் கோப்புகளை அழி</gui> ஐ தேர்ந்தெடுக்கவும்.</p>
    </item>
    <item>
      <p>Switch one or both of the <gui>Automatically empty Trash</gui> or
      <gui>Automatically purge Temporary Files</gui> switches to on.</p>
    </item>
    <item>
      <p><gui>இவ்வளவு காலத்திற்குப் பிறகு அழி</gui> மதிப்பை அமைப்பதன் மூலம், உங்கள் <em>குப்பைத் தொட்டி</em> மற்றும் <em>தற்காலிகக் கோப்புகள்</em> எவ்வளவு காலத்திற்கு ஒரு முறை அழிக்கப்பட வேண்டும் என்பதை அமைக்கலாம்.</p>
    </item>
    <item>
      <p>Use the <gui>Empty Trash</gui> or <gui>Purge Temporary Files</gui>
      buttons to perform these actions immediately.</p>
    </item>
  </steps>

  <note style="tip">
    <p>You can delete files immediately and permanently without using the Trash.
    See <link xref="files-delete#permanent"/> for information.</p>
  </note>

</page>
