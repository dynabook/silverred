<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="power-batterylife" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspend"/>
    <link type="seealso" xref="shell-exit#suspend"/>
    <link type="seealso" xref="shell-exit#shutdown"/>
    <link type="seealso" xref="display-brightness"/>
    <link type="seealso" xref="power-whydim"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>

    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>减少计算机能耗的技巧。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>使用很少电源，延长电池寿命</title>

  <p>电脑消耗很多电，通过一些简便的节电措施，您可以减少电费单并且改善环境。</p>

<section id="general">
  <title>常规提示</title>

<list>
  <item>
    <p>在不使用电脑的时候，<link xref="shell-exit#suspend">挂起</link>电脑，这可以减少大量的电能损耗，而且它也可以很方便地唤醒。</p>
  </item>
  <item>
    <p><link xref="shell-exit#shutdown">Turn off</link> the computer when you
    will not be using it for longer periods. Some people worry that turning off
    a computer regularly may cause it to wear out faster, but this is not the
    case.</p>
  </item>
  <item>
    <p>Use the <gui>Power</gui> panel in <app>Settings</app> to change your
    power settings. There are a number of options that will help to save power:
    you can <link xref="display-blank">automatically blank the screen</link>
    after a certain time, reduce the <link xref="display-brightness">screen
    brightness</link>, and have the computer
    <link xref="power-autosuspend">automatically suspend</link> if you have not
    used it for a certain period of time.</p>
  </item>
  <item>
    <p>关闭外部设备(像打印机和扫描仪)，在您不使用它们的时候。</p>
  </item>
</list>

</section>

<section id="laptop">
  <title>笔记本、上网本和其他电池供电设备</title>

 <list>
   <item>
     <p>Reduce the <link xref="display-brightness">screen
     brightness</link>. Powering the screen accounts for a significant fraction
     of a laptop power consumption.</p>
     <p>大多数笔记本键盘上有一个按钮(或快捷键)，可以降低屏幕亮度。</p>
   </item>
   <item>
     <p>If you do not need an Internet connection for a little while, turn off
     the wireless or Bluetooth cards. These devices work by broadcasting radio
     waves, which takes quite a bit of power.</p>
     <p>一些计算机有一个拨动开关可以用来关闭它，另外有一些是用键盘快捷键，在需要的时候再开启它们。</p>
   </item>
 </list>

</section>

<section id="advanced">
  <title>更多高级提示</title>

 <list>
   <item>
     <p>减少后台运行的任务，电脑在进行多个任务时耗电也多。</p>
     <p>Most of your running applications do very little when you are not
     actively using them. However, applications that frequently grab data from
     the internet or play music or movies can impact your power consumption.</p>
   </item>
 </list>

</section>

</page>
