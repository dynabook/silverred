<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="bluetooth-turn-on-off" xml:lang="nl">

  <info>
    <link type="guide" xref="bluetooth" group="#first"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-04" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-21" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Het Bluetooth-apparaat van uw computer in- of uitschakelen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

<title>Bluetooth in- of uitschakelen</title>

  <p>U kunt Bluetooth inschakelen om verbinding te maken met andere Bluetooth-apparaten of u kunt dit uitschakelen om energie te sparen. Om Bluetooth in te schakelen:</p>

  <steps>
    <item>
      <p>Open het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht en typ <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Klik op het <gui>Bluetooth</gui> om het paneel te openen.</p>
    </item>
    <item>
      <p>Set the switch at the top to on.</p>
    </item>
  </steps>

  <p>Veel laptops hebben een schakelaar of toetscombinatie waarmee Bluetooth kan worden in- of uitgeschakeld. Zoek naar een schakelaar of een toets op uw toetsenbord. De toets op het toetsenbord vindt u vaak met behulp van de <key>Fn</key>-toets.</p>

  <p>Om Bluetooth uit te schakelen:</p>
  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#systemmenu">system menu</gui> from the
      right side of the top bar.</p>
    </item>
    <item>
      <p>Selecteer <gui><media its:translate="no" type="image" mime="image/svg" src="figures/bluetooth-active-symbolic.svg"/> Niet in gebruik</gui>. Het Bluetooth-onderdeel van het menu zal uitklappen.</p>
    </item>
    <item>
      <p>Selecteer <gui>Uitschakelen</gui>.</p>
    </item>
  </steps>

  <note><p>Uw computer is <link xref="bluetooth-visibility">zichtbaar</link> zolang het <gui>Bluetooth</gui>-paneel open staat.</p></note>

</page>
