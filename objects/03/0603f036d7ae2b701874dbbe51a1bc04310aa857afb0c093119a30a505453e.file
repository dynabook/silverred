<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-suspend" xml:lang="pl">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspendfail"/>

    <desc>Uśpienie wstrzymuje działanie komputera, więc używa mniej prądu.</desc>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Projekt dokumentacji GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  </info>

<title>Co dzieje się po uśpieniu komputera?</title>

<p><em>Uśpienie</em> wstrzymuje działanie komputera. Wszystkie programy i dokumenty pozostają otwarte, ale ekran i inne części komputera są wyłączane, aby oszczędzać prąd. Komputer jest nadal włączony i nadal używa trochę prądu. Można go przebudzić naciskając klawisz lub klikając mysz. Jeśli to nie zadziała, to spróbuj nacisnąć przycisk zasilania.</p>

<p>Niektóre komputery mają problemy z obsługą sprzętu, co oznacza że <link xref="power-suspendfail">mogą nie być w stanie się poprawnie uśpić</link>. Dobrze jest przetestować usypianie komputera, aby zobaczyć czy działa zanim zacznie być używane.</p>

<note style="important">
  <title>Zawsze zapisuj pracę przed uśpieniem</title>
  <p>Należy zapisywać pracę przed uśpieniem komputera, w razie gdyby coś się nie powiodło i otwarte programy i dokumenty nie mogły zostać przywrócone po jego przebudzeniu.</p>
</note>

</page>
