<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-switch-tasks" version="1.0 if/1.0" xml:lang="ro">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>
    <link type="guide" xref="getting-started" group="videos"/>
    <title role="trail" type="link">Comutarea între sarcini</title>
    <link type="seealso" xref="shell-windows-switching"/>
    <title role="seealso" type="link">Un ghid despre comutarea între sarcini</title>
    <link type="next" xref="gs-use-windows-workspaces"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Șerbănescu</mal:name>
      <mal:email>daniel [at] serbanescu [dot] dk</mal:email>
      <mal:years>2018-2019</mal:years>
    </mal:credit>
  </info>

  <title>Comutarea între sarcini</title>

  <ui:overlay width="812" height="452">
  <media type="video" its:translate="no" src="figures/gnome-task-switching.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-task-switching.svg"/>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>Comutarea între sarcini</tt:p>
         </tt:div>
         <tt:div begin="5s" end="8s">
           <tt:p if:test="!platform:gnome-classic">Mutați cursorul mausului în colțul cu <gui>Activități</gui> din partea stângă sus a ecranului.</tt:p>
           </tt:div>
         <tt:div begin="9s" end="12s">
           <tt:p>Clic pe o fereastră pentrua comuta la acea sarcină.</tt:p>
         </tt:div>
         <tt:div begin="12s" end="14s">
           <tt:p>Pentru a maximiza fereastra în partea stângă a ecranului, apucați bara de titlu a ferestrei și trageți-o la stânga.</tt:p>
         </tt:div>
         <tt:div begin="14s" end="16s">
           <tt:p>Când jumătate din ecran este evidențiat, eliberați fereastra.</tt:p>
         </tt:div>
         <tt:div begin="16s" end="18">
           <tt:p>Pentru a maximiza fereastra în partea dreaptă, apucați bara de titlu a ferestrei și trageți-o la dreapta.</tt:p>
         </tt:div>
         <tt:div begin="18s" end="20s">
           <tt:p>Când jumătate din ecran este evidențiat, eliberați fereastra.</tt:p>
         </tt:div>
         <tt:div begin="23s" end="27s">
           <tt:p>Apăsați <keyseq><key href="help:gnome-help/keyboard-key-super">Super</key><key>Tab</key></keyseq> pentru a arăta <gui>comutatorul de ferestre</gui>.</tt:p>
         </tt:div>
         <tt:div begin="27s" end="29s">
           <tt:p>Eliberați tasta <key href="help:gnome-help/keyboard-key-super">Super</key> pentru a selecta următoarea fereastră evidențiată.</tt:p>
         </tt:div>
         <tt:div begin="29s" end="32s">
           <tt:p>Pentru a cicla prin lista de ferestre deschise, nu elibarți tasta <key href="help:gnome-help/keyboard-key-super">Super</key> în timp ce apăsați <key>Tab</key>.</tt:p>
         </tt:div>
         <tt:div begin="35s" end="37s">
           <tt:p>Apăsați tasta <key href="help:gnome-help/keyboard-key-super">Super</key> pentru a arăta vederea de ansamblu <gui>Activități</gui>.</tt:p>
         </tt:div>
         <tt:div begin="37s" end="40s">
           <tt:p>Începeți să tastați numele aplicației la care doriți să comutați.</tt:p>
         </tt:div>
         <tt:div begin="40s" end="43s">
           <tt:p>Când aplicația va apărea ca prim rezultat, apăsați <key>Enter</key> pentru a comuta la ea.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

  <section id="switch-tasks-overview">
    <title>Comutarea între sarcini</title>

<if:choose>
<if:when test="!platform:gnome-classic">

  <steps>
    <item><p>Mutați cursorul mausului la colțul <gui>Activități</gui> situat în partea stânga sus a ecranului pentru a afișa vederea de ansamblu <gui>Activități</gui> de unde puteți vedea sarcinile, care rulează momentan, în formă de ferestre mici.</p></item>
     <item><p>Clic pe o fereastră pentrua comuta la acea sarcină.</p></item>
  </steps>

</if:when>
<if:when test="platform:gnome-classic">

  <steps>
    <item><p>Puteți comuta între sarcini folosind <gui>lista de ferestre</gui> din partea inferioară a ecranului. Deschideți sarcini ce apar ca butoane în <gui>lista de ferestre</gui>.</p></item>
     <item><p>Clic pe un buton în <gui>lista de ferestre</gui> pentru a comuta la acea sarcină.</p></item>
  </steps>

</if:when>
</if:choose>

  </section>

  <section id="switching-tasks-tiling">
    <title>Juxtapunerea ferestrelor</title>
    
    <steps>
      <item><p>Pentru a maximiza o ferestră de-alungul unei părți a ecranului, apucați de titlul ferestrei și trageți-o în partea stângă sau dreaptă a ecranului.</p></item>
      <item><p>Când jumătate din ecran este evidențiat, eliberați fereastra pentru a o maximiza de-alungul părții selectate de ecran.</p></item>
      <item><p>Pentru a maximiza două ferestre una lângă alta, apucați bara de titlu a ferestrei a doua și trageți-o în partea opusă a ecranului.</p></item>
       <item><p>Când jumătate din ecran este evidențiat, eliberați fereastra pentru a o maximiza în partea opusă a ecranului.</p></item>
    </steps>
    
  </section>
  
  <section id="switch-tasks-windows">
    <title>Comutarea între ferestre</title>
    
  <steps>
   <item><p>Apăsați <keyseq><key href="help:gnome-help/keyboard-key-super">Super</key><key>Tab</key></keyseq> pentru a afișa <gui>comutatorul de ferestre</gui>, care listează ferestrele deschise curent.</p></item>
   <item><p>Eliberați tasta <key href="help:gnome-help/keyboard-key-super">Super</key> pentru a selecta fereastra evidențiată în <gui>comutatorul de ferestre</gui>.</p>
   </item>
   <item><p>Pentru a cicla prin lista de ferestre deschise, nu elibarți tasta <key href="help:gnome-help/keyboard-key-super">Super</key> în timp ce apăsați <key>Tab</key>.</p></item>
  </steps>

  </section>

  <section id="switch-tasks-search">
    <title>Utilizarea căutării pentru a comuta aplicații</title>
    
    <steps>
      <item><p>Apăsați tasta <key href="help:gnome-help/keyboard-key-super">Super</key> pentru a afișa vederea de ansamblu <gui>Activități</gui>.</p></item>
      <item><p>Începeți să tastați numele aplicației la care doriți să comutați. Aplicațiile ce se potrivesc la ceea ce ați tastat vor apărea pe măsură ce tastați.</p></item>
      <item><p>Când aplicația la care doriți să comutați apare ca prim rezultat, apăsați <key>Enter</key> pentru a comuta la ea.</p></item>
      
    </steps>
    
  </section>

</page>
