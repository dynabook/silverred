<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-purge" xml:lang="gu">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.10" date="2013-09-29" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <credit type="author">
      <name>જીમ કેમ્પબેલ</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>માઇકલ હીલ</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shaun McCance</name>
      <email>mdhillca@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>તમારાં કમ્પ્યૂટરમાંથી કેવી રીતે તમારી કચરાપેટી અને કામચલાઉ ફાઇલોને સાફ કરશે તેને સુયોજિત કરો.</desc>
  </info>

  <title>કચરાપેટી અને કામચલાઉ ફાઇલોને સાફ કરો</title>

  <p>તમારી કચરાપેટીને સાફ કરી રહ્યા છે અને કામચલાઉ ફાઇલો એ તમારાં કમ્પ્યૂટરમાંથી અનિચ્છિત અને બિનજરૂરી ફાઇલોને દૂર કરે છે, તમારી હાર્ડ ડ્રાઇવર પર વધારે જગ્યાને મુક્ત કરે છે. તમે તમારી માટે આને આપમેળે કરવા માટે તમારાં કમ્પ્યૂટરને પણ સુયોજિત કરી શકો છો.</p>

  <p>Temporary files are files created automatically by applications in the
  background. They can increase performance by providing a copy of data that
  was downloaded or computed.</p>

  <steps>
    <title>Automatically empty your trash and clear temporary files</title>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Privacy</gui>.</p>
    </item>
    <item>
      <p>પેનલને ખોલવા માટે <gui>ખાનગી</gui> પર ક્લિક કરો.</p>
    </item>
    <item>
      <p><gui>કચરાપેટી અને કામચલાઉ ફાઇલો સાફ કરો</gui>.</p>
    </item>
    <item>
      <p>Switch one or both of the <gui>Automatically empty Trash</gui> or
      <gui>Automatically purge Temporary Files</gui> switches to on.</p>
    </item>
    <item>
      <p>તમે કેટલી વાર તમારી <em>કચરાપેટી</em> અને <em>કામચલાઉ ફાઇલો</em> ને ખાલી કરવા માંગો છો તે <gui>આટલા સમય પછી ખાલી કરો</gui> ની કિંમત બદલીને સુયોજીત કરી શકો છો.</p>
    </item>
    <item>
      <p>Use the <gui>Empty Trash</gui> or <gui>Purge Temporary Files</gui>
      buttons to perform these actions immediately.</p>
    </item>
  </steps>

  <note style="tip">
    <p>You can delete files immediately and permanently without using the Trash.
    See <link xref="files-delete#permanent"/> for information.</p>
  </note>

</page>
