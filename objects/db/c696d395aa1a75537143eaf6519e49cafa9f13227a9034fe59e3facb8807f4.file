<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-mousekeys" xml:lang="hu">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="pointing"/>

    <revision pkgversion="3.8" date="2013-03-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013, 2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    
    <desc>Egérbillentyűk engedélyezése az egérmutató mozgatásához a numerikus billentyűzettel.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Kattintás és az egérmutató mozgatása a numerikus billentyűzettel</title>

  <p>Ha gondot okoz az egér vagy más mutatóeszköz használata, akkor az egérmutatót irányíthatja a numerikus billentyűzetről is. Ezt a szolgáltatást <em>egérbillentyűknek</em> hívjuk.</p>

  <steps>
    <item>
      <p>Nyissa meg a <gui xref="shell-introduction#activities">Tevékenységek</gui> áttekintést, és kezdje el begépelni az <gui>Akadálymentesítés</gui> szót.</p>
      <p>A <gui>Tevékenységek</gui> áttekintést rákattintva, az egérmutatót a képernyő bal felső sarkába mozgatva, a <keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq> kombináció után az <key>Entert</key> megnyomva, vagy a <key xref="keyboard-key-super">Super</key> billentyűt megnyomva érheti el.</p>
    </item>
    <item>
      <p>Kattintson az <gui>Akadálymentesítés</gui> elemre a panel megnyitásához.</p>
    </item>
    <item>
      <!-- TODO investigate keyboard navigation, because the arrow keys show no
           visual feedback, and do not seem to work. -->
      <p>Use the up and down arrow keys to select <gui>Mouse Keys</gui> in the
      <gui>Pointing &amp; Clicking</gui> section, then press <key>Enter</key>
      to switch the <gui>Mouse Keys</gui> switch to on.</p>
    </item>
    <item>
      <p>Győződjön meg róla, hogy a <key>Num Lock</key> billentyű ki van kapcsolva. Ezután az egérmutatót a numerikus billentyűzet használatával mozgathatja.</p>
    </item>
  </steps>

  <p>A numerikus billentyűzet a billentyűzet számbillentyűkből álló része, amelyet általában négyzetrácsba rendeznek. Ha billentyűzetén nincs numerikus billentyűzet (például laptop esetén), akkor a funkció billentyűt (<key>Fn</key>) kell nyomva tartani, és más billentyűk lesznek numerikus billentyűzetként használhatók. Ha gyakran használja laptopon ezt a szolgáltatást, akkor vásárolhat külső USB vagy Bluetooth numerikus billentyűzetet.</p>

  <p>A numerikus billentyűzet minden száma megfelel egy iránynak. A <key>8</key> lenyomása például felfelé mozgatja a mutatót, a <key>2</key> lenyomása pedig lefelé. Nyomja meg egyszer az <key>5</key> billentyűt az egérkattintáshoz, vagy kétszer gyorsan a dupla kattintáshoz.</p>

  <p>A legtöbb billentyűzeten van a jobb kattintást végző speciális billentyű, amelyet <key xref="keyboard-key-menu">Menü</key> billentyűnek neveznek. Azonban ez nem az egérmutató által mutatott helyen, hanem a billentyűzetfókusz helyén jeleníti meg a helyi menüt. A jobb egérgombbal való kattintásnak az <key>5</key> billentyűvel vagy a bal egérgomb lenyomva tartásával történő kiváltásával kapcsolatos további információkért lásd: <link xref="a11y-right-click"/>.</p>

  <p>Ha a numerikus billentyűzeten számokat szeretne bevinni, mialatt az egérbillentyűk szolgáltatás engedélyezve van, akkor kapcsolja be a <key>Num Lock</key> billentyűt. Az egér nem irányítható a numerikus billentyűzetről, amikor a <key>Num Lock</key> engedélyezett.</p>

  <note>
    <p>A normális számbillentyűk a billentyűzet tetején nem irányítják az egérmutatót. Csak a numerikus billentyűzet számbillentyűi használhatók.</p>
  </note>

</page>
