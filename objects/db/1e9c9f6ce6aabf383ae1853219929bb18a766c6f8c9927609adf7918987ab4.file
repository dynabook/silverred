<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-calibrationdevices" xml:lang="mr">

  <info>

    <link type="guide" xref="color#calibration"/>

    <desc>आम्ही कॅलिब्रेशन साधनांची मोठी संख्याकरिता समर्थन पुरवतो.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

  <title>कोणते रंग मोजमाप यंत्र समर्थीत आहे?</title>

  <p>रंग यंत्रांना समर्थन पुरवण्याकरिता GNOME Argyll रंग व्यवस्थपान प्रणालीवर आधारित आहे. अशा प्रकारे खालील मोजमाप डिस्पले यंत्र समर्थीत आहे:</p>

  <list>
    <item><p>Gretag-Macbeth i1 Pro (स्पेक्ट्रोमीटर्)</p></item>
    <item><p>Gretag-Macbeth i1 मॉनिटर (स्पेक्ट्रोमीटर्)</p></item>
    <item><p>Gretag-Macbeth i1 डिस्पले 1, 2 or LT (कलरीमीटर)</p></item>
    <item><p>X-Rite i1 डिस्पले Pro (कलरीमीटर्)</p></item>
    <item><p>X-Rite ColorMunki रचना किंवा छबी (स्पेक्ट्रोमीटर)</p></item>
    <item><p>X-Rite ColorMunki Create (कलरीमीटर)</p></item>
    <item><p>X-Rite ColorMunki डिसप्ले (कलरीमीटर)</p></item>
    <item><p>Pantone Huey (कलरीमीटर)</p></item>
    <item><p>MonacoOPTIX (कलरीमीटर)</p></item>
    <item><p>ColorVision Spyder २ आणि ३ (कलरीमीटर)</p></item>
    <item><p>Colorimètre HCFR (कलरीमीटर)</p></item>
  </list>

  <note style="tip">
   <p>Pantone Huey सध्या कमी किमतीचा आहे आणि Linux मध्ये सर्वोत्तम समर्थीत हार्डेवेअर आहे.</p>
  </note>

  <p>
    Thanks to Argyll there’s also a number of spot and strip reading
    reflective spectrometers supported to help you calibrating and
    characterizing your printers:
  </p>

  <list>
    <item><p>X-Rite DTP20 “Pulse” (“swipe” type reflective spectrometer)</p></item>
    <item><p>X-Rite DTP22 Digital Swatchbook (स्पॉट टाइप रिफ्लेक्टिव्ह स्पेक्ट्रोमिटर)</p></item>
    <item><p>X-Rite DTP41 (रिडिंग रिफ्लेक्टिव्ह स्पेक्ट्रोमिटर स्पॉट आणि स्ट्रिप करा)</p></item>
    <item><p>X-Rite DTP41T (रिडिंग रिफ्लेक्टिव्ह स्पेक्ट्रोमिटर स्पॉट आणि स्ट्रिप करा)</p></item>
    <item><p>X-Rite DTP51 (रिडिंग रिफ्लेक्टिव्ह स्पेक्ट्रोमिटर स्पॉट करा)</p></item>
  </list>

</page>
