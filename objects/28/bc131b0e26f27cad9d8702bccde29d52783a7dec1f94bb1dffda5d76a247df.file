<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="solaris-mode" xml:lang="cs">
  <info>
    <revision version="0.2" pkgversion="3.11" date="2014-01-26" status="review"/>
    <link type="guide" xref="index#other" group="other"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author copyright">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
      <years>2011</years>
    </credit>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2011, 2014</years>
    </credit>

    <desc>Jak použít režim Solaris, aby byl brán v úvahu počet CPU.</desc>
  </info>

  <title>Co je to režim Solaris?</title>

  <p>V systémech, které mají více procesorů nebo <link xref="cpu-multicore">jader</link>, jich může proces využívat i více naráz. Potom může nastati situace, kdy je ve sloupci <gui>% CPU</gui> zobrazena hodnota větší než 100 % (např. 400 % na 4procesorovém systému). V <gui>režimu Solaris</gui> se u každého procesu podělí <gui>% CPU</gui> počtem CPU v systému, takže celková hodnota nepřesáhne 100 %.</p>

  <p>Když chcete zobrazit <gui>% CPU</gui> v <gui>režimu Solaris</gui>:</p>

  <steps>
    <item><p>V nabídce aplikace klikněte na <gui>Předvolby</gui>.</p></item>
    <item><p>Klikněte na kartu <gui>Procesy</gui>.</p></item>
    <item><p>Vyberte <gui>Dělit vytížení procesoru počtem procesorů</gui>.</p></item>
  </steps>

    <note><p>Výraz <gui>režim Solaris</gui> pochází z UNIXu od firmy Sun na rozdíl od v Linuxu výchozího režimu IRIX pojmenovaného podle UNIXu od firmy SGI.</p></note>

</page>
