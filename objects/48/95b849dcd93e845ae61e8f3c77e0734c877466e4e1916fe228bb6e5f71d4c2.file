<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-adhoc" xml:lang="de">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.32" date="2019-09-01" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <desc>Ein Ad-Hoc-Netzwerk verwenden, um anderen Geräten zu ermöglichen, sich mit Ihrem Rechner zu verbinden und seine Netzwerkverbindungen zu nutzen.</desc>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2019.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2019.</mal:years>
    </mal:credit>
  </info>

<title>Einen Funknetzwerk-Hotspot erstellen</title>

  <p>Sie können Ihren Rechner als Funknetzwerk-Hotspot verwenden. Dies ermöglicht anderen Geräten, sich mit Ihnen ohne ein separates Netzwerk zu verbinden. Außerdem ist es dadurch möglich, eine Internetverbindung freizugeben, die Sie über eine andere Schnittstelle erstellt haben, wie ein kabelgebundenes oder ein mobiles Breitbandnetzwerk.</p>

<steps>
  <item>
    <p>Öffnen Sie das <gui xref="shell-introduction#systemmenu">Systemmenü</gui> auf der rechten Seite der obersten Leiste.</p>
  </item>
  <item>
    <p>Wählen Sie <gui><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-signal-excellent-symbolic.svg" width="16" height="16"/> WLAN nicht verbunden</gui> oder den Namen des Funknetzwerks, mit dem Sie bereits verbunden sind. Der WLAN-Abschnitt des Menüs wird erweitert.</p>
  </item>
  <item>
    <p>Wählen Sie <gui>Drahtlosnetzwerkeinstellungen</gui>.</p></item>
  <item><p>Klicken Sie auf den Menüknopf rechts oben im Fenster und wählen Sie <gui>WLAN-Hotspot einschalten …</gui>.</p></item>
  <item><p>Falls Sie bereits mit einem Funknetzwerk verbunden sind, werden Sie gefragt, ob Sie die Verbindung zu diesem Netzwerk trennen wollen. Ein einzelner Funknetzwerkadapter kann nur eine Verbindung gleichzeitig erstellen oder sich damit verbinden. Klicken Sie zum Bestätigen auf <gui>Einschalten</gui>.</p></item>
</steps>

<p>Ein Netzwerkname (SSID) und ein Sicherheitsschlüssel werden automatisch erzeugt. Der Netzwerkname basiert auf dem Namen Ihres Rechners. Andere Geräte benötigen diese Informationen, um sich mit dem soeben angelegten Hotspot zu verbinden.</p>

</page>
