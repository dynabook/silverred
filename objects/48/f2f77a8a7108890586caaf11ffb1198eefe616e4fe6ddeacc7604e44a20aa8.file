<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="login-enterprise" xml:lang="ko">

  <info>
    <link type="guide" xref="login#management"/>
    <revision pkgversion="3.12" date="2014-01-28" status="draft"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>그놈에 로그인할 때 액티브 디렉터리 또는 IPA 도메인 인증을 사용합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조재은</mal:name>
      <mal:email>ckr971028@gmail.com</mal:email>
      <mal:years>2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

  <title>그놈에서 기업 인증으로 로그인하기</title>
  
  <p>네트워크에 액티브 디렉터리나 IPA 도메인이 있고, 도메인 계정을 가지고 있으면 그놈으로 로그인할 때 해당 도메인 인증을 사용할 수 있습니다.</p>
  <p>머신에 도메인 계정 설정이 잘 끝나면, 사용자는 해당 계정으로 그놈으로 로그인할 수 있습니다. 로그인 프롬프트가 뜨면 도메인 사용자 이름과 <sys>@</sys> 기호, 도메인 이름을 입력하십시오. 예를 들면, 도메인 이름이 <var>example.com</var> 이고 사용자 이름이 <var>User</var>면 다음과 같습니다:</p>
  <screen><input>User@example.com</input></screen>
  <p>도메인 계정을 머신에 설정했을 경우 로그인에 도움이 될 만한 형식 설명 힌트가 화면에 나타나야합니다.</p>

  <section id="enterprise-login-welcome-screens">
    <title>환영 화면에서 기업 인증 사용하기</title>
    <p>기업 인증 머신으로 설정하지 않았다면 <app>그놈 초기 설정</app> 프로그램에서 <gui>환영</gui> 화면을 설정할 수 있습니다.</p>
    <steps>
    <title>기업 인증 설정</title>
      <item>
        <p><gui>로그인</gui> 환영 화면에서 <gui>기업 로그인 설정</gui>을 누르십시오.</p>
      </item>
      <item>
        <p><gui>도메인</gui> 입력란에 도메인 이름이 없다면 도메인 이름을 입력하십시오.</p>
      </item>
      <item>
        <p>도메인 계정 사용자와 암호를 관련 입력란에 입력하십시오.</p>
      </item>
      <item>
        <p><gui>다음</gui>을 누르십시오.</p>
      </item>
    </steps>
    <p>도메인 설정에 따라, 로그인 진행시 도메인 관리자 이름과 암호를 물어볼 수 있습니다.</p>
  </section>
  <section id="enterprise-login-change-to">
    <title>그놈에 로그인할 기업 인증 정보 바꾸기</title>
    <p>이미 초기 설정이 끝났고 그놈으로 로그인할 도메인 계정을 시작하려면, 그놈 설정의 사용자 창에서 설정할 수 있습니다.</p>
  <steps>
    <title>기업 인증 설정</title>
    <item>
      <p><gui href="help:gnome-help/shell-terminology">활동 개요</gui> 를 열어 <gui>사용자</gui>를 입력하십시오.</p>
    </item>
    <item>
      <p><gui>사용자</gui>를 눌러 창을 여십시오.</p>
    </item>
    <item>
      <p><gui>잠금 해제</gui> 단추를 누르고 컴퓨터 관리자 암호를 입력하십시오.</p>
    </item>
    <item>
      <p>창 좌측 하단의 <gui>[+]</gui> 단추를 누르십시오.</p>
    </item>
    <item>
      <p><gui>기업 로그인</gui> 단추를 누르십시오.</p>
    </item>
    <item>
      <p>기업 계정 도메인, 사용자, 암호를 입력하고 <gui>추가</gui>를 누르십시오.</p>
   </item>
  </steps>
  <p>도메인 설정에 따라, 로그인 진행시 도메인 관리자 이름과 암호를 물어볼 수 있습니다.</p>
  </section>
  <section id="enterprise-login-troubleshoot">
    <title>문제 해결 및 고급 설정</title>
    <p><cmd>realm</cmd> 명령과 하위 명령은 기업 로그인 실패시 문제 해결책으로 활용할 수 있습니다. 예를 들어 기업 로그인 설정 여부와 내용을 확인하려면 다음 명령을 실행하면 됩니다:</p>
    <screen><output>$ </output><input>realm list</input></screen>
    <p>네트워크 관리자는 관련 도메인에 해당 워크스테이션을 미리 참여하도록 설정하는게 좋습니다. 간단하게 <cmd>realm join</cmd> 명령으로 시작하거나, 스크립트 자동화 방식으로 <cmd>realm join</cmd> 명령을 실행하면 됩니다.</p>
  </section>
  <section id="enterprise-login-more-information">
     <title>더 많은 정보</title>
   <list>
    <item>
      <p>realmd <link href="http://www.freedesktop.org/software/realmd/docs/">관리자 안내서</link>에는 기업 로그인에 실패했을 때에 대응할 더 자세한 정보가 있습니다.</p>
    </item>
   </list>
  </section>

</page>
