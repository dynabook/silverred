<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="lockdown-single-app-mode" xml:lang="ko">

  <info>
    <link type="guide" xref="user-settings#lockdown"/>
    <link type="guide" xref="sundry#session"/>
    <link type="seealso" xref="lockdown-printing"/>
    <link type="seealso" xref="lockdown-file-saving"/>
    <link type="seealso" xref="lockdown-repartitioning"/>
    <link type="seealso" xref="lockdown-command-line"/>
    <link type="seealso" xref="login-automatic"/>
    <link type="seealso" xref="session-custom"/>
    <link type="seealso" xref="session-user"/>

    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>mclasen@redhat.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
        <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
       
    <desc>키오스크 같은, 단일 프로그램 시스템을 구성합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조재은</mal:name>
      <mal:email>ckr971028@gmail.com</mal:email>
      <mal:years>2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

  <title>단일 프로그램 모드 설정</title>

  <p>단일 프로그램 모드는 대화식 키오스크처럼 셸을 설정한 수정 그놈 셸입니다. 관리자는 일부 기능에만 집중하도록 사용자에게 표준 데스크톱의 일부 기능을 잠급니다.</p>

  <p>여러 영역에서 다양한 기능(통신에서 엔터테인먼트, 교육 등...)을 수행할 단일 프로그램 모드를 구성하고, 자체 기능 제공 머신, 즉, 행사 관리자, 등록처 등으로 활용하십시오.</p>

  <steps>
  <title>단일 프로그램 모드 구성</title>
    <item>
      <p>인쇄, 터미널 접근 등을 못하도록 잠급니다.</p>
      <list type="disc">
        <item><p><link xref="lockdown-command-line"/></p></item>
        <item><p><link xref="lockdown-printing"/></p></item>
        <item><p><link xref="lockdown-file-saving"/></p></item>
        <item><p><link xref="lockdown-repartitioning"/></p></item>
      </list>
    </item>
    <item>
      <p>각 사용자에 대해 <file>/etc/gdm/custom.conf</file> 파일에서 자동 로그인을 설정하십시오.</p>
      <p>자세한 내용은 <link xref="login-automatic"/> 문서를 참고하십시오.</p>
    </item>
    <item>
      <p>전형적인 작명 규칙을 따르는 이름(공백, 특수문자를 허용하지 않고, 대시 문자 또는 숫자로 시작하지 않는..)으로 새 사용자를 만드십시오. 또한 사용자 이름이 세션을 참조하는 등의 관련 이름에 해당하는지 확인하십시오. 이를테면 <em>kiosk-user</em>가 있습니다.</p>
    </item>
    <item>
      <p>사용자 이름에 해당하는 세션을 만드십시오(예를 들어, 위의 <em>kiosk-user</em> 사용자에 대해 <em>kiosk</em> 가 잘 들어맞습니다). <file>/usr/share/xsessions/<var>kiosk</var>.desktop</file> 파일을 만들고 <code>Exec</code> 줄을 다음과 같이 설정하십시오:</p>
<code>
Exec=gnome-session --session kiosk
</code>
    </item>
    <item>
      <p><em>kiosk-user</em> 기본 세션은 <file>/var/lib/AccountsService/users/<var>kiosk-user</var> </file> 파일에 다음 내용을 추가하여 설정하십시오:</p>
<code>
XSession=kiosk
</code>
    </item>
    <item>
      <p><em>kiosk</em> 세션을 다음 내용이 들어간 개별 세션 명세를 넣어 정의하십시오:</p>
<code>
RequiredComponents=kiosk-app;gnome-settings-daemon;kiosk-shell;
</code>
      <p>이 줄은 <sys>kiosk-app</sys> (예제 프로그램), <sys>gnome-settings-daemon</sys> (그놈 세션 표준 구성요소), <sys>kiosk-shell</sys> (그놈 셸 개별 설정 버전) 프로그램을 실행합니다.</p>
    </item>
    <item>
      <p>Create a desktop file for <sys>kiosk-shell</sys> in
      <file>/usr/share/applications/kiosk-shell.desktop</file>, containing the
      following lines:</p>
<code>
[Desktop Entry]
Exec=gnome-shell --mode=kiosk
</code>
    </item>
    <item>
      <p><file>/usr/share/gnome-shell/modes/kiosk.json</file> 모드 정의를 만드십시오. <sys>gnome-shell</sys> 사용자 인터페이스를 정의하는 간단한 json 파일입니다.</p>
      <p>시작하면서 <file>/usr/share/gnome-shell/modes/classic.json</file> 파일과 <file>/usr/share/gnome-shell/modes/initial-setup.json</file> 파일을 예로 살펴보십시오.</p>
    </item>
  </steps>

</page>
