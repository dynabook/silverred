<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-what" xml:lang="fi">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Gnomen dokumentointiprojekti</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Varmuuskopioi kaikki, mitä ei ole varaa menettää jos kaikki menee pieleen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

  <title>Mitä tulisi varmuuskopioida</title>

  <p>Tärkeintä on varmuuskopioida <link xref="backup-thinkabout">kaikista tärkeimmät tiedostot</link> sekä tiedostot, joiden uudelleenluominen on hankalaa. Esimerkki tärkeimmästä vähiten tärkeimpään:</p>

<terms>
 <item>
  <title>Henkilökohtaiset tiedostot</title>
   <p>Muun muassa asiakirjat, laskentataulukot, sähköposti, kalenteri, taloustiedot, perhevalokuvat ja muut korvaamattomat henkilökohtaiset tiedostot.</p>
 </item>

 <item>
  <title>Henkilökohtaiset asetuksesi</title>
   <p>Tämä sisältää väreihin, taustoihin sekä näytön ja hiiren asetuksiin tehdyt muutokset. Lisäksi tähän kuuluu myös sovellusten asetukset, kuten <app>LibreOfficen</app>, musiikkisoittimen ja sähköpostisovelluksen. Nämä asetukset on korvattavissa, mutta niiden tekemisessä uudelleen saattaa kulua hetki.</p>
 </item>

 <item>
  <title>Järjestelmäasetukset</title>
   <p>Suurin osa käyttäjistä ei koskaan muuta järjestelmän oletusasetuksia. Jos muutat järjestelmäasetuksia syystä tai toisesta, kuten jos ylläpidät palvelinta, ne kannattaa varmuuskopioida.</p>
 </item>

 <item>
  <title>Asennetut ohjelmat</title>
   <p>Sovellukset voidaan yleensä palauttaa kohtalaisen vähäisellä vaivalla asentamalla ne uudelleen ongelmien jälkeen.</p>
 </item>
</terms>

  <p>Yleisesti ottaen, haluat varmuuskopioida tiedostot, jotka ovat korvaamattomia ja tiedostot, joiden uudelleenluominen on hankalaa ilman kopiota. Jos tiedostojen uudelleenluominen on helppoa, et välttämättä halua kuluttaa tallennustilaa niiden varmuuskopiointiin.</p>

</page>
