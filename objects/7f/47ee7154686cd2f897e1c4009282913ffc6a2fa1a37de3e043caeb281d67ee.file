<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-use-system-search" xml:lang="as">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>ইয়াকুব স্টাইনাৰ</name>
    </credit>
    <credit type="author">
      <name>পিটাৰ ক'ভাৰ</name>
    </credit>
    <credit type="author">
      <name>Hannie Dumoleyn</name>
    </credit>
    <link type="guide" xref="getting-started" group="tasks"/>
    <title role="trail" type="link">চিস্টেমৰ সন্ধান ব্যৱহাৰ কৰক</title>
    <link type="seealso" xref="shell-apps-open"/>
    <title role="seealso" type="link">চিস্টেমৰ সন্ধান ব্যৱহাৰ কৰিবলৈ এটা শিক্ষা</title>
    <link type="next" xref="gs-get-online"/>
  </info>

  <title>চিস্টেমৰ সন্ধান ব্যৱহাৰ কৰক</title>

    <media its:translate="no" type="image" mime="image/svg" src="gs-search1.svg" width="100%"/>
    
    <steps>
    <item><p>Open the <gui>Activities</gui> overview by clicking <gui>Activities</gui> 
    at the top left of the screen, or by pressing the
    <key href="help:gnome-help/keyboard-key-super">Super</key> key. 
    Start typing to search.</p>
    <p>আপুনি টাইপ কৰি থাকোতে আপোনাৰ টাইপ কৰা সৈতে মিল খোৱা ফলাফলসমূহ উপস্থিত হব। প্ৰথম ফলাফল সদায় উজ্জ্বল থাকে আৰু ওপৰত দেখুৱা হয়।</p>
    <p>প্ৰথম উজ্জ্বল ফলাফললৈ যাবলৈ <key> Enter</key> টিপক।</p></item>
    </steps>
    
    <media its:translate="no" type="image" mime="image/svg" src="gs-search2.svg" width="100%"/>
    <steps style="continues">
      <item><p>সন্ধান ফলাফলসমূহত উপস্থিত হব পৰা বস্তুবোৰত অন্তৰ্ভুক্ত হ'ব পাৰে:</p>
      <list>
        <item><p>মিল থকা এপ্লিকেচনসমূহ, সন্ধান ফলাফলসমূহৰ ওপৰত দেখুৱা হয়,</p></item>
        <item><p>মিল থকা সংহতিসমূহ,</p></item>
        <item><p>matching contacts,</p></item>
        <item><p>matching documents,</p></item>
        <item><p>matching calendar,</p></item>
        <item><p>matching calculator,</p></item>
        <item><p>matching software,</p></item>
        <item><p>matching files,</p></item>
        <item><p>matching terminal,</p></item>
        <item><p>matching passwords and keys.</p></item>
      </list>
      </item>
      <item><p>সন্ধান ফলাফলসমূহত, যাবলৈ বিচৰা বস্তুত ক্লিক কৰক।</p>
      <p>বিকল্পভাৱে, এৰ' কিসমূহ ব্যৱহাৰ কৰি এটা বস্তু উজ্জ্বল কৰক আৰু <key>Enter</key> টিপক।</p></item>
    </steps>

    <section id="use-search-inside-applications">
    
      <title>অভ্যন্তৰীক এপ্লিকেচনসমূহৰ পৰা সন্ধান কৰক</title>
      
      <p>চিস্টেম সন্ধানে বিভিন্ন এপ্লিকেচনৰ পৰা ফলাফলসমূহ একত্ৰিত কৰে। সন্ধান ফলাফলসমূহৰ বাঁও- ফালে, আপুনি সন্ধান ফলাফলসমূহ দেখুৱা এপ্লিকেচনসমূহৰ আইকন দেখি পাব। সেই আইকনৰ সৈতে জড়িত এপ্লিকেচনৰ ভিতৰৰ পৰা সন্ধান পুনাৰম্ভ কৰিবলৈ আইকসমূহৰ এটাত ক্লিক কৰক। যিহেতু কেৱল উত্তম মিলসমূহ <gui>কাৰ্য্যসমূহ অভাৰভিউ</gui> ত দেখুৱা হয়, এপ্লিকেচনৰ ভিতৰৰ পৰা সন্ধান কৰিলে আপুনি অধিক ভাল সন্ধানৰ ফলাফল পাব পাৰে।</p>

    </section>

    <section id="use-search-customize">

      <title>সন্ধানৰ ফলাফলসমূহ স্বনিৰ্বাচন কৰক</title>

      <media its:translate="no" type="image" mime="image/svg" src="gs-search-settings.svg" width="100%"/>

      <note style="important">
      <p>Your computer lets you customize what you want to display in the search
       results in the <gui>Activities Overview</gui>. For example, you can
        choose whether you want to show results for websites, photos, or music.
        </p>
      </note>


      <steps>
        <title>সন্ধান ফলাফলসমূহত কি প্ৰদৰ্শন কৰা হ'ব স্বনিৰ্বাচন কৰিবলে:</title>
        <item><p>Click the <gui xref="shell-introduction#yourname">system menu</gui>
        on the right side of the top bar.</p></item>
        <item><p>Click <gui>Settings</gui>.</p></item>
        <item><p>Click <gui>Search</gui> in the left panel.</p></item>
        <item><p>In the list of search locations, click the switch next to the
        search location you want to enable or disable.</p></item>
      </steps>

    </section>

</page>
