<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-resize" xml:lang="es">
  <info>
    <link type="guide" xref="disk"/>


    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <revision pkgversion="3.25.90" date="2017-08-17" status="review"/>

    <desc>Reducir o ampliar un sistema de archivos y su partición.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>Ajustar el tamaño de un sistema de archivos</title>

  <p>Un sistema de archivos puede extenderse para hacer uso del espacio libre después de su partición. A veces esto es posible incluso mientras el sistema de archivos está montado.</p>
  <p>Para hacer espacio para otra partición después del sistema de archivos, se puede encoger de forma acorde al espacio libre que haya en él.</p>
  <p>No todos los sistemas de archivos soportan el cambio de tamaño.</p>
  <p>El tamaño de la partición se cambiará junto con el tamaño del sistema de archivos. También es posible redimensionar una partición sin un sistema de archivos del mismo modo.</p>

<steps>
  <title>Redimensionar un sistema de archivos/partición</title>
  <item>
    <p>Abra <app>Discos</app> desde la vista de <gui>Actividades</gui>.</p>
  </item>
  <item>
    <p>Seleccione el disco que contiene el sistema de archivos de la lista de dispositivos de almacenamiento de la izquierda. Si hay más de un volumen en el disco, seleccione el que contiene el sistema de archivos.</p>
  </item>
  <item>
    <p>En la barra de herramientas, debajo de la sección <gui>Volúmenes</gui>, pulse el botón del menú. Después pulse <gui>Redimensionar sistema de archivos…</gui>. o <gui>Redimensionar</gui> si no hay sistema de archivos.</p>
  </item>
  <item>
    <p>Se abrirá un diálogo en el que podrá elegir el nuevo tamaño. El sistema de archivos se montará para calcular el tamaño mínimo por la cantidad de contenido actual. Si la reducción no está soportada, el tamaño mínimo será el tamaño actual. Deje suficiente espacio en el sistema de archivos cuando lo reduzca para asegurar que podrá funcionar rápidamente y de forma fiable.</p>
    <p>Dependiendo de la cantidad de datos que se deben mover de la parte reducida, la redimensión del sistema de archivos puede tardar mucho tiempo.</p>
    <note style="warning">
      <p>El redimensionamiento del sistema de archivos implica automáticamente la <link xref="disk-repair">reparación</link> del sistema de archivos. En cualquier caso, se recomienda hacer una copia de seguridad de los datos importantes antes de empezar. La acción no deberá interrumpirse o podría resultar en un sistema de archivos dañado.</p>
    </note>
  </item>
  <item>
      <p>Para realizar la acción confirme pulsando el botón <gui style="button">Redimensionar</gui>.</p>
   <p>La acción desmontará el sistema de archivo si redimensionar un sistema de archivos montado no está soportado. Sea paciente mientras el sistema de archivos se redimensiona.</p>
  </item>
  <item>
    <p>Al finalizar las acciones necesarias de redimensión y reparación del sistema de archivos estará listo para usarlo de nuevo.</p>
  </item>
</steps>

</page>
