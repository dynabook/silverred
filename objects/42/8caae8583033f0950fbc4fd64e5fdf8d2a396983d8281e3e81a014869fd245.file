<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="contacts-add-remove" xml:lang="de">

  <info>
    <link type="guide" xref="contacts"/>
    <revision pkgversion="3.5.5" date="2012-08-13" status="review"/>
    <revision pkgversion="3.8" date="2013-04-27" status="review"/>
    <revision pkgversion="3.12" date="2014-02-26" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Lucie Hankey</name>
      <email>ldhankey@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>So entfernen Sie einen Kontakt oder fügen ihn dem lokalen Adressbuch hinzu.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2019.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2019.</mal:years>
    </mal:credit>
  </info>

<title>Einen Kontakt hinzufügen oder entfernen</title>

  <p>So fügen Sie einen Kontakt hinzu:</p>

  <steps>
    <item>
      <p>Klicken Sie auf den Knopf <gui style="button">+</gui>.</p>
    </item>
    <item>
      <p>Geben Sie im Fenster <gui>Neuer Kontakt</gui> den Kontaktnamen und weitere Informationen ein. Klicken Sie auf die Auswahlliste neben jedem Feld und wählen Sie die Art der Angabe.</p>
    </item>
    <item>
      <p>Klicken Sie auf den Knopf <gui style="button">Hinzufügen</gui>.</p>
    </item>
  </steps>

  <p>So entfernen Sie einen Kontakt:</p>

  <steps>
    <item>
      <p>Wählen Sie den Kontakt in der Kontaktliste aus.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui style="button">Bearbeiten</gui> in der oberen rechten Ecke von <app>Kontakte</app>.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui style="button">Kontakt entfernen</gui>.</p>
    </item>
  </steps>

</page>
