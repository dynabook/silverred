<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-preview" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-preview"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Šon Mek Kens</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Dejvid King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Odredite kada će se koristiti minijature za datoteke.</desc>
  </info>

<title>Postavke pregleda upravnika datoteka</title>

<p>The file manager creates thumbnails to preview image, video, and text
files. Thumbnail previews can be slow for large files or over networks, so
you can control when previews are made. Click the menu button in the top-right
of the window, select <gui>Preferences</gui>, then select the
<gui>Search &amp; Preview</gui> tab.</p>

<terms>
  <item>
    <title><gui>Datoteke</gui></title>
    <p>By default, all previews are done for
    <gui>Files on this computer only</gui>, those on your computer or connected
    external drives. You can set this feature to <gui>All Files</gui> or
    <gui>Never</gui>. The file manager can
    <link xref="nautilus-connect">browse files on other computers</link> over
    a local area network or the internet. If you often browse files over a local
    area network, and the network has high bandwidth, you may want to set the
    preview option to <gui>All Files</gui>.</p>
    <p>Pored toga, možete da koristite postavku <gui>Samo za datoteke manje od</gui> da ograničite veličinu datoteka za pretpregled.</p>
  </item>
  <item>
    <title><gui>File count</gui></title>
    <p>If you show file sizes in <link xref="nautilus-list">list view columns</link>
    or <link xref="nautilus-display#icon-captions">icon captions</link>,
    folders will be shown with a count of how many files and folders they
    contain. Counting items in a folder can be slow, especially for very large
    folders, or over a network.</p>
    <p>You can turn this feature on or off, or turn it on only for files on
    your computer and local external drives.</p>
  </item>
</terms>
</page>
