<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-switching" xml:lang="pt">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>
    <link type="guide" xref="shell-overview#apps"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.12" date="2014-03-07" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Projeto de documentação de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>


    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Carregue <keyseq><key>Super</key><key>Tab</key></keyseq>.</desc>
  </info>

<title>Mudar entre janelas</title>

  <p>No <em>seletor de janelas</em> pode ver todos as aplicações em execução que têm uma interface gráfica. Isto faz que a mudança entre tarefas seja um processo dum só passo e proporciona uma visão completa das aplicações que estão em execução.</p>

  <p>Desde uma área de trabalho:</p>

  <steps>
    <item>
      <p>Carregue <keyseq><key xref="keyboard-key-super">Super</key><key>Tab</key></keyseq> para mostrar o <gui>intercambiador de janelas</gui>.</p>
    </item>
    <item>
      <p>Solte a tecla <key xref="keyboard-key-super">Super</key> para selecionar a seguinte janela (realçada) no seletor.</p>
    </item>
    <item>
      <p>Otherwise, still holding down the <key xref="keyboard-key-super">Super</key>
      key, press <key>Tab</key> to cycle through the list of open
      windows, or <keyseq><key>Shift</key><key>Tab</key></keyseq> to cycle
      backwards.</p>
    </item>
  </steps>

  <p if:test="platform:gnome-classic">Também pode usar a lista de janelas na barra inferior para aceder a todas suas janelas abertas e mudar entre elas.</p>

  <note style="tip" if:test="!platform:gnome-classic">
    <p>As janelas no seletor de janelas agrupam-se por aplicações. As vistas prévias dos aplicações com múltiplas janelas se despliegan quando as clica. Carregue a tecla <key xref="keyboard-key-super">Super</key> e carregue <key>`</key> (ou a tecla <key>Tab</key>) para percorrer a lista.</p>
  </note>

  <p>Também pode mover entre os ícones de aplicações no seletor de janelas com as teclas <key>→</key> ou <key>←</key>, ou selecionar uma clicando com o rato.</p>

  <p>As vistas prévias dos aplicações com uma sozinha janela podem-se mostrar com a tecla <key>↓</key>.</p>

  <p>Carregue numa <link xref="shell-windows">janela</link> na vista de <gui>Atividades</gui> para alterar para ela e sair da vista prévia. Se tem várias <link xref="shell-windows#working-with-workspaces">áreas de trabalho</link> abertas, pode carregar na cada uma delas para ver as janelas abertas na cada área de trabalho.</p>

</page>
