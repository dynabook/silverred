<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sound-alert" xml:lang="cs">

  <info>
    <link type="guide" xref="media#sound"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak si vybrat zvuk, který se přehraje při upozornění, a jak nastavit jeho hlasitost, případně jak jej vypnout.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Výběr a vypnutí zvuku upozornění</title>

  <p>Pro určité typy zpráv a událostí přehrává počítač jednoduchý zvuk kvůli upozornění. Můžete si vybrat z několika přichystaných zvuků, nastavit jejich hlasitost nezávisle na systémové hlasitosti nebo zvuky upozornění zcela zakázat.</p>

  <steps>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Zvuk</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Zvuk</gui> otevřete příslušný panel.</p>
    </item>
    <item>
      <p>V části <gui>Zvuk upozornění</gui> vyberte zvuk upozornění. Jednotlivé zvuky se přehrají, když na ně kliknete, takže si je můžete předem poslechnout.</p>
    </item>
  </steps>

  <p>Použijte táhlo hlasitosti pro <gui>Systémové zvuky</gui> v části <gui>Úrovně hlasitosti</gui> k nastavení hlasitosti zvuku upozornění. Neovlivní to hlasitost ostatních zvuků, jako jsou hudba, filmy apod.</p>

</page>
