<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-purge" xml:lang="de">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.10" date="2013-09-29" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shaun McCance</name>
      <email>mdhillca@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Stellen Sie ein, wie oft auf Ihrem Rechner der Papierkorb geleert und temporäre Dateien gelöscht werden.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2019.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2019.</mal:years>
    </mal:credit>
  </info>

  <title>Papierkorb leeren und temporäre Dateien löschen</title>

  <p>Das Leeren des Papierkorbs und das Löschen temporärer Dateien entfernt nicht mehr benötigte Dateien von Ihrem Rechner und macht auch mehr Platz auf Ihrer Festplatte frei. Sie können manuell den Papierkorb leeren und temporäre Dateien löschen, aber auch Ihren Rechner so einrichten, dass dies automatisch erledigt wird.</p>

  <p>Temporäre Dateien werden automatisch von Anwendungen im Hintergrund angelegt. Sie können die Leistung verbessern, indem Kopien heruntergeladener oder errechneter Daten bereitgestellt werden.</p>

  <steps>
    <title>Papierkorb automatisch leeren und temporäre Dateien löschen</title>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Privatsphäre</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Privatsphäre</gui>, um das Panel zu öffnen.</p>
    </item>
    <item>
      <p>Wählen Sie <gui>Papierkorb leeren und temporäre Dateien</gui>.</p>
    </item>
    <item>
      <p>Stellen Sie die Schalter <gui>Automatisch den Papierkorb leeren</gui> oder <gui>Automatisch temporäre Dateien löschen</gui> an.</p>
    </item>
    <item>
      <p>Legen Sie mit dem Wert für <gui>Löschen nach</gui> fest, wie oft der <em>Papierkorb</em> geleert und <em>Temporäre Dateien</em> gelöscht werden sollen.</p>
    </item>
    <item>
      <p>Verwenden Sie die Knöpfe <gui>Papierkorb leeren</gui> und <gui>Temporäre Dateien löschen</gui>, um diese Aktionen sofort auszuführen.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Sie können Dateien direkt und dauerhaft löschen, ohne den Papierkorb zu verwenden. Weitere Informationen finden Sie in <link xref="files-delete#permanent"/>.</p>
  </note>

</page>
