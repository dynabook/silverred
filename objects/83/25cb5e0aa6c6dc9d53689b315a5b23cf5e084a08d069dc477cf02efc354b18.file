<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="net-security-tips" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.4.0" date="2012-02-21" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Steven Richards</name>
      <email>steven.richardspc@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>General tips to keep in mind when using the internet.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>使用网络时保持安全</title>

  <p>A possible reason for why you are using Linux is the robust security that
  it is known for. One reason that Linux is relatively safe from malware and
  viruses is due to the lower number of people who use it. Viruses are targeted
  at popular operating systems, like Windows, that have an extremely large user
  base. Linux is also very secure due to its open source nature, which allows
  experts to modify and enhance the security features included with each
  distribution.</p>

  <p>尽管已经有很多措施来保护您的 Linux 系统安全，但漏洞总会存在。作为一个普通网络用户，您仍容易遭受以下威胁：</p>

  <list>
    <item>
      <p>网络钓鱼(尝试通过欺骗获取敏感信息的站点)</p>
    </item>
    <item>
      <p><link xref="net-email-virus">转发恶意电子邮件</link></p>
    </item>
    <item>
      <p><link xref="net-antivirus">带有恶意企图的应用程序(病毒)</link></p>
    </item>
    <item>
      <p><link xref="net-wireless-wepwpa">Unauthorized remote/local network
      access</link></p>
    </item>
  </list>

  <p>要远离恶意入侵，请牢记以下提示：</p>

  <list>
    <item>
      <p>不要打开未知人员发送的电子邮件、附件或链接。</p>
    </item>
    <item>
      <p>If a website’s offer is too good to be true, or asks for sensitive
      information that seems unnecessary, then think twice about what
      information you are submitting and the potential consequences if that
      information is compromised by identity thieves or other criminals.</p>
    </item>
    <item>
      <p>Be careful in providing
      <link xref="user-admin-explain">root level permissions</link> to any
      application, especially ones that you have not used before or which are
      not well-known. Providing anyone or anything with root level permissions
      puts your computer at high risk to exploitation.</p>
    </item>
    <item>
      <p>请确保您只运行必要的远程访问服务。<app>SSH</app> 或 <app>VNC</app> 有时会很有用，但如果保障不力，也会让您的计算机对入侵敞开大门。考虑使用<link xref="net-firewall-on-off">防火墙</link>来帮助计算机防御入侵。</p>
    </item>
  </list>

</page>
