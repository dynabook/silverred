<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-use-system-search" xml:lang="ta">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>ஜேக்கப் ஸ்டெயினர்</name>
    </credit>
    <credit type="author">
      <name>பெட்ர் கோவர்</name>
    </credit>
    <credit type="author">
      <name>Hannie Dumoleyn</name>
    </credit>
    <link type="guide" xref="getting-started" group="tasks"/>
    <title role="trail" type="link">கணினி தேடலைப் பயன்படுத்து</title>
    <link type="seealso" xref="shell-apps-open"/>
    <title role="seealso" type="link">கணினி தேடலைப் பயன்படுத்துவது பற்றிய பயிற்சி</title>
    <link type="next" xref="gs-get-online"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Dr. T. Vasudevan</mal:name>
      <mal:email>agnihot3@gmail.com</mal:email>
      <mal:years>2012Shantha kumar &lt;shantha.thamizh@gmail.com&gt; 2013</mal:years>
    </mal:credit>
  </info>

  <title>கணினி தேடலைப் பயன்படுத்து</title>

    <media its:translate="no" type="image" mime="image/svg" src="gs-search1.svg" width="100%"/>
    
    <steps>
    <item><p>Open the <gui>Activities</gui> overview by clicking <gui>Activities</gui> 
    at the top left of the screen, or by pressing the
    <key href="help:gnome-help/keyboard-key-super">Super</key> key. 
    Start typing to search.</p>
    <p>நீங்கள் தட்டச்சு செய்யும் போதே, நீங்கள் தட்டச்சு செய்த உரைக்குப் பொருந்தும் முடிவுகள் காண்பிக்கப்படும். முதல் முடிவு எப்போதும் தனிப்படுத்தப்பட்டு மேலே காண்பிக்கப்படும்.</p>
    <p>தனிப்படுத்திக் காண்பிக்கப்படும் முதல் முடிவுக்கு மாற <key>Enter</key> ஐ அழுத்தவும்.</p></item>
    </steps>
    
    <media its:translate="no" type="image" mime="image/svg" src="gs-search2.svg" width="100%"/>
    <steps style="continues">
      <item><p>உங்கள் தேடல் முடிவுகளில் பின்வருபவை சேர்க்கப்பட்டிருக்கலாம்:</p>
      <list>
        <item><p>தேடல் முடிவுகளில் மேல் பகுதியில் காண்பிக்கப்படும், பொருந்தும் பயன்பாடுகள்,</p></item>
        <item><p>பொருந்தும் அமைவுகள்,</p></item>
        <item><p>matching contacts,</p></item>
        <item><p>matching documents,</p></item>
        <item><p>matching calendar,</p></item>
        <item><p>matching calculator,</p></item>
        <item><p>matching software,</p></item>
        <item><p>matching files,</p></item>
        <item><p>matching terminal,</p></item>
        <item><p>matching passwords and keys.</p></item>
      </list>
      </item>
      <item><p>தேடல் முடிவுகளில், ஒரு உருப்படிக்கு மாற அதைச் சொடுக்கவும்.</p>
      <p>மாறாக, அம்புக்குறி விசைகளைப் பயன்படுத்தி ஒரு உருப்படியைத் தனிப்படுத்திக் காண்பித்து, <key>Enter</key> ஐ அழுத்தவும்.</p></item>
    </steps>

    <section id="use-search-inside-applications">
    
      <title>பயன்பாடுகளின் உள்ளிருந்து தேடு</title>
      
      <p>கணினி தேடலானது பல்வேறு பயன்பாடுகளிலிருந்து முடிவுகளை ஒருங்கிணைத்துக் கொண்டு வரும். தேடல் முடிவுகளின் இடப்புறத்தில், தேடல் முடிவுகளை வழங்கிய பயன்பாடுகளின் சின்னங்களை நீங்கள் காணலாம். ஒரு பயன்பாட்டுக்கு உள்ளிருந்து தேடலை மீண்டும் தொடங்க அந்தப் பயன்பாட்டுடன் தொடர்புடைய சின்னத்தைச் சொடுக்கவும். <gui>செயல்பாடுகள் மேலோட்டத்தில்</gui> நன்றாகப் பொருந்துபவை மட்டுமே காண்பிக்கப்படும் என்பதால், பயன்பாடுகளுக்கு உள்ளிருந்து தேடுவது இன்னும் சிறந்த முடிவுகளை வழங்கக்கூடும்.</p>

    </section>

    <section id="use-search-customize">

      <title>தேடல் முடிவுகளைத் தனிப்பயனாக்கு</title>

      <media its:translate="no" type="image" mime="image/svg" src="gs-search-settings.svg" width="100%"/>

      <note style="important">
      <p>Your computer lets you customize what you want to display in the search
       results in the <gui>Activities Overview</gui>. For example, you can
        choose whether you want to show results for websites, photos, or music.
        </p>
      </note>


      <steps>
        <title>தேடல் முடிவுகளில் எவை காண்பிக்கப்பட வேண்டும் எனத் தனிப்பயனாக்கம் செய்ய:</title>
        <item><p>Click the <gui xref="shell-introduction#yourname">system menu</gui>
        on the right side of the top bar.</p></item>
        <item><p>Click <gui>Settings</gui>.</p></item>
        <item><p>Click <gui>Search</gui> in the left panel.</p></item>
        <item><p>In the list of search locations, click the switch next to the
        search location you want to enable or disable.</p></item>
      </steps>

    </section>

</page>
