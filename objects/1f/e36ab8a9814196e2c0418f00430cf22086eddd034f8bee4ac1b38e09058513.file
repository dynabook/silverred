<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="mouse-problem-notmoving" xml:lang="ta">

  <info>
    <link type="guide" xref="mouse#problems"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <!-- TODO: reorganise page and tidy because it's one ugly wall of text -->
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
        <name>ஃபில் புல்</name>
        <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>ஷான் மெக்கேன்ஸ்</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>How to check why your mouse is not working.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

<title>சொடுக்கி சுட்டி நகரவில்லை</title>

<links type="section"/>

<section id="plugged-in">
 <title>சொடுக்கி செருகப்பட்டுள்ளதா எனப் பார்க்கவும்</title>
 <p>உங்கள் சொடுக்கி கேபிளால் இணைக்கப்படுவது எனில், கேபிள் உங்கள் கணினியுடன் உறுதியாக செருகப்பட்டுள்ளதா எனப் பார்க்கவும்.</p>
 <p>அது (ஒரு செவ்வக கனெக்ட்டரைக் கொண்டது) ஒரு USB சொடுக்கி எனில், அதை வேறு ஒரு USB போர்ட்டில் செருகி முயற்சிக்கவும். அலல்து அது PS/2 (ஆறு பின்களைக் கொண்ட ஒரு சிறிய வட்ட வடிவ கனெக்ட்டர் கொண்டது) சொடுக்கி எனில், அது விசைப்பலகைக்கான இளஞ்சிவப்பு போர்ட்டில் இல்லாமல் சொடுக்கிக்கான பச்சை போர்ட்டில் தான் செருகியுள்ளதா என உறுதிப்படுத்தவும். அது செருகப்படாதிருந்தால் கணினியை மறுதொடக்கம் செய்ய வேண்டியிருக்கலாம்</p>
</section>

<section id="broken">
 <title>சொடுக்கி உண்மையில் வேலை செய்கிறதா என்று பாருங்கள்</title>
 <p>சொடுக்கியை வேறு ஒரு கணினியில் இணைத்து அது வேலை செய்கிறதா எனப் பார்க்கவும்.</p>

 <p>ஆப்ட்டிக்கல் அல்லது லேசர் சொடுக்கியாக இருந்தால், சொடுக்கி இயக்கப்படும் போது அதன் கீழே ஒரு ஒளி பிரகாசிக்க வேண்டும். அந்த ஒளி வராவிட்டால், அது இயக்கப்பட்டுள்ளதா என சரிபார்க்கவும். அது இயக்கப்பட்டிருந்தும் ஒளி இல்லாவிட்டால், சொடுக்கி செயலிழந்திருக்கலாம்.</p>
</section>

<section id="wireless-mice">
 <title>வயர்லெஸ் சொடுக்கிகளை சோதித்தல்</title>

  <list>
    <item>
      <p>Make sure the mouse is turned on. There is often a switch on the
      bottom of the mouse to turn the mouse off completely, so you can take it
      with you without it constantly waking up.</p>
    </item>
   <item><p>நீங்கள் ஒரு Bluetooth சொடுக்கியைப் பயன்படுத்தினால், சொடுக்கியை உங்கள் கணினியுடன் இணையாக்கியுள்ளீர்களா எனப் பார்த்துக்கொள்ளவும். <link xref="bluetooth-connect-device"/> ஐப் பார்க்கவும்.</p></item>
  <item>
   <p>ஒரு பொத்தானை சொடுக்கி, இப்போது சொடுக்கி சுட்டி நகர்கிறதா எனப் பார்க்கவும். சில வயர்லெஸ் சொடுக்கிகள் மின்சக்தியை சேமிக்க தூக்க முறைக்குச் சென்றுவிடக்கூடும், இதனால் நீங்கள் ஒரு பொத்தானை சொடுக்காதவரை அது வேலைசெய்யாது. <link xref="mouse-wakeup"/> ஐப் பார்க்கவும்.</p>
  </item>
  <item>
   <p>சொடுக்கியின் பேட்டரியில் சார்ஜ் உள்ளதா என்று பாருங்கள்.</p>
  </item>
  <item>
   <p>ரிசீவர் (டாங்கிள்) கணினியில் உறுதியாக செருகப்பட்டுள்ளதா என்று உறுதிப்படுத்திக்கொள்ளவும்.</p>
  </item>
  <item>
   <p>உங்கள் சொடுக்கி மற்றும் ரிசீவர் இரண்டும் வெவ்வேறு ரேடியோ சேனல்களில் இயங்க முடியும் என்றால், அவை இரண்டும் ஒரே சேனலில் அமைக்கப்பட்டுள்ளதா என்று உறுதிப்படுத்திக்கொள்ளவும்.</p>
  </item>
  <item>
   <p>நீங்கள் ஒரு இணைப்பை உருவாக்க சொடுக்கி, ரிசீவர் அல்லது இரண்டிலும் ஒரு பொத்தானை அழுத்த வேண்டி இருக்கலாம். இப்படி நடக்குமேயானால் உங்கள் சொடுக்கிக்கான அறிவுறுத்தல் கையேட்டில் மேலும் விவரங்கள் இருக்கும்.</p>
  </item>
 </list>

 <p>பெரும்பாலான RF (ரேடியோ) வயர்லெஸ் சொடுக்கிகள், நீங்கள் அவற்றை உங்கள் கணினியில் செருகும் போது தானாகவே வேலை செய்ய வேண்டும். நீங்கள் ஒரு Bluetooth அல்லது IR (அகச்சிவப்பு) வயர்லெஸ் சொடுக்கியைக் கொண்டிருந்தால், அதை வேலை செய்ய வைக்க நீங்கள் கூடுதல் செயல்களைச் செய்ய வேண்டி இருக்கலாம். அச்செயல்கள் உங்கள் சொடுக்கியின் மாடல் அல்லது உற்பத்தி நிறுவனத்திற்கேற்ப மாறலாம்.</p>
</section>

</page>
