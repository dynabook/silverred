<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-workspaces-switch" xml:lang="sr">

  <info>
    <link type="guide" xref="shell-windows#working-with-workspaces"/>
    <link type="seealso" xref="shell-workspaces"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.35.91" date="2020-02-27" status="candidate"/>

    <credit type="author">
      <name>Гномов пројекат документације</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Андре Клапер</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Користите бирача радног простора.</desc>
  </info>

 <title>Пребацујте се између радних простора</title>

 <steps>  
 <title>Користећи миша:</title>
 <item>
  <p if:test="!platform:gnome-classic">Отворите преглед <gui xref="shell-introduction#activities">Активности</gui>.</p>
  <p if:test="platform:gnome-classic">At the bottom right of the screen, click
  on one of the four workspaces to activate the workspace.</p>
 </item>
 <item if:test="!platform:gnome-classic">
  <p>Кликните на радни простор у <link xref="shell-workspaces">бирачу радног простора</link> на десној страни екрана да погледате отворене прозоре на том радном простору.</p>
 </item>
 <item if:test="!platform:gnome-classic">
  <p>Притисните неку сличицу прозора да активирате радни простор.</p>
 </item>
 </steps>
 
 <list>
 <title>Користећи тастатуру:</title>  
  <item>
    <p if:test="!platform:gnome-classic">Притисните <keyseq><key xref="keyboard-key-super">Супер</key><key>Страница горе</key></keyseq> или <keyseq><key>Ктрл</key><key>Алт</key><key>Стрелица на горе</key></keyseq> да се преместите на радни простор који се налази изнад текућег радног простора у бирачу радних простора.</p>
    <p if:test="platform:gnome-classic">Press <keyseq><key>Ctrl</key>
    <key>Alt</key><key>←</key></keyseq> to move to the workspace shown left
    of the current workspace on the <em>workspace selector</em>.</p>
  </item>
  <item>
    <p if:test="!platform:gnome-classic">Притисните<keyseq><key>Супер</key><key>Страница доле</key></keyseq> или <keyseq><key>Ктрл</key><key>Алт</key><key>Стрелица на доле</key></keyseq> да се преместите на радни простор који се налази испод текућег радног простора у бирачу радних простора.</p>
    <p if:test="platform:gnome-classic">Press <keyseq><key>Ctrl</key>
    <key>Alt</key><key>→</key></keyseq> to move to the workspace shown right of
    the current workspace on the <em>workspace selector</em>.</p>
  </item>
 </list>

</page>
