<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-slow" xml:lang="gu">

  <info>
    <link type="guide" xref="net-problem"/>

    <revision pkgversion="3.4.0" date="2012-02-21" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>ફીલ બુલ</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>બીજી વસ્તુઓને ડાઉનલોડ કરી શકાય છે, તમારી પાસે નબળુ જોડાણ હોઇ શકે છે, અથવા તે દિવસનો વ્યસ્ત સમય હોઇ શકે છે.</desc>
  </info>

  <title>ઇન્ટરનેટ ધીમું લાગે છે</title>

  <p>જો તમે ઇન્ટરનેટને વાપરી રહ્યા હોય અને તે ધીમુ હોય તેવુ લાગે છે, ત્યાં ઘણી વસ્તુઓ છે કે જે ધીમુ થવાનું કારણ બની શકે છે.</p>

  <p>તમારાં વેબ બ્રાઉઝરને બંધ કરવાનો પ્રયત્ન કરો અને પછી તેને ફરી ખોલી રહ્યા છે, અને ઇન્ટરનેટમાંથી જોડાણ તોડી રહ્યા છે અને પછી ફરી જોડાઇ રહ્યા છે. (ઘણી વસ્તુઓને ફરી સુયોજિત કરી રહ્યા છે કે જે ઇન્ટરનેટને ધીમુ થવાનું કારણ બની શકે છે.)</p>

  <list>
    <item>
      <p><em style="strong">દિવસનો વ્યસ્ત સમય</em></p>
      <p>Internet service providers commonly setup internet connections so that
      they are shared between several households. Even though you connect
      separately, through your own phone line or cable connection, the
      connection to the rest of the internet at the telephone exchange might
      actually be shared. If this is the case and lots of your neighbors are
      using the internet at the same time as you, you might notice a slow-down.
      You’re most likely to experience this at times when your neighbors are
      probably on the internet (in the evenings, for example).</p>
    </item>
    <item>
      <p><em style="strong">એકવાર ઘણી વસ્તુઓને ડાઉનલોડ કરી રહ્યા છે</em></p>
      <p>If you or someone else using your internet connection are downloading
      several files at once, or watching videos, the internet connection might
      not be fast enough to keep up with the demand. In this case, it will feel
      slower.</p>
    </item>
    <item>
      <p><em style="strong">અવિશ્ર્વાસુ જોડાણ</em></p>
      <p>Some internet connections are just unreliable, especially temporary
      ones or those in high demand areas. If you are in a busy coffee shop or a
      conference center, the internet connection might be too busy or simply
      unreliable.</p>
    </item>
    <item>
      <p><em style="strong">નીચુ વાયરલેસ જોડાણ સંકેત</em></p>
      <p>If you are connected to the internet by wireless (Wi-Fi), check the
      network icon on the top bar to see if you have good wireless signal. If
      not, the internet may be slow because you don’t have a very strong
      signal.</p>
    </item>
    <item>
      <p><em style="strong">ધીમુ મોબાઇલ ઇન્ટરનેટ જોડાણને વાપરી રહ્યા છે</em></p>
      <p>If you have a mobile internet connection and notice that it is slow,
      you may have moved into an area where signal reception is poor. When this
      happens, the internet connection will automatically switch from a fast
      “mobile broadband” connection like 3G to a more reliable, but slower,
      connection like GPRS.</p>
    </item>
    <item>
      <p><em style="strong">વેબબ્રાઉઝરને સમસ્યા છે</em></p>
      <p>Sometimes web browsers encounter a problem that makes them run slow.
      This could be for any number of reasons — you could have visited a
      website that the browser struggled to load, or you might have had the
      browser open for a long time, for example. Try closing all of the
      browser’s windows and then opening the browser again to see if this makes
      a difference.</p>
    </item>
  </list>

</page>
