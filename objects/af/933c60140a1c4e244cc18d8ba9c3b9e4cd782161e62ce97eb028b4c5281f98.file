<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>hdv1394src: GStreamer Good Plugins 1.0 Plugins Reference Manual</title>
<meta name="generator" content="DocBook XSL Stylesheets Vsnapshot">
<link rel="home" href="index.html" title="GStreamer Good Plugins 1.0 Plugins Reference Manual">
<link rel="up" href="ch01.html" title="gst-plugins-good Elements">
<link rel="prev" href="gst-plugins-good-plugins-gtksink.html" title="gtksink">
<link rel="next" href="gst-plugins-good-plugins-icydemux.html" title="icydemux">
<meta name="generator" content="GTK-Doc V1.32 (XML mode)">
<link rel="stylesheet" href="style.css" type="text/css">
</head>
<body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF">
<table class="navigation" id="top" width="100%" summary="Navigation header" cellpadding="2" cellspacing="5"><tr valign="middle">
<td width="100%" align="left" class="shortcuts">
<a href="#" class="shortcut">Top</a><span id="nav_description">  <span class="dim">|</span> 
                  <a href="#gst-plugins-good-plugins-hdv1394src.description" class="shortcut">Description</a></span><span id="nav_hierarchy">  <span class="dim">|</span> 
                  <a href="#gst-plugins-good-plugins-hdv1394src.object-hierarchy" class="shortcut">Object Hierarchy</a></span><span id="nav_interfaces">  <span class="dim">|</span> 
                  <a href="#gst-plugins-good-plugins-hdv1394src.implemented-interfaces" class="shortcut">Implemented Interfaces</a></span><span id="nav_properties">  <span class="dim">|</span> 
                  <a href="#gst-plugins-good-plugins-hdv1394src.properties" class="shortcut">Properties</a></span>
</td>
<td><a accesskey="h" href="index.html"><img src="home.png" width="16" height="16" border="0" alt="Home"></a></td>
<td><a accesskey="u" href="ch01.html"><img src="up.png" width="16" height="16" border="0" alt="Up"></a></td>
<td><a accesskey="p" href="gst-plugins-good-plugins-gtksink.html"><img src="left.png" width="16" height="16" border="0" alt="Prev"></a></td>
<td><a accesskey="n" href="gst-plugins-good-plugins-icydemux.html"><img src="right.png" width="16" height="16" border="0" alt="Next"></a></td>
</tr></table>
<div class="refentry">
<a name="gst-plugins-good-plugins-hdv1394src"></a><div class="titlepage"></div>
<div class="refnamediv"><table width="100%"><tr>
<td valign="top">
<h2><span class="refentrytitle"><a name="gst-plugins-good-plugins-hdv1394src.top_of_page"></a>hdv1394src</span></h2>
<p>hdv1394src</p>
</td>
<td class="gallery_image" valign="top" align="right"></td>
</tr></table></div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-hdv1394src.properties"></a><h2>Properties</h2>
<div class="informaltable"><table class="informaltable" border="0">
<colgroup>
<col width="150px" class="properties_type">
<col width="300px" class="properties_name">
<col width="200px" class="properties_flags">
</colgroup>
<tbody>
<tr>
<td class="property_type"><span class="type">gint</span></td>
<td class="property_name"><a class="link" href="gst-plugins-good-plugins-hdv1394src.html#GstHDV1394Src--channel" title="The “channel” property">channel</a></td>
<td class="property_flags">Read / Write</td>
</tr>
<tr>
<td class="property_type">
<span class="type">gchar</span> *</td>
<td class="property_name"><a class="link" href="gst-plugins-good-plugins-hdv1394src.html#GstHDV1394Src--device-name" title="The “device-name” property">device-name</a></td>
<td class="property_flags">Read</td>
</tr>
<tr>
<td class="property_type"><span class="type">guint64</span></td>
<td class="property_name"><a class="link" href="gst-plugins-good-plugins-hdv1394src.html#GstHDV1394Src--guid" title="The “guid” property">guid</a></td>
<td class="property_flags">Read / Write</td>
</tr>
<tr>
<td class="property_type"><span class="type">gint</span></td>
<td class="property_name"><a class="link" href="gst-plugins-good-plugins-hdv1394src.html#GstHDV1394Src--port" title="The “port” property">port</a></td>
<td class="property_flags">Read / Write</td>
</tr>
<tr>
<td class="property_type"><span class="type">gboolean</span></td>
<td class="property_name"><a class="link" href="gst-plugins-good-plugins-hdv1394src.html#GstHDV1394Src--use-avc" title="The “use-avc” property">use-avc</a></td>
<td class="property_flags">Read / Write</td>
</tr>
</tbody>
</table></div>
</div>
<a name="GstHDV1394Src"></a><div class="refsect1">
<a name="gst-plugins-good-plugins-hdv1394src.other"></a><h2>Types and Values</h2>
<div class="informaltable"><table class="informaltable" width="100%" border="0">
<colgroup>
<col width="150px" class="other_proto_type">
<col class="other_proto_name">
</colgroup>
<tbody><tr>
<td class="datatype_keyword">struct</td>
<td class="function_name"><a class="link" href="gst-plugins-good-plugins-hdv1394src.html#GstHDV1394Src-struct" title="struct GstHDV1394Src">GstHDV1394Src</a></td>
</tr></tbody>
</table></div>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-hdv1394src.object-hierarchy"></a><h2>Object Hierarchy</h2>
<pre class="screen">    GObject
    <span class="lineart">╰──</span> GInitiallyUnowned
        <span class="lineart">╰──</span> GstObject
            <span class="lineart">╰──</span> GstElement
                <span class="lineart">╰──</span> GstBaseSrc
                    <span class="lineart">╰──</span> GstPushSrc
                        <span class="lineart">╰──</span> GstHDV1394Src
</pre>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-hdv1394src.implemented-interfaces"></a><h2>Implemented Interfaces</h2>
<p>
GstHDV1394Src implements
 GstURIHandler and  GstPropertyProbe.</p>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-hdv1394src.description"></a><h2>Description</h2>
<p>Read MPEG-TS data from firewire port.</p>
<div class="refsect2">
<a name="id-1.2.72.8.3"></a><h3>Example launch line</h3>
<div class="informalexample">
  <table class="listing_frame" border="0" cellpadding="0" cellspacing="0">
    <tbody>
      <tr>
        <td class="listing_lines" align="right"><pre>1</pre></td>
        <td class="listing_code"><pre class="programlisting"><span class="n">gst</span><span class="o">-</span><span class="n">launch</span><span class="o">-</span><span class="mf">1.0</span> <span class="n">hdv1394src</span> <span class="o">!</span> <span class="n">queue</span> <span class="o">!</span> <span class="n">decodebin</span> <span class="n">name</span><span class="o">=</span><span class="n">d</span> <span class="o">!</span> <span class="n">queue</span> <span class="o">!</span> <span class="n">xvimagesink</span> <span class="n">d</span><span class="p">.</span> <span class="o">!</span> <span class="n">queue</span> <span class="o">!</span> <span class="n">alsasink</span></pre></td>
      </tr>
    </tbody>
  </table>
</div>
 captures from the firewire port and plays the streams.
<div class="informalexample">
  <table class="listing_frame" border="0" cellpadding="0" cellspacing="0">
    <tbody>
      <tr>
        <td class="listing_lines" align="right"><pre>1</pre></td>
        <td class="listing_code"><pre class="programlisting"><span class="n">gst</span><span class="o">-</span><span class="n">launch</span><span class="o">-</span><span class="mf">1.0</span> <span class="n">hdv1394src</span> <span class="o">!</span> <span class="n">queue</span> <span class="o">!</span> <span class="n">filesink</span> <span class="n">location</span><span class="o">=</span><span class="n">mydump</span><span class="p">.</span><span class="n">ts</span></pre></td>
      </tr>
    </tbody>
  </table>
</div>
 capture to a disk file
</div>
<div class="refsynopsisdiv">
<h2>Synopsis</h2>
<div class="refsect2">
<a name="id-1.2.72.8.4.1"></a><h3>Element Information</h3>
<div class="variablelist"><table border="0" class="variablelist">
<colgroup>
<col align="left" valign="top">
<col>
</colgroup>
<tbody>
<tr>
<td><p><span class="term">plugin</span></p></td>
<td>
            <a class="link" href="gst-plugins-good-plugins-plugin-1394.html#plugin-1394">1394</a>
          </td>
</tr>
<tr>
<td><p><span class="term">author</span></p></td>
<td>Edward Hervey &lt;bilboed@bilboed.com&gt;</td>
</tr>
<tr>
<td><p><span class="term">class</span></p></td>
<td>Source/Video</td>
</tr>
</tbody>
</table></div>
</div>
<hr>
<div class="refsect2">
<a name="id-1.2.72.8.4.2"></a><h3>Element Pads</h3>
<div class="variablelist"><table border="0" class="variablelist">
<colgroup>
<col align="left" valign="top">
<col>
</colgroup>
<tbody>
<tr>
<td><p><span class="term">name</span></p></td>
<td>src</td>
</tr>
<tr>
<td><p><span class="term">direction</span></p></td>
<td>source</td>
</tr>
<tr>
<td><p><span class="term">presence</span></p></td>
<td>always</td>
</tr>
<tr>
<td><p><span class="term">details</span></p></td>
<td>video/mpegts, systemstream=(boolean)true, packetsize=(int)188</td>
</tr>
</tbody>
</table></div>
</div>
</div>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-hdv1394src.functions_details"></a><h2>Functions</h2>
<p></p>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-hdv1394src.other_details"></a><h2>Types and Values</h2>
<div class="refsect2">
<a name="GstHDV1394Src-struct"></a><h3>struct GstHDV1394Src</h3>
<pre class="programlisting">struct GstHDV1394Src;</pre>
</div>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-hdv1394src.property-details"></a><h2>Property Details</h2>
<div class="refsect2">
<a name="GstHDV1394Src--channel"></a><h3>The <code class="literal">“channel”</code> property</h3>
<pre class="programlisting">  “channel”                  <span class="type">gint</span></pre>
<p>Channel number for listening.</p>
<p>Owner: GstHDV1394Src</p>
<p>Flags: Read / Write</p>
<p>Allowed values: [0,64]</p>
<p>Default value: 63</p>
</div>
<hr>
<div class="refsect2">
<a name="GstHDV1394Src--device-name"></a><h3>The <code class="literal">“device-name”</code> property</h3>
<pre class="programlisting">  “device-name”              <span class="type">gchar</span> *</pre>
<p>Descriptive name of the currently opened device</p>
<p>Owner: GstHDV1394Src</p>
<p>Flags: Read</p>
<p>Default value: "Default"</p>
</div>
<hr>
<div class="refsect2">
<a name="GstHDV1394Src--guid"></a><h3>The <code class="literal">“guid”</code> property</h3>
<pre class="programlisting">  “guid”                     <span class="type">guint64</span></pre>
<p>select one of multiple DV devices by its GUID. use a hexadecimal like 0xhhhhhhhhhhhhhhhh. (0 = no guid).</p>
<p>Owner: GstHDV1394Src</p>
<p>Flags: Read / Write</p>
<p>Default value: 0</p>
</div>
<hr>
<div class="refsect2">
<a name="GstHDV1394Src--port"></a><h3>The <code class="literal">“port”</code> property</h3>
<pre class="programlisting">  “port”                     <span class="type">gint</span></pre>
<p>Port number (-1 automatic).</p>
<p>Owner: GstHDV1394Src</p>
<p>Flags: Read / Write</p>
<p>Allowed values: [G_MAXULONG,16]</p>
<p>Default value: -1</p>
</div>
<hr>
<div class="refsect2">
<a name="GstHDV1394Src--use-avc"></a><h3>The <code class="literal">“use-avc”</code> property</h3>
<pre class="programlisting">  “use-avc”                  <span class="type">gboolean</span></pre>
<p>Use AV/C VTR control.</p>
<p>Owner: GstHDV1394Src</p>
<p>Flags: Read / Write</p>
<p>Default value: TRUE</p>
</div>
</div>
</div>
<div class="footer">
<hr>Generated by GTK-Doc V1.32</div>
</body>
</html>