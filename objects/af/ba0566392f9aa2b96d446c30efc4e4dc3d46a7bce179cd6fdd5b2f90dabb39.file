<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mem-check" xml:lang="fr">

  <info>
    <revision pkgversion="3.11" date="2014-01-28" status="candidate"/>
    <link type="guide" xref="index#memory" group="memory"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author copyright">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
      <years>2011</years>
    </credit>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2011, 2014</years>
    </credit>

    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>L'onglet <gui>Ressources</gui> indique la quantité de mémoire (RAM) que l'ordinateur utilise.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>naybnet</mal:name>
      <mal:email>naybnet@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leonor Palazzo</mal:name>
      <mal:email>leonor.palazzo@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Quantité de mémoire utilisée</title>

   <p>Pour connaître l'utilisation actuelle de la mémoire de votre ordinateur :</p>

  <steps>
    <item>
      <p>cliquez sur l'onglet <gui>Ressources</gui>.</p>
    </item>
  </steps>

  <p>L'<gui>Historique d'utilisation de la mémoire physique et du fichier d'échange</gui> montre un graphique affichant de façon continue une ligne pour la mémoire et pour le fichier d'échange, qui sont un pourcentage du total disponible. Ces lignes sont tracées sur une échelle temps dont l'origine est à droite.</p>

  <note style="tip">
    <p>La couleur de chaque ligne est indiquée par les graphes en camembert en-dessous. Cliquez sur le graphe en camembert pour changer la couleur de la ligne du graphique.</p>
  </note>

  <p>Le graphe en camembert <gui>Mémoire</gui> affiche l'utilisation de la mémoire en <link xref="units">Gio</link> et en pourcentage du total disponible.</p>

  <p>Pour modifier la <gui>Fréquence de mise à jour</gui> :</p>

  <steps>
    <item>
      <p>ouvrez <guiseq><gui>Moniteur système</gui><gui>Préférences</gui></guiseq>,</p>
    </item>
    <item>
      <p>cliquez sur l'onglet <gui>Ressources</gui>.</p>
    </item>
    <item>
      <p>saisissez une valeur pour la <gui>Fréquence de mise à jour en secondes :</gui>.</p>
    </item>
  </steps>

<section id="highusage">
  <title>Processus utilisant le plus de mémoire</title>

  <p>Pour savoir quels processus utilisent le plus de mémoire :</p>

  <steps>	
    <item>
      <p>cliquez sur l'onglet <gui>Processus</gui>.</p>
    </item>
    <item>
      <p>Cliquez sur l'en-tête de la colonne <gui>Mémoire</gui> pour trier les processus en fonction de l'utilisation de la mémoire.</p>
      <note>
      	<p>La flèche de l'en-tête de la colonne indique le sens du tri ; cliquez à nouveau pour l'inverser. Si la flèche pointe vers le haut, les processus utilisant le plus de mémoire sont en haut de la liste.</p>
      </note>
    </item>
  </steps>
</section>

</page>
