<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="fs-device" xml:lang="pl">

  <info>
    <revision version="0.1" date="2014-01-26" status="review"/>
    <link type="guide" xref="index#filesystems" group="filesystems"/>
    <link type="seealso" xref="fs-info"/>
    
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    
    <credit type="author copyright">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
      <years>2014</years>
    </credit>

    <desc>Każde urządzenie odpowiada <em>partycji</em> na dysku twardym.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Czym są różne urządzenia w karcie <gui>Systemy plików</gui>?</title>

  <p>Wszystkie urządzenia w karcie <gui>Systemy plików</gui> to dyski do przechowywania danych (takie jak dyski twarde i dyski USB) i partycje dysku. Dla każdego urządzenia wyświetlana jest całkowita pojemność, stopień wykorzystania tej pojemności oraz informacje techniczne o <link xref="fs-info">typie systemu plików</link> i <link xref="fs-info">gdzie jest „zamontowany”</link>.</p>
  
  <p>Miejsce na jednym fizycznym dysku twardym może być podzielone na wiele części, zwanych <em>partycjami</em>, z których każda można być używana tak, jakby była oddzielnym dyskiem. Jeśli dysk twardy został podzielony na partycje (przez użytkownika lub producenta komputera), to każda partycja będzie wyświetlana oddzielnie na liście systemów plików.</p>
  
  <note>
    <p>Można zarządzać dyskami i partycjami oraz wyświetlać o nich więcej informacji za pomocą programu <app>Dyski</app>.</p>
  </note>

</page>
