U
    ��F^�.  �                   @   sz   d Z ddlmZ ddlmZ ddlmZ dd� ZG dd	� d	ej�Zd
d� Z	G dd� d�Z
dd� Zdd� ZG dd� d�ZdS )aW  
Classes representing basic access.

SELinux - at the most basic level - represents access as
the 4-tuple subject (type or context), target (type or context),
object class, permission. The policy language elaborates this basic
access to facilitate more concise rules (e.g., allow rules can have multiple
source or target types - see refpolicy for more information).

This module has objects for representing the most basic access (AccessVector)
and sets of that access (AccessVectorSet). These objects are used in Madison
in a variety of ways, but they are the fundamental representation of access.
�   )�	refpolicy)�util�    )�	audit2whyc                 C   sP   t | �dkrH| d dkrHzt| dd� � W n tk
rB   Y dS X dS dS dS )z�Determine if an id is a parameter in the form $N, where N is
    an integer.

    Returns:
      True if the id is a parameter
      False if the id is not a parameter
    r   r   �$NFT)�len�int�
ValueError)�id� r   �3/usr/lib/python3.8/site-packages/sepolgen/access.py�
is_idparam'   s    r   c                   @   sJ   e Zd ZdZddd�Zdd� Zdd� Zd	d
� Zdd� Zdd� Z	dd� Z
dS )�AccessVectora  
    An access vector is the basic unit of access in SELinux.

    Access vectors are the most basic representation of access within
    SELinux. It represents the access a source type has to a target
    type in terms of an object class and a set of permissions.

    Access vectors are distinct from AVRules in that they can only
    store a single source type, target type, and object class. The
    simplicity of AccessVectors makes them useful for storing access
    in a form that is easy to search and compare.

    The source, target, and object are stored as string. No checking
    done to verify that the strings are valid SELinux identifiers.
    Identifiers in the form $N (where N is an integer) are reserved as
    interface parameters and are treated as wild cards in many
    circumstances.

    Properties:
     .src_type - The source type allowed access. [String or None]
     .tgt_type - The target type to which access is allowed. [String or None]
     .obj_class - The object class to which access is allowed. [String or None]
     .perms - The permissions allowed to the object class. [IdSet]
     .audit_msgs - The audit messages that generated this access vector [List of strings]
     .xperms - Extended permissions attached to the AV. [Dictionary {operation: xperm set}]
    Nc                 C   sV   |r| � |� nd | _d | _d | _t�� | _g | _tj	| _
g | _i | _d | _d | _d S �N)�	from_list�src_type�tgt_type�	obj_classr   �IdSet�perms�
audit_msgsr   �TERULE�type�data�xperms�__hash__Zinfo_flow_dir)�selfZ	init_listr   r   r   �__init__S   s    
zAccessVector.__init__c                 C   sR   t |�dk rtdt|� ��|d | _|d | _|d | _t�|dd� �| _dS )ax  Initialize an access vector from a list.

        Initialize an access vector from a list treating the list as
        positional arguments - i.e., 0 = src_type, 1 = tgt_type, etc.
        All of the list elements 3 and greater are treated as perms.
        For example, the list ['foo_t', 'bar_t', 'file', 'read', 'write']
        would create an access vector list with the source type 'foo_t',
        target type 'bar_t', object class 'file', and permissions 'read'
        and 'write'.

        This format is useful for very simple storage to strings or disc
        (see to_list) and for initializing access vectors.
        �   z+List must contain at least four elements %sr   r   �   �   N)	r   r	   �strr   r   r   r   r   r   )r   �listr   r   r   r   h   s    


zAccessVector.from_listc                 C   s$   | j | j| jg}|�t| j�� |S )z�
        Convert an access vector to a list.

        Convert an access vector to a list treating the list as positional
        values. See from_list for more information on how an access vector
        is represented in a list.
        )r   r   r   �extend�sortedr   )r   �lr   r   r   �to_list}   s    zAccessVector.to_listc                 C   sL   | j �|j � |jD ]2}|| jkr0t�� | j|< | j| �|j| � qdS )z0Add permissions and extended permissions from AVN)r   �updater   r   ZXpermSetr#   )r   �av�opr   r   r   �merge�   s
    

zAccessVector.mergec                 C   s   | � � S r   )�	to_string�r   r   r   r   �__str__�   s    zAccessVector.__str__c                 C   s   d| j | j| j| j�� f S )Nzallow %s %s:%s %s;)r   r   r   r   Zto_space_strr,   r   r   r   r+   �   s    
 �zAccessVector.to_stringc              	   C   sv   zTt | j�}| j| j| j|f}t |j�}|��  |��  |j|j|j|f}|||�W S  ttfk
rp   t Y S X d S r   )	r"   r   r   r   r   �sort�AttributeError�	TypeError�NotImplemented)r   �other�method�x�a�y�br   r   r   �_compare�   s    

zAccessVector._compare)N)�__name__�
__module__�__qualname__�__doc__r   r   r&   r*   r-   r+   r8   r   r   r   r   r   8   s   
	r   c                 C   sj   t | t�r| gS g }| jD ]J}| jD ]>}| jD ]2}t� }||_||_||_| j�	� |_|�
|� q.q$q|S )aR  Convert an avrule into a list of access vectors.

    AccessVectors and AVRules are similarly, but differ in that
    an AVRule can more than one source type, target type, and
    object class. This function expands a single avrule into a
    list of one or more AccessVectors representing the access
    defined in the AVRule.

    
    )�
isinstancer   Z	src_typesZ	tgt_typesZobj_classesr   r   r   r   �copy�append)Zavruler5   r   r   r   �accessr   r   r   �avrule_to_access_vectors�   s    



rA   c                   @   sT   e Zd ZdZdd� Zdd� Zdd� Zdd	� Zd
d� Zde	j
g fdd�Zddd�ZdS )�AccessVectorSeta/  A non-overlapping set of access vectors.

    An AccessVectorSet is designed to store one or more access vectors
    that are non-overlapping. Access can be added to the set
    incrementally and access vectors will be added or merged as
    necessary.  For example, adding the following access vectors using
    add_av:
       allow $1 etc_t : read;
       allow $1 etc_t : write;
       allow $1 var_log_t : read;
    Would result in an access vector set with the access vectors:
       allow $1 etc_t : { read write};
       allow $1 var_log_t : read;
    c                 C   s   i | _ d| _dS )z)Initialize an access vector set.
        N)�srcZinfo_dirr,   r   r   r   r   �   s    zAccessVectorSet.__init__c                 c   s6   | j �� D ]&}|�� D ]}|�� D ]
}|V  q"qq
dS )z9Iterate over all of the unique access vectors in the set.N)rC   �values)r   �tgts�objsr(   r   r   r   �__iter__�   s    zAccessVectorSet.__iter__c                 C   s2   d}| j �� D ]}|�� D ]}|t|�7 }qq|S )a6  Return the number of unique access vectors in the set.

        Because of the internal representation of the access vector set,
        __len__ is not a constant time operation. Worst case is O(N)
        where N is the number of unique access vectors, but the common
        case is probably better.
        r   )rC   rD   r   )r   r%   rE   rF   r   r   r   �__len__�   s
    zAccessVectorSet.__len__c                 C   s    g }| D ]}|� |�� � q|S )ab  Return the unique access vectors in the set as a list.

        The format of the returned list is a set of nested lists,
        each access vector represented by a list. This format is
        designed to be simply  serializable to a file.

        For example, consider an access vector set with the following
        access vectors:
          allow $1 user_t : file read;
          allow $1 etc_t : file { read write};
        to_list would return the following:
          [[$1, user_t, file, read]
           [$1, etc_t, file, read, write]]

        See AccessVector.to_list for more information.
        )r?   r&   �r   r%   r(   r   r   r   r&   �   s    zAccessVectorSet.to_listc                 C   s   |D ]}| � t|�� qdS )a  Add access vectors stored in a list.

        See to list for more information on the list format that this
        method accepts.

        This will add all of the access from the list. Any existing
        access vectors in the set will be retained.
        N)�add_avr   rI   r   r   r   r     s    	zAccessVectorSet.from_listNc           	      C   s:   t � }||_||_||_||_||_||_| �||� dS )z)Add an access vector to the set.
        N)r   r   r   r   r   r   r   rJ   )	r   r   r   r   r   �	audit_msgZavc_typer   r(   r   r   r   �add  s    zAccessVectorSet.addc                 C   sv   | j �|ji �}|�|ji �}|j|jf|krF||j|jf �|� n|||j|jf< |rr||j|jf j�|� dS )z Add an access vector to the set.N)	rC   �
setdefaultr   r   r   r   r*   r   r?   )r   r(   rK   Ztgt�clsr   r   r   rJ     s    zAccessVectorSet.add_av)N)r9   r:   r;   r<   r   rG   rH   r&   r   r   r   rL   rJ   r   r   r   r   rB   �   s   	rB   c                 C   s.   t �� }| D ]}|�|j� |�|j� q|S r   )r   r   rL   r   r   )�avs�typesr(   r   r   r   �avs_extract_types*  s
    rQ   c                 C   sF   i }| D ]8}|j |kr"||j  }nt�� }|||j < |�|j� q|S r   )r   r   r   r'   r   )rO   r   r(   �sr   r   r   �avs_extract_obj_perms2  s    

rS   c                   @   s0   e Zd ZdZdd� Zdd� Zdd� Zdd	� Zd
S )�RoleTypeSetz�A non-overlapping set of role type statements.

    This class allows the incremental addition of role type statements and
    maintains a non-overlapping list of statements.
    c                 C   s
   i | _ dS )z Initialize an access vector set.N)�
role_typesr,   r   r   r   r   C  s    zRoleTypeSet.__init__c                 c   s   | j �� D ]
}|V  q
dS )zAIterate over all of the unique role allows statements in the set.N)rU   rD   )r   �	role_typer   r   r   rG   G  s    zRoleTypeSet.__iter__c                 C   s   t | j�� �S )z2Return the unique number of role allow statements.)r   rU   �keysr,   r   r   r   rH   L  s    zRoleTypeSet.__len__c                 C   s>   || j kr| j | }nt�� }||_|| j |< |j�|� d S r   )rU   r   ZRoleType�rolerP   rL   )r   rX   r   rV   r   r   r   rL   P  s    

zRoleTypeSet.addN)r9   r:   r;   r<   r   rG   rH   rL   r   r   r   r   rT   =  s
   rT   N)r<   � r   r   Zselinuxr   r   Z
Comparisonr   rA   rB   rQ   rS   rT   r   r   r   r   �<module>   s   oj