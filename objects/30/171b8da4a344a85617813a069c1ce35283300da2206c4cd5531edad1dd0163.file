<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-disable-service" xml:lang="id">

  <info>
    <link type="guide" xref="accounts"/>

    <revision pkgversion="3.5.5" date="2012-08-14" status="review"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Beberapa akun daring dapat dipakai untuk mengakses multi layanan (seperti kalender dan surel). Anda dapat mengendalikan mana saja layanan yang dapat dipakai oleh aplikasi.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

  <title>Mengendalikan agar suatu akun dapat dipakai untuk mengakses layanan daring mana</title>

  <p>Beberapa jenis akun daring memungkinkan Anda untuk mengakses beberapa layanan dengan akun pengguna yang sama. Sebagai contoh, akun Google menyediakan akses ke kalender, surel, kontak, dan obrolan. Anda mungkin ingin menggunakan akun Anda untuk beberapa layanan, tetapi tidak yang lain. Sebagai contoh, Anda mungkin ingin menggunakan akun Google Anda untuk surel tapi tidak mengobrol, karena Anda memiliki akun daring yang berbeda yang Anda gunakan untuk mengobrol.</p>

  <p>Anda dapat menonaktifkan beberapa layanan yang disediakan oleh setiap akun daring:</p>

  <steps>
    <item>
      <p>Buka ringkasan <gui xref="shell-introduction#activities">Aktivitas</gui> dan mulai mengetik <gui>Akun Daring</gui>.</p>
    </item>
    <item>
      <p>Klik pada <gui>Akun Daring</gui> untuk membuka panel.</p>
    </item>
    <item>
      <p>Select the account which you want to change from the list on the
      right.</p>
    </item>
    <item>
      <p>Daftar layanan yang tersedia dengan akun ini akan ditunjukkan di bawah <gui>Pakai untuk</gui>. Lihat <link xref="accounts-which-application"/> mengenai aplikasi mana mengakses layanan apa.</p>
    </item>
    <item>
      <p>Matikan sebarang layanan yang tak ingin Anda pakai.</p>
    </item>
  </steps>

  <p>Sekali suatu layanan telah dinonaktifkan bagi suatu akun, aplikasi pada komputer Anda tak akan bisa lagi memakai akun itu untuk menyambung ke layanan tersebut.</p>

  <p>To turn on a service that you disabled, just go back to the <gui>Online
  Accounts</gui> panel and switch it on.</p>

</page>
