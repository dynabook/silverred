<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>jpegdec: GStreamer Good Plugins 1.0 Plugins Reference Manual</title>
<meta name="generator" content="DocBook XSL Stylesheets Vsnapshot">
<link rel="home" href="index.html" title="GStreamer Good Plugins 1.0 Plugins Reference Manual">
<link rel="up" href="ch01.html" title="gst-plugins-good Elements">
<link rel="prev" href="gst-plugins-good-plugins-jackaudiosrc.html" title="jackaudiosrc">
<link rel="next" href="gst-plugins-good-plugins-jpegenc.html" title="jpegenc">
<meta name="generator" content="GTK-Doc V1.32 (XML mode)">
<link rel="stylesheet" href="style.css" type="text/css">
</head>
<body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF">
<table class="navigation" id="top" width="100%" summary="Navigation header" cellpadding="2" cellspacing="5"><tr valign="middle">
<td width="100%" align="left" class="shortcuts">
<a href="#" class="shortcut">Top</a><span id="nav_description">  <span class="dim">|</span> 
                  <a href="#gst-plugins-good-plugins-jpegdec.description" class="shortcut">Description</a></span><span id="nav_hierarchy">  <span class="dim">|</span> 
                  <a href="#gst-plugins-good-plugins-jpegdec.object-hierarchy" class="shortcut">Object Hierarchy</a></span><span id="nav_properties">  <span class="dim">|</span> 
                  <a href="#gst-plugins-good-plugins-jpegdec.properties" class="shortcut">Properties</a></span>
</td>
<td><a accesskey="h" href="index.html"><img src="home.png" width="16" height="16" border="0" alt="Home"></a></td>
<td><a accesskey="u" href="ch01.html"><img src="up.png" width="16" height="16" border="0" alt="Up"></a></td>
<td><a accesskey="p" href="gst-plugins-good-plugins-jackaudiosrc.html"><img src="left.png" width="16" height="16" border="0" alt="Prev"></a></td>
<td><a accesskey="n" href="gst-plugins-good-plugins-jpegenc.html"><img src="right.png" width="16" height="16" border="0" alt="Next"></a></td>
</tr></table>
<div class="refentry">
<a name="gst-plugins-good-plugins-jpegdec"></a><div class="titlepage"></div>
<div class="refnamediv"><table width="100%"><tr>
<td valign="top">
<h2><span class="refentrytitle"><a name="gst-plugins-good-plugins-jpegdec.top_of_page"></a>jpegdec</span></h2>
<p>jpegdec</p>
</td>
<td class="gallery_image" valign="top" align="right"></td>
</tr></table></div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-jpegdec.properties"></a><h2>Properties</h2>
<div class="informaltable"><table class="informaltable" border="0">
<colgroup>
<col width="150px" class="properties_type">
<col width="300px" class="properties_name">
<col width="200px" class="properties_flags">
</colgroup>
<tbody>
<tr>
<td class="property_type"><span class="type">GstIDCTMethod</span></td>
<td class="property_name"><a class="link" href="gst-plugins-good-plugins-jpegdec.html#GstJpegDec--idct-method" title="The “idct-method” property">idct-method</a></td>
<td class="property_flags">Read / Write</td>
</tr>
<tr>
<td class="property_type"><span class="type">gint</span></td>
<td class="property_name"><a class="link" href="gst-plugins-good-plugins-jpegdec.html#GstJpegDec--max-errors" title="The “max-errors” property">max-errors</a></td>
<td class="property_flags">Read / Write</td>
</tr>
</tbody>
</table></div>
</div>
<a name="GstJpegDec"></a><div class="refsect1">
<a name="gst-plugins-good-plugins-jpegdec.other"></a><h2>Types and Values</h2>
<div class="informaltable"><table class="informaltable" width="100%" border="0">
<colgroup>
<col width="150px" class="other_proto_type">
<col class="other_proto_name">
</colgroup>
<tbody><tr>
<td class="datatype_keyword">struct</td>
<td class="function_name"><a class="link" href="gst-plugins-good-plugins-jpegdec.html#GstJpegDec-struct" title="struct GstJpegDec">GstJpegDec</a></td>
</tr></tbody>
</table></div>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-jpegdec.object-hierarchy"></a><h2>Object Hierarchy</h2>
<pre class="screen">    GObject
    <span class="lineart">╰──</span> GInitiallyUnowned
        <span class="lineart">╰──</span> GstObject
            <span class="lineart">╰──</span> GstElement
                <span class="lineart">╰──</span> GstVideoDecoder
                    <span class="lineart">╰──</span> GstJpegDec
</pre>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-jpegdec.description"></a><h2>Description</h2>
<p>Decodes jpeg images.</p>
<div class="refsect2">
<a name="id-1.2.83.7.3"></a><h3>Example launch line</h3>
<div class="informalexample">
  <table class="listing_frame" border="0" cellpadding="0" cellspacing="0">
    <tbody>
      <tr>
        <td class="listing_lines" align="right"><pre>1</pre></td>
        <td class="listing_code"><pre class="programlisting"><span class="n">gst</span><span class="o">-</span><span class="n">launch</span><span class="o">-</span><span class="mf">1.0</span> <span class="o">-</span><span class="n">v</span> <span class="n">filesrc</span> <span class="n">location</span><span class="o">=</span><span class="n">mjpeg</span><span class="p">.</span><span class="n">avi</span> <span class="o">!</span> <span class="n">avidemux</span> <span class="o">!</span>  <span class="n">queue</span> <span class="o">!</span> <span class="n">jpegdec</span> <span class="o">!</span> <span class="n">videoconvert</span> <span class="o">!</span> <span class="n">videoscale</span> <span class="o">!</span> <span class="n">autovideosink</span></pre></td>
      </tr>
    </tbody>
  </table>
</div>
 The above pipeline decode the mjpeg stream and renders it to the screen.
</div>
<div class="refsynopsisdiv">
<h2>Synopsis</h2>
<div class="refsect2">
<a name="id-1.2.83.7.4.1"></a><h3>Element Information</h3>
<div class="variablelist"><table border="0" class="variablelist">
<colgroup>
<col align="left" valign="top">
<col>
</colgroup>
<tbody>
<tr>
<td><p><span class="term">plugin</span></p></td>
<td>
            <a class="link" href="gst-plugins-good-plugins-plugin-jpeg.html#plugin-jpeg">jpeg</a>
          </td>
</tr>
<tr>
<td><p><span class="term">author</span></p></td>
<td>Wim Taymans &lt;wim@fluendo.com&gt;</td>
</tr>
<tr>
<td><p><span class="term">class</span></p></td>
<td>Codec/Decoder/Image</td>
</tr>
</tbody>
</table></div>
</div>
<hr>
<div class="refsect2">
<a name="id-1.2.83.7.4.2"></a><h3>Element Pads</h3>
<div class="variablelist"><table border="0" class="variablelist">
<colgroup>
<col align="left" valign="top">
<col>
</colgroup>
<tbody>
<tr>
<td><p><span class="term">name</span></p></td>
<td>sink</td>
</tr>
<tr>
<td><p><span class="term">direction</span></p></td>
<td>sink</td>
</tr>
<tr>
<td><p><span class="term">presence</span></p></td>
<td>always</td>
</tr>
<tr>
<td><p><span class="term">details</span></p></td>
<td>image/jpeg</td>
</tr>
</tbody>
</table></div>
<div class="variablelist"><table border="0" class="variablelist">
<colgroup>
<col align="left" valign="top">
<col>
</colgroup>
<tbody>
<tr>
<td><p><span class="term">name</span></p></td>
<td>src</td>
</tr>
<tr>
<td><p><span class="term">direction</span></p></td>
<td>source</td>
</tr>
<tr>
<td><p><span class="term">presence</span></p></td>
<td>always</td>
</tr>
<tr>
<td><p><span class="term">details</span></p></td>
<td>video/x-raw, format=(string){ I420, RGB, BGR, RGBx, xRGB, BGRx, xBGR, GRAY8 }, width=(int)[ 1, 2147483647 ], height=(int)[ 1, 2147483647 ], framerate=(fraction)[ 0/1, 2147483647/1 ]</td>
</tr>
</tbody>
</table></div>
</div>
</div>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-jpegdec.functions_details"></a><h2>Functions</h2>
<p></p>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-jpegdec.other_details"></a><h2>Types and Values</h2>
<div class="refsect2">
<a name="GstJpegDec-struct"></a><h3>struct GstJpegDec</h3>
<pre class="programlisting">struct GstJpegDec;</pre>
</div>
</div>
<div class="refsect1">
<a name="gst-plugins-good-plugins-jpegdec.property-details"></a><h2>Property Details</h2>
<div class="refsect2">
<a name="GstJpegDec--idct-method"></a><h3>The <code class="literal">“idct-method”</code> property</h3>
<pre class="programlisting">  “idct-method”              <span class="type">GstIDCTMethod</span></pre>
<p>The IDCT algorithm to use.</p>
<p>Owner: GstJpegDec</p>
<p>Flags: Read / Write</p>
<p>Default value: Faster, less accurate integer method</p>
</div>
<hr>
<div class="refsect2">
<a name="GstJpegDec--max-errors"></a><h3>The <code class="literal">“max-errors”</code> property</h3>
<pre class="programlisting">  “max-errors”               <span class="type">gint</span></pre>
<p>Error out after receiving N consecutive decoding errors
(-1 = never error out, 0 = automatic, 1 = fail on first error, etc.)</p>
<div class="warning">
<p><code class="literal">GstJpegDec:max-errors</code> has been deprecated since version 1.3.1 and should not be used in newly-written code.</p>
<p>Property wasn't used internally</p>
</div>
<p>Owner: GstJpegDec</p>
<p>Flags: Read / Write</p>
<p>Allowed values: &gt;= G_MAXULONG</p>
<p>Default value: 0</p>
</div>
</div>
</div>
<div class="footer">
<hr>Generated by GTK-Doc V1.32</div>
</body>
</html>