<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="net-security-tips" xml:lang="gl">

  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.4.0" date="2012-02-21" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Steven Richards</name>
      <email>steven.richardspc@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Suxestións xerais a ter en conta ao usar Internet.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2020</mal:years>
    </mal:credit>
  </info>

  <title>Manterse seguro na Internet</title>

  <p>A possible reason for why you are using Linux is the robust security that
  it is known for. One reason that Linux is relatively safe from malware and
  viruses is due to the lower number of people who use it. Viruses are targeted
  at popular operating systems, like Windows, that have an extremely large user
  base. Linux is also very secure due to its open source nature, which allows
  experts to modify and enhance the security features included with each
  distribution.</p>

  <p>A pesar de que as medidas levadas a cabo para asegurar a súa instalación de Linux sexan seguras existen vulnerabilidades. Como usuario medio de internet tamén pode ser susceptíbel de:</p>

  <list>
    <item>
      <p>Fraudes de suplantación de identidade (sitios web e correos electrónicos que tentan obter información sensíbel mediante unha suplantación)</p>
    </item>
    <item>
      <p><link xref="net-email-virus">Reenvío de correos maliciosos</link></p>
    </item>
    <item>
      <p><link xref="net-antivirus">Aplicacións con intencións maliciosas (virus)</link></p>
    </item>
    <item>
      <p><link xref="net-wireless-wepwpa">Unauthorized remote/local network
      access</link></p>
    </item>
  </list>

  <p>Para conectarse de maneira segura, recorde as seguintes suxestións:</p>

  <list>
    <item>
      <p>Teña coidado de correos electrónicos, anexos ou ligazóns que lle envíen persoas que non coñece.</p>
    </item>
    <item>
      <p>If a website’s offer is too good to be true, or asks for sensitive
      information that seems unnecessary, then think twice about what
      information you are submitting and the potential consequences if that
      information is compromised by identity thieves or other criminals.</p>
    </item>
    <item>
      <p>Be careful in providing
      <link xref="user-admin-explain">root level permissions</link> to any
      application, especially ones that you have not used before or which are
      not well-known. Providing anyone or anything with root level permissions
      puts your computer at high risk to exploitation.</p>
    </item>
    <item>
      <p>Asegúrese de que só está executando os servizos de acceso remoto necesarios. Tendo SSH ou VNC executándose pode ser útil, pero deixa o seu computador aberto á intrusión se non os asegura de forma correcta. Considere o uso dunha <link xref="net-firewall-on-off">devasa</link> que lle axude ao seu computador nas intrusións.</p>
    </item>
  </list>

</page>
