<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-bouncekeys" xml:lang="ca">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Com ignorar les pulsacions repetides ràpidament d'una mateixa tecla.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

  <title>Activació de les tecles de salt</title>

  <p>Habiliteu les <em>tecles de salt</em> per ignorar les repeticions en prémer ràpidament una mateixa tecla. Si, per exemple, teniu tremolors a les mans que us fan prémer més d'una vegada una tecla que només voleu prémer una única vegada, hauríeu d'activar les tecles de salt.</p>

  <steps>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a teclejar <gui>Paràmetres</gui>.</p>
    </item>
    <item>
      <p>Féu clic a <gui>Paràmetres</gui>.</p>
    </item>
    <item>
      <p>Feu clic a <gui>Accés universal</gui> en la barra lateral per obrir el quadre.</p>
    </item>
    <item>
      <p>Premeu <gui>Assistència d'escriptura (AccessX)</gui> a la secció <gui>Escriptura</gui>.</p>
    </item>
    <item>
      <p>Canvia les <gui>Tecles de salt</gui> a <gui>ON</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Activa i desactiva ràpidament les tecles de rebot</title>
    <p>Podeu activar les tecles de salt si feu clic a la <link xref="a11y-icon">icona d'accessibilitat</link> de la barra superior i trieu <gui>Tecles de salt</gui>. La icona d'accessibilitat només es mostra si s'ha activat alguna opció en el quadre d'<gui>Accés universal</gui>.</p>
  </note>

  <p>Utilitzeu la barra lliscant del <gui>Retard d'acceptació</gui> per modificar el temps que les tecles de salt esperen per registrar una nova pulsació després que hàgiu fet la primera. Seleccioneu <gui>Fes un avís sonor quan es denegui una tecla</gui> si voleu que l'ordinador emeti un so cada vegada que ignora la pulsació d'una tecla ja que aquesta ha succeït massa aviat respecte a la pulsació anterior.</p>

</page>
