<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-display" xml:lang="nl">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-display"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>De door de bestandsbeheerder gebruikte pictogrambijschriften beheren.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

<title>Bestandsbeheer-weergavevoorkeuren</title>

<p>You can control how the file manager displays captions under icons. Click
the menu button in the top-right corner of the window and select
<gui>Preferences</gui>, then select the <gui>Views</gui> tab.</p>

<section id="icon-captions">
  <title>Pictogrambijschriften</title>
  <!-- TODO: update screenshot for 3.18 and above. -->
  <media type="image" src="figures/nautilus-icons.png" width="250" height="110" style="floatend floatright">
    <p>Bestandsbeheerder-pictogrammen met bijschriften</p>
  </media>
  <p>Wanneer u pictogrammenweergave gebruikt, kunt u kiezen voor het weergeven van extra informatie over bestanden en mappen in een bijschrift onder elk pictogram. Dit is bijvoorbeeld nuttig als u vaak wilt zien wie de eigenaar van een bestand is of wanneer het voor het laatst gewijzigd is.</p>
  <p>U kunt op een map inzoomen door in de menubalk te klikken op de weergaveoptiesknop en met de schuifregelaar een zoomniveau te kiezen. Het bestandsbeheer zal tijdens het inzoomen meer en meer informatie in de bijschriften weergeven. U kunt maximaal drie dingen in de bijschriften laten weergeven. Het eerste zal in de meeste zoomniveaus worden weergegeven. Het laatste zal pas bij zeer hoge niveaus worden weergegeven.</p>
  <p>De informatie die u kunt tonen in een pictogrambijschrift is dezelfde als de kolommen die u in de lijstweergave kunt gebruiken. Zie <link xref="nautilus-list"/> voor meer informatie.</p>
</section>

<section id="list-view">

  <title>Lijstweergave</title>

  <p>When viewing files as a list, you can <gui>Allow folders to be
  expanded</gui>. This shows expanders on each directory in the file list, so
  that the contents of several folders can be shown at once. This is useful if
  the folder structure is relevant, such as if your music files are organized
  with a folder per artist, and a subfolder per album.</p>

</section>

</page>
