<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="sound-crackle" xml:lang="hu">

  <info>
    <link type="guide" xref="sound-broken"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>GNOME dokumentációs projekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ellenőrizze a hangkábeleket és a hangkártya illesztőprogramjait.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

<title>Recsegést vagy zúgást hallok hanglejátszáskor</title>

  <p>Ha recsegést vagy zúgást hall hangok lejátszásakor a számítógépen, akkor a probléma a hangkábelekkel, -csatlakozókkal vagy a hangkártya illesztőprogramjaival lehet kapcsolatban.</p>

<list>
 <item>
  <p>Ellenőrizze, hogy a hangszórók megfelelően vannak-e csatlakoztatva.</p>
  <p>Ha a hangszórók nincsenek teljesen bedugva, vagy rossz aljzatba vannak bedugva, akkor zúgó hangot hallhat.</p>
 </item>

 <item>
  <p>Győződjön meg róla, hogy a hangszóró vagy fülhallgató kábele nem sérült.</p>
  <p>A hangkábelek és csatlakozók a használat során elkopnak. Próbálja meg bedugni a kábelt vagy a fülhallgatót másik hangeszközbe (például MP3- vagy CD-lejátszóba), és ellenőrizze, hogy ekkor is hallja-e a recsegő hangot. Ha igen, akkor a kábelt vagy a fülhallgatót ki kell cserélni.</p>
 </item>

 <item>
  <p>Ellenőrizze, hogy a hangkártya illesztőprogramjai megfelelőek-e.</p>
  <p>Egyes hangkártyák azért nem működnek jól Linux alatt, mert nincs hozzájuk jó minőségű illesztőprogram. Ezt a problémát nehezebb azonosítani. Próbálja meg az interneten a hangkártya gyártójára és típusára, valamint a „Linux” kifejezésre keresve kideríteni, hogy mások is tapasztalják-e ezt a problémát.</p>
  <p>A hangkártyával kapcsolatos további információk beszerzésére használhatja az <cmd>lspci</cmd> parancsot.</p>
 </item>
</list>

</page>
