<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="task" id="printing-to-file" xml:lang="fi">

  <info>
    <link type="guide" xref="printing" group="#last"/>

    <revision pkgversion="3.8" date="2013-03-29" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Tallenna asiakirja PDF-, PostScript- tai SVG-tiedostoksi varsinaisen tulostamisen sijaan.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

  <title>Tulosta tiedostoon</title>

  <p>You can choose to print a document to a file instead of sending it to
  print from a printer. Printing to file will create a <sys>PDF</sys>, a
  <sys>PostScript</sys> or a <sys>SVG</sys> file that contains the document.
  This can be useful if you want to transfer the document to another machine
  or to share it with someone.</p>

  <steps>
    <title>Tulosta tiedosto:</title>
    <item>
      <p>Avaa tulostusikkuna painamalla <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>Select <gui>Print to File</gui> under <gui>Printer</gui> in the
      <gui style="tab">General</gui> tab.</p>
    </item>
    <item>
      <p>To change the default filename and where the file is saved to, click
      the filename below the printer selection. Click
      <gui style="button">Select</gui> once you have finished choosing.</p>
    </item>
    <item>
      <p><sys>PDF</sys> is the default file type for the document. If you want
      to use a different <gui>Output format</gui>, select either
      <sys>PostScript</sys> or <sys>SVG</sys>.</p>
    </item>
    <item>
      <p>Choose your other page preferences.</p>
    </item>
    <item>
      <p>Napsauta <gui style="button">Tulosta</gui> tallentaaksesi tiedoston.</p>
    </item>
  </steps>

</page>
