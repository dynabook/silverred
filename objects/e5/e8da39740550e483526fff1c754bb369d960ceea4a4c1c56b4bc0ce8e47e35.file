<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-night-light" xml:lang="pl">
  <info>
    <link type="guide" xref="prefs-display"/>

    <revision pkgversion="3.28" date="2018-07-28" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>
    <revision pkgversion="3.34" date="2019-11-11" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2018</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Nocne światło zmienia kolory ekranu zgodnie z obecną godziną.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  </info>

  <title>Dostosowanie temperatury kolorów ekranu</title>

  <p>Monitor komputera emituje niebieskie światło, które przyczynia się do bezsenności i przemęczenia wzroku po zmroku. <gui>Nocne światło</gui> zmienia kolory ekranu zgodnie z obecną godziną, dzięki czemu są one cieplejsze wieczorem. Aby włączyć <gui>Nocne światło</gui>:</p>

  <steps>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Ekrany</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Ekrany</gui>, aby otworzyć panel.</p>
    </item>
    <item>
      <p>Kliknij <gui>Nocne światło</gui>, aby otworzyć ustawienia.</p>
    </item>
    <item>
      <p>Kliknij <gui>Od zachodu do wschodu słońca</gui>, aby kolory ekranu były zmieniane zgodnie z czasem zachodu i wschodu słońca dla położenia komputera. Kliknij przycisk <gui>Ręcznie</gui>, aby ustawić czas na inny.</p>
    </item>
    <item>
      <p>Użyj suwaka, aby dostosować <gui>Temperaturę kolorów</gui> na bardziej lub mniej ciepłą.</p>
    </item>
  </steps>
      <note>
        <p><link xref="shell-introduction">Górny pasek</link> pokazuje, kiedy <gui>Nocne światło</gui> jest aktywne. Można je tymczasowo wyłączyć w menu systemowym.</p>
      </note>



</page>
