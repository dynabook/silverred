<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-autologin" xml:lang="pt">

  <info>
    <link type="guide" xref="user-accounts#manage"/>
    <link type="seealso" xref="shell-exit"/>

    <revision pkgversion="3.8" date="2013-04-04" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Configurar o início de sessão automático para quando acende a computador.</desc>
  </info>

  <title>Iniciar sessão automaticamente</title>

  <p>Pode mudar a sua configuração para que se inicie a sessão automaticamente com a sua conta quando arranque a computador:</p>

  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Atividades</gui> e comece a escrever <gui>Utentes</gui>.</p>
    </item>
    <item>
      <p>Carregue em <gui>Utentes</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Selecione a conta de utilizador com a que quer iniciar a sessão automaticamente ao início.</p>
    </item>
    <item>
      <p>Carregue <gui style="button">Desbloquear</gui> no canto superior direita e introduza a sua palavra-passe quando se lhe peça.</p>
    </item>
    <item>
      <p>Switch the <gui>Automatic Login</gui> switch to on.</p>
    </item>
  </steps>

  <p>A proxima vez que arranque o seu computador, iniciar-se-á a sessão automaticamente. Se tem esta opção ativada, não será necessário que escreva a sua palavra-passe para iniciar a sessão, o que significa que se alguém arranca o seu computador, poderá aceder a sua conta e a seus dados pessoais, incluindo seus ficheiros e seu histórico de navegação.</p>

  <note>
    <p>Se a sua conta é de tipo <em>Regular</em>, não poderá mudar esta configuração; o administrador do sistema pode mudar isto em seu lugar.</p>
  </note>

</page>
