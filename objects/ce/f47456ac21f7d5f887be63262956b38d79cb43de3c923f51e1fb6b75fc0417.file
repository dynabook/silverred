<?xml version="1.0" encoding="UTF-8"?>
<libosinfo version="0.0.1">

  <os id="http://endlessos.com/eos/3.2">
    <short-id>eos3.2</short-id>
    <name>Endless OS 3.2</name>
    <name xml:lang="fr">Endless OS 3.2</name>
    <name xml:lang="pl">Endless OS 3.2</name>
    <name xml:lang="uk">Endless OS 3.2</name>
    <version>3.2</version>
    <vendor>Endless Mobile, Inc</vendor>
    <vendor xml:lang="ca">Endless Mobile, Inc</vendor>
    <vendor xml:lang="fr">Endless Mobile, Inc</vendor>
    <vendor xml:lang="it">Endless Mobile, Inc</vendor>
    <vendor xml:lang="pl">Endless Mobile, Inc</vendor>
    <vendor xml:lang="uk">Endless Mobile, Inc</vendor>
    <family>linux</family>
    <distro>eos</distro>

    <upgrades id="http://endlessos.com/eos/3.1"/>
    <derives-from id="http://endlessos.com/eos/3.1"/>

    <release-date>2017-07-18</release-date>

    <eol-date>2017-10-24</eol-date>



    <variant id="ar">
      <name>Endless OS Arabic</name>
      <name xml:lang="fr">Endless OS Arabe</name>
      <name xml:lang="it">Endless OS (arabo)</name>
      <name xml:lang="pl">Endless OS (arabski)</name>
      <name xml:lang="uk">Endless OS Arabic</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="ar"/>
      <url>https://images-dl.endlessm.com/release/3.2.6/eos-amd64-amd64/ar/eos-eos3.2-amd64-amd64.170922-043353.ar.iso</url>
      <iso>
        <volume-id>Endless-OS-3-2-\d+-ar$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <variant id="base">
      <name>Endless OS Basic</name>
      <name xml:lang="fr">Endless OS Basique</name>
      <name xml:lang="it">Endless OS Basic</name>
      <name xml:lang="pl">Endless OS Basic</name>
      <name xml:lang="uk">Endless OS Basic</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="base"/>
      <url>https://images-dl.endlessm.com/release/3.2.6/eos-amd64-amd64/base/eos-eos3.2-amd64-amd64.170922-041134.base.iso</url>
      <iso>
        <volume-id>Endless-OS-3-2-\d+-base$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <variant id="bn">
      <name>Endless OS Bangla</name>
      <name xml:lang="fr">Endless OS Bangla</name>
      <name xml:lang="pl">Endless OS (bengalski)</name>
      <name xml:lang="uk">Endless OS Bangla</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="bn"/>
      <url>https://images-dl.endlessm.com/release/3.2.6/eos-amd64-amd64/bn/eos-eos3.2-amd64-amd64.170922-050831.bn.iso</url>
      <iso>
        <volume-id>Endless-OS-3-2-\d+-bn$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <variant id="en">
      <name>Endless OS English</name>
      <name xml:lang="fr">Endless OS Anglais</name>
      <name xml:lang="it">Endless OS (inglese)</name>
      <name xml:lang="pl">Endless OS (angielski)</name>
      <name xml:lang="uk">Endless OS English</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="en"/>
      <url>https://images-dl.endlessm.com/release/3.2.6/eos-amd64-amd64/en/eos-eos3.2-amd64-amd64.170922-055208.en.iso</url>
      <iso>
        <volume-id>Endless-OS-3-2-\d+-en$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <variant id="es">
      <name>Endless OS Spanish</name>
      <name xml:lang="fr">Endless OS Espagnol</name>
      <name xml:lang="it">Endless OS (spagnolo)</name>
      <name xml:lang="pl">Endless OS (hiszpański)</name>
      <name xml:lang="uk">Endless OS Spanish</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="es"/>
      <url>https://images-dl.endlessm.com/release/3.2.6/eos-amd64-amd64/es/eos-eos3.2-amd64-amd64.170922-064601.es.iso</url>
      <iso>
        <volume-id>Endless-OS-3-2-\d+-es$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <variant id="es_GT">
      <name>Endless OS Spanish (Guatemala)</name>
      <name xml:lang="fr">Endless OS Espagnol (Guatemala)</name>
      <name xml:lang="it">Endless OS (spagnolo, Guatemala)</name>
      <name xml:lang="pl">Endless OS (hiszpański, Gwatemala)</name>
      <name xml:lang="uk">Endless OS Spanish (Гватемала)</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="es_GT"/>
      <url>https://images-dl.endlessm.com/release/3.2.6/eos-amd64-amd64/es_GT/eos-eos3.2-amd64-amd64.170922-082829.es_GT.iso</url>
      <iso>
        <volume-id>Endless-OS-3-2-\d+-es_GT$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <variant id="es_MX">
      <name>Endless OS Spanish (Mexico)</name>
      <name xml:lang="fr">Endless OS Espagnol (Mexique)</name>
      <name xml:lang="it">Endless OS (spagnolo, Messico)</name>
      <name xml:lang="pl">Endless OS (hiszpański, Meksyk)</name>
      <name xml:lang="uk">Endless OS Spanish (Мексика)</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="es_MX"/>
      <url>https://images-dl.endlessm.com/release/3.2.6/eos-amd64-amd64/es_MX/eos-eos3.2-amd64-amd64.170922-103111.es_MX.iso</url>
      <iso>
        <volume-id>Endless-OS-3-2-\d+-es_MX$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <variant id="fr">
      <name>Endless OS French</name>
      <name xml:lang="fr">Endless OS Français</name>
      <name xml:lang="it">Endless OS (francese)</name>
      <name xml:lang="pl">Endless OS (francuski)</name>
      <name xml:lang="uk">Endless OS French</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="fr"/>
      <url>https://images-dl.endlessm.com/release/3.2.6/eos-amd64-amd64/fr/eos-eos3.2-amd64-amd64.170922-122905.fr.iso</url>
      <iso>
        <volume-id>Endless-OS-3-2-\d+-fr$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <variant id="id">
      <name>Endless OS Indonesian</name>
      <name xml:lang="fr">Endless OS Indonésien</name>
      <name xml:lang="it">Endless OS (indonesiano)</name>
      <name xml:lang="pl">Endless OS (indonezyjski)</name>
      <name xml:lang="uk">Endless OS Indonesian</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="id"/>
      <url>https://images-dl.endlessm.com/release/3.2.6/eos-amd64-amd64/id/eos-eos3.2-amd64-amd64.170922-141422.id.iso</url>
      <iso>
        <volume-id>Endless-OS-3-2-\d+-id$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <variant id="pt_BR">
      <name>Endless OS Portuguese (Brazil)</name>
      <name xml:lang="fr">Endless OS Portugais (Brésil)</name>
      <name xml:lang="it">Endless OS (portoghese, Brasile)</name>
      <name xml:lang="pl">Endless OS (brazylijski portugalski)</name>
      <name xml:lang="uk">Endless OS Portuguese (Бразилія)</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="pt_BR"/>
      <url>https://images-dl.endlessm.com/release/3.2.6/eos-amd64-amd64/pt_BR/eos-eos3.2-amd64-amd64.170922-155603.pt_BR.iso</url>
      <iso>
        <volume-id>Endless-OS-3-2-\d+-pt_BR$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <variant id="th">
      <name>Endless OS Thai</name>
      <name xml:lang="fr">Endless OS Thaï</name>
      <name xml:lang="it">Endless OS (tailandese)</name>
      <name xml:lang="pl">Endless OS (tajski)</name>
      <name xml:lang="uk">Endless OS Thai</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="th"/>
      <url>https://images-dl.endlessm.com/release/3.2.6/eos-amd64-amd64/th/eos-eos3.2-amd64-amd64.170922-174710.th.iso</url>
      <iso>
        <volume-id>Endless-OS-3-2-\d+-th$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <variant id="vi">
      <name>Endless OS Vietnamese</name>
      <name xml:lang="fr">Endless OS Vietnamien</name>
      <name xml:lang="it">Endless OS (vietnamita)</name>
      <name xml:lang="pl">Endless OS (wietnamski)</name>
      <name xml:lang="uk">Endless OS Vietnamese</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="vi"/>
      <url>https://images-dl.endlessm.com/release/3.2.6/eos-amd64-amd64/vi/eos-eos3.2-amd64-amd64.170922-162331.vi.iso</url>
      <iso>
        <volume-id>Endless-OS-3-2-\d+-vi$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <variant id="zh_CN">
      <name>Endless OS Chinese (China)</name>
      <name xml:lang="fr">Endless OS Chinois (Chine)</name>
      <name xml:lang="it">Endless OS (cinese, Cina)</name>
      <name xml:lang="pl">Endless OS (chiński, Chiny)</name>
      <name xml:lang="uk">Endless OS Chinese (Китай)</name>
    </variant>

    <media live="true" arch="x86_64">
      <variant id="zh_CN"/>
      <url>https://images-dl.endlessm.com/release/3.2.6/eos-amd64-amd64/zh_CN/eos-eos3.2-amd64-amd64.170922-165552.zh_CN.iso</url>
      <iso>
        <volume-id>Endless-OS-3-2-\d+-zh_CN$</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>


    <variant id="other">
      <name>Endless OS (other)</name>
      <name xml:lang="fr">Endless OS (autres)</name>
      <name xml:lang="pl">Endless OS (inne)</name>
      <name xml:lang="uk">Endless OS (інша)</name>
    </variant>

    <media live="true" arch="all">
      <variant id="other"/>
      <iso>
        <volume-id>\D+-3-2-\d+</volume-id>
        <publisher-id>ENDLESS COMPUTERS</publisher-id>
      </iso>
    </media>

    <resources arch="all">
      <minimum>
        <n-cpus>1</n-cpus>
        <cpu>1000000000</cpu>
        <ram>2147483648</ram>
        <storage>21474836480</storage>
      </minimum>

      <recommended>
        <ram>2147483648</ram>
        <storage>42949672960</storage>
      </recommended>
    </resources>

  </os>
</libosinfo>