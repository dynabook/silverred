<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-closelid" xml:lang="nl">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspendfail"/>
    <link type="seealso" xref="power-suspend"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.10" date="2013-11-08" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.26" date="2017-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <credit type="author">
      <name>Gnome-documentatieproject</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Laptops worden in de pauzestand gezet wanneer ze dicht gedaan worden, om energie te sparen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Waarom wordt mijn computer uitgeschakeld wanneer ik hem dichtdoe?</title>

  <p>Wanneer u uw laptop dichtdoet, wordt deze in de <link xref="power-suspend"><em>pauzestand</em></link> gezet om energie te sparen. Dit houdt in dat de computer niet werkelijk wordt uitgeschakeld maar in de pauzestand wordt gezet. U kunt hem weer activeren door hem open te maken. Als dat niet gebeurt, probeer dan een muisklik te geven of op een toets te drukken. Als dat nog niet werkt, druk dan op de aan/uitknop.</p>

  <p>Sommige computers kunnen niet op de juiste manier in de pauzestand worden gezet, meestal omdat hun hardware niet volledig wordt ondersteund door het besturingssysteem (bijv. de Linux stuurprogramma's zijn onvolledig). In dit geval kan het zijn dat u uw laptop niet kunt activeren nadat hij dicht is gemaakt. U kunt <link xref="power-suspendfail">het probleem met de pauzestand oplossen</link> proberen, of u kunt voorkomen dat de computer in de pauzestand wordt gezet wanneer hij dicht gedaan wordt.</p>

<section id="nosuspend">
  <title>Voorkomen dat de computer in de pauzestand wordt gezet wanneer hij dicht wordt gedaan</title>

  <note style="important">
    <p>Deze instructies werken alleen als u <app>systemd</app> gebruikt. Neem contact op met uw distributie voor meer informatie.</p>
  </note>

  <note style="important">
    <p>U dient <app>Afstelhulp (Tweaks)</app> te hebben geïnstalleerd op uw computer om deze instelling te wijzigen.</p>
    <if:if xmlns:if="http://projectmallard.org/if/1.0/" test="action:install">
      <p><link style="button" action="install:gnome-tweak-tool"><app>Afstelhulp</app> installeren</link></p>
    </if:if>
  </note>

  <p>Als u niet wilt dat de computer in de pauzestand wordt gezet wanneer u hem dichtdoet, kunt u de instellingen daarvoor wijzigen:</p>

  <note style="warning">
    <p>Wees zeer voorzichtig als u deze instelling wijzigt. Sommige laptops kunnen oververhit raken als ze actief blijven als de klep gesloten is, vooral als de laptop in een afgesloten plaats zoals een rugzak zit.</p>
  </note>

  <steps>
    <item>
      <p>Open het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht en typ <gui>Afstelhulp</gui>.</p>
    </item>
    <item>
      <p>Klik op <gui>Afstelhulp</gui> om de toepassing te openen.</p>
    </item>
    <item>
      <p>Select the <gui>General</gui> tab.</p>
    </item>
    <item>
      <p>Switch the <gui>Suspend when laptop lid is closed</gui> switch to
      off.</p>
    </item>
    <item>
      <p>Sluit het <gui>Afstelhulp</gui>-venster.</p>
    </item>
  </steps>

</section>

</page>
