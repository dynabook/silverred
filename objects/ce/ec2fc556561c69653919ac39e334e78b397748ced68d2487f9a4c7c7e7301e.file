<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="net-what-is-ip-address" xml:lang="nl">

  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Een IP-adres is als een telefoonnummer voor uw computer.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Wat is een IP-adres?</title>

  <p>‘IP-adres’ staat voor <em>Internet Protocol address</em>, en elk apparaat dat verbonden is met een netwerk (zoals het internet) heeft er een.</p>

  <p>Een IP-adres is net als uw telefoonnummer. Uw telefoonnummer is een unieke reeks cijfers waarmee uw telefoon wordt geïdentificeerd zodat anderen u kunnen bellen. Zo ook is een IP-adres een reeks cijfers waarmee uw computer geïdentificeerd wordt, zodat die data kan versturen naar en ontvangen van andere computers.</p>

  <p>Op dit moment bestaan de meeste IP-adressen uit vier sets getallen, elk gescheiden door een punt. <code>192.168.1.42</code> is een voorbeeld van een IP-adres.</p>

  <note style="tip">
    <p>Een IP-adres kan <em>dynamisch</em> of <em>statisch</em> zijn. Dynamische IP-adressen worden tijdelijk toegekend, elke keer wanneer uw computer verbinding maakt met een netwerk. Statische IP-adressen liggen vast en veranderen niet. Dynamische IP-adressen komen vaker voor dan statische adressen--statische IP-adressen worden eigenlijk alleen gebruikt wanneer er een speciale reden voor is, zoals het beheren van een server.</p>
  </note>

</page>
