<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="task" version="1.0 ui/1.0" id="files-copy" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-15" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>复制或移动项目到一个新文件夹中。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>复制或移动文件和文件夹</title>

 <p>通过鼠标的拖放操作、使用复制粘贴命令，或者使用键盘快捷键，可以将文件或文件夹复制或移动到另一个文件夹。</p>

 <p>For example, you might want to copy a presentation onto a memory stick so
 you can take it to work with you. Or, you could make a back-up copy of a
 document before you make changes to it (and then use the old copy if you don’t
 like your changes).</p>

 <p>这些做法对文件和文件夹都有效，您可以用同样的方式移动和复制文件和文件夹。</p>

<steps ui:expanded="false">
<title>复制和粘贴文件</title>
<item><p>单击选中您想复制的文件。</p></item>
<item><p>右击并然后选择<gui>复制</gui>命令，或者按组合键 <keyseq><key>Ctrl</key><key>C</key></keyseq>。</p></item>
<item><p>再打开您想要存放复制后文件的目录。</p></item>
<item><p>Click the menu button and pick <gui>Paste</gui> to finish copying the
 file, or press <keyseq><key>Ctrl</key><key>V</key></keyseq>. There
 will now be a copy of the file in the original folder and the other
 folder.</p></item>
</steps>

<steps ui:expanded="false">
<title>通过剪切和粘贴操作来移动文件</title>
<item><p>单击选中您想要移动的文件。</p></item>
<item><p>右击并选择<gui>剪切</gui>命令，或者按组合键 <keyseq><key>Ctrl</key><key>X</key></keyseq>。</p></item>
<item><p>再打开您想要存放移动后项目的文件夹。</p></item>
<item><p>Click the menu button in the toolbar and pick <gui>Paste</gui> to
 finish moving the file, or press <keyseq><key>Ctrl</key><key>V</key></keyseq>.
 The file will be taken out of its original folder and moved to the other
 folder.</p></item>
</steps>

<steps ui:expanded="false">
<title>用拖动的方法复制或移动文件</title>
<item><p>打开文件管理器，进入您想要复制项目的文件夹。</p></item>
<item><p>点击顶部面板上的<gui>文件</gui>菜单，选择<gui>新建窗口</gui>(或者按组合键 <keyseq><key>Ctrl</key><key>N</key></keyseq>)，打开第二个窗口。在新窗口中，找到您想要存放复制或移动项目的文件夹。</p></item>
<item>
 <p>单击并将该项目从一个窗口拖动至另一个。默认情况下，如果目的地在<em>同一</em>设备上(即如果两个文件夹都在计算机的同一个分区)，拖动项目将<em>移动</em>该项目。如果目标文件夹在<em>不同</em>的设备分区中，这将会<em>复制它</em></p>
 <p>For example, if you drag a file from a USB memory stick to your Home folder,
 it will be copied, because you’re dragging from one device to another.</p>
 <p>您可以在拖动时按住 <key>Ctrl</key> 键来强制要求复制，或者按住 <key>Shift</key> 键来强制要求移动。</p>
 </item>
</steps>

<note>
  <p>You cannot copy or move a file into a folder that is <em>read-only</em>.
  Some folders are read-only to prevent you from making changes to their
  contents. You can change things from being read-only by
  <link xref="nautilus-file-properties-permissions">changing file permissions
  </link>.</p>
</note>

</page>
