<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-othersedit" xml:lang="ru">

  <info>
    <link type="guide" xref="net-problem"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-31" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Фил Булл (Phil Bull)</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Надо отключить опцию <gui>Доступно всем пользователям</gui> в параметрах сетевого соединения.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Other users can’t edit the network connections</title>

  <p>If you can edit a network connection but other users on your computer
  can’t, you may have set the connection to be <em>available to all users</em>.
  This makes it so that everyone on the computer can <em>connect</em> using
  that connection<!--, but only users
  <link xref="user-admin-explain">with administrative rights</link> are allowed
  to change its settings-->.</p>

<!--
  <p>The reason for this is that, since everyone is affected if the settings
  are changed, only highly-trusted (administrator) users should be allowed to
  modify the connection.</p>

  <p>If other users really need to be able to change the connection themselves,
  make it so the connection is <em>not</em> set to be available to everyone on
  the computer. This way, everyone will be able to manage their own connection
  settings rather than relying on one set of shared, system-wide settings for
  the connection.</p>
-->

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Network</gui>.</p>
    </item>
    <item>
      <p>Нажмите <gui>Сеть</gui> чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
      <p>Выберите <gui>Беспроводное</gui> в списке слева.</p>
    </item>
    <item>
      <p>Click the 
      <media its:translate="no" type="image" src="figures/emblem-system.png"><span its:translate="yes">settings</span></media> button to open the connection
      details.</p>
    </item>
    <item>
      <p>Select <gui>Identity</gui> from the pane on the left.</p>
    </item>
    <item>
      <p>Внизу панели <gui>Подлинность</gui> отметьте параметр <gui>Доступно для других пользователей</gui> чтобы разрешить другим пользователям использовать сетевое соединение.</p>
    </item>
    <item>
      <p>Для сохранения изменений нажмите <gui style="button">Применить</gui>.</p>
    </item>
  </steps>

</page>
