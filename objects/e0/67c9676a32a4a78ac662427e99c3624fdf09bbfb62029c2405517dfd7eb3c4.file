<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:ui="http://projectmallard.org/experimental/ui/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-switch-tasks" version="1.0 if/1.0" xml:lang="eo">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub STEINER</name>
    </credit>
    <credit type="author">
      <name>Petr KOVAR</name>
    </credit>
    <link type="guide" xref="getting-started" group="videos"/>
    <title role="trail" type="link">Ŝanĝi taskojn</title>
    <link type="seealso" xref="shell-windows-switching"/>
    <title role="seealso" type="link">Instruilo pri ŝanĝi taskojn</title>
    <link type="next" xref="gs-use-windows-workspaces"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Carmen Bianca BAKKER</mal:name>
      <mal:email>carmen@carmenbianca.eu</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

  <title>Ŝanĝi taskojn</title>

  <ui:overlay width="812" height="452">
  <media type="video" its:translate="no" src="figures/gnome-task-switching.webm" width="700" height="394">
    <ui:thumb type="image" mime="image/svg" src="gs-thumb-task-switching.svg"/>
      <tt:tt xmlns:tt="http://www.w3.org/ns/ttml" its:translate="yes">
       <tt:body>
         <tt:div begin="1s" end="5s">
           <tt:p>Ŝanĝi taskojn</tt:p>
         </tt:div>
         <tt:div begin="5s" end="8s">
           <tt:p if:test="!platform:gnome-classic">Movu vian musmontrilon al la <gui>Aktivecoj</gui>-angulo ĉe la supra maldekstro de la ekrano.</tt:p>
           </tt:div>
         <tt:div begin="9s" end="12s">
           <tt:p>Alklaku fenestron por ŝanĝi al tiu tasko.</tt:p>
         </tt:div>
         <tt:div begin="12s" end="14s">
           <tt:p>Por maksimumigi fenestron ĉe la maldekstra flanko de la ekrano, kaptu la titolbreton de la fenestro kaj ŝovu ĝin maldekstren.</tt:p>
         </tt:div>
         <tt:div begin="14s" end="16s">
           <tt:p>Kiam duono de la ekrano estas emfazita, malkaptu la fenestron.</tt:p>
         </tt:div>
         <tt:div begin="16s" end="18">
           <tt:p>Por maksimumigi fenestron ĉe la dekstra flanko, kaptu la titolbreton de la fenestro kaj ŝovu ĝin dekstren.</tt:p>
         </tt:div>
         <tt:div begin="18s" end="20s">
           <tt:p>Kiam duono de la ekrano estas emfazita, malkaptu la fenestron.</tt:p>
         </tt:div>
         <tt:div begin="23s" end="27s">
           <tt:p>Premu <keyseq> <key href="help:gnome-help/keyboard-key-super">Super</key><key> Tab</key></keyseq> por montri la <gui>fenestra ŝanĝilo</gui>.</tt:p>
         </tt:div>
         <tt:div begin="27s" end="29s">
           <tt:p>Malkaptu <key href="help:gnome-help/keyboard-key-super">Super </key> por elekti la sekvan emfazitan fenestron.</tt:p>
         </tt:div>
         <tt:div begin="29s" end="32s">
           <tt:p>Por trairi tra la listo de malfermaj fenestroj, ne malkaptu <key href="help:gnome-help/keyboard-key-super">Super</key> sed tenu ĝin, kaj premu <key>Tab</key>.</tt:p>
         </tt:div>
         <tt:div begin="35s" end="37s">
           <tt:p>Premu la <key href="help:gnome-help/keyboard-key-super">Super </key>-klavon por montri la <gui>Aktivecoj-Superrigardon</gui>.</tt:p>
         </tt:div>
         <tt:div begin="37s" end="40s">
           <tt:p>Komencu tajpi la nomon de la aplikaĵo al kiu vi volas ŝanĝi.</tt:p>
         </tt:div>
         <tt:div begin="40s" end="43s">
           <tt:p>Kiam aperas la aplikaĵo kiel la unua rezulto, premu <key>Enter</key> por ŝanĝi al ĝi.</tt:p>
         </tt:div>
       </tt:body>
     </tt:tt>
  </media>
  </ui:overlay>

  <section id="switch-tasks-overview">
    <title>Ŝanĝi taskojn</title>

<if:choose>
<if:when test="!platform:gnome-classic">

  <steps>
    <item><p>Movu vian musmontrilon al la <gui>Aktivecoj</gui>-angulo ĉe la supra maldekstra angulo de la ekrano por montri la <gui>Aktivecoj-Superrigardo</gui>, kie vi povas vidi la aktualajn taskojn kiel etaj fenestroj.</p></item>
     <item><p>Alklaku fenestron por ŝanĝi al tiu tasko.</p></item>
  </steps>

</if:when>
<if:when test="platform:gnome-classic">

  <steps>
    <item><p>Vi povas ŝanĝi inter taskoj uzante la <gui>fenestroliston</gui> ĉe la subo de la ekrano. Malfermaj taskoj aperas kiel butonoj en la <gui>fenestrolisto</gui>.</p></item>
     <item><p>Alklaku butonon en la <gui>fenestrolisto</gui> por ŝanĝi al tiu tasko.</p></item>
  </steps>

</if:when>
</if:choose>

  </section>

  <section id="switching-tasks-tiling">
    <title>Tile windows</title>
    
    <steps>
      <item><p>Por maksimumigi fenestron ĉe flanko de la ekrano, kaptu la titolbreton de la fenestro kaj ŝovu ĝin maldekstren aŭ dekstren.</p></item>
      <item><p>Kiam duono de la ekrano estas emfazita, malkaptu la fenestron por maksimumigi ĝin ĉe tiu flanko de la ekrano.</p></item>
      <item><p>Por maksimumigi du fenestrojn flankalflanke, kaptu la titolbreton de la dua fenestro kaj ŝovu ĝin al la kontraŭa flanko de la ekrano.</p></item>
       <item><p>Kiam duono de la ekrano estas emfazita, malkaptu la fenestron por maksimumigi ĝin ĉe la kontraŭa flanko de la ekrano.</p></item>
    </steps>
    
  </section>
  
  <section id="switch-tasks-windows">
    <title>Ŝanĝi inter fenestroj</title>
    
  <steps>
   <item><p>Premu <keyseq><key href="help:gnome-help/keyboard-key-super">Super </key><key>Tab</key></keyseq> por montri la <gui>fenestran ŝanĝilon</gui>, kiu enhavas la aktualajn malfermajn fenestrojn.</p></item>
   <item><p>Malkaptu <key href="help:gnome-help/keyboard-key-super">Super</key> por elekti la sekvan emfazitan fenestron en la <gui>fenestra ŝanĝilo</gui>.</p>
   </item>
   <item><p>Por trairi tra la listo de malfermaj fenestroj, ne malkaptu <key href="help:gnome-help/keyboard-key-super">Super</key> sed tenu ĝin, kaj premu <key>Tab</key>.</p></item>
  </steps>

  </section>

  <section id="switch-tasks-search">
    <title>Uzu serĉon por ŝanĝi aplikaĵojn</title>
    
    <steps>
      <item><p>Premu la <key href="help:gnome-help/keyboard-key-super">Super</key>-klavon por montri la <gui>Aktivecoj-Superrigardon</gui>.</p></item>
      <item><p>Simple komencu tajpi la nomon de la aplikaĵo al kiu vi volas ŝanĝi. Aplikaĵoj kiuj kongruas tion, kion vi entajpis, aperos dum via tajpado.</p></item>
      <item><p>Kiam la aplikaĵo al kiu vi volas ŝanĝi aperas kiel la unua rezulto, premu <key>Enter</key> por ŝanĝi al ĝi.</p></item>
      
    </steps>
    
  </section>

</page>
