<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-disable-service" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="accounts"/>

    <revision pkgversion="3.5.5" date="2012-08-14" status="review"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Algumas contas on-line podem ser usadas para acessar múltiplos serviços (como agenda e e-mail). Você pode controlar quais desses serviços podem ser usados por aplicativos.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

  <title>Controlando quais serviços on-line uma conta pode ser usada para acessar</title>

  <p>Alguns tipos de conta on-line permitem que você acesse diversos serviços com a mesma conta de usuário. Por exemplo, contas Google fornecem acesso a agenda, e-mail, contatos e bate-papo. Talvez você queira usar sua conta para alguns serviços, mas não para outros. Por exemplo, talvez você queira usar sua conta Google para e-mail, mas não para bate-papo, por ter uma conta online diferente para usar para bate-papo.</p>

  <p>Você pode desabilitar alguns dos serviços que são fornecidos por cada conta on-line:</p>

  <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Contas on-line</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Contas on-line</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Selecione a conta que você deseja alterar na lista à direita.</p>
    </item>
    <item>
      <p>Uma lista de serviços que estão disponíveis para esta conta será mostrada sob <gui>Usar para</gui>. Veja <link xref="accounts-which-application"/> para ver que aplicativos acessam quais serviços.</p>
    </item>
    <item>
      <p>Alterne para off quaisquer serviços que você não deseja usar.</p>
    </item>
  </steps>

  <p>Uma vez que um serviço tenha sido desabilitado para uma conta, aplicativos no seu computador não mais poderão usar a conta para conectar àquele serviço.</p>

  <p>Para habilitar o serviço que você desativou, basta voltar na caixa de diálogo de <gui>Contas on-line</gui> e alterne-o para ON.</p>

</page>
