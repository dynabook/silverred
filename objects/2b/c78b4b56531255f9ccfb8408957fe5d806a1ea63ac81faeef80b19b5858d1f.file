<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="desktop-lockscreen" xml:lang="de">

  <info>
    <link type="guide" xref="appearance"/>
    <link type="seealso" xref="dconf-profiles"/>
    <link type="seealso" xref="dconf-lockdown"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

    <desc>Den Bildschirm automatisch sperren, so dass der Benutzer nach einer Zeit der Untätigkeit ein Passwort eingeben muss.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2017, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

  <title>Den Bildschirm sperren, wenn der Benutzer untätig ist</title>

  <p>Sie können den Bildschirm automatisch sperren, wenn der Benutzer für eine bestimmte Zeitspanne untätig ist. Das ist insbesondere dann sinnvoll, wenn sich der Rechner unbewacht an öffentlichen oder sonstigen sicherheitskritischen Orten befindet.</p>

  <steps>
    <title>Automatische Bildschirmsperre aktivieren</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
    <item>
      <p>Erstellen Sie die Schlüsseldatei <file>/etc/dconf/db/local.d/00-screensaver</file>, um Informationen für die <sys>local</sys>-Datenbank bereitzustellen.</p>
      <listing>
      <title><file>/etc/dconf/db/local.d/00-screensaver</file></title>
<code>
# dconf-Pfad festlegen
[org/gnome/desktop/session]

# Sekunden der Inaktivität, bevor der Bildschirm geleert wird
# Auf 0 Sekunden setzen, wenn Sie den Bildschirmschoner deaktivieren wollen.
idle-delay=uint32 180

# dconf-Pfad festlegen
[org/gnome/desktop/screensaver]

# Sekunden, die der Bildschirm leer sein muss, bevor er gesperrt wird
lock-delay=uint32 0
</code>
      </listing>
      <p>Sie müssen <code>uint32</code> zusammen mit dem ganzzahligen Schlüssel angeben, wie angezeigt.</p>
    </item>
    <item>
      <p>Um den Benutzer daran zu hindern, diese Einstellungen zu überschreiben, erstellen Sie die Datei <file>/etc/dconf/db/local.d/locks/screensaver</file> mit dem folgenden Inhalt:</p>
      <listing>
      <title><file>/etc/dconf/db/local.db/locks/screensaver</file></title>
<code>
# Bildschirmschonereinstellungen sperren
/org/gnome/desktop/session/idle-delay
/org/gnome/desktop/screensaver/lock-delay
</code>
      </listing>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
  </steps>

</page>
