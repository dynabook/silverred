<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-language" xml:lang="gl">

  <info>
    <link type="guide" xref="prefs-language"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Proxecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Cambiar a un idioma diferente a interface de usuario e o texto da axuda.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2020</mal:years>
    </mal:credit>
  </info>

  <title>Cambiar o idioma que usa</title>

  <p>Pode usar o seu escritorio e aplicacións en decenas de idiomas, sempre e cando teña instalados no seu computador os paquetes de idiomas axeitados.</p>

  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Actividades</gui> e comece a escribir <gui>Rexión e idioma</gui>.</p>
    </item>
    <item>
      <p>Prema <gui>Rexión e idioma</gui> para abrir o panel.</p>
    </item>
    <item>
      <p>Prema <gui>Idioma</gui>.</p>
    </item>
    <item>
      <p>Seleccione a rexión e idioma desexados. Se a súa rexión e idioma non se mostran prema <gui><media its:translate="no" type="image" mime="image/svg" src="figures/view-more-symbolic.svg"><span its:translate="yes">…</span></media></gui> na parte inferior da lista para seleccionar todas as rexións e idioma dispoñibeis.</p>
    </item>
    <item>
      <p>Prema <gui style="button">Feito</gui> para gardar.</p>
    </item>
    <item>
      <p>Respond to the prompt, <gui>Your session needs to be restarted for
      changes to take effect</gui> by clicking
      <gui style="button">Restart Now</gui>, or click
      <gui style="button">×</gui> to restart later.</p>
    </item>
  </steps>

  <p>Some translations may be incomplete, and certain applications may not
  support your language at all. Any untranslated text will appear in the
  language in which the software was originally developed, usually American
  English.</p>

  <p>Hai algúns cartafoles especiais no cartafol persoal onde as apliacións poden almacenar cousas como música, imaxes e documentos. Estes cartafoles usan nomes estándar de acordo ao seu idioma. Cando volva a entrar, preguntaráselle se quere cambiar o nome destes cartafoles cos nomes estándar para o idioma seleccionado. Se vai empregar o novo idioma todo o tempo, debe actualizar os nomes dos cartafoles.</p>

  <note style="tip">
    <p>If there are multiple user accounts on your system, there is a separate
    instance of the <gui>Region &amp; Language</gui> panel for the login screen.
    Click the <gui>Login Screen</gui> button at the top right to toggle between
    the two instances.</p>
  </note>

</page>
