<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-change-date-time-timezone" version="1.0" xml:lang="sv">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>Jakub Steiner</name>
    </credit>
    <credit type="author">
      <name>Petr Kovar</name>
    </credit>
    <link type="guide" xref="getting-started" group="tasks"/>
    <title role="trail" type="link">Ändra datum, tid och tidszon</title>
    <link type="seealso" xref="clock"/>
    <title role="seealso" type="link">En handledning i att ändra datum, tid och tidszon</title>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Ändra datum, tid och tidszon</title>

  <media its:translate="no" type="image" mime="image/svg" src="gs-goa1.svg" width="100%"/>

  <steps>
    <item><p>Klicka på <gui xref="shell-introduction#yourname">systemmenyn</gui> på höger sida av systemraden.</p></item>
    <item><p>Klicka på <gui>Inställningar</gui>.</p></item>
  </steps>

  <media its:translate="no" type="image" mime="image/svg" src="gs-datetime.svg" width="100%"/>

  <steps style="continues">
    <item><p>Välj <gui>Datum &amp; tid</gui> från sidopanelen.</p></item>
    <item><p>Säkerställ att menyalternativet <gui>Automatisk tidszon</gui> är avstängt, och klicka sedan nedanför på menyalternativet <gui>Tidszon</gui>.</p></item>
    <item><p>Klicka på din position på världskartan. Detta väljer din aktuella stad, som du också kan söka efter i sökrutan ovanför kartan.</p></item>
    <item><p>Stäng världskartan för att gå tillbaka till panelen <gui>Datum &amp; tid</gui>.</p></item>
    <item><p>Säkerställ att menyalternativet <gui>Automatisk Datum- &amp; tid</gui> är avstängt och klicka sedan på menyalternativet <gui>Datum- &amp; tid</gui> nedanför det öppna fönstret <gui>Datum- &amp; tid</gui>. Där kan du justera dina datum- och tidsinställningar genom att klicka på <gui>+</gui>- eller <gui>-</gui>-knapparna.</p>
     </item>
    <item><p>Stäng fönstret för att gå tillbaka till panelen <gui>Datum &amp; tid</gui>, och stäng sedan panelen.</p></item>
  </steps>

</page>
