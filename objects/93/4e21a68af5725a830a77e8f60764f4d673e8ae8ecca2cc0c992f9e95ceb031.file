<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="session-screenlocks" xml:lang="el">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="guide" xref="hardware-problems-graphics"/>

    <revision pkgversion="3.8" version="0.3" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-03" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.28" date="2018-07-19" status="review"/>
    <revision pkgversion="3.34" date="2019-11-12" status="review"/>

    <credit type="author">
      <name>Έργο Τεκμηρίωσης GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Αλλάξτε τον χρόνο αναμονής πριν να κλειδώσετε την οθόνη στις ρυθμίσεις <gui>Ιδιωτικότητα</gui>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Η οθόνη κλειδώνει υπερβολικά γρήγορα</title>

  <p>Εάν αφήσετε τον υπολογιστή σας για λίγο, η οθόνη θα κλειδώσει τον εαυτόν της αυτόματα, έτσι πρέπει να εισάγετε τον κωδικό πρόσβασής σας για να την ξαναχρησιμοποιήσετε. Αυτό γίνεται για λόγους ασφάλειας (έτσι ώστε κανένας να μην μπορεί να επέμβει στην εργασία σας εάν αφήσετε τον υπολογιστή αφύλακτο), αλλά μπορεί να είναι ενοχλητικό εάν η οθόνη κλειδώνεται μόνη της υπερβολικά γρήγορα.</p>

  <p>Για μεγαλύτερη περίοδο αναμονής πριν να κλειδωθεί η οθόνη αυτόματα:</p>

  <steps>
    <item>
      <p>Ανοίξτε την επισκόπηση <gui xref="shell-introduction#activities">Δραστηριότητες</gui> και αρχίστε να πληκτρολογείτε <gui>Ιδιωτικότητα</gui>.</p>
    </item>

    <item>
      <p>Κάντε κλικ στο <gui>Ιδιωτικότητα</gui> για να ανοίξετε τον πίνακα.</p>
    </item>
    <item>
      <p>Πατήστε στο <gui>Κλείδωμα οθόνης</gui>.</p>
    </item>
    <item>
      <p>Αν το <gui>Αυτόματο κλείδωμα οθόνης</gui> είναι ενεργό, μπορείτε να αλλάξτε την τιμή στην αναπτυσσόμενη λίστα <gui>Κλείδωμα οθόνης μετά από κενό για</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <p>If you don’t ever want the screen to lock itself automatically, switch
    the <gui>Automatic Screen Lock</gui> switch to off.</p>
  </note>

</page>
