<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-wireless-disconnecting" xml:lang="es">

  <info>
    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-problem"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <desc>Es posible que tenga baja señal, o la red podría ser la que no le permite conectar correctamente.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>¿Por qué mi red inalámbrica permanece desconectada?</title>

<p>Es posible que se desconecte de una red inalámbrica a pesar de querer mantenerse en contacto. Su equipo normalmente intentará conectarse de nuevo a la red cuando esto suceda (el icono de red en la barra superior girará o parpadeará si está tratando de volver a conectar), pero puede ser molesto, especialmente si está utilizando Internet en ese momento.</p>

<section id="signal">
 <title>Señal inalámbrica débil</title>

 <p>Una razón común para desconectarse de una red inalámbrica es que tiene señal baja. Las redes inalámbricas tienen un alcance limitado, por lo que si están demasiado lejos de la estación de base inalámbrica, puede que no sea capaz de obtener una señal lo suficientemente fuerte como para mantener una conexión. Las paredes y otros objetos entre usted y la estación base también pueden debilitar la señal.</p>

 <p>El icono de red en la barra superior muestra la cantidad de señal inalámbrica. Si la señal se ve baja, trate de acercarse a la estación base inalámbrica.</p>

</section>

<section id="network">
 <title>La conexión de red no está establecida correctamente</title>

 <p>A veces, cuando se conecta a una red inalámbrica, puede parecer que se ha conectado correctamente al principio, pero luego se desconectará. Esto sucede normalmente porque el equipo obtuvo solo un éxito parcial en la conexión a la red. Gestionó cómo establecer una conexión, pero no pudo finalizar la conexión por alguna razón y la desconectó.</p>

 <p>Una posible razón para esto es que ha introducido la contraseña inalámbrica mal, o que el equipo no tiene permisos en la red (ya que la red requiere un nombre de usuario para iniciar sesión, por ejemplo).</p>

</section>

<section id="hardware">
 <title>Controladores o hardware inalámbrico poco fiables</title>

 <p>Algunos equipos de red inalámbrica pueden ser poco fiables. Las redes inalámbricas son complicadas, por lo que las tarjetas inalámbricas y estaciones base de vez en cuando tienen problemas de menor importancia y pueden perder la conexión. Esto es molesto, pero ocurre con bastante regularidad con muchos dispositivos. Si se desconecta de conexiones inalámbricas de vez en cuando, esta puede ser la única razón. Si esto ocurre con mucha frecuencia, es posible que quiera considerar el uso de hardware diferente.</p>

</section>

<section id="busy">
 <title>Redes inalámbricas ocupadas</title>

 <p>Las redes inalámbricas en lugares concurridos (universidades y cafeterías, por ejemplo) a menudo tienen muchos equipos tratando de conectarse con ellos a la vez. A veces estas redes están demasiado ocupadas y no son capaces de manejar todos los equipos que están tratando de conectar, por lo que algunos de ellos se desconectan.</p>

</section>

</page>
