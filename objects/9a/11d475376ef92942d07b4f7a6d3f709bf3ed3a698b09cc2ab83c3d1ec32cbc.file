<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="reference" id="net-firewall-ports" xml:lang="ca">

  <info>
    <link type="guide" xref="net-security"/>
    <link type="seealso" xref="net-firewall-on-off"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Heu d'especificar el port de xarxa correcte per habilitar / desactivar l'accés a la xarxa d'un programa a través del tallafoc.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

  <title>Ports de xarxa habitualment utilitzats</title>

  <p>Aquesta és una llista de ports de xarxa que utilitzen comunament les aplicacions que proporcionen serveis de xarxa, com ara compartir fitxers o visualitzar l'escriptori remot. Podeu canviar el tallafoc del vostre sistema a <link xref="net-firewall-on-off">bloquejar o permetre l'accés</link> a aquestes aplicacions. Hi ha milers de ports en ús, per tant, aquesta taula no està completa.</p>

  <table shade="rows" frame="top">
    <thead>
      <tr>
	<td>
	  <p>Port</p>
	</td>
	<td>
	  <p>Nom</p>
	</td>
	<td>
	  <p>Descripció</p>
	</td>
      </tr>
    </thead>
    <tbody>
      <tr>
	<td>
	  <p>5353/udp</p>
	</td>
	<td>
	  <p>mDNS, Avahi</p>
	</td>
	<td>
	  <p>Permet que els sistemes es trobin entre ells i descriguin els serveis que ofereixen, sense que s'hagin d'especificar els detalls manualment.</p>
	</td>
      </tr>
      <tr>
	<td>
	  <p>631/udp</p>
	</td>
	<td>
	  <p>Impressió</p>
	</td>
	<td>
	  <p>Permet enviar treballs d'impressió a una impressora a través de la xarxa.</p>
	</td>
      </tr>
      <tr>
	<td>
	  <p>631/tcp</p>
	</td>
	<td>
	  <p>Impressió</p>
	</td>
	<td>
	  <p>Permet compartir la vostra impressora amb altres persones a través de la xarxa.</p>
	</td>
      </tr>
      <tr>
	<td>
	  <p>5298/tcp</p>
	</td>
	<td>
	  <p>Presència</p>
	</td>
	<td>
	  <p>Us permet anunciar el vostre estat de missatgeria instantània a altres persones de la xarxa, com ara «en línia» o «ocupat».</p>
	</td>
      </tr>
      <tr>
	<td>
	  <p>5900/tcp</p>
	</td>
	<td>
	  <p>Escriptori remot</p>
	</td>
	<td>
	  <p>Us permet compartir l'escriptori perquè altres persones puguin veure'l o proporcionar assistència remota.</p>
	</td>
      </tr>
      <tr>
	<td>
	  <p>3689/tcp</p>
	</td>
	<td>
	  <p>Compartir música (DAAP)</p>
	</td>
	<td>
	  <p>Us permet compartir la biblioteca de música amb altres persones de la vostra xarxa.</p>
	</td>
      </tr>
    </tbody>
  </table>

</page>
