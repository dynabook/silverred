<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-mousekeys" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="pointing"/>

    <revision pkgversion="3.8" date="2013-03-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <credit type="author">
      <name>Fil Bul</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Šon Mek Kens</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013, 2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    
    <desc>Uključite tastere miša da upravljate njim koristeći brojevnu tastaturu.</desc>
  </info>

  <title>Click and move the mouse pointer using the keypad</title>

  <p>Ako imate poteškoća prilikom korišćenja miša ili nekog drugog ukazivačkog uređaja, možete da upravljate pokazivačem miša koristeći brojni odeljak na vašoj tastaturi. Ova funkcija se naziva <em>tasteri miša</em>.</p>

  <steps>
    <item>
      <p>Otvorite pregled <gui xref="shell-introduction#activities">Aktivnosti</gui> i počnite da kucate <gui>Univerzalni pristup</gui>.</p>
      <p>You can access the <gui>Activities</gui> overview by pressing on it,
      by moving your mouse pointer against the top-left corner of the screen,
      by using <keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq>
      followed by <key>Enter</key>, or by using the
      <key xref="keyboard-key-super">Super</key> key.</p>
    </item>
    <item>
      <p>Kliknite na <gui>Univerzalni pristup</gui> da otvorite panel.</p>
    </item>
    <item>
      <!-- TODO investigate keyboard navigation, because the arrow keys show no
           visual feedback, and do not seem to work. -->
      <p>Use the up and down arrow keys to select <gui>Mouse Keys</gui> in the
      <gui>Pointing &amp; Clicking</gui> section, then press <key>Enter</key>
      to switch the <gui>Mouse Keys</gui> switch to on.</p>
    </item>
    <item>
      <p>Uverite se da je isključeno <key>Zaključaj broj</key>. Sada ćete moći da pomerate pokazivač miša koristeći tastaturu.</p>
    </item>
  </steps>

  <p>Padiljon tastature je skup brojevnih tastera na vašoj tastaturi, obično poređanih u kvadratnoj mreži. Ako imate tastaturu na kojoj ne postoji padiljon (kao što je tastatura prenosnih računara), moraćete da držite pritisnutim funkcijski taster (<key>Fn</key>) i da koristite određene druge tastere na vašoj tastaturi kao padiljon tastature. Ako ovu mogućnost koristite često na vašem prenosnom računaru, možete da dokupite spoljni USB ili blutut brojevni padiljon.</p>

  <p>Svaki broj na padiljonu odgovara jednom smeru. Na primer, kada pritisnete <key>8</key> pomerićete ga na gore a kada pritisnete <key>2</key> pomerićete ga na dole. Pritisnite taster <key>5</key> da izvršite jedan klik mišem, ili ga brzo pritisnite dva puta da izvršite dvoklik.</p>

  <p>Većina tastatura ima poseban taster koji vam omogućava da izvršite desni klik, koji se ponekad naziva taster <key xref="keyboard-key-menu">Izbornik</key>. Znajte, međutim, da ovaj taster odgovara prvom planu vaše tastature, a ne pokazivača miša. Pogledajte <link xref="a11y-right-click"/> o tome kako da izvršite desni klik držeći pritisnutom <key>5</key> ili levi taster miša.</p>

  <p>Ako želite da koristite padiljon tastature da kucate brojeve dok su uključeni tasteri miša, uključite <key>Zaključaj broj</key>. Ne možete da upravljate mišem koristeći padiljon tastature kada je uključeno <key>Zaključaj broj</key>.</p>

  <note>
    <p>Obični tasteri brojeva, koji se nalaze u jednom redu pri vrhu tastature, neće upravljati pokazivačem miša. Mogu biti korišćeni samo tasteri brojeva na padiljonu tastature.</p>
  </note>

</page>
