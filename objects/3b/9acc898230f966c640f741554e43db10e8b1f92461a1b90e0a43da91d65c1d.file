<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="power-batteryoptimal" xml:lang="ru">

  <info>
    <link type="guide" xref="power"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Tips such as “Do not let the battery charge get too low”.</desc>

    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Фил Булл (Phil Bull)</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Как получить максимум возможного от аккумулятора ноутбука</title>

<p>Когда аккумуляторы ноутбуков стареют, они начинают хуже хранить заряд и их ёмкость значительно уменьшается. Есть несколько способов, позволяющих продлить их время жизни, хотя не стоит ожидать от них большого эффекта.</p>

<list>
  <item>
    <p>Не допускайте полного разряда аккумулятора. Всегда заряжайте аккумулятор <em>до того</em>, как уровень заряда станет слишком низким, хотя большинство аккумуляторов имеют встроенную защиту от полного разряда. Перезарядка при частичном разряде более эффективна, однако выполнять её каждый раз, когда аккумулятор лишь слегка разрядился, более вредно для него.</p>
  </item>
  <item>
    <p>Нагрев оказывает пагубный эффект на эффективность зарядки аккумулятора. Не позволяйте аккумулятору нагреваться больше, чем следует.</p>
  </item>
  <item>
    <p>Batteries age even if you leave them in storage. There is little
    advantage in buying a replacement battery at the same time as you get the
    original battery — always buy replacements when you need them.</p>
  </item>
</list>

<note>
  <p>Эти советы относятся только к литий-ионным (Li-Ion) аккумуляторам — наиболее распространённому типу аккумуляторов. Правила обращения с другими типами аккумуляторов могут отличаться.</p>
</note>

</page>
