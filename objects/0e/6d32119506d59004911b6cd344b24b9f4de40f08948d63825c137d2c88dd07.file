<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-inklevel" xml:lang="nl">

  <info>
    <link type="guide" xref="printing"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Anita Reitere</name>
      <email>nitalynx@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Controleer de hoeveelheid inkt of toner in de printerpatronen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Hoe kan ik het inkt- of tonerniveau van mijn printer controleren?</title>

  <p>Hoe u controleert hoeveel inkt of toner er nog in uw printer zit hangt af van het model en de fabrikant van uw printer, en van de stuurprogramma's en toepassingen die op uw computer zijn geïnstalleerd.</p>

  <p>Sommige printers hebben een ingebouwd scherm om inktniveaus en andere informatie weer te geven.</p>

  <p>Some printers report toner or ink levels to the computer, which can be
  found in the <gui>Printers</gui> panel in <app>Settings</app>. The ink
  level will be shown with the printer details if it is available.</p>

  <p>De hulpmiddelen voor stuurprogramma's en status voor de meeste HP-printers komen van het ‘HP Linux Imaging and Printing (HPLIP)’-project . Het kan zijn dat andere fabrikanten niet-vrije stuurprogramma's met gelijke functies leveren.</p>

  <p>U kunt ook een toepassing installeren om het inktniveau te controleren en  in de gaten houden. <app>Inkblot</app> toont de inktstatus voor vele HP, Epson en Canon-printers. Controleer of uw printer voorkomt in de <link href="http://libinklevel.sourceforge.net./#supported">lijst met ondersteunde modellen</link>. Een andere toepassing voor het inktniveaubeheer van Epson en sommige andere printers is <app>mktink</app>.</p>

  <p>Sommige printers worden nog niet goed ondersteund in Linux, en anderen zijn niet ontworpen om hun inktniveaus weer te geven.</p>

</page>
