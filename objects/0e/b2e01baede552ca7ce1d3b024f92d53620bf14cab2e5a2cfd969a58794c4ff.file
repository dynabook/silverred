<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="touchscreen-gestures" xml:lang="hu">

  <info>
    <link type="guide" xref="mouse" group="#last"/>

    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Manipulate your desktop using gestures on your touchscreen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Use touchscreen gestures to navigate the desktop</title>

  <p>Multitouch gestures can be used on touchscreens for system navigation,
  as well as in applications.</p>

  <p>A number of applications make use of gestures. In
  <app>Document Viewer</app>, documents can be zoomed and swiped with gestures,
  and <app>Image Viewer</app> allows you to zoom, rotate and pan.</p>

<section id="system">
  <title>System-wide gestures</title>

<table rules="rows" frame="bottom">
  <tr xml:id="overview">
    <td><p><media type="image" src="figures/3-finger-pinch.svg" width="256" its:translate="no"/></p></td>
    <td><p><em>Open the Activities Overview</em></p>
    <p>Bring three or more fingers closer together while touching the screen.</p></td>
  </tr>
  <tr xml:id="applist">
    <td><p><media type="image" src="figures/edge-drag-right.svg" width="256" its:translate="no"/></p></td>
    <td><p><em>Open the Applications View</em></p>
    <p>Slide right from the left screen edge.</p></td>
  </tr>
  <tr xml:id="notifications">
    <td><p><media type="image" src="figures/edge-drag-down.svg" width="256" its:translate="no"/></p></td>
    <td><p><em>Bring down the notifications list</em></p>
    <p>Slide down from the top center edge.</p></td>
  </tr>
  <tr xml:id="systemmenu">
    <td><p><media type="image" src="figures/right-edge-drag-down.svg" width="256" its:translate="no"/></p></td>
    <td><p><em>Bring down the system menu</em></p>
    <p>Slide down from the top right edge.</p></td>
  </tr>
  <tr xml:id="osk">
    <td><p><media type="image" src="figures/edge-drag-up.svg" width="256" its:translate="no"/></p></td>
    <td><p><em>Bring up the on-screen keyboard</em></p>
    <p>Slide up from the bottom screen edge.</p></td>
  </tr>
  <tr xml:id="appswitch">
    <td><p><media type="image" src="figures/3-finger-hold-and-tap.svg" width="256" its:translate="no"/></p></td>
    <td><p><em>Switch Application</em></p>
    <p>Hold three fingers on the surface while tapping with the fourth.</p></td>
  </tr>
  <tr xml:id="workspaceswitch">
    <td><p><media type="image" src="figures/4-finger-drag.svg" width="256" its:translate="no"/></p></td>
    <td><p><em>Switch Workspace</em></p>
    <p>Drag up or down with four fingers touching the screen.</p></td>
  </tr>
</table>

</section>

<section id="apps">
  <title>Application gestures</title>

<table rules="rows" frame="bottom">
  <tr xml:id="tap">
    <td><p><media type="image" src="figures/gesture-tap.png" width="96" its:translate="no"/></p></td>
    <td><p><em>Open an item, launch an application, play a song</em></p>
    <p>Tap on an item.</p></td>
  </tr>
  <tr xml:id="hold">
    <td><p><media type="image" src="figures/gesture-hold.png" width="72" its:translate="no"/></p></td>
    <td><p><em>Select an item and list actions that can be performed</em></p>
    <p>Press and hold for a second or two.</p></td>
  </tr>
  <tr xml:id="drag">
    <td><p><media type="image" src="figures/gesture-drag.png" width="128" its:translate="no"/></p></td>
    <td><p><em>Scroll the area on the screen</em></p>
    <p>Drag: slide a finger touching the surface.</p></td>
  </tr>
  <tr xml:id="pinchstretch">
    <td>
      <p><media type="image" src="figures/gesture-pinch-symbolic.svg" width="96" its:translate="no"/>
      <media type="image" src="figures/gesture-stretch-symbolic.svg" width="96" its:translate="no"/></p>
    </td>
    <td><p><em>Change the zoom level of a view (<app>Maps</app>,
    <app>Photos</app>)</em></p>
    <p>Two-finger pinch or stretch: Touch the surface with two fingers while bringing them
    closer or further apart.</p></td>
  </tr>
  <tr xml:id="rotate">
    <td><p><media type="image" src="figures/gesture-rotate-anticlockwise-symbolic.svg" width="96" its:translate="no"/>
    <media type="image" src="figures/gesture-rotate-clockwise-symbolic.svg" width="96" its:translate="no"/></p></td>
    <td><p><em>Rotate a photo</em></p>
    <p>Two-finger rotate: Touch the surface with two fingers and rotate.</p></td>
  </tr>

<!--
  <tr xml:id="doubletap">
    <td><p><media type="image" src="figures/4-finger-drag.svg" width="256" its:translate="no"/></p></td>
    <td><p><em>Tap twice in quick succession</em></p>
    <p>Stepped zoom in.</p></td>
  </tr>
-->
</table>

</section>

</page>
