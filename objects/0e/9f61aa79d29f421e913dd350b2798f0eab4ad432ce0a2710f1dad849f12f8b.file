<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="backup-check" xml:lang="pt">

  <info>
    <link type="guide" xref="files#backup"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Projeto de documentação de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>davidk@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Verifique que a sua copia de copia de segurança seja correta.</desc>
  </info>

  <title>Verifique a sua copia de copia de segurança</title>

  <p>After you have backed up your files, you should make sure that the
 backup was successful. If it didn’t work properly, you could lose important
 data since some files could be missing from the backup.</p>

   <p>Quando usa <app>Ficheiros</app> para copiar ou mover ficheiros, a computador comprova que todos os dados se transferem corretamente. Não obstante, se está a transferir dados importantíssimos, pode querer realizar verificações adicionais para confirmar que seus dados se transferiram corretamente.</p>

  <p>Pode fazer uma verificação adicional olhando através dos ficheiros e diretórios copiadas no médio de destino. Comprovando que os ficheiros e diretórios realmente se transladaram à copia de segurança, pode ter a confiança adicional de que o processo se realizou corretamente.</p>

  <note style="tip"><p>Se observa que costuma fazer com frequência cópias de segurança de grandes quantidades de dados, pode que lhe resulte mais fácil usar um programa especifico de cópias de segurança, como <app>Déjà Dup</app>. Esse programa é mais potente e fiável que simplesmente copiar e colar os ficheiros.</p></note>

</page>
