<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-templates" xml:lang="ru">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Anita Reitere</name>
      <email>nitalynx@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Дэвид Кинг (David King)</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Быстрое создание новых документов из заданных пользователем шаблонов.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Шаблоны для часто используемых типов документов</title>

  <p>Если вы часто создаёте документы с практически одинаковым содержимым, полезно воспользоваться шаблонами файлов. Шаблон может быть документом любого типа с содержимым, которое вы хотите добавлять в другие файлы. Например, можно создать шаблон с заголовком письма.</p>

  <steps>
    <title>Создание шаблона</title>
    <item>
      <p>Создайте документ, который хотите использовать в качестве шаблона. Например, создайте заголовок письма в текстовом редакторе.</p>
    </item>
    <item>
      <p>Save the file with the template content in the <file>Templates</file>
      folder in your <file>Home</file> folder. If the <file>Templates</file>
      folder does not exist, you will need to create it first.</p>
    </item>
  </steps>

  <steps>
    <title>Создание документа с помощью шаблона</title>
    <item>
      <p>Откройте папку, в которую хотите поместить новый документ.</p>
    </item>
    <item>
      <p>Нажмите на любую пустую область внутри окна папки, затем выберите <gui style="menuitem">Создать документ</gui>. Список доступных шаблонов будет показан в подменю.</p>
    </item>
    <item>
      <p>Выберите нужный шаблон из списка.</p>
    </item>
    <item>
      <p>Double-click the file to open it and start editing. You may wish to
      <link xref="files-rename">rename the file</link> when you are
      finished.</p>
    </item>
  </steps>

</page>
