<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-findip" xml:lang="lv">

  <info>
    <link type="guide" xref="net-general"/>
    <link type="seealso" xref="net-what-is-ip-address"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jūsu IP adreses zināšana var palīdzēt jums novērst tīkla problēmas.</desc>
  </info>

  <title>Atrodiet savu IP adresi.</title>

  <p>Jūsu IP adreses zināšana var palīdzēt jums novērst interneta savienojuma problēmas.</p>

  <steps>
    <title>Atrodiet savu iekšējo (tīkla) IP adresi</title>
    <item>
      <p>Atveriet <gui xref="shell-introduction#activities">Aktivitāšu</gui> pārskatu un sāciet rakstīt <gui>Tīkls</gui>.</p>
    </item>
    <item>
      <p>Spiediet <gui>Tīkls</gui>, lai atvērtu paneli.</p>
    </item>
    <item>
      <p>Izvēlieties <gui>Wi-Fi</gui> vai <gui>Vadu</gui> no saraksta kreisajā pusē.</p>
      <p>IP adrese vadu savienojumam tiks parādīta labajā pusē.</p>
      
      <p>Spiediet pogu <media its:translate="no" type="image" src="figures/emblem-system.png"><span its:translate="yes">iestatījumi</span></media>, lai redzētu IP adreses bezvadu tīklam rūtī <gui>Sistēmas informācija</gui>.</p>
    </item>
  </steps>

  <steps>
  	<title>Atrodiet savu ārējo (interneta) IP adresi.</title>
    <item>
      <p>Apmeklējiet <link href="http://whatismyipaddress.com/">whatismyipaddress.com</link>.</p>
    </item>
    <item>
      <p>Lapa jums parādīs jūsu ārējo IP adresi.</p>
    </item>
  </steps>

  <p>Atkarībā no tā, kā dators savienojas ar internetu, abas šīs adreses var būt vienādas.</p>

</page>
