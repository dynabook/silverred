<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-apps-favorites" xml:lang="de">

  <info>
    <link type="seealso" xref="shell-apps-open"/>
    <link type="guide" xref="shell-overview#desktop"/>

    <revision pkgversion="3.6.0" date="2012-10-14" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Fügen Sie Symbole häufig genutzter Programme zum Dash hinzu oder entfernen Sie sie daraus.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2019.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2019.</mal:years>
    </mal:credit>
  </info>

  <title>Ihre Lieblingsanwendungen am Dash anheften</title>

  <p>So fügen Sie eine Anwendung zum <link xref="shell-introduction#activities">Dash</link> hinzu, um leichter darauf zugreifen zu können:</p>

  <steps>
    <item>
      <p if:test="!platform:gnome-classic">Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht, indem Sie auf <gui>Aktivitäten</gui> in der linken oberen Ecke des Bildschirms klicken.</p>
      <p if:test="platform:gnome-classic">Klicken Sie auf das <gui xref="shell-introduction#activities">Anwendungen</gui>-Menü oben links auf dem Bildschirm und wählen Sie <gui>Aktivitäten-Übersicht</gui> aus dem Menü.</p></item>
    <item>
      <p>Klicken Sie auf den Rasterknopf im Dash und suchen Sie die gewünschte Anwendung, die Sie hinzufügen wollen.</p>
    </item>
    <item>
      <p>Klicken Sie mit der rechten Maustaste auf das Symbol und wählen Sie <gui>Zu Favoriten hinzufügen</gui>.</p>
      <p>Alternativ können Sie auf das Symbol mit der Maus klicken und es in das Dash ziehen.</p>
    </item>
  </steps>

  <p>Um ein Anwendungssymbol aus dem Dash zu entfernen, klicken Sie mit der rechten Maustaste auf das Anwendungssymbol und wählen Sie <gui>Aus Favoriten entfernen</gui>.</p>

  <note style="tip" if:test="platform:gnome-classic"><p>Lieblingsanwendungen erscheinen auch im Abschnitt <gui>Favoriten</gui> im Menü <gui xref="shell-introduction#activities">Anwendungen</gui>.</p>
  </note>

</page>
