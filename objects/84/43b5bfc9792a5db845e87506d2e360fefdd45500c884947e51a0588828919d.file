<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-delete" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>
    <link type="seealso" xref="files-recover"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-16" status="review"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="candidate"/>
    <revision pkgversion="3.16" date="2015-02-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Remova arquivos e pastas que não precise mais.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2020.</mal:years>
    </mal:credit>
  </info>

<title>Excluindo arquivos e pastas</title>

  <p>Se você não mais quiser um arquivo ou pasta, você pode excluí-lo(a). Quando você exclui um item, ele é movido para a <gui>Lixeira</gui>, onde ele fica armazenado até que você a esvazie. Você pode <link xref="files-recover">restaurar os itens</link> da <gui>Lixeira</gui> para sua localização original se você resolver que precisa deles, ou eles se foram excluídos acidentalmente.</p>

  <steps>
    <title>Para enviar um arquivo para a lixeira:</title>
    <item><p>Selecione o item que queira excluir clicando nele uma vez.</p></item>
    <item><p>Pressione <key>Delete</key> no seu teclado. Alternativamente, arraste o item para a <gui>Lixeira</gui> no barra lateral.</p></item>
  </steps>

  <p>O arquivo será movido para a lixeira e você terá a opção de <gui>Desfazer</gui> a exclusão. O botão <gui>Desfazer</gui> aparecerá por poucos segundos. Se você selecionar <gui>Desfazer</gui>, o arquivo será restaurado para sua localização original.</p>

  <p>Para excluir os arquivos permanentemente, e liberar espaço em disco do seu computador, você precisa esvaziar a lixeira. Para esvaziar, clique com botão direito na <gui>Lixeira</gui> na barra lateral e selecione <gui>Esvaziar lixeira</gui>.</p>

  <section id="permanent">
    <title>Excluir um arquivo permanentemente</title>
    <p>Você pode excluir imediatamente um arquivo de forma permanente, sem ter que enviá-lo para a lixeira antes.</p>

  <steps>
    <title>Para excluir permanentemente um arquivo:</title>
    <item><p>Selecione o item que queira excluir.</p></item>
    <item><p>Pressione e mantenha a tecla <key>Shift</key>, e então pressione a tecla <key>Delete</key> no seu teclado.</p></item>
    <item><p>Como você não poderá desfazer essa ação, será pedida uma confirmação sobre seu desejo de excluir o arquivo ou a pasta.</p></item>
  </steps>

  <note><p>Os arquivos excluídos em um <link xref="files#removable">dispositivo removível</link> podem não ser visíveis em outros sistemas operacionais, como Windows ou Mac OS. Os arquivos ainda estão lá e estarão disponíveis quando você plugar o dispositivo de volta a seu computador.</p></note>

  </section>

</page>
