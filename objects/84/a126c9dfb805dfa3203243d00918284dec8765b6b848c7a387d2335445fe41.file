<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-lost" xml:lang="ca">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>

    <revision pkgversion="3.8.0" date="2013-04-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Comproveu la vista general d'<gui>Activitats</gui> o altres espais de treball.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020</mal:years>
    </mal:credit>
  </info>

  <title>Trobar una finestra tancada</title>

  <p>Una finestra en un espai de treball diferent, o amagada darrere d'una altra, es troba fàcilment utilitzant la vista general d'<gui xref="shell-introduction#activities">Activitats</gui>:</p>

  <list>
    <item>
      <p>Obriu la vista general d'<gui>Activitats</gui>. Si la finestra que falta és a l'actual <link xref="shell-windows#working-with-workspaces">espai de treball</link>, es mostrarà en miniatura. Simplement feu clic a la miniatura per a tornar-la a mostrar, o bé</p>
    </item>
    <item>
      <p>Feu clic a diferents espais de treball al <link xref="shell-workspaces">selector d'espais de treball </link> al costat dret de la pantalla per intentar trobar la vostra finestra, o bé</p>
    </item>
    <item>
      <p>Feu clic amb el botó dret a l'aplicació al tauler i apareixeran les finestres obertes. Feu clic a la finestra de la llista per a activar-la.</p>
    </item>
  </list>

  <p>Utilitzant el commutador de finestres:</p>

  <list>
    <item>
      <p>Premeu <keyseq><key xref="keyboard-key-super">Súper</key><key>Tab</key></keyseq> per mostrar el <link xref="shell-windows-switching">canviador de finestres</link>. Continueu mantenint premuda la tecla <key>Súper</key> i premeu <key>Tab</key> per saltar cíclicament entre les finestres obertes, o <keyseq><key>Shift</key><key>Tab</key> </keyseq> per fer el cercle enrere.</p>
    </item>
    <item if:test="!platform:gnome-classic">
      <p>Si una aplicació té múltiples finestres obertes, manteniu premuda <key>Súper</key> i premeu <key>`</key> (o la tecla de sobre <key>Tab</key>) per passar a través d'elles.</p>
    </item>
  </list>

</page>
