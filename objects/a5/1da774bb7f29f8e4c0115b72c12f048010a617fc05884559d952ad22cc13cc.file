<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-purge" xml:lang="pl">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.10" date="2013-09-29" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shaun McCance</name>
      <email>mdhillca@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ustawianie, jak często pliki w koszu i pliki tymczasowe są usuwane z komputera.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  </info>

  <title>Usuwanie plików w koszu i plików tymczasowych</title>

  <p>Opróżnianie kosza i usuwanie plików tymczasowych usuwa niechciane i niepotrzebne pliki z komputera, a także zwalnia więcej miejsca na dysku twardym. Można ręcznie opróżnić kosz i usunąć pliki tymczasowe, ale można także ustawić komputer tak, aby automatycznie robił to za użytkownika.</p>

  <p>Pliki tymczasowe są automatycznie tworzone przez programy w tle. Mogą one zwiększać wydajność przez dostarczanie kopii danych, które zostały pobrane lub obliczone.</p>

  <steps>
    <title>Automatyczne opróżnianie kosza i usuwanie plików tymczasowych</title>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Prywatność</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Prywatność</gui>, aby otworzyć panel.</p>
    </item>
    <item>
      <p>Kliknij <gui>Usuwanie plików w koszu i plików tymczasowych</gui>.</p>
    </item>
    <item>
      <p>Przełącz opcję <gui>Automatyczne opróżnianie kosza</gui> lub <gui>Automatyczne usuwanie plików tymczasowych</gui> (albo obie opcje) na <gui>|</gui> (włączone).</p>
    </item>
    <item>
      <p>Ustaw, jak często opróżniać <em>kosz</em> i usuwać <em>pliki tymczasowe</em> zmieniając wartość <gui>Usuwanie po</gui>.</p>
    </item>
    <item>
      <p>Użyj przycisków <gui>Opróżnij kosz</gui> lub <gui>Usuń pliki tymczasowe</gui>, aby wykonać te działania od razu.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Można usuwać pliki natychmiastowo i trwale bez używania kosza. <link xref="files-delete#permanent"/> zawiera więcej informacji.</p>
  </note>

</page>
