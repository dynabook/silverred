<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-stickykeys" xml:lang="hu">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Gyorsbillentyűk bevitele billentyűnként, az összes billentyű egyidejű lenyomása helyett.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Ragadós billentyűk bekapcsolása</title>

  <p>A <em>Ragadós billentyűk</em> lehetővé teszi a gyorsbillentyűk billentyűnként való bevitelét, így nem egyszerre kell lenyomnia azokat. Az <keyseq><key xref="keyboard-key-super">Super</key><key>Tab</key></keyseq> gyorsbillentyű például az ablakok közt vált. A ragadós billentyűk nélkül mindkét billentyűt egyszerre kellene lenyomva tartania, míg a ragadós billentyűk bekapcsolása esetén előbb a <key>Super</key> billentyű, majd a <key>Tab</key> lenyomásával érheti el ugyanazt.</p>

  <p>Akkor kapcsolja be a ragadós billentyűket, ha problémát okoz egyszerre több billentyű lenyomva tartása.</p>

  <steps>
    <item>
      <p>Nyissa meg a <gui xref="shell-introduction#activities">Tevékenységek</gui> áttekintést, és kezdje el begépelni a <gui>Beállítások</gui> szót</p>
    </item>
    <item>
      <p>Válassza a <gui>Beállítások</gui> lehetőséget.</p>
    </item>
    <item>
      <p>Kattintson az <gui>Akadálymentesítés</gui> elemre az oldalsávon a panel megnyitásához.</p>
    </item>
    <item>
      <p>Nyomja meg a <gui>Gépelési segéd</gui> gombot a <gui>Gépelés</gui> szakaszban.</p>
    </item>
    <item>
      <p>Switch the <gui>Sticky Keys</gui> switch to on.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Ragadós billentyűk gyors be- és kikapcsolása</title>
    <p>Az <gui>Engedélyezés billentyűzettel</gui> alatt jelölje be az <gui>Akadálymentesítési szolgáltatások bekapcsolása a billentyűzetről</gui> jelölőnégyzetet a ragadós billentyűk be- és kikapcsolásához a billentyűzetről. Ha ez a négyzet ki van választva, akkor a <key>Shift</key> billentyűt ötször egymás után lenyomva engedélyezheti vagy letilthatja a ragadós billentyűket.</p>
    <p>A ragadós billentyűket be- és kikapcsolhatja a felső sávon az <link xref="a11y-icon">akadálymentesítés ikonra</link> kattintással, majd a <gui>Ragadós billentyűk</gui> menüpont kiválasztásával is. Az akadálymentesítési ikon akkor látható, ha legalább egy funkciót bekapcsolt az <gui>Akadálymentesítés</gui> panelen.</p>
  </note>

  <p>Ha egyszerre két billentyűt nyom le, azzal ideiglenesen kikapcsoltathatja a ragadós billentyűket, és a megszokott módon használhatja a gyorsbillentyűket.</p>

  <p>Ha például a ragadós billentyűk be vannak kapcsolva, de egyszerre megnyomja a <key>Super</key> és <key>Tab</key> billentyűket, akkor a ragadós billentyűk szolgáltatás nem vár újabb billentyű lenyomására. Ha csak egy billentyűt nyomott volna le, akkor <em>várna</em> csak. Ez akkor hasznos, ha néhány gyorsbillentyűt le tud nyomni (például az egymáshoz közeli billentyűkből állókat), de másokat nem.</p>

  <p>Válassza a <gui>Letiltás két billentyű egyidejű lenyomásakor</gui> lehetőséget ennek bekapcsolásához.</p>

  <p>A számítógép beállítható, hogy hangjelzést adjon gyorsbillentyű bevitelének megkezdésekor a ragadós billentyűk használata mellett. Ez akkor hasznos, ha tudni szeretné, hogy a ragadós billentyűk szolgáltatás egy gyorsbillentyű bevitelét várja, így a következő leütés a gyorsbillentyű részeként kerül értelmezésre. Válassza a <gui>Hangjelzés módosító billentyű lenyomásakor</gui> lehetőséget ennek bekapcsolásához.</p>

</page>
