<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="sharing-desktop" xml:lang="nl">

  <info>
    <link type="guide" xref="sharing"/>
    <link type="guide" xref="prefs-sharing"/>

    <revision pkgversion="3.14" date="2015-01-25" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.29" date="2018-08-28" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Laat andere mensen meekijken en uw computer bedienen met behulp van VNC.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Deel uw desktop</title>

  <p>U kunt instellen dat anderen uw bureaublad kunnen bekijken en besturen van een andere computer via een desktopviewer. Stel <gui>Bureaublad delen</gui> zo in dat anderen toegang mogen hebben tot uw bureaublad en beveiligingsvoorkeuren mogen instellen.</p>

  <note style="info package">
    <p>Om <gui>Schermdelen</gui> zichtbaar te krijgen dient het pakket <app>Vino</app> geïnstalleerd te zijn.</p>

    <if:choose xmlns:if="http://projectmallard.org/if/1.0/">
      <if:when test="action:install">
        <p><link action="install:vino" style="button">Vino installeren</link></p>
      </if:when>
    </if:choose>
  </note>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Sharing</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>If the <gui>Sharing</gui> switch at the top-right of the window is set
      to off, switch it to on.</p>

      <note style="info"><p>Als u de tekst onder <gui>Computernaam</gui>kunt bewerken, dan kunt u de naam van uw computer zoals die getoond wordt in het netwerk <link xref="sharing-displayname">wijzigen</link></p></note>
    </item>
    <item>
      <p>Selecteer <gui>Scherm delen</gui>.</p>
    </item>
    <item>
      <p>To let others view your desktop, switch the <gui>Screen Sharing</gui>
      switch to on. This means that other people will be able to attempt to
      connect to your computer and view what’s on your screen.</p>
    </item>
    <item>
      <p>To let others interact with your desktop, ensure that <gui>Allow
      connections to control the screen</gui> is checked. This may allow the
      other person to move your mouse, run applications, and browse files on
      your computer, depending on the security settings which you are currently
      using.</p>
    </item>
  </steps>

  <section id="security">
  <title>Beveiliging</title>

  <p>Het is belangrijk dat u zich bewust bent van wat elke beveiligingsoptie precies betekent voordat u iets wijzigt.</p>

  <terms>
    <item>
      <title>Nieuwe verbindingen moeten toegang vragen</title>
      <p>Als u de mogelijkheid wilt hebben te kiezen of u iemand toegang geeft tot uw bureaublad, selecteer dan <gui>U dient elke toegang tot deze machine te bevestigen</gui>. Als u deze optie uitschakelt, dan wordt u niet gevraagd of u iemand toe wilt staan met uw computer te verbinden.</p>
      <note style="tip">
        <p>Deze optie is standaard ingeschakeld.</p>
      </note>
    </item>
    <item>
      <title>Een wachtwoord vereisen</title>
      <p>Om te vereisen dat andere mensen een wachtwoord invoeren om met uw bureaublad te verbinden, schakel <gui>Een wachtwoord vereisen</gui> in. Wanneer u deze optie niet gebruikt, kan iedereen proberen om uw bureaublad te bekijken.</p>
      <note style="tip">
        <p>Deze optie is standaard uitgeschakeld, maar u kunt hem inschakelen en een veilig wachtwoord instellen.</p>
      </note>
    </item>
<!-- TODO: check whether this option exists.
    <item>
      <title>Allow access to your desktop over the Internet</title>
      <p>If your router supports UPnP Internet Gateway Device Protocol and it
      is enabled, you can allow other people who are not on your local network
      to view your desktop. To allow this, select <gui>Automatically configure
      UPnP router to open and forward ports</gui>. Alternatively, you can
      configure your router manually.</p>
      <note style="tip">
        <p>This option is disabled by default.</p>
      </note>
    </item>
-->
  </terms>
  </section>

  <section id="networks">
  <title>Netwerken</title>

  <p>The <gui>Networks</gui> section lists the networks to which you are
  currently connected. Use the switch next to each to choose where your
  desktop can be shared.</p>
  </section>

  <section id="disconnect">
  <title>Uw bureaublad niet meer delen</title>

  <p>To disconnect someone who is viewing your desktop:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Sharing</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p><gui>Screen Sharing</gui> will show as <gui>Active</gui>. Click on
      it.</p>
    </item>
    <item>
      <p>Toggle the switch at the top to off.</p>
    </item>
  </steps>

  </section>


</page>
