<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="look-resolution" xml:lang="sr">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="seealso" xref="look-display-fuzzy"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.28" date="2018-07-19" status="review"/>
    <revision pkgversion="3.33" date="2019-07-19" status="candidate"/>
    <revision pkgversion="3.34" date="2019-11-12" status="review"/>

    <credit type="author">
      <name>Гномов пројекат документације</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Наталија Руз Лејва</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="editor">
      <name>Мајкл Хил</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Шоба Тјаги</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Измените резолуцију екрана и његово усмерење (ротацију).</desc>
  </info>

  <title>Change the resolution or orientation of the screen</title>

  <p>Можете да измените колико ће велике (или са колико појединости) изгледати ствари на екрану мењајући <em>резолуцију екрана</em>. Можете да измените како ће ствари бити приказане (на пример, ако имате екран који можете да окренете) тако што ћете изменити <em>ротацију</em>.</p>

  <steps>
    <item>
      <p>Отворите преглед <gui xref="shell-introduction#activities">Активности</gui> и почните да куцате <gui>Екрани</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Displays</gui> to open the panel.</p>
    </item>
    <item>
      <p>Ако имате више екрана а нису клонирани, можете имати различита подешавања за сваки екран. Изаберите екран у области за преглед.</p>
    </item>
    <item>
      <p>Select the orientation, resolution or scale, and refresh rate.</p>
    </item>
    <item>
      <p>Кликните <gui>Примени</gui>. Нова подешавања ће бити примењена у року од 20 секунди пре враћања на претходно стање. На тај начин, ако не можете да видите ништа са новим подешавањима, ваша стара подешавања ће бити враћена. Ако сте задовољни новим подешавањима, кликните <gui>Задржи подешавања</gui>.</p>
    </item>
  </steps>

<section id="resolution">
  <title>Резолуција</title>

  <p>Резолуција представља број пиксела (тачака на екрану) који могу бити приказани у сваком правцу. Свака резолуција има <em>однос размере</em>, однос ширине наспрам висине. Широки екрани користе однос размере 16:9, док старији традиционални користе 4:3. Ако изаберете резолуцију која не одговара односу размере вашег екрана, при дну и на врху екрана ће бити приказане црне траке како би се избегло изобличавање.</p>

  <p>Можете да изаберете резолуцију коју желите из падајућег списка <gui>Резолуција</gui>. Ако изаберете неку која не одговара вашем екрану исти може <link xref="look-display-fuzzy">изгледати нејасно или тачкасто</link>.</p>

</section>

<section id="native">
  <title>Native Resolution</title>

  <p>The <em>native resolution</em> of a laptop screen or LCD monitor is the
  one that works best: the pixels in the video signal will line up precisely
  with the pixels on the screen. When the screen is required to show other
  resolutions, interpolation is necessary to represent the pixels, causing a
  loss of image quality.</p>

</section>

<section id="refresh">
  <title>Refresh Rate</title>

  <p>The refresh rate is the number of times per second the screen image is
  drawn, or refreshed.</p>
</section>

<section id="scale">
  <title>Scale</title>

  <p>The scale setting increases the size of objects shown on the screen to
  match the density of your display, making them easier to read. Choose
  <gui>100%</gui> or <gui>200%</gui>.</p>

</section>

<section id="orientation">
  <title>Orientation</title>

  <p>On some devices, you can physically rotate the screen in many directions.
  Click <gui>Orientation</gui> in the panel and choose from
  <gui>Landscape</gui>, <gui>Portrait Right</gui>, <gui>Portrait Left</gui>, or
  <gui>Landscape (flipped)</gui>.</p>

  <note style="tip">
    <p>If your device rotates the screen automatically, you can lock the current
    rotation using the
<media its:translate="no" type="image" src="figures/rotation-locked-symbolic.svg"><span its:translate="yes">rotation lock</span></media> button at the bottom of the <gui xref="shell-introduction#systemmenu">system menu</gui>. To unlock, press the 
<media its:translate="no" type="image" src="figures/rotation-allowed-symbolic.svg"><span its:translate="yes">rotation unlock</span></media> button</p>
  </note>

</section>

</page>
