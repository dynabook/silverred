<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="look-background" xml:lang="pl">

  <info>
    <link type="guide" xref="prefs-display"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-09" status="review"/>
    <revision pkgversion="3.34" date="2019-11-12" status="review"/>

    <credit type="author">
      <name>Projekt dokumentacji GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>April Gonzales</name>
      <email>loonycookie@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ustawianie obrazu jako tło pulpitu i ekranu blokady.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  </info>

  <title>Zmiana tła pulpitu i ekranu blokady</title>

  <p>Aby zmienić obraz używany jako tła:</p>

  <steps>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Tło</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Tło</gui>, aby otworzyć panel. Na górze wyświetlane są obecnie wybrane tło i ekran blokady.</p>
    </item>
    <item>
      <p>Są dwa sposoby na zmianę obrazu używanego jako tła:</p>
      <list>
        <item>
          <p>Kliknij jedno z wielu profesjonalnych teł dostarczanych z GNOME. Można wybrać <gui>Ustaw tło</gui>, <gui>Ustaw ekran blokady</gui> lub <gui>Ustaw tło i ekran blokady</gui>.</p>
        <note style="info">
          <p>Niektóre tapety zmieniają się w ciągu dnia. Ten rodzaj ma małą ikonę zegara w dolnym prawym rogu.</p>
        </note>
        </item>
        <item>
          <p>Kliknij przycisk <gui>Dodaj obraz…</gui>, aby użyć jedno ze swoich zdjęć z katalogu <file>Obrazy</file>. Większość programów do zarządzania zdjęciami przechowuje w nim zdjęcia.</p>
        </item>
      </list>
    </item>
    <item>
      <p>Ustawienia są zastosowywane od razu.</p>
        <note style="tip">
          <p>Aby użyć obrazu spoza katalogu <file>Obrazy</file>, kliknij go prawym przyciskiem myszy w programie <app>Pliki</app> i wybierz <gui>Ustaw jako tapetę</gui>, albo otwórz go w <app>Przeglądarce obrazów</app>, kliknij przycisk menu na pasku tytułowym i wybierz <gui>Ustaw jako tapetę</gui>. Ustawi to tylko tło pulpitu.</p>
        </note>
    </item>
    <item>
      <p><link xref="shell-workspaces-switch">Przełącz na pusty obszar roboczy</link>, aby wyświetlić cały pulpit.</p>
    </item>
  </steps>

</page>
