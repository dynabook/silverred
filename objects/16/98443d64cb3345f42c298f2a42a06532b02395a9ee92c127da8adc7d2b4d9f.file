<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="gs-use-system-search" xml:lang="te">

  <info>
    <include xmlns="http://www.w3.org/2001/XInclude" href="gs-legal.xml"/>
    <credit type="author">
      <name>జాకబ్ స్టైనర్</name>
    </credit>
    <credit type="author">
      <name>పెత్ర్ కోవర్</name>
    </credit>
    <credit type="author">
      <name>Hannie Dumoleyn</name>
    </credit>
    <link type="guide" xref="getting-started" group="tasks"/>
    <title role="trail" type="link">వ్యవస్థ అన్వేషణను వుపయోగించుము</title>
    <link type="seealso" xref="shell-apps-open"/>
    <title role="seealso" type="link">వ్యవస్థ అన్వేషణను వుపయోగించుటపై వొక ట్యుటోరియల్</title>
    <link type="next" xref="gs-get-online"/>
  </info>

  <title>వ్యవస్థ అన్వేషణను వుపయోగించు</title>

    <media its:translate="no" type="image" mime="image/svg" src="gs-search1.svg" width="100%"/>
    
    <steps>
    <item><p>Open the <gui>Activities</gui> overview by clicking <gui>Activities</gui> 
    at the top left of the screen, or by pressing the
    <key href="help:gnome-help/keyboard-key-super">Super</key> key. 
    Start typing to search.</p>
    <p>మీరు టైపు చేసిన దానితో సరిపోయిన ఫలితాలు మీరు టైపు చేయగానే కనపడును. ఎల్లప్పుడూ మొదటి ఫలితం ఉద్దీపించబడి అన్నిటికంటే పైన చూపబడును.</p>
    <p>మొదట ఉద్దీపనం చేయబడిన ఫలితంకు మారుటకు <key>Enter</key> వత్తుము.</p></item>
    </steps>
    
    <media its:translate="no" type="image" mime="image/svg" src="gs-search2.svg" width="100%"/>
    <steps style="continues">
      <item><p>అన్వేషణ ఫలితాల నందు కనిపించు అంశాలు వీటిని కలిగివుండవచ్చు:</p>
      <list>
        <item><p>సరిపోలిన అనువర్తనాలు, అన్వేషణ ఫలితాల పైన చూపబడును,</p></item>
        <item><p>సరిపోలిన అమరికలు,</p></item>
        <item><p>matching contacts,</p></item>
        <item><p>matching documents,</p></item>
        <item><p>matching calendar,</p></item>
        <item><p>matching calculator,</p></item>
        <item><p>matching software,</p></item>
        <item><p>matching files,</p></item>
        <item><p>matching terminal,</p></item>
        <item><p>matching passwords and keys.</p></item>
      </list>
      </item>
      <item><p>అన్వేషణ ఫలితాల నందు, అంశమునకు మారుటకు దానిని నొక్కండి.</p>
      <p>ప్రత్యామ్నాయంగా, అంశమును బాణపు గుర్తులను వుపయోగించి ఉద్దీపనం చేసి <key>Enter</key> వత్తండి.</p></item>
    </steps>

    <section id="use-search-inside-applications">
    
      <title>లోపని అనువర్తనాల నుండి అన్వేషించు</title>
      
      <p>వ్యవస్థ అన్వేషణ అనునది ఫలితాలను వివిధ అనువర్తనాల నుండి సగటు తీయును. అన్వేషణ ఫలితాల ఎడమ-చేతి వైపున, అన్వేషణ ఫలితాలు అందించిన అనువర్తనాల ప్రతిమలను చూడవచ్చు. ఆ ప్రతిమలలో వొక దానిపై నొక్కి ఆ ప్రతిమతో సంభందించివున్న అనువర్తనం లోపల అన్వేషణను పునఃప్రారంభించుము. ఎంచేతంటే బాగా సరిపోయినవి మాత్రమే <gui>కార్యకలాపాల అవలోకనం</gui> నందు చూపబడును, అనువర్తనం లోపల అన్వేషించుట మీకు వుత్తమ ఫలితాలను ఇన్వవచ్చు.</p>

    </section>

    <section id="use-search-customize">

      <title>అన్వేషణ ఫలితాలను మలచుకొనుము</title>

      <media its:translate="no" type="image" mime="image/svg" src="gs-search-settings.svg" width="100%"/>

      <note style="important">
      <p>Your computer lets you customize what you want to display in the search
       results in the <gui>Activities Overview</gui>. For example, you can
        choose whether you want to show results for websites, photos, or music.
        </p>
      </note>


      <steps>
        <title>అన్వేషణ ఫలితాల నందు ఏమి కనిపించాలో మలచుకొనుటకు:</title>
        <item><p>Click the <gui xref="shell-introduction#yourname">system menu</gui>
        on the right side of the top bar.</p></item>
        <item><p>Click <gui>Settings</gui>.</p></item>
        <item><p>Click <gui>Search</gui> in the left panel.</p></item>
        <item><p>In the list of search locations, click the switch next to the
        search location you want to enable or disable.</p></item>
      </steps>

    </section>

</page>
