<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="task" version="1.0 ui/1.0" id="files-copy" xml:lang="hu">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-15" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Elemek új mappába másolása vagy áthelyezése.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

<title>Fájlok és mappák másolása vagy mozgatása</title>

 <p>Egy fájl vagy mappa új helyre másolásához vagy áthelyezéséhez használhatja a fogd és vidd módszert, a másolás és beillesztés parancsokat vagy gyorsbillentyűket.</p>

 <p>Szüksége lehet például egy bemutató USB-kulcsra másolására, hogy magával vihesse munkahelyére. Vagy biztonsági másolatot készíthet egy dokumentumról, mielőtt megváltoztatná azt, hogy később visszatérhessen az eredeti fájlhoz, ha nem elégedett a változtatásokkal.</p>

 <p>Ezek az utasítások fájlokra és mappákra is vonatkoznak. A fájlok és mappák ugyanúgy másolhatók és áthelyezhetők.</p>

<steps ui:expanded="false">
<title>Fájlok másolása és beillesztése</title>
<item><p>Egy kattintással válassza ki a másolni kívánt fájlt.</p></item>
<item><p>Kattintson a jobb egérgombbal, és válassza a <gui>Másolás</gui> menüpontot, vagy nyomja meg a <keyseq><key>Ctrl</key><key>C</key></keyseq> kombinációt.</p></item>
<item><p>Lépjen egy másik mappába, amelybe el kívánja helyezni a fájl másolatát.</p></item>
<item><p>Kattintson a menü gombra, és válassza a <gui>Beillesztés</gui> menüpontot a fájl másolásának befejezéséhez, vagy nyomja meg a <keyseq><key>Ctrl</key><key>V</key></keyseq> kombinációt. A fájlból ezután lesz egy példány az eredeti mappában és a másik mappában is.</p></item>
</steps>

<steps ui:expanded="false">
<title>Fájlok kivágása és beillesztése az áthelyezésükhöz</title>
<item><p>Egy kattintással válassza ki az áthelyezni kívánt fájlt.</p></item>
<item><p>Kattintson a jobb egérgombbal, és válassza a <gui>Kivágás</gui> menüpontot, vagy nyomja meg a <keyseq><key>Ctrl</key><key>X</key></keyseq> kombinációt.</p></item>
<item><p>Lépjen egy másik mappába, amelybe át kívánja helyezni a fájlt.</p></item>
<item><p>Kattintson a menü gombra az eszköztáron, és válassza a <gui>Beillesztés</gui> menüpontot a fájl áthelyezésének befejezéséhez, vagy nyomja meg a <keyseq><key>Ctrl</key><key>V</key></keyseq> kombinációt. A fájl az eredeti mappából eltűnik, és áthelyezésre kerül a másik mappába.</p></item>
</steps>

<steps ui:expanded="false">
<title>Fájlok másolása vagy áthelyezése húzással</title>
<item><p>Nyissa meg a fájlkezelőben a másolni kívánt fájlt tartalmazó mappát.</p></item>
<item><p>Válassza a <gui>Fájlok</gui> ikont a felső sávon, majd az <gui>Új ablak</gui> lehetőséget (vagy nyomja meg a <keyseq><key>Ctrl</key><key>N</key></keyseq> kombinációt) egy második ablak megnyitásához. Az új ablakban nyissa meg azt a mappát, amelybe a fájlt át szeretné helyezni vagy másolni.</p></item>
<item>
 <p>Kattintson a fájlra, és húzza át az egyik ablakból a másikba. Ezzel a fájlt <em>áthelyezi</em>, ha a cél <em>ugyanazon</em> az eszközön van, vagy <em>átmásolja</em>, ha a cél egy <em>másik</em> eszközön van.</p>
 <p>Ha például egy fájlt az USB-kulcsról a saját mappájába húz, akkor másolás történik, mert az egyik eszközről a másikra húzza.</p>
 <p>A fájl másolásának kényszerítéséhez tartsa lenyomva a <key>Ctrl</key> billentyűt a húzás közben, vagy a fájl áthelyezésének kényszerítéséhez a <key>Shift</key> billentyűt.</p>
 </item>
</steps>

<note>
  <p>Nem másolhat vagy helyezhet át egy fájlt <em>írásvédett</em> mappába. Néhány mappa írásvédett, megakadályozandó a tartalmának módosítását. Az írásvédelmet a <link xref="nautilus-file-properties-permissions">fájl jogosultságainak módosításával</link> szüntetheti meg.</p>
</note>

</page>
