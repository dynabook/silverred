<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-font-size" xml:lang="gu">

  <info>
    <link type="guide" xref="a11y#vision" group="lowvision"/>

    <revision pkgversion="3.7.1" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.26" date="2017-08-04" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>માઇકલ હીલ</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>ઍકાટેરીના ગેરાસીમોવા</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>વાંચવા માટે લખાણને સરળ બનાવવા માટે વિશાળ ફોન્ટને વાપરો.</desc>
  </info>

  <title>સ્ક્રીન પર લખાણ માપને બદલો</title>

  <p>જો તમે તમારી સ્ક્રીન પર લખાણને વાંચવામાં મુશ્કેલી હોય તો, તમે ફોન્ટનું નામ બદલી શકો છો.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Universal Access</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Universal Access</gui> to open the panel.</p>
    </item>
    <item>
      <p>In the <gui>Seeing</gui> section, switch the <gui>Large Text</gui>
      switch to on.</p>
    </item>
  </steps>

  <p>વૈકલ્પિક રીતે, તમે ટોચની પટ્ટી પર <link xref="a11y-icon">સુલભતા ચિહ્ન</link> પર ક્લિક કરીને ઝડપથી લખાણ માપને બદલી શકો છો અને <gui>વિશાળ લખાણ</gui> ને પસંદ કરી રહ્યા છે.</p>

  <note style="tip">
    <p>In many applications, you can increase the text size at any time by
    pressing <keyseq><key>Ctrl</key><key>+</key></keyseq>. To reduce the text
    size, press <keyseq><key>Ctrl</key><key>-</key></keyseq>.</p>
  </note>

  <p><gui>Large Text</gui> will scale the text by 1.2 times. You can use
  <app>Tweaks</app> to make text size bigger or smaller.</p>

</page>
