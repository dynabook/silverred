<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-check" xml:lang="id">
  <info>
    <link type="guide" xref="disk"/>


    <credit type="author">
      <name>Proyek Dokumentasi GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.91" date="2014-09-05" status="review"/>

    <desc>Uji hard disk Anda apakah bermasalah untuk memastikan itu sehat.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

<title>Periksa hard disk Anda apakah bermasalah</title>

<section id="disk-status">
 <title>Memeriksa hard disk</title>
  <p>Hard disk memiliki alat uji kesehatan bawaan bernama <app>SMART</app> (Self-Monitoring, Analysis, and Reporting Technology; Teknologi Pemantauan Mandiri, Analisis, dan Pelaporan), yang secara terus-menerus memeriksa disk tentang masalah-masalah potensial. SMART juga memperingatkan Anda bila disk akan gagal, membantu Anda menghindari kehilangan data yang penting.</p>

  <p>Walaupun SMART berjalan secara otomatis, Anda juga dapat memeriksa kesehatan disk Anda dengan menjalankan aplikasi <app>Disk</app>:</p>

<steps>
 <title>Memeriksa kesehatan disk Anda memakai aplikasi Disk</title>

  <item>
    <p>Buka <app>Pengaturan</app> dari ringkasan <gui>Aktivitas</gui>.</p>
  </item>
  <item>
    <p>Pilih disk yang Anda ingin periksa dari daftar perangkat penyimpanan di sebelah kiri. Informasi dan status disk akan ditampilkan.</p>
  </item>
  <item>
    <p>Klik tombol menu dan pilih <gui>SMART Data &amp; Periksa Mandiri…</gui>. <gui>Pendataan Keseluruhan</gui> mengatakan "Disk OK".</p>
  </item>
  <item>
    <p>Lihat infomrasi lebih lanjut pada <gui>Atribut SMART</gui>, atau klik tombol <gui style="button">Mulai Periksa-Mandiri</gui> untuk menjalankan pemeriksaan secara mandiri.</p>
  </item>

</steps>

</section>

<section id="disk-not-healthy">

 <title>Bagaimana bila disk tidak sehat?</title>

  <p>Bahkan bila <gui>Pemeriksaan Keseluruhan</gui> mengindikasikan bahwa disk <em>tidak</em> sehat, mungkin tidak ada alasan untuk alarm. Namun, lebih baik bersiap-siap dengan suatu <link xref="backup-why">backup</link> untuk mencegah kehilangan data.</p>

  <p>Jika status mengatakan "Pre-fail", disk masih cukup sehat tetapi tanda-tanda memakai telah terdeteksi yang berarti mungkin gagal dalam waktu dekat. Jika hard disk Anda (atau komputer) adalah berumur beberapa tahun, Anda akan cenderung untuk melihat pesan ini pada setidaknya beberapa pemeriksaan kesehatan. Anda harus <link xref="backup-how">backup file penting Anda secara teratur</link> dan memeriksa status disk secara berkala untuk melihat apakah itu akan lebih buruk.</p>

  <p>Bila itu semakin buruk, Anda mungkin ingin membawa komputer/hard disk ke seorang profesional untuk diagnosis atau perbaikan lebih lanjut.</p>

</section>

</page>
