<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-autobrightness" xml:lang="id">

  <info>
    <link type="guide" xref="power#saving"/>
    <link type="seealso" xref="display-brightness"/>

    <revision pkgversion="3.20" date="2016-06-15" status="candidate"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2016</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Automatically control screen brightness to reduce battery use.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

  <title>Enable automatic brightness</title>

  <p>If your computer has an integrated light sensor, it can be used to
  automatically control screen brightness. This ensures that the screen is
  always easy to see in different ambient light conditions, and helps to reduce
  battery consumption.</p>

  <steps>

    <item>
      <p>Buka ringkasan <gui xref="shell-introduction#activities">Aktivitas</gui> dan mulai mengetik <gui>Daya</gui>.</p>
    </item>
    <item>
      <p>Klik pada <gui>Daya</gui> untuk membuka panel.</p>
    </item>
    <item>
      <p>In the <gui>Power Saving</gui> section, ensure that the
      <gui>Automatic brightness</gui> switch is set to on.</p>
    </item>

  </steps>

  <p>To disable automatic screen brightness, switch it to off.</p>

</page>
